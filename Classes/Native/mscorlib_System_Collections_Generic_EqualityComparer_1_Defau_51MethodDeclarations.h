﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/TrackableResultData>
struct DefaultComparer_t4362;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/TrackableResultData>::.ctor()
 void DefaultComparer__ctor_m25906 (DefaultComparer_t4362 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/TrackableResultData>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m25907 (DefaultComparer_t4362 * __this, TrackableResultData_t684  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/TrackableResultData>::Equals(T,T)
 bool DefaultComparer_Equals_m25908 (DefaultComparer_t4362 * __this, TrackableResultData_t684  ___x, TrackableResultData_t684  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
