﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ILoadLevelEventHandler>
struct Comparer_1_t4223;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ILoadLevelEventHandler>
struct Comparer_1_t4223  : public Object_t
{
};
struct Comparer_1_t4223_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ILoadLevelEventHandler>::_default
	Comparer_1_t4223 * ____default_0;
};
