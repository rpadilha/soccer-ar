﻿#pragma once
#include <stdint.h>
// Vuforia.DataSet[]
struct DataSetU5BU5D_t4031;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t663  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.DataSet>::_items
	DataSetU5BU5D_t4031* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::_version
	int32_t ____version_3;
};
struct List_1_t663_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.DataSet>::EmptyArray
	DataSetU5BU5D_t4031* ___EmptyArray_4;
};
