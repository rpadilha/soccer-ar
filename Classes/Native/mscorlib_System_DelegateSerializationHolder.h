﻿#pragma once
#include <stdint.h>
// System.Delegate
struct Delegate_t152;
// System.Object
#include "mscorlib_System_Object.h"
// System.DelegateSerializationHolder
struct DelegateSerializationHolder_t2257  : public Object_t
{
	// System.Delegate System.DelegateSerializationHolder::_delegate
	Delegate_t152 * ____delegate_0;
};
