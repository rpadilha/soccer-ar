﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.DataSetTrackableBehaviour>
struct UnityAction_1_t3850;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetTrackableBehaviour>
struct InvokableCall_1_t3849  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.DataSetTrackableBehaviour>::Delegate
	UnityAction_1_t3850 * ___Delegate_0;
};
