﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>
struct UnityAction_1_t4876;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>
struct InvokableCall_1_t4875  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>::Delegate
	UnityAction_1_t4876 * ___Delegate_0;
};
