﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ObsoleteAttribute
struct ObsoleteAttribute_t491;
// System.String
struct String_t;

// System.Void System.ObsoleteAttribute::.ctor()
 void ObsoleteAttribute__ctor_m8142 (ObsoleteAttribute_t491 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String)
 void ObsoleteAttribute__ctor_m4700 (ObsoleteAttribute_t491 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
 void ObsoleteAttribute__ctor_m2182 (ObsoleteAttribute_t491 * __this, String_t* ___message, bool ___error, MethodInfo* method) IL2CPP_METHOD_ATTR;
