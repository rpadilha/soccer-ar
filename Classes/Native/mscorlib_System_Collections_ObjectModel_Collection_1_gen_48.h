﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.IVirtualButtonEventHandler>
struct IList_1_t4589;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<Vuforia.IVirtualButtonEventHandler>
struct Collection_1_t4590  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.IVirtualButtonEventHandler>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.IVirtualButtonEventHandler>::syncRoot
	Object_t * ___syncRoot_1;
};
