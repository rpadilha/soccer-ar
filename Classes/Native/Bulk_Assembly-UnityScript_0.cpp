﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DUnityScript_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo U3CModuleU3E_t206_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DUnityScript_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Metadata Definition <Module>
static MethodInfo* U3CModuleU3E_t206_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType U3CModuleU3E_t206_0_0_0;
extern Il2CppType U3CModuleU3E_t206_1_0_0;
struct U3CModuleU3E_t206;
TypeInfo U3CModuleU3E_t206_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t206_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &U3CModuleU3E_t206_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &U3CModuleU3E_t206_il2cpp_TypeInfo/* cast_class */
	, &U3CModuleU3E_t206_0_0_0/* byval_arg */
	, &U3CModuleU3E_t206_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t206)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Jump_Button
#include "AssemblyU2DUnityScript_Jump_Button.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Jump_Button_t207_il2cpp_TypeInfo;
// Jump_Button
#include "AssemblyU2DUnityScript_Jump_ButtonMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
// UnityEngine.GUITexture
#include "UnityEngine_UnityEngine_GUITexture.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo Input_t187_il2cpp_TypeInfo;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.GUIElement
#include "UnityEngine_UnityEngine_GUIElementMethodDeclarations.h"
extern MethodInfo MonoBehaviour__ctor_m254_MethodInfo;
extern MethodInfo Input_get_touches_m702_MethodInfo;
extern MethodInfo Touch_get_phase_m698_MethodInfo;
extern MethodInfo Component_GetComponent_TisGUITexture_t100_m666_MethodInfo;
extern MethodInfo Touch_get_position_m687_MethodInfo;
extern MethodInfo Vector2_op_Implicit_m690_MethodInfo;
extern MethodInfo GUIElement_HitTest_m691_MethodInfo;
extern MethodInfo Input_get_touchCount_m685_MethodInfo;
struct Component_t128;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// UnityEngine.CastHelper`1<UnityEngine.GUITexture>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_12.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
struct Component_t128;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.CastHelper`1<System.Object>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_0.h"
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
 Object_t * Component_GetComponent_TisObject_t_m280_gshared (Component_t128 * __this, MethodInfo* method);
#define Component_GetComponent_TisObject_t_m280(__this, method) (Object_t *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.GUITexture>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUITexture>()
#define Component_GetComponent_TisGUITexture_t100_m666(__this, method) (GUITexture_t100 *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)


// System.Void Jump_Button::.ctor()
extern MethodInfo Jump_Button__ctor_m731_MethodInfo;
 void Jump_Button__ctor_m731 (Jump_Button_t207 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// System.Void Jump_Button::Update()
extern MethodInfo Jump_Button_Update_m732_MethodInfo;
 void Jump_Button_Update_m732 (Jump_Button_t207 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	Touch_t201  V_1 = {0};
	{
		V_0 = 0;
		goto IL_0045;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		TouchU5BU5D_t203* L_0 = Input_get_touches_m702(NULL /*static, unused*/, /*hidden argument*/&Input_get_touches_m702_MethodInfo);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, V_0);
		V_1 = (*(Touch_t201 *)((Touch_t201 *)(Touch_t201 *)SZArrayLdElema(L_0, V_0)));
		int32_t L_1 = Touch_get_phase_m698((&V_1), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((uint32_t)L_1) != ((uint32_t)0)))
		{
			goto IL_0041;
		}
	}
	{
		GUITexture_t100 * L_2 = Component_GetComponent_TisGUITexture_t100_m666(__this, /*hidden argument*/&Component_GetComponent_TisGUITexture_t100_m666_MethodInfo);
		Vector2_t99  L_3 = Touch_get_position_m687((&V_1), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector3_t73  L_4 = Vector2_op_Implicit_m690(NULL /*static, unused*/, L_3, /*hidden argument*/&Vector2_op_Implicit_m690_MethodInfo);
		NullCheck(L_2);
		bool L_5 = GUIElement_HitTest_m691(L_2, L_4, /*hidden argument*/&GUIElement_HitTest_m691_MethodInfo);
		if (!L_5)
		{
			goto IL_0041;
		}
	}

IL_0041:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		int32_t L_6 = Input_get_touchCount_m685(NULL /*static, unused*/, /*hidden argument*/&Input_get_touchCount_m685_MethodInfo);
		if ((((int32_t)V_0) < ((int32_t)L_6)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Jump_Button::Main()
extern MethodInfo Jump_Button_Main_m733_MethodInfo;
 void Jump_Button_Main_m733 (Jump_Button_t207 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition Jump_Button
extern Il2CppType GameObject_t29_0_0_6;
FieldInfo Jump_Button_t207____player_2_FieldInfo = 
{
	"player"/* name */
	, &GameObject_t29_0_0_6/* type */
	, &Jump_Button_t207_il2cpp_TypeInfo/* parent */
	, offsetof(Jump_Button_t207, ___player_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Jump_Button_t207_FieldInfos[] =
{
	&Jump_Button_t207____player_2_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Jump_Button::.ctor()
MethodInfo Jump_Button__ctor_m731_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Jump_Button__ctor_m731/* method */
	, &Jump_Button_t207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Jump_Button::Update()
MethodInfo Jump_Button_Update_m732_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&Jump_Button_Update_m732/* method */
	, &Jump_Button_t207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Jump_Button::Main()
MethodInfo Jump_Button_Main_m733_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&Jump_Button_Main_m733/* method */
	, &Jump_Button_t207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Jump_Button_t207_MethodInfos[] =
{
	&Jump_Button__ctor_m731_MethodInfo,
	&Jump_Button_Update_m732_MethodInfo,
	&Jump_Button_Main_m733_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m197_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Object_GetHashCode_m199_MethodInfo;
extern MethodInfo Object_ToString_m200_MethodInfo;
static MethodInfo* Jump_Button_t207_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&Jump_Button_Update_m732_MethodInfo,
	&Jump_Button_Main_m733_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType Jump_Button_t207_0_0_0;
extern Il2CppType Jump_Button_t207_1_0_0;
extern TypeInfo MonoBehaviour_t10_il2cpp_TypeInfo;
struct Jump_Button_t207;
TypeInfo Jump_Button_t207_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "Jump_Button"/* name */
	, ""/* namespaze */
	, Jump_Button_t207_MethodInfos/* methods */
	, NULL/* properties */
	, Jump_Button_t207_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Jump_Button_t207_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Jump_Button_t207_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Jump_Button_t207_il2cpp_TypeInfo/* cast_class */
	, &Jump_Button_t207_0_0_0/* byval_arg */
	, &Jump_Button_t207_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Jump_Button_t207)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraRelativeControl
#include "AssemblyU2DUnityScript_CameraRelativeControl.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CameraRelativeControl_t210_il2cpp_TypeInfo;
// CameraRelativeControl
#include "AssemblyU2DUnityScript_CameraRelativeControlMethodDeclarations.h"

// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.CharacterController
#include "UnityEngine_UnityEngine_CharacterController.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// Joystick
#include "AssemblyU2DUnityScript_Joystick.h"
// UnityEngine.CollisionFlags
#include "UnityEngine_UnityEngine_CollisionFlags.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
extern TypeInfo Vector2_t99_il2cpp_TypeInfo;
extern TypeInfo Transform_t74_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo CharacterController_t209_il2cpp_TypeInfo;
extern TypeInfo Vector3_t73_il2cpp_TypeInfo;
extern TypeInfo Joystick_t208_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
extern TypeInfo Mathf_t179_il2cpp_TypeInfo;
extern TypeInfo Boolean_t122_il2cpp_TypeInfo;
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.CharacterController
#include "UnityEngine_UnityEngine_CharacterControllerMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// Joystick
#include "AssemblyU2DUnityScript_JoystickMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
extern Il2CppType Transform_t74_0_0_0;
extern Il2CppType CharacterController_t209_0_0_0;
extern MethodInfo Vector2__ctor_m636_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Component_GetComponent_m800_MethodInfo;
extern MethodInfo GameObject_Find_m801_MethodInfo;
extern MethodInfo Object_op_Implicit_m257_MethodInfo;
extern MethodInfo GameObject_get_transform_m537_MethodInfo;
extern MethodInfo Transform_get_position_m538_MethodInfo;
extern MethodInfo Transform_set_position_m570_MethodInfo;
extern MethodInfo CharacterController_get_velocity_m802_MethodInfo;
extern MethodInfo Vector3_get_magnitude_m577_MethodInfo;
extern MethodInfo Vector3_get_normalized_m706_MethodInfo;
extern MethodInfo Transform_set_forward_m708_MethodInfo;
extern MethodInfo Joystick_Disable_m753_MethodInfo;
extern MethodInfo Behaviour_set_enabled_m547_MethodInfo;
extern MethodInfo Vector3__ctor_m568_MethodInfo;
extern MethodInfo Transform_TransformDirection_m803_MethodInfo;
extern MethodInfo Vector3_Normalize_m581_MethodInfo;
extern MethodInfo Mathf_Abs_m699_MethodInfo;
extern MethodInfo Vector3_op_Multiply_m573_MethodInfo;
extern MethodInfo CharacterController_get_isGrounded_m804_MethodInfo;
extern MethodInfo Joystick_IsFingerDown_m755_MethodInfo;
extern MethodInfo Physics_get_gravity_m805_MethodInfo;
extern MethodInfo Time_get_deltaTime_m615_MethodInfo;
extern MethodInfo Vector3_op_Addition_m576_MethodInfo;
extern MethodInfo CharacterController_Move_m806_MethodInfo;
extern MethodInfo Vector3_get_zero_m807_MethodInfo;
extern MethodInfo CameraRelativeControl_FaceMovementDirection_m736_MethodInfo;
extern MethodInfo Vector2_op_Multiply_m808_MethodInfo;
extern MethodInfo Transform_Rotate_m809_MethodInfo;
extern MethodInfo Transform_Rotate_m623_MethodInfo;


// System.Void CameraRelativeControl::.ctor()
extern MethodInfo CameraRelativeControl__ctor_m734_MethodInfo;
 void CameraRelativeControl__ctor_m734 (CameraRelativeControl_t210 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		__this->___speed_6 = (((float)5));
		__this->___jumpSpeed_7 = (((float)8));
		__this->___inAirMultiplier_8 = (0.25f);
		Vector2_t99  L_0 = {0};
		Vector2__ctor_m636(&L_0, (((float)((int32_t)50))), (((float)((int32_t)25))), /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		__this->___rotationSpeed_9 = L_0;
		__this->___canJump_13 = 1;
		return;
	}
}
// System.Void CameraRelativeControl::Start()
extern MethodInfo CameraRelativeControl_Start_m735_MethodInfo;
 void CameraRelativeControl_Start_m735 (CameraRelativeControl_t210 * __this, MethodInfo* method){
	GameObject_t29 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Transform_t74_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Component_t128 * L_1 = Component_GetComponent_m800(__this, L_0, /*hidden argument*/&Component_GetComponent_m800_MethodInfo);
		__this->___thisTransform_10 = ((Transform_t74 *)Castclass(L_1, InitializedTypeInfo(&Transform_t74_il2cpp_TypeInfo)));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&CharacterController_t209_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Component_t128 * L_3 = Component_GetComponent_m800(__this, L_2, /*hidden argument*/&Component_GetComponent_m800_MethodInfo);
		__this->___character_11 = ((CharacterController_t209 *)Castclass(L_3, InitializedTypeInfo(&CharacterController_t209_il2cpp_TypeInfo)));
		GameObject_t29 * L_4 = GameObject_Find_m801(NULL /*static, unused*/, (String_t*) &_stringLiteral81, /*hidden argument*/&GameObject_Find_m801_MethodInfo);
		V_0 = L_4;
		bool L_5 = Object_op_Implicit_m257(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_5)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t74 * L_6 = (__this->___thisTransform_10);
		NullCheck(V_0);
		Transform_t74 * L_7 = GameObject_get_transform_m537(V_0, /*hidden argument*/&GameObject_get_transform_m537_MethodInfo);
		NullCheck(L_7);
		Vector3_t73  L_8 = Transform_get_position_m538(L_7, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		NullCheck(L_6);
		Transform_set_position_m570(L_6, L_8, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
	}

IL_0062:
	{
		return;
	}
}
// System.Void CameraRelativeControl::FaceMovementDirection()
 void CameraRelativeControl_FaceMovementDirection_m736 (CameraRelativeControl_t210 * __this, MethodInfo* method){
	Vector3_t73  V_0 = {0};
	{
		CharacterController_t209 * L_0 = (__this->___character_11);
		NullCheck(L_0);
		Vector3_t73  L_1 = CharacterController_get_velocity_m802(L_0, /*hidden argument*/&CharacterController_get_velocity_m802_MethodInfo);
		V_0 = L_1;
		NullCheck((&V_0));
		(&V_0)->___y_2 = (((float)0));
		float L_2 = Vector3_get_magnitude_m577((&V_0), /*hidden argument*/&Vector3_get_magnitude_m577_MethodInfo);
		if ((((float)L_2) <= ((float)(0.1f))))
		{
			goto IL_0038;
		}
	}
	{
		Transform_t74 * L_3 = (__this->___thisTransform_10);
		Vector3_t73  L_4 = Vector3_get_normalized_m706((&V_0), /*hidden argument*/&Vector3_get_normalized_m706_MethodInfo);
		NullCheck(L_3);
		Transform_set_forward_m708(L_3, L_4, /*hidden argument*/&Transform_set_forward_m708_MethodInfo);
	}

IL_0038:
	{
		return;
	}
}
// System.Void CameraRelativeControl::OnEndGame()
extern MethodInfo CameraRelativeControl_OnEndGame_m737_MethodInfo;
 void CameraRelativeControl_OnEndGame_m737 (CameraRelativeControl_t210 * __this, MethodInfo* method){
	{
		Joystick_t208 * L_0 = (__this->___moveJoystick_2);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(&Joystick_Disable_m753_MethodInfo, L_0);
		Joystick_t208 * L_1 = (__this->___rotateJoystick_3);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(&Joystick_Disable_m753_MethodInfo, L_1);
		Behaviour_set_enabled_m547(__this, 0, /*hidden argument*/&Behaviour_set_enabled_m547_MethodInfo);
		return;
	}
}
// System.Void CameraRelativeControl::Update()
extern MethodInfo CameraRelativeControl_Update_m738_MethodInfo;
 void CameraRelativeControl_Update_m738 (CameraRelativeControl_t210 * __this, MethodInfo* method){
	Vector3_t73  V_0 = {0};
	Vector2_t99  V_1 = {0};
	Vector2_t99  V_2 = {0};
	Vector3_t73  V_3 = {0};
	float G_B2_0 = 0.0f;
	Vector3_t73  G_B2_1 = {0};
	float G_B1_0 = 0.0f;
	Vector3_t73  G_B1_1 = {0};
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	Vector3_t73  G_B3_2 = {0};
	{
		Transform_t74 * L_0 = (__this->___cameraTransform_5);
		Joystick_t208 * L_1 = (__this->___moveJoystick_2);
		NullCheck(L_1);
		Vector2_t99 * L_2 = &(L_1->___position_9);
		NullCheck(L_2);
		float L_3 = (L_2->___x_1);
		Joystick_t208 * L_4 = (__this->___moveJoystick_2);
		NullCheck(L_4);
		Vector2_t99 * L_5 = &(L_4->___position_9);
		NullCheck(L_5);
		float L_6 = (L_5->___y_2);
		Vector3_t73  L_7 = {0};
		Vector3__ctor_m568(&L_7, L_3, (((float)0)), L_6, /*hidden argument*/&Vector3__ctor_m568_MethodInfo);
		NullCheck(L_0);
		Vector3_t73  L_8 = Transform_TransformDirection_m803(L_0, L_7, /*hidden argument*/&Transform_TransformDirection_m803_MethodInfo);
		V_0 = L_8;
		NullCheck((&V_0));
		(&V_0)->___y_2 = (((float)0));
		Vector3_Normalize_m581((&V_0), /*hidden argument*/&Vector3_Normalize_m581_MethodInfo);
		Joystick_t208 * L_9 = (__this->___moveJoystick_2);
		NullCheck(L_9);
		Vector2_t99 * L_10 = &(L_9->___position_9);
		NullCheck(L_10);
		float L_11 = (L_10->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_12 = fabsf(L_11);
		Joystick_t208 * L_13 = (__this->___moveJoystick_2);
		NullCheck(L_13);
		Vector2_t99 * L_14 = &(L_13->___position_9);
		NullCheck(L_14);
		float L_15 = (L_14->___y_2);
		float L_16 = fabsf(L_15);
		Vector2_t99  L_17 = {0};
		Vector2__ctor_m636(&L_17, L_12, L_16, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		V_1 = L_17;
		float L_18 = (__this->___speed_6);
		NullCheck((&V_1));
		float L_19 = ((&V_1)->___x_1);
		NullCheck((&V_1));
		float L_20 = ((&V_1)->___y_2);
		G_B1_0 = L_18;
		G_B1_1 = V_0;
		if ((((float)L_19) <= ((float)L_20)))
		{
			G_B2_0 = L_18;
			G_B2_1 = V_0;
			goto IL_0099;
		}
	}
	{
		NullCheck((&V_1));
		float L_21 = ((&V_1)->___x_1);
		G_B3_0 = L_21;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_00a0;
	}

IL_0099:
	{
		NullCheck((&V_1));
		float L_22 = ((&V_1)->___y_2);
		G_B3_0 = L_22;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_00a0:
	{
		Vector3_t73  L_23 = Vector3_op_Multiply_m573(NULL /*static, unused*/, G_B3_2, ((float)((float)G_B3_1*(float)G_B3_0)), /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_23;
		CharacterController_t209 * L_24 = (__this->___character_11);
		NullCheck(L_24);
		bool L_25 = CharacterController_get_isGrounded_m804(L_24, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_25)
		{
			goto IL_0118;
		}
	}
	{
		Joystick_t208 * L_26 = (__this->___rotateJoystick_3);
		NullCheck(L_26);
		bool L_27 = (bool)VirtFuncInvoker0< bool >::Invoke(&Joystick_IsFingerDown_m755_MethodInfo, L_26);
		if (L_27)
		{
			goto IL_00ce;
		}
	}
	{
		__this->___canJump_13 = 1;
	}

IL_00ce:
	{
		bool L_28 = (__this->___canJump_13);
		if (!L_28)
		{
			goto IL_0113;
		}
	}
	{
		Joystick_t208 * L_29 = (__this->___rotateJoystick_3);
		NullCheck(L_29);
		int32_t L_30 = (L_29->___tapCount_10);
		if ((((uint32_t)L_30) != ((uint32_t)2)))
		{
			goto IL_0113;
		}
	}
	{
		CharacterController_t209 * L_31 = (__this->___character_11);
		NullCheck(L_31);
		Vector3_t73  L_32 = CharacterController_get_velocity_m802(L_31, /*hidden argument*/&CharacterController_get_velocity_m802_MethodInfo);
		__this->___velocity_12 = L_32;
		Vector3_t73 * L_33 = &(__this->___velocity_12);
		float L_34 = (__this->___jumpSpeed_7);
		NullCheck(L_33);
		L_33->___y_2 = L_34;
		__this->___canJump_13 = 0;
	}

IL_0113:
	{
		goto IL_016c;
	}

IL_0118:
	{
		Vector3_t73 * L_35 = &(__this->___velocity_12);
		Vector3_t73 * L_36 = &(__this->___velocity_12);
		NullCheck(L_36);
		float L_37 = (L_36->___y_2);
		Vector3_t73  L_38 = Physics_get_gravity_m805(NULL /*static, unused*/, /*hidden argument*/&Physics_get_gravity_m805_MethodInfo);
		V_3 = L_38;
		NullCheck((&V_3));
		float L_39 = ((&V_3)->___y_2);
		float L_40 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		NullCheck(L_35);
		L_35->___y_2 = ((float)(L_37+((float)((float)L_39*(float)L_40))));
		NullCheck((&V_0));
		float L_41 = ((&V_0)->___x_1);
		float L_42 = (__this->___inAirMultiplier_8);
		NullCheck((&V_0));
		(&V_0)->___x_1 = ((float)((float)L_41*(float)L_42));
		NullCheck((&V_0));
		float L_43 = ((&V_0)->___z_3);
		float L_44 = (__this->___inAirMultiplier_8);
		NullCheck((&V_0));
		(&V_0)->___z_3 = ((float)((float)L_43*(float)L_44));
	}

IL_016c:
	{
		Vector3_t73  L_45 = (__this->___velocity_12);
		Vector3_t73  L_46 = Vector3_op_Addition_m576(NULL /*static, unused*/, V_0, L_45, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_0 = L_46;
		Vector3_t73  L_47 = Physics_get_gravity_m805(NULL /*static, unused*/, /*hidden argument*/&Physics_get_gravity_m805_MethodInfo);
		Vector3_t73  L_48 = Vector3_op_Addition_m576(NULL /*static, unused*/, V_0, L_47, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_0 = L_48;
		float L_49 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		Vector3_t73  L_50 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_0, L_49, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_50;
		CharacterController_t209 * L_51 = (__this->___character_11);
		NullCheck(L_51);
		CharacterController_Move_m806(L_51, V_0, /*hidden argument*/&CharacterController_Move_m806_MethodInfo);
		CharacterController_t209 * L_52 = (__this->___character_11);
		NullCheck(L_52);
		bool L_53 = CharacterController_get_isGrounded_m804(L_52, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_53)
		{
			goto IL_01b9;
		}
	}
	{
		Vector3_t73  L_54 = Vector3_get_zero_m807(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m807_MethodInfo);
		__this->___velocity_12 = L_54;
	}

IL_01b9:
	{
		VirtActionInvoker0::Invoke(&CameraRelativeControl_FaceMovementDirection_m736_MethodInfo, __this);
		Joystick_t208 * L_55 = (__this->___rotateJoystick_3);
		NullCheck(L_55);
		Vector2_t99  L_56 = (L_55->___position_9);
		V_2 = L_56;
		NullCheck((&V_2));
		float L_57 = ((&V_2)->___x_1);
		Vector2_t99 * L_58 = &(__this->___rotationSpeed_9);
		NullCheck(L_58);
		float L_59 = (L_58->___x_1);
		NullCheck((&V_2));
		(&V_2)->___x_1 = ((float)((float)L_57*(float)L_59));
		NullCheck((&V_2));
		float L_60 = ((&V_2)->___y_2);
		Vector2_t99 * L_61 = &(__this->___rotationSpeed_9);
		NullCheck(L_61);
		float L_62 = (L_61->___y_2);
		NullCheck((&V_2));
		(&V_2)->___y_2 = ((float)((float)L_60*(float)L_62));
		float L_63 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		Vector2_t99  L_64 = Vector2_op_Multiply_m808(NULL /*static, unused*/, V_2, L_63, /*hidden argument*/&Vector2_op_Multiply_m808_MethodInfo);
		V_2 = L_64;
		Transform_t74 * L_65 = (__this->___cameraPivot_4);
		NullCheck((&V_2));
		float L_66 = ((&V_2)->___x_1);
		NullCheck(L_65);
		Transform_Rotate_m809(L_65, (((float)0)), L_66, (((float)0)), 0, /*hidden argument*/&Transform_Rotate_m809_MethodInfo);
		Transform_t74 * L_67 = (__this->___cameraPivot_4);
		NullCheck((&V_2));
		float L_68 = ((&V_2)->___y_2);
		NullCheck(L_67);
		Transform_Rotate_m623(L_67, L_68, (((float)0)), (((float)0)), /*hidden argument*/&Transform_Rotate_m623_MethodInfo);
		return;
	}
}
// System.Void CameraRelativeControl::Main()
extern MethodInfo CameraRelativeControl_Main_m739_MethodInfo;
 void CameraRelativeControl_Main_m739 (CameraRelativeControl_t210 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition CameraRelativeControl
extern Il2CppType Joystick_t208_0_0_6;
FieldInfo CameraRelativeControl_t210____moveJoystick_2_FieldInfo = 
{
	"moveJoystick"/* name */
	, &Joystick_t208_0_0_6/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___moveJoystick_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Joystick_t208_0_0_6;
FieldInfo CameraRelativeControl_t210____rotateJoystick_3_FieldInfo = 
{
	"rotateJoystick"/* name */
	, &Joystick_t208_0_0_6/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___rotateJoystick_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_6;
FieldInfo CameraRelativeControl_t210____cameraPivot_4_FieldInfo = 
{
	"cameraPivot"/* name */
	, &Transform_t74_0_0_6/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___cameraPivot_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_6;
FieldInfo CameraRelativeControl_t210____cameraTransform_5_FieldInfo = 
{
	"cameraTransform"/* name */
	, &Transform_t74_0_0_6/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___cameraTransform_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo CameraRelativeControl_t210____speed_6_FieldInfo = 
{
	"speed"/* name */
	, &Single_t170_0_0_6/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___speed_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo CameraRelativeControl_t210____jumpSpeed_7_FieldInfo = 
{
	"jumpSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___jumpSpeed_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo CameraRelativeControl_t210____inAirMultiplier_8_FieldInfo = 
{
	"inAirMultiplier"/* name */
	, &Single_t170_0_0_6/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___inAirMultiplier_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo CameraRelativeControl_t210____rotationSpeed_9_FieldInfo = 
{
	"rotationSpeed"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___rotationSpeed_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_1;
FieldInfo CameraRelativeControl_t210____thisTransform_10_FieldInfo = 
{
	"thisTransform"/* name */
	, &Transform_t74_0_0_1/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___thisTransform_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType CharacterController_t209_0_0_1;
FieldInfo CameraRelativeControl_t210____character_11_FieldInfo = 
{
	"character"/* name */
	, &CharacterController_t209_0_0_1/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___character_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo CameraRelativeControl_t210____velocity_12_FieldInfo = 
{
	"velocity"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___velocity_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo CameraRelativeControl_t210____canJump_13_FieldInfo = 
{
	"canJump"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* parent */
	, offsetof(CameraRelativeControl_t210, ___canJump_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CameraRelativeControl_t210_FieldInfos[] =
{
	&CameraRelativeControl_t210____moveJoystick_2_FieldInfo,
	&CameraRelativeControl_t210____rotateJoystick_3_FieldInfo,
	&CameraRelativeControl_t210____cameraPivot_4_FieldInfo,
	&CameraRelativeControl_t210____cameraTransform_5_FieldInfo,
	&CameraRelativeControl_t210____speed_6_FieldInfo,
	&CameraRelativeControl_t210____jumpSpeed_7_FieldInfo,
	&CameraRelativeControl_t210____inAirMultiplier_8_FieldInfo,
	&CameraRelativeControl_t210____rotationSpeed_9_FieldInfo,
	&CameraRelativeControl_t210____thisTransform_10_FieldInfo,
	&CameraRelativeControl_t210____character_11_FieldInfo,
	&CameraRelativeControl_t210____velocity_12_FieldInfo,
	&CameraRelativeControl_t210____canJump_13_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void CameraRelativeControl::.ctor()
MethodInfo CameraRelativeControl__ctor_m734_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CameraRelativeControl__ctor_m734/* method */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void CameraRelativeControl::Start()
MethodInfo CameraRelativeControl_Start_m735_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&CameraRelativeControl_Start_m735/* method */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void CameraRelativeControl::FaceMovementDirection()
MethodInfo CameraRelativeControl_FaceMovementDirection_m736_MethodInfo = 
{
	"FaceMovementDirection"/* name */
	, (methodPointerType)&CameraRelativeControl_FaceMovementDirection_m736/* method */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void CameraRelativeControl::OnEndGame()
MethodInfo CameraRelativeControl_OnEndGame_m737_MethodInfo = 
{
	"OnEndGame"/* name */
	, (methodPointerType)&CameraRelativeControl_OnEndGame_m737/* method */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void CameraRelativeControl::Update()
MethodInfo CameraRelativeControl_Update_m738_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&CameraRelativeControl_Update_m738/* method */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void CameraRelativeControl::Main()
MethodInfo CameraRelativeControl_Main_m739_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&CameraRelativeControl_Main_m739/* method */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CameraRelativeControl_t210_MethodInfos[] =
{
	&CameraRelativeControl__ctor_m734_MethodInfo,
	&CameraRelativeControl_Start_m735_MethodInfo,
	&CameraRelativeControl_FaceMovementDirection_m736_MethodInfo,
	&CameraRelativeControl_OnEndGame_m737_MethodInfo,
	&CameraRelativeControl_Update_m738_MethodInfo,
	&CameraRelativeControl_Main_m739_MethodInfo,
	NULL
};
static MethodInfo* CameraRelativeControl_t210_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&CameraRelativeControl_Start_m735_MethodInfo,
	&CameraRelativeControl_FaceMovementDirection_m736_MethodInfo,
	&CameraRelativeControl_OnEndGame_m737_MethodInfo,
	&CameraRelativeControl_Update_m738_MethodInfo,
	&CameraRelativeControl_Main_m739_MethodInfo,
};
extern TypeInfo RequireComponent_t154_il2cpp_TypeInfo;
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
extern MethodInfo RequireComponent__ctor_m348_MethodInfo;
void CameraRelativeControl_t210_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t154 * tmp;
		tmp = (RequireComponent_t154 *)il2cpp_codegen_object_new (&RequireComponent_t154_il2cpp_TypeInfo);
		RequireComponent__ctor_m348(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&CharacterController_t209_il2cpp_TypeInfo)), &RequireComponent__ctor_m348_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache CameraRelativeControl_t210__CustomAttributeCache = {
1,
NULL,
&CameraRelativeControl_t210_CustomAttributesCacheGenerator
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType CameraRelativeControl_t210_0_0_0;
extern Il2CppType CameraRelativeControl_t210_1_0_0;
struct CameraRelativeControl_t210;
extern CustomAttributesCache CameraRelativeControl_t210__CustomAttributeCache;
TypeInfo CameraRelativeControl_t210_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "CameraRelativeControl"/* name */
	, ""/* namespaze */
	, CameraRelativeControl_t210_MethodInfos/* methods */
	, NULL/* properties */
	, CameraRelativeControl_t210_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CameraRelativeControl_t210_VTable/* vtable */
	, &CameraRelativeControl_t210__CustomAttributeCache/* custom_attributes_cache */
	, &CameraRelativeControl_t210_il2cpp_TypeInfo/* cast_class */
	, &CameraRelativeControl_t210_0_0_0/* byval_arg */
	, &CameraRelativeControl_t210_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraRelativeControl_t210)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// FirstPersonControl
#include "AssemblyU2DUnityScript_FirstPersonControl.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo FirstPersonControl_t211_il2cpp_TypeInfo;
// FirstPersonControl
#include "AssemblyU2DUnityScript_FirstPersonControlMethodDeclarations.h"

extern MethodInfo Vector2_get_zero_m665_MethodInfo;
extern MethodInfo Input_get_acceleration_m810_MethodInfo;


// System.Void FirstPersonControl::.ctor()
extern MethodInfo FirstPersonControl__ctor_m740_MethodInfo;
 void FirstPersonControl__ctor_m740 (FirstPersonControl_t211 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		__this->___forwardSpeed_5 = (((float)4));
		__this->___backwardSpeed_6 = (((float)1));
		__this->___sidestepSpeed_7 = (((float)1));
		__this->___jumpSpeed_8 = (((float)8));
		__this->___inAirMultiplier_9 = (0.25f);
		Vector2_t99  L_0 = {0};
		Vector2__ctor_m636(&L_0, (((float)((int32_t)50))), (((float)((int32_t)25))), /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		__this->___rotationSpeed_10 = L_0;
		__this->___tiltPositiveYAxis_11 = (0.6f);
		__this->___tiltNegativeYAxis_12 = (0.4f);
		__this->___tiltXAxisMinimum_13 = (0.1f);
		__this->___canJump_18 = 1;
		return;
	}
}
// System.Void FirstPersonControl::Start()
extern MethodInfo FirstPersonControl_Start_m741_MethodInfo;
 void FirstPersonControl_Start_m741 (FirstPersonControl_t211 * __this, MethodInfo* method){
	GameObject_t29 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Transform_t74_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Component_t128 * L_1 = Component_GetComponent_m800(__this, L_0, /*hidden argument*/&Component_GetComponent_m800_MethodInfo);
		__this->___thisTransform_14 = ((Transform_t74 *)Castclass(L_1, InitializedTypeInfo(&Transform_t74_il2cpp_TypeInfo)));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&CharacterController_t209_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Component_t128 * L_3 = Component_GetComponent_m800(__this, L_2, /*hidden argument*/&Component_GetComponent_m800_MethodInfo);
		__this->___character_15 = ((CharacterController_t209 *)Castclass(L_3, InitializedTypeInfo(&CharacterController_t209_il2cpp_TypeInfo)));
		GameObject_t29 * L_4 = GameObject_Find_m801(NULL /*static, unused*/, (String_t*) &_stringLiteral81, /*hidden argument*/&GameObject_Find_m801_MethodInfo);
		V_0 = L_4;
		bool L_5 = Object_op_Implicit_m257(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_5)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t74 * L_6 = (__this->___thisTransform_14);
		NullCheck(V_0);
		Transform_t74 * L_7 = GameObject_get_transform_m537(V_0, /*hidden argument*/&GameObject_get_transform_m537_MethodInfo);
		NullCheck(L_7);
		Vector3_t73  L_8 = Transform_get_position_m538(L_7, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		NullCheck(L_6);
		Transform_set_position_m570(L_6, L_8, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
	}

IL_0062:
	{
		return;
	}
}
// System.Void FirstPersonControl::OnEndGame()
extern MethodInfo FirstPersonControl_OnEndGame_m742_MethodInfo;
 void FirstPersonControl_OnEndGame_m742 (FirstPersonControl_t211 * __this, MethodInfo* method){
	{
		Joystick_t208 * L_0 = (__this->___moveTouchPad_2);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(&Joystick_Disable_m753_MethodInfo, L_0);
		Joystick_t208 * L_1 = (__this->___rotateTouchPad_3);
		bool L_2 = Object_op_Implicit_m257(NULL /*static, unused*/, L_1, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		Joystick_t208 * L_3 = (__this->___rotateTouchPad_3);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(&Joystick_Disable_m753_MethodInfo, L_3);
	}

IL_0026:
	{
		Behaviour_set_enabled_m547(__this, 0, /*hidden argument*/&Behaviour_set_enabled_m547_MethodInfo);
		return;
	}
}
// System.Void FirstPersonControl::Update()
extern MethodInfo FirstPersonControl_Update_m743_MethodInfo;
 void FirstPersonControl_Update_m743 (FirstPersonControl_t211 * __this, MethodInfo* method){
	Vector3_t73  V_0 = {0};
	Vector2_t99  V_1 = {0};
	bool V_2 = false;
	Joystick_t208 * V_3 = {0};
	Vector2_t99  V_4 = {0};
	Vector3_t73  V_5 = {0};
	float V_6 = 0.0f;
	Vector3_t73  V_7 = {0};
	{
		Transform_t74 * L_0 = (__this->___thisTransform_14);
		Joystick_t208 * L_1 = (__this->___moveTouchPad_2);
		NullCheck(L_1);
		Vector2_t99 * L_2 = &(L_1->___position_9);
		NullCheck(L_2);
		float L_3 = (L_2->___x_1);
		Joystick_t208 * L_4 = (__this->___moveTouchPad_2);
		NullCheck(L_4);
		Vector2_t99 * L_5 = &(L_4->___position_9);
		NullCheck(L_5);
		float L_6 = (L_5->___y_2);
		Vector3_t73  L_7 = {0};
		Vector3__ctor_m568(&L_7, L_3, (((float)0)), L_6, /*hidden argument*/&Vector3__ctor_m568_MethodInfo);
		NullCheck(L_0);
		Vector3_t73  L_8 = Transform_TransformDirection_m803(L_0, L_7, /*hidden argument*/&Transform_TransformDirection_m803_MethodInfo);
		V_0 = L_8;
		NullCheck((&V_0));
		(&V_0)->___y_2 = (((float)0));
		Vector3_Normalize_m581((&V_0), /*hidden argument*/&Vector3_Normalize_m581_MethodInfo);
		Joystick_t208 * L_9 = (__this->___moveTouchPad_2);
		NullCheck(L_9);
		Vector2_t99 * L_10 = &(L_9->___position_9);
		NullCheck(L_10);
		float L_11 = (L_10->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_12 = fabsf(L_11);
		Joystick_t208 * L_13 = (__this->___moveTouchPad_2);
		NullCheck(L_13);
		Vector2_t99 * L_14 = &(L_13->___position_9);
		NullCheck(L_14);
		float L_15 = (L_14->___y_2);
		float L_16 = fabsf(L_15);
		Vector2_t99  L_17 = {0};
		Vector2__ctor_m636(&L_17, L_12, L_16, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		V_1 = L_17;
		NullCheck((&V_1));
		float L_18 = ((&V_1)->___y_2);
		NullCheck((&V_1));
		float L_19 = ((&V_1)->___x_1);
		if ((((float)L_18) <= ((float)L_19)))
		{
			goto IL_00d1;
		}
	}
	{
		Joystick_t208 * L_20 = (__this->___moveTouchPad_2);
		NullCheck(L_20);
		Vector2_t99 * L_21 = &(L_20->___position_9);
		NullCheck(L_21);
		float L_22 = (L_21->___y_2);
		if ((((float)L_22) <= ((float)(((float)0)))))
		{
			goto IL_00b7;
		}
	}
	{
		float L_23 = (__this->___forwardSpeed_5);
		NullCheck((&V_1));
		float L_24 = ((&V_1)->___y_2);
		Vector3_t73  L_25 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_0, ((float)((float)L_23*(float)L_24)), /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_25;
		goto IL_00cc;
	}

IL_00b7:
	{
		float L_26 = (__this->___backwardSpeed_6);
		NullCheck((&V_1));
		float L_27 = ((&V_1)->___y_2);
		Vector3_t73  L_28 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_0, ((float)((float)L_26*(float)L_27)), /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_28;
	}

IL_00cc:
	{
		goto IL_00e6;
	}

IL_00d1:
	{
		float L_29 = (__this->___sidestepSpeed_7);
		NullCheck((&V_1));
		float L_30 = ((&V_1)->___x_1);
		Vector3_t73  L_31 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_0, ((float)((float)L_29*(float)L_30)), /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_31;
	}

IL_00e6:
	{
		CharacterController_t209 * L_32 = (__this->___character_15);
		NullCheck(L_32);
		bool L_33 = CharacterController_get_isGrounded_m804(L_32, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_33)
		{
			goto IL_017c;
		}
	}
	{
		V_2 = 0;
		V_3 = (Joystick_t208 *)NULL;
		Joystick_t208 * L_34 = (__this->___rotateTouchPad_3);
		bool L_35 = Object_op_Implicit_m257(NULL /*static, unused*/, L_34, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_35)
		{
			goto IL_0116;
		}
	}
	{
		Joystick_t208 * L_36 = (__this->___rotateTouchPad_3);
		V_3 = L_36;
		goto IL_011d;
	}

IL_0116:
	{
		Joystick_t208 * L_37 = (__this->___moveTouchPad_2);
		V_3 = L_37;
	}

IL_011d:
	{
		NullCheck(V_3);
		bool L_38 = (bool)VirtFuncInvoker0< bool >::Invoke(&Joystick_IsFingerDown_m755_MethodInfo, V_3);
		if (L_38)
		{
			goto IL_012f;
		}
	}
	{
		__this->___canJump_18 = 1;
	}

IL_012f:
	{
		bool L_39 = (__this->___canJump_18);
		if (!L_39)
		{
			goto IL_014f;
		}
	}
	{
		NullCheck(V_3);
		int32_t L_40 = (V_3->___tapCount_10);
		if ((((int32_t)L_40) < ((int32_t)2)))
		{
			goto IL_014f;
		}
	}
	{
		V_2 = 1;
		__this->___canJump_18 = 0;
	}

IL_014f:
	{
		if (!V_2)
		{
			goto IL_0177;
		}
	}
	{
		CharacterController_t209 * L_41 = (__this->___character_15);
		NullCheck(L_41);
		Vector3_t73  L_42 = CharacterController_get_velocity_m802(L_41, /*hidden argument*/&CharacterController_get_velocity_m802_MethodInfo);
		__this->___velocity_17 = L_42;
		Vector3_t73 * L_43 = &(__this->___velocity_17);
		float L_44 = (__this->___jumpSpeed_8);
		NullCheck(L_43);
		L_43->___y_2 = L_44;
	}

IL_0177:
	{
		goto IL_01d1;
	}

IL_017c:
	{
		Vector3_t73 * L_45 = &(__this->___velocity_17);
		Vector3_t73 * L_46 = &(__this->___velocity_17);
		NullCheck(L_46);
		float L_47 = (L_46->___y_2);
		Vector3_t73  L_48 = Physics_get_gravity_m805(NULL /*static, unused*/, /*hidden argument*/&Physics_get_gravity_m805_MethodInfo);
		V_7 = L_48;
		NullCheck((&V_7));
		float L_49 = ((&V_7)->___y_2);
		float L_50 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		NullCheck(L_45);
		L_45->___y_2 = ((float)(L_47+((float)((float)L_49*(float)L_50))));
		NullCheck((&V_0));
		float L_51 = ((&V_0)->___x_1);
		float L_52 = (__this->___inAirMultiplier_9);
		NullCheck((&V_0));
		(&V_0)->___x_1 = ((float)((float)L_51*(float)L_52));
		NullCheck((&V_0));
		float L_53 = ((&V_0)->___z_3);
		float L_54 = (__this->___inAirMultiplier_9);
		NullCheck((&V_0));
		(&V_0)->___z_3 = ((float)((float)L_53*(float)L_54));
	}

IL_01d1:
	{
		Vector3_t73  L_55 = (__this->___velocity_17);
		Vector3_t73  L_56 = Vector3_op_Addition_m576(NULL /*static, unused*/, V_0, L_55, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_0 = L_56;
		Vector3_t73  L_57 = Physics_get_gravity_m805(NULL /*static, unused*/, /*hidden argument*/&Physics_get_gravity_m805_MethodInfo);
		Vector3_t73  L_58 = Vector3_op_Addition_m576(NULL /*static, unused*/, V_0, L_57, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_0 = L_58;
		float L_59 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		Vector3_t73  L_60 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_0, L_59, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_60;
		CharacterController_t209 * L_61 = (__this->___character_15);
		NullCheck(L_61);
		CharacterController_Move_m806(L_61, V_0, /*hidden argument*/&CharacterController_Move_m806_MethodInfo);
		CharacterController_t209 * L_62 = (__this->___character_15);
		NullCheck(L_62);
		bool L_63 = CharacterController_get_isGrounded_m804(L_62, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_63)
		{
			goto IL_021e;
		}
	}
	{
		Vector3_t73  L_64 = Vector3_get_zero_m807(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m807_MethodInfo);
		__this->___velocity_17 = L_64;
	}

IL_021e:
	{
		CharacterController_t209 * L_65 = (__this->___character_15);
		NullCheck(L_65);
		bool L_66 = CharacterController_get_isGrounded_m804(L_65, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_66)
		{
			goto IL_0380;
		}
	}
	{
		Vector2_t99  L_67 = Vector2_get_zero_m665(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m665_MethodInfo);
		V_4 = L_67;
		Joystick_t208 * L_68 = (__this->___rotateTouchPad_3);
		bool L_69 = Object_op_Implicit_m257(NULL /*static, unused*/, L_68, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_69)
		{
			goto IL_0257;
		}
	}
	{
		Joystick_t208 * L_70 = (__this->___rotateTouchPad_3);
		NullCheck(L_70);
		Vector2_t99  L_71 = (L_70->___position_9);
		V_4 = L_71;
		goto IL_0310;
	}

IL_0257:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		Vector3_t73  L_72 = Input_get_acceleration_m810(NULL /*static, unused*/, /*hidden argument*/&Input_get_acceleration_m810_MethodInfo);
		V_5 = L_72;
		NullCheck((&V_5));
		float L_73 = ((&V_5)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_74 = fabsf(L_73);
		V_6 = L_74;
		NullCheck((&V_5));
		float L_75 = ((&V_5)->___z_3);
		if ((((float)L_75) >= ((float)(((float)0)))))
		{
			goto IL_02d9;
		}
	}
	{
		NullCheck((&V_5));
		float L_76 = ((&V_5)->___x_1);
		if ((((float)L_76) >= ((float)(((float)0)))))
		{
			goto IL_02d9;
		}
	}
	{
		float L_77 = (__this->___tiltPositiveYAxis_11);
		if ((((float)V_6) < ((float)L_77)))
		{
			goto IL_02b4;
		}
	}
	{
		float L_78 = (__this->___tiltPositiveYAxis_11);
		float L_79 = (__this->___tiltPositiveYAxis_11);
		NullCheck((&V_4));
		(&V_4)->___y_2 = ((float)((float)((float)(V_6-L_78))/(float)((float)((((float)1))-L_79))));
		goto IL_02d9;
	}

IL_02b4:
	{
		float L_80 = (__this->___tiltNegativeYAxis_12);
		if ((((float)V_6) > ((float)L_80)))
		{
			goto IL_02d9;
		}
	}
	{
		float L_81 = (__this->___tiltNegativeYAxis_12);
		float L_82 = (__this->___tiltNegativeYAxis_12);
		NullCheck((&V_4));
		(&V_4)->___y_2 = ((float)((float)((-((float)(L_81-V_6))))/(float)L_82));
	}

IL_02d9:
	{
		NullCheck((&V_5));
		float L_83 = ((&V_5)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_84 = fabsf(L_83);
		float L_85 = (__this->___tiltXAxisMinimum_13);
		if ((((float)L_84) < ((float)L_85)))
		{
			goto IL_0310;
		}
	}
	{
		NullCheck((&V_5));
		float L_86 = ((&V_5)->___y_2);
		float L_87 = (__this->___tiltXAxisMinimum_13);
		float L_88 = (__this->___tiltXAxisMinimum_13);
		NullCheck((&V_4));
		(&V_4)->___x_1 = ((float)((float)((-((float)(L_86-L_87))))/(float)((float)((((float)1))-L_88))));
	}

IL_0310:
	{
		NullCheck((&V_4));
		float L_89 = ((&V_4)->___x_1);
		Vector2_t99 * L_90 = &(__this->___rotationSpeed_10);
		NullCheck(L_90);
		float L_91 = (L_90->___x_1);
		NullCheck((&V_4));
		(&V_4)->___x_1 = ((float)((float)L_89*(float)L_91));
		NullCheck((&V_4));
		float L_92 = ((&V_4)->___y_2);
		Vector2_t99 * L_93 = &(__this->___rotationSpeed_10);
		NullCheck(L_93);
		float L_94 = (L_93->___y_2);
		NullCheck((&V_4));
		(&V_4)->___y_2 = ((float)((float)L_92*(float)L_94));
		float L_95 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		Vector2_t99  L_96 = Vector2_op_Multiply_m808(NULL /*static, unused*/, V_4, L_95, /*hidden argument*/&Vector2_op_Multiply_m808_MethodInfo);
		V_4 = L_96;
		Transform_t74 * L_97 = (__this->___thisTransform_14);
		NullCheck((&V_4));
		float L_98 = ((&V_4)->___x_1);
		NullCheck(L_97);
		Transform_Rotate_m809(L_97, (((float)0)), L_98, (((float)0)), 0, /*hidden argument*/&Transform_Rotate_m809_MethodInfo);
		Transform_t74 * L_99 = (__this->___cameraPivot_4);
		NullCheck((&V_4));
		float L_100 = ((&V_4)->___y_2);
		NullCheck(L_99);
		Transform_Rotate_m623(L_99, ((-L_100)), (((float)0)), (((float)0)), /*hidden argument*/&Transform_Rotate_m623_MethodInfo);
	}

IL_0380:
	{
		return;
	}
}
// System.Void FirstPersonControl::Main()
extern MethodInfo FirstPersonControl_Main_m744_MethodInfo;
 void FirstPersonControl_Main_m744 (FirstPersonControl_t211 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition FirstPersonControl
extern Il2CppType Joystick_t208_0_0_6;
FieldInfo FirstPersonControl_t211____moveTouchPad_2_FieldInfo = 
{
	"moveTouchPad"/* name */
	, &Joystick_t208_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___moveTouchPad_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Joystick_t208_0_0_6;
FieldInfo FirstPersonControl_t211____rotateTouchPad_3_FieldInfo = 
{
	"rotateTouchPad"/* name */
	, &Joystick_t208_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___rotateTouchPad_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_6;
FieldInfo FirstPersonControl_t211____cameraPivot_4_FieldInfo = 
{
	"cameraPivot"/* name */
	, &Transform_t74_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___cameraPivot_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo FirstPersonControl_t211____forwardSpeed_5_FieldInfo = 
{
	"forwardSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___forwardSpeed_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo FirstPersonControl_t211____backwardSpeed_6_FieldInfo = 
{
	"backwardSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___backwardSpeed_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo FirstPersonControl_t211____sidestepSpeed_7_FieldInfo = 
{
	"sidestepSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___sidestepSpeed_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo FirstPersonControl_t211____jumpSpeed_8_FieldInfo = 
{
	"jumpSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___jumpSpeed_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo FirstPersonControl_t211____inAirMultiplier_9_FieldInfo = 
{
	"inAirMultiplier"/* name */
	, &Single_t170_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___inAirMultiplier_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo FirstPersonControl_t211____rotationSpeed_10_FieldInfo = 
{
	"rotationSpeed"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___rotationSpeed_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo FirstPersonControl_t211____tiltPositiveYAxis_11_FieldInfo = 
{
	"tiltPositiveYAxis"/* name */
	, &Single_t170_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___tiltPositiveYAxis_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo FirstPersonControl_t211____tiltNegativeYAxis_12_FieldInfo = 
{
	"tiltNegativeYAxis"/* name */
	, &Single_t170_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___tiltNegativeYAxis_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo FirstPersonControl_t211____tiltXAxisMinimum_13_FieldInfo = 
{
	"tiltXAxisMinimum"/* name */
	, &Single_t170_0_0_6/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___tiltXAxisMinimum_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_1;
FieldInfo FirstPersonControl_t211____thisTransform_14_FieldInfo = 
{
	"thisTransform"/* name */
	, &Transform_t74_0_0_1/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___thisTransform_14)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType CharacterController_t209_0_0_1;
FieldInfo FirstPersonControl_t211____character_15_FieldInfo = 
{
	"character"/* name */
	, &CharacterController_t209_0_0_1/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___character_15)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo FirstPersonControl_t211____cameraVelocity_16_FieldInfo = 
{
	"cameraVelocity"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___cameraVelocity_16)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo FirstPersonControl_t211____velocity_17_FieldInfo = 
{
	"velocity"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___velocity_17)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo FirstPersonControl_t211____canJump_18_FieldInfo = 
{
	"canJump"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* parent */
	, offsetof(FirstPersonControl_t211, ___canJump_18)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* FirstPersonControl_t211_FieldInfos[] =
{
	&FirstPersonControl_t211____moveTouchPad_2_FieldInfo,
	&FirstPersonControl_t211____rotateTouchPad_3_FieldInfo,
	&FirstPersonControl_t211____cameraPivot_4_FieldInfo,
	&FirstPersonControl_t211____forwardSpeed_5_FieldInfo,
	&FirstPersonControl_t211____backwardSpeed_6_FieldInfo,
	&FirstPersonControl_t211____sidestepSpeed_7_FieldInfo,
	&FirstPersonControl_t211____jumpSpeed_8_FieldInfo,
	&FirstPersonControl_t211____inAirMultiplier_9_FieldInfo,
	&FirstPersonControl_t211____rotationSpeed_10_FieldInfo,
	&FirstPersonControl_t211____tiltPositiveYAxis_11_FieldInfo,
	&FirstPersonControl_t211____tiltNegativeYAxis_12_FieldInfo,
	&FirstPersonControl_t211____tiltXAxisMinimum_13_FieldInfo,
	&FirstPersonControl_t211____thisTransform_14_FieldInfo,
	&FirstPersonControl_t211____character_15_FieldInfo,
	&FirstPersonControl_t211____cameraVelocity_16_FieldInfo,
	&FirstPersonControl_t211____velocity_17_FieldInfo,
	&FirstPersonControl_t211____canJump_18_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void FirstPersonControl::.ctor()
MethodInfo FirstPersonControl__ctor_m740_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FirstPersonControl__ctor_m740/* method */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void FirstPersonControl::Start()
MethodInfo FirstPersonControl_Start_m741_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&FirstPersonControl_Start_m741/* method */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void FirstPersonControl::OnEndGame()
MethodInfo FirstPersonControl_OnEndGame_m742_MethodInfo = 
{
	"OnEndGame"/* name */
	, (methodPointerType)&FirstPersonControl_OnEndGame_m742/* method */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void FirstPersonControl::Update()
MethodInfo FirstPersonControl_Update_m743_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&FirstPersonControl_Update_m743/* method */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void FirstPersonControl::Main()
MethodInfo FirstPersonControl_Main_m744_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&FirstPersonControl_Main_m744/* method */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* FirstPersonControl_t211_MethodInfos[] =
{
	&FirstPersonControl__ctor_m740_MethodInfo,
	&FirstPersonControl_Start_m741_MethodInfo,
	&FirstPersonControl_OnEndGame_m742_MethodInfo,
	&FirstPersonControl_Update_m743_MethodInfo,
	&FirstPersonControl_Main_m744_MethodInfo,
	NULL
};
static MethodInfo* FirstPersonControl_t211_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&FirstPersonControl_Start_m741_MethodInfo,
	&FirstPersonControl_OnEndGame_m742_MethodInfo,
	&FirstPersonControl_Update_m743_MethodInfo,
	&FirstPersonControl_Main_m744_MethodInfo,
};
void FirstPersonControl_t211_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t154 * tmp;
		tmp = (RequireComponent_t154 *)il2cpp_codegen_object_new (&RequireComponent_t154_il2cpp_TypeInfo);
		RequireComponent__ctor_m348(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&CharacterController_t209_il2cpp_TypeInfo)), &RequireComponent__ctor_m348_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache FirstPersonControl_t211__CustomAttributeCache = {
1,
NULL,
&FirstPersonControl_t211_CustomAttributesCacheGenerator
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType FirstPersonControl_t211_0_0_0;
extern Il2CppType FirstPersonControl_t211_1_0_0;
struct FirstPersonControl_t211;
extern CustomAttributesCache FirstPersonControl_t211__CustomAttributeCache;
TypeInfo FirstPersonControl_t211_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "FirstPersonControl"/* name */
	, ""/* namespaze */
	, FirstPersonControl_t211_MethodInfos/* methods */
	, NULL/* properties */
	, FirstPersonControl_t211_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, FirstPersonControl_t211_VTable/* vtable */
	, &FirstPersonControl_t211__CustomAttributeCache/* custom_attributes_cache */
	, &FirstPersonControl_t211_il2cpp_TypeInfo/* cast_class */
	, &FirstPersonControl_t211_0_0_0/* byval_arg */
	, &FirstPersonControl_t211_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FirstPersonControl_t211)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// FollowTransform
#include "AssemblyU2DUnityScript_FollowTransform.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo FollowTransform_t212_il2cpp_TypeInfo;
// FollowTransform
#include "AssemblyU2DUnityScript_FollowTransformMethodDeclarations.h"

extern MethodInfo Component_get_transform_m487_MethodInfo;
extern MethodInfo Transform_get_forward_m572_MethodInfo;


// System.Void FollowTransform::.ctor()
extern MethodInfo FollowTransform__ctor_m745_MethodInfo;
 void FollowTransform__ctor_m745 (FollowTransform_t212 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// System.Void FollowTransform::Start()
extern MethodInfo FollowTransform_Start_m746_MethodInfo;
 void FollowTransform_Start_m746 (FollowTransform_t212 * __this, MethodInfo* method){
	{
		Transform_t74 * L_0 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		__this->___thisTransform_4 = L_0;
		return;
	}
}
// System.Void FollowTransform::Update()
extern MethodInfo FollowTransform_Update_m747_MethodInfo;
 void FollowTransform_Update_m747 (FollowTransform_t212 * __this, MethodInfo* method){
	{
		Transform_t74 * L_0 = (__this->___thisTransform_4);
		Transform_t74 * L_1 = (__this->___targetTransform_2);
		NullCheck(L_1);
		Vector3_t73  L_2 = Transform_get_position_m538(L_1, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		NullCheck(L_0);
		Transform_set_position_m570(L_0, L_2, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
		bool L_3 = (__this->___faceForward_3);
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t74 * L_4 = (__this->___thisTransform_4);
		Transform_t74 * L_5 = (__this->___targetTransform_2);
		NullCheck(L_5);
		Vector3_t73  L_6 = Transform_get_forward_m572(L_5, /*hidden argument*/&Transform_get_forward_m572_MethodInfo);
		NullCheck(L_4);
		Transform_set_forward_m708(L_4, L_6, /*hidden argument*/&Transform_set_forward_m708_MethodInfo);
	}

IL_0037:
	{
		return;
	}
}
// System.Void FollowTransform::Main()
extern MethodInfo FollowTransform_Main_m748_MethodInfo;
 void FollowTransform_Main_m748 (FollowTransform_t212 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition FollowTransform
extern Il2CppType Transform_t74_0_0_6;
FieldInfo FollowTransform_t212____targetTransform_2_FieldInfo = 
{
	"targetTransform"/* name */
	, &Transform_t74_0_0_6/* type */
	, &FollowTransform_t212_il2cpp_TypeInfo/* parent */
	, offsetof(FollowTransform_t212, ___targetTransform_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo FollowTransform_t212____faceForward_3_FieldInfo = 
{
	"faceForward"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &FollowTransform_t212_il2cpp_TypeInfo/* parent */
	, offsetof(FollowTransform_t212, ___faceForward_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_1;
FieldInfo FollowTransform_t212____thisTransform_4_FieldInfo = 
{
	"thisTransform"/* name */
	, &Transform_t74_0_0_1/* type */
	, &FollowTransform_t212_il2cpp_TypeInfo/* parent */
	, offsetof(FollowTransform_t212, ___thisTransform_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* FollowTransform_t212_FieldInfos[] =
{
	&FollowTransform_t212____targetTransform_2_FieldInfo,
	&FollowTransform_t212____faceForward_3_FieldInfo,
	&FollowTransform_t212____thisTransform_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void FollowTransform::.ctor()
MethodInfo FollowTransform__ctor_m745_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FollowTransform__ctor_m745/* method */
	, &FollowTransform_t212_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void FollowTransform::Start()
MethodInfo FollowTransform_Start_m746_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&FollowTransform_Start_m746/* method */
	, &FollowTransform_t212_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void FollowTransform::Update()
MethodInfo FollowTransform_Update_m747_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&FollowTransform_Update_m747/* method */
	, &FollowTransform_t212_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void FollowTransform::Main()
MethodInfo FollowTransform_Main_m748_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&FollowTransform_Main_m748/* method */
	, &FollowTransform_t212_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* FollowTransform_t212_MethodInfos[] =
{
	&FollowTransform__ctor_m745_MethodInfo,
	&FollowTransform_Start_m746_MethodInfo,
	&FollowTransform_Update_m747_MethodInfo,
	&FollowTransform_Main_m748_MethodInfo,
	NULL
};
static MethodInfo* FollowTransform_t212_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&FollowTransform_Start_m746_MethodInfo,
	&FollowTransform_Update_m747_MethodInfo,
	&FollowTransform_Main_m748_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType FollowTransform_t212_0_0_0;
extern Il2CppType FollowTransform_t212_1_0_0;
struct FollowTransform_t212;
TypeInfo FollowTransform_t212_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "FollowTransform"/* name */
	, ""/* namespaze */
	, FollowTransform_t212_MethodInfos/* methods */
	, NULL/* properties */
	, FollowTransform_t212_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &FollowTransform_t212_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, FollowTransform_t212_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &FollowTransform_t212_il2cpp_TypeInfo/* cast_class */
	, &FollowTransform_t212_0_0_0/* byval_arg */
	, &FollowTransform_t212_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FollowTransform_t212)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Boundary
#include "AssemblyU2DUnityScript_Boundary.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Boundary_t213_il2cpp_TypeInfo;
// Boundary
#include "AssemblyU2DUnityScript_BoundaryMethodDeclarations.h"

// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern MethodInfo Object__ctor_m312_MethodInfo;


// System.Void Boundary::.ctor()
extern MethodInfo Boundary__ctor_m749_MethodInfo;
 void Boundary__ctor_m749 (Boundary_t213 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		Vector2_t99  L_0 = Vector2_get_zero_m665(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m665_MethodInfo);
		__this->___min_0 = L_0;
		Vector2_t99  L_1 = Vector2_get_zero_m665(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m665_MethodInfo);
		__this->___max_1 = L_1;
		return;
	}
}
// Metadata Definition Boundary
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo Boundary_t213____min_0_FieldInfo = 
{
	"min"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &Boundary_t213_il2cpp_TypeInfo/* parent */
	, offsetof(Boundary_t213, ___min_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo Boundary_t213____max_1_FieldInfo = 
{
	"max"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &Boundary_t213_il2cpp_TypeInfo/* parent */
	, offsetof(Boundary_t213, ___max_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Boundary_t213_FieldInfos[] =
{
	&Boundary_t213____min_0_FieldInfo,
	&Boundary_t213____max_1_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Boundary::.ctor()
MethodInfo Boundary__ctor_m749_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Boundary__ctor_m749/* method */
	, &Boundary_t213_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Boundary_t213_MethodInfos[] =
{
	&Boundary__ctor_m749_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
static MethodInfo* Boundary_t213_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType Boundary_t213_0_0_0;
extern Il2CppType Boundary_t213_1_0_0;
extern TypeInfo Object_t_il2cpp_TypeInfo;
struct Boundary_t213;
TypeInfo Boundary_t213_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "Boundary"/* name */
	, ""/* namespaze */
	, Boundary_t213_MethodInfos/* methods */
	, NULL/* properties */
	, Boundary_t213_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Boundary_t213_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Boundary_t213_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Boundary_t213_il2cpp_TypeInfo/* cast_class */
	, &Boundary_t213_0_0_0/* byval_arg */
	, &Boundary_t213_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Boundary_t213)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
#include "Assembly-UnityScript_ArrayTypes.h"
extern TypeInfo GUITexture_t100_il2cpp_TypeInfo;
extern TypeInfo Color_t66_il2cpp_TypeInfo;
extern TypeInfo JoystickU5BU5D_t214_il2cpp_TypeInfo;
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
// UnityEngine.GUITexture
#include "UnityEngine_UnityEngine_GUITextureMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern Il2CppType GUITexture_t100_0_0_0;
extern Il2CppType Joystick_t208_0_0_0;
extern MethodInfo GUITexture_get_pixelInset_m674_MethodInfo;
extern MethodInfo Rect_get_x_m675_MethodInfo;
extern MethodInfo Screen_get_width_m260_MethodInfo;
extern MethodInfo Rect_set_x_m676_MethodInfo;
extern MethodInfo Rect_get_y_m677_MethodInfo;
extern MethodInfo Screen_get_height_m261_MethodInfo;
extern MethodInfo Rect_set_y_m678_MethodInfo;
extern MethodInfo GUITexture_get_texture_m667_MethodInfo;
extern MethodInfo Rect_get_width_m679_MethodInfo;
extern MethodInfo Rect_get_height_m680_MethodInfo;
extern MethodInfo Component_get_gameObject_m419_MethodInfo;
extern MethodInfo GameObject_set_active_m811_MethodInfo;
extern MethodInfo GUITexture_set_pixelInset_m681_MethodInfo;
extern MethodInfo GUITexture_get_color_m682_MethodInfo;
extern MethodInfo GUITexture_set_color_m684_MethodInfo;
extern MethodInfo Joystick_ResetJoystick_m754_MethodInfo;
extern MethodInfo Object_FindObjectsOfType_m812_MethodInfo;
extern MethodInfo Input_GetTouch_m686_MethodInfo;
extern MethodInfo Vector2_op_Subtraction_m688_MethodInfo;
extern MethodInfo Rect_Contains_m689_MethodInfo;
extern MethodInfo Touch_get_fingerId_m692_MethodInfo;
extern MethodInfo Time_get_time_m813_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Object_op_Inequality_m489_MethodInfo;
extern MethodInfo Joystick_LatchedFinger_m756_MethodInfo;
extern MethodInfo Touch_get_tapCount_m696_MethodInfo;
extern MethodInfo Mathf_Clamp_m697_MethodInfo;
extern MethodInfo Mathf_Sign_m700_MethodInfo;


// System.Void Joystick::.ctor()
extern MethodInfo Joystick__ctor_m750_MethodInfo;
 void Joystick__ctor_m750 (Joystick_t208 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		Vector2_t99  L_0 = Vector2_get_zero_m665(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m665_MethodInfo);
		__this->___deadZone_7 = L_0;
		__this->___lastFingerId_11 = (-1);
		__this->___firstDeltaTime_15 = (0.5f);
		Boundary_t213 * L_1 = (Boundary_t213 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Boundary_t213_il2cpp_TypeInfo));
		Boundary__ctor_m749(L_1, /*hidden argument*/&Boundary__ctor_m749_MethodInfo);
		__this->___guiBoundary_18 = L_1;
		return;
	}
}
// System.Void Joystick::.cctor()
extern MethodInfo Joystick__cctor_m751_MethodInfo;
 void Joystick__cctor_m751 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		((Joystick_t208_StaticFields*)InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo)->static_fields)->___tapTimeDelta_4 = (0.3f);
		return;
	}
}
// System.Void Joystick::Start()
extern MethodInfo Joystick_Start_m752_MethodInfo;
 void Joystick_Start_m752 (Joystick_t208 * __this, MethodInfo* method){
	float V_0 = 0.0f;
	Vector3_t73  V_1 = {0};
	float V_2 = 0.0f;
	Vector3_t73  V_3 = {0};
	Vector3_t73  V_4 = {0};
	Vector3_t73  V_5 = {0};
	float V_6 = 0.0f;
	Vector3_t73  V_7 = {0};
	float V_8 = 0.0f;
	Vector3_t73  V_9 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&GUITexture_t100_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Component_t128 * L_1 = Component_GetComponent_m800(__this, L_0, /*hidden argument*/&Component_GetComponent_m800_MethodInfo);
		__this->___gui_16 = ((GUITexture_t100 *)Castclass(L_1, InitializedTypeInfo(&GUITexture_t100_il2cpp_TypeInfo)));
		GUITexture_t100 * L_2 = (__this->___gui_16);
		NullCheck(L_2);
		Rect_t103  L_3 = GUITexture_get_pixelInset_m674(L_2, /*hidden argument*/&GUITexture_get_pixelInset_m674_MethodInfo);
		__this->___defaultRect_17 = L_3;
		Rect_t103 * L_4 = &(__this->___defaultRect_17);
		Rect_t103 * L_5 = &(__this->___defaultRect_17);
		float L_6 = Rect_get_x_m675(L_5, /*hidden argument*/&Rect_get_x_m675_MethodInfo);
		Transform_t74 * L_7 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_7);
		Vector3_t73  L_8 = Transform_get_position_m538(L_7, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		V_4 = L_8;
		NullCheck((&V_4));
		float L_9 = ((&V_4)->___x_1);
		int32_t L_10 = Screen_get_width_m260(NULL /*static, unused*/, /*hidden argument*/&Screen_get_width_m260_MethodInfo);
		Rect_set_x_m676(L_4, ((float)(L_6+((float)((float)L_9*(float)(((float)L_10)))))), /*hidden argument*/&Rect_set_x_m676_MethodInfo);
		Rect_t103 * L_11 = &(__this->___defaultRect_17);
		Rect_t103 * L_12 = &(__this->___defaultRect_17);
		float L_13 = Rect_get_y_m677(L_12, /*hidden argument*/&Rect_get_y_m677_MethodInfo);
		Transform_t74 * L_14 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_14);
		Vector3_t73  L_15 = Transform_get_position_m538(L_14, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		V_5 = L_15;
		NullCheck((&V_5));
		float L_16 = ((&V_5)->___y_2);
		int32_t L_17 = Screen_get_height_m261(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m261_MethodInfo);
		Rect_set_y_m678(L_11, ((float)(L_13+((float)((float)L_16*(float)(((float)L_17)))))), /*hidden argument*/&Rect_set_y_m678_MethodInfo);
		float L_18 = (((float)0));
		V_0 = L_18;
		Transform_t74 * L_19 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_19);
		Vector3_t73  L_20 = Transform_get_position_m538(L_19, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		Vector3_t73  L_21 = L_20;
		V_1 = L_21;
		float L_22 = V_0;
		V_6 = L_22;
		NullCheck((&V_1));
		(&V_1)->___x_1 = L_22;
		Transform_t74 * L_23 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		Vector3_t73  L_24 = V_1;
		V_7 = L_24;
		NullCheck(L_23);
		Transform_set_position_m570(L_23, L_24, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
		float L_25 = (((float)0));
		V_2 = L_25;
		Transform_t74 * L_26 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_26);
		Vector3_t73  L_27 = Transform_get_position_m538(L_26, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		Vector3_t73  L_28 = L_27;
		V_3 = L_28;
		float L_29 = V_2;
		V_8 = L_29;
		NullCheck((&V_3));
		(&V_3)->___y_2 = L_29;
		Transform_t74 * L_30 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		Vector3_t73  L_31 = V_3;
		V_9 = L_31;
		NullCheck(L_30);
		Transform_set_position_m570(L_30, L_31, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
		bool L_32 = (__this->___touchPad_5);
		if (!L_32)
		{
			goto IL_0127;
		}
	}
	{
		GUITexture_t100 * L_33 = (__this->___gui_16);
		NullCheck(L_33);
		Texture_t107 * L_34 = GUITexture_get_texture_m667(L_33, /*hidden argument*/&GUITexture_get_texture_m667_MethodInfo);
		bool L_35 = Object_op_Implicit_m257(NULL /*static, unused*/, L_34, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_35)
		{
			goto IL_0122;
		}
	}
	{
		Rect_t103  L_36 = (__this->___defaultRect_17);
		__this->___touchZone_6 = L_36;
	}

IL_0122:
	{
		goto IL_023f;
	}

IL_0127:
	{
		Vector2_t99 * L_37 = &(__this->___guiTouchOffset_19);
		Rect_t103 * L_38 = &(__this->___defaultRect_17);
		float L_39 = Rect_get_width_m679(L_38, /*hidden argument*/&Rect_get_width_m679_MethodInfo);
		NullCheck(L_37);
		L_37->___x_1 = ((float)((float)L_39*(float)(0.5f)));
		Vector2_t99 * L_40 = &(__this->___guiTouchOffset_19);
		Rect_t103 * L_41 = &(__this->___defaultRect_17);
		float L_42 = Rect_get_height_m680(L_41, /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		NullCheck(L_40);
		L_40->___y_2 = ((float)((float)L_42*(float)(0.5f)));
		Vector2_t99 * L_43 = &(__this->___guiCenter_20);
		Rect_t103 * L_44 = &(__this->___defaultRect_17);
		float L_45 = Rect_get_x_m675(L_44, /*hidden argument*/&Rect_get_x_m675_MethodInfo);
		Vector2_t99 * L_46 = &(__this->___guiTouchOffset_19);
		NullCheck(L_46);
		float L_47 = (L_46->___x_1);
		NullCheck(L_43);
		L_43->___x_1 = ((float)(L_45+L_47));
		Vector2_t99 * L_48 = &(__this->___guiCenter_20);
		Rect_t103 * L_49 = &(__this->___defaultRect_17);
		float L_50 = Rect_get_y_m677(L_49, /*hidden argument*/&Rect_get_y_m677_MethodInfo);
		Vector2_t99 * L_51 = &(__this->___guiTouchOffset_19);
		NullCheck(L_51);
		float L_52 = (L_51->___y_2);
		NullCheck(L_48);
		L_48->___y_2 = ((float)(L_50+L_52));
		Boundary_t213 * L_53 = (__this->___guiBoundary_18);
		NullCheck(L_53);
		Vector2_t99 * L_54 = &(L_53->___min_0);
		Rect_t103 * L_55 = &(__this->___defaultRect_17);
		float L_56 = Rect_get_x_m675(L_55, /*hidden argument*/&Rect_get_x_m675_MethodInfo);
		Vector2_t99 * L_57 = &(__this->___guiTouchOffset_19);
		NullCheck(L_57);
		float L_58 = (L_57->___x_1);
		NullCheck(L_54);
		L_54->___x_1 = ((float)(L_56-L_58));
		Boundary_t213 * L_59 = (__this->___guiBoundary_18);
		NullCheck(L_59);
		Vector2_t99 * L_60 = &(L_59->___max_1);
		Rect_t103 * L_61 = &(__this->___defaultRect_17);
		float L_62 = Rect_get_x_m675(L_61, /*hidden argument*/&Rect_get_x_m675_MethodInfo);
		Vector2_t99 * L_63 = &(__this->___guiTouchOffset_19);
		NullCheck(L_63);
		float L_64 = (L_63->___x_1);
		NullCheck(L_60);
		L_60->___x_1 = ((float)(L_62+L_64));
		Boundary_t213 * L_65 = (__this->___guiBoundary_18);
		NullCheck(L_65);
		Vector2_t99 * L_66 = &(L_65->___min_0);
		Rect_t103 * L_67 = &(__this->___defaultRect_17);
		float L_68 = Rect_get_y_m677(L_67, /*hidden argument*/&Rect_get_y_m677_MethodInfo);
		Vector2_t99 * L_69 = &(__this->___guiTouchOffset_19);
		NullCheck(L_69);
		float L_70 = (L_69->___y_2);
		NullCheck(L_66);
		L_66->___y_2 = ((float)(L_68-L_70));
		Boundary_t213 * L_71 = (__this->___guiBoundary_18);
		NullCheck(L_71);
		Vector2_t99 * L_72 = &(L_71->___max_1);
		Rect_t103 * L_73 = &(__this->___defaultRect_17);
		float L_74 = Rect_get_y_m677(L_73, /*hidden argument*/&Rect_get_y_m677_MethodInfo);
		Vector2_t99 * L_75 = &(__this->___guiTouchOffset_19);
		NullCheck(L_75);
		float L_76 = (L_75->___y_2);
		NullCheck(L_72);
		L_72->___y_2 = ((float)(L_74+L_76));
	}

IL_023f:
	{
		return;
	}
}
// System.Void Joystick::Disable()
 void Joystick_Disable_m753 (Joystick_t208 * __this, MethodInfo* method){
	{
		GameObject_t29 * L_0 = Component_get_gameObject_m419(__this, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		NullCheck(L_0);
		GameObject_set_active_m811(L_0, 0, /*hidden argument*/&GameObject_set_active_m811_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo));
		((Joystick_t208_StaticFields*)InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo)->static_fields)->___enumeratedJoysticks_3 = 0;
		return;
	}
}
// System.Void Joystick::ResetJoystick()
 void Joystick_ResetJoystick_m754 (Joystick_t208 * __this, MethodInfo* method){
	Vector2_t99  V_0 = {0};
	float V_1 = 0.0f;
	Color_t66  V_2 = {0};
	float V_3 = 0.0f;
	Color_t66  V_4 = {0};
	{
		GUITexture_t100 * L_0 = (__this->___gui_16);
		Rect_t103  L_1 = (__this->___defaultRect_17);
		NullCheck(L_0);
		GUITexture_set_pixelInset_m681(L_0, L_1, /*hidden argument*/&GUITexture_set_pixelInset_m681_MethodInfo);
		__this->___lastFingerId_11 = (-1);
		Vector2_t99  L_2 = Vector2_get_zero_m665(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m665_MethodInfo);
		__this->___position_9 = L_2;
		Vector2_t99  L_3 = Vector2_get_zero_m665(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m665_MethodInfo);
		V_0 = L_3;
		bool L_4 = (__this->___touchPad_5);
		if (!L_4)
		{
			goto IL_0068;
		}
	}
	{
		float L_5 = (0.025f);
		V_1 = L_5;
		GUITexture_t100 * L_6 = (__this->___gui_16);
		NullCheck(L_6);
		Color_t66  L_7 = GUITexture_get_color_m682(L_6, /*hidden argument*/&GUITexture_get_color_m682_MethodInfo);
		Color_t66  L_8 = L_7;
		V_2 = L_8;
		float L_9 = V_1;
		V_3 = L_9;
		NullCheck((&V_2));
		(&V_2)->___a_3 = L_9;
		GUITexture_t100 * L_10 = (__this->___gui_16);
		Color_t66  L_11 = V_2;
		V_4 = L_11;
		NullCheck(L_10);
		GUITexture_set_color_m684(L_10, L_11, /*hidden argument*/&GUITexture_set_color_m684_MethodInfo);
	}

IL_0068:
	{
		return;
	}
}
// System.Boolean Joystick::IsFingerDown()
 bool Joystick_IsFingerDown_m755 (Joystick_t208 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___lastFingerId_11);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Joystick::LatchedFinger(System.Int32)
 void Joystick_LatchedFinger_m756 (Joystick_t208 * __this, int32_t ___fingerId, MethodInfo* method){
	{
		int32_t L_0 = (__this->___lastFingerId_11);
		if ((((uint32_t)L_0) != ((uint32_t)___fingerId)))
		{
			goto IL_0012;
		}
	}
	{
		VirtActionInvoker0::Invoke(&Joystick_ResetJoystick_m754_MethodInfo, __this);
	}

IL_0012:
	{
		return;
	}
}
// System.Void Joystick::Update()
extern MethodInfo Joystick_Update_m757_MethodInfo;
 void Joystick_Update_m757 (Joystick_t208 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Touch_t201  V_2 = {0};
	Vector2_t99  V_3 = {0};
	bool V_4 = false;
	Joystick_t208 * V_5 = {0};
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	JoystickU5BU5D_t214* V_9 = {0};
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	Color_t66  V_12 = {0};
	float V_13 = 0.0f;
	Rect_t103  V_14 = {0};
	float V_15 = 0.0f;
	Rect_t103  V_16 = {0};
	float V_17 = 0.0f;
	Color_t66  V_18 = {0};
	Vector2_t99  V_19 = {0};
	Vector2_t99  V_20 = {0};
	float V_21 = 0.0f;
	Rect_t103  V_22 = {0};
	float V_23 = 0.0f;
	Rect_t103  V_24 = {0};
	Rect_t103  V_25 = {0};
	Rect_t103  V_26 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo));
		if ((((Joystick_t208_StaticFields*)InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo)->static_fields)->___enumeratedJoysticks_3))
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Joystick_t208_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		ObjectU5BU5D_t227* L_1 = Object_FindObjectsOfType_m812(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectsOfType_m812_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo));
		((Joystick_t208_StaticFields*)InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo)->static_fields)->___joysticks_2 = ((JoystickU5BU5D_t214*)Castclass(L_1, InitializedTypeInfo(&JoystickU5BU5D_t214_il2cpp_TypeInfo)));
		((Joystick_t208_StaticFields*)InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo)->static_fields)->___enumeratedJoysticks_3 = 1;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		int32_t L_2 = Input_get_touchCount_m685(NULL /*static, unused*/, /*hidden argument*/&Input_get_touchCount_m685_MethodInfo);
		V_0 = L_2;
		float L_3 = (__this->___tapTimeWindow_12);
		if ((((float)L_3) <= ((float)(((float)0)))))
		{
			goto IL_0053;
		}
	}
	{
		float L_4 = (__this->___tapTimeWindow_12);
		float L_5 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		__this->___tapTimeWindow_12 = ((float)(L_4-L_5));
		goto IL_005a;
	}

IL_0053:
	{
		__this->___tapCount_10 = 0;
	}

IL_005a:
	{
		if (V_0)
		{
			goto IL_006b;
		}
	}
	{
		VirtActionInvoker0::Invoke(&Joystick_ResetJoystick_m754_MethodInfo, __this);
		goto IL_039a;
	}

IL_006b:
	{
		V_1 = 0;
		goto IL_0393;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		Touch_t201  L_6 = Input_GetTouch_m686(NULL /*static, unused*/, V_1, /*hidden argument*/&Input_GetTouch_m686_MethodInfo);
		V_2 = L_6;
		Vector2_t99  L_7 = Touch_get_position_m687((&V_2), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_8 = (__this->___guiTouchOffset_19);
		Vector2_t99  L_9 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		V_3 = L_9;
		V_4 = 0;
		bool L_10 = (__this->___touchPad_5);
		if (!L_10)
		{
			goto IL_00b9;
		}
	}
	{
		Rect_t103 * L_11 = &(__this->___touchZone_6);
		Vector2_t99  L_12 = Touch_get_position_m687((&V_2), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		bool L_13 = Rect_Contains_m689(L_11, L_12, /*hidden argument*/&Rect_Contains_m689_MethodInfo);
		if (!L_13)
		{
			goto IL_00b4;
		}
	}
	{
		V_4 = 1;
	}

IL_00b4:
	{
		goto IL_00d8;
	}

IL_00b9:
	{
		GUITexture_t100 * L_14 = (__this->___gui_16);
		Vector2_t99  L_15 = Touch_get_position_m687((&V_2), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector3_t73  L_16 = Vector2_op_Implicit_m690(NULL /*static, unused*/, L_15, /*hidden argument*/&Vector2_op_Implicit_m690_MethodInfo);
		NullCheck(L_14);
		bool L_17 = GUIElement_HitTest_m691(L_14, L_16, /*hidden argument*/&GUIElement_HitTest_m691_MethodInfo);
		if (!L_17)
		{
			goto IL_00d8;
		}
	}
	{
		V_4 = 1;
	}

IL_00d8:
	{
		if (!V_4)
		{
			goto IL_01ee;
		}
	}
	{
		int32_t L_18 = (__this->___lastFingerId_11);
		if ((((int32_t)L_18) == ((int32_t)(-1))))
		{
			goto IL_00fd;
		}
	}
	{
		int32_t L_19 = (__this->___lastFingerId_11);
		int32_t L_20 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		if ((((int32_t)L_19) == ((int32_t)L_20)))
		{
			goto IL_01ee;
		}
	}

IL_00fd:
	{
		bool L_21 = (__this->___touchPad_5);
		if (!L_21)
		{
			goto IL_0167;
		}
	}
	{
		float L_22 = (0.15f);
		V_11 = L_22;
		GUITexture_t100 * L_23 = (__this->___gui_16);
		NullCheck(L_23);
		Color_t66  L_24 = GUITexture_get_color_m682(L_23, /*hidden argument*/&GUITexture_get_color_m682_MethodInfo);
		Color_t66  L_25 = L_24;
		V_12 = L_25;
		float L_26 = V_11;
		V_17 = L_26;
		NullCheck((&V_12));
		(&V_12)->___a_3 = L_26;
		GUITexture_t100 * L_27 = (__this->___gui_16);
		Color_t66  L_28 = V_12;
		V_18 = L_28;
		NullCheck(L_27);
		GUITexture_set_color_m684(L_27, L_28, /*hidden argument*/&GUITexture_set_color_m684_MethodInfo);
		int32_t L_29 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		__this->___lastFingerId_11 = L_29;
		Vector2_t99  L_30 = Touch_get_position_m687((&V_2), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		__this->___fingerDownPos_13 = L_30;
		float L_31 = Time_get_time_m813(NULL /*static, unused*/, /*hidden argument*/&Time_get_time_m813_MethodInfo);
		__this->___fingerDownTime_14 = L_31;
	}

IL_0167:
	{
		int32_t L_32 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		__this->___lastFingerId_11 = L_32;
		float L_33 = (__this->___tapTimeWindow_12);
		if ((((float)L_33) <= ((float)(((float)0)))))
		{
			goto IL_0194;
		}
	}
	{
		int32_t L_34 = (__this->___tapCount_10);
		__this->___tapCount_10 = ((int32_t)(L_34+1));
		goto IL_01a6;
	}

IL_0194:
	{
		__this->___tapCount_10 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo));
		__this->___tapTimeWindow_12 = (((Joystick_t208_StaticFields*)InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo)->static_fields)->___tapTimeDelta_4);
	}

IL_01a6:
	{
		V_8 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo));
		V_9 = (((Joystick_t208_StaticFields*)InitializedTypeInfo(&Joystick_t208_il2cpp_TypeInfo)->static_fields)->___joysticks_2);
		NullCheck(V_9);
		int32_t L_35 = Array_get_Length_m814(V_9, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_10 = L_35;
		goto IL_01e5;
	}

IL_01be:
	{
		NullCheck(V_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_9, V_8);
		int32_t L_36 = V_8;
		bool L_37 = Object_op_Inequality_m489(NULL /*static, unused*/, (*(Joystick_t208 **)(Joystick_t208 **)SZArrayLdElema(V_9, L_36)), __this, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_37)
		{
			goto IL_01df;
		}
	}
	{
		NullCheck(V_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_9, V_8);
		int32_t L_38 = V_8;
		int32_t L_39 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		NullCheck((*(Joystick_t208 **)(Joystick_t208 **)SZArrayLdElema(V_9, L_38)));
		VirtActionInvoker1< int32_t >::Invoke(&Joystick_LatchedFinger_m756_MethodInfo, (*(Joystick_t208 **)(Joystick_t208 **)SZArrayLdElema(V_9, L_38)), L_39);
	}

IL_01df:
	{
		V_8 = ((int32_t)(V_8+1));
	}

IL_01e5:
	{
		if ((((int32_t)V_8) < ((int32_t)V_10)))
		{
			goto IL_01be;
		}
	}

IL_01ee:
	{
		int32_t L_40 = (__this->___lastFingerId_11);
		int32_t L_41 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		if ((((uint32_t)L_40) != ((uint32_t)L_41)))
		{
			goto IL_038f;
		}
	}
	{
		int32_t L_42 = Touch_get_tapCount_m696((&V_2), /*hidden argument*/&Touch_get_tapCount_m696_MethodInfo);
		int32_t L_43 = (__this->___tapCount_10);
		if ((((int32_t)L_42) <= ((int32_t)L_43)))
		{
			goto IL_021f;
		}
	}
	{
		int32_t L_44 = Touch_get_tapCount_m696((&V_2), /*hidden argument*/&Touch_get_tapCount_m696_MethodInfo);
		__this->___tapCount_10 = L_44;
	}

IL_021f:
	{
		bool L_45 = (__this->___touchPad_5);
		if (!L_45)
		{
			goto IL_02ad;
		}
	}
	{
		Vector2_t99 * L_46 = &(__this->___position_9);
		Vector2_t99  L_47 = Touch_get_position_m687((&V_2), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		V_19 = L_47;
		NullCheck((&V_19));
		float L_48 = ((&V_19)->___x_1);
		Vector2_t99 * L_49 = &(__this->___fingerDownPos_13);
		NullCheck(L_49);
		float L_50 = (L_49->___x_1);
		Rect_t103 * L_51 = &(__this->___touchZone_6);
		float L_52 = Rect_get_width_m679(L_51, /*hidden argument*/&Rect_get_width_m679_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_53 = Mathf_Clamp_m697(NULL /*static, unused*/, ((float)((float)((float)(L_48-L_50))/(float)((float)((float)L_52/(float)(((float)2)))))), (((float)(-1))), (((float)1)), /*hidden argument*/&Mathf_Clamp_m697_MethodInfo);
		NullCheck(L_46);
		L_46->___x_1 = L_53;
		Vector2_t99 * L_54 = &(__this->___position_9);
		Vector2_t99  L_55 = Touch_get_position_m687((&V_2), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		V_20 = L_55;
		NullCheck((&V_20));
		float L_56 = ((&V_20)->___y_2);
		Vector2_t99 * L_57 = &(__this->___fingerDownPos_13);
		NullCheck(L_57);
		float L_58 = (L_57->___y_2);
		Rect_t103 * L_59 = &(__this->___touchZone_6);
		float L_60 = Rect_get_height_m680(L_59, /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		float L_61 = Mathf_Clamp_m697(NULL /*static, unused*/, ((float)((float)((float)(L_56-L_58))/(float)((float)((float)L_60/(float)(((float)2)))))), (((float)(-1))), (((float)1)), /*hidden argument*/&Mathf_Clamp_m697_MethodInfo);
		NullCheck(L_54);
		L_54->___y_2 = L_61;
		goto IL_036f;
	}

IL_02ad:
	{
		NullCheck((&V_3));
		float L_62 = ((&V_3)->___x_1);
		Boundary_t213 * L_63 = (__this->___guiBoundary_18);
		NullCheck(L_63);
		Vector2_t99 * L_64 = &(L_63->___min_0);
		NullCheck(L_64);
		float L_65 = (L_64->___x_1);
		Boundary_t213 * L_66 = (__this->___guiBoundary_18);
		NullCheck(L_66);
		Vector2_t99 * L_67 = &(L_66->___max_1);
		NullCheck(L_67);
		float L_68 = (L_67->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_69 = Mathf_Clamp_m697(NULL /*static, unused*/, L_62, L_65, L_68, /*hidden argument*/&Mathf_Clamp_m697_MethodInfo);
		float L_70 = L_69;
		V_13 = L_70;
		GUITexture_t100 * L_71 = (__this->___gui_16);
		NullCheck(L_71);
		Rect_t103  L_72 = GUITexture_get_pixelInset_m674(L_71, /*hidden argument*/&GUITexture_get_pixelInset_m674_MethodInfo);
		Rect_t103  L_73 = L_72;
		V_14 = L_73;
		float L_74 = V_13;
		V_21 = L_74;
		Rect_set_x_m676((&V_14), L_74, /*hidden argument*/&Rect_set_x_m676_MethodInfo);
		GUITexture_t100 * L_75 = (__this->___gui_16);
		Rect_t103  L_76 = V_14;
		V_22 = L_76;
		NullCheck(L_75);
		GUITexture_set_pixelInset_m681(L_75, L_76, /*hidden argument*/&GUITexture_set_pixelInset_m681_MethodInfo);
		NullCheck((&V_3));
		float L_77 = ((&V_3)->___y_2);
		Boundary_t213 * L_78 = (__this->___guiBoundary_18);
		NullCheck(L_78);
		Vector2_t99 * L_79 = &(L_78->___min_0);
		NullCheck(L_79);
		float L_80 = (L_79->___y_2);
		Boundary_t213 * L_81 = (__this->___guiBoundary_18);
		NullCheck(L_81);
		Vector2_t99 * L_82 = &(L_81->___max_1);
		NullCheck(L_82);
		float L_83 = (L_82->___y_2);
		float L_84 = Mathf_Clamp_m697(NULL /*static, unused*/, L_77, L_80, L_83, /*hidden argument*/&Mathf_Clamp_m697_MethodInfo);
		float L_85 = L_84;
		V_15 = L_85;
		GUITexture_t100 * L_86 = (__this->___gui_16);
		NullCheck(L_86);
		Rect_t103  L_87 = GUITexture_get_pixelInset_m674(L_86, /*hidden argument*/&GUITexture_get_pixelInset_m674_MethodInfo);
		Rect_t103  L_88 = L_87;
		V_16 = L_88;
		float L_89 = V_15;
		V_23 = L_89;
		Rect_set_y_m678((&V_16), L_89, /*hidden argument*/&Rect_set_y_m678_MethodInfo);
		GUITexture_t100 * L_90 = (__this->___gui_16);
		Rect_t103  L_91 = V_16;
		V_24 = L_91;
		NullCheck(L_90);
		GUITexture_set_pixelInset_m681(L_90, L_91, /*hidden argument*/&GUITexture_set_pixelInset_m681_MethodInfo);
	}

IL_036f:
	{
		int32_t L_92 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((int32_t)L_92) == ((int32_t)3)))
		{
			goto IL_0389;
		}
	}
	{
		int32_t L_93 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((uint32_t)L_93) != ((uint32_t)4)))
		{
			goto IL_038f;
		}
	}

IL_0389:
	{
		VirtActionInvoker0::Invoke(&Joystick_ResetJoystick_m754_MethodInfo, __this);
	}

IL_038f:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0393:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0072;
		}
	}

IL_039a:
	{
		bool L_94 = (__this->___touchPad_5);
		if (L_94)
		{
			goto IL_042b;
		}
	}
	{
		Vector2_t99 * L_95 = &(__this->___position_9);
		GUITexture_t100 * L_96 = (__this->___gui_16);
		NullCheck(L_96);
		Rect_t103  L_97 = GUITexture_get_pixelInset_m674(L_96, /*hidden argument*/&GUITexture_get_pixelInset_m674_MethodInfo);
		V_25 = L_97;
		float L_98 = Rect_get_x_m675((&V_25), /*hidden argument*/&Rect_get_x_m675_MethodInfo);
		Vector2_t99 * L_99 = &(__this->___guiTouchOffset_19);
		NullCheck(L_99);
		float L_100 = (L_99->___x_1);
		Vector2_t99 * L_101 = &(__this->___guiCenter_20);
		NullCheck(L_101);
		float L_102 = (L_101->___x_1);
		Vector2_t99 * L_103 = &(__this->___guiTouchOffset_19);
		NullCheck(L_103);
		float L_104 = (L_103->___x_1);
		NullCheck(L_95);
		L_95->___x_1 = ((float)((float)((float)(((float)(L_98+L_100))-L_102))/(float)L_104));
		Vector2_t99 * L_105 = &(__this->___position_9);
		GUITexture_t100 * L_106 = (__this->___gui_16);
		NullCheck(L_106);
		Rect_t103  L_107 = GUITexture_get_pixelInset_m674(L_106, /*hidden argument*/&GUITexture_get_pixelInset_m674_MethodInfo);
		V_26 = L_107;
		float L_108 = Rect_get_y_m677((&V_26), /*hidden argument*/&Rect_get_y_m677_MethodInfo);
		Vector2_t99 * L_109 = &(__this->___guiTouchOffset_19);
		NullCheck(L_109);
		float L_110 = (L_109->___y_2);
		Vector2_t99 * L_111 = &(__this->___guiCenter_20);
		NullCheck(L_111);
		float L_112 = (L_111->___y_2);
		Vector2_t99 * L_113 = &(__this->___guiTouchOffset_19);
		NullCheck(L_113);
		float L_114 = (L_113->___y_2);
		NullCheck(L_105);
		L_105->___y_2 = ((float)((float)((float)(((float)(L_108+L_110))-L_112))/(float)L_114));
	}

IL_042b:
	{
		Vector2_t99 * L_115 = &(__this->___position_9);
		NullCheck(L_115);
		float L_116 = (L_115->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_117 = fabsf(L_116);
		V_6 = L_117;
		Vector2_t99 * L_118 = &(__this->___position_9);
		NullCheck(L_118);
		float L_119 = (L_118->___y_2);
		float L_120 = fabsf(L_119);
		V_7 = L_120;
		Vector2_t99 * L_121 = &(__this->___deadZone_7);
		NullCheck(L_121);
		float L_122 = (L_121->___x_1);
		if ((((float)V_6) >= ((float)L_122)))
		{
			goto IL_0473;
		}
	}
	{
		Vector2_t99 * L_123 = &(__this->___position_9);
		NullCheck(L_123);
		L_123->___x_1 = (((float)0));
		goto IL_04b7;
	}

IL_0473:
	{
		bool L_124 = (__this->___normalize_8);
		if (!L_124)
		{
			goto IL_04b7;
		}
	}
	{
		Vector2_t99 * L_125 = &(__this->___position_9);
		Vector2_t99 * L_126 = &(__this->___position_9);
		NullCheck(L_126);
		float L_127 = (L_126->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_128 = Mathf_Sign_m700(NULL /*static, unused*/, L_127, /*hidden argument*/&Mathf_Sign_m700_MethodInfo);
		Vector2_t99 * L_129 = &(__this->___deadZone_7);
		NullCheck(L_129);
		float L_130 = (L_129->___x_1);
		Vector2_t99 * L_131 = &(__this->___deadZone_7);
		NullCheck(L_131);
		float L_132 = (L_131->___x_1);
		NullCheck(L_125);
		L_125->___x_1 = ((float)((float)((float)((float)L_128*(float)((float)(V_6-L_130))))/(float)((float)((((float)1))-L_132))));
	}

IL_04b7:
	{
		Vector2_t99 * L_133 = &(__this->___deadZone_7);
		NullCheck(L_133);
		float L_134 = (L_133->___y_2);
		if ((((float)V_7) >= ((float)L_134)))
		{
			goto IL_04db;
		}
	}
	{
		Vector2_t99 * L_135 = &(__this->___position_9);
		NullCheck(L_135);
		L_135->___y_2 = (((float)0));
		goto IL_051f;
	}

IL_04db:
	{
		bool L_136 = (__this->___normalize_8);
		if (!L_136)
		{
			goto IL_051f;
		}
	}
	{
		Vector2_t99 * L_137 = &(__this->___position_9);
		Vector2_t99 * L_138 = &(__this->___position_9);
		NullCheck(L_138);
		float L_139 = (L_138->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_140 = Mathf_Sign_m700(NULL /*static, unused*/, L_139, /*hidden argument*/&Mathf_Sign_m700_MethodInfo);
		Vector2_t99 * L_141 = &(__this->___deadZone_7);
		NullCheck(L_141);
		float L_142 = (L_141->___y_2);
		Vector2_t99 * L_143 = &(__this->___deadZone_7);
		NullCheck(L_143);
		float L_144 = (L_143->___y_2);
		NullCheck(L_137);
		L_137->___y_2 = ((float)((float)((float)((float)L_140*(float)((float)(V_7-L_142))))/(float)((float)((((float)1))-L_144))));
	}

IL_051f:
	{
		return;
	}
}
// System.Void Joystick::Main()
extern MethodInfo Joystick_Main_m758_MethodInfo;
 void Joystick_Main_m758 (Joystick_t208 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition Joystick
extern Il2CppType JoystickU5BU5D_t214_0_0_145;
FieldInfo Joystick_t208____joysticks_2_FieldInfo = 
{
	"joysticks"/* name */
	, &JoystickU5BU5D_t214_0_0_145/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208_StaticFields, ___joysticks_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_145;
FieldInfo Joystick_t208____enumeratedJoysticks_3_FieldInfo = 
{
	"enumeratedJoysticks"/* name */
	, &Boolean_t122_0_0_145/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208_StaticFields, ___enumeratedJoysticks_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_145;
FieldInfo Joystick_t208____tapTimeDelta_4_FieldInfo = 
{
	"tapTimeDelta"/* name */
	, &Single_t170_0_0_145/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208_StaticFields, ___tapTimeDelta_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo Joystick_t208____touchPad_5_FieldInfo = 
{
	"touchPad"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___touchPad_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Rect_t103_0_0_6;
FieldInfo Joystick_t208____touchZone_6_FieldInfo = 
{
	"touchZone"/* name */
	, &Rect_t103_0_0_6/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___touchZone_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo Joystick_t208____deadZone_7_FieldInfo = 
{
	"deadZone"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___deadZone_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo Joystick_t208____normalize_8_FieldInfo = 
{
	"normalize"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___normalize_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo Joystick_t208____position_9_FieldInfo = 
{
	"position"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___position_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_6;
FieldInfo Joystick_t208____tapCount_10_FieldInfo = 
{
	"tapCount"/* name */
	, &Int32_t123_0_0_6/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___tapCount_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo Joystick_t208____lastFingerId_11_FieldInfo = 
{
	"lastFingerId"/* name */
	, &Int32_t123_0_0_1/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___lastFingerId_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo Joystick_t208____tapTimeWindow_12_FieldInfo = 
{
	"tapTimeWindow"/* name */
	, &Single_t170_0_0_1/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___tapTimeWindow_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_1;
FieldInfo Joystick_t208____fingerDownPos_13_FieldInfo = 
{
	"fingerDownPos"/* name */
	, &Vector2_t99_0_0_1/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___fingerDownPos_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo Joystick_t208____fingerDownTime_14_FieldInfo = 
{
	"fingerDownTime"/* name */
	, &Single_t170_0_0_1/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___fingerDownTime_14)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo Joystick_t208____firstDeltaTime_15_FieldInfo = 
{
	"firstDeltaTime"/* name */
	, &Single_t170_0_0_1/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___firstDeltaTime_15)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GUITexture_t100_0_0_1;
FieldInfo Joystick_t208____gui_16_FieldInfo = 
{
	"gui"/* name */
	, &GUITexture_t100_0_0_1/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___gui_16)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Rect_t103_0_0_1;
FieldInfo Joystick_t208____defaultRect_17_FieldInfo = 
{
	"defaultRect"/* name */
	, &Rect_t103_0_0_1/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___defaultRect_17)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boundary_t213_0_0_1;
FieldInfo Joystick_t208____guiBoundary_18_FieldInfo = 
{
	"guiBoundary"/* name */
	, &Boundary_t213_0_0_1/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___guiBoundary_18)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_1;
FieldInfo Joystick_t208____guiTouchOffset_19_FieldInfo = 
{
	"guiTouchOffset"/* name */
	, &Vector2_t99_0_0_1/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___guiTouchOffset_19)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_1;
FieldInfo Joystick_t208____guiCenter_20_FieldInfo = 
{
	"guiCenter"/* name */
	, &Vector2_t99_0_0_1/* type */
	, &Joystick_t208_il2cpp_TypeInfo/* parent */
	, offsetof(Joystick_t208, ___guiCenter_20)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Joystick_t208_FieldInfos[] =
{
	&Joystick_t208____joysticks_2_FieldInfo,
	&Joystick_t208____enumeratedJoysticks_3_FieldInfo,
	&Joystick_t208____tapTimeDelta_4_FieldInfo,
	&Joystick_t208____touchPad_5_FieldInfo,
	&Joystick_t208____touchZone_6_FieldInfo,
	&Joystick_t208____deadZone_7_FieldInfo,
	&Joystick_t208____normalize_8_FieldInfo,
	&Joystick_t208____position_9_FieldInfo,
	&Joystick_t208____tapCount_10_FieldInfo,
	&Joystick_t208____lastFingerId_11_FieldInfo,
	&Joystick_t208____tapTimeWindow_12_FieldInfo,
	&Joystick_t208____fingerDownPos_13_FieldInfo,
	&Joystick_t208____fingerDownTime_14_FieldInfo,
	&Joystick_t208____firstDeltaTime_15_FieldInfo,
	&Joystick_t208____gui_16_FieldInfo,
	&Joystick_t208____defaultRect_17_FieldInfo,
	&Joystick_t208____guiBoundary_18_FieldInfo,
	&Joystick_t208____guiTouchOffset_19_FieldInfo,
	&Joystick_t208____guiCenter_20_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Joystick::.ctor()
MethodInfo Joystick__ctor_m750_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Joystick__ctor_m750/* method */
	, &Joystick_t208_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Joystick::.cctor()
MethodInfo Joystick__cctor_m751_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Joystick__cctor_m751/* method */
	, &Joystick_t208_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Joystick::Start()
MethodInfo Joystick_Start_m752_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Joystick_Start_m752/* method */
	, &Joystick_t208_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Joystick::Disable()
MethodInfo Joystick_Disable_m753_MethodInfo = 
{
	"Disable"/* name */
	, (methodPointerType)&Joystick_Disable_m753/* method */
	, &Joystick_t208_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Joystick::ResetJoystick()
MethodInfo Joystick_ResetJoystick_m754_MethodInfo = 
{
	"ResetJoystick"/* name */
	, (methodPointerType)&Joystick_ResetJoystick_m754/* method */
	, &Joystick_t208_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Joystick::IsFingerDown()
MethodInfo Joystick_IsFingerDown_m755_MethodInfo = 
{
	"IsFingerDown"/* name */
	, (methodPointerType)&Joystick_IsFingerDown_m755/* method */
	, &Joystick_t208_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Joystick_t208_Joystick_LatchedFinger_m756_ParameterInfos[] = 
{
	{"fingerId", 0, 134217729, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void Joystick::LatchedFinger(System.Int32)
MethodInfo Joystick_LatchedFinger_m756_MethodInfo = 
{
	"LatchedFinger"/* name */
	, (methodPointerType)&Joystick_LatchedFinger_m756/* method */
	, &Joystick_t208_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Joystick_t208_Joystick_LatchedFinger_m756_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Joystick::Update()
MethodInfo Joystick_Update_m757_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&Joystick_Update_m757/* method */
	, &Joystick_t208_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Joystick::Main()
MethodInfo Joystick_Main_m758_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&Joystick_Main_m758/* method */
	, &Joystick_t208_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Joystick_t208_MethodInfos[] =
{
	&Joystick__ctor_m750_MethodInfo,
	&Joystick__cctor_m751_MethodInfo,
	&Joystick_Start_m752_MethodInfo,
	&Joystick_Disable_m753_MethodInfo,
	&Joystick_ResetJoystick_m754_MethodInfo,
	&Joystick_IsFingerDown_m755_MethodInfo,
	&Joystick_LatchedFinger_m756_MethodInfo,
	&Joystick_Update_m757_MethodInfo,
	&Joystick_Main_m758_MethodInfo,
	NULL
};
static MethodInfo* Joystick_t208_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&Joystick_Start_m752_MethodInfo,
	&Joystick_Disable_m753_MethodInfo,
	&Joystick_ResetJoystick_m754_MethodInfo,
	&Joystick_IsFingerDown_m755_MethodInfo,
	&Joystick_LatchedFinger_m756_MethodInfo,
	&Joystick_Update_m757_MethodInfo,
	&Joystick_Main_m758_MethodInfo,
};
void Joystick_t208_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t154 * tmp;
		tmp = (RequireComponent_t154 *)il2cpp_codegen_object_new (&RequireComponent_t154_il2cpp_TypeInfo);
		RequireComponent__ctor_m348(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&GUITexture_t100_il2cpp_TypeInfo)), &RequireComponent__ctor_m348_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache Joystick_t208__CustomAttributeCache = {
1,
NULL,
&Joystick_t208_CustomAttributesCacheGenerator
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType Joystick_t208_1_0_0;
struct Joystick_t208;
extern CustomAttributesCache Joystick_t208__CustomAttributeCache;
TypeInfo Joystick_t208_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "Joystick"/* name */
	, ""/* namespaze */
	, Joystick_t208_MethodInfos/* methods */
	, NULL/* properties */
	, Joystick_t208_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Joystick_t208_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Joystick_t208_VTable/* vtable */
	, &Joystick_t208__CustomAttributeCache/* custom_attributes_cache */
	, &Joystick_t208_il2cpp_TypeInfo/* cast_class */
	, &Joystick_t208_0_0_0/* byval_arg */
	, &Joystick_t208_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Joystick_t208)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Joystick_t208_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 9/* method_count */
	, 0/* property_count */
	, 19/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// ObliqueNear
#include "AssemblyU2DUnityScript_ObliqueNear.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ObliqueNear_t215_il2cpp_TypeInfo;
// ObliqueNear
#include "AssemblyU2DUnityScript_ObliqueNearMethodDeclarations.h"

// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
extern TypeInfo Vector4_t216_il2cpp_TypeInfo;
extern TypeInfo Matrix4x4_t176_il2cpp_TypeInfo;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
extern MethodInfo Matrix4x4_get_inverse_m815_MethodInfo;
extern MethodInfo Vector4__ctor_m816_MethodInfo;
extern MethodInfo Matrix4x4_op_Multiply_m817_MethodInfo;
extern MethodInfo Vector4_Dot_m818_MethodInfo;
extern MethodInfo Vector4_op_Multiply_m819_MethodInfo;
extern MethodInfo Matrix4x4_get_Item_m820_MethodInfo;
extern MethodInfo Matrix4x4_set_Item_m821_MethodInfo;
extern MethodInfo Component_GetComponent_TisCamera_t168_m822_MethodInfo;
extern MethodInfo Camera_get_projectionMatrix_m823_MethodInfo;
extern MethodInfo Camera_get_worldToCameraMatrix_m824_MethodInfo;
extern MethodInfo Matrix4x4_MultiplyPoint_m825_MethodInfo;
extern MethodInfo Vector3_get_up_m826_MethodInfo;
extern MethodInfo Vector3_op_UnaryNegation_m707_MethodInfo;
extern MethodInfo Matrix4x4_MultiplyVector_m827_MethodInfo;
extern MethodInfo Vector4_op_Implicit_m828_MethodInfo;
extern MethodInfo Vector3_Dot_m582_MethodInfo;
extern MethodInfo ObliqueNear_CalculateObliqueMatrix_m760_MethodInfo;
extern MethodInfo Camera_set_projectionMatrix_m829_MethodInfo;
struct Component_t128;
// UnityEngine.CastHelper`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_17.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t168_m822(__this, method) (Camera_t168 *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)


// System.Void ObliqueNear::.ctor()
extern MethodInfo ObliqueNear__ctor_m759_MethodInfo;
 void ObliqueNear__ctor_m759 (ObliqueNear_t215 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// UnityEngine.Matrix4x4 ObliqueNear::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
 Matrix4x4_t176  ObliqueNear_CalculateObliqueMatrix_m760 (ObliqueNear_t215 * __this, Matrix4x4_t176  ___projection, Vector4_t216  ___clipPlane, MethodInfo* method){
	Vector4_t216  V_0 = {0};
	Vector4_t216  V_1 = {0};
	{
		Matrix4x4_t176  L_0 = Matrix4x4_get_inverse_m815((&___projection), /*hidden argument*/&Matrix4x4_get_inverse_m815_MethodInfo);
		NullCheck((&___clipPlane));
		float L_1 = ((&___clipPlane)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_2 = Mathf_Sign_m700(NULL /*static, unused*/, L_1, /*hidden argument*/&Mathf_Sign_m700_MethodInfo);
		NullCheck((&___clipPlane));
		float L_3 = ((&___clipPlane)->___y_2);
		float L_4 = Mathf_Sign_m700(NULL /*static, unused*/, L_3, /*hidden argument*/&Mathf_Sign_m700_MethodInfo);
		Vector4_t216  L_5 = {0};
		Vector4__ctor_m816(&L_5, L_2, L_4, (1.0f), (1.0f), /*hidden argument*/&Vector4__ctor_m816_MethodInfo);
		Vector4_t216  L_6 = Matrix4x4_op_Multiply_m817(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/&Matrix4x4_op_Multiply_m817_MethodInfo);
		V_0 = L_6;
		float L_7 = Vector4_Dot_m818(NULL /*static, unused*/, ___clipPlane, V_0, /*hidden argument*/&Vector4_Dot_m818_MethodInfo);
		Vector4_t216  L_8 = Vector4_op_Multiply_m819(NULL /*static, unused*/, ___clipPlane, ((float)((float)(2.0f)/(float)L_7)), /*hidden argument*/&Vector4_op_Multiply_m819_MethodInfo);
		V_1 = L_8;
		NullCheck((&V_1));
		float L_9 = ((&V_1)->___x_1);
		float L_10 = Matrix4x4_get_Item_m820((&___projection), 3, /*hidden argument*/&Matrix4x4_get_Item_m820_MethodInfo);
		Matrix4x4_set_Item_m821((&___projection), 2, ((float)(L_9-L_10)), /*hidden argument*/&Matrix4x4_set_Item_m821_MethodInfo);
		NullCheck((&V_1));
		float L_11 = ((&V_1)->___y_2);
		float L_12 = Matrix4x4_get_Item_m820((&___projection), 7, /*hidden argument*/&Matrix4x4_get_Item_m820_MethodInfo);
		Matrix4x4_set_Item_m821((&___projection), 6, ((float)(L_11-L_12)), /*hidden argument*/&Matrix4x4_set_Item_m821_MethodInfo);
		NullCheck((&V_1));
		float L_13 = ((&V_1)->___z_3);
		float L_14 = Matrix4x4_get_Item_m820((&___projection), ((int32_t)11), /*hidden argument*/&Matrix4x4_get_Item_m820_MethodInfo);
		Matrix4x4_set_Item_m821((&___projection), ((int32_t)10), ((float)(L_13-L_14)), /*hidden argument*/&Matrix4x4_set_Item_m821_MethodInfo);
		NullCheck((&V_1));
		float L_15 = ((&V_1)->___w_4);
		float L_16 = Matrix4x4_get_Item_m820((&___projection), ((int32_t)15), /*hidden argument*/&Matrix4x4_get_Item_m820_MethodInfo);
		Matrix4x4_set_Item_m821((&___projection), ((int32_t)14), ((float)(L_15-L_16)), /*hidden argument*/&Matrix4x4_set_Item_m821_MethodInfo);
		return ___projection;
	}
}
// System.Void ObliqueNear::OnPreCull()
extern MethodInfo ObliqueNear_OnPreCull_m761_MethodInfo;
 void ObliqueNear_OnPreCull_m761 (ObliqueNear_t215 * __this, MethodInfo* method){
	Matrix4x4_t176  V_0 = {0};
	Matrix4x4_t176  V_1 = {0};
	Vector3_t73  V_2 = {0};
	Vector3_t73  V_3 = {0};
	Vector4_t216  V_4 = {0};
	{
		Camera_t168 * L_0 = Component_GetComponent_TisCamera_t168_m822(__this, /*hidden argument*/&Component_GetComponent_TisCamera_t168_m822_MethodInfo);
		NullCheck(L_0);
		Matrix4x4_t176  L_1 = Camera_get_projectionMatrix_m823(L_0, /*hidden argument*/&Camera_get_projectionMatrix_m823_MethodInfo);
		V_0 = L_1;
		Camera_t168 * L_2 = Component_GetComponent_TisCamera_t168_m822(__this, /*hidden argument*/&Component_GetComponent_TisCamera_t168_m822_MethodInfo);
		NullCheck(L_2);
		Matrix4x4_t176  L_3 = Camera_get_worldToCameraMatrix_m824(L_2, /*hidden argument*/&Camera_get_worldToCameraMatrix_m824_MethodInfo);
		V_1 = L_3;
		Transform_t74 * L_4 = (__this->___plane_2);
		NullCheck(L_4);
		Vector3_t73  L_5 = Transform_get_position_m538(L_4, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		Vector3_t73  L_6 = Matrix4x4_MultiplyPoint_m825((&V_1), L_5, /*hidden argument*/&Matrix4x4_MultiplyPoint_m825_MethodInfo);
		V_2 = L_6;
		Vector3_t73  L_7 = Vector3_get_up_m826(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_up_m826_MethodInfo);
		Vector3_t73  L_8 = Vector3_op_UnaryNegation_m707(NULL /*static, unused*/, L_7, /*hidden argument*/&Vector3_op_UnaryNegation_m707_MethodInfo);
		Vector3_t73  L_9 = Matrix4x4_MultiplyVector_m827((&V_1), L_8, /*hidden argument*/&Matrix4x4_MultiplyVector_m827_MethodInfo);
		V_3 = L_9;
		Vector3_Normalize_m581((&V_3), /*hidden argument*/&Vector3_Normalize_m581_MethodInfo);
		Vector4_t216  L_10 = Vector4_op_Implicit_m828(NULL /*static, unused*/, V_3, /*hidden argument*/&Vector4_op_Implicit_m828_MethodInfo);
		V_4 = L_10;
		float L_11 = Vector3_Dot_m582(NULL /*static, unused*/, V_3, V_2, /*hidden argument*/&Vector3_Dot_m582_MethodInfo);
		NullCheck((&V_4));
		(&V_4)->___w_4 = ((-L_11));
		Camera_t168 * L_12 = Component_GetComponent_TisCamera_t168_m822(__this, /*hidden argument*/&Component_GetComponent_TisCamera_t168_m822_MethodInfo);
		Matrix4x4_t176  L_13 = (Matrix4x4_t176 )VirtFuncInvoker2< Matrix4x4_t176 , Matrix4x4_t176 , Vector4_t216  >::Invoke(&ObliqueNear_CalculateObliqueMatrix_m760_MethodInfo, __this, V_0, V_4);
		NullCheck(L_12);
		Camera_set_projectionMatrix_m829(L_12, L_13, /*hidden argument*/&Camera_set_projectionMatrix_m829_MethodInfo);
		return;
	}
}
// System.Void ObliqueNear::Main()
extern MethodInfo ObliqueNear_Main_m762_MethodInfo;
 void ObliqueNear_Main_m762 (ObliqueNear_t215 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition ObliqueNear
extern Il2CppType Transform_t74_0_0_6;
FieldInfo ObliqueNear_t215____plane_2_FieldInfo = 
{
	"plane"/* name */
	, &Transform_t74_0_0_6/* type */
	, &ObliqueNear_t215_il2cpp_TypeInfo/* parent */
	, offsetof(ObliqueNear_t215, ___plane_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* ObliqueNear_t215_FieldInfos[] =
{
	&ObliqueNear_t215____plane_2_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void ObliqueNear::.ctor()
MethodInfo ObliqueNear__ctor_m759_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObliqueNear__ctor_m759/* method */
	, &ObliqueNear_t215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Matrix4x4_t176_0_0_0;
extern Il2CppType Matrix4x4_t176_0_0_0;
extern Il2CppType Vector4_t216_0_0_0;
extern Il2CppType Vector4_t216_0_0_0;
static ParameterInfo ObliqueNear_t215_ObliqueNear_CalculateObliqueMatrix_m760_ParameterInfos[] = 
{
	{"projection", 0, 134217730, &EmptyCustomAttributesCache, &Matrix4x4_t176_0_0_0},
	{"clipPlane", 1, 134217731, &EmptyCustomAttributesCache, &Vector4_t216_0_0_0},
};
extern Il2CppType Matrix4x4_t176_0_0_0;
extern void* RuntimeInvoker_Matrix4x4_t176_Matrix4x4_t176_Vector4_t216 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Matrix4x4 ObliqueNear::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
MethodInfo ObliqueNear_CalculateObliqueMatrix_m760_MethodInfo = 
{
	"CalculateObliqueMatrix"/* name */
	, (methodPointerType)&ObliqueNear_CalculateObliqueMatrix_m760/* method */
	, &ObliqueNear_t215_il2cpp_TypeInfo/* declaring_type */
	, &Matrix4x4_t176_0_0_0/* return_type */
	, RuntimeInvoker_Matrix4x4_t176_Matrix4x4_t176_Vector4_t216/* invoker_method */
	, ObliqueNear_t215_ObliqueNear_CalculateObliqueMatrix_m760_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void ObliqueNear::OnPreCull()
MethodInfo ObliqueNear_OnPreCull_m761_MethodInfo = 
{
	"OnPreCull"/* name */
	, (methodPointerType)&ObliqueNear_OnPreCull_m761/* method */
	, &ObliqueNear_t215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void ObliqueNear::Main()
MethodInfo ObliqueNear_Main_m762_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&ObliqueNear_Main_m762/* method */
	, &ObliqueNear_t215_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ObliqueNear_t215_MethodInfos[] =
{
	&ObliqueNear__ctor_m759_MethodInfo,
	&ObliqueNear_CalculateObliqueMatrix_m760_MethodInfo,
	&ObliqueNear_OnPreCull_m761_MethodInfo,
	&ObliqueNear_Main_m762_MethodInfo,
	NULL
};
static MethodInfo* ObliqueNear_t215_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&ObliqueNear_CalculateObliqueMatrix_m760_MethodInfo,
	&ObliqueNear_OnPreCull_m761_MethodInfo,
	&ObliqueNear_Main_m762_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType ObliqueNear_t215_0_0_0;
extern Il2CppType ObliqueNear_t215_1_0_0;
struct ObliqueNear_t215;
TypeInfo ObliqueNear_t215_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "ObliqueNear"/* name */
	, ""/* namespaze */
	, ObliqueNear_t215_MethodInfos/* methods */
	, NULL/* properties */
	, ObliqueNear_t215_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ObliqueNear_t215_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ObliqueNear_t215_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ObliqueNear_t215_il2cpp_TypeInfo/* cast_class */
	, &ObliqueNear_t215_0_0_0/* byval_arg */
	, &ObliqueNear_t215_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObliqueNear_t215)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// PlayerRelativeControl
#include "AssemblyU2DUnityScript_PlayerRelativeControl.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PlayerRelativeControl_t217_il2cpp_TypeInfo;
// PlayerRelativeControl
#include "AssemblyU2DUnityScript_PlayerRelativeControlMethodDeclarations.h"

extern MethodInfo Transform_get_localPosition_m830_MethodInfo;
extern MethodInfo Mathf_SmoothDamp_m831_MethodInfo;
extern MethodInfo Transform_set_localPosition_m832_MethodInfo;


// System.Void PlayerRelativeControl::.ctor()
extern MethodInfo PlayerRelativeControl__ctor_m763_MethodInfo;
 void PlayerRelativeControl__ctor_m763 (PlayerRelativeControl_t217 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		__this->___forwardSpeed_5 = (((float)4));
		__this->___backwardSpeed_6 = (((float)1));
		__this->___sidestepSpeed_7 = (((float)1));
		__this->___jumpSpeed_8 = (((float)8));
		__this->___inAirMultiplier_9 = (0.25f);
		Vector2_t99  L_0 = {0};
		Vector2__ctor_m636(&L_0, (((float)((int32_t)50))), (((float)((int32_t)25))), /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		__this->___rotationSpeed_10 = L_0;
		return;
	}
}
// System.Void PlayerRelativeControl::Start()
extern MethodInfo PlayerRelativeControl_Start_m764_MethodInfo;
 void PlayerRelativeControl_Start_m764 (PlayerRelativeControl_t217 * __this, MethodInfo* method){
	GameObject_t29 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Transform_t74_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Component_t128 * L_1 = Component_GetComponent_m800(__this, L_0, /*hidden argument*/&Component_GetComponent_m800_MethodInfo);
		__this->___thisTransform_11 = ((Transform_t74 *)Castclass(L_1, InitializedTypeInfo(&Transform_t74_il2cpp_TypeInfo)));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&CharacterController_t209_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Component_t128 * L_3 = Component_GetComponent_m800(__this, L_2, /*hidden argument*/&Component_GetComponent_m800_MethodInfo);
		__this->___character_12 = ((CharacterController_t209 *)Castclass(L_3, InitializedTypeInfo(&CharacterController_t209_il2cpp_TypeInfo)));
		GameObject_t29 * L_4 = GameObject_Find_m801(NULL /*static, unused*/, (String_t*) &_stringLiteral81, /*hidden argument*/&GameObject_Find_m801_MethodInfo);
		V_0 = L_4;
		bool L_5 = Object_op_Implicit_m257(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_5)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t74 * L_6 = (__this->___thisTransform_11);
		NullCheck(V_0);
		Transform_t74 * L_7 = GameObject_get_transform_m537(V_0, /*hidden argument*/&GameObject_get_transform_m537_MethodInfo);
		NullCheck(L_7);
		Vector3_t73  L_8 = Transform_get_position_m538(L_7, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		NullCheck(L_6);
		Transform_set_position_m570(L_6, L_8, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
	}

IL_0062:
	{
		return;
	}
}
// System.Void PlayerRelativeControl::OnEndGame()
extern MethodInfo PlayerRelativeControl_OnEndGame_m765_MethodInfo;
 void PlayerRelativeControl_OnEndGame_m765 (PlayerRelativeControl_t217 * __this, MethodInfo* method){
	{
		Joystick_t208 * L_0 = (__this->___moveJoystick_2);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(&Joystick_Disable_m753_MethodInfo, L_0);
		Joystick_t208 * L_1 = (__this->___rotateJoystick_3);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(&Joystick_Disable_m753_MethodInfo, L_1);
		Behaviour_set_enabled_m547(__this, 0, /*hidden argument*/&Behaviour_set_enabled_m547_MethodInfo);
		return;
	}
}
// System.Void PlayerRelativeControl::Update()
extern MethodInfo PlayerRelativeControl_Update_m766_MethodInfo;
 void PlayerRelativeControl_Update_m766 (PlayerRelativeControl_t217 * __this, MethodInfo* method){
	Vector3_t73  V_0 = {0};
	Vector3_t73  V_1 = {0};
	Vector2_t99  V_2 = {0};
	Vector3_t73  V_3 = {0};
	Vector2_t99  V_4 = {0};
	Vector3_t73  V_5 = {0};
	{
		Transform_t74 * L_0 = (__this->___thisTransform_11);
		Joystick_t208 * L_1 = (__this->___moveJoystick_2);
		NullCheck(L_1);
		Vector2_t99 * L_2 = &(L_1->___position_9);
		NullCheck(L_2);
		float L_3 = (L_2->___x_1);
		Joystick_t208 * L_4 = (__this->___moveJoystick_2);
		NullCheck(L_4);
		Vector2_t99 * L_5 = &(L_4->___position_9);
		NullCheck(L_5);
		float L_6 = (L_5->___y_2);
		Vector3_t73  L_7 = {0};
		Vector3__ctor_m568(&L_7, L_3, (((float)0)), L_6, /*hidden argument*/&Vector3__ctor_m568_MethodInfo);
		NullCheck(L_0);
		Vector3_t73  L_8 = Transform_TransformDirection_m803(L_0, L_7, /*hidden argument*/&Transform_TransformDirection_m803_MethodInfo);
		V_0 = L_8;
		NullCheck((&V_0));
		(&V_0)->___y_2 = (((float)0));
		Vector3_Normalize_m581((&V_0), /*hidden argument*/&Vector3_Normalize_m581_MethodInfo);
		Vector3_t73  L_9 = Vector3_get_zero_m807(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m807_MethodInfo);
		V_1 = L_9;
		Joystick_t208 * L_10 = (__this->___moveJoystick_2);
		NullCheck(L_10);
		Vector2_t99 * L_11 = &(L_10->___position_9);
		NullCheck(L_11);
		float L_12 = (L_11->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_13 = fabsf(L_12);
		Joystick_t208 * L_14 = (__this->___moveJoystick_2);
		NullCheck(L_14);
		Vector2_t99 * L_15 = &(L_14->___position_9);
		NullCheck(L_15);
		float L_16 = (L_15->___y_2);
		float L_17 = fabsf(L_16);
		Vector2_t99  L_18 = {0};
		Vector2__ctor_m636(&L_18, L_13, L_17, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		V_2 = L_18;
		NullCheck((&V_2));
		float L_19 = ((&V_2)->___y_2);
		NullCheck((&V_2));
		float L_20 = ((&V_2)->___x_1);
		if ((((float)L_19) <= ((float)L_20)))
		{
			goto IL_00f4;
		}
	}
	{
		Joystick_t208 * L_21 = (__this->___moveJoystick_2);
		NullCheck(L_21);
		Vector2_t99 * L_22 = &(L_21->___position_9);
		NullCheck(L_22);
		float L_23 = (L_22->___y_2);
		if ((((float)L_23) <= ((float)(((float)0)))))
		{
			goto IL_00bd;
		}
	}
	{
		float L_24 = (__this->___forwardSpeed_5);
		NullCheck((&V_2));
		float L_25 = ((&V_2)->___y_2);
		Vector3_t73  L_26 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_0, ((float)((float)L_24*(float)L_25)), /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_26;
		goto IL_00ef;
	}

IL_00bd:
	{
		float L_27 = (__this->___backwardSpeed_6);
		NullCheck((&V_2));
		float L_28 = ((&V_2)->___y_2);
		Vector3_t73  L_29 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_0, ((float)((float)L_27*(float)L_28)), /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_29;
		Joystick_t208 * L_30 = (__this->___moveJoystick_2);
		NullCheck(L_30);
		Vector2_t99 * L_31 = &(L_30->___position_9);
		NullCheck(L_31);
		float L_32 = (L_31->___y_2);
		NullCheck((&V_1));
		(&V_1)->___z_3 = ((float)((float)L_32*(float)(0.75f)));
	}

IL_00ef:
	{
		goto IL_0127;
	}

IL_00f4:
	{
		float L_33 = (__this->___sidestepSpeed_7);
		NullCheck((&V_2));
		float L_34 = ((&V_2)->___x_1);
		Vector3_t73  L_35 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_0, ((float)((float)L_33*(float)L_34)), /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_35;
		Joystick_t208 * L_36 = (__this->___moveJoystick_2);
		NullCheck(L_36);
		Vector2_t99 * L_37 = &(L_36->___position_9);
		NullCheck(L_37);
		float L_38 = (L_37->___x_1);
		NullCheck((&V_1));
		(&V_1)->___x_1 = ((float)((float)((-L_38))*(float)(0.5f)));
	}

IL_0127:
	{
		CharacterController_t209 * L_39 = (__this->___character_12);
		NullCheck(L_39);
		bool L_40 = CharacterController_get_isGrounded_m804(L_39, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_40)
		{
			goto IL_016f;
		}
	}
	{
		Joystick_t208 * L_41 = (__this->___rotateJoystick_3);
		NullCheck(L_41);
		int32_t L_42 = (L_41->___tapCount_10);
		if ((((uint32_t)L_42) != ((uint32_t)2)))
		{
			goto IL_016a;
		}
	}
	{
		CharacterController_t209 * L_43 = (__this->___character_12);
		NullCheck(L_43);
		Vector3_t73  L_44 = CharacterController_get_velocity_m802(L_43, /*hidden argument*/&CharacterController_get_velocity_m802_MethodInfo);
		__this->___velocity_14 = L_44;
		Vector3_t73 * L_45 = &(__this->___velocity_14);
		float L_46 = (__this->___jumpSpeed_8);
		NullCheck(L_45);
		L_45->___y_2 = L_46;
	}

IL_016a:
	{
		goto IL_01d8;
	}

IL_016f:
	{
		Vector3_t73 * L_47 = &(__this->___velocity_14);
		Vector3_t73 * L_48 = &(__this->___velocity_14);
		NullCheck(L_48);
		float L_49 = (L_48->___y_2);
		Vector3_t73  L_50 = Physics_get_gravity_m805(NULL /*static, unused*/, /*hidden argument*/&Physics_get_gravity_m805_MethodInfo);
		V_5 = L_50;
		NullCheck((&V_5));
		float L_51 = ((&V_5)->___y_2);
		float L_52 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		NullCheck(L_47);
		L_47->___y_2 = ((float)(L_49+((float)((float)L_51*(float)L_52))));
		float L_53 = (__this->___jumpSpeed_8);
		NullCheck((&V_1));
		(&V_1)->___z_3 = ((float)((float)((-L_53))*(float)(0.25f)));
		NullCheck((&V_0));
		float L_54 = ((&V_0)->___x_1);
		float L_55 = (__this->___inAirMultiplier_9);
		NullCheck((&V_0));
		(&V_0)->___x_1 = ((float)((float)L_54*(float)L_55));
		NullCheck((&V_0));
		float L_56 = ((&V_0)->___z_3);
		float L_57 = (__this->___inAirMultiplier_9);
		NullCheck((&V_0));
		(&V_0)->___z_3 = ((float)((float)L_56*(float)L_57));
	}

IL_01d8:
	{
		Vector3_t73  L_58 = (__this->___velocity_14);
		Vector3_t73  L_59 = Vector3_op_Addition_m576(NULL /*static, unused*/, V_0, L_58, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_0 = L_59;
		Vector3_t73  L_60 = Physics_get_gravity_m805(NULL /*static, unused*/, /*hidden argument*/&Physics_get_gravity_m805_MethodInfo);
		Vector3_t73  L_61 = Vector3_op_Addition_m576(NULL /*static, unused*/, V_0, L_60, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_0 = L_61;
		float L_62 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		Vector3_t73  L_63 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_0, L_62, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_63;
		CharacterController_t209 * L_64 = (__this->___character_12);
		NullCheck(L_64);
		CharacterController_Move_m806(L_64, V_0, /*hidden argument*/&CharacterController_Move_m806_MethodInfo);
		CharacterController_t209 * L_65 = (__this->___character_12);
		NullCheck(L_65);
		bool L_66 = CharacterController_get_isGrounded_m804(L_65, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_66)
		{
			goto IL_0225;
		}
	}
	{
		Vector3_t73  L_67 = Vector3_get_zero_m807(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m807_MethodInfo);
		__this->___velocity_14 = L_67;
	}

IL_0225:
	{
		Transform_t74 * L_68 = (__this->___cameraPivot_4);
		NullCheck(L_68);
		Vector3_t73  L_69 = Transform_get_localPosition_m830(L_68, /*hidden argument*/&Transform_get_localPosition_m830_MethodInfo);
		V_3 = L_69;
		NullCheck((&V_3));
		float L_70 = ((&V_3)->___x_1);
		NullCheck((&V_1));
		float L_71 = ((&V_1)->___x_1);
		Vector3_t73 * L_72 = &(__this->___cameraVelocity_13);
		NullCheck(L_72);
		float* L_73 = &(L_72->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_74 = Mathf_SmoothDamp_m831(NULL /*static, unused*/, L_70, L_71, L_73, (0.3f), /*hidden argument*/&Mathf_SmoothDamp_m831_MethodInfo);
		NullCheck((&V_3));
		(&V_3)->___x_1 = L_74;
		NullCheck((&V_3));
		float L_75 = ((&V_3)->___z_3);
		NullCheck((&V_1));
		float L_76 = ((&V_1)->___z_3);
		Vector3_t73 * L_77 = &(__this->___cameraVelocity_13);
		NullCheck(L_77);
		float* L_78 = &(L_77->___z_3);
		float L_79 = Mathf_SmoothDamp_m831(NULL /*static, unused*/, L_75, L_76, L_78, (0.5f), /*hidden argument*/&Mathf_SmoothDamp_m831_MethodInfo);
		NullCheck((&V_3));
		(&V_3)->___z_3 = L_79;
		Transform_t74 * L_80 = (__this->___cameraPivot_4);
		NullCheck(L_80);
		Transform_set_localPosition_m832(L_80, V_3, /*hidden argument*/&Transform_set_localPosition_m832_MethodInfo);
		CharacterController_t209 * L_81 = (__this->___character_12);
		NullCheck(L_81);
		bool L_82 = CharacterController_get_isGrounded_m804(L_81, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_82)
		{
			goto IL_031d;
		}
	}
	{
		Joystick_t208 * L_83 = (__this->___rotateJoystick_3);
		NullCheck(L_83);
		Vector2_t99  L_84 = (L_83->___position_9);
		V_4 = L_84;
		NullCheck((&V_4));
		float L_85 = ((&V_4)->___x_1);
		Vector2_t99 * L_86 = &(__this->___rotationSpeed_10);
		NullCheck(L_86);
		float L_87 = (L_86->___x_1);
		NullCheck((&V_4));
		(&V_4)->___x_1 = ((float)((float)L_85*(float)L_87));
		NullCheck((&V_4));
		float L_88 = ((&V_4)->___y_2);
		Vector2_t99 * L_89 = &(__this->___rotationSpeed_10);
		NullCheck(L_89);
		float L_90 = (L_89->___y_2);
		NullCheck((&V_4));
		(&V_4)->___y_2 = ((float)((float)L_88*(float)L_90));
		float L_91 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		Vector2_t99  L_92 = Vector2_op_Multiply_m808(NULL /*static, unused*/, V_4, L_91, /*hidden argument*/&Vector2_op_Multiply_m808_MethodInfo);
		V_4 = L_92;
		Transform_t74 * L_93 = (__this->___thisTransform_11);
		NullCheck((&V_4));
		float L_94 = ((&V_4)->___x_1);
		NullCheck(L_93);
		Transform_Rotate_m809(L_93, (((float)0)), L_94, (((float)0)), 0, /*hidden argument*/&Transform_Rotate_m809_MethodInfo);
		Transform_t74 * L_95 = (__this->___cameraPivot_4);
		NullCheck((&V_4));
		float L_96 = ((&V_4)->___y_2);
		NullCheck(L_95);
		Transform_Rotate_m623(L_95, L_96, (((float)0)), (((float)0)), /*hidden argument*/&Transform_Rotate_m623_MethodInfo);
	}

IL_031d:
	{
		return;
	}
}
// System.Void PlayerRelativeControl::Main()
extern MethodInfo PlayerRelativeControl_Main_m767_MethodInfo;
 void PlayerRelativeControl_Main_m767 (PlayerRelativeControl_t217 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition PlayerRelativeControl
extern Il2CppType Joystick_t208_0_0_6;
FieldInfo PlayerRelativeControl_t217____moveJoystick_2_FieldInfo = 
{
	"moveJoystick"/* name */
	, &Joystick_t208_0_0_6/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___moveJoystick_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Joystick_t208_0_0_6;
FieldInfo PlayerRelativeControl_t217____rotateJoystick_3_FieldInfo = 
{
	"rotateJoystick"/* name */
	, &Joystick_t208_0_0_6/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___rotateJoystick_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_6;
FieldInfo PlayerRelativeControl_t217____cameraPivot_4_FieldInfo = 
{
	"cameraPivot"/* name */
	, &Transform_t74_0_0_6/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___cameraPivot_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo PlayerRelativeControl_t217____forwardSpeed_5_FieldInfo = 
{
	"forwardSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___forwardSpeed_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo PlayerRelativeControl_t217____backwardSpeed_6_FieldInfo = 
{
	"backwardSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___backwardSpeed_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo PlayerRelativeControl_t217____sidestepSpeed_7_FieldInfo = 
{
	"sidestepSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___sidestepSpeed_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo PlayerRelativeControl_t217____jumpSpeed_8_FieldInfo = 
{
	"jumpSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___jumpSpeed_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo PlayerRelativeControl_t217____inAirMultiplier_9_FieldInfo = 
{
	"inAirMultiplier"/* name */
	, &Single_t170_0_0_6/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___inAirMultiplier_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_6;
FieldInfo PlayerRelativeControl_t217____rotationSpeed_10_FieldInfo = 
{
	"rotationSpeed"/* name */
	, &Vector2_t99_0_0_6/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___rotationSpeed_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_1;
FieldInfo PlayerRelativeControl_t217____thisTransform_11_FieldInfo = 
{
	"thisTransform"/* name */
	, &Transform_t74_0_0_1/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___thisTransform_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType CharacterController_t209_0_0_1;
FieldInfo PlayerRelativeControl_t217____character_12_FieldInfo = 
{
	"character"/* name */
	, &CharacterController_t209_0_0_1/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___character_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo PlayerRelativeControl_t217____cameraVelocity_13_FieldInfo = 
{
	"cameraVelocity"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___cameraVelocity_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo PlayerRelativeControl_t217____velocity_14_FieldInfo = 
{
	"velocity"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* parent */
	, offsetof(PlayerRelativeControl_t217, ___velocity_14)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* PlayerRelativeControl_t217_FieldInfos[] =
{
	&PlayerRelativeControl_t217____moveJoystick_2_FieldInfo,
	&PlayerRelativeControl_t217____rotateJoystick_3_FieldInfo,
	&PlayerRelativeControl_t217____cameraPivot_4_FieldInfo,
	&PlayerRelativeControl_t217____forwardSpeed_5_FieldInfo,
	&PlayerRelativeControl_t217____backwardSpeed_6_FieldInfo,
	&PlayerRelativeControl_t217____sidestepSpeed_7_FieldInfo,
	&PlayerRelativeControl_t217____jumpSpeed_8_FieldInfo,
	&PlayerRelativeControl_t217____inAirMultiplier_9_FieldInfo,
	&PlayerRelativeControl_t217____rotationSpeed_10_FieldInfo,
	&PlayerRelativeControl_t217____thisTransform_11_FieldInfo,
	&PlayerRelativeControl_t217____character_12_FieldInfo,
	&PlayerRelativeControl_t217____cameraVelocity_13_FieldInfo,
	&PlayerRelativeControl_t217____velocity_14_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void PlayerRelativeControl::.ctor()
MethodInfo PlayerRelativeControl__ctor_m763_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PlayerRelativeControl__ctor_m763/* method */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void PlayerRelativeControl::Start()
MethodInfo PlayerRelativeControl_Start_m764_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&PlayerRelativeControl_Start_m764/* method */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void PlayerRelativeControl::OnEndGame()
MethodInfo PlayerRelativeControl_OnEndGame_m765_MethodInfo = 
{
	"OnEndGame"/* name */
	, (methodPointerType)&PlayerRelativeControl_OnEndGame_m765/* method */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void PlayerRelativeControl::Update()
MethodInfo PlayerRelativeControl_Update_m766_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&PlayerRelativeControl_Update_m766/* method */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void PlayerRelativeControl::Main()
MethodInfo PlayerRelativeControl_Main_m767_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&PlayerRelativeControl_Main_m767/* method */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PlayerRelativeControl_t217_MethodInfos[] =
{
	&PlayerRelativeControl__ctor_m763_MethodInfo,
	&PlayerRelativeControl_Start_m764_MethodInfo,
	&PlayerRelativeControl_OnEndGame_m765_MethodInfo,
	&PlayerRelativeControl_Update_m766_MethodInfo,
	&PlayerRelativeControl_Main_m767_MethodInfo,
	NULL
};
static MethodInfo* PlayerRelativeControl_t217_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&PlayerRelativeControl_Start_m764_MethodInfo,
	&PlayerRelativeControl_OnEndGame_m765_MethodInfo,
	&PlayerRelativeControl_Update_m766_MethodInfo,
	&PlayerRelativeControl_Main_m767_MethodInfo,
};
void PlayerRelativeControl_t217_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t154 * tmp;
		tmp = (RequireComponent_t154 *)il2cpp_codegen_object_new (&RequireComponent_t154_il2cpp_TypeInfo);
		RequireComponent__ctor_m348(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&CharacterController_t209_il2cpp_TypeInfo)), &RequireComponent__ctor_m348_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache PlayerRelativeControl_t217__CustomAttributeCache = {
1,
NULL,
&PlayerRelativeControl_t217_CustomAttributesCacheGenerator
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType PlayerRelativeControl_t217_0_0_0;
extern Il2CppType PlayerRelativeControl_t217_1_0_0;
struct PlayerRelativeControl_t217;
extern CustomAttributesCache PlayerRelativeControl_t217__CustomAttributeCache;
TypeInfo PlayerRelativeControl_t217_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "PlayerRelativeControl"/* name */
	, ""/* namespaze */
	, PlayerRelativeControl_t217_MethodInfos/* methods */
	, NULL/* properties */
	, PlayerRelativeControl_t217_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, PlayerRelativeControl_t217_VTable/* vtable */
	, &PlayerRelativeControl_t217__CustomAttributeCache/* custom_attributes_cache */
	, &PlayerRelativeControl_t217_il2cpp_TypeInfo/* cast_class */
	, &PlayerRelativeControl_t217_0_0_0/* byval_arg */
	, &PlayerRelativeControl_t217_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlayerRelativeControl_t217)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// RollABall
#include "AssemblyU2DUnityScript_RollABall.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RollABall_t218_il2cpp_TypeInfo;
// RollABall
#include "AssemblyU2DUnityScript_RollABallMethodDeclarations.h"

// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
extern MethodInfo Component_GetComponent_TisCollider_t70_m833_MethodInfo;
extern MethodInfo Collider_get_bounds_m662_MethodInfo;
extern MethodInfo Bounds_get_extents_m834_MethodInfo;
extern MethodInfo Component_GetComponent_TisRigidbody_t180_m656_MethodInfo;
extern MethodInfo Rigidbody_AddForce_m619_MethodInfo;
extern MethodInfo Vector3_op_Subtraction_m574_MethodInfo;
extern MethodInfo Vector3_op_Division_m630_MethodInfo;
extern MethodInfo Transform_Rotate_m835_MethodInfo;
struct Component_t128;
// UnityEngine.CastHelper`1<UnityEngine.Collider>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_18.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t70_m833(__this, method) (Collider_t70 *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)
struct Component_t128;
// UnityEngine.CastHelper`1<UnityEngine.Rigidbody>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_8.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t180_m656(__this, method) (Rigidbody_t180 *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)


// System.Void RollABall::.ctor()
extern MethodInfo RollABall__ctor_m768_MethodInfo;
 void RollABall__ctor_m768 (RollABall_t218 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		Vector3_t73  L_0 = Vector3_get_zero_m807(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m807_MethodInfo);
		__this->___tilt_2 = L_0;
		return;
	}
}
// System.Void RollABall::Start()
extern MethodInfo RollABall_Start_m769_MethodInfo;
 void RollABall_Start_m769 (RollABall_t218 * __this, MethodInfo* method){
	Bounds_t198  V_0 = {0};
	Vector3_t73  V_1 = {0};
	{
		Collider_t70 * L_0 = Component_GetComponent_TisCollider_t70_m833(__this, /*hidden argument*/&Component_GetComponent_TisCollider_t70_m833_MethodInfo);
		NullCheck(L_0);
		Bounds_t198  L_1 = Collider_get_bounds_m662(L_0, /*hidden argument*/&Collider_get_bounds_m662_MethodInfo);
		V_0 = L_1;
		Vector3_t73  L_2 = Bounds_get_extents_m834((&V_0), /*hidden argument*/&Bounds_get_extents_m834_MethodInfo);
		V_1 = L_2;
		NullCheck((&V_1));
		float L_3 = ((&V_1)->___x_1);
		__this->___circ_4 = ((float)((float)(6.28318548f)*(float)L_3));
		Transform_t74 * L_4 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_4);
		Vector3_t73  L_5 = Transform_get_position_m538(L_4, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		__this->___previousPosition_5 = L_5;
		return;
	}
}
// System.Void RollABall::Update()
extern MethodInfo RollABall_Update_m770_MethodInfo;
 void RollABall_Update_m770 (RollABall_t218 * __this, MethodInfo* method){
	Vector3_t73  V_0 = {0};
	Vector3_t73  V_1 = {0};
	{
		Vector3_t73 * L_0 = &(__this->___tilt_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		Vector3_t73  L_1 = Input_get_acceleration_m810(NULL /*static, unused*/, /*hidden argument*/&Input_get_acceleration_m810_MethodInfo);
		V_0 = L_1;
		NullCheck((&V_0));
		float L_2 = ((&V_0)->___y_2);
		NullCheck(L_0);
		L_0->___x_1 = ((-L_2));
		Vector3_t73 * L_3 = &(__this->___tilt_2);
		Vector3_t73  L_4 = Input_get_acceleration_m810(NULL /*static, unused*/, /*hidden argument*/&Input_get_acceleration_m810_MethodInfo);
		V_1 = L_4;
		NullCheck((&V_1));
		float L_5 = ((&V_1)->___x_1);
		NullCheck(L_3);
		L_3->___z_3 = L_5;
		Rigidbody_t180 * L_6 = Component_GetComponent_TisRigidbody_t180_m656(__this, /*hidden argument*/&Component_GetComponent_TisRigidbody_t180_m656_MethodInfo);
		Vector3_t73  L_7 = (__this->___tilt_2);
		float L_8 = (__this->___speed_3);
		Vector3_t73  L_9 = Vector3_op_Multiply_m573(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		float L_10 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		Vector3_t73  L_11 = Vector3_op_Multiply_m573(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		NullCheck(L_6);
		Rigidbody_AddForce_m619(L_6, L_11, /*hidden argument*/&Rigidbody_AddForce_m619_MethodInfo);
		return;
	}
}
// System.Void RollABall::LateUpdate()
extern MethodInfo RollABall_LateUpdate_m771_MethodInfo;
 void RollABall_LateUpdate_m771 (RollABall_t218 * __this, MethodInfo* method){
	Vector3_t73  V_0 = {0};
	{
		Transform_t74 * L_0 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_0);
		Vector3_t73  L_1 = Transform_get_position_m538(L_0, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		Vector3_t73  L_2 = (__this->___previousPosition_5);
		Vector3_t73  L_3 = Vector3_op_Subtraction_m574(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/&Vector3_op_Subtraction_m574_MethodInfo);
		V_0 = L_3;
		NullCheck((&V_0));
		float L_4 = ((&V_0)->___z_3);
		NullCheck((&V_0));
		float L_5 = ((&V_0)->___x_1);
		Vector3_t73  L_6 = {0};
		Vector3__ctor_m568(&L_6, L_4, (((float)0)), ((-L_5)), /*hidden argument*/&Vector3__ctor_m568_MethodInfo);
		V_0 = L_6;
		Transform_t74 * L_7 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		float L_8 = (__this->___circ_4);
		Vector3_t73  L_9 = Vector3_op_Division_m630(NULL /*static, unused*/, V_0, L_8, /*hidden argument*/&Vector3_op_Division_m630_MethodInfo);
		Vector3_t73  L_10 = Vector3_op_Multiply_m573(NULL /*static, unused*/, L_9, (((float)((int32_t)360))), /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		NullCheck(L_7);
		Transform_Rotate_m835(L_7, L_10, 0, /*hidden argument*/&Transform_Rotate_m835_MethodInfo);
		Transform_t74 * L_11 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_11);
		Vector3_t73  L_12 = Transform_get_position_m538(L_11, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		__this->___previousPosition_5 = L_12;
		return;
	}
}
// System.Void RollABall::Main()
extern MethodInfo RollABall_Main_m772_MethodInfo;
 void RollABall_Main_m772 (RollABall_t218 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition RollABall
extern Il2CppType Vector3_t73_0_0_6;
FieldInfo RollABall_t218____tilt_2_FieldInfo = 
{
	"tilt"/* name */
	, &Vector3_t73_0_0_6/* type */
	, &RollABall_t218_il2cpp_TypeInfo/* parent */
	, offsetof(RollABall_t218, ___tilt_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo RollABall_t218____speed_3_FieldInfo = 
{
	"speed"/* name */
	, &Single_t170_0_0_6/* type */
	, &RollABall_t218_il2cpp_TypeInfo/* parent */
	, offsetof(RollABall_t218, ___speed_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo RollABall_t218____circ_4_FieldInfo = 
{
	"circ"/* name */
	, &Single_t170_0_0_1/* type */
	, &RollABall_t218_il2cpp_TypeInfo/* parent */
	, offsetof(RollABall_t218, ___circ_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo RollABall_t218____previousPosition_5_FieldInfo = 
{
	"previousPosition"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &RollABall_t218_il2cpp_TypeInfo/* parent */
	, offsetof(RollABall_t218, ___previousPosition_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* RollABall_t218_FieldInfos[] =
{
	&RollABall_t218____tilt_2_FieldInfo,
	&RollABall_t218____speed_3_FieldInfo,
	&RollABall_t218____circ_4_FieldInfo,
	&RollABall_t218____previousPosition_5_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void RollABall::.ctor()
MethodInfo RollABall__ctor_m768_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RollABall__ctor_m768/* method */
	, &RollABall_t218_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void RollABall::Start()
MethodInfo RollABall_Start_m769_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&RollABall_Start_m769/* method */
	, &RollABall_t218_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void RollABall::Update()
MethodInfo RollABall_Update_m770_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&RollABall_Update_m770/* method */
	, &RollABall_t218_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void RollABall::LateUpdate()
MethodInfo RollABall_LateUpdate_m771_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&RollABall_LateUpdate_m771/* method */
	, &RollABall_t218_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void RollABall::Main()
MethodInfo RollABall_Main_m772_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&RollABall_Main_m772/* method */
	, &RollABall_t218_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RollABall_t218_MethodInfos[] =
{
	&RollABall__ctor_m768_MethodInfo,
	&RollABall_Start_m769_MethodInfo,
	&RollABall_Update_m770_MethodInfo,
	&RollABall_LateUpdate_m771_MethodInfo,
	&RollABall_Main_m772_MethodInfo,
	NULL
};
static MethodInfo* RollABall_t218_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&RollABall_Start_m769_MethodInfo,
	&RollABall_Update_m770_MethodInfo,
	&RollABall_LateUpdate_m771_MethodInfo,
	&RollABall_Main_m772_MethodInfo,
};
extern TypeInfo Rigidbody_t180_il2cpp_TypeInfo;
void RollABall_t218_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t154 * tmp;
		tmp = (RequireComponent_t154 *)il2cpp_codegen_object_new (&RequireComponent_t154_il2cpp_TypeInfo);
		RequireComponent__ctor_m348(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&Rigidbody_t180_il2cpp_TypeInfo)), &RequireComponent__ctor_m348_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache RollABall_t218__CustomAttributeCache = {
1,
NULL,
&RollABall_t218_CustomAttributesCacheGenerator
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType RollABall_t218_0_0_0;
extern Il2CppType RollABall_t218_1_0_0;
struct RollABall_t218;
extern CustomAttributesCache RollABall_t218__CustomAttributeCache;
TypeInfo RollABall_t218_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "RollABall"/* name */
	, ""/* namespaze */
	, RollABall_t218_MethodInfos/* methods */
	, NULL/* properties */
	, RollABall_t218_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &RollABall_t218_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, RollABall_t218_VTable/* vtable */
	, &RollABall_t218__CustomAttributeCache/* custom_attributes_cache */
	, &RollABall_t218_il2cpp_TypeInfo/* cast_class */
	, &RollABall_t218_0_0_0/* byval_arg */
	, &RollABall_t218_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RollABall_t218)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// ConstraintAxis
#include "AssemblyU2DUnityScript_ConstraintAxis.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ConstraintAxis_t219_il2cpp_TypeInfo;
// ConstraintAxis
#include "AssemblyU2DUnityScript_ConstraintAxisMethodDeclarations.h"



// Metadata Definition ConstraintAxis
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo ConstraintAxis_t219____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &ConstraintAxis_t219_il2cpp_TypeInfo/* parent */
	, offsetof(ConstraintAxis_t219, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ConstraintAxis_t219_0_0_32854;
FieldInfo ConstraintAxis_t219____X_2_FieldInfo = 
{
	"X"/* name */
	, &ConstraintAxis_t219_0_0_32854/* type */
	, &ConstraintAxis_t219_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ConstraintAxis_t219_0_0_32854;
FieldInfo ConstraintAxis_t219____Y_3_FieldInfo = 
{
	"Y"/* name */
	, &ConstraintAxis_t219_0_0_32854/* type */
	, &ConstraintAxis_t219_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ConstraintAxis_t219_0_0_32854;
FieldInfo ConstraintAxis_t219____Z_4_FieldInfo = 
{
	"Z"/* name */
	, &ConstraintAxis_t219_0_0_32854/* type */
	, &ConstraintAxis_t219_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* ConstraintAxis_t219_FieldInfos[] =
{
	&ConstraintAxis_t219____value___1_FieldInfo,
	&ConstraintAxis_t219____X_2_FieldInfo,
	&ConstraintAxis_t219____Y_3_FieldInfo,
	&ConstraintAxis_t219____Z_4_FieldInfo,
	NULL
};
static const int32_t ConstraintAxis_t219____X_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry ConstraintAxis_t219____X_2_DefaultValue = 
{
	&ConstraintAxis_t219____X_2_FieldInfo/* field */
	, { (char*)&ConstraintAxis_t219____X_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ConstraintAxis_t219____Y_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry ConstraintAxis_t219____Y_3_DefaultValue = 
{
	&ConstraintAxis_t219____Y_3_FieldInfo/* field */
	, { (char*)&ConstraintAxis_t219____Y_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ConstraintAxis_t219____Z_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry ConstraintAxis_t219____Z_4_DefaultValue = 
{
	&ConstraintAxis_t219____Z_4_FieldInfo/* field */
	, { (char*)&ConstraintAxis_t219____Z_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* ConstraintAxis_t219_FieldDefaultValues[] = 
{
	&ConstraintAxis_t219____X_2_DefaultValue,
	&ConstraintAxis_t219____Y_3_DefaultValue,
	&ConstraintAxis_t219____Z_4_DefaultValue,
	NULL
};
static MethodInfo* ConstraintAxis_t219_MethodInfos[] =
{
	NULL
};
extern MethodInfo Enum_Equals_m587_MethodInfo;
extern MethodInfo Enum_GetHashCode_m588_MethodInfo;
extern MethodInfo Enum_ToString_m589_MethodInfo;
extern MethodInfo Enum_ToString_m590_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m591_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m592_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m593_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m594_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m595_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m596_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m597_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m598_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m599_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m600_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m601_MethodInfo;
extern MethodInfo Enum_ToString_m602_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m603_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m604_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m605_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m606_MethodInfo;
extern MethodInfo Enum_CompareTo_m607_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m608_MethodInfo;
static MethodInfo* ConstraintAxis_t219_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
extern TypeInfo IFormattable_t182_il2cpp_TypeInfo;
extern TypeInfo IConvertible_t183_il2cpp_TypeInfo;
extern TypeInfo IComparable_t184_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair ConstraintAxis_t219_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType ConstraintAxis_t219_0_0_0;
extern Il2CppType ConstraintAxis_t219_1_0_0;
extern TypeInfo Enum_t185_il2cpp_TypeInfo;
TypeInfo ConstraintAxis_t219_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "ConstraintAxis"/* name */
	, ""/* namespaze */
	, ConstraintAxis_t219_MethodInfos/* methods */
	, NULL/* properties */
	, ConstraintAxis_t219_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ConstraintAxis_t219_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &ConstraintAxis_t219_0_0_0/* byval_arg */
	, &ConstraintAxis_t219_1_0_0/* this_arg */
	, ConstraintAxis_t219_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, ConstraintAxis_t219_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstraintAxis_t219)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// RotationConstraint
#include "AssemblyU2DUnityScript_RotationConstraint.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RotationConstraint_t220_il2cpp_TypeInfo;
// RotationConstraint
#include "AssemblyU2DUnityScript_RotationConstraintMethodDeclarations.h"

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
extern MethodInfo Vector3_get_right_m836_MethodInfo;
extern MethodInfo Vector3_get_forward_m837_MethodInfo;
extern MethodInfo Transform_get_localRotation_m838_MethodInfo;
extern MethodInfo Quaternion_AngleAxis_m839_MethodInfo;
extern MethodInfo Quaternion_op_Multiply_m705_MethodInfo;
extern MethodInfo Quaternion_get_eulerAngles_m840_MethodInfo;
extern MethodInfo Vector3_get_Item_m841_MethodInfo;
extern MethodInfo Quaternion_Angle_m842_MethodInfo;
extern MethodInfo Vector3_set_Item_m843_MethodInfo;
extern MethodInfo Transform_set_localEulerAngles_m844_MethodInfo;


// System.Void RotationConstraint::.ctor()
extern MethodInfo RotationConstraint__ctor_m773_MethodInfo;
 void RotationConstraint__ctor_m773 (RotationConstraint_t220 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// System.Void RotationConstraint::Start()
extern MethodInfo RotationConstraint_Start_m774_MethodInfo;
 void RotationConstraint_Start_m774 (RotationConstraint_t220 * __this, MethodInfo* method){
	int32_t V_0 = {0};
	{
		Transform_t74 * L_0 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		__this->___thisTransform_5 = L_0;
		int32_t L_1 = (__this->___axis_2);
		V_0 = L_1;
		if ((((uint32_t)V_0) != ((uint32_t)0)))
		{
			goto IL_002a;
		}
	}
	{
		Vector3_t73  L_2 = Vector3_get_right_m836(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_right_m836_MethodInfo);
		__this->___rotateAround_6 = L_2;
		goto IL_0058;
	}

IL_002a:
	{
		if ((((uint32_t)V_0) != ((uint32_t)1)))
		{
			goto IL_0041;
		}
	}
	{
		Vector3_t73  L_3 = Vector3_get_up_m826(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_up_m826_MethodInfo);
		__this->___rotateAround_6 = L_3;
		goto IL_0058;
	}

IL_0041:
	{
		if ((((uint32_t)V_0) != ((uint32_t)2)))
		{
			goto IL_0058;
		}
	}
	{
		Vector3_t73  L_4 = Vector3_get_forward_m837(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_forward_m837_MethodInfo);
		__this->___rotateAround_6 = L_4;
		goto IL_0058;
	}

IL_0058:
	{
		Transform_t74 * L_5 = (__this->___thisTransform_5);
		NullCheck(L_5);
		Quaternion_t108  L_6 = Transform_get_localRotation_m838(L_5, /*hidden argument*/&Transform_get_localRotation_m838_MethodInfo);
		float L_7 = (__this->___min_3);
		Vector3_t73  L_8 = (__this->___rotateAround_6);
		Quaternion_t108  L_9 = Quaternion_AngleAxis_m839(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/&Quaternion_AngleAxis_m839_MethodInfo);
		Quaternion_t108  L_10 = Quaternion_op_Multiply_m705(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/&Quaternion_op_Multiply_m705_MethodInfo);
		__this->___minQuaternion_7 = L_10;
		Transform_t74 * L_11 = (__this->___thisTransform_5);
		NullCheck(L_11);
		Quaternion_t108  L_12 = Transform_get_localRotation_m838(L_11, /*hidden argument*/&Transform_get_localRotation_m838_MethodInfo);
		float L_13 = (__this->___max_4);
		Vector3_t73  L_14 = (__this->___rotateAround_6);
		Quaternion_t108  L_15 = Quaternion_AngleAxis_m839(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/&Quaternion_AngleAxis_m839_MethodInfo);
		Quaternion_t108  L_16 = Quaternion_op_Multiply_m705(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/&Quaternion_op_Multiply_m705_MethodInfo);
		__this->___maxQuaternion_8 = L_16;
		float L_17 = (__this->___max_4);
		float L_18 = (__this->___min_3);
		__this->___range_9 = ((float)(L_17-L_18));
		return;
	}
}
// System.Void RotationConstraint::LateUpdate()
extern MethodInfo RotationConstraint_LateUpdate_m775_MethodInfo;
 void RotationConstraint_LateUpdate_m775 (RotationConstraint_t220 * __this, MethodInfo* method){
	Quaternion_t108  V_0 = {0};
	Quaternion_t108  V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t73  V_4 = {0};
	Vector3_t73  V_5 = {0};
	Vector3_t73  V_6 = {0};
	Vector3_t73  V_7 = {0};
	{
		Transform_t74 * L_0 = (__this->___thisTransform_5);
		NullCheck(L_0);
		Quaternion_t108  L_1 = Transform_get_localRotation_m838(L_0, /*hidden argument*/&Transform_get_localRotation_m838_MethodInfo);
		V_0 = L_1;
		Vector3_t73  L_2 = Quaternion_get_eulerAngles_m840((&V_0), /*hidden argument*/&Quaternion_get_eulerAngles_m840_MethodInfo);
		V_5 = L_2;
		int32_t L_3 = (__this->___axis_2);
		float L_4 = Vector3_get_Item_m841((&V_5), L_3, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		Vector3_t73  L_5 = (__this->___rotateAround_6);
		Quaternion_t108  L_6 = Quaternion_AngleAxis_m839(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/&Quaternion_AngleAxis_m839_MethodInfo);
		V_1 = L_6;
		Quaternion_t108  L_7 = (__this->___minQuaternion_7);
		float L_8 = Quaternion_Angle_m842(NULL /*static, unused*/, V_1, L_7, /*hidden argument*/&Quaternion_Angle_m842_MethodInfo);
		V_2 = L_8;
		Quaternion_t108  L_9 = (__this->___maxQuaternion_8);
		float L_10 = Quaternion_Angle_m842(NULL /*static, unused*/, V_1, L_9, /*hidden argument*/&Quaternion_Angle_m842_MethodInfo);
		V_3 = L_10;
		float L_11 = (__this->___range_9);
		if ((((float)V_2) > ((float)L_11)))
		{
			goto IL_0065;
		}
	}
	{
		float L_12 = (__this->___range_9);
		if ((((float)V_3) > ((float)L_12)))
		{
			goto IL_0065;
		}
	}
	{
		goto IL_00d5;
	}

IL_0065:
	{
		Vector3_t73  L_13 = Quaternion_get_eulerAngles_m840((&V_0), /*hidden argument*/&Quaternion_get_eulerAngles_m840_MethodInfo);
		V_4 = L_13;
		if ((((float)V_2) <= ((float)V_3)))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_14 = (__this->___axis_2);
		Quaternion_t108 * L_15 = &(__this->___maxQuaternion_8);
		Vector3_t73  L_16 = Quaternion_get_eulerAngles_m840(L_15, /*hidden argument*/&Quaternion_get_eulerAngles_m840_MethodInfo);
		V_6 = L_16;
		int32_t L_17 = (__this->___axis_2);
		float L_18 = Vector3_get_Item_m841((&V_6), L_17, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		Vector3_set_Item_m843((&V_4), L_14, L_18, /*hidden argument*/&Vector3_set_Item_m843_MethodInfo);
		goto IL_00c8;
	}

IL_00a1:
	{
		int32_t L_19 = (__this->___axis_2);
		Quaternion_t108 * L_20 = &(__this->___minQuaternion_7);
		Vector3_t73  L_21 = Quaternion_get_eulerAngles_m840(L_20, /*hidden argument*/&Quaternion_get_eulerAngles_m840_MethodInfo);
		V_7 = L_21;
		int32_t L_22 = (__this->___axis_2);
		float L_23 = Vector3_get_Item_m841((&V_7), L_22, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		Vector3_set_Item_m843((&V_4), L_19, L_23, /*hidden argument*/&Vector3_set_Item_m843_MethodInfo);
	}

IL_00c8:
	{
		Transform_t74 * L_24 = (__this->___thisTransform_5);
		NullCheck(L_24);
		Transform_set_localEulerAngles_m844(L_24, V_4, /*hidden argument*/&Transform_set_localEulerAngles_m844_MethodInfo);
	}

IL_00d5:
	{
		return;
	}
}
// System.Void RotationConstraint::Main()
extern MethodInfo RotationConstraint_Main_m776_MethodInfo;
 void RotationConstraint_Main_m776 (RotationConstraint_t220 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition RotationConstraint
extern Il2CppType ConstraintAxis_t219_0_0_6;
FieldInfo RotationConstraint_t220____axis_2_FieldInfo = 
{
	"axis"/* name */
	, &ConstraintAxis_t219_0_0_6/* type */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* parent */
	, offsetof(RotationConstraint_t220, ___axis_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo RotationConstraint_t220____min_3_FieldInfo = 
{
	"min"/* name */
	, &Single_t170_0_0_6/* type */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* parent */
	, offsetof(RotationConstraint_t220, ___min_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo RotationConstraint_t220____max_4_FieldInfo = 
{
	"max"/* name */
	, &Single_t170_0_0_6/* type */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* parent */
	, offsetof(RotationConstraint_t220, ___max_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_1;
FieldInfo RotationConstraint_t220____thisTransform_5_FieldInfo = 
{
	"thisTransform"/* name */
	, &Transform_t74_0_0_1/* type */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* parent */
	, offsetof(RotationConstraint_t220, ___thisTransform_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo RotationConstraint_t220____rotateAround_6_FieldInfo = 
{
	"rotateAround"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* parent */
	, offsetof(RotationConstraint_t220, ___rotateAround_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Quaternion_t108_0_0_1;
FieldInfo RotationConstraint_t220____minQuaternion_7_FieldInfo = 
{
	"minQuaternion"/* name */
	, &Quaternion_t108_0_0_1/* type */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* parent */
	, offsetof(RotationConstraint_t220, ___minQuaternion_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Quaternion_t108_0_0_1;
FieldInfo RotationConstraint_t220____maxQuaternion_8_FieldInfo = 
{
	"maxQuaternion"/* name */
	, &Quaternion_t108_0_0_1/* type */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* parent */
	, offsetof(RotationConstraint_t220, ___maxQuaternion_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo RotationConstraint_t220____range_9_FieldInfo = 
{
	"range"/* name */
	, &Single_t170_0_0_1/* type */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* parent */
	, offsetof(RotationConstraint_t220, ___range_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* RotationConstraint_t220_FieldInfos[] =
{
	&RotationConstraint_t220____axis_2_FieldInfo,
	&RotationConstraint_t220____min_3_FieldInfo,
	&RotationConstraint_t220____max_4_FieldInfo,
	&RotationConstraint_t220____thisTransform_5_FieldInfo,
	&RotationConstraint_t220____rotateAround_6_FieldInfo,
	&RotationConstraint_t220____minQuaternion_7_FieldInfo,
	&RotationConstraint_t220____maxQuaternion_8_FieldInfo,
	&RotationConstraint_t220____range_9_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void RotationConstraint::.ctor()
MethodInfo RotationConstraint__ctor_m773_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RotationConstraint__ctor_m773/* method */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void RotationConstraint::Start()
MethodInfo RotationConstraint_Start_m774_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&RotationConstraint_Start_m774/* method */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void RotationConstraint::LateUpdate()
MethodInfo RotationConstraint_LateUpdate_m775_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&RotationConstraint_LateUpdate_m775/* method */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void RotationConstraint::Main()
MethodInfo RotationConstraint_Main_m776_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&RotationConstraint_Main_m776/* method */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RotationConstraint_t220_MethodInfos[] =
{
	&RotationConstraint__ctor_m773_MethodInfo,
	&RotationConstraint_Start_m774_MethodInfo,
	&RotationConstraint_LateUpdate_m775_MethodInfo,
	&RotationConstraint_Main_m776_MethodInfo,
	NULL
};
static MethodInfo* RotationConstraint_t220_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&RotationConstraint_Start_m774_MethodInfo,
	&RotationConstraint_LateUpdate_m775_MethodInfo,
	&RotationConstraint_Main_m776_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType RotationConstraint_t220_0_0_0;
extern Il2CppType RotationConstraint_t220_1_0_0;
struct RotationConstraint_t220;
TypeInfo RotationConstraint_t220_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "RotationConstraint"/* name */
	, ""/* namespaze */
	, RotationConstraint_t220_MethodInfos/* methods */
	, NULL/* properties */
	, RotationConstraint_t220_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, RotationConstraint_t220_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &RotationConstraint_t220_il2cpp_TypeInfo/* cast_class */
	, &RotationConstraint_t220_0_0_0/* byval_arg */
	, &RotationConstraint_t220_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RotationConstraint_t220)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SidescrollControl
#include "AssemblyU2DUnityScript_SidescrollControl.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SidescrollControl_t221_il2cpp_TypeInfo;
// SidescrollControl
#include "AssemblyU2DUnityScript_SidescrollControlMethodDeclarations.h"



// System.Void SidescrollControl::.ctor()
extern MethodInfo SidescrollControl__ctor_m777_MethodInfo;
 void SidescrollControl__ctor_m777 (SidescrollControl_t221 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		__this->___forwardSpeed_4 = (((float)4));
		__this->___backwardSpeed_5 = (((float)4));
		__this->___jumpSpeed_6 = (((float)((int32_t)16)));
		__this->___inAirMultiplier_7 = (0.25f);
		__this->___canJump_11 = 1;
		return;
	}
}
// System.Void SidescrollControl::Start()
extern MethodInfo SidescrollControl_Start_m778_MethodInfo;
 void SidescrollControl_Start_m778 (SidescrollControl_t221 * __this, MethodInfo* method){
	GameObject_t29 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&Transform_t74_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Component_t128 * L_1 = Component_GetComponent_m800(__this, L_0, /*hidden argument*/&Component_GetComponent_m800_MethodInfo);
		__this->___thisTransform_8 = ((Transform_t74 *)Castclass(L_1, InitializedTypeInfo(&Transform_t74_il2cpp_TypeInfo)));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&CharacterController_t209_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Component_t128 * L_3 = Component_GetComponent_m800(__this, L_2, /*hidden argument*/&Component_GetComponent_m800_MethodInfo);
		__this->___character_9 = ((CharacterController_t209 *)Castclass(L_3, InitializedTypeInfo(&CharacterController_t209_il2cpp_TypeInfo)));
		GameObject_t29 * L_4 = GameObject_Find_m801(NULL /*static, unused*/, (String_t*) &_stringLiteral81, /*hidden argument*/&GameObject_Find_m801_MethodInfo);
		V_0 = L_4;
		bool L_5 = Object_op_Implicit_m257(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_5)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t74 * L_6 = (__this->___thisTransform_8);
		NullCheck(V_0);
		Transform_t74 * L_7 = GameObject_get_transform_m537(V_0, /*hidden argument*/&GameObject_get_transform_m537_MethodInfo);
		NullCheck(L_7);
		Vector3_t73  L_8 = Transform_get_position_m538(L_7, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		NullCheck(L_6);
		Transform_set_position_m570(L_6, L_8, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
	}

IL_0062:
	{
		return;
	}
}
// System.Void SidescrollControl::OnEndGame()
extern MethodInfo SidescrollControl_OnEndGame_m779_MethodInfo;
 void SidescrollControl_OnEndGame_m779 (SidescrollControl_t221 * __this, MethodInfo* method){
	{
		Joystick_t208 * L_0 = (__this->___moveTouchPad_2);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(&Joystick_Disable_m753_MethodInfo, L_0);
		Joystick_t208 * L_1 = (__this->___jumpTouchPad_3);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(&Joystick_Disable_m753_MethodInfo, L_1);
		Behaviour_set_enabled_m547(__this, 0, /*hidden argument*/&Behaviour_set_enabled_m547_MethodInfo);
		return;
	}
}
// System.Void SidescrollControl::Update()
extern MethodInfo SidescrollControl_Update_m780_MethodInfo;
 void SidescrollControl_Update_m780 (SidescrollControl_t221 * __this, MethodInfo* method){
	Vector3_t73  V_0 = {0};
	bool V_1 = false;
	Joystick_t208 * V_2 = {0};
	Vector3_t73  V_3 = {0};
	{
		Vector3_t73  L_0 = Vector3_get_zero_m807(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m807_MethodInfo);
		V_0 = L_0;
		Joystick_t208 * L_1 = (__this->___moveTouchPad_2);
		NullCheck(L_1);
		Vector2_t99 * L_2 = &(L_1->___position_9);
		NullCheck(L_2);
		float L_3 = (L_2->___x_1);
		if ((((float)L_3) <= ((float)(((float)0)))))
		{
			goto IL_0048;
		}
	}
	{
		Vector3_t73  L_4 = Vector3_get_right_m836(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_right_m836_MethodInfo);
		float L_5 = (__this->___forwardSpeed_4);
		Vector3_t73  L_6 = Vector3_op_Multiply_m573(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		Joystick_t208 * L_7 = (__this->___moveTouchPad_2);
		NullCheck(L_7);
		Vector2_t99 * L_8 = &(L_7->___position_9);
		NullCheck(L_8);
		float L_9 = (L_8->___x_1);
		Vector3_t73  L_10 = Vector3_op_Multiply_m573(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_10;
		goto IL_006e;
	}

IL_0048:
	{
		Vector3_t73  L_11 = Vector3_get_right_m836(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_right_m836_MethodInfo);
		float L_12 = (__this->___backwardSpeed_5);
		Vector3_t73  L_13 = Vector3_op_Multiply_m573(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		Joystick_t208 * L_14 = (__this->___moveTouchPad_2);
		NullCheck(L_14);
		Vector2_t99 * L_15 = &(L_14->___position_9);
		NullCheck(L_15);
		float L_16 = (L_15->___x_1);
		Vector3_t73  L_17 = Vector3_op_Multiply_m573(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_17;
	}

IL_006e:
	{
		CharacterController_t209 * L_18 = (__this->___character_9);
		NullCheck(L_18);
		bool L_19 = CharacterController_get_isGrounded_m804(L_18, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_19)
		{
			goto IL_00e5;
		}
	}
	{
		V_1 = 0;
		Joystick_t208 * L_20 = (__this->___jumpTouchPad_3);
		V_2 = L_20;
		NullCheck(V_2);
		bool L_21 = (bool)VirtFuncInvoker0< bool >::Invoke(&Joystick_IsFingerDown_m755_MethodInfo, V_2);
		if (L_21)
		{
			goto IL_0099;
		}
	}
	{
		__this->___canJump_11 = 1;
	}

IL_0099:
	{
		bool L_22 = (__this->___canJump_11);
		if (!L_22)
		{
			goto IL_00b8;
		}
	}
	{
		NullCheck(V_2);
		bool L_23 = (bool)VirtFuncInvoker0< bool >::Invoke(&Joystick_IsFingerDown_m755_MethodInfo, V_2);
		if (!L_23)
		{
			goto IL_00b8;
		}
	}
	{
		V_1 = 1;
		__this->___canJump_11 = 0;
	}

IL_00b8:
	{
		if (!V_1)
		{
			goto IL_00e0;
		}
	}
	{
		CharacterController_t209 * L_24 = (__this->___character_9);
		NullCheck(L_24);
		Vector3_t73  L_25 = CharacterController_get_velocity_m802(L_24, /*hidden argument*/&CharacterController_get_velocity_m802_MethodInfo);
		__this->___velocity_10 = L_25;
		Vector3_t73 * L_26 = &(__this->___velocity_10);
		float L_27 = (__this->___jumpSpeed_6);
		NullCheck(L_26);
		L_26->___y_2 = L_27;
	}

IL_00e0:
	{
		goto IL_0124;
	}

IL_00e5:
	{
		Vector3_t73 * L_28 = &(__this->___velocity_10);
		Vector3_t73 * L_29 = &(__this->___velocity_10);
		NullCheck(L_29);
		float L_30 = (L_29->___y_2);
		Vector3_t73  L_31 = Physics_get_gravity_m805(NULL /*static, unused*/, /*hidden argument*/&Physics_get_gravity_m805_MethodInfo);
		V_3 = L_31;
		NullCheck((&V_3));
		float L_32 = ((&V_3)->___y_2);
		float L_33 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		NullCheck(L_28);
		L_28->___y_2 = ((float)(L_30+((float)((float)L_32*(float)L_33))));
		NullCheck((&V_0));
		float L_34 = ((&V_0)->___x_1);
		float L_35 = (__this->___inAirMultiplier_7);
		NullCheck((&V_0));
		(&V_0)->___x_1 = ((float)((float)L_34*(float)L_35));
	}

IL_0124:
	{
		Vector3_t73  L_36 = (__this->___velocity_10);
		Vector3_t73  L_37 = Vector3_op_Addition_m576(NULL /*static, unused*/, V_0, L_36, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_0 = L_37;
		Vector3_t73  L_38 = Physics_get_gravity_m805(NULL /*static, unused*/, /*hidden argument*/&Physics_get_gravity_m805_MethodInfo);
		Vector3_t73  L_39 = Vector3_op_Addition_m576(NULL /*static, unused*/, V_0, L_38, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_0 = L_39;
		float L_40 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		Vector3_t73  L_41 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_0, L_40, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_0 = L_41;
		CharacterController_t209 * L_42 = (__this->___character_9);
		NullCheck(L_42);
		CharacterController_Move_m806(L_42, V_0, /*hidden argument*/&CharacterController_Move_m806_MethodInfo);
		CharacterController_t209 * L_43 = (__this->___character_9);
		NullCheck(L_43);
		bool L_44 = CharacterController_get_isGrounded_m804(L_43, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_44)
		{
			goto IL_0171;
		}
	}
	{
		Vector3_t73  L_45 = Vector3_get_zero_m807(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m807_MethodInfo);
		__this->___velocity_10 = L_45;
	}

IL_0171:
	{
		return;
	}
}
// System.Void SidescrollControl::Main()
extern MethodInfo SidescrollControl_Main_m781_MethodInfo;
 void SidescrollControl_Main_m781 (SidescrollControl_t221 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition SidescrollControl
extern Il2CppType Joystick_t208_0_0_6;
FieldInfo SidescrollControl_t221____moveTouchPad_2_FieldInfo = 
{
	"moveTouchPad"/* name */
	, &Joystick_t208_0_0_6/* type */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* parent */
	, offsetof(SidescrollControl_t221, ___moveTouchPad_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Joystick_t208_0_0_6;
FieldInfo SidescrollControl_t221____jumpTouchPad_3_FieldInfo = 
{
	"jumpTouchPad"/* name */
	, &Joystick_t208_0_0_6/* type */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* parent */
	, offsetof(SidescrollControl_t221, ___jumpTouchPad_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo SidescrollControl_t221____forwardSpeed_4_FieldInfo = 
{
	"forwardSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* parent */
	, offsetof(SidescrollControl_t221, ___forwardSpeed_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo SidescrollControl_t221____backwardSpeed_5_FieldInfo = 
{
	"backwardSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* parent */
	, offsetof(SidescrollControl_t221, ___backwardSpeed_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo SidescrollControl_t221____jumpSpeed_6_FieldInfo = 
{
	"jumpSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* parent */
	, offsetof(SidescrollControl_t221, ___jumpSpeed_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo SidescrollControl_t221____inAirMultiplier_7_FieldInfo = 
{
	"inAirMultiplier"/* name */
	, &Single_t170_0_0_6/* type */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* parent */
	, offsetof(SidescrollControl_t221, ___inAirMultiplier_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_1;
FieldInfo SidescrollControl_t221____thisTransform_8_FieldInfo = 
{
	"thisTransform"/* name */
	, &Transform_t74_0_0_1/* type */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* parent */
	, offsetof(SidescrollControl_t221, ___thisTransform_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType CharacterController_t209_0_0_1;
FieldInfo SidescrollControl_t221____character_9_FieldInfo = 
{
	"character"/* name */
	, &CharacterController_t209_0_0_1/* type */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* parent */
	, offsetof(SidescrollControl_t221, ___character_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo SidescrollControl_t221____velocity_10_FieldInfo = 
{
	"velocity"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* parent */
	, offsetof(SidescrollControl_t221, ___velocity_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo SidescrollControl_t221____canJump_11_FieldInfo = 
{
	"canJump"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* parent */
	, offsetof(SidescrollControl_t221, ___canJump_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SidescrollControl_t221_FieldInfos[] =
{
	&SidescrollControl_t221____moveTouchPad_2_FieldInfo,
	&SidescrollControl_t221____jumpTouchPad_3_FieldInfo,
	&SidescrollControl_t221____forwardSpeed_4_FieldInfo,
	&SidescrollControl_t221____backwardSpeed_5_FieldInfo,
	&SidescrollControl_t221____jumpSpeed_6_FieldInfo,
	&SidescrollControl_t221____inAirMultiplier_7_FieldInfo,
	&SidescrollControl_t221____thisTransform_8_FieldInfo,
	&SidescrollControl_t221____character_9_FieldInfo,
	&SidescrollControl_t221____velocity_10_FieldInfo,
	&SidescrollControl_t221____canJump_11_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void SidescrollControl::.ctor()
MethodInfo SidescrollControl__ctor_m777_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SidescrollControl__ctor_m777/* method */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void SidescrollControl::Start()
MethodInfo SidescrollControl_Start_m778_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SidescrollControl_Start_m778/* method */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void SidescrollControl::OnEndGame()
MethodInfo SidescrollControl_OnEndGame_m779_MethodInfo = 
{
	"OnEndGame"/* name */
	, (methodPointerType)&SidescrollControl_OnEndGame_m779/* method */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void SidescrollControl::Update()
MethodInfo SidescrollControl_Update_m780_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SidescrollControl_Update_m780/* method */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void SidescrollControl::Main()
MethodInfo SidescrollControl_Main_m781_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&SidescrollControl_Main_m781/* method */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SidescrollControl_t221_MethodInfos[] =
{
	&SidescrollControl__ctor_m777_MethodInfo,
	&SidescrollControl_Start_m778_MethodInfo,
	&SidescrollControl_OnEndGame_m779_MethodInfo,
	&SidescrollControl_Update_m780_MethodInfo,
	&SidescrollControl_Main_m781_MethodInfo,
	NULL
};
static MethodInfo* SidescrollControl_t221_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&SidescrollControl_Start_m778_MethodInfo,
	&SidescrollControl_OnEndGame_m779_MethodInfo,
	&SidescrollControl_Update_m780_MethodInfo,
	&SidescrollControl_Main_m781_MethodInfo,
};
void SidescrollControl_t221_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t154 * tmp;
		tmp = (RequireComponent_t154 *)il2cpp_codegen_object_new (&RequireComponent_t154_il2cpp_TypeInfo);
		RequireComponent__ctor_m348(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&CharacterController_t209_il2cpp_TypeInfo)), &RequireComponent__ctor_m348_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache SidescrollControl_t221__CustomAttributeCache = {
1,
NULL,
&SidescrollControl_t221_CustomAttributesCacheGenerator
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType SidescrollControl_t221_0_0_0;
extern Il2CppType SidescrollControl_t221_1_0_0;
struct SidescrollControl_t221;
extern CustomAttributesCache SidescrollControl_t221__CustomAttributeCache;
TypeInfo SidescrollControl_t221_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "SidescrollControl"/* name */
	, ""/* namespaze */
	, SidescrollControl_t221_MethodInfos/* methods */
	, NULL/* properties */
	, SidescrollControl_t221_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SidescrollControl_t221_VTable/* vtable */
	, &SidescrollControl_t221__CustomAttributeCache/* custom_attributes_cache */
	, &SidescrollControl_t221_il2cpp_TypeInfo/* cast_class */
	, &SidescrollControl_t221_0_0_0/* byval_arg */
	, &SidescrollControl_t221_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SidescrollControl_t221)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SmoothFollow2D
#include "AssemblyU2DUnityScript_SmoothFollow2D.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SmoothFollow2D_t222_il2cpp_TypeInfo;
// SmoothFollow2D
#include "AssemblyU2DUnityScript_SmoothFollow2DMethodDeclarations.h"



// System.Void SmoothFollow2D::.ctor()
extern MethodInfo SmoothFollow2D__ctor_m782_MethodInfo;
 void SmoothFollow2D__ctor_m782 (SmoothFollow2D_t222 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		__this->___smoothTime_3 = (0.3f);
		return;
	}
}
// System.Void SmoothFollow2D::Start()
extern MethodInfo SmoothFollow2D_Start_m783_MethodInfo;
 void SmoothFollow2D_Start_m783 (SmoothFollow2D_t222 * __this, MethodInfo* method){
	{
		Transform_t74 * L_0 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		__this->___thisTransform_4 = L_0;
		return;
	}
}
// System.Void SmoothFollow2D::Update()
extern MethodInfo SmoothFollow2D_Update_m784_MethodInfo;
 void SmoothFollow2D_Update_m784 (SmoothFollow2D_t222 * __this, MethodInfo* method){
	float V_0 = 0.0f;
	Vector3_t73  V_1 = {0};
	float V_2 = 0.0f;
	Vector3_t73  V_3 = {0};
	Vector3_t73  V_4 = {0};
	Vector3_t73  V_5 = {0};
	float V_6 = 0.0f;
	Vector3_t73  V_7 = {0};
	Vector3_t73  V_8 = {0};
	Vector3_t73  V_9 = {0};
	float V_10 = 0.0f;
	Vector3_t73  V_11 = {0};
	{
		Transform_t74 * L_0 = (__this->___thisTransform_4);
		NullCheck(L_0);
		Vector3_t73  L_1 = Transform_get_position_m538(L_0, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		V_4 = L_1;
		NullCheck((&V_4));
		float L_2 = ((&V_4)->___x_1);
		Transform_t74 * L_3 = (__this->___target_2);
		NullCheck(L_3);
		Vector3_t73  L_4 = Transform_get_position_m538(L_3, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		V_5 = L_4;
		NullCheck((&V_5));
		float L_5 = ((&V_5)->___x_1);
		Vector2_t99 * L_6 = &(__this->___velocity_5);
		NullCheck(L_6);
		float* L_7 = &(L_6->___x_1);
		float L_8 = (__this->___smoothTime_3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_9 = Mathf_SmoothDamp_m831(NULL /*static, unused*/, L_2, L_5, L_7, L_8, /*hidden argument*/&Mathf_SmoothDamp_m831_MethodInfo);
		float L_10 = L_9;
		V_0 = L_10;
		Transform_t74 * L_11 = (__this->___thisTransform_4);
		NullCheck(L_11);
		Vector3_t73  L_12 = Transform_get_position_m538(L_11, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		Vector3_t73  L_13 = L_12;
		V_1 = L_13;
		float L_14 = V_0;
		V_6 = L_14;
		NullCheck((&V_1));
		(&V_1)->___x_1 = L_14;
		Transform_t74 * L_15 = (__this->___thisTransform_4);
		Vector3_t73  L_16 = V_1;
		V_7 = L_16;
		NullCheck(L_15);
		Transform_set_position_m570(L_15, L_16, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
		Transform_t74 * L_17 = (__this->___thisTransform_4);
		NullCheck(L_17);
		Vector3_t73  L_18 = Transform_get_position_m538(L_17, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		V_8 = L_18;
		NullCheck((&V_8));
		float L_19 = ((&V_8)->___y_2);
		Transform_t74 * L_20 = (__this->___target_2);
		NullCheck(L_20);
		Vector3_t73  L_21 = Transform_get_position_m538(L_20, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		V_9 = L_21;
		NullCheck((&V_9));
		float L_22 = ((&V_9)->___y_2);
		Vector2_t99 * L_23 = &(__this->___velocity_5);
		NullCheck(L_23);
		float* L_24 = &(L_23->___y_2);
		float L_25 = (__this->___smoothTime_3);
		float L_26 = Mathf_SmoothDamp_m831(NULL /*static, unused*/, L_19, L_22, L_24, L_25, /*hidden argument*/&Mathf_SmoothDamp_m831_MethodInfo);
		float L_27 = L_26;
		V_2 = L_27;
		Transform_t74 * L_28 = (__this->___thisTransform_4);
		NullCheck(L_28);
		Vector3_t73  L_29 = Transform_get_position_m538(L_28, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		Vector3_t73  L_30 = L_29;
		V_3 = L_30;
		float L_31 = V_2;
		V_10 = L_31;
		NullCheck((&V_3));
		(&V_3)->___y_2 = L_31;
		Transform_t74 * L_32 = (__this->___thisTransform_4);
		Vector3_t73  L_33 = V_3;
		V_11 = L_33;
		NullCheck(L_32);
		Transform_set_position_m570(L_32, L_33, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
		return;
	}
}
// System.Void SmoothFollow2D::Main()
extern MethodInfo SmoothFollow2D_Main_m785_MethodInfo;
 void SmoothFollow2D_Main_m785 (SmoothFollow2D_t222 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition SmoothFollow2D
extern Il2CppType Transform_t74_0_0_6;
FieldInfo SmoothFollow2D_t222____target_2_FieldInfo = 
{
	"target"/* name */
	, &Transform_t74_0_0_6/* type */
	, &SmoothFollow2D_t222_il2cpp_TypeInfo/* parent */
	, offsetof(SmoothFollow2D_t222, ___target_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo SmoothFollow2D_t222____smoothTime_3_FieldInfo = 
{
	"smoothTime"/* name */
	, &Single_t170_0_0_6/* type */
	, &SmoothFollow2D_t222_il2cpp_TypeInfo/* parent */
	, offsetof(SmoothFollow2D_t222, ___smoothTime_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_1;
FieldInfo SmoothFollow2D_t222____thisTransform_4_FieldInfo = 
{
	"thisTransform"/* name */
	, &Transform_t74_0_0_1/* type */
	, &SmoothFollow2D_t222_il2cpp_TypeInfo/* parent */
	, offsetof(SmoothFollow2D_t222, ___thisTransform_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_1;
FieldInfo SmoothFollow2D_t222____velocity_5_FieldInfo = 
{
	"velocity"/* name */
	, &Vector2_t99_0_0_1/* type */
	, &SmoothFollow2D_t222_il2cpp_TypeInfo/* parent */
	, offsetof(SmoothFollow2D_t222, ___velocity_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SmoothFollow2D_t222_FieldInfos[] =
{
	&SmoothFollow2D_t222____target_2_FieldInfo,
	&SmoothFollow2D_t222____smoothTime_3_FieldInfo,
	&SmoothFollow2D_t222____thisTransform_4_FieldInfo,
	&SmoothFollow2D_t222____velocity_5_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void SmoothFollow2D::.ctor()
MethodInfo SmoothFollow2D__ctor_m782_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SmoothFollow2D__ctor_m782/* method */
	, &SmoothFollow2D_t222_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void SmoothFollow2D::Start()
MethodInfo SmoothFollow2D_Start_m783_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SmoothFollow2D_Start_m783/* method */
	, &SmoothFollow2D_t222_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void SmoothFollow2D::Update()
MethodInfo SmoothFollow2D_Update_m784_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&SmoothFollow2D_Update_m784/* method */
	, &SmoothFollow2D_t222_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void SmoothFollow2D::Main()
MethodInfo SmoothFollow2D_Main_m785_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&SmoothFollow2D_Main_m785/* method */
	, &SmoothFollow2D_t222_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SmoothFollow2D_t222_MethodInfos[] =
{
	&SmoothFollow2D__ctor_m782_MethodInfo,
	&SmoothFollow2D_Start_m783_MethodInfo,
	&SmoothFollow2D_Update_m784_MethodInfo,
	&SmoothFollow2D_Main_m785_MethodInfo,
	NULL
};
static MethodInfo* SmoothFollow2D_t222_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&SmoothFollow2D_Start_m783_MethodInfo,
	&SmoothFollow2D_Update_m784_MethodInfo,
	&SmoothFollow2D_Main_m785_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType SmoothFollow2D_t222_0_0_0;
extern Il2CppType SmoothFollow2D_t222_1_0_0;
struct SmoothFollow2D_t222;
TypeInfo SmoothFollow2D_t222_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "SmoothFollow2D"/* name */
	, ""/* namespaze */
	, SmoothFollow2D_t222_MethodInfos/* methods */
	, NULL/* properties */
	, SmoothFollow2D_t222_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SmoothFollow2D_t222_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SmoothFollow2D_t222_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SmoothFollow2D_t222_il2cpp_TypeInfo/* cast_class */
	, &SmoothFollow2D_t222_0_0_0/* byval_arg */
	, &SmoothFollow2D_t222_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SmoothFollow2D_t222)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// ControlState
#include "AssemblyU2DUnityScript_ControlState.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ControlState_t223_il2cpp_TypeInfo;
// ControlState
#include "AssemblyU2DUnityScript_ControlStateMethodDeclarations.h"



// Metadata Definition ControlState
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo ControlState_t223____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &ControlState_t223_il2cpp_TypeInfo/* parent */
	, offsetof(ControlState_t223, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ControlState_t223_0_0_32854;
FieldInfo ControlState_t223____WaitingForFirstTouch_2_FieldInfo = 
{
	"WaitingForFirstTouch"/* name */
	, &ControlState_t223_0_0_32854/* type */
	, &ControlState_t223_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ControlState_t223_0_0_32854;
FieldInfo ControlState_t223____WaitingForSecondTouch_3_FieldInfo = 
{
	"WaitingForSecondTouch"/* name */
	, &ControlState_t223_0_0_32854/* type */
	, &ControlState_t223_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ControlState_t223_0_0_32854;
FieldInfo ControlState_t223____MovingCharacter_4_FieldInfo = 
{
	"MovingCharacter"/* name */
	, &ControlState_t223_0_0_32854/* type */
	, &ControlState_t223_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ControlState_t223_0_0_32854;
FieldInfo ControlState_t223____WaitingForMovement_5_FieldInfo = 
{
	"WaitingForMovement"/* name */
	, &ControlState_t223_0_0_32854/* type */
	, &ControlState_t223_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ControlState_t223_0_0_32854;
FieldInfo ControlState_t223____ZoomingCamera_6_FieldInfo = 
{
	"ZoomingCamera"/* name */
	, &ControlState_t223_0_0_32854/* type */
	, &ControlState_t223_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ControlState_t223_0_0_32854;
FieldInfo ControlState_t223____RotatingCamera_7_FieldInfo = 
{
	"RotatingCamera"/* name */
	, &ControlState_t223_0_0_32854/* type */
	, &ControlState_t223_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ControlState_t223_0_0_32854;
FieldInfo ControlState_t223____WaitingForNoFingers_8_FieldInfo = 
{
	"WaitingForNoFingers"/* name */
	, &ControlState_t223_0_0_32854/* type */
	, &ControlState_t223_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* ControlState_t223_FieldInfos[] =
{
	&ControlState_t223____value___1_FieldInfo,
	&ControlState_t223____WaitingForFirstTouch_2_FieldInfo,
	&ControlState_t223____WaitingForSecondTouch_3_FieldInfo,
	&ControlState_t223____MovingCharacter_4_FieldInfo,
	&ControlState_t223____WaitingForMovement_5_FieldInfo,
	&ControlState_t223____ZoomingCamera_6_FieldInfo,
	&ControlState_t223____RotatingCamera_7_FieldInfo,
	&ControlState_t223____WaitingForNoFingers_8_FieldInfo,
	NULL
};
static const int32_t ControlState_t223____WaitingForFirstTouch_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry ControlState_t223____WaitingForFirstTouch_2_DefaultValue = 
{
	&ControlState_t223____WaitingForFirstTouch_2_FieldInfo/* field */
	, { (char*)&ControlState_t223____WaitingForFirstTouch_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ControlState_t223____WaitingForSecondTouch_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry ControlState_t223____WaitingForSecondTouch_3_DefaultValue = 
{
	&ControlState_t223____WaitingForSecondTouch_3_FieldInfo/* field */
	, { (char*)&ControlState_t223____WaitingForSecondTouch_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ControlState_t223____MovingCharacter_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry ControlState_t223____MovingCharacter_4_DefaultValue = 
{
	&ControlState_t223____MovingCharacter_4_FieldInfo/* field */
	, { (char*)&ControlState_t223____MovingCharacter_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ControlState_t223____WaitingForMovement_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry ControlState_t223____WaitingForMovement_5_DefaultValue = 
{
	&ControlState_t223____WaitingForMovement_5_FieldInfo/* field */
	, { (char*)&ControlState_t223____WaitingForMovement_5_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ControlState_t223____ZoomingCamera_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry ControlState_t223____ZoomingCamera_6_DefaultValue = 
{
	&ControlState_t223____ZoomingCamera_6_FieldInfo/* field */
	, { (char*)&ControlState_t223____ZoomingCamera_6_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ControlState_t223____RotatingCamera_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry ControlState_t223____RotatingCamera_7_DefaultValue = 
{
	&ControlState_t223____RotatingCamera_7_FieldInfo/* field */
	, { (char*)&ControlState_t223____RotatingCamera_7_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t ControlState_t223____WaitingForNoFingers_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry ControlState_t223____WaitingForNoFingers_8_DefaultValue = 
{
	&ControlState_t223____WaitingForNoFingers_8_FieldInfo/* field */
	, { (char*)&ControlState_t223____WaitingForNoFingers_8_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* ControlState_t223_FieldDefaultValues[] = 
{
	&ControlState_t223____WaitingForFirstTouch_2_DefaultValue,
	&ControlState_t223____WaitingForSecondTouch_3_DefaultValue,
	&ControlState_t223____MovingCharacter_4_DefaultValue,
	&ControlState_t223____WaitingForMovement_5_DefaultValue,
	&ControlState_t223____ZoomingCamera_6_DefaultValue,
	&ControlState_t223____RotatingCamera_7_DefaultValue,
	&ControlState_t223____WaitingForNoFingers_8_DefaultValue,
	NULL
};
static MethodInfo* ControlState_t223_MethodInfos[] =
{
	NULL
};
static MethodInfo* ControlState_t223_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair ControlState_t223_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType ControlState_t223_0_0_0;
extern Il2CppType ControlState_t223_1_0_0;
TypeInfo ControlState_t223_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "ControlState"/* name */
	, ""/* namespaze */
	, ControlState_t223_MethodInfos/* methods */
	, NULL/* properties */
	, ControlState_t223_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ControlState_t223_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &ControlState_t223_0_0_0/* byval_arg */
	, &ControlState_t223_1_0_0/* this_arg */
	, ControlState_t223_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, ControlState_t223_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ControlState_t223)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// TapControl
#include "AssemblyU2DUnityScript_TapControl.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TapControl_t226_il2cpp_TypeInfo;
// TapControl
#include "AssemblyU2DUnityScript_TapControlMethodDeclarations.h"

#include "mscorlib_ArrayTypes.h"
// ZoomCamera
#include "AssemblyU2DUnityScript_ZoomCamera.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
extern TypeInfo Int32U5BU5D_t175_il2cpp_TypeInfo;
extern TypeInfo Vector2U5BU5D_t225_il2cpp_TypeInfo;
extern TypeInfo ZoomCamera_t224_il2cpp_TypeInfo;
extern TypeInfo RaycastHit_t228_il2cpp_TypeInfo;
extern TypeInfo Touch_t201_il2cpp_TypeInfo;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
extern Il2CppType ZoomCamera_t224_0_0_0;
extern MethodInfo GameObject_GetComponent_m845_MethodInfo;
extern MethodInfo GameObject_GetComponent_TisCamera_t168_m846_MethodInfo;
extern MethodInfo TapControl_ResetControlState_m792_MethodInfo;
extern MethodInfo Vector2_get_magnitude_m847_MethodInfo;
extern MethodInfo Vector2_op_Division_m848_MethodInfo;
extern MethodInfo Touch_get_deltaPosition_m849_MethodInfo;
extern MethodInfo Vector2_Dot_m850_MethodInfo;
extern MethodInfo Vector3__ctor_m851_MethodInfo;
extern MethodInfo Vector3_Cross_m852_MethodInfo;
extern MethodInfo Mathf_Acos_m583_MethodInfo;
extern MethodInfo Camera_ScreenPointToRay_m853_MethodInfo;
extern MethodInfo Physics_Raycast_m854_MethodInfo;
extern MethodInfo RaycastHit_get_point_m855_MethodInfo;
extern MethodInfo TapControl_FaceMovementDirection_m789_MethodInfo;
extern MethodInfo Time_get_frameCount_m856_MethodInfo;
extern MethodInfo TapControl_CameraControl_m790_MethodInfo;
extern MethodInfo TapControl_CharacterControl_m791_MethodInfo;
extern MethodInfo Transform_get_eulerAngles_m712_MethodInfo;
extern MethodInfo Mathf_SmoothDampAngle_m857_MethodInfo;
extern MethodInfo Transform_set_eulerAngles_m713_MethodInfo;
struct GameObject_t29;
struct GameObject_t29;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
 Object_t * GameObject_GetComponent_TisObject_t_m564_gshared (GameObject_t29 * __this, MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m564(__this, method) (Object_t *)GameObject_GetComponent_TisObject_t_m564_gshared((GameObject_t29 *)__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t168_m846(__this, method) (Camera_t168 *)GameObject_GetComponent_TisObject_t_m564_gshared((GameObject_t29 *)__this, method)


// System.Void TapControl::.ctor()
extern MethodInfo TapControl__ctor_m786_MethodInfo;
 void TapControl__ctor_m786 (TapControl_t226 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		__this->___inAirMultiplier_7 = (0.25f);
		__this->___minimumDistanceToMove_8 = (1.0f);
		__this->___minimumTimeUntilMove_9 = (0.25f);
		__this->___rotateEpsilon_14 = (((float)1));
		__this->___state_24 = 0;
		__this->___fingerDown_25 = ((Int32U5BU5D_t175*)SZArrayNew(InitializedTypeInfo(&Int32U5BU5D_t175_il2cpp_TypeInfo), 2));
		__this->___fingerDownPosition_26 = ((Vector2U5BU5D_t225*)SZArrayNew(InitializedTypeInfo(&Vector2U5BU5D_t225_il2cpp_TypeInfo), 2));
		__this->___fingerDownFrame_27 = ((Int32U5BU5D_t175*)SZArrayNew(InitializedTypeInfo(&Int32U5BU5D_t175_il2cpp_TypeInfo), 2));
		return;
	}
}
// System.Void TapControl::Start()
extern MethodInfo TapControl_Start_m787_MethodInfo;
 void TapControl_Start_m787 (TapControl_t226 * __this, MethodInfo* method){
	GameObject_t29 * V_0 = {0};
	{
		Transform_t74 * L_0 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		__this->___thisTransform_17 = L_0;
		GameObject_t29 * L_1 = (__this->___cameraObject_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ZoomCamera_t224_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_1);
		Component_t128 * L_3 = GameObject_GetComponent_m845(L_1, L_2, /*hidden argument*/&GameObject_GetComponent_m845_MethodInfo);
		__this->___zoomCamera_15 = ((ZoomCamera_t224 *)Castclass(L_3, InitializedTypeInfo(&ZoomCamera_t224_il2cpp_TypeInfo)));
		GameObject_t29 * L_4 = (__this->___cameraObject_2);
		NullCheck(L_4);
		Camera_t168 * L_5 = GameObject_GetComponent_TisCamera_t168_m846(L_4, /*hidden argument*/&GameObject_GetComponent_TisCamera_t168_m846_MethodInfo);
		__this->___cam_16 = L_5;
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&CharacterController_t209_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Component_t128 * L_7 = Component_GetComponent_m800(__this, L_6, /*hidden argument*/&Component_GetComponent_m800_MethodInfo);
		__this->___character_18 = ((CharacterController_t209 *)Castclass(L_7, InitializedTypeInfo(&CharacterController_t209_il2cpp_TypeInfo)));
		VirtActionInvoker0::Invoke(&TapControl_ResetControlState_m792_MethodInfo, __this);
		GameObject_t29 * L_8 = GameObject_Find_m801(NULL /*static, unused*/, (String_t*) &_stringLiteral81, /*hidden argument*/&GameObject_Find_m801_MethodInfo);
		V_0 = L_8;
		bool L_9 = Object_op_Implicit_m257(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_9)
		{
			goto IL_008a;
		}
	}
	{
		Transform_t74 * L_10 = (__this->___thisTransform_17);
		NullCheck(V_0);
		Transform_t74 * L_11 = GameObject_get_transform_m537(V_0, /*hidden argument*/&GameObject_get_transform_m537_MethodInfo);
		NullCheck(L_11);
		Vector3_t73  L_12 = Transform_get_position_m538(L_11, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		NullCheck(L_10);
		Transform_set_position_m570(L_10, L_12, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
	}

IL_008a:
	{
		return;
	}
}
// System.Void TapControl::OnEndGame()
extern MethodInfo TapControl_OnEndGame_m788_MethodInfo;
 void TapControl_OnEndGame_m788 (TapControl_t226 * __this, MethodInfo* method){
	{
		Behaviour_set_enabled_m547(__this, 0, /*hidden argument*/&Behaviour_set_enabled_m547_MethodInfo);
		return;
	}
}
// System.Void TapControl::FaceMovementDirection()
 void TapControl_FaceMovementDirection_m789 (TapControl_t226 * __this, MethodInfo* method){
	Vector3_t73  V_0 = {0};
	{
		CharacterController_t209 * L_0 = (__this->___character_18);
		NullCheck(L_0);
		Vector3_t73  L_1 = CharacterController_get_velocity_m802(L_0, /*hidden argument*/&CharacterController_get_velocity_m802_MethodInfo);
		V_0 = L_1;
		NullCheck((&V_0));
		(&V_0)->___y_2 = (((float)0));
		float L_2 = Vector3_get_magnitude_m577((&V_0), /*hidden argument*/&Vector3_get_magnitude_m577_MethodInfo);
		if ((((float)L_2) <= ((float)(0.1f))))
		{
			goto IL_0038;
		}
	}
	{
		Transform_t74 * L_3 = (__this->___thisTransform_17);
		Vector3_t73  L_4 = Vector3_get_normalized_m706((&V_0), /*hidden argument*/&Vector3_get_normalized_m706_MethodInfo);
		NullCheck(L_3);
		Transform_set_forward_m708(L_3, L_4, /*hidden argument*/&Transform_set_forward_m708_MethodInfo);
	}

IL_0038:
	{
		return;
	}
}
// System.Void TapControl::CameraControl(UnityEngine.Touch,UnityEngine.Touch)
 void TapControl_CameraControl_m790 (TapControl_t226 * __this, Touch_t201  ___touch0, Touch_t201  ___touch1, MethodInfo* method){
	Vector2_t99  V_0 = {0};
	Vector2_t99  V_1 = {0};
	Vector2_t99  V_2 = {0};
	Vector2_t99  V_3 = {0};
	float V_4 = 0.0f;
	Vector3_t73  V_5 = {0};
	Vector3_t73  V_6 = {0};
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t73  V_12 = {0};
	Vector3_t73  V_13 = {0};
	Vector2_t99  V_14 = {0};
	Vector2_t99  V_15 = {0};
	{
		bool L_0 = (__this->___rotateEnabled_13);
		if (!L_0)
		{
			goto IL_0153;
		}
	}
	{
		int32_t L_1 = (__this->___state_24);
		if ((((uint32_t)L_1) != ((uint32_t)5)))
		{
			goto IL_0153;
		}
	}
	{
		Vector2_t99  L_2 = Touch_get_position_m687((&___touch1), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_3 = Touch_get_position_m687((&___touch0), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_4 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		V_0 = L_4;
		float L_5 = Vector2_get_magnitude_m847((&V_0), /*hidden argument*/&Vector2_get_magnitude_m847_MethodInfo);
		Vector2_t99  L_6 = Vector2_op_Division_m848(NULL /*static, unused*/, V_0, L_5, /*hidden argument*/&Vector2_op_Division_m848_MethodInfo);
		V_1 = L_6;
		Vector2_t99  L_7 = Touch_get_position_m687((&___touch1), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_8 = Touch_get_deltaPosition_m849((&___touch1), /*hidden argument*/&Touch_get_deltaPosition_m849_MethodInfo);
		Vector2_t99  L_9 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		Vector2_t99  L_10 = Touch_get_position_m687((&___touch0), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_11 = Touch_get_deltaPosition_m849((&___touch0), /*hidden argument*/&Touch_get_deltaPosition_m849_MethodInfo);
		Vector2_t99  L_12 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		Vector2_t99  L_13 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_9, L_12, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		V_2 = L_13;
		float L_14 = Vector2_get_magnitude_m847((&V_2), /*hidden argument*/&Vector2_get_magnitude_m847_MethodInfo);
		Vector2_t99  L_15 = Vector2_op_Division_m848(NULL /*static, unused*/, V_2, L_14, /*hidden argument*/&Vector2_op_Division_m848_MethodInfo);
		V_3 = L_15;
		float L_16 = Vector2_Dot_m850(NULL /*static, unused*/, V_1, V_3, /*hidden argument*/&Vector2_Dot_m850_MethodInfo);
		V_4 = L_16;
		if ((((float)V_4) >= ((float)(((float)1)))))
		{
			goto IL_014e;
		}
	}
	{
		NullCheck((&V_0));
		float L_17 = ((&V_0)->___x_1);
		NullCheck((&V_0));
		float L_18 = ((&V_0)->___y_2);
		Vector3_t73  L_19 = {0};
		Vector3__ctor_m851(&L_19, L_17, L_18, /*hidden argument*/&Vector3__ctor_m851_MethodInfo);
		V_5 = L_19;
		NullCheck((&V_2));
		float L_20 = ((&V_2)->___x_1);
		NullCheck((&V_2));
		float L_21 = ((&V_2)->___y_2);
		Vector3_t73  L_22 = {0};
		Vector3__ctor_m851(&L_22, L_20, L_21, /*hidden argument*/&Vector3__ctor_m851_MethodInfo);
		V_6 = L_22;
		Vector3_t73  L_23 = Vector3_Cross_m852(NULL /*static, unused*/, V_5, V_6, /*hidden argument*/&Vector3_Cross_m852_MethodInfo);
		V_12 = L_23;
		Vector3_t73  L_24 = Vector3_get_normalized_m706((&V_12), /*hidden argument*/&Vector3_get_normalized_m706_MethodInfo);
		V_13 = L_24;
		NullCheck((&V_13));
		float L_25 = ((&V_13)->___z_3);
		V_7 = L_25;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_26 = acosf(V_4);
		V_8 = L_26;
		float L_27 = (__this->___rotationTarget_21);
		__this->___rotationTarget_21 = ((float)(L_27+((float)((float)((float)((float)V_8*(float)(57.29578f)))*(float)V_7))));
		float L_28 = (__this->___rotationTarget_21);
		if ((((float)L_28) >= ((float)(((float)0)))))
		{
			goto IL_012a;
		}
	}
	{
		float L_29 = (__this->___rotationTarget_21);
		__this->___rotationTarget_21 = ((float)(L_29+(((float)((int32_t)360)))));
		goto IL_014e;
	}

IL_012a:
	{
		float L_30 = (__this->___rotationTarget_21);
		if ((((float)L_30) < ((float)(((float)((int32_t)360))))))
		{
			goto IL_014e;
		}
	}
	{
		float L_31 = (__this->___rotationTarget_21);
		__this->___rotationTarget_21 = ((float)(L_31-(((float)((int32_t)360)))));
	}

IL_014e:
	{
		goto IL_0203;
	}

IL_0153:
	{
		bool L_32 = (__this->___zoomEnabled_10);
		if (!L_32)
		{
			goto IL_0203;
		}
	}
	{
		int32_t L_33 = (__this->___state_24);
		if ((((uint32_t)L_33) != ((uint32_t)4)))
		{
			goto IL_0203;
		}
	}
	{
		Vector2_t99  L_34 = Touch_get_position_m687((&___touch1), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_35 = Touch_get_position_m687((&___touch0), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_36 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		V_14 = L_36;
		float L_37 = Vector2_get_magnitude_m847((&V_14), /*hidden argument*/&Vector2_get_magnitude_m847_MethodInfo);
		V_9 = L_37;
		Vector2_t99  L_38 = Touch_get_position_m687((&___touch1), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_39 = Touch_get_deltaPosition_m849((&___touch1), /*hidden argument*/&Touch_get_deltaPosition_m849_MethodInfo);
		Vector2_t99  L_40 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		Vector2_t99  L_41 = Touch_get_position_m687((&___touch0), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_42 = Touch_get_deltaPosition_m849((&___touch0), /*hidden argument*/&Touch_get_deltaPosition_m849_MethodInfo);
		Vector2_t99  L_43 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		Vector2_t99  L_44 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_40, L_43, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		V_15 = L_44;
		float L_45 = Vector2_get_magnitude_m847((&V_15), /*hidden argument*/&Vector2_get_magnitude_m847_MethodInfo);
		V_10 = L_45;
		V_11 = ((float)(V_9-V_10));
		ZoomCamera_t224 * L_46 = (__this->___zoomCamera_15);
		ZoomCamera_t224 * L_47 = (__this->___zoomCamera_15);
		NullCheck(L_47);
		float L_48 = (L_47->___zoom_3);
		float L_49 = (__this->___zoomRate_12);
		float L_50 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		NullCheck(L_46);
		L_46->___zoom_3 = ((float)(L_48+((float)((float)((float)((float)V_11*(float)L_49))*(float)L_50))));
	}

IL_0203:
	{
		return;
	}
}
// System.Void TapControl::CharacterControl()
 void TapControl_CharacterControl_m791 (TapControl_t226 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	Touch_t201  V_1 = {0};
	Ray_t229  V_2 = {0};
	RaycastHit_t228  V_3 = {0};
	float V_4 = 0.0f;
	Vector3_t73  V_5 = {0};
	float V_6 = 0.0f;
	Vector2_t99  V_7 = {0};
	Vector2_t99  V_8 = {0};
	Vector3_t73  V_9 = {0};
	Vector3_t73  V_10 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		int32_t L_0 = Input_get_touchCount_m685(NULL /*static, unused*/, /*hidden argument*/&Input_get_touchCount_m685_MethodInfo);
		V_0 = L_0;
		if ((((uint32_t)V_0) != ((uint32_t)1)))
		{
			goto IL_0125;
		}
	}
	{
		int32_t L_1 = (__this->___state_24);
		if ((((uint32_t)L_1) != ((uint32_t)2)))
		{
			goto IL_0125;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		Touch_t201  L_2 = Input_GetTouch_m686(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m686_MethodInfo);
		V_1 = L_2;
		CharacterController_t209 * L_3 = (__this->___character_18);
		NullCheck(L_3);
		bool L_4 = CharacterController_get_isGrounded_m804(L_3, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		GUITexture_t100 * L_5 = (__this->___jumpButton_4);
		Vector2_t99  L_6 = Touch_get_position_m687((&V_1), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector3_t73  L_7 = Vector2_op_Implicit_m690(NULL /*static, unused*/, L_6, /*hidden argument*/&Vector2_op_Implicit_m690_MethodInfo);
		NullCheck(L_5);
		bool L_8 = GUIElement_HitTest_m691(L_5, L_7, /*hidden argument*/&GUIElement_HitTest_m691_MethodInfo);
		if (!L_8)
		{
			goto IL_0073;
		}
	}
	{
		CharacterController_t209 * L_9 = (__this->___character_18);
		NullCheck(L_9);
		Vector3_t73  L_10 = CharacterController_get_velocity_m802(L_9, /*hidden argument*/&CharacterController_get_velocity_m802_MethodInfo);
		__this->___velocity_23 = L_10;
		Vector3_t73 * L_11 = &(__this->___velocity_23);
		float L_12 = (__this->___jumpSpeed_6);
		NullCheck(L_11);
		L_11->___y_2 = L_12;
		goto IL_0125;
	}

IL_0073:
	{
		GUITexture_t100 * L_13 = (__this->___jumpButton_4);
		Vector2_t99  L_14 = Touch_get_position_m687((&V_1), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector3_t73  L_15 = Vector2_op_Implicit_m690(NULL /*static, unused*/, L_14, /*hidden argument*/&Vector2_op_Implicit_m690_MethodInfo);
		NullCheck(L_13);
		bool L_16 = GUIElement_HitTest_m691(L_13, L_15, /*hidden argument*/&GUIElement_HitTest_m691_MethodInfo);
		if (L_16)
		{
			goto IL_0125;
		}
	}
	{
		int32_t L_17 = Touch_get_phase_m698((&V_1), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((int32_t)L_17) == ((int32_t)0)))
		{
			goto IL_0125;
		}
	}
	{
		Camera_t168 * L_18 = (__this->___cam_16);
		Vector2_t99  L_19 = Touch_get_position_m687((&V_1), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		V_7 = L_19;
		NullCheck((&V_7));
		float L_20 = ((&V_7)->___x_1);
		Vector2_t99  L_21 = Touch_get_position_m687((&V_1), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		V_8 = L_21;
		NullCheck((&V_8));
		float L_22 = ((&V_8)->___y_2);
		Vector3_t73  L_23 = {0};
		Vector3__ctor_m851(&L_23, L_20, L_22, /*hidden argument*/&Vector3__ctor_m851_MethodInfo);
		NullCheck(L_18);
		Ray_t229  L_24 = Camera_ScreenPointToRay_m853(L_18, L_23, /*hidden argument*/&Camera_ScreenPointToRay_m853_MethodInfo);
		V_2 = L_24;
		Initobj (&RaycastHit_t228_il2cpp_TypeInfo, (&V_3));
		bool L_25 = Physics_Raycast_m854(NULL /*static, unused*/, V_2, (&V_3), /*hidden argument*/&Physics_Raycast_m854_MethodInfo);
		if (!L_25)
		{
			goto IL_0125;
		}
	}
	{
		Transform_t74 * L_26 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_26);
		Vector3_t73  L_27 = Transform_get_position_m538(L_26, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		Vector3_t73  L_28 = RaycastHit_get_point_m855((&V_3), /*hidden argument*/&RaycastHit_get_point_m855_MethodInfo);
		Vector3_t73  L_29 = Vector3_op_Subtraction_m574(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/&Vector3_op_Subtraction_m574_MethodInfo);
		V_9 = L_29;
		float L_30 = Vector3_get_magnitude_m577((&V_9), /*hidden argument*/&Vector3_get_magnitude_m577_MethodInfo);
		V_4 = L_30;
		float L_31 = (__this->___minimumDistanceToMove_8);
		if ((((float)V_4) <= ((float)L_31)))
		{
			goto IL_011e;
		}
	}
	{
		Vector3_t73  L_32 = RaycastHit_get_point_m855((&V_3), /*hidden argument*/&RaycastHit_get_point_m855_MethodInfo);
		__this->___targetLocation_19 = L_32;
	}

IL_011e:
	{
		__this->___moving_20 = 1;
	}

IL_0125:
	{
		Vector3_t73  L_33 = Vector3_get_zero_m807(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m807_MethodInfo);
		V_5 = L_33;
		bool L_34 = (__this->___moving_20);
		if (!L_34)
		{
			goto IL_018a;
		}
	}
	{
		Vector3_t73  L_35 = (__this->___targetLocation_19);
		Transform_t74 * L_36 = (__this->___thisTransform_17);
		NullCheck(L_36);
		Vector3_t73  L_37 = Transform_get_position_m538(L_36, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		Vector3_t73  L_38 = Vector3_op_Subtraction_m574(NULL /*static, unused*/, L_35, L_37, /*hidden argument*/&Vector3_op_Subtraction_m574_MethodInfo);
		V_5 = L_38;
		NullCheck((&V_5));
		(&V_5)->___y_2 = (((float)0));
		float L_39 = Vector3_get_magnitude_m577((&V_5), /*hidden argument*/&Vector3_get_magnitude_m577_MethodInfo);
		V_6 = L_39;
		if ((((float)V_6) >= ((float)(((float)1)))))
		{
			goto IL_0176;
		}
	}
	{
		__this->___moving_20 = 0;
		goto IL_018a;
	}

IL_0176:
	{
		Vector3_t73  L_40 = Vector3_get_normalized_m706((&V_5), /*hidden argument*/&Vector3_get_normalized_m706_MethodInfo);
		float L_41 = (__this->___speed_5);
		Vector3_t73  L_42 = Vector3_op_Multiply_m573(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_5 = L_42;
	}

IL_018a:
	{
		CharacterController_t209 * L_43 = (__this->___character_18);
		NullCheck(L_43);
		bool L_44 = CharacterController_get_isGrounded_m804(L_43, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (L_44)
		{
			goto IL_01ef;
		}
	}
	{
		Vector3_t73 * L_45 = &(__this->___velocity_23);
		Vector3_t73 * L_46 = &(__this->___velocity_23);
		NullCheck(L_46);
		float L_47 = (L_46->___y_2);
		Vector3_t73  L_48 = Physics_get_gravity_m805(NULL /*static, unused*/, /*hidden argument*/&Physics_get_gravity_m805_MethodInfo);
		V_10 = L_48;
		NullCheck((&V_10));
		float L_49 = ((&V_10)->___y_2);
		float L_50 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		NullCheck(L_45);
		L_45->___y_2 = ((float)(L_47+((float)((float)L_49*(float)L_50))));
		NullCheck((&V_5));
		float L_51 = ((&V_5)->___x_1);
		float L_52 = (__this->___inAirMultiplier_7);
		NullCheck((&V_5));
		(&V_5)->___x_1 = ((float)((float)L_51*(float)L_52));
		NullCheck((&V_5));
		float L_53 = ((&V_5)->___z_3);
		float L_54 = (__this->___inAirMultiplier_7);
		NullCheck((&V_5));
		(&V_5)->___z_3 = ((float)((float)L_53*(float)L_54));
	}

IL_01ef:
	{
		Vector3_t73  L_55 = (__this->___velocity_23);
		Vector3_t73  L_56 = Vector3_op_Addition_m576(NULL /*static, unused*/, V_5, L_55, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_5 = L_56;
		Vector3_t73  L_57 = Physics_get_gravity_m805(NULL /*static, unused*/, /*hidden argument*/&Physics_get_gravity_m805_MethodInfo);
		Vector3_t73  L_58 = Vector3_op_Addition_m576(NULL /*static, unused*/, V_5, L_57, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_5 = L_58;
		float L_59 = Time_get_deltaTime_m615(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m615_MethodInfo);
		Vector3_t73  L_60 = Vector3_op_Multiply_m573(NULL /*static, unused*/, V_5, L_59, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		V_5 = L_60;
		CharacterController_t209 * L_61 = (__this->___character_18);
		NullCheck(L_61);
		CharacterController_Move_m806(L_61, V_5, /*hidden argument*/&CharacterController_Move_m806_MethodInfo);
		CharacterController_t209 * L_62 = (__this->___character_18);
		NullCheck(L_62);
		bool L_63 = CharacterController_get_isGrounded_m804(L_62, /*hidden argument*/&CharacterController_get_isGrounded_m804_MethodInfo);
		if (!L_63)
		{
			goto IL_0243;
		}
	}
	{
		Vector3_t73  L_64 = Vector3_get_zero_m807(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m807_MethodInfo);
		__this->___velocity_23 = L_64;
	}

IL_0243:
	{
		VirtActionInvoker0::Invoke(&TapControl_FaceMovementDirection_m789_MethodInfo, __this);
		return;
	}
}
// System.Void TapControl::ResetControlState()
 void TapControl_ResetControlState_m792 (TapControl_t226 * __this, MethodInfo* method){
	{
		__this->___state_24 = 0;
		Int32U5BU5D_t175* L_0 = (__this->___fingerDown_25);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, 0)) = (int32_t)(-1);
		Int32U5BU5D_t175* L_1 = (__this->___fingerDown_25);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_1, 1)) = (int32_t)(-1);
		return;
	}
}
// System.Void TapControl::Update()
extern MethodInfo TapControl_Update_m793_MethodInfo;
 void TapControl_Update_m793 (TapControl_t226 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Touch_t201  V_2 = {0};
	TouchU5BU5D_t203* V_3 = {0};
	Touch_t201  V_4 = {0};
	Touch_t201  V_5 = {0};
	bool V_6 = false;
	bool V_7 = false;
	Vector2_t99  V_8 = {0};
	Vector2_t99  V_9 = {0};
	Vector2_t99  V_10 = {0};
	Vector2_t99  V_11 = {0};
	Vector2_t99  V_12 = {0};
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	float V_15 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		int32_t L_0 = Input_get_touchCount_m685(NULL /*static, unused*/, /*hidden argument*/&Input_get_touchCount_m685_MethodInfo);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_0017;
		}
	}
	{
		VirtActionInvoker0::Invoke(&TapControl_ResetControlState_m792_MethodInfo, __this);
		goto IL_047b;
	}

IL_0017:
	{
		Initobj (&Int32_t123_il2cpp_TypeInfo, (&V_1));
		Initobj (&Touch_t201_il2cpp_TypeInfo, (&V_2));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t187_il2cpp_TypeInfo));
		TouchU5BU5D_t203* L_1 = Input_get_touches_m702(NULL /*static, unused*/, /*hidden argument*/&Input_get_touches_m702_MethodInfo);
		V_3 = L_1;
		Initobj (&Touch_t201_il2cpp_TypeInfo, (&V_4));
		Initobj (&Touch_t201_il2cpp_TypeInfo, (&V_5));
		V_6 = 0;
		V_7 = 0;
		int32_t L_2 = (__this->___state_24);
		if ((((uint32_t)L_2) != ((uint32_t)0)))
		{
			goto IL_00d3;
		}
	}
	{
		V_1 = 0;
		goto IL_00cc;
	}

IL_0056:
	{
		NullCheck(V_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_3, V_1);
		V_2 = (*(Touch_t201 *)((Touch_t201 *)(Touch_t201 *)SZArrayLdElema(V_3, V_1)));
		int32_t L_3 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((int32_t)L_3) == ((int32_t)3)))
		{
			goto IL_00c8;
		}
	}
	{
		int32_t L_4 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((int32_t)L_4) == ((int32_t)4)))
		{
			goto IL_00c8;
		}
	}
	{
		__this->___state_24 = 1;
		float L_5 = Time_get_time_m813(NULL /*static, unused*/, /*hidden argument*/&Time_get_time_m813_MethodInfo);
		__this->___firstTouchTime_28 = L_5;
		Int32U5BU5D_t175* L_6 = (__this->___fingerDown_25);
		int32_t L_7 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_6, 0)) = (int32_t)L_7;
		Vector2U5BU5D_t225* L_8 = (__this->___fingerDownPosition_26);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		Vector2_t99  L_9 = Touch_get_position_m687((&V_2), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		*((Vector2_t99 *)(Vector2_t99 *)SZArrayLdElema(L_8, 0)) = L_9;
		Int32U5BU5D_t175* L_10 = (__this->___fingerDownFrame_27);
		int32_t L_11 = Time_get_frameCount_m856(NULL /*static, unused*/, /*hidden argument*/&Time_get_frameCount_m856_MethodInfo);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_10, 0)) = (int32_t)L_11;
		goto IL_00d3;
	}

IL_00c8:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_00cc:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0056;
		}
	}

IL_00d3:
	{
		int32_t L_12 = (__this->___state_24);
		if ((((uint32_t)L_12) != ((uint32_t)1)))
		{
			goto IL_01d5;
		}
	}
	{
		V_1 = 0;
		goto IL_01ce;
	}

IL_00e6:
	{
		NullCheck(V_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_3, V_1);
		V_2 = (*(Touch_t201 *)((Touch_t201 *)(Touch_t201 *)SZArrayLdElema(V_3, V_1)));
		int32_t L_13 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((int32_t)L_13) == ((int32_t)4)))
		{
			goto IL_01ca;
		}
	}
	{
		if ((((int32_t)V_0) < ((int32_t)2)))
		{
			goto IL_0160;
		}
	}
	{
		int32_t L_14 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		Int32U5BU5D_t175* L_15 = (__this->___fingerDown_25);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		int32_t L_16 = 0;
		if ((((int32_t)L_14) == ((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_15, L_16)))))
		{
			goto IL_0160;
		}
	}
	{
		__this->___state_24 = 3;
		Int32U5BU5D_t175* L_17 = (__this->___fingerDown_25);
		int32_t L_18 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_17, 1)) = (int32_t)L_18;
		Vector2U5BU5D_t225* L_19 = (__this->___fingerDownPosition_26);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		Vector2_t99  L_20 = Touch_get_position_m687((&V_2), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		*((Vector2_t99 *)(Vector2_t99 *)SZArrayLdElema(L_19, 1)) = L_20;
		Int32U5BU5D_t175* L_21 = (__this->___fingerDownFrame_27);
		int32_t L_22 = Time_get_frameCount_m856(NULL /*static, unused*/, /*hidden argument*/&Time_get_frameCount_m856_MethodInfo);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_21, 1)) = (int32_t)L_22;
		goto IL_01d5;
	}
	// Dead block : IL_015b: br IL_01ca

IL_0160:
	{
		if ((((uint32_t)V_0) != ((uint32_t)1)))
		{
			goto IL_01ca;
		}
	}
	{
		Vector2_t99  L_23 = Touch_get_position_m687((&V_2), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2U5BU5D_t225* L_24 = (__this->___fingerDownPosition_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		Vector2_t99  L_25 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_23, (*(Vector2_t99 *)((Vector2_t99 *)(Vector2_t99 *)SZArrayLdElema(L_24, 0))), /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		V_8 = L_25;
		int32_t L_26 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		Int32U5BU5D_t175* L_27 = (__this->___fingerDown_25);
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 0);
		int32_t L_28 = 0;
		if ((((uint32_t)L_26) != ((uint32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_28)))))
		{
			goto IL_01ca;
		}
	}
	{
		float L_29 = Time_get_time_m813(NULL /*static, unused*/, /*hidden argument*/&Time_get_time_m813_MethodInfo);
		float L_30 = (__this->___firstTouchTime_28);
		float L_31 = (__this->___minimumTimeUntilMove_9);
		if ((((float)L_29) > ((float)((float)(L_30+L_31)))))
		{
			goto IL_01be;
		}
	}
	{
		int32_t L_32 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((uint32_t)L_32) != ((uint32_t)3)))
		{
			goto IL_01ca;
		}
	}

IL_01be:
	{
		__this->___state_24 = 2;
		goto IL_01d5;
	}

IL_01ca:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_01ce:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_00e6;
		}
	}

IL_01d5:
	{
		int32_t L_33 = (__this->___state_24);
		if ((((uint32_t)L_33) != ((uint32_t)3)))
		{
			goto IL_03c0;
		}
	}
	{
		V_1 = 0;
		goto IL_02d4;
	}

IL_01e8:
	{
		NullCheck(V_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_3, V_1);
		V_2 = (*(Touch_t201 *)((Touch_t201 *)(Touch_t201 *)SZArrayLdElema(V_3, V_1)));
		int32_t L_34 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((uint32_t)L_34) != ((uint32_t)0)))
		{
			goto IL_0270;
		}
	}
	{
		int32_t L_35 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		Int32U5BU5D_t175* L_36 = (__this->___fingerDown_25);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 0);
		int32_t L_37 = 0;
		if ((((uint32_t)L_35) != ((uint32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_37)))))
		{
			goto IL_0233;
		}
	}
	{
		Int32U5BU5D_t175* L_38 = (__this->___fingerDownFrame_27);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		int32_t L_39 = 0;
		int32_t L_40 = Time_get_frameCount_m856(NULL /*static, unused*/, /*hidden argument*/&Time_get_frameCount_m856_MethodInfo);
		if ((((uint32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_38, L_39))) != ((uint32_t)L_40)))
		{
			goto IL_0233;
		}
	}
	{
		V_4 = V_2;
		V_6 = 1;
		goto IL_0270;
	}

IL_0233:
	{
		int32_t L_41 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		Int32U5BU5D_t175* L_42 = (__this->___fingerDown_25);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 0);
		int32_t L_43 = 0;
		if ((((int32_t)L_41) == ((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_42, L_43)))))
		{
			goto IL_0270;
		}
	}
	{
		int32_t L_44 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		Int32U5BU5D_t175* L_45 = (__this->___fingerDown_25);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 1);
		int32_t L_46 = 1;
		if ((((int32_t)L_44) == ((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_45, L_46)))))
		{
			goto IL_0270;
		}
	}
	{
		Int32U5BU5D_t175* L_47 = (__this->___fingerDown_25);
		int32_t L_48 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_47, 1)) = (int32_t)L_48;
		V_5 = V_2;
		V_7 = 1;
	}

IL_0270:
	{
		int32_t L_49 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((int32_t)L_49) == ((int32_t)1)))
		{
			goto IL_0297;
		}
	}
	{
		int32_t L_50 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((int32_t)L_50) == ((int32_t)2)))
		{
			goto IL_0297;
		}
	}
	{
		int32_t L_51 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((uint32_t)L_51) != ((uint32_t)3)))
		{
			goto IL_02d0;
		}
	}

IL_0297:
	{
		int32_t L_52 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		Int32U5BU5D_t175* L_53 = (__this->___fingerDown_25);
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
		int32_t L_54 = 0;
		if ((((uint32_t)L_52) != ((uint32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_53, L_54)))))
		{
			goto IL_02b6;
		}
	}
	{
		V_4 = V_2;
		V_6 = 1;
		goto IL_02d0;
	}

IL_02b6:
	{
		int32_t L_55 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		Int32U5BU5D_t175* L_56 = (__this->___fingerDown_25);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		int32_t L_57 = 1;
		if ((((uint32_t)L_55) != ((uint32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_56, L_57)))))
		{
			goto IL_02d0;
		}
	}
	{
		V_5 = V_2;
		V_7 = 1;
	}

IL_02d0:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_02d4:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_01e8;
		}
	}
	{
		if (!V_6)
		{
			goto IL_03b9;
		}
	}
	{
		if (!V_7)
		{
			goto IL_03b4;
		}
	}
	{
		Vector2U5BU5D_t225* L_58 = (__this->___fingerDownPosition_26);
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 1);
		Vector2U5BU5D_t225* L_59 = (__this->___fingerDownPosition_26);
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, 0);
		Vector2_t99  L_60 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, (*(Vector2_t99 *)((Vector2_t99 *)(Vector2_t99 *)SZArrayLdElema(L_58, 1))), (*(Vector2_t99 *)((Vector2_t99 *)(Vector2_t99 *)SZArrayLdElema(L_59, 0))), /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		V_9 = L_60;
		Vector2_t99  L_61 = Touch_get_position_m687((&V_5), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_62 = Touch_get_position_m687((&V_4), /*hidden argument*/&Touch_get_position_m687_MethodInfo);
		Vector2_t99  L_63 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		V_10 = L_63;
		float L_64 = Vector2_get_magnitude_m847((&V_9), /*hidden argument*/&Vector2_get_magnitude_m847_MethodInfo);
		Vector2_t99  L_65 = Vector2_op_Division_m848(NULL /*static, unused*/, V_9, L_64, /*hidden argument*/&Vector2_op_Division_m848_MethodInfo);
		V_11 = L_65;
		float L_66 = Vector2_get_magnitude_m847((&V_10), /*hidden argument*/&Vector2_get_magnitude_m847_MethodInfo);
		Vector2_t99  L_67 = Vector2_op_Division_m848(NULL /*static, unused*/, V_10, L_66, /*hidden argument*/&Vector2_op_Division_m848_MethodInfo);
		V_12 = L_67;
		float L_68 = Vector2_Dot_m850(NULL /*static, unused*/, V_11, V_12, /*hidden argument*/&Vector2_Dot_m850_MethodInfo);
		V_13 = L_68;
		if ((((float)V_13) >= ((float)(((float)1)))))
		{
			goto IL_037e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_69 = acosf(V_13);
		V_14 = L_69;
		float L_70 = (__this->___rotateEpsilon_14);
		if ((((float)V_14) <= ((float)((float)((float)L_70*(float)(0.0174532924f))))))
		{
			goto IL_037e;
		}
	}
	{
		__this->___state_24 = 5;
	}

IL_037e:
	{
		int32_t L_71 = (__this->___state_24);
		if ((((uint32_t)L_71) != ((uint32_t)3)))
		{
			goto IL_03b4;
		}
	}
	{
		float L_72 = Vector2_get_magnitude_m847((&V_9), /*hidden argument*/&Vector2_get_magnitude_m847_MethodInfo);
		float L_73 = Vector2_get_magnitude_m847((&V_10), /*hidden argument*/&Vector2_get_magnitude_m847_MethodInfo);
		V_15 = ((float)(L_72-L_73));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_74 = fabsf(V_15);
		float L_75 = (__this->___zoomEpsilon_11);
		if ((((float)L_74) <= ((float)L_75)))
		{
			goto IL_03b4;
		}
	}
	{
		__this->___state_24 = 4;
	}

IL_03b4:
	{
		goto IL_03c0;
	}

IL_03b9:
	{
		__this->___state_24 = 6;
	}

IL_03c0:
	{
		int32_t L_76 = (__this->___state_24);
		if ((((int32_t)L_76) == ((int32_t)5)))
		{
			goto IL_03d8;
		}
	}
	{
		int32_t L_77 = (__this->___state_24);
		if ((((uint32_t)L_77) != ((uint32_t)4)))
		{
			goto IL_047b;
		}
	}

IL_03d8:
	{
		V_1 = 0;
		goto IL_0450;
	}

IL_03df:
	{
		NullCheck(V_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_3, V_1);
		V_2 = (*(Touch_t201 *)((Touch_t201 *)(Touch_t201 *)SZArrayLdElema(V_3, V_1)));
		int32_t L_78 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((int32_t)L_78) == ((int32_t)1)))
		{
			goto IL_0413;
		}
	}
	{
		int32_t L_79 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((int32_t)L_79) == ((int32_t)2)))
		{
			goto IL_0413;
		}
	}
	{
		int32_t L_80 = Touch_get_phase_m698((&V_2), /*hidden argument*/&Touch_get_phase_m698_MethodInfo);
		if ((((uint32_t)L_80) != ((uint32_t)3)))
		{
			goto IL_044c;
		}
	}

IL_0413:
	{
		int32_t L_81 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		Int32U5BU5D_t175* L_82 = (__this->___fingerDown_25);
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, 0);
		int32_t L_83 = 0;
		if ((((uint32_t)L_81) != ((uint32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_82, L_83)))))
		{
			goto IL_0432;
		}
	}
	{
		V_4 = V_2;
		V_6 = 1;
		goto IL_044c;
	}

IL_0432:
	{
		int32_t L_84 = Touch_get_fingerId_m692((&V_2), /*hidden argument*/&Touch_get_fingerId_m692_MethodInfo);
		Int32U5BU5D_t175* L_85 = (__this->___fingerDown_25);
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, 1);
		int32_t L_86 = 1;
		if ((((uint32_t)L_84) != ((uint32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_85, L_86)))))
		{
			goto IL_044c;
		}
	}
	{
		V_5 = V_2;
		V_7 = 1;
	}

IL_044c:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0450:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_03df;
		}
	}
	{
		if (!V_6)
		{
			goto IL_0474;
		}
	}
	{
		if (!V_7)
		{
			goto IL_046f;
		}
	}
	{
		VirtActionInvoker2< Touch_t201 , Touch_t201  >::Invoke(&TapControl_CameraControl_m790_MethodInfo, __this, V_4, V_5);
	}

IL_046f:
	{
		goto IL_047b;
	}

IL_0474:
	{
		__this->___state_24 = 6;
	}

IL_047b:
	{
		VirtActionInvoker0::Invoke(&TapControl_CharacterControl_m791_MethodInfo, __this);
		return;
	}
}
// System.Void TapControl::LateUpdate()
extern MethodInfo TapControl_LateUpdate_m794_MethodInfo;
 void TapControl_LateUpdate_m794 (TapControl_t226 * __this, MethodInfo* method){
	float V_0 = 0.0f;
	Vector3_t73  V_1 = {0};
	Vector3_t73  V_2 = {0};
	float V_3 = 0.0f;
	Vector3_t73  V_4 = {0};
	{
		Transform_t74 * L_0 = (__this->___cameraPivot_3);
		NullCheck(L_0);
		Vector3_t73  L_1 = Transform_get_eulerAngles_m712(L_0, /*hidden argument*/&Transform_get_eulerAngles_m712_MethodInfo);
		V_2 = L_1;
		NullCheck((&V_2));
		float L_2 = ((&V_2)->___y_2);
		float L_3 = (__this->___rotationTarget_21);
		float* L_4 = &(__this->___rotationVelocity_22);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_5 = Mathf_SmoothDampAngle_m857(NULL /*static, unused*/, L_2, L_3, L_4, (0.3f), /*hidden argument*/&Mathf_SmoothDampAngle_m857_MethodInfo);
		float L_6 = L_5;
		V_0 = L_6;
		Transform_t74 * L_7 = (__this->___cameraPivot_3);
		NullCheck(L_7);
		Vector3_t73  L_8 = Transform_get_eulerAngles_m712(L_7, /*hidden argument*/&Transform_get_eulerAngles_m712_MethodInfo);
		Vector3_t73  L_9 = L_8;
		V_1 = L_9;
		float L_10 = V_0;
		V_3 = L_10;
		NullCheck((&V_1));
		(&V_1)->___y_2 = L_10;
		Transform_t74 * L_11 = (__this->___cameraPivot_3);
		Vector3_t73  L_12 = V_1;
		V_4 = L_12;
		NullCheck(L_11);
		Transform_set_eulerAngles_m713(L_11, L_12, /*hidden argument*/&Transform_set_eulerAngles_m713_MethodInfo);
		return;
	}
}
// System.Void TapControl::Main()
extern MethodInfo TapControl_Main_m795_MethodInfo;
 void TapControl_Main_m795 (TapControl_t226 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition TapControl
extern Il2CppType GameObject_t29_0_0_6;
FieldInfo TapControl_t226____cameraObject_2_FieldInfo = 
{
	"cameraObject"/* name */
	, &GameObject_t29_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___cameraObject_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_6;
FieldInfo TapControl_t226____cameraPivot_3_FieldInfo = 
{
	"cameraPivot"/* name */
	, &Transform_t74_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___cameraPivot_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GUITexture_t100_0_0_6;
FieldInfo TapControl_t226____jumpButton_4_FieldInfo = 
{
	"jumpButton"/* name */
	, &GUITexture_t100_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___jumpButton_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo TapControl_t226____speed_5_FieldInfo = 
{
	"speed"/* name */
	, &Single_t170_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___speed_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo TapControl_t226____jumpSpeed_6_FieldInfo = 
{
	"jumpSpeed"/* name */
	, &Single_t170_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___jumpSpeed_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo TapControl_t226____inAirMultiplier_7_FieldInfo = 
{
	"inAirMultiplier"/* name */
	, &Single_t170_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___inAirMultiplier_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo TapControl_t226____minimumDistanceToMove_8_FieldInfo = 
{
	"minimumDistanceToMove"/* name */
	, &Single_t170_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___minimumDistanceToMove_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo TapControl_t226____minimumTimeUntilMove_9_FieldInfo = 
{
	"minimumTimeUntilMove"/* name */
	, &Single_t170_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___minimumTimeUntilMove_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo TapControl_t226____zoomEnabled_10_FieldInfo = 
{
	"zoomEnabled"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___zoomEnabled_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo TapControl_t226____zoomEpsilon_11_FieldInfo = 
{
	"zoomEpsilon"/* name */
	, &Single_t170_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___zoomEpsilon_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo TapControl_t226____zoomRate_12_FieldInfo = 
{
	"zoomRate"/* name */
	, &Single_t170_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___zoomRate_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo TapControl_t226____rotateEnabled_13_FieldInfo = 
{
	"rotateEnabled"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___rotateEnabled_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo TapControl_t226____rotateEpsilon_14_FieldInfo = 
{
	"rotateEpsilon"/* name */
	, &Single_t170_0_0_6/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___rotateEpsilon_14)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ZoomCamera_t224_0_0_1;
FieldInfo TapControl_t226____zoomCamera_15_FieldInfo = 
{
	"zoomCamera"/* name */
	, &ZoomCamera_t224_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___zoomCamera_15)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Camera_t168_0_0_1;
FieldInfo TapControl_t226____cam_16_FieldInfo = 
{
	"cam"/* name */
	, &Camera_t168_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___cam_16)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_1;
FieldInfo TapControl_t226____thisTransform_17_FieldInfo = 
{
	"thisTransform"/* name */
	, &Transform_t74_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___thisTransform_17)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType CharacterController_t209_0_0_1;
FieldInfo TapControl_t226____character_18_FieldInfo = 
{
	"character"/* name */
	, &CharacterController_t209_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___character_18)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo TapControl_t226____targetLocation_19_FieldInfo = 
{
	"targetLocation"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___targetLocation_19)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo TapControl_t226____moving_20_FieldInfo = 
{
	"moving"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___moving_20)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo TapControl_t226____rotationTarget_21_FieldInfo = 
{
	"rotationTarget"/* name */
	, &Single_t170_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___rotationTarget_21)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo TapControl_t226____rotationVelocity_22_FieldInfo = 
{
	"rotationVelocity"/* name */
	, &Single_t170_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___rotationVelocity_22)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo TapControl_t226____velocity_23_FieldInfo = 
{
	"velocity"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___velocity_23)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ControlState_t223_0_0_1;
FieldInfo TapControl_t226____state_24_FieldInfo = 
{
	"state"/* name */
	, &ControlState_t223_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___state_24)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32U5BU5D_t175_0_0_1;
FieldInfo TapControl_t226____fingerDown_25_FieldInfo = 
{
	"fingerDown"/* name */
	, &Int32U5BU5D_t175_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___fingerDown_25)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2U5BU5D_t225_0_0_1;
FieldInfo TapControl_t226____fingerDownPosition_26_FieldInfo = 
{
	"fingerDownPosition"/* name */
	, &Vector2U5BU5D_t225_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___fingerDownPosition_26)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32U5BU5D_t175_0_0_1;
FieldInfo TapControl_t226____fingerDownFrame_27_FieldInfo = 
{
	"fingerDownFrame"/* name */
	, &Int32U5BU5D_t175_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___fingerDownFrame_27)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo TapControl_t226____firstTouchTime_28_FieldInfo = 
{
	"firstTouchTime"/* name */
	, &Single_t170_0_0_1/* type */
	, &TapControl_t226_il2cpp_TypeInfo/* parent */
	, offsetof(TapControl_t226, ___firstTouchTime_28)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TapControl_t226_FieldInfos[] =
{
	&TapControl_t226____cameraObject_2_FieldInfo,
	&TapControl_t226____cameraPivot_3_FieldInfo,
	&TapControl_t226____jumpButton_4_FieldInfo,
	&TapControl_t226____speed_5_FieldInfo,
	&TapControl_t226____jumpSpeed_6_FieldInfo,
	&TapControl_t226____inAirMultiplier_7_FieldInfo,
	&TapControl_t226____minimumDistanceToMove_8_FieldInfo,
	&TapControl_t226____minimumTimeUntilMove_9_FieldInfo,
	&TapControl_t226____zoomEnabled_10_FieldInfo,
	&TapControl_t226____zoomEpsilon_11_FieldInfo,
	&TapControl_t226____zoomRate_12_FieldInfo,
	&TapControl_t226____rotateEnabled_13_FieldInfo,
	&TapControl_t226____rotateEpsilon_14_FieldInfo,
	&TapControl_t226____zoomCamera_15_FieldInfo,
	&TapControl_t226____cam_16_FieldInfo,
	&TapControl_t226____thisTransform_17_FieldInfo,
	&TapControl_t226____character_18_FieldInfo,
	&TapControl_t226____targetLocation_19_FieldInfo,
	&TapControl_t226____moving_20_FieldInfo,
	&TapControl_t226____rotationTarget_21_FieldInfo,
	&TapControl_t226____rotationVelocity_22_FieldInfo,
	&TapControl_t226____velocity_23_FieldInfo,
	&TapControl_t226____state_24_FieldInfo,
	&TapControl_t226____fingerDown_25_FieldInfo,
	&TapControl_t226____fingerDownPosition_26_FieldInfo,
	&TapControl_t226____fingerDownFrame_27_FieldInfo,
	&TapControl_t226____firstTouchTime_28_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void TapControl::.ctor()
MethodInfo TapControl__ctor_m786_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TapControl__ctor_m786/* method */
	, &TapControl_t226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void TapControl::Start()
MethodInfo TapControl_Start_m787_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&TapControl_Start_m787/* method */
	, &TapControl_t226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void TapControl::OnEndGame()
MethodInfo TapControl_OnEndGame_m788_MethodInfo = 
{
	"OnEndGame"/* name */
	, (methodPointerType)&TapControl_OnEndGame_m788/* method */
	, &TapControl_t226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void TapControl::FaceMovementDirection()
MethodInfo TapControl_FaceMovementDirection_m789_MethodInfo = 
{
	"FaceMovementDirection"/* name */
	, (methodPointerType)&TapControl_FaceMovementDirection_m789/* method */
	, &TapControl_t226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Touch_t201_0_0_0;
extern Il2CppType Touch_t201_0_0_0;
extern Il2CppType Touch_t201_0_0_0;
static ParameterInfo TapControl_t226_TapControl_CameraControl_m790_ParameterInfos[] = 
{
	{"touch0", 0, 134217732, &EmptyCustomAttributesCache, &Touch_t201_0_0_0},
	{"touch1", 1, 134217733, &EmptyCustomAttributesCache, &Touch_t201_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Touch_t201_Touch_t201 (MethodInfo* method, void* obj, void** args);
// System.Void TapControl::CameraControl(UnityEngine.Touch,UnityEngine.Touch)
MethodInfo TapControl_CameraControl_m790_MethodInfo = 
{
	"CameraControl"/* name */
	, (methodPointerType)&TapControl_CameraControl_m790/* method */
	, &TapControl_t226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Touch_t201_Touch_t201/* invoker_method */
	, TapControl_t226_TapControl_CameraControl_m790_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void TapControl::CharacterControl()
MethodInfo TapControl_CharacterControl_m791_MethodInfo = 
{
	"CharacterControl"/* name */
	, (methodPointerType)&TapControl_CharacterControl_m791/* method */
	, &TapControl_t226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void TapControl::ResetControlState()
MethodInfo TapControl_ResetControlState_m792_MethodInfo = 
{
	"ResetControlState"/* name */
	, (methodPointerType)&TapControl_ResetControlState_m792/* method */
	, &TapControl_t226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void TapControl::Update()
MethodInfo TapControl_Update_m793_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TapControl_Update_m793/* method */
	, &TapControl_t226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void TapControl::LateUpdate()
MethodInfo TapControl_LateUpdate_m794_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&TapControl_LateUpdate_m794/* method */
	, &TapControl_t226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void TapControl::Main()
MethodInfo TapControl_Main_m795_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&TapControl_Main_m795/* method */
	, &TapControl_t226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TapControl_t226_MethodInfos[] =
{
	&TapControl__ctor_m786_MethodInfo,
	&TapControl_Start_m787_MethodInfo,
	&TapControl_OnEndGame_m788_MethodInfo,
	&TapControl_FaceMovementDirection_m789_MethodInfo,
	&TapControl_CameraControl_m790_MethodInfo,
	&TapControl_CharacterControl_m791_MethodInfo,
	&TapControl_ResetControlState_m792_MethodInfo,
	&TapControl_Update_m793_MethodInfo,
	&TapControl_LateUpdate_m794_MethodInfo,
	&TapControl_Main_m795_MethodInfo,
	NULL
};
static MethodInfo* TapControl_t226_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&TapControl_Start_m787_MethodInfo,
	&TapControl_OnEndGame_m788_MethodInfo,
	&TapControl_FaceMovementDirection_m789_MethodInfo,
	&TapControl_CameraControl_m790_MethodInfo,
	&TapControl_CharacterControl_m791_MethodInfo,
	&TapControl_ResetControlState_m792_MethodInfo,
	&TapControl_Update_m793_MethodInfo,
	&TapControl_LateUpdate_m794_MethodInfo,
	&TapControl_Main_m795_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType TapControl_t226_0_0_0;
extern Il2CppType TapControl_t226_1_0_0;
struct TapControl_t226;
TypeInfo TapControl_t226_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "TapControl"/* name */
	, ""/* namespaze */
	, TapControl_t226_MethodInfos/* methods */
	, NULL/* properties */
	, TapControl_t226_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TapControl_t226_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TapControl_t226_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TapControl_t226_il2cpp_TypeInfo/* cast_class */
	, &TapControl_t226_0_0_0/* byval_arg */
	, &TapControl_t226_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TapControl_t226)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 10/* method_count */
	, 0/* property_count */
	, 27/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
// ZoomCamera
#include "AssemblyU2DUnityScript_ZoomCameraMethodDeclarations.h"

extern MethodInfo Transform_get_parent_m858_MethodInfo;
extern MethodInfo Transform_InverseTransformDirection_m859_MethodInfo;
extern MethodInfo Transform_TransformPoint_m628_MethodInfo;
extern MethodInfo Physics_Linecast_m860_MethodInfo;


// System.Void ZoomCamera::.ctor()
extern MethodInfo ZoomCamera__ctor_m796_MethodInfo;
 void ZoomCamera__ctor_m796 (ZoomCamera_t224 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		__this->___zoomMin_4 = (((float)((int32_t)-5)));
		__this->___zoomMax_5 = (((float)5));
		__this->___seekTime_6 = (1.0f);
		return;
	}
}
// System.Void ZoomCamera::Start()
extern MethodInfo ZoomCamera_Start_m797_MethodInfo;
 void ZoomCamera_Start_m797 (ZoomCamera_t224 * __this, MethodInfo* method){
	{
		Transform_t74 * L_0 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		__this->___thisTransform_9 = L_0;
		Transform_t74 * L_1 = (__this->___thisTransform_9);
		NullCheck(L_1);
		Vector3_t73  L_2 = Transform_get_localPosition_m830(L_1, /*hidden argument*/&Transform_get_localPosition_m830_MethodInfo);
		__this->___defaultLocalPosition_8 = L_2;
		float L_3 = (__this->___zoom_3);
		__this->___currentZoom_10 = L_3;
		return;
	}
}
// System.Void ZoomCamera::Update()
extern MethodInfo ZoomCamera_Update_m798_MethodInfo;
 void ZoomCamera_Update_m798 (ZoomCamera_t224 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	RaycastHit_t228  V_1 = {0};
	Vector3_t73  V_2 = {0};
	Vector3_t73  V_3 = {0};
	Vector3_t73  V_4 = {0};
	Vector3_t73  V_5 = {0};
	Vector3_t73  V_6 = {0};
	{
		float L_0 = (__this->___zoom_3);
		float L_1 = (__this->___zoomMin_4);
		float L_2 = (__this->___zoomMax_5);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_3 = Mathf_Clamp_m697(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/&Mathf_Clamp_m697_MethodInfo);
		__this->___zoom_3 = L_3;
		V_0 = ((int32_t)-261);
		Initobj (&RaycastHit_t228_il2cpp_TypeInfo, (&V_1));
		Transform_t74 * L_4 = (__this->___origin_2);
		NullCheck(L_4);
		Vector3_t73  L_5 = Transform_get_position_m538(L_4, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		V_2 = L_5;
		Vector3_t73  L_6 = (__this->___defaultLocalPosition_8);
		Transform_t74 * L_7 = (__this->___thisTransform_9);
		NullCheck(L_7);
		Transform_t74 * L_8 = Transform_get_parent_m858(L_7, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		Transform_t74 * L_9 = (__this->___thisTransform_9);
		NullCheck(L_9);
		Vector3_t73  L_10 = Transform_get_forward_m572(L_9, /*hidden argument*/&Transform_get_forward_m572_MethodInfo);
		float L_11 = (__this->___zoom_3);
		Vector3_t73  L_12 = Vector3_op_Multiply_m573(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		NullCheck(L_8);
		Vector3_t73  L_13 = Transform_InverseTransformDirection_m859(L_8, L_12, /*hidden argument*/&Transform_InverseTransformDirection_m859_MethodInfo);
		Vector3_t73  L_14 = Vector3_op_Addition_m576(NULL /*static, unused*/, L_6, L_13, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_3 = L_14;
		Transform_t74 * L_15 = (__this->___thisTransform_9);
		NullCheck(L_15);
		Transform_t74 * L_16 = Transform_get_parent_m858(L_15, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		NullCheck(L_16);
		Vector3_t73  L_17 = Transform_TransformPoint_m628(L_16, V_3, /*hidden argument*/&Transform_TransformPoint_m628_MethodInfo);
		V_4 = L_17;
		bool L_18 = Physics_Linecast_m860(NULL /*static, unused*/, V_2, V_4, (&V_1), V_0, /*hidden argument*/&Physics_Linecast_m860_MethodInfo);
		if (!L_18)
		{
			goto IL_00db;
		}
	}
	{
		Vector3_t73  L_19 = RaycastHit_get_point_m855((&V_1), /*hidden argument*/&RaycastHit_get_point_m855_MethodInfo);
		Transform_t74 * L_20 = (__this->___thisTransform_9);
		Vector3_t73  L_21 = Vector3_get_forward_m837(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_forward_m837_MethodInfo);
		NullCheck(L_20);
		Vector3_t73  L_22 = Transform_TransformDirection_m803(L_20, L_21, /*hidden argument*/&Transform_TransformDirection_m803_MethodInfo);
		Vector3_t73  L_23 = Vector3_op_Addition_m576(NULL /*static, unused*/, L_19, L_22, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_5 = L_23;
		Transform_t74 * L_24 = (__this->___thisTransform_9);
		NullCheck(L_24);
		Transform_t74 * L_25 = Transform_get_parent_m858(L_24, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		Vector3_t73  L_26 = (__this->___defaultLocalPosition_8);
		NullCheck(L_25);
		Vector3_t73  L_27 = Transform_TransformPoint_m628(L_25, L_26, /*hidden argument*/&Transform_TransformPoint_m628_MethodInfo);
		Vector3_t73  L_28 = Vector3_op_Subtraction_m574(NULL /*static, unused*/, V_5, L_27, /*hidden argument*/&Vector3_op_Subtraction_m574_MethodInfo);
		V_6 = L_28;
		float L_29 = Vector3_get_magnitude_m577((&V_6), /*hidden argument*/&Vector3_get_magnitude_m577_MethodInfo);
		__this->___targetZoom_11 = L_29;
		goto IL_00e7;
	}

IL_00db:
	{
		float L_30 = (__this->___zoom_3);
		__this->___targetZoom_11 = L_30;
	}

IL_00e7:
	{
		float L_31 = (__this->___targetZoom_11);
		float L_32 = (__this->___zoomMin_4);
		float L_33 = (__this->___zoomMax_5);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_34 = Mathf_Clamp_m697(NULL /*static, unused*/, L_31, L_32, L_33, /*hidden argument*/&Mathf_Clamp_m697_MethodInfo);
		__this->___targetZoom_11 = L_34;
		bool L_35 = (__this->___smoothZoomIn_7);
		if (L_35)
		{
			goto IL_0134;
		}
	}
	{
		float L_36 = (__this->___targetZoom_11);
		float L_37 = (__this->___currentZoom_10);
		if ((((float)((float)(L_36-L_37))) <= ((float)(((float)0)))))
		{
			goto IL_0134;
		}
	}
	{
		float L_38 = (__this->___targetZoom_11);
		__this->___currentZoom_10 = L_38;
		goto IL_0157;
	}

IL_0134:
	{
		float L_39 = (__this->___currentZoom_10);
		float L_40 = (__this->___targetZoom_11);
		float* L_41 = &(__this->___zoomVelocity_12);
		float L_42 = (__this->___seekTime_6);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_43 = Mathf_SmoothDamp_m831(NULL /*static, unused*/, L_39, L_40, L_41, L_42, /*hidden argument*/&Mathf_SmoothDamp_m831_MethodInfo);
		__this->___currentZoom_10 = L_43;
	}

IL_0157:
	{
		Vector3_t73  L_44 = (__this->___defaultLocalPosition_8);
		Transform_t74 * L_45 = (__this->___thisTransform_9);
		NullCheck(L_45);
		Transform_t74 * L_46 = Transform_get_parent_m858(L_45, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		Transform_t74 * L_47 = (__this->___thisTransform_9);
		NullCheck(L_47);
		Vector3_t73  L_48 = Transform_get_forward_m572(L_47, /*hidden argument*/&Transform_get_forward_m572_MethodInfo);
		float L_49 = (__this->___currentZoom_10);
		Vector3_t73  L_50 = Vector3_op_Multiply_m573(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/&Vector3_op_Multiply_m573_MethodInfo);
		NullCheck(L_46);
		Vector3_t73  L_51 = Transform_InverseTransformDirection_m859(L_46, L_50, /*hidden argument*/&Transform_InverseTransformDirection_m859_MethodInfo);
		Vector3_t73  L_52 = Vector3_op_Addition_m576(NULL /*static, unused*/, L_44, L_51, /*hidden argument*/&Vector3_op_Addition_m576_MethodInfo);
		V_3 = L_52;
		Transform_t74 * L_53 = (__this->___thisTransform_9);
		NullCheck(L_53);
		Transform_set_localPosition_m832(L_53, V_3, /*hidden argument*/&Transform_set_localPosition_m832_MethodInfo);
		return;
	}
}
// System.Void ZoomCamera::Main()
extern MethodInfo ZoomCamera_Main_m799_MethodInfo;
 void ZoomCamera_Main_m799 (ZoomCamera_t224 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition ZoomCamera
extern Il2CppType Transform_t74_0_0_6;
FieldInfo ZoomCamera_t224____origin_2_FieldInfo = 
{
	"origin"/* name */
	, &Transform_t74_0_0_6/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___origin_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo ZoomCamera_t224____zoom_3_FieldInfo = 
{
	"zoom"/* name */
	, &Single_t170_0_0_6/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___zoom_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo ZoomCamera_t224____zoomMin_4_FieldInfo = 
{
	"zoomMin"/* name */
	, &Single_t170_0_0_6/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___zoomMin_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo ZoomCamera_t224____zoomMax_5_FieldInfo = 
{
	"zoomMax"/* name */
	, &Single_t170_0_0_6/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___zoomMax_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_6;
FieldInfo ZoomCamera_t224____seekTime_6_FieldInfo = 
{
	"seekTime"/* name */
	, &Single_t170_0_0_6/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___seekTime_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo ZoomCamera_t224____smoothZoomIn_7_FieldInfo = 
{
	"smoothZoomIn"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___smoothZoomIn_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t73_0_0_1;
FieldInfo ZoomCamera_t224____defaultLocalPosition_8_FieldInfo = 
{
	"defaultLocalPosition"/* name */
	, &Vector3_t73_0_0_1/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___defaultLocalPosition_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Transform_t74_0_0_1;
FieldInfo ZoomCamera_t224____thisTransform_9_FieldInfo = 
{
	"thisTransform"/* name */
	, &Transform_t74_0_0_1/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___thisTransform_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo ZoomCamera_t224____currentZoom_10_FieldInfo = 
{
	"currentZoom"/* name */
	, &Single_t170_0_0_1/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___currentZoom_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo ZoomCamera_t224____targetZoom_11_FieldInfo = 
{
	"targetZoom"/* name */
	, &Single_t170_0_0_1/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___targetZoom_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo ZoomCamera_t224____zoomVelocity_12_FieldInfo = 
{
	"zoomVelocity"/* name */
	, &Single_t170_0_0_1/* type */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* parent */
	, offsetof(ZoomCamera_t224, ___zoomVelocity_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* ZoomCamera_t224_FieldInfos[] =
{
	&ZoomCamera_t224____origin_2_FieldInfo,
	&ZoomCamera_t224____zoom_3_FieldInfo,
	&ZoomCamera_t224____zoomMin_4_FieldInfo,
	&ZoomCamera_t224____zoomMax_5_FieldInfo,
	&ZoomCamera_t224____seekTime_6_FieldInfo,
	&ZoomCamera_t224____smoothZoomIn_7_FieldInfo,
	&ZoomCamera_t224____defaultLocalPosition_8_FieldInfo,
	&ZoomCamera_t224____thisTransform_9_FieldInfo,
	&ZoomCamera_t224____currentZoom_10_FieldInfo,
	&ZoomCamera_t224____targetZoom_11_FieldInfo,
	&ZoomCamera_t224____zoomVelocity_12_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void ZoomCamera::.ctor()
MethodInfo ZoomCamera__ctor_m796_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ZoomCamera__ctor_m796/* method */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void ZoomCamera::Start()
MethodInfo ZoomCamera_Start_m797_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&ZoomCamera_Start_m797/* method */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void ZoomCamera::Update()
MethodInfo ZoomCamera_Update_m798_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&ZoomCamera_Update_m798/* method */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void ZoomCamera::Main()
MethodInfo ZoomCamera_Main_m799_MethodInfo = 
{
	"Main"/* name */
	, (methodPointerType)&ZoomCamera_Main_m799/* method */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ZoomCamera_t224_MethodInfos[] =
{
	&ZoomCamera__ctor_m796_MethodInfo,
	&ZoomCamera_Start_m797_MethodInfo,
	&ZoomCamera_Update_m798_MethodInfo,
	&ZoomCamera_Main_m799_MethodInfo,
	NULL
};
static MethodInfo* ZoomCamera_t224_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&ZoomCamera_Start_m797_MethodInfo,
	&ZoomCamera_Update_m798_MethodInfo,
	&ZoomCamera_Main_m799_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern Il2CppType ZoomCamera_t224_1_0_0;
struct ZoomCamera_t224;
TypeInfo ZoomCamera_t224_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DUnityScript_Image/* image */
	, NULL/* gc_desc */
	, "ZoomCamera"/* name */
	, ""/* namespaze */
	, ZoomCamera_t224_MethodInfos/* methods */
	, NULL/* properties */
	, ZoomCamera_t224_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ZoomCamera_t224_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ZoomCamera_t224_il2cpp_TypeInfo/* cast_class */
	, &ZoomCamera_t224_0_0_0/* byval_arg */
	, &ZoomCamera_t224_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ZoomCamera_t224)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
