﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>
struct Enumerator_t4073;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t701;

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>)
 void Enumerator__ctor_m23053 (Enumerator_t4073 * __this, LinkedList_1_t701 * ___parent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23054 (Enumerator_t4073 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::get_Current()
 int32_t Enumerator_get_Current_m23055 (Enumerator_t4073 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::MoveNext()
 bool Enumerator_MoveNext_m23056 (Enumerator_t4073 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::Dispose()
 void Enumerator_Dispose_m23057 (Enumerator_t4073 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
