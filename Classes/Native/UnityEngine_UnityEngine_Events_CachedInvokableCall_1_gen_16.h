﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_12.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MarkerBehaviour>
struct CachedInvokableCall_1_t2881  : public InvokableCall_1_t2882
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.MarkerBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
