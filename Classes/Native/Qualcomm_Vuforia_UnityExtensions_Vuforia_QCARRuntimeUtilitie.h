﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.QCARRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie_0.h"
// Vuforia.QCARRuntimeUtilities
struct QCARRuntimeUtilities_t156  : public Object_t
{
};
struct QCARRuntimeUtilities_t156_StaticFields{
	// Vuforia.QCARRuntimeUtilities/WebCamUsed Vuforia.QCARRuntimeUtilities::sWebCamUsed
	int32_t ___sWebCamUsed_0;
};
