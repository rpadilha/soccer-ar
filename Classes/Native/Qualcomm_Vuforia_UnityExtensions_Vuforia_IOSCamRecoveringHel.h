﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.IOSCamRecoveringHelper
struct IOSCamRecoveringHelper_t592  : public Object_t
{
};
struct IOSCamRecoveringHelper_t592_StaticFields{
	// System.Int32 Vuforia.IOSCamRecoveringHelper::CHECKED_FAILED_FRAME_MAX
	int32_t ___CHECKED_FAILED_FRAME_MAX_0;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::WAITED_FRAME_RECOVER_MAX
	int32_t ___WAITED_FRAME_RECOVER_MAX_1;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::RECOVER_ATTEMPT_MAX
	int32_t ___RECOVER_ATTEMPT_MAX_2;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sHasJustResumed
	bool ___sHasJustResumed_3;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sCheckFailedFrameAfterResume
	bool ___sCheckFailedFrameAfterResume_4;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sCheckedFailedFrameCounter
	int32_t ___sCheckedFailedFrameCounter_5;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sWaitToRecoverCameraRestart
	bool ___sWaitToRecoverCameraRestart_6;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sWaitedFrameRecoverCounter
	int32_t ___sWaitedFrameRecoverCounter_7;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sRecoveryAttemptCounter
	int32_t ___sRecoveryAttemptCounter_8;
};
