﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SmoothFollow2D
struct SmoothFollow2D_t222;

// System.Void SmoothFollow2D::.ctor()
 void SmoothFollow2D__ctor_m782 (SmoothFollow2D_t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmoothFollow2D::Start()
 void SmoothFollow2D_Start_m783 (SmoothFollow2D_t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmoothFollow2D::Update()
 void SmoothFollow2D_Update_m784 (SmoothFollow2D_t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmoothFollow2D::Main()
 void SmoothFollow2D_Main_m785 (SmoothFollow2D_t222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
