﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>
struct InternalEnumerator_1_t5047;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"

// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30562 (InternalEnumerator_1_t5047 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563 (InternalEnumerator_1_t5047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::Dispose()
 void InternalEnumerator_1_Dispose_m30564 (InternalEnumerator_1_t5047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30565 (InternalEnumerator_1_t5047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30566 (InternalEnumerator_1_t5047 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
