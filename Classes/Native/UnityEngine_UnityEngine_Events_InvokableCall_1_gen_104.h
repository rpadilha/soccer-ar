﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>
struct UnityAction_1_t3786;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
struct InvokableCall_1_t3785  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::Delegate
	UnityAction_1_t3786 * ___Delegate_0;
};
