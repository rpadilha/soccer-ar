﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.KeyCode>
struct InternalEnumerator_1_t4750;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m28822 (InternalEnumerator_1_t4750 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28823 (InternalEnumerator_1_t4750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::Dispose()
 void InternalEnumerator_1_Dispose_m28824 (InternalEnumerator_1_t4750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m28825 (InternalEnumerator_1_t4750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28826 (InternalEnumerator_1_t4750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
