﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Selectable
struct Selectable_t324;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Selectable>
struct Predicate_1_t3646  : public MulticastDelegate_t373
{
};
