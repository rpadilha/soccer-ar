﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1059;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1060;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.AudioClip
struct AudioClip_t1061  : public Object_t120
{
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t1059 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t1060 * ___m_PCMSetPositionCallback_3;
};
