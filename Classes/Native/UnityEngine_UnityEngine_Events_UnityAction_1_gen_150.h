﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// UnityEngine.Texture2D
struct Texture2D_t196;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Texture2D>
struct UnityAction_1_t4659  : public MulticastDelegate_t373
{
};
