﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t500;
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.Canvas
struct Canvas_t340  : public Behaviour_t559
{
};
struct Canvas_t340_StaticFields{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t500 * ___willRenderCanvases_2;
};
