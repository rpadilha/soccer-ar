﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DataSetImpl
struct DataSetImpl_t610;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Vuforia.Trackable>
struct IEnumerable_1_t629;
// Vuforia.DataSetTrackableBehaviour
struct DataSetTrackableBehaviour_t596;
// Vuforia.TrackableSource
struct TrackableSource_t630;
// UnityEngine.GameObject
struct GameObject_t29;
// Vuforia.Trackable
struct Trackable_t594;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARUnity/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_StorageTy.h"
// Vuforia.DataSet/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet_StorageType.h"

// System.IntPtr Vuforia.DataSetImpl::get_DataSetPtr()
 IntPtr_t121 DataSetImpl_get_DataSetPtr_m3029 (DataSetImpl_t610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.DataSetImpl::get_Path()
 String_t* DataSetImpl_get_Path_m3030 (DataSetImpl_t610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARUnity/StorageType Vuforia.DataSetImpl::get_FileStorageType()
 int32_t DataSetImpl_get_FileStorageType_m3031 (DataSetImpl_t610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetImpl::.ctor(System.IntPtr)
 void DataSetImpl__ctor_m3032 (DataSetImpl_t610 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::Load(System.String)
 bool DataSetImpl_Load_m3033 (DataSetImpl_t610 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::Load(System.String,Vuforia.DataSet/StorageType)
 bool DataSetImpl_Load_m3034 (DataSetImpl_t610 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::Load(System.String,Vuforia.QCARUnity/StorageType)
 bool DataSetImpl_Load_m3035 (DataSetImpl_t610 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSetImpl::GetTrackables()
 Object_t* DataSetImpl_GetTrackables_m3036 (DataSetImpl_t610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DataSetTrackableBehaviour Vuforia.DataSetImpl::CreateTrackable(Vuforia.TrackableSource,System.String)
 DataSetTrackableBehaviour_t596 * DataSetImpl_CreateTrackable_m3037 (DataSetImpl_t610 * __this, TrackableSource_t630 * ___trackableSource, String_t* ___gameObjectName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DataSetTrackableBehaviour Vuforia.DataSetImpl::CreateTrackable(Vuforia.TrackableSource,UnityEngine.GameObject)
 DataSetTrackableBehaviour_t596 * DataSetImpl_CreateTrackable_m3038 (DataSetImpl_t610 * __this, TrackableSource_t630 * ___trackableSource, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::Destroy(Vuforia.Trackable,System.Boolean)
 bool DataSetImpl_Destroy_m3039 (DataSetImpl_t610 * __this, Object_t * ___trackable, bool ___destroyGameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::HasReachedTrackableLimit()
 bool DataSetImpl_HasReachedTrackableLimit_m3040 (DataSetImpl_t610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::Contains(Vuforia.Trackable)
 bool DataSetImpl_Contains_m3041 (DataSetImpl_t610 * __this, Object_t * ___trackable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetImpl::DestroyAllTrackables(System.Boolean)
 void DataSetImpl_DestroyAllTrackables_m3042 (DataSetImpl_t610 * __this, bool ___destroyGameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::ExistsImpl(System.String,Vuforia.QCARUnity/StorageType)
 bool DataSetImpl_ExistsImpl_m3043 (Object_t * __this/* static, unused */, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::CreateImageTargets()
 bool DataSetImpl_CreateImageTargets_m3044 (DataSetImpl_t610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::CreateMultiTargets()
 bool DataSetImpl_CreateMultiTargets_m3045 (DataSetImpl_t610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::CreateCylinderTargets()
 bool DataSetImpl_CreateCylinderTargets_m3046 (DataSetImpl_t610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DataSetImpl::CreateObjectTargets()
 bool DataSetImpl_CreateObjectTargets_m3047 (DataSetImpl_t610 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
