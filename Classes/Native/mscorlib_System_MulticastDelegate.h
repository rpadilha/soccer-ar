﻿#pragma once
#include <stdint.h>
// System.MulticastDelegate
struct MulticastDelegate_t373;
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.MulticastDelegate
struct MulticastDelegate_t373  : public Delegate_t152
{
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t373 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t373 * ___kpm_next_10;
};
