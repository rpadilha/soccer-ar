﻿#pragma once
#include <stdint.h>
// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t11;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.ReconstructionBehaviour>
struct CastHelper_1_t2797 
{
	// T UnityEngine.CastHelper`1<Vuforia.ReconstructionBehaviour>::t
	ReconstructionBehaviour_t11 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.ReconstructionBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
