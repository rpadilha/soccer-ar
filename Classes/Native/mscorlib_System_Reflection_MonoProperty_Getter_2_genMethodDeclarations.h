﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t5240;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
 void Getter_2__ctor_m31574_gshared (Getter_2_t5240 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method);
#define Getter_2__ctor_m31574(__this, ___object, ___method, method) (void)Getter_2__ctor_m31574_gshared((Getter_2_t5240 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
 Object_t * Getter_2_Invoke_m31575_gshared (Getter_2_t5240 * __this, Object_t * ____this, MethodInfo* method);
#define Getter_2_Invoke_m31575(__this, ____this, method) (Object_t *)Getter_2_Invoke_m31575_gshared((Getter_2_t5240 *)__this, (Object_t *)____this, method)
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
 Object_t * Getter_2_BeginInvoke_m31576_gshared (Getter_2_t5240 * __this, Object_t * ____this, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method);
#define Getter_2_BeginInvoke_m31576(__this, ____this, ___callback, ___object, method) (Object_t *)Getter_2_BeginInvoke_m31576_gshared((Getter_2_t5240 *)__this, (Object_t *)____this, (AsyncCallback_t251 *)___callback, (Object_t *)___object, method)
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
 Object_t * Getter_2_EndInvoke_m31577_gshared (Getter_2_t5240 * __this, Object_t * ___result, MethodInfo* method);
#define Getter_2_EndInvoke_m31577(__this, ___result, method) (Object_t *)Getter_2_EndInvoke_m31577_gshared((Getter_2_t5240 *)__this, (Object_t *)___result, method)
