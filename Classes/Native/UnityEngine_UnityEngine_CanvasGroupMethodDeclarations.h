﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CanvasGroup
struct CanvasGroup_t512;
// UnityEngine.Camera
struct Camera_t168;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Boolean UnityEngine.CanvasGroup::get_interactable()
 bool CanvasGroup_get_interactable_m2584 (CanvasGroup_t512 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
 bool CanvasGroup_get_blocksRaycasts_m6397 (CanvasGroup_t512 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
 bool CanvasGroup_get_ignoreParentGroups_m2316 (CanvasGroup_t512 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
 bool CanvasGroup_IsRaycastLocationValid_m6398 (CanvasGroup_t512 * __this, Vector2_t99  ___sp, Camera_t168 * ___eventCamera, MethodInfo* method) IL2CPP_METHOD_ATTR;
