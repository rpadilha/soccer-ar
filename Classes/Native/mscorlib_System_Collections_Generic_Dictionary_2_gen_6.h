﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t175;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2607;
// Vuforia.VirtualButton[]
struct VirtualButtonU5BU5D_t3925;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3325;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,System.Collections.DictionaryEntry>
struct Transform_1_t4004;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>
struct Dictionary_2_t657  : public Object_t
{
	// System.Int32[] System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::table
	Int32U5BU5D_t175* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::linkSlots
	LinkU5BU5D_t2607* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::keySlots
	Int32U5BU5D_t175* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::valueSlots
	VirtualButtonU5BU5D_t3925* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::hcp
	Object_t* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::serialization_info
	SerializationInfo_t1118 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::generation
	int32_t ___generation_14;
};
struct Dictionary_2_t657_StaticFields{
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::INITIAL_SIZE
	int32_t ___INITIAL_SIZE_0;
	// System.Single System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::DEFAULT_LOAD_FACTOR
	float ___DEFAULT_LOAD_FACTOR_1;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::NO_SLOT
	int32_t ___NO_SLOT_2;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::HASH_FLAG
	int32_t ___HASH_FLAG_3;
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::<>f__am$cacheB
	Transform_1_t4004 * ___U3CU3Ef__am$cacheB_15;
};
