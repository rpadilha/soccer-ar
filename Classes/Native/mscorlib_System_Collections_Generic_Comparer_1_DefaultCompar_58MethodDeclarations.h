﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t5331;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::.ctor()
 void DefaultComparer__ctor_m32105 (DefaultComparer_t5331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::Compare(T,T)
 int32_t DefaultComparer_Compare_m32106 (DefaultComparer_t5331 * __this, DateTime_t674  ___x, DateTime_t674  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
