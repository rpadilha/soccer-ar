﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/WordResultData
struct WordResultData_t688;
struct WordResultData_t688_marshaled;

void WordResultData_t688_marshal(const WordResultData_t688& unmarshaled, WordResultData_t688_marshaled& marshaled);
void WordResultData_t688_marshal_back(const WordResultData_t688_marshaled& marshaled, WordResultData_t688& unmarshaled);
void WordResultData_t688_marshal_cleanup(WordResultData_t688_marshaled& marshaled);
