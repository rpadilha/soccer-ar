﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.Trackable>
struct Comparer_1_t3996;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.Trackable>
struct Comparer_1_t3996  : public Object_t
{
};
struct Comparer_1_t3996_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.Trackable>::_default
	Comparer_1_t3996 * ____default_0;
};
