﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>
struct InternalEnumerator_1_t4437;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26631 (InternalEnumerator_1_t4437 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26632 (InternalEnumerator_1_t4437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::Dispose()
 void InternalEnumerator_1_Dispose_m26633 (InternalEnumerator_1_t4437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26634 (InternalEnumerator_1_t4437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::get_Current()
 WebCamDevice_t929  InternalEnumerator_1_get_Current_m26635 (InternalEnumerator_1_t4437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
