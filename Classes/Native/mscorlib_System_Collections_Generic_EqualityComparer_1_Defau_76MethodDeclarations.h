﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t5333;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::.ctor()
 void DefaultComparer__ctor_m32114 (DefaultComparer_t5333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m32115 (DefaultComparer_t5333 * __this, DateTime_t674  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::Equals(T,T)
 bool DefaultComparer_Equals_m32116 (DefaultComparer_t5333 * __this, DateTime_t674  ___x, DateTime_t674  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
