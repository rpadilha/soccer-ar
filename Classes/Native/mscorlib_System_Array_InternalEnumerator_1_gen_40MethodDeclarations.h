﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int32>
struct InternalEnumerator_1_t2850;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m14678 (InternalEnumerator_1_t2850 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14679 (InternalEnumerator_1_t2850 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
 void InternalEnumerator_1_Dispose_m14680 (InternalEnumerator_1_t2850 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m14681 (InternalEnumerator_1_t2850 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m14682 (InternalEnumerator_1_t2850 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
