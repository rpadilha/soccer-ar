﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ResolveEventHandler
struct ResolveEventHandler_t2237;
// System.Object
struct Object_t;
// System.Reflection.Assembly
struct Assembly_t1556;
// System.ResolveEventArgs
struct ResolveEventArgs_t2297;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.ResolveEventHandler::.ctor(System.Object,System.IntPtr)
 void ResolveEventHandler__ctor_m13494 (ResolveEventHandler_t2237 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.ResolveEventHandler::Invoke(System.Object,System.ResolveEventArgs)
 Assembly_t1556 * ResolveEventHandler_Invoke_m13495 (ResolveEventHandler_t2237 * __this, Object_t * ___sender, ResolveEventArgs_t2297 * ___args, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.ResolveEventHandler::BeginInvoke(System.Object,System.ResolveEventArgs,System.AsyncCallback,System.Object)
 Object_t * ResolveEventHandler_BeginInvoke_m13496 (ResolveEventHandler_t2237 * __this, Object_t * ___sender, ResolveEventArgs_t2297 * ___args, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.ResolveEventHandler::EndInvoke(System.IAsyncResult)
 Assembly_t1556 * ResolveEventHandler_EndInvoke_m13497 (ResolveEventHandler_t2237 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
