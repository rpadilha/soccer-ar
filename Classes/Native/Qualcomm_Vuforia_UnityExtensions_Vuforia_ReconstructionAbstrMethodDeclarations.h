﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t46;
// Vuforia.Reconstruction
struct Reconstruction_t620;
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t763;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t758;
// System.Action`1<Vuforia.Prop>
struct Action_1_t126;
// System.Action`1<Vuforia.Surface>
struct Action_1_t127;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t43;
// Vuforia.Prop
struct Prop_t15;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t51;
// Vuforia.Surface
struct Surface_t16;
// System.Collections.Generic.IEnumerable`1<Vuforia.Prop>
struct IEnumerable_1_t764;
// System.Collections.Generic.IEnumerable`1<Vuforia.Surface>
struct IEnumerable_1_t765;
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
struct SmartTerrainRevisionDataU5BU5D_t716;
// Vuforia.QCARManagerImpl/SurfaceData[]
struct SurfaceDataU5BU5D_t717;
// Vuforia.QCARManagerImpl/PropData[]
struct PropDataU5BU5D_t718;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t625;
// UnityEngine.Mesh
struct Mesh_t174;
// System.Int32[]
struct Int32U5BU5D_t175;
// System.Collections.Generic.List`1<Vuforia.Prop>
struct List_1_t766;
// System.Collections.Generic.List`1<Vuforia.Surface>
struct List_1_t767;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Vuforia.QCARManagerImpl/MeshData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Mes.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// Vuforia.Reconstruction Vuforia.ReconstructionAbstractBehaviour::get_Reconstruction()
 Object_t * ReconstructionAbstractBehaviour_get_Reconstruction_m4110 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Start()
 void ReconstructionAbstractBehaviour_Start_m4111 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::OnDrawGizmos()
 void ReconstructionAbstractBehaviour_OnDrawGizmos_m4112 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSmartTerrainEventHandler(Vuforia.ISmartTerrainEventHandler)
 void ReconstructionAbstractBehaviour_RegisterSmartTerrainEventHandler_m4113 (ReconstructionAbstractBehaviour_t46 * __this, Object_t * ___trackableEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::UnregisterSmartTerrainEventHandler(Vuforia.ISmartTerrainEventHandler)
 bool ReconstructionAbstractBehaviour_UnregisterSmartTerrainEventHandler_m4114 (ReconstructionAbstractBehaviour_t46 * __this, Object_t * ___trackableEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterInitializedCallback(System.Action`1<Vuforia.SmartTerrainInitializationInfo>)
 void ReconstructionAbstractBehaviour_RegisterInitializedCallback_m4115 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t758 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterInitializedCallback(System.Action`1<Vuforia.SmartTerrainInitializationInfo>)
 void ReconstructionAbstractBehaviour_UnregisterInitializedCallback_m4116 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t758 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterPropCreatedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m273 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t126 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterPropCreatedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m276 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t126 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterPropUpdatedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_RegisterPropUpdatedCallback_m4117 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t126 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterPropUpdatedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_UnregisterPropUpdatedCallback_m4118 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t126 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterPropDeletedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_RegisterPropDeletedCallback_m4119 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t126 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterPropDeletedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_UnregisterPropDeletedCallback_m4120 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t126 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSurfaceCreatedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m275 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t127 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterSurfaceCreatedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m277 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t127 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSurfaceUpdatedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_RegisterSurfaceUpdatedCallback_m4121 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t127 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterSurfaceUpdatedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_UnregisterSurfaceUpdatedCallback_m4122 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t127 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSurfaceDeletedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_RegisterSurfaceDeletedCallback_m4123 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t127 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterSurfaceDeletedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_UnregisterSurfaceDeletedCallback_m4124 (ReconstructionAbstractBehaviour_t46 * __this, Action_1_t127 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.PropAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::AssociateProp(Vuforia.PropAbstractBehaviour,Vuforia.Prop)
 PropAbstractBehaviour_t43 * ReconstructionAbstractBehaviour_AssociateProp_m278 (ReconstructionAbstractBehaviour_t46 * __this, PropAbstractBehaviour_t43 * ___templateBehaviour, Object_t * ___newProp, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SurfaceAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::AssociateSurface(Vuforia.SurfaceAbstractBehaviour,Vuforia.Surface)
 SurfaceAbstractBehaviour_t51 * ReconstructionAbstractBehaviour_AssociateSurface_m279 (ReconstructionAbstractBehaviour_t46 * __this, SurfaceAbstractBehaviour_t51 * ___templateBehaviour, Object_t * ___newSurface, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::GetActiveProps()
 Object_t* ReconstructionAbstractBehaviour_GetActiveProps_m4125 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::TryGetPropBehaviour(Vuforia.Prop,Vuforia.PropAbstractBehaviour&)
 bool ReconstructionAbstractBehaviour_TryGetPropBehaviour_m4126 (ReconstructionAbstractBehaviour_t46 * __this, Object_t * ___prop, PropAbstractBehaviour_t43 ** ___behaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::GetActiveSurfaces()
 Object_t* ReconstructionAbstractBehaviour_GetActiveSurfaces_m4127 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::TryGetSurfaceBehaviour(Vuforia.Surface,Vuforia.SurfaceAbstractBehaviour&)
 bool ReconstructionAbstractBehaviour_TryGetSurfaceBehaviour_m4128 (ReconstructionAbstractBehaviour_t46 * __this, Object_t * ___surface, SurfaceAbstractBehaviour_t51 ** ___behaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Initialize(Vuforia.Reconstruction)
 void ReconstructionAbstractBehaviour_Initialize_m4129 (ReconstructionAbstractBehaviour_t46 * __this, Object_t * ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Deinitialize()
 void ReconstructionAbstractBehaviour_Deinitialize_m4130 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UpdateSmartTerrainData(Vuforia.QCARManagerImpl/SmartTerrainRevisionData[],Vuforia.QCARManagerImpl/SurfaceData[],Vuforia.QCARManagerImpl/PropData[])
 void ReconstructionAbstractBehaviour_UpdateSmartTerrainData_m4131 (ReconstructionAbstractBehaviour_t46 * __this, SmartTerrainRevisionDataU5BU5D_t716* ___smartTerrainRevisions, SurfaceDataU5BU5D_t717* ___updatedSurfaces, PropDataU5BU5D_t718* ___updatedProps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::SetBehavioursToNotFound()
 void ReconstructionAbstractBehaviour_SetBehavioursToNotFound_m4132 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::ClearOnReset()
 void ReconstructionAbstractBehaviour_ClearOnReset_m4133 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::OnReconstructionRemoved()
 void ReconstructionAbstractBehaviour_OnReconstructionRemoved_m4134 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.PropAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::InstantiatePropBehaviour(Vuforia.PropAbstractBehaviour)
 PropAbstractBehaviour_t43 * ReconstructionAbstractBehaviour_InstantiatePropBehaviour_m4135 (Object_t * __this/* static, unused */, PropAbstractBehaviour_t43 * ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::AssociatePropBehaviour(Vuforia.Prop,Vuforia.PropAbstractBehaviour)
 void ReconstructionAbstractBehaviour_AssociatePropBehaviour_m4136 (ReconstructionAbstractBehaviour_t46 * __this, Object_t * ___trackable, PropAbstractBehaviour_t43 * ___behaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SurfaceAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::InstantiateSurfaceBehaviour(Vuforia.SurfaceAbstractBehaviour)
 SurfaceAbstractBehaviour_t51 * ReconstructionAbstractBehaviour_InstantiateSurfaceBehaviour_m4137 (Object_t * __this/* static, unused */, SurfaceAbstractBehaviour_t51 * ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::AssociateSurfaceBehaviour(Vuforia.Surface,Vuforia.SurfaceAbstractBehaviour)
 void ReconstructionAbstractBehaviour_AssociateSurfaceBehaviour_m4138 (ReconstructionAbstractBehaviour_t46 * __this, Object_t * ___trackable, SurfaceAbstractBehaviour_t51 * ___behaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainTrackable Vuforia.ReconstructionAbstractBehaviour::FindSmartTerrainTrackable(System.Int32)
 Object_t * ReconstructionAbstractBehaviour_FindSmartTerrainTrackable_m4139 (ReconstructionAbstractBehaviour_t46 * __this, int32_t ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::NotifySurfaceEventHandlers(System.Collections.Generic.IEnumerable`1<Vuforia.Surface>,System.Collections.Generic.IEnumerable`1<Vuforia.Surface>,System.Collections.Generic.IEnumerable`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_NotifySurfaceEventHandlers_m4140 (ReconstructionAbstractBehaviour_t46 * __this, Object_t* ___newSurfaces, Object_t* ___updatedSurfaces, Object_t* ___deletedSurfaces, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::NotifyPropEventHandlers(System.Collections.Generic.IEnumerable`1<Vuforia.Prop>,System.Collections.Generic.IEnumerable`1<Vuforia.Prop>,System.Collections.Generic.IEnumerable`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_NotifyPropEventHandlers_m4141 (ReconstructionAbstractBehaviour_t46 * __this, Object_t* ___newProps, Object_t* ___updatedProps, Object_t* ___deletedProps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh Vuforia.ReconstructionAbstractBehaviour::UpdateMesh(Vuforia.QCARManagerImpl/MeshData,UnityEngine.Mesh,System.Boolean)
 Mesh_t174 * ReconstructionAbstractBehaviour_UpdateMesh_m4142 (Object_t * __this/* static, unused */, MeshData_t691  ___meshData, Mesh_t174 * ___oldMesh, bool ___setNormalsUpwards, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Vuforia.ReconstructionAbstractBehaviour::ReadMeshBoundaries(System.Int32,System.IntPtr)
 Int32U5BU5D_t175* ReconstructionAbstractBehaviour_ReadMeshBoundaries_m4143 (Object_t * __this/* static, unused */, int32_t ___numBoundaries, IntPtr_t121 ___boundaryArray, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterDeletedProps(System.Collections.Generic.List`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_UnregisterDeletedProps_m4144 (ReconstructionAbstractBehaviour_t46 * __this, List_1_t766 * ___deletedProps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterDeletedSurfaces(System.Collections.Generic.List`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_UnregisterDeletedSurfaces_m4145 (ReconstructionAbstractBehaviour_t46 * __this, List_1_t767 * ___deletedSurfaces, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UpdateSurfaces(Vuforia.QCARManagerImpl/SmartTerrainRevisionData[],Vuforia.QCARManagerImpl/SurfaceData[])
 void ReconstructionAbstractBehaviour_UpdateSurfaces_m4146 (ReconstructionAbstractBehaviour_t46 * __this, SmartTerrainRevisionDataU5BU5D_t716* ___smartTerrainRevisions, SurfaceDataU5BU5D_t717* ___updatedSurfaceData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UpdateProps(Vuforia.QCARManagerImpl/SmartTerrainRevisionData[],Vuforia.QCARManagerImpl/PropData[])
 void ReconstructionAbstractBehaviour_UpdateProps_m4147 (ReconstructionAbstractBehaviour_t46 * __this, SmartTerrainRevisionDataU5BU5D_t716* ___smartTerrainRevisions, PropDataU5BU5D_t718* ___updatedPropData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_InitializedInEditor()
 bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_InitializedInEditor_m433 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetInitializedInEditor(System.Boolean)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetInitializedInEditor_m434 (ReconstructionAbstractBehaviour_t46 * __this, bool ___initializedInEditor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetMaximumExtentEnabled(System.Boolean)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtentEnabled_m435 (ReconstructionAbstractBehaviour_t46 * __this, bool ___maxExtendEnabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_MaximumExtentEnabled()
 bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtentEnabled_m436 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetMaximumExtent(UnityEngine.Rect)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtent_m437 (ReconstructionAbstractBehaviour_t46 * __this, Rect_t103  ___rectangle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_MaximumExtent()
 Rect_t103  ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtent_m438 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetAutomaticStart(System.Boolean)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetAutomaticStart_m439 (ReconstructionAbstractBehaviour_t46 * __this, bool ___autoStart, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_AutomaticStart()
 bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_AutomaticStart_m440 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetNavMeshUpdates(System.Boolean)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshUpdates_m441 (ReconstructionAbstractBehaviour_t46 * __this, bool ___navMeshUpdates, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_NavMeshUpdates()
 bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshUpdates_m442 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetNavMeshPadding(System.Single)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshPadding_m443 (ReconstructionAbstractBehaviour_t46 * __this, float ___navMeshPadding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_NavMeshPadding()
 float ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshPadding_m444 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.ScaleEditorMeshesByFactor(System.Single)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorMeshesByFactor_m445 (ReconstructionAbstractBehaviour_t46 * __this, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.ScaleEditorPropPositionsByFactor(System.Single)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorPropPositionsByFactor_m446 (ReconstructionAbstractBehaviour_t46 * __this, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::.ctor()
 void ReconstructionAbstractBehaviour__ctor_m432 (ReconstructionAbstractBehaviour_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
