﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackerManagerImpl
struct TrackerManagerImpl_t788;
// Vuforia.StateManager
struct StateManager_t768;

// Vuforia.StateManager Vuforia.TrackerManagerImpl::GetStateManager()
 StateManager_t768 * TrackerManagerImpl_GetStateManager_m4210 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManagerImpl::.ctor()
 void TrackerManagerImpl__ctor_m4211 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
