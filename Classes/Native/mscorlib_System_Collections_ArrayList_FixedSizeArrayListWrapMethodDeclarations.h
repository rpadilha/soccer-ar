﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ArrayList/FixedSizeArrayListWrapper
struct FixedSizeArrayListWrapper_t1877;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1361;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t1259;

// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::.ctor(System.Collections.ArrayList)
 void FixedSizeArrayListWrapper__ctor_m10478 (FixedSizeArrayListWrapper_t1877 * __this, ArrayList_t1361 * ___innerList, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.ArrayList/FixedSizeArrayListWrapper::get_ErrorMessage()
 String_t* FixedSizeArrayListWrapper_get_ErrorMessage_m10479 (FixedSizeArrayListWrapper_t1877 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/FixedSizeArrayListWrapper::get_IsFixedSize()
 bool FixedSizeArrayListWrapper_get_IsFixedSize_m10480 (FixedSizeArrayListWrapper_t1877 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/FixedSizeArrayListWrapper::Add(System.Object)
 int32_t FixedSizeArrayListWrapper_Add_m10481 (FixedSizeArrayListWrapper_t1877 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::AddRange(System.Collections.ICollection)
 void FixedSizeArrayListWrapper_AddRange_m10482 (FixedSizeArrayListWrapper_t1877 * __this, Object_t * ___c, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::Clear()
 void FixedSizeArrayListWrapper_Clear_m10483 (FixedSizeArrayListWrapper_t1877 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::Insert(System.Int32,System.Object)
 void FixedSizeArrayListWrapper_Insert_m10484 (FixedSizeArrayListWrapper_t1877 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::InsertRange(System.Int32,System.Collections.ICollection)
 void FixedSizeArrayListWrapper_InsertRange_m10485 (FixedSizeArrayListWrapper_t1877 * __this, int32_t ___index, Object_t * ___c, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::Remove(System.Object)
 void FixedSizeArrayListWrapper_Remove_m10486 (FixedSizeArrayListWrapper_t1877 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::RemoveAt(System.Int32)
 void FixedSizeArrayListWrapper_RemoveAt_m10487 (FixedSizeArrayListWrapper_t1877 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
