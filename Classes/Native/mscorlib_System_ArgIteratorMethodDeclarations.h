﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ArgIterator
struct ArgIterator_t1771;
// System.Object
struct Object_t;

// System.Boolean System.ArgIterator::Equals(System.Object)
 bool ArgIterator_Equals_m9924 (ArgIterator_t1771 * __this, Object_t * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ArgIterator::GetHashCode()
 int32_t ArgIterator_GetHashCode_m9925 (ArgIterator_t1771 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
