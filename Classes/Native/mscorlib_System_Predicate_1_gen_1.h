﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t128;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t442  : public MulticastDelegate_t373
{
};
