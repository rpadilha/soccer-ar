﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Text>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_92.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Text>
struct CachedInvokableCall_1_t3696  : public InvokableCall_1_t3697
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Text>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
