﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$56
struct $ArrayType$56_t2315;
struct $ArrayType$56_t2315_marshaled;

void $ArrayType$56_t2315_marshal(const $ArrayType$56_t2315& unmarshaled, $ArrayType$56_t2315_marshaled& marshaled);
void $ArrayType$56_t2315_marshal_back(const $ArrayType$56_t2315_marshaled& marshaled, $ArrayType$56_t2315& unmarshaled);
void $ArrayType$56_t2315_marshal_cleanup($ArrayType$56_t2315_marshaled& marshaled);
