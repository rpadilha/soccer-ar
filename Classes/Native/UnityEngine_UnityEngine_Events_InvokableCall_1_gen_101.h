﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
struct UnityAction_1_t3774;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
struct InvokableCall_1_t3773  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>::Delegate
	UnityAction_1_t3774 * ___Delegate_0;
};
