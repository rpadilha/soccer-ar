﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUITexture
struct GUITexture_t100;
// UnityEngine.Texture
struct Texture_t107;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)
 void GUITexture_INTERNAL_get_color_m5719 (GUITexture_t100 * __this, Color_t66 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)
 void GUITexture_INTERNAL_set_color_m5720 (GUITexture_t100 * __this, Color_t66 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUITexture::get_color()
 Color_t66  GUITexture_get_color_m682 (GUITexture_t100 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_color(UnityEngine.Color)
 void GUITexture_set_color_m684 (GUITexture_t100 * __this, Color_t66  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.GUITexture::get_texture()
 Texture_t107 * GUITexture_get_texture_m667 (GUITexture_t100 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_get_pixelInset(UnityEngine.Rect&)
 void GUITexture_INTERNAL_get_pixelInset_m5721 (GUITexture_t100 * __this, Rect_t103 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_set_pixelInset(UnityEngine.Rect&)
 void GUITexture_INTERNAL_set_pixelInset_m5722 (GUITexture_t100 * __this, Rect_t103 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUITexture::get_pixelInset()
 Rect_t103  GUITexture_get_pixelInset_m674 (GUITexture_t100 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_pixelInset(UnityEngine.Rect)
 void GUITexture_set_pixelInset_m681 (GUITexture_t100 * __this, Rect_t103  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
