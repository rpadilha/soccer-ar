﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.BoneWeight
struct BoneWeight_t983;
// System.Object
struct Object_t;
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"

// System.Single UnityEngine.BoneWeight::get_weight0()
 float BoneWeight_get_weight0_m5668 (BoneWeight_t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoneWeight::set_weight0(System.Single)
 void BoneWeight_set_weight0_m5669 (BoneWeight_t983 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.BoneWeight::get_weight1()
 float BoneWeight_get_weight1_m5670 (BoneWeight_t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoneWeight::set_weight1(System.Single)
 void BoneWeight_set_weight1_m5671 (BoneWeight_t983 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.BoneWeight::get_weight2()
 float BoneWeight_get_weight2_m5672 (BoneWeight_t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoneWeight::set_weight2(System.Single)
 void BoneWeight_set_weight2_m5673 (BoneWeight_t983 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.BoneWeight::get_weight3()
 float BoneWeight_get_weight3_m5674 (BoneWeight_t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoneWeight::set_weight3(System.Single)
 void BoneWeight_set_weight3_m5675 (BoneWeight_t983 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.BoneWeight::get_boneIndex0()
 int32_t BoneWeight_get_boneIndex0_m5676 (BoneWeight_t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoneWeight::set_boneIndex0(System.Int32)
 void BoneWeight_set_boneIndex0_m5677 (BoneWeight_t983 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.BoneWeight::get_boneIndex1()
 int32_t BoneWeight_get_boneIndex1_m5678 (BoneWeight_t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoneWeight::set_boneIndex1(System.Int32)
 void BoneWeight_set_boneIndex1_m5679 (BoneWeight_t983 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.BoneWeight::get_boneIndex2()
 int32_t BoneWeight_get_boneIndex2_m5680 (BoneWeight_t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoneWeight::set_boneIndex2(System.Int32)
 void BoneWeight_set_boneIndex2_m5681 (BoneWeight_t983 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.BoneWeight::get_boneIndex3()
 int32_t BoneWeight_get_boneIndex3_m5682 (BoneWeight_t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoneWeight::set_boneIndex3(System.Int32)
 void BoneWeight_set_boneIndex3_m5683 (BoneWeight_t983 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.BoneWeight::GetHashCode()
 int32_t BoneWeight_GetHashCode_m5684 (BoneWeight_t983 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.BoneWeight::Equals(System.Object)
 bool BoneWeight_Equals_m5685 (BoneWeight_t983 * __this, Object_t * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.BoneWeight::op_Equality(UnityEngine.BoneWeight,UnityEngine.BoneWeight)
 bool BoneWeight_op_Equality_m5686 (Object_t * __this/* static, unused */, BoneWeight_t983  ___lhs, BoneWeight_t983  ___rhs, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.BoneWeight::op_Inequality(UnityEngine.BoneWeight,UnityEngine.BoneWeight)
 bool BoneWeight_op_Inequality_m5687 (Object_t * __this/* static, unused */, BoneWeight_t983  ___lhs, BoneWeight_t983  ___rhs, MethodInfo* method) IL2CPP_METHOD_ATTR;
