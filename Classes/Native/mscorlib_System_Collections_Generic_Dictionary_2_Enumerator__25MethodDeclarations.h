﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t4724;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t999;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t1016;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m28627(__this, ___dictionary, method) (void)Enumerator__ctor_m18373_gshared((Enumerator_t3460 *)__this, (Dictionary_2_t3452 *)___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m28628(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m18374_gshared((Enumerator_t3460 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28629(__this, method) (DictionaryEntry_t1355 )Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18375_gshared((Enumerator_t3460 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28630(__this, method) (Object_t *)Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18376_gshared((Enumerator_t3460 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28631(__this, method) (Object_t *)Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18377_gshared((Enumerator_t3460 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m28632(__this, method) (bool)Enumerator_MoveNext_m18378_gshared((Enumerator_t3460 *)__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
 KeyValuePair_2_t4722  Enumerator_get_Current_m28633 (Enumerator_t4724 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m28634(__this, method) (String_t*)Enumerator_get_CurrentKey_m18380_gshared((Enumerator_t3460 *)__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m28635(__this, method) (GUIStyle_t999 *)Enumerator_get_CurrentValue_m18381_gshared((Enumerator_t3460 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyState()
#define Enumerator_VerifyState_m28636(__this, method) (void)Enumerator_VerifyState_m18382_gshared((Enumerator_t3460 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m28637(__this, method) (void)Enumerator_VerifyCurrent_m18383_gshared((Enumerator_t3460 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m28638(__this, method) (void)Enumerator_Dispose_m18384_gshared((Enumerator_t3460 *)__this, method)
