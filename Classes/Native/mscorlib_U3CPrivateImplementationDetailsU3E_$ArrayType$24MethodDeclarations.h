﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$24
struct $ArrayType$24_t2316;
struct $ArrayType$24_t2316_marshaled;

void $ArrayType$24_t2316_marshal(const $ArrayType$24_t2316& unmarshaled, $ArrayType$24_t2316_marshaled& marshaled);
void $ArrayType$24_t2316_marshal_back(const $ArrayType$24_t2316_marshaled& marshaled, $ArrayType$24_t2316& unmarshaled);
void $ArrayType$24_t2316_marshal_cleanup($ArrayType$24_t2316_marshaled& marshaled);
