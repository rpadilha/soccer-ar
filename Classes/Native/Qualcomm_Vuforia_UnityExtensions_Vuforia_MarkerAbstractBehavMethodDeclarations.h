﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t32;
// Vuforia.Marker
struct Marker_t667;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.GameObject
struct GameObject_t29;

// Vuforia.Marker Vuforia.MarkerAbstractBehaviour::get_Marker()
 Object_t * MarkerAbstractBehaviour_get_Marker_m4253 (MarkerAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::.ctor()
 void MarkerAbstractBehaviour__ctor_m349 (MarkerAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::InternalUnregisterTrackable()
 void MarkerAbstractBehaviour_InternalUnregisterTrackable_m355 (MarkerAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::CorrectScaleImpl()
 bool MarkerAbstractBehaviour_CorrectScaleImpl_m356 (MarkerAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.get_MarkerID()
 int32_t MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m357 (MarkerAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.SetMarkerID(System.Int32)
 bool MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m358 (MarkerAbstractBehaviour_t32 * __this, int32_t ___markerID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.InitializeMarker(Vuforia.Marker)
 void MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m359 (MarkerAbstractBehaviour_t32 * __this, Object_t * ___marker, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m350 (MarkerAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m351 (MarkerAbstractBehaviour_t32 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t74 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m352 (MarkerAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t29 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m353 (MarkerAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
