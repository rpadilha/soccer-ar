﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinder
struct TargetFinder_t660;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerable_1_t777;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t23;
// UnityEngine.GameObject
struct GameObject_t29;
// System.Collections.Generic.IEnumerable`1<Vuforia.ImageTarget>
struct IEnumerable_1_t778;
// Vuforia.TargetFinder/InitState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Vuforia.TargetFinder/UpdateState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Boolean Vuforia.TargetFinder::StartInit(System.String,System.String)
// Vuforia.TargetFinder/InitState Vuforia.TargetFinder::GetInitState()
// System.Boolean Vuforia.TargetFinder::Deinit()
// System.Boolean Vuforia.TargetFinder::StartRecognition()
// System.Boolean Vuforia.TargetFinder::Stop()
// System.Void Vuforia.TargetFinder::SetUIScanlineColor(UnityEngine.Color)
// System.Void Vuforia.TargetFinder::SetUIPointColor(UnityEngine.Color)
// System.Boolean Vuforia.TargetFinder::IsRequesting()
// Vuforia.TargetFinder/UpdateState Vuforia.TargetFinder::Update()
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult> Vuforia.TargetFinder::GetResults()
// Vuforia.ImageTargetAbstractBehaviour Vuforia.TargetFinder::EnableTracking(Vuforia.TargetFinder/TargetSearchResult,System.String)
// Vuforia.ImageTargetAbstractBehaviour Vuforia.TargetFinder::EnableTracking(Vuforia.TargetFinder/TargetSearchResult,UnityEngine.GameObject)
// System.Void Vuforia.TargetFinder::ClearTrackables(System.Boolean)
// System.Collections.Generic.IEnumerable`1<Vuforia.ImageTarget> Vuforia.TargetFinder::GetImageTargets()
// System.Void Vuforia.TargetFinder::.ctor()
 void TargetFinder__ctor_m4181 (TargetFinder_t660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
