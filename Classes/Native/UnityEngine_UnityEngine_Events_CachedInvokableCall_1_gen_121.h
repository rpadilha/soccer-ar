﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_123.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionAbstractBehaviour>
struct CachedInvokableCall_1_t4236  : public InvokableCall_1_t4237
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
