﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.PermissionSet
struct PermissionSet_t1971;

// System.Void System.Security.PermissionSet::.ctor()
 void PermissionSet__ctor_m12350 (PermissionSet_t1971 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
