﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t701;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t859;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3338;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Int32[]
struct Int32U5BU5D_t175;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor()
 void LinkedList_1__ctor_m4996 (LinkedList_1_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void LinkedList_1__ctor_m23034 (LinkedList_1_t701 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
 void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23035 (LinkedList_1_t701 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void LinkedList_1_System_Collections_ICollection_CopyTo_m23036 (LinkedList_1_t701 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
 Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23037 (LinkedList_1_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m23038 (LinkedList_1_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
 bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23039 (LinkedList_1_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
 bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m23040 (LinkedList_1_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
 Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m23041 (LinkedList_1_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedList_1_VerifyReferencedNode_m23042 (LinkedList_1_t701 * __this, LinkedListNode_1_t859 * ___node, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddLast(T)
 LinkedListNode_1_t859 * LinkedList_1_AddLast_m5009 (LinkedList_1_t701 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Clear()
 void LinkedList_1_Clear_m23043 (LinkedList_1_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(T)
 bool LinkedList_1_Contains_m5008 (LinkedList_1_t701 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32)
 void LinkedList_1_CopyTo_m23044 (LinkedList_1_t701 * __this, Int32U5BU5D_t175* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::Find(T)
 LinkedListNode_1_t859 * LinkedList_1_Find_m23045 (LinkedList_1_t701 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Int32>::GetEnumerator()
 Enumerator_t4073  LinkedList_1_GetEnumerator_m23046 (LinkedList_1_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void LinkedList_1_GetObjectData_m23047 (LinkedList_1_t701 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::OnDeserialization(System.Object)
 void LinkedList_1_OnDeserialization_m23048 (LinkedList_1_t701 * __this, Object_t * ___sender, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(T)
 bool LinkedList_1_Remove_m5010 (LinkedList_1_t701 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedList_1_Remove_m5387 (LinkedList_1_t701 * __this, LinkedListNode_1_t859 * ___node, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count()
 int32_t LinkedList_1_get_Count_m5017 (LinkedList_1_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::get_First()
 LinkedListNode_1_t859 * LinkedList_1_get_First_m5018 (LinkedList_1_t701 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
