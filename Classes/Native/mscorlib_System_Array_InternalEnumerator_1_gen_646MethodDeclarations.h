﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>
struct InternalEnumerator_1_t5190;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Globalization.DateTimeFormatFlags
#include "mscorlib_System_Globalization_DateTimeFormatFlags.h"

// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31324 (InternalEnumerator_1_t5190 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31325 (InternalEnumerator_1_t5190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::Dispose()
 void InternalEnumerator_1_Dispose_m31326 (InternalEnumerator_1_t5190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31327 (InternalEnumerator_1_t5190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31328 (InternalEnumerator_1_t5190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
