﻿#pragma once
#include <stdint.h>
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t140;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t150  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Reflection.MethodInfo>::_items
	MethodInfoU5BU5D_t140* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Reflection.MethodInfo>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Reflection.MethodInfo>::_version
	int32_t ____version_3;
};
struct List_1_t150_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<System.Reflection.MethodInfo>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<System.Reflection.MethodInfo>::EmptyArray
	MethodInfoU5BU5D_t140* ___EmptyArray_4;
};
