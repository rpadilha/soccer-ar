﻿#pragma once
#include <stdint.h>
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t34;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.WordAbstractBehaviour>
struct CastHelper_1_t4207 
{
	// T UnityEngine.CastHelper`1<Vuforia.WordAbstractBehaviour>::t
	WordAbstractBehaviour_t34 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.WordAbstractBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
