﻿#pragma once
#include <stdint.h>
// System.Collections.IEnumerable
struct IEnumerable_t1179;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// Vuforia.DataSet
struct DataSet_t612;
// System.Object
#include "mscorlib_System_Object.h"
// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t4044  : public Object_t
{
	// System.Collections.IEnumerable System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::source
	Object_t * ___source_0;
	// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::<$s_41>__0
	Object_t * ___U3C$s_41U3E__0_1;
	// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::<element>__1
	DataSet_t612 * ___U3CelementU3E__1_2;
	// System.Int32 System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::$PC
	int32_t ___$PC_3;
	// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::$current
	DataSet_t612 * ___$current_4;
	// System.Collections.IEnumerable System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<Vuforia.DataSet>::<$>source
	Object_t * ___U3C$U3Esource_5;
};
