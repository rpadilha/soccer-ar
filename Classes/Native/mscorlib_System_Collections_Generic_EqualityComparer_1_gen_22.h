﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct EqualityComparer_1_t3692;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct EqualityComparer_1_t3692  : public Object_t
{
};
struct EqualityComparer_1_t3692_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.StencilMaterial/MatEntry>::_default
	EqualityComparer_1_t3692 * ____default_0;
};
