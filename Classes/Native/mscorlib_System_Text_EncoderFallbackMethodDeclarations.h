﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderFallback
struct EncoderFallback_t2203;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2194;

// System.Void System.Text.EncoderFallback::.ctor()
 void EncoderFallback__ctor_m12442 (EncoderFallback_t2203 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.EncoderFallback::.cctor()
 void EncoderFallback__cctor_m12443 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_ExceptionFallback()
 EncoderFallback_t2203 * EncoderFallback_get_ExceptionFallback_m12444 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_ReplacementFallback()
 EncoderFallback_t2203 * EncoderFallback_get_ReplacementFallback_m12445 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_StandardSafeFallback()
 EncoderFallback_t2203 * EncoderFallback_get_StandardSafeFallback_m12446 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.EncoderFallback::CreateFallbackBuffer()
