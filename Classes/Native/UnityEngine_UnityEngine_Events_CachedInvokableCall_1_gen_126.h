﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_128.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetAbstractBehaviour>
struct CachedInvokableCall_1_t4479  : public InvokableCall_1_t4480
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
