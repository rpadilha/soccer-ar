﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t582;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
 void AssemblyTrademarkAttribute__ctor_m2756 (AssemblyTrademarkAttribute_t582 * __this, String_t* ___trademark, MethodInfo* method) IL2CPP_METHOD_ATTR;
