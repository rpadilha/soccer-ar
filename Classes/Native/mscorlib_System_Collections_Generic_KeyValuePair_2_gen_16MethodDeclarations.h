﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct KeyValuePair_2_t4271;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t51;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m25042 (KeyValuePair_2_t4271 * __this, int32_t ___key, SurfaceAbstractBehaviour_t51 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Key()
 int32_t KeyValuePair_2_get_Key_m25043 (KeyValuePair_2_t4271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m25044 (KeyValuePair_2_t4271 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Value()
 SurfaceAbstractBehaviour_t51 * KeyValuePair_2_get_Value_m25045 (KeyValuePair_2_t4271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m25046 (KeyValuePair_2_t4271 * __this, SurfaceAbstractBehaviour_t51 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>::ToString()
 String_t* KeyValuePair_2_ToString_m25047 (KeyValuePair_2_t4271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
