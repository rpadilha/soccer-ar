﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_6.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>
struct CachedInvokableCall_1_t2801  : public InvokableCall_1_t2802
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
