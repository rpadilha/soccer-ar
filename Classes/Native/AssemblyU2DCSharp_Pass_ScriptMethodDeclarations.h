﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Pass_Script
struct Pass_Script_t104;

// System.Void Pass_Script::.ctor()
 void Pass_Script__ctor_m173 (Pass_Script_t104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pass_Script::Update()
 void Pass_Script_Update_m174 (Pass_Script_t104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
