﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButtonAbstractBehaviour,Vuforia.VirtualButtonAbstractBehaviour>
struct Transform_1_t4470;
// System.Object
struct Object_t;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t30;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButtonAbstractBehaviour,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m26889 (Transform_1_t4470 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButtonAbstractBehaviour,Vuforia.VirtualButtonAbstractBehaviour>::Invoke(TKey,TValue)
 VirtualButtonAbstractBehaviour_t30 * Transform_1_Invoke_m26890 (Transform_1_t4470 * __this, int32_t ___key, VirtualButtonAbstractBehaviour_t30 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButtonAbstractBehaviour,Vuforia.VirtualButtonAbstractBehaviour>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m26891 (Transform_1_t4470 * __this, int32_t ___key, VirtualButtonAbstractBehaviour_t30 * ___value, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButtonAbstractBehaviour,Vuforia.VirtualButtonAbstractBehaviour>::EndInvoke(System.IAsyncResult)
 VirtualButtonAbstractBehaviour_t30 * Transform_1_EndInvoke_m26892 (Transform_1_t4470 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
