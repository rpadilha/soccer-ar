﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct DefaultComparer_t4206;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t742;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor()
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_0MethodDeclarations.h"
#define DefaultComparer__ctor_m24426(__this, method) (void)DefaultComparer__ctor_m14665_gshared((DefaultComparer_t2847 *)__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::GetHashCode(T)
#define DefaultComparer_GetHashCode_m24427(__this, ___obj, method) (int32_t)DefaultComparer_GetHashCode_m14666_gshared((DefaultComparer_t2847 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Equals(T,T)
#define DefaultComparer_Equals_m24428(__this, ___x, ___y, method) (bool)DefaultComparer_Equals_m14667_gshared((DefaultComparer_t2847 *)__this, (Object_t *)___x, (Object_t *)___y, method)
