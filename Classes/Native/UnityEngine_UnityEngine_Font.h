﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityEngine.Font>
struct Action_1_t507;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t1079;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Font
struct Font_t332  : public Object_t120
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t1079 * ___m_FontTextureRebuildCallback_3;
};
struct Font_t332_StaticFields{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t507 * ___textureRebuilt_2;
};
