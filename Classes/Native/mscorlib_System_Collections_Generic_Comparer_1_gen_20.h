﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>
struct Comparer_1_t3845;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>
struct Comparer_1_t3845  : public Object_t
{
};
struct Comparer_1_t3845_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>::_default
	Comparer_1_t3845 * ____default_0;
};
