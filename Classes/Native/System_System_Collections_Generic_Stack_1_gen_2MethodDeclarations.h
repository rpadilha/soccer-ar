﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_t3506;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct IEnumerator_1_t3508;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t345;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_1.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m18711(__this, method) (void)Stack_1__ctor_m16404_gshared((Stack_1_t3209 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m18712(__this, method) (bool)Stack_1_System_Collections_ICollection_get_IsSynchronized_m16405_gshared((Stack_1_t3209 *)__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m18713(__this, method) (Object_t *)Stack_1_System_Collections_ICollection_get_SyncRoot_m16406_gshared((Stack_1_t3209 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m18714(__this, ___dest, ___idx, method) (void)Stack_1_System_Collections_ICollection_CopyTo_m16407_gshared((Stack_1_t3209 *)__this, (Array_t *)___dest, (int32_t)___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18715(__this, method) (Object_t*)Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16408_gshared((Stack_1_t3209 *)__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m18716(__this, method) (Object_t *)Stack_1_System_Collections_IEnumerable_GetEnumerator_m16409_gshared((Stack_1_t3209 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Peek()
#define Stack_1_Peek_m18717(__this, method) (List_1_t345 *)Stack_1_Peek_m16410_gshared((Stack_1_t3209 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Pop()
#define Stack_1_Pop_m18718(__this, method) (List_1_t345 *)Stack_1_Pop_m16411_gshared((Stack_1_t3209 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Push(T)
#define Stack_1_Push_m18719(__this, ___t, method) (void)Stack_1_Push_m16412_gshared((Stack_1_t3209 *)__this, (Object_t *)___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_Count()
#define Stack_1_get_Count_m18720(__this, method) (int32_t)Stack_1_get_Count_m16413_gshared((Stack_1_t3209 *)__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::GetEnumerator()
 Enumerator_t3509  Stack_1_GetEnumerator_m18721 (Stack_1_t3506 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
