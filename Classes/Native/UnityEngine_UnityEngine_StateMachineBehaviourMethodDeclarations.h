﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_t1121;
// UnityEngine.Animator
struct Animator_t404;
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"

// System.Void UnityEngine.StateMachineBehaviour::.ctor()
 void StateMachineBehaviour__ctor_m6506 (StateMachineBehaviour_t1121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
 void StateMachineBehaviour_OnStateEnter_m6507 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, AnimatorStateInfo_t1064  ___stateInfo, int32_t ___layerIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
 void StateMachineBehaviour_OnStateUpdate_m6508 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, AnimatorStateInfo_t1064  ___stateInfo, int32_t ___layerIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
 void StateMachineBehaviour_OnStateExit_m6509 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, AnimatorStateInfo_t1064  ___stateInfo, int32_t ___layerIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
 void StateMachineBehaviour_OnStateMove_m6510 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, AnimatorStateInfo_t1064  ___stateInfo, int32_t ___layerIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
 void StateMachineBehaviour_OnStateIK_m6511 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, AnimatorStateInfo_t1064  ___stateInfo, int32_t ___layerIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
 void StateMachineBehaviour_OnStateMachineEnter_m6512 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, int32_t ___stateMachinePathHash, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
 void StateMachineBehaviour_OnStateMachineExit_m6513 (StateMachineBehaviour_t1121 * __this, Animator_t404 * ___animator, int32_t ___stateMachinePathHash, MethodInfo* method) IL2CPP_METHOD_ATTR;
