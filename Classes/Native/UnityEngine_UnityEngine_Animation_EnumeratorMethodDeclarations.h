﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Animation/Enumerator
struct Enumerator_t1071;
// System.Object
struct Object_t;
// UnityEngine.Animation
struct Animation_t181;

// System.Void UnityEngine.Animation/Enumerator::.ctor(UnityEngine.Animation)
 void Enumerator__ctor_m6314 (Enumerator_t1071 * __this, Animation_t181 * ___outer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Animation/Enumerator::get_Current()
 Object_t * Enumerator_get_Current_m6315 (Enumerator_t1071 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation/Enumerator::MoveNext()
 bool Enumerator_MoveNext_m6316 (Enumerator_t1071 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
