﻿#pragma once
#include <stdint.h>
// Vuforia.IBehaviourComponentFactory
struct IBehaviourComponentFactory_t153;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.BehaviourComponentFactory
struct BehaviourComponentFactory_t637  : public Object_t
{
};
struct BehaviourComponentFactory_t637_StaticFields{
	// Vuforia.IBehaviourComponentFactory Vuforia.BehaviourComponentFactory::sInstance
	Object_t * ___sInstance_0;
};
