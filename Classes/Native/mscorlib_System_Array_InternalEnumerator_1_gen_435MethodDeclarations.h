﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>
struct InternalEnumerator_1_t4757;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m28852 (InternalEnumerator_1_t4757 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28853 (InternalEnumerator_1_t4757 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::Dispose()
 void InternalEnumerator_1_Dispose_m28854 (InternalEnumerator_1_t4757 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m28855 (InternalEnumerator_1_t4757 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Edge>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28856 (InternalEnumerator_1_t4757 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
