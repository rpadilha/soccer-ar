﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.RawImage
struct RawImage_t387;
// UnityEngine.Texture
struct Texture_t107;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t345;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void UnityEngine.UI.RawImage::.ctor()
 void RawImage__ctor_m1509 (RawImage_t387 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.UI.RawImage::get_mainTexture()
 Texture_t107 * RawImage_get_mainTexture_m1510 (RawImage_t387 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.UI.RawImage::get_texture()
 Texture_t107 * RawImage_get_texture_m1511 (RawImage_t387 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
 void RawImage_set_texture_m1512 (RawImage_t387 * __this, Texture_t107 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.UI.RawImage::get_uvRect()
 Rect_t103  RawImage_get_uvRect_m1513 (RawImage_t387 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::set_uvRect(UnityEngine.Rect)
 void RawImage_set_uvRect_m1514 (RawImage_t387 * __this, Rect_t103  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::SetNativeSize()
 void RawImage_SetNativeSize_m1515 (RawImage_t387 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
 void RawImage_OnFillVBO_m1516 (RawImage_t387 * __this, List_1_t345 * ___vbo, MethodInfo* method) IL2CPP_METHOD_ATTR;
