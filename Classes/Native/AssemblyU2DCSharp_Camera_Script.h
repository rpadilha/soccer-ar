﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Camera_Script
struct Camera_Script_t75  : public MonoBehaviour_t10
{
	// UnityEngine.Transform Camera_Script::target
	Transform_t74 * ___target_2;
	// UnityEngine.Vector3 Camera_Script::targetOffsetPos
	Vector3_t73  ___targetOffsetPos_3;
	// UnityEngine.Vector3 Camera_Script::oldPos
	Vector3_t73  ___oldPos_4;
};
