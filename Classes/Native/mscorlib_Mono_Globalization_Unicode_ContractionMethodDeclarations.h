﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.Contraction
struct Contraction_t1785;
// System.Char[]
struct CharU5BU5D_t378;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void Mono.Globalization.Unicode.Contraction::.ctor(System.Char[],System.String,System.Byte[])
 void Contraction__ctor_m9941 (Contraction_t1785 * __this, CharU5BU5D_t378* ___source, String_t* ___replacement, ByteU5BU5D_t653* ___sortkey, MethodInfo* method) IL2CPP_METHOD_ATTR;
