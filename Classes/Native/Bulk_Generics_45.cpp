﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IComparer_1_t7002_il2cpp_TypeInfo;

// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Int32 System.Collections.Generic.IComparer`1<UnityEngine.Events.BaseInvokableCall>::Compare(T,T)
// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
static ParameterInfo IComparer_1_t7002_IComparer_1_Compare_m39306_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparer_1_Compare_m39306_GenericMethod;
// System.Int32 System.Collections.Generic.IComparer`1<UnityEngine.Events.BaseInvokableCall>::Compare(T,T)
MethodInfo IComparer_1_Compare_m39306_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &IComparer_1_t7002_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, IComparer_1_t7002_IComparer_1_Compare_m39306_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparer_1_Compare_m39306_GenericMethod/* genericMethod */

};
static MethodInfo* IComparer_1_t7002_MethodInfos[] =
{
	&IComparer_1_Compare_m39306_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparer_1_t7002_0_0_0;
extern Il2CppType IComparer_1_t7002_1_0_0;
struct IComparer_1_t7002;
extern Il2CppGenericClass IComparer_1_t7002_GenericClass;
TypeInfo IComparer_1_t7002_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IComparer_1_t7002_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparer_1_t7002_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparer_1_t7002_il2cpp_TypeInfo/* cast_class */
	, &IComparer_1_t7002_0_0_0/* byval_arg */
	, &IComparer_1_t7002_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparer_1_t7002_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t7003_il2cpp_TypeInfo;



// System.Int32 System.IComparable`1<UnityEngine.Events.BaseInvokableCall>::CompareTo(T)
// Metadata Definition System.IComparable`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
static ParameterInfo IComparable_1_t7003_IComparable_1_CompareTo_m39307_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m39307_GenericMethod;
// System.Int32 System.IComparable`1<UnityEngine.Events.BaseInvokableCall>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m39307_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t7003_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IComparable_1_t7003_IComparable_1_CompareTo_m39307_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m39307_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t7003_MethodInfos[] =
{
	&IComparable_1_CompareTo_m39307_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t7003_0_0_0;
extern Il2CppType IComparable_1_t7003_1_0_0;
struct IComparable_1_t7003;
extern Il2CppGenericClass IComparable_1_t7003_GenericClass;
TypeInfo IComparable_1_t7003_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t7003_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t7003_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t7003_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t7003_0_0_0/* byval_arg */
	, &IComparable_1_t7003_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t7003_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_56.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DefaultComparer_t5023_il2cpp_TypeInfo;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_56MethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
extern TypeInfo IComparable_t184_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_55MethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern MethodInfo Comparer_1__ctor_m30431_MethodInfo;
extern MethodInfo IComparable_1_CompareTo_m39307_MethodInfo;
extern MethodInfo IComparable_CompareTo_m13522_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;


// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Events.BaseInvokableCall>::.ctor()
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Events.BaseInvokableCall>::Compare(T,T)
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer__ctor_m30435_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Events.BaseInvokableCall>::.ctor()
MethodInfo DefaultComparer__ctor_m30435_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultComparer__ctor_m14676_gshared/* method */
	, &DefaultComparer_t5023_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer__ctor_m30435_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
static ParameterInfo DefaultComparer_t5023_DefaultComparer_Compare_m30436_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_Compare_m30436_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Events.BaseInvokableCall>::Compare(T,T)
MethodInfo DefaultComparer_Compare_m30436_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&DefaultComparer_Compare_m14677_gshared/* method */
	, &DefaultComparer_t5023_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, DefaultComparer_t5023_DefaultComparer_Compare_m30436_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_Compare_m30436_GenericMethod/* genericMethod */

};
static MethodInfo* DefaultComparer_t5023_MethodInfos[] =
{
	&DefaultComparer__ctor_m30435_MethodInfo,
	&DefaultComparer_Compare_m30436_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo DefaultComparer_Compare_m30436_MethodInfo;
extern MethodInfo Comparer_1_System_Collections_IComparer_Compare_m30433_MethodInfo;
static MethodInfo* DefaultComparer_t5023_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&DefaultComparer_Compare_m30436_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m30433_MethodInfo,
	&DefaultComparer_Compare_m30436_MethodInfo,
};
extern TypeInfo IComparer_t1356_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair DefaultComparer_t5023_InterfacesOffsets[] = 
{
	{ &IComparer_1_t7002_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern TypeInfo Comparer_1_t5022_il2cpp_TypeInfo;
extern TypeInfo Comparer_1_t5022_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t5023_il2cpp_TypeInfo;
extern MethodInfo DefaultComparer__ctor_m30435_MethodInfo;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
extern MethodInfo Comparer_1_Compare_m50007_MethodInfo;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
extern TypeInfo IComparable_1_t7003_il2cpp_TypeInfo;
static Il2CppRGCTXData DefaultComparer_t5023_RGCTXData[12] = 
{
	&IComparable_1_t7003_0_0_0/* Type Usage */,
	&BaseInvokableCall_t1127_0_0_0/* Type Usage */,
	&Comparer_1_t5022_il2cpp_TypeInfo/* Class Usage */,
	&Comparer_1_t5022_il2cpp_TypeInfo/* Static Usage */,
	&DefaultComparer_t5023_il2cpp_TypeInfo/* Class Usage */,
	&DefaultComparer__ctor_m30435_MethodInfo/* Method Usage */,
	&BaseInvokableCall_t1127_il2cpp_TypeInfo/* Class Usage */,
	&Comparer_1_Compare_m50007_MethodInfo/* Method Usage */,
	&Comparer_1__ctor_m30431_MethodInfo/* Method Usage */,
	&BaseInvokableCall_t1127_il2cpp_TypeInfo/* Class Usage */,
	&IComparable_1_t7003_il2cpp_TypeInfo/* Class Usage */,
	&IComparable_1_CompareTo_m39307_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType DefaultComparer_t5023_0_0_0;
extern Il2CppType DefaultComparer_t5023_1_0_0;
extern TypeInfo Comparer_1_t5022_il2cpp_TypeInfo;
struct DefaultComparer_t5023;
extern Il2CppGenericClass DefaultComparer_t5023_GenericClass;
extern TypeInfo Comparer_1_t1854_il2cpp_TypeInfo;
TypeInfo DefaultComparer_t5023_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, DefaultComparer_t5023_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Comparer_1_t5022_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Comparer_1_t1854_il2cpp_TypeInfo/* nested_in */
	, &DefaultComparer_t5023_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultComparer_t5023_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultComparer_t5023_il2cpp_TypeInfo/* cast_class */
	, &DefaultComparer_t5023_0_0_0/* byval_arg */
	, &DefaultComparer_t5023_1_0_0/* this_arg */
	, DefaultComparer_t5023_InterfacesOffsets/* interface_offsets */
	, &DefaultComparer_t5023_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, DefaultComparer_t5023_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultComparer_t5023)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Comparison`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Comparison_1_gen_56.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Comparison_1_t5015_il2cpp_TypeInfo;
// System.Comparison`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Comparison_1_gen_56MethodDeclarations.h"

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Object,System.IntPtr)
// System.Int32 System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::Invoke(T,T)
// System.IAsyncResult System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
// System.Int32 System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::EndInvoke(System.IAsyncResult)
// Metadata Definition System.Comparison`1<UnityEngine.Events.BaseInvokableCall>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Comparison_1_t5015_Comparison_1__ctor_m30437_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1__ctor_m30437_GenericMethod;
// System.Void System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Object,System.IntPtr)
MethodInfo Comparison_1__ctor_m30437_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Comparison_1__ctor_m14743_gshared/* method */
	, &Comparison_1_t5015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Comparison_1_t5015_Comparison_1__ctor_m30437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1__ctor_m30437_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
static ParameterInfo Comparison_1_t5015_Comparison_1_Invoke_m30438_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_Invoke_m30438_GenericMethod;
// System.Int32 System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::Invoke(T,T)
MethodInfo Comparison_1_Invoke_m30438_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Comparison_1_Invoke_m14744_gshared/* method */
	, &Comparison_1_t5015_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, Comparison_1_t5015_Comparison_1_Invoke_m30438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_Invoke_m30438_GenericMethod/* genericMethod */

};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Comparison_1_t5015_Comparison_1_BeginInvoke_m30439_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseInvokableCall_t1127_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_BeginInvoke_m30439_GenericMethod;
// System.IAsyncResult System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
MethodInfo Comparison_1_BeginInvoke_m30439_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Comparison_1_BeginInvoke_m14745_gshared/* method */
	, &Comparison_1_t5015_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Comparison_1_t5015_Comparison_1_BeginInvoke_m30439_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_BeginInvoke_m30439_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Comparison_1_t5015_Comparison_1_EndInvoke_m30440_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_EndInvoke_m30440_GenericMethod;
// System.Int32 System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::EndInvoke(System.IAsyncResult)
MethodInfo Comparison_1_EndInvoke_m30440_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Comparison_1_EndInvoke_m14746_gshared/* method */
	, &Comparison_1_t5015_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Comparison_1_t5015_Comparison_1_EndInvoke_m30440_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_EndInvoke_m30440_GenericMethod/* genericMethod */

};
static MethodInfo* Comparison_1_t5015_MethodInfos[] =
{
	&Comparison_1__ctor_m30437_MethodInfo,
	&Comparison_1_Invoke_m30438_MethodInfo,
	&Comparison_1_BeginInvoke_m30439_MethodInfo,
	&Comparison_1_EndInvoke_m30440_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo Comparison_1_Invoke_m30438_MethodInfo;
extern MethodInfo Comparison_1_BeginInvoke_m30439_MethodInfo;
extern MethodInfo Comparison_1_EndInvoke_m30440_MethodInfo;
static MethodInfo* Comparison_1_t5015_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Comparison_1_Invoke_m30438_MethodInfo,
	&Comparison_1_BeginInvoke_m30439_MethodInfo,
	&Comparison_1_EndInvoke_m30440_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair Comparison_1_t5015_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Comparison_1_t5015_0_0_0;
extern Il2CppType Comparison_1_t5015_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct Comparison_1_t5015;
extern Il2CppGenericClass Comparison_1_t5015_GenericClass;
TypeInfo Comparison_1_t5015_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparison`1"/* name */
	, "System"/* namespaze */
	, Comparison_1_t5015_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Comparison_1_t5015_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Comparison_1_t5015_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Comparison_1_t5015_il2cpp_TypeInfo/* cast_class */
	, &Comparison_1_t5015_0_0_0/* byval_arg */
	, &Comparison_1_t5015_1_0_0/* this_arg */
	, Comparison_1_t5015_InterfacesOffsets/* interface_offsets */
	, &Comparison_1_t5015_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Comparison_1_t5015)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5064_il2cpp_TypeInfo;

// System.Boolean
#include "mscorlib_System_Boolean.h"


// T System.Collections.Generic.IEnumerator`1<System.Boolean>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Boolean>
extern MethodInfo IEnumerator_1_get_Current_m50008_MethodInfo;
static PropertyInfo IEnumerator_1_t5064____Current_PropertyInfo = 
{
	&IEnumerator_1_t5064_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50008_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5064_PropertyInfos[] =
{
	&IEnumerator_1_t5064____Current_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50008_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Boolean>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50008_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5064_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50008_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5064_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50008_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t5064_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5064_0_0_0;
extern Il2CppType IEnumerator_1_t5064_1_0_0;
struct IEnumerator_1_t5064;
extern Il2CppGenericClass IEnumerator_1_t5064_GenericClass;
TypeInfo IEnumerator_1_t5064_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5064_MethodInfos/* methods */
	, IEnumerator_1_t5064_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5064_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5064_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5064_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5064_0_0_0/* byval_arg */
	, &IEnumerator_1_t5064_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5064_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Boolean>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_512.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5024_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Boolean>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_512MethodDeclarations.h"

// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo Boolean_t122_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m30445_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBoolean_t122_m39312_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
 bool Array_InternalArray__get_Item_TisBoolean_t122_m39312 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30441_MethodInfo;
 void InternalEnumerator_1__ctor_m30441 (InternalEnumerator_1_t5024 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30442_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30442 (InternalEnumerator_1_t5024 * __this, MethodInfo* method){
	{
		bool L_0 = InternalEnumerator_1_get_Current_m30445(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30445_MethodInfo);
		bool L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Boolean_t122_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30443_MethodInfo;
 void InternalEnumerator_1_Dispose_m30443 (InternalEnumerator_1_t5024 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30444_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30444 (InternalEnumerator_1_t5024 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
 bool InternalEnumerator_1_get_Current_m30445 (InternalEnumerator_1_t5024 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		bool L_8 = Array_InternalArray__get_Item_TisBoolean_t122_m39312(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisBoolean_t122_m39312_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Boolean>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5024____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5024, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5024____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5024, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5024_FieldInfos[] =
{
	&InternalEnumerator_1_t5024____array_0_FieldInfo,
	&InternalEnumerator_1_t5024____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5024____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5024_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30442_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5024____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5024_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30445_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5024_PropertyInfos[] =
{
	&InternalEnumerator_1_t5024____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5024____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5024_InternalEnumerator_1__ctor_m30441_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30441_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30441_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30441/* method */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5024_InternalEnumerator_1__ctor_m30441_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30441_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30442_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30442_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30442/* method */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30442_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30443_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30443_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30443/* method */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30443_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30444_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30444_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30444/* method */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30444_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30445_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30445_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30445/* method */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30445_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5024_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30441_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30442_MethodInfo,
	&InternalEnumerator_1_Dispose_m30443_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30444_MethodInfo,
	&InternalEnumerator_1_get_Current_m30445_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5024_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30442_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30444_MethodInfo,
	&InternalEnumerator_1_Dispose_m30443_MethodInfo,
	&InternalEnumerator_1_get_Current_m30445_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5024_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5064_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5024_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5064_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5024_0_0_0;
extern Il2CppType InternalEnumerator_1_t5024_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t5024_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t5024_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5024_MethodInfos/* methods */
	, InternalEnumerator_1_t5024_PropertyInfos/* properties */
	, InternalEnumerator_1_t5024_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5024_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5024_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5024_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5024_1_0_0/* this_arg */
	, InternalEnumerator_1_t5024_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5024_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5024)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8898_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Boolean>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Boolean>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Boolean>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Boolean>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Boolean>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Boolean>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Boolean>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Boolean>
extern MethodInfo ICollection_1_get_Count_m50009_MethodInfo;
static PropertyInfo ICollection_1_t8898____Count_PropertyInfo = 
{
	&ICollection_1_t8898_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50009_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50010_MethodInfo;
static PropertyInfo ICollection_1_t8898____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8898_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50010_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8898_PropertyInfos[] =
{
	&ICollection_1_t8898____Count_PropertyInfo,
	&ICollection_1_t8898____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50009_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Boolean>::get_Count()
MethodInfo ICollection_1_get_Count_m50009_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8898_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50009_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50010_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Boolean>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50010_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8898_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50010_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo ICollection_1_t8898_ICollection_1_Add_m50011_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50011_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Boolean>::Add(T)
MethodInfo ICollection_1_Add_m50011_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8898_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, ICollection_1_t8898_ICollection_1_Add_m50011_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50011_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50012_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Boolean>::Clear()
MethodInfo ICollection_1_Clear_m50012_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8898_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50012_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo ICollection_1_t8898_ICollection_1_Contains_m50013_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_SByte_t125 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50013_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Boolean>::Contains(T)
MethodInfo ICollection_1_Contains_m50013_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8898_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_SByte_t125/* invoker_method */
	, ICollection_1_t8898_ICollection_1_Contains_m50013_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50013_GenericMethod/* genericMethod */

};
extern Il2CppType BooleanU5BU5D_t1398_0_0_0;
extern Il2CppType BooleanU5BU5D_t1398_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8898_ICollection_1_CopyTo_m50014_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BooleanU5BU5D_t1398_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50014_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Boolean>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50014_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8898_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8898_ICollection_1_CopyTo_m50014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50014_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo ICollection_1_t8898_ICollection_1_Remove_m50015_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_SByte_t125 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50015_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Boolean>::Remove(T)
MethodInfo ICollection_1_Remove_m50015_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8898_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_SByte_t125/* invoker_method */
	, ICollection_1_t8898_ICollection_1_Remove_m50015_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50015_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8898_MethodInfos[] =
{
	&ICollection_1_get_Count_m50009_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50010_MethodInfo,
	&ICollection_1_Add_m50011_MethodInfo,
	&ICollection_1_Clear_m50012_MethodInfo,
	&ICollection_1_Contains_m50013_MethodInfo,
	&ICollection_1_CopyTo_m50014_MethodInfo,
	&ICollection_1_Remove_m50015_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8900_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8898_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8900_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8898_0_0_0;
extern Il2CppType ICollection_1_t8898_1_0_0;
struct ICollection_1_t8898;
extern Il2CppGenericClass ICollection_1_t8898_GenericClass;
TypeInfo ICollection_1_t8898_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8898_MethodInfos/* methods */
	, ICollection_1_t8898_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8898_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8898_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8898_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8898_0_0_0/* byval_arg */
	, &ICollection_1_t8898_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8898_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Boolean>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Boolean>
extern Il2CppType IEnumerator_1_t5064_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50016_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Boolean>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50016_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8900_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5064_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50016_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8900_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50016_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8900_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8900_0_0_0;
extern Il2CppType IEnumerable_1_t8900_1_0_0;
struct IEnumerable_1_t8900;
extern Il2CppGenericClass IEnumerable_1_t8900_GenericClass;
TypeInfo IEnumerable_1_t8900_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8900_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8900_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8900_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8900_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8900_0_0_0/* byval_arg */
	, &IEnumerable_1_t8900_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8900_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8899_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Boolean>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Boolean>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Boolean>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Boolean>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Boolean>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Boolean>
extern MethodInfo IList_1_get_Item_m50017_MethodInfo;
extern MethodInfo IList_1_set_Item_m50018_MethodInfo;
static PropertyInfo IList_1_t8899____Item_PropertyInfo = 
{
	&IList_1_t8899_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50017_MethodInfo/* get */
	, &IList_1_set_Item_m50018_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8899_PropertyInfos[] =
{
	&IList_1_t8899____Item_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo IList_1_t8899_IList_1_IndexOf_m50019_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_SByte_t125 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50019_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Boolean>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50019_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_SByte_t125/* invoker_method */
	, IList_1_t8899_IList_1_IndexOf_m50019_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50019_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo IList_1_t8899_IList_1_Insert_m50020_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_SByte_t125 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50020_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Boolean>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50020_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_SByte_t125/* invoker_method */
	, IList_1_t8899_IList_1_Insert_m50020_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50020_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8899_IList_1_RemoveAt_m50021_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50021_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Boolean>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50021_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8899_IList_1_RemoveAt_m50021_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50021_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8899_IList_1_get_Item_m50017_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50017_GenericMethod;
// T System.Collections.Generic.IList`1<System.Boolean>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50017_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, IList_1_t8899_IList_1_get_Item_m50017_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50017_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo IList_1_t8899_IList_1_set_Item_m50018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_SByte_t125 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50018_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Boolean>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50018_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_SByte_t125/* invoker_method */
	, IList_1_t8899_IList_1_set_Item_m50018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50018_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8899_MethodInfos[] =
{
	&IList_1_IndexOf_m50019_MethodInfo,
	&IList_1_Insert_m50020_MethodInfo,
	&IList_1_RemoveAt_m50021_MethodInfo,
	&IList_1_get_Item_m50017_MethodInfo,
	&IList_1_set_Item_m50018_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8899_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8898_il2cpp_TypeInfo,
	&IEnumerable_1_t8900_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8899_0_0_0;
extern Il2CppType IList_1_t8899_1_0_0;
struct IList_1_t8899;
extern Il2CppGenericClass IList_1_t8899_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8899_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8899_MethodInfos/* methods */
	, IList_1_t8899_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8899_il2cpp_TypeInfo/* element_class */
	, IList_1_t8899_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8899_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8899_0_0_0/* byval_arg */
	, &IList_1_t8899_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8899_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8901_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>
extern MethodInfo ICollection_1_get_Count_m50022_MethodInfo;
static PropertyInfo ICollection_1_t8901____Count_PropertyInfo = 
{
	&ICollection_1_t8901_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50022_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50023_MethodInfo;
static PropertyInfo ICollection_1_t8901____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8901_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50023_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8901_PropertyInfos[] =
{
	&ICollection_1_t8901____Count_PropertyInfo,
	&ICollection_1_t8901____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50022_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::get_Count()
MethodInfo ICollection_1_get_Count_m50022_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8901_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50022_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50023_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50023_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8901_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50023_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2390_0_0_0;
extern Il2CppType IComparable_1_t2390_0_0_0;
static ParameterInfo ICollection_1_t8901_ICollection_1_Add_m50024_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2390_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50024_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Add(T)
MethodInfo ICollection_1_Add_m50024_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8901_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8901_ICollection_1_Add_m50024_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50024_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50025_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Clear()
MethodInfo ICollection_1_Clear_m50025_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8901_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50025_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2390_0_0_0;
static ParameterInfo ICollection_1_t8901_ICollection_1_Contains_m50026_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2390_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50026_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Contains(T)
MethodInfo ICollection_1_Contains_m50026_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8901_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8901_ICollection_1_Contains_m50026_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50026_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1U5BU5D_t5446_0_0_0;
extern Il2CppType IComparable_1U5BU5D_t5446_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8901_ICollection_1_CopyTo_m50027_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1U5BU5D_t5446_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50027_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50027_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8901_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8901_ICollection_1_CopyTo_m50027_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50027_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2390_0_0_0;
static ParameterInfo ICollection_1_t8901_ICollection_1_Remove_m50028_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2390_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50028_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Remove(T)
MethodInfo ICollection_1_Remove_m50028_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8901_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8901_ICollection_1_Remove_m50028_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50028_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8901_MethodInfos[] =
{
	&ICollection_1_get_Count_m50022_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50023_MethodInfo,
	&ICollection_1_Add_m50024_MethodInfo,
	&ICollection_1_Clear_m50025_MethodInfo,
	&ICollection_1_Contains_m50026_MethodInfo,
	&ICollection_1_CopyTo_m50027_MethodInfo,
	&ICollection_1_Remove_m50028_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8903_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8901_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8903_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8901_0_0_0;
extern Il2CppType ICollection_1_t8901_1_0_0;
struct ICollection_1_t8901;
extern Il2CppGenericClass ICollection_1_t8901_GenericClass;
TypeInfo ICollection_1_t8901_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8901_MethodInfos/* methods */
	, ICollection_1_t8901_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8901_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8901_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8901_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8901_0_0_0/* byval_arg */
	, &ICollection_1_t8901_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8901_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Boolean>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Boolean>>
extern Il2CppType IEnumerator_1_t7005_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50029_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Boolean>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50029_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8903_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7005_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50029_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8903_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50029_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8903_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8903_0_0_0;
extern Il2CppType IEnumerable_1_t8903_1_0_0;
struct IEnumerable_1_t8903;
extern Il2CppGenericClass IEnumerable_1_t8903_GenericClass;
TypeInfo IEnumerable_1_t8903_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8903_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8903_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8903_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8903_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8903_0_0_0/* byval_arg */
	, &IEnumerable_1_t8903_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8903_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7005_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Boolean>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Boolean>>
extern MethodInfo IEnumerator_1_get_Current_m50030_MethodInfo;
static PropertyInfo IEnumerator_1_t7005____Current_PropertyInfo = 
{
	&IEnumerator_1_t7005_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50030_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7005_PropertyInfos[] =
{
	&IEnumerator_1_t7005____Current_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2390_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50030_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Boolean>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50030_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7005_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2390_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50030_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7005_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50030_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7005_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7005_0_0_0;
extern Il2CppType IEnumerator_1_t7005_1_0_0;
struct IEnumerator_1_t7005;
extern Il2CppGenericClass IEnumerator_1_t7005_GenericClass;
TypeInfo IEnumerator_1_t7005_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7005_MethodInfos/* methods */
	, IEnumerator_1_t7005_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7005_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7005_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7005_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7005_0_0_0/* byval_arg */
	, &IEnumerator_1_t7005_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7005_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t2390_il2cpp_TypeInfo;



// System.Int32 System.IComparable`1<System.Boolean>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.Boolean>
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo IComparable_1_t2390_IComparable_1_CompareTo_m50031_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_SByte_t125 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m50031_GenericMethod;
// System.Int32 System.IComparable`1<System.Boolean>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m50031_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t2390_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_SByte_t125/* invoker_method */
	, IComparable_1_t2390_IComparable_1_CompareTo_m50031_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m50031_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t2390_MethodInfos[] =
{
	&IComparable_1_CompareTo_m50031_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t2390_1_0_0;
struct IComparable_1_t2390;
extern Il2CppGenericClass IComparable_1_t2390_GenericClass;
TypeInfo IComparable_1_t2390_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t2390_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2390_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t2390_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t2390_0_0_0/* byval_arg */
	, &IComparable_1_t2390_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t2390_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_513.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5025_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_513MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m30450_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_1_t2390_m39323_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Boolean>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Boolean>>(System.Int32)
#define Array_InternalArray__get_Item_TisIComparable_1_t2390_m39323(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5025____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5025, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5025____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5025, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5025_FieldInfos[] =
{
	&InternalEnumerator_1_t5025____array_0_FieldInfo,
	&InternalEnumerator_1_t5025____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30447_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5025____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5025_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30447_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5025____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5025_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30450_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5025_PropertyInfos[] =
{
	&InternalEnumerator_1_t5025____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5025____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5025_InternalEnumerator_1__ctor_m30446_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30446_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30446_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5025_InternalEnumerator_1__ctor_m30446_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30446_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30447_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30447_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30447_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30448_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30448_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30448_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30449_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30449_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30449_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2390_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30450_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30450_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2390_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30450_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5025_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30446_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30447_MethodInfo,
	&InternalEnumerator_1_Dispose_m30448_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30449_MethodInfo,
	&InternalEnumerator_1_get_Current_m30450_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30449_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30448_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5025_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30447_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30449_MethodInfo,
	&InternalEnumerator_1_Dispose_m30448_MethodInfo,
	&InternalEnumerator_1_get_Current_m30450_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5025_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7005_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5025_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7005_il2cpp_TypeInfo, 7},
};
extern TypeInfo IComparable_1_t2390_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5025_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30450_MethodInfo/* Method Usage */,
	&IComparable_1_t2390_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIComparable_1_t2390_m39323_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5025_0_0_0;
extern Il2CppType InternalEnumerator_1_t5025_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5025_GenericClass;
TypeInfo InternalEnumerator_1_t5025_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5025_MethodInfos/* methods */
	, InternalEnumerator_1_t5025_PropertyInfos/* properties */
	, InternalEnumerator_1_t5025_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5025_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5025_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5025_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5025_1_0_0/* this_arg */
	, InternalEnumerator_1_t5025_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5025_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5025_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5025)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8902_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>
extern MethodInfo IList_1_get_Item_m50032_MethodInfo;
extern MethodInfo IList_1_set_Item_m50033_MethodInfo;
static PropertyInfo IList_1_t8902____Item_PropertyInfo = 
{
	&IList_1_t8902_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50032_MethodInfo/* get */
	, &IList_1_set_Item_m50033_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8902_PropertyInfos[] =
{
	&IList_1_t8902____Item_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2390_0_0_0;
static ParameterInfo IList_1_t8902_IList_1_IndexOf_m50034_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2390_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50034_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50034_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8902_IList_1_IndexOf_m50034_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50034_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IComparable_1_t2390_0_0_0;
static ParameterInfo IList_1_t8902_IList_1_Insert_m50035_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2390_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50035_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50035_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8902_IList_1_Insert_m50035_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50035_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8902_IList_1_RemoveAt_m50036_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50036_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50036_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8902_IList_1_RemoveAt_m50036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50036_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8902_IList_1_get_Item_m50032_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IComparable_1_t2390_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50032_GenericMethod;
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50032_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2390_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8902_IList_1_get_Item_m50032_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50032_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IComparable_1_t2390_0_0_0;
static ParameterInfo IList_1_t8902_IList_1_set_Item_m50033_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2390_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50033_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50033_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8902_IList_1_set_Item_m50033_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50033_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8902_MethodInfos[] =
{
	&IList_1_IndexOf_m50034_MethodInfo,
	&IList_1_Insert_m50035_MethodInfo,
	&IList_1_RemoveAt_m50036_MethodInfo,
	&IList_1_get_Item_m50032_MethodInfo,
	&IList_1_set_Item_m50033_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8902_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8901_il2cpp_TypeInfo,
	&IEnumerable_1_t8903_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8902_0_0_0;
extern Il2CppType IList_1_t8902_1_0_0;
struct IList_1_t8902;
extern Il2CppGenericClass IList_1_t8902_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8902_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8902_MethodInfos/* methods */
	, IList_1_t8902_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8902_il2cpp_TypeInfo/* element_class */
	, IList_1_t8902_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8902_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8902_0_0_0/* byval_arg */
	, &IList_1_t8902_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8902_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8904_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>
extern MethodInfo ICollection_1_get_Count_m50037_MethodInfo;
static PropertyInfo ICollection_1_t8904____Count_PropertyInfo = 
{
	&ICollection_1_t8904_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50037_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50038_MethodInfo;
static PropertyInfo ICollection_1_t8904____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8904_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50038_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8904_PropertyInfos[] =
{
	&ICollection_1_t8904____Count_PropertyInfo,
	&ICollection_1_t8904____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50037_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::get_Count()
MethodInfo ICollection_1_get_Count_m50037_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8904_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50037_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50038_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50038_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8904_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50038_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2391_0_0_0;
extern Il2CppType IEquatable_1_t2391_0_0_0;
static ParameterInfo ICollection_1_t8904_ICollection_1_Add_m50039_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2391_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50039_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Add(T)
MethodInfo ICollection_1_Add_m50039_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8904_ICollection_1_Add_m50039_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50039_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50040_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Clear()
MethodInfo ICollection_1_Clear_m50040_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50040_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2391_0_0_0;
static ParameterInfo ICollection_1_t8904_ICollection_1_Contains_m50041_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2391_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50041_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Contains(T)
MethodInfo ICollection_1_Contains_m50041_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8904_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8904_ICollection_1_Contains_m50041_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50041_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1U5BU5D_t5447_0_0_0;
extern Il2CppType IEquatable_1U5BU5D_t5447_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8904_ICollection_1_CopyTo_m50042_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1U5BU5D_t5447_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50042_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50042_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8904_ICollection_1_CopyTo_m50042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50042_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2391_0_0_0;
static ParameterInfo ICollection_1_t8904_ICollection_1_Remove_m50043_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2391_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50043_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Remove(T)
MethodInfo ICollection_1_Remove_m50043_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8904_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8904_ICollection_1_Remove_m50043_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50043_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8904_MethodInfos[] =
{
	&ICollection_1_get_Count_m50037_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50038_MethodInfo,
	&ICollection_1_Add_m50039_MethodInfo,
	&ICollection_1_Clear_m50040_MethodInfo,
	&ICollection_1_Contains_m50041_MethodInfo,
	&ICollection_1_CopyTo_m50042_MethodInfo,
	&ICollection_1_Remove_m50043_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8906_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8904_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8906_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8904_0_0_0;
extern Il2CppType ICollection_1_t8904_1_0_0;
struct ICollection_1_t8904;
extern Il2CppGenericClass ICollection_1_t8904_GenericClass;
TypeInfo ICollection_1_t8904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8904_MethodInfos/* methods */
	, ICollection_1_t8904_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8904_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8904_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8904_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8904_0_0_0/* byval_arg */
	, &ICollection_1_t8904_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8904_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Boolean>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Boolean>>
extern Il2CppType IEnumerator_1_t7007_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50044_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Boolean>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50044_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8906_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7007_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50044_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8906_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50044_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8906_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8906_0_0_0;
extern Il2CppType IEnumerable_1_t8906_1_0_0;
struct IEnumerable_1_t8906;
extern Il2CppGenericClass IEnumerable_1_t8906_GenericClass;
TypeInfo IEnumerable_1_t8906_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8906_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8906_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8906_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8906_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8906_0_0_0/* byval_arg */
	, &IEnumerable_1_t8906_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8906_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7007_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Boolean>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Boolean>>
extern MethodInfo IEnumerator_1_get_Current_m50045_MethodInfo;
static PropertyInfo IEnumerator_1_t7007____Current_PropertyInfo = 
{
	&IEnumerator_1_t7007_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50045_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7007_PropertyInfos[] =
{
	&IEnumerator_1_t7007____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2391_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50045_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Boolean>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50045_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7007_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2391_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50045_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7007_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50045_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7007_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7007_0_0_0;
extern Il2CppType IEnumerator_1_t7007_1_0_0;
struct IEnumerator_1_t7007;
extern Il2CppGenericClass IEnumerator_1_t7007_GenericClass;
TypeInfo IEnumerator_1_t7007_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7007_MethodInfos/* methods */
	, IEnumerator_1_t7007_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7007_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7007_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7007_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7007_0_0_0/* byval_arg */
	, &IEnumerator_1_t7007_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7007_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t2391_il2cpp_TypeInfo;



// System.Boolean System.IEquatable`1<System.Boolean>::Equals(T)
// Metadata Definition System.IEquatable`1<System.Boolean>
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo IEquatable_1_t2391_IEquatable_1_Equals_m50046_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_SByte_t125 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m50046_GenericMethod;
// System.Boolean System.IEquatable`1<System.Boolean>::Equals(T)
MethodInfo IEquatable_1_Equals_m50046_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t2391_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_SByte_t125/* invoker_method */
	, IEquatable_1_t2391_IEquatable_1_Equals_m50046_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m50046_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t2391_MethodInfos[] =
{
	&IEquatable_1_Equals_m50046_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t2391_1_0_0;
struct IEquatable_1_t2391;
extern Il2CppGenericClass IEquatable_1_t2391_GenericClass;
TypeInfo IEquatable_1_t2391_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t2391_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t2391_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t2391_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t2391_0_0_0/* byval_arg */
	, &IEquatable_1_t2391_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t2391_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_514.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5026_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_514MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m30455_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEquatable_1_t2391_m39334_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Boolean>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Boolean>>(System.Int32)
#define Array_InternalArray__get_Item_TisIEquatable_1_t2391_m39334(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5026____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5026, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5026____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5026, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5026_FieldInfos[] =
{
	&InternalEnumerator_1_t5026____array_0_FieldInfo,
	&InternalEnumerator_1_t5026____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30452_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5026____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5026_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30452_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5026____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5026_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30455_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5026_PropertyInfos[] =
{
	&InternalEnumerator_1_t5026____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5026____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5026_InternalEnumerator_1__ctor_m30451_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30451_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30451_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5026_InternalEnumerator_1__ctor_m30451_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30451_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30452_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30452_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30452_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30453_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30453_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30453_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30454_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30454_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30454_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2391_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30455_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30455_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2391_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30455_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5026_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30451_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30452_MethodInfo,
	&InternalEnumerator_1_Dispose_m30453_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30454_MethodInfo,
	&InternalEnumerator_1_get_Current_m30455_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30454_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30453_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5026_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30452_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30454_MethodInfo,
	&InternalEnumerator_1_Dispose_m30453_MethodInfo,
	&InternalEnumerator_1_get_Current_m30455_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5026_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7007_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5026_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7007_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEquatable_1_t2391_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5026_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30455_MethodInfo/* Method Usage */,
	&IEquatable_1_t2391_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEquatable_1_t2391_m39334_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5026_0_0_0;
extern Il2CppType InternalEnumerator_1_t5026_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5026_GenericClass;
TypeInfo InternalEnumerator_1_t5026_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5026_MethodInfos/* methods */
	, InternalEnumerator_1_t5026_PropertyInfos/* properties */
	, InternalEnumerator_1_t5026_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5026_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5026_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5026_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5026_1_0_0/* this_arg */
	, InternalEnumerator_1_t5026_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5026_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5026_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5026)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8905_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>
extern MethodInfo IList_1_get_Item_m50047_MethodInfo;
extern MethodInfo IList_1_set_Item_m50048_MethodInfo;
static PropertyInfo IList_1_t8905____Item_PropertyInfo = 
{
	&IList_1_t8905_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50047_MethodInfo/* get */
	, &IList_1_set_Item_m50048_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8905_PropertyInfos[] =
{
	&IList_1_t8905____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2391_0_0_0;
static ParameterInfo IList_1_t8905_IList_1_IndexOf_m50049_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2391_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50049_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50049_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8905_IList_1_IndexOf_m50049_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50049_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEquatable_1_t2391_0_0_0;
static ParameterInfo IList_1_t8905_IList_1_Insert_m50050_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2391_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50050_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50050_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8905_IList_1_Insert_m50050_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50050_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8905_IList_1_RemoveAt_m50051_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50051_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50051_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8905_IList_1_RemoveAt_m50051_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50051_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8905_IList_1_get_Item_m50047_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEquatable_1_t2391_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50047_GenericMethod;
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50047_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2391_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8905_IList_1_get_Item_m50047_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50047_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEquatable_1_t2391_0_0_0;
static ParameterInfo IList_1_t8905_IList_1_set_Item_m50048_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2391_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50048_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50048_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8905_IList_1_set_Item_m50048_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50048_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8905_MethodInfos[] =
{
	&IList_1_IndexOf_m50049_MethodInfo,
	&IList_1_Insert_m50050_MethodInfo,
	&IList_1_RemoveAt_m50051_MethodInfo,
	&IList_1_get_Item_m50047_MethodInfo,
	&IList_1_set_Item_m50048_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8905_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8904_il2cpp_TypeInfo,
	&IEnumerable_1_t8906_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8905_0_0_0;
extern Il2CppType IList_1_t8905_1_0_0;
struct IList_1_t8905;
extern Il2CppGenericClass IList_1_t8905_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8905_MethodInfos/* methods */
	, IList_1_t8905_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8905_il2cpp_TypeInfo/* element_class */
	, IList_1_t8905_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8905_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8905_0_0_0/* byval_arg */
	, &IList_1_t8905_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8905_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEvent_2_t5027_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_genMethodDeclarations.h"

// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo TypeU5BU5D_t922_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_2_t4988_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_genMethodDeclarations.h"
extern MethodInfo UnityEventBase__ctor_m6555_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo UnityEventBase_GetValidMethodInfo_m6563_MethodInfo;
extern MethodInfo InvokableCall_2__ctor_m30118_MethodInfo;


// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
extern MethodInfo UnityEvent_2__ctor_m30456_MethodInfo;
 void UnityEvent_2__ctor_m30456_gshared (UnityEvent_2_t5027 * __this, MethodInfo* method)
{
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), 2));
		UnityEventBase__ctor_m6555(__this, /*hidden argument*/&UnityEventBase__ctor_m6555_MethodInfo);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern MethodInfo UnityEvent_2_FindMethod_Impl_m30457_MethodInfo;
 MethodInfo_t141 * UnityEvent_2_FindMethod_Impl_m30457_gshared (UnityEvent_2_t5027 * __this, String_t* ___name, Object_t * ___targetObj, MethodInfo* method)
{
	{
		TypeU5BU5D_t922* L_0 = ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 2));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_0, 0)) = (Type_t *)L_1;
		TypeU5BU5D_t922* L_2 = L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 1)) = (Type_t *)L_3;
		MethodInfo_t141 * L_4 = UnityEventBase_GetValidMethodInfo_m6563(NULL /*static, unused*/, ___targetObj, ___name, L_2, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern MethodInfo UnityEvent_2_GetDelegate_m30458_MethodInfo;
 BaseInvokableCall_t1127 * UnityEvent_2_GetDelegate_m30458_gshared (UnityEvent_2_t5027 * __this, Object_t * ___target, MethodInfo_t141 * ___theFunction, MethodInfo* method)
{
	{
		InvokableCall_2_t4988 * L_0 = (InvokableCall_2_t4988 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (InvokableCall_2_t4988 * __this, Object_t * p0, MethodInfo_t141 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, ___target, ___theFunction, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo UnityEvent_2_t5027____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &UnityEvent_2_t5027_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEvent_2_t5027, ___m_InvokeArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_2_t5027_FieldInfos[] =
{
	&UnityEvent_2_t5027____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_2__ctor_m30456_GenericMethod;
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
MethodInfo UnityEvent_2__ctor_m30456_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent_2__ctor_m30456_gshared/* method */
	, &UnityEvent_2_t5027_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_2__ctor_m30456_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_2_t5027_UnityEvent_2_FindMethod_Impl_m30457_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_2_FindMethod_Impl_m30457_GenericMethod;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_2_FindMethod_Impl_m30457_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_2_FindMethod_Impl_m30457_gshared/* method */
	, &UnityEvent_2_t5027_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_2_t5027_UnityEvent_2_FindMethod_Impl_m30457_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_2_FindMethod_Impl_m30457_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo UnityEvent_2_t5027_UnityEvent_2_GetDelegate_m30458_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_2_GetDelegate_m30458_GenericMethod;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_2_GetDelegate_m30458_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_2_GetDelegate_m30458_gshared/* method */
	, &UnityEvent_2_t5027_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_2_t5027_UnityEvent_2_GetDelegate_m30458_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_2_GetDelegate_m30458_GenericMethod/* genericMethod */

};
static MethodInfo* UnityEvent_2_t5027_MethodInfos[] =
{
	&UnityEvent_2__ctor_m30456_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m30457_MethodInfo,
	&UnityEvent_2_GetDelegate_m30458_MethodInfo,
	NULL
};
extern MethodInfo UnityEventBase_ToString_m2086_MethodInfo;
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087_MethodInfo;
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088_MethodInfo;
static MethodInfo* UnityEvent_2_t5027_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&UnityEventBase_ToString_m2086_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m30457_MethodInfo,
	&UnityEvent_2_GetDelegate_m30458_MethodInfo,
};
extern TypeInfo ISerializationCallbackReceiver_t470_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityEvent_2_t5027_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t470_il2cpp_TypeInfo, 4},
};
extern TypeInfo InvokableCall_2_t4988_il2cpp_TypeInfo;
static Il2CppRGCTXData UnityEvent_2_t5027_RGCTXData[4] = 
{
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&InvokableCall_2_t4988_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_2__ctor_m30118_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_2_t5027_0_0_0;
extern Il2CppType UnityEvent_2_t5027_1_0_0;
extern TypeInfo UnityEventBase_t1136_il2cpp_TypeInfo;
struct UnityEvent_2_t5027;
extern Il2CppGenericClass UnityEvent_2_t5027_GenericClass;
TypeInfo UnityEvent_2_t5027_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_2_t5027_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_2_t5027_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_2_t5027_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEvent_2_t5027_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEvent_2_t5027_il2cpp_TypeInfo/* cast_class */
	, &UnityEvent_2_t5027_0_0_0/* byval_arg */
	, &UnityEvent_2_t5027_1_0_0/* this_arg */
	, UnityEvent_2_t5027_InterfacesOffsets/* interface_offsets */
	, &UnityEvent_2_t5027_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, UnityEvent_2_t5027_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_2_t5027)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEvent_3_t5028_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_genMethodDeclarations.h"

// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen.h"
extern TypeInfo InvokableCall_3_t4990_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_genMethodDeclarations.h"
extern MethodInfo InvokableCall_3__ctor_m30125_MethodInfo;


// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern MethodInfo UnityEvent_3__ctor_m30459_MethodInfo;
 void UnityEvent_3__ctor_m30459_gshared (UnityEvent_3_t5028 * __this, MethodInfo* method)
{
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), 3));
		UnityEventBase__ctor_m6555(__this, /*hidden argument*/&UnityEventBase__ctor_m6555_MethodInfo);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern MethodInfo UnityEvent_3_FindMethod_Impl_m30460_MethodInfo;
 MethodInfo_t141 * UnityEvent_3_FindMethod_Impl_m30460_gshared (UnityEvent_3_t5028 * __this, String_t* ___name, Object_t * ___targetObj, MethodInfo* method)
{
	{
		TypeU5BU5D_t922* L_0 = ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 3));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_0, 0)) = (Type_t *)L_1;
		TypeU5BU5D_t922* L_2 = L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 1)) = (Type_t *)L_3;
		TypeU5BU5D_t922* L_4 = L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 2)) = (Type_t *)L_5;
		MethodInfo_t141 * L_6 = UnityEventBase_GetValidMethodInfo_m6563(NULL /*static, unused*/, ___targetObj, ___name, L_4, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo);
		return L_6;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern MethodInfo UnityEvent_3_GetDelegate_m30461_MethodInfo;
 BaseInvokableCall_t1127 * UnityEvent_3_GetDelegate_m30461_gshared (UnityEvent_3_t5028 * __this, Object_t * ___target, MethodInfo_t141 * ___theFunction, MethodInfo* method)
{
	{
		InvokableCall_3_t4990 * L_0 = (InvokableCall_3_t4990 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		(( void (*) (InvokableCall_3_t4990 * __this, Object_t * p0, MethodInfo_t141 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, ___target, ___theFunction, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo UnityEvent_3_t5028____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &UnityEvent_3_t5028_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEvent_3_t5028, ___m_InvokeArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_3_t5028_FieldInfos[] =
{
	&UnityEvent_3_t5028____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_3__ctor_m30459_GenericMethod;
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
MethodInfo UnityEvent_3__ctor_m30459_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent_3__ctor_m30459_gshared/* method */
	, &UnityEvent_3_t5028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_3__ctor_m30459_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_3_t5028_UnityEvent_3_FindMethod_Impl_m30460_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_3_FindMethod_Impl_m30460_GenericMethod;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_3_FindMethod_Impl_m30460_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_3_FindMethod_Impl_m30460_gshared/* method */
	, &UnityEvent_3_t5028_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_3_t5028_UnityEvent_3_FindMethod_Impl_m30460_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_3_FindMethod_Impl_m30460_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo UnityEvent_3_t5028_UnityEvent_3_GetDelegate_m30461_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_3_GetDelegate_m30461_GenericMethod;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_3_GetDelegate_m30461_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_3_GetDelegate_m30461_gshared/* method */
	, &UnityEvent_3_t5028_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_3_t5028_UnityEvent_3_GetDelegate_m30461_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_3_GetDelegate_m30461_GenericMethod/* genericMethod */

};
static MethodInfo* UnityEvent_3_t5028_MethodInfos[] =
{
	&UnityEvent_3__ctor_m30459_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m30460_MethodInfo,
	&UnityEvent_3_GetDelegate_m30461_MethodInfo,
	NULL
};
static MethodInfo* UnityEvent_3_t5028_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&UnityEventBase_ToString_m2086_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m30460_MethodInfo,
	&UnityEvent_3_GetDelegate_m30461_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityEvent_3_t5028_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t470_il2cpp_TypeInfo, 4},
};
extern TypeInfo InvokableCall_3_t4990_il2cpp_TypeInfo;
static Il2CppRGCTXData UnityEvent_3_t5028_RGCTXData[5] = 
{
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&InvokableCall_3_t4990_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_3__ctor_m30125_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_3_t5028_0_0_0;
extern Il2CppType UnityEvent_3_t5028_1_0_0;
struct UnityEvent_3_t5028;
extern Il2CppGenericClass UnityEvent_3_t5028_GenericClass;
TypeInfo UnityEvent_3_t5028_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_3_t5028_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_3_t5028_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_3_t5028_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEvent_3_t5028_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEvent_3_t5028_il2cpp_TypeInfo/* cast_class */
	, &UnityEvent_3_t5028_0_0_0/* byval_arg */
	, &UnityEvent_3_t5028_1_0_0/* this_arg */
	, UnityEvent_3_t5028_InterfacesOffsets/* interface_offsets */
	, &UnityEvent_3_t5028_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, UnityEvent_3_t5028_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_3_t5028)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEvent_4_t5029_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_genMethodDeclarations.h"

// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen.h"
extern TypeInfo InvokableCall_4_t4992_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_genMethodDeclarations.h"
extern MethodInfo InvokableCall_4__ctor_m30132_MethodInfo;


// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern MethodInfo UnityEvent_4__ctor_m30462_MethodInfo;
 void UnityEvent_4__ctor_m30462_gshared (UnityEvent_4_t5029 * __this, MethodInfo* method)
{
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t130*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo), 4));
		UnityEventBase__ctor_m6555(__this, /*hidden argument*/&UnityEventBase__ctor_m6555_MethodInfo);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern MethodInfo UnityEvent_4_FindMethod_Impl_m30463_MethodInfo;
 MethodInfo_t141 * UnityEvent_4_FindMethod_Impl_m30463_gshared (UnityEvent_4_t5029 * __this, String_t* ___name, Object_t * ___targetObj, MethodInfo* method)
{
	{
		TypeU5BU5D_t922* L_0 = ((TypeU5BU5D_t922*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t922_il2cpp_TypeInfo), 4));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_0, 0)) = (Type_t *)L_1;
		TypeU5BU5D_t922* L_2 = L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 1)) = (Type_t *)L_3;
		TypeU5BU5D_t922* L_4 = L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 2)) = (Type_t *)L_5;
		TypeU5BU5D_t922* L_6 = L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_6, 3)) = (Type_t *)L_7;
		MethodInfo_t141 * L_8 = UnityEventBase_GetValidMethodInfo_m6563(NULL /*static, unused*/, ___targetObj, ___name, L_6, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6563_MethodInfo);
		return L_8;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern MethodInfo UnityEvent_4_GetDelegate_m30464_MethodInfo;
 BaseInvokableCall_t1127 * UnityEvent_4_GetDelegate_m30464_gshared (UnityEvent_4_t5029 * __this, Object_t * ___target, MethodInfo_t141 * ___theFunction, MethodInfo* method)
{
	{
		InvokableCall_4_t4992 * L_0 = (InvokableCall_4_t4992 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (InvokableCall_4_t4992 * __this, Object_t * p0, MethodInfo_t141 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, ___target, ___theFunction, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo UnityEvent_4_t5029____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &UnityEvent_4_t5029_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEvent_4_t5029, ___m_InvokeArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_4_t5029_FieldInfos[] =
{
	&UnityEvent_4_t5029____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_4__ctor_m30462_GenericMethod;
// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
MethodInfo UnityEvent_4__ctor_m30462_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent_4__ctor_m30462_gshared/* method */
	, &UnityEvent_4_t5029_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_4__ctor_m30462_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_4_t5029_UnityEvent_4_FindMethod_Impl_m30463_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t141_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_4_FindMethod_Impl_m30463_GenericMethod;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_4_FindMethod_Impl_m30463_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_4_FindMethod_Impl_m30463_gshared/* method */
	, &UnityEvent_4_t5029_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_4_t5029_UnityEvent_4_FindMethod_Impl_m30463_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_4_FindMethod_Impl_m30463_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo UnityEvent_4_t5029_UnityEvent_4_GetDelegate_m30464_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1127_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_4_GetDelegate_m30464_GenericMethod;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_4_GetDelegate_m30464_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_4_GetDelegate_m30464_gshared/* method */
	, &UnityEvent_4_t5029_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1127_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_4_t5029_UnityEvent_4_GetDelegate_m30464_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_4_GetDelegate_m30464_GenericMethod/* genericMethod */

};
static MethodInfo* UnityEvent_4_t5029_MethodInfos[] =
{
	&UnityEvent_4__ctor_m30462_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m30463_MethodInfo,
	&UnityEvent_4_GetDelegate_m30464_MethodInfo,
	NULL
};
static MethodInfo* UnityEvent_4_t5029_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&UnityEventBase_ToString_m2086_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2087_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2088_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m30463_MethodInfo,
	&UnityEvent_4_GetDelegate_m30464_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityEvent_4_t5029_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t470_il2cpp_TypeInfo, 4},
};
extern TypeInfo InvokableCall_4_t4992_il2cpp_TypeInfo;
static Il2CppRGCTXData UnityEvent_4_t5029_RGCTXData[6] = 
{
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&InvokableCall_4_t4992_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_4__ctor_m30132_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_4_t5029_0_0_0;
extern Il2CppType UnityEvent_4_t5029_1_0_0;
struct UnityEvent_4_t5029;
extern Il2CppGenericClass UnityEvent_4_t5029_GenericClass;
TypeInfo UnityEvent_4_t5029_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_4_t5029_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_4_t5029_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEventBase_t1136_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_4_t5029_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEvent_4_t5029_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEvent_4_t5029_il2cpp_TypeInfo/* cast_class */
	, &UnityEvent_4_t5029_0_0_0/* byval_arg */
	, &UnityEvent_4_t5029_1_0_0/* this_arg */
	, UnityEvent_4_t5029_InterfacesOffsets/* interface_offsets */
	, &UnityEvent_4_t5029_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, UnityEvent_4_t5029_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_4_t5029)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7009_il2cpp_TypeInfo;

// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UserAuthorizationDialog>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UserAuthorizationDialog>
extern MethodInfo IEnumerator_1_get_Current_m50052_MethodInfo;
static PropertyInfo IEnumerator_1_t7009____Current_PropertyInfo = 
{
	&IEnumerator_1_t7009_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50052_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7009_PropertyInfos[] =
{
	&IEnumerator_1_t7009____Current_PropertyInfo,
	NULL
};
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50052_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UserAuthorizationDialog>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50052_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7009_il2cpp_TypeInfo/* declaring_type */
	, &UserAuthorizationDialog_t1145_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50052_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7009_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50052_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7009_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7009_0_0_0;
extern Il2CppType IEnumerator_1_t7009_1_0_0;
struct IEnumerator_1_t7009;
extern Il2CppGenericClass IEnumerator_1_t7009_GenericClass;
TypeInfo IEnumerator_1_t7009_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7009_MethodInfos/* methods */
	, IEnumerator_1_t7009_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7009_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7009_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7009_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7009_0_0_0/* byval_arg */
	, &IEnumerator_1_t7009_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7009_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_515.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5030_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_515MethodDeclarations.h"

extern TypeInfo UserAuthorizationDialog_t1145_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30469_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUserAuthorizationDialog_t1145_m39345_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UserAuthorizationDialog>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UserAuthorizationDialog>(System.Int32)
#define Array_InternalArray__get_Item_TisUserAuthorizationDialog_t1145_m39345(__this, p0, method) (UserAuthorizationDialog_t1145 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5030____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5030_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5030, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5030____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5030_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5030, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5030_FieldInfos[] =
{
	&InternalEnumerator_1_t5030____array_0_FieldInfo,
	&InternalEnumerator_1_t5030____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30466_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5030____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5030_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30466_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5030____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5030_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30469_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5030_PropertyInfos[] =
{
	&InternalEnumerator_1_t5030____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5030____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5030_InternalEnumerator_1__ctor_m30465_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30465_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30465_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5030_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5030_InternalEnumerator_1__ctor_m30465_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30465_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30466_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30466_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5030_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30466_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30467_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30467_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5030_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30467_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30468_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30468_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5030_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30468_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30469_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30469_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5030_il2cpp_TypeInfo/* declaring_type */
	, &UserAuthorizationDialog_t1145_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30469_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5030_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30465_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30466_MethodInfo,
	&InternalEnumerator_1_Dispose_m30467_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30468_MethodInfo,
	&InternalEnumerator_1_get_Current_m30469_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30468_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30467_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5030_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30466_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30468_MethodInfo,
	&InternalEnumerator_1_Dispose_m30467_MethodInfo,
	&InternalEnumerator_1_get_Current_m30469_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5030_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7009_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5030_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7009_il2cpp_TypeInfo, 7},
};
extern TypeInfo UserAuthorizationDialog_t1145_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5030_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30469_MethodInfo/* Method Usage */,
	&UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisUserAuthorizationDialog_t1145_m39345_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5030_0_0_0;
extern Il2CppType InternalEnumerator_1_t5030_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5030_GenericClass;
TypeInfo InternalEnumerator_1_t5030_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5030_MethodInfos/* methods */
	, InternalEnumerator_1_t5030_PropertyInfos/* properties */
	, InternalEnumerator_1_t5030_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5030_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5030_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5030_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5030_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5030_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5030_1_0_0/* this_arg */
	, InternalEnumerator_1_t5030_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5030_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5030_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5030)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8907_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>
extern MethodInfo ICollection_1_get_Count_m50053_MethodInfo;
static PropertyInfo ICollection_1_t8907____Count_PropertyInfo = 
{
	&ICollection_1_t8907_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50053_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50054_MethodInfo;
static PropertyInfo ICollection_1_t8907____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8907_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50054_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8907_PropertyInfos[] =
{
	&ICollection_1_t8907____Count_PropertyInfo,
	&ICollection_1_t8907____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50053_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::get_Count()
MethodInfo ICollection_1_get_Count_m50053_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8907_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50053_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50054_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50054_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8907_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50054_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
static ParameterInfo ICollection_1_t8907_ICollection_1_Add_m50055_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1145_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50055_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Add(T)
MethodInfo ICollection_1_Add_m50055_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8907_ICollection_1_Add_m50055_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50055_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50056_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Clear()
MethodInfo ICollection_1_Clear_m50056_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50056_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
static ParameterInfo ICollection_1_t8907_ICollection_1_Contains_m50057_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1145_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50057_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Contains(T)
MethodInfo ICollection_1_Contains_m50057_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8907_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8907_ICollection_1_Contains_m50057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50057_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialogU5BU5D_t5765_0_0_0;
extern Il2CppType UserAuthorizationDialogU5BU5D_t5765_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8907_ICollection_1_CopyTo_m50058_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialogU5BU5D_t5765_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50058_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50058_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8907_ICollection_1_CopyTo_m50058_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50058_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
static ParameterInfo ICollection_1_t8907_ICollection_1_Remove_m50059_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1145_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50059_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Remove(T)
MethodInfo ICollection_1_Remove_m50059_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8907_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8907_ICollection_1_Remove_m50059_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50059_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8907_MethodInfos[] =
{
	&ICollection_1_get_Count_m50053_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50054_MethodInfo,
	&ICollection_1_Add_m50055_MethodInfo,
	&ICollection_1_Clear_m50056_MethodInfo,
	&ICollection_1_Contains_m50057_MethodInfo,
	&ICollection_1_CopyTo_m50058_MethodInfo,
	&ICollection_1_Remove_m50059_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8909_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8907_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8909_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8907_0_0_0;
extern Il2CppType ICollection_1_t8907_1_0_0;
struct ICollection_1_t8907;
extern Il2CppGenericClass ICollection_1_t8907_GenericClass;
TypeInfo ICollection_1_t8907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8907_MethodInfos/* methods */
	, ICollection_1_t8907_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8907_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8907_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8907_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8907_0_0_0/* byval_arg */
	, &ICollection_1_t8907_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8907_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UserAuthorizationDialog>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType IEnumerator_1_t7009_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50060_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UserAuthorizationDialog>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50060_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8909_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7009_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50060_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8909_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50060_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8909_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8909_0_0_0;
extern Il2CppType IEnumerable_1_t8909_1_0_0;
struct IEnumerable_1_t8909;
extern Il2CppGenericClass IEnumerable_1_t8909_GenericClass;
TypeInfo IEnumerable_1_t8909_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8909_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8909_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8909_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8909_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8909_0_0_0/* byval_arg */
	, &IEnumerable_1_t8909_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8909_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8908_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>
extern MethodInfo IList_1_get_Item_m50061_MethodInfo;
extern MethodInfo IList_1_set_Item_m50062_MethodInfo;
static PropertyInfo IList_1_t8908____Item_PropertyInfo = 
{
	&IList_1_t8908_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50061_MethodInfo/* get */
	, &IList_1_set_Item_m50062_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8908_PropertyInfos[] =
{
	&IList_1_t8908____Item_PropertyInfo,
	NULL
};
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
static ParameterInfo IList_1_t8908_IList_1_IndexOf_m50063_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1145_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50063_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50063_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8908_IList_1_IndexOf_m50063_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50063_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
static ParameterInfo IList_1_t8908_IList_1_Insert_m50064_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1145_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50064_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50064_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8908_IList_1_Insert_m50064_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50064_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8908_IList_1_RemoveAt_m50065_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50065_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50065_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8908_IList_1_RemoveAt_m50065_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50065_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8908_IList_1_get_Item_m50061_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50061_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50061_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &UserAuthorizationDialog_t1145_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8908_IList_1_get_Item_m50061_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50061_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
static ParameterInfo IList_1_t8908_IList_1_set_Item_m50062_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1145_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50062_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50062_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8908_IList_1_set_Item_m50062_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50062_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8908_MethodInfos[] =
{
	&IList_1_IndexOf_m50063_MethodInfo,
	&IList_1_Insert_m50064_MethodInfo,
	&IList_1_RemoveAt_m50065_MethodInfo,
	&IList_1_get_Item_m50061_MethodInfo,
	&IList_1_set_Item_m50062_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8908_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8907_il2cpp_TypeInfo,
	&IEnumerable_1_t8909_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8908_0_0_0;
extern Il2CppType IList_1_t8908_1_0_0;
struct IList_1_t8908;
extern Il2CppGenericClass IList_1_t8908_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8908_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8908_MethodInfos/* methods */
	, IList_1_t8908_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8908_il2cpp_TypeInfo/* element_class */
	, IList_1_t8908_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8908_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8908_0_0_0/* byval_arg */
	, &IList_1_t8908_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8908_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_183.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t5031_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_183MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_186.h"
extern TypeInfo InvokableCall_1_t5032_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_186MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m30472_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m30474_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t5031____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t5031_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t5031, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t5031_FieldInfos[] =
{
	&CachedInvokableCall_1_t5031____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
static ParameterInfo CachedInvokableCall_1_t5031_CachedInvokableCall_1__ctor_m30470_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1145_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m30470_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m30470_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t5031_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t5031_CachedInvokableCall_1__ctor_m30470_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m30470_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t5031_CachedInvokableCall_1_Invoke_m30471_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m30471_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m30471_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t5031_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t5031_CachedInvokableCall_1_Invoke_m30471_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m30471_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t5031_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m30470_MethodInfo,
	&CachedInvokableCall_1_Invoke_m30471_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m30471_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m30475_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t5031_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m30471_MethodInfo,
	&InvokableCall_1_Find_m30475_MethodInfo,
};
extern Il2CppType UnityAction_1_t5033_0_0_0;
extern TypeInfo UnityAction_1_t5033_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisUserAuthorizationDialog_t1145_m39355_MethodInfo;
extern TypeInfo UserAuthorizationDialog_t1145_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m30477_MethodInfo;
extern TypeInfo UserAuthorizationDialog_t1145_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t5031_RGCTXData[8] = 
{
	&UnityAction_1_t5033_0_0_0/* Type Usage */,
	&UnityAction_1_t5033_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisUserAuthorizationDialog_t1145_m39355_MethodInfo/* Method Usage */,
	&UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m30477_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m30472_MethodInfo/* Method Usage */,
	&UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m30474_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t5031_0_0_0;
extern Il2CppType CachedInvokableCall_1_t5031_1_0_0;
struct CachedInvokableCall_1_t5031;
extern Il2CppGenericClass CachedInvokableCall_1_t5031_GenericClass;
TypeInfo CachedInvokableCall_1_t5031_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t5031_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t5031_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t5032_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t5031_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t5031_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t5031_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t5031_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t5031_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t5031_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t5031_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t5031)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_190.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
extern TypeInfo UnityAction_1_t5033_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_190MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UserAuthorizationDialog>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UserAuthorizationDialog>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisUserAuthorizationDialog_t1145_m39355(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType UnityAction_1_t5033_0_0_1;
FieldInfo InvokableCall_1_t5032____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t5033_0_0_1/* type */
	, &InvokableCall_1_t5032_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t5032, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t5032_FieldInfos[] =
{
	&InvokableCall_1_t5032____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t5032_InvokableCall_1__ctor_m30472_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m30472_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m30472_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t5032_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t5032_InvokableCall_1__ctor_m30472_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m30472_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t5033_0_0_0;
static ParameterInfo InvokableCall_1_t5032_InvokableCall_1__ctor_m30473_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t5033_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m30473_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m30473_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t5032_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t5032_InvokableCall_1__ctor_m30473_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m30473_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t5032_InvokableCall_1_Invoke_m30474_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m30474_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m30474_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t5032_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t5032_InvokableCall_1_Invoke_m30474_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m30474_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t5032_InvokableCall_1_Find_m30475_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m30475_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m30475_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t5032_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t5032_InvokableCall_1_Find_m30475_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m30475_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t5032_MethodInfos[] =
{
	&InvokableCall_1__ctor_m30472_MethodInfo,
	&InvokableCall_1__ctor_m30473_MethodInfo,
	&InvokableCall_1_Invoke_m30474_MethodInfo,
	&InvokableCall_1_Find_m30475_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t5032_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m30474_MethodInfo,
	&InvokableCall_1_Find_m30475_MethodInfo,
};
extern TypeInfo UnityAction_1_t5033_il2cpp_TypeInfo;
extern TypeInfo UserAuthorizationDialog_t1145_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t5032_RGCTXData[5] = 
{
	&UnityAction_1_t5033_0_0_0/* Type Usage */,
	&UnityAction_1_t5033_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisUserAuthorizationDialog_t1145_m39355_MethodInfo/* Method Usage */,
	&UserAuthorizationDialog_t1145_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m30477_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t5032_0_0_0;
extern Il2CppType InvokableCall_1_t5032_1_0_0;
struct InvokableCall_1_t5032;
extern Il2CppGenericClass InvokableCall_1_t5032_GenericClass;
TypeInfo InvokableCall_1_t5032_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t5032_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t5032_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t5032_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t5032_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t5032_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t5032_0_0_0/* byval_arg */
	, &InvokableCall_1_t5032_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t5032_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t5032_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t5032)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t5033_UnityAction_1__ctor_m30476_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m30476_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m30476_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t5033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t5033_UnityAction_1__ctor_m30476_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m30476_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
static ParameterInfo UnityAction_1_t5033_UnityAction_1_Invoke_m30477_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1145_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m30477_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m30477_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t5033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t5033_UnityAction_1_Invoke_m30477_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m30477_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1145_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t5033_UnityAction_1_BeginInvoke_m30478_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1145_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m30478_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m30478_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t5033_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t5033_UnityAction_1_BeginInvoke_m30478_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m30478_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t5033_UnityAction_1_EndInvoke_m30479_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m30479_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m30479_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t5033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t5033_UnityAction_1_EndInvoke_m30479_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m30479_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t5033_MethodInfos[] =
{
	&UnityAction_1__ctor_m30476_MethodInfo,
	&UnityAction_1_Invoke_m30477_MethodInfo,
	&UnityAction_1_BeginInvoke_m30478_MethodInfo,
	&UnityAction_1_EndInvoke_m30479_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m30478_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m30479_MethodInfo;
static MethodInfo* UnityAction_1_t5033_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m30477_MethodInfo,
	&UnityAction_1_BeginInvoke_m30478_MethodInfo,
	&UnityAction_1_EndInvoke_m30479_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t5033_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t5033_1_0_0;
struct UnityAction_1_t5033;
extern Il2CppGenericClass UnityAction_1_t5033_GenericClass;
TypeInfo UnityAction_1_t5033_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t5033_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t5033_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t5033_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t5033_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t5033_0_0_0/* byval_arg */
	, &UnityAction_1_t5033_1_0_0/* this_arg */
	, UnityAction_1_t5033_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t5033_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t5033)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7011_il2cpp_TypeInfo;

// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50066_MethodInfo;
static PropertyInfo IEnumerator_1_t7011____Current_PropertyInfo = 
{
	&IEnumerator_1_t7011_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50066_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7011_PropertyInfos[] =
{
	&IEnumerator_1_t7011____Current_PropertyInfo,
	NULL
};
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50066_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50066_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7011_il2cpp_TypeInfo/* declaring_type */
	, &DefaultValueAttribute_t1146_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50066_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7011_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50066_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7011_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7011_0_0_0;
extern Il2CppType IEnumerator_1_t7011_1_0_0;
struct IEnumerator_1_t7011;
extern Il2CppGenericClass IEnumerator_1_t7011_GenericClass;
TypeInfo IEnumerator_1_t7011_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7011_MethodInfos/* methods */
	, IEnumerator_1_t7011_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7011_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7011_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7011_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7011_0_0_0/* byval_arg */
	, &IEnumerator_1_t7011_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7011_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_516.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5034_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_516MethodDeclarations.h"

extern TypeInfo DefaultValueAttribute_t1146_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30484_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDefaultValueAttribute_t1146_m39357_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Internal.DefaultValueAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Internal.DefaultValueAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisDefaultValueAttribute_t1146_m39357(__this, p0, method) (DefaultValueAttribute_t1146 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5034____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5034_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5034, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5034____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5034_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5034, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5034_FieldInfos[] =
{
	&InternalEnumerator_1_t5034____array_0_FieldInfo,
	&InternalEnumerator_1_t5034____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30481_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5034____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5034_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30481_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5034____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5034_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30484_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5034_PropertyInfos[] =
{
	&InternalEnumerator_1_t5034____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5034____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5034_InternalEnumerator_1__ctor_m30480_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30480_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30480_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5034_InternalEnumerator_1__ctor_m30480_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30480_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30481_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30481_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5034_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30481_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30482_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30482_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30482_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30483_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30483_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5034_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30483_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30484_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30484_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5034_il2cpp_TypeInfo/* declaring_type */
	, &DefaultValueAttribute_t1146_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30484_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5034_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30480_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30481_MethodInfo,
	&InternalEnumerator_1_Dispose_m30482_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30483_MethodInfo,
	&InternalEnumerator_1_get_Current_m30484_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30483_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30482_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5034_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30481_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30483_MethodInfo,
	&InternalEnumerator_1_Dispose_m30482_MethodInfo,
	&InternalEnumerator_1_get_Current_m30484_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5034_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7011_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5034_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7011_il2cpp_TypeInfo, 7},
};
extern TypeInfo DefaultValueAttribute_t1146_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5034_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30484_MethodInfo/* Method Usage */,
	&DefaultValueAttribute_t1146_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDefaultValueAttribute_t1146_m39357_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5034_0_0_0;
extern Il2CppType InternalEnumerator_1_t5034_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5034_GenericClass;
TypeInfo InternalEnumerator_1_t5034_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5034_MethodInfos/* methods */
	, InternalEnumerator_1_t5034_PropertyInfos/* properties */
	, InternalEnumerator_1_t5034_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5034_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5034_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5034_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5034_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5034_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5034_1_0_0/* this_arg */
	, InternalEnumerator_1_t5034_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5034_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5034_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5034)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8910_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>
extern MethodInfo ICollection_1_get_Count_m50067_MethodInfo;
static PropertyInfo ICollection_1_t8910____Count_PropertyInfo = 
{
	&ICollection_1_t8910_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50067_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50068_MethodInfo;
static PropertyInfo ICollection_1_t8910____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8910_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50068_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8910_PropertyInfos[] =
{
	&ICollection_1_t8910____Count_PropertyInfo,
	&ICollection_1_t8910____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50067_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50067_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8910_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50067_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50068_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50068_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8910_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50068_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
static ParameterInfo ICollection_1_t8910_ICollection_1_Add_m50069_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1146_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50069_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50069_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8910_ICollection_1_Add_m50069_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50069_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50070_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50070_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50070_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
static ParameterInfo ICollection_1_t8910_ICollection_1_Contains_m50071_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1146_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50071_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50071_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8910_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8910_ICollection_1_Contains_m50071_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50071_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultValueAttributeU5BU5D_t5766_0_0_0;
extern Il2CppType DefaultValueAttributeU5BU5D_t5766_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8910_ICollection_1_CopyTo_m50072_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttributeU5BU5D_t5766_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50072_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50072_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8910_ICollection_1_CopyTo_m50072_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50072_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
static ParameterInfo ICollection_1_t8910_ICollection_1_Remove_m50073_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1146_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50073_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50073_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8910_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8910_ICollection_1_Remove_m50073_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50073_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8910_MethodInfos[] =
{
	&ICollection_1_get_Count_m50067_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50068_MethodInfo,
	&ICollection_1_Add_m50069_MethodInfo,
	&ICollection_1_Clear_m50070_MethodInfo,
	&ICollection_1_Contains_m50071_MethodInfo,
	&ICollection_1_CopyTo_m50072_MethodInfo,
	&ICollection_1_Remove_m50073_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8912_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8910_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8912_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8910_0_0_0;
extern Il2CppType ICollection_1_t8910_1_0_0;
struct ICollection_1_t8910;
extern Il2CppGenericClass ICollection_1_t8910_GenericClass;
TypeInfo ICollection_1_t8910_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8910_MethodInfos/* methods */
	, ICollection_1_t8910_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8910_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8910_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8910_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8910_0_0_0/* byval_arg */
	, &ICollection_1_t8910_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8910_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.DefaultValueAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.DefaultValueAttribute>
extern Il2CppType IEnumerator_1_t7011_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50074_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.DefaultValueAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50074_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8912_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7011_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50074_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8912_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50074_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8912_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8912_0_0_0;
extern Il2CppType IEnumerable_1_t8912_1_0_0;
struct IEnumerable_1_t8912;
extern Il2CppGenericClass IEnumerable_1_t8912_GenericClass;
TypeInfo IEnumerable_1_t8912_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8912_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8912_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8912_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8912_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8912_0_0_0/* byval_arg */
	, &IEnumerable_1_t8912_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8912_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8911_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>
extern MethodInfo IList_1_get_Item_m50075_MethodInfo;
extern MethodInfo IList_1_set_Item_m50076_MethodInfo;
static PropertyInfo IList_1_t8911____Item_PropertyInfo = 
{
	&IList_1_t8911_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50075_MethodInfo/* get */
	, &IList_1_set_Item_m50076_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8911_PropertyInfos[] =
{
	&IList_1_t8911____Item_PropertyInfo,
	NULL
};
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
static ParameterInfo IList_1_t8911_IList_1_IndexOf_m50077_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1146_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50077_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50077_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8911_IList_1_IndexOf_m50077_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50077_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
static ParameterInfo IList_1_t8911_IList_1_Insert_m50078_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1146_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50078_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50078_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8911_IList_1_Insert_m50078_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50078_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8911_IList_1_RemoveAt_m50079_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50079_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50079_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8911_IList_1_RemoveAt_m50079_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50079_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8911_IList_1_get_Item_m50075_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50075_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50075_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &DefaultValueAttribute_t1146_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8911_IList_1_get_Item_m50075_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50075_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DefaultValueAttribute_t1146_0_0_0;
static ParameterInfo IList_1_t8911_IList_1_set_Item_m50076_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1146_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50076_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50076_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8911_IList_1_set_Item_m50076_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50076_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8911_MethodInfos[] =
{
	&IList_1_IndexOf_m50077_MethodInfo,
	&IList_1_Insert_m50078_MethodInfo,
	&IList_1_RemoveAt_m50079_MethodInfo,
	&IList_1_get_Item_m50075_MethodInfo,
	&IList_1_set_Item_m50076_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8911_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8910_il2cpp_TypeInfo,
	&IEnumerable_1_t8912_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8911_0_0_0;
extern Il2CppType IList_1_t8911_1_0_0;
struct IList_1_t8911;
extern Il2CppGenericClass IList_1_t8911_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8911_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8911_MethodInfos/* methods */
	, IList_1_t8911_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8911_il2cpp_TypeInfo/* element_class */
	, IList_1_t8911_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8911_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8911_0_0_0/* byval_arg */
	, &IList_1_t8911_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8911_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7013_il2cpp_TypeInfo;

// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50080_MethodInfo;
static PropertyInfo IEnumerator_1_t7013____Current_PropertyInfo = 
{
	&IEnumerator_1_t7013_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50080_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7013_PropertyInfos[] =
{
	&IEnumerator_1_t7013____Current_PropertyInfo,
	NULL
};
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50080_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50080_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7013_il2cpp_TypeInfo/* declaring_type */
	, &ExcludeFromDocsAttribute_t1147_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50080_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7013_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50080_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7013_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7013_0_0_0;
extern Il2CppType IEnumerator_1_t7013_1_0_0;
struct IEnumerator_1_t7013;
extern Il2CppGenericClass IEnumerator_1_t7013_GenericClass;
TypeInfo IEnumerator_1_t7013_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7013_MethodInfos/* methods */
	, IEnumerator_1_t7013_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7013_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7013_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7013_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7013_0_0_0/* byval_arg */
	, &IEnumerator_1_t7013_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7013_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_517.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5035_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_517MethodDeclarations.h"

extern TypeInfo ExcludeFromDocsAttribute_t1147_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30489_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisExcludeFromDocsAttribute_t1147_m39368_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Internal.ExcludeFromDocsAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Internal.ExcludeFromDocsAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisExcludeFromDocsAttribute_t1147_m39368(__this, p0, method) (ExcludeFromDocsAttribute_t1147 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5035____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5035_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5035, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5035____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5035_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5035, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5035_FieldInfos[] =
{
	&InternalEnumerator_1_t5035____array_0_FieldInfo,
	&InternalEnumerator_1_t5035____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30486_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5035____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5035_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30486_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5035____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5035_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30489_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5035_PropertyInfos[] =
{
	&InternalEnumerator_1_t5035____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5035____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5035_InternalEnumerator_1__ctor_m30485_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30485_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30485_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5035_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5035_InternalEnumerator_1__ctor_m30485_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30485_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30486_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30486_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5035_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30486_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30487_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30487_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5035_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30487_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30488_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30488_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5035_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30488_GenericMethod/* genericMethod */

};
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30489_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30489_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5035_il2cpp_TypeInfo/* declaring_type */
	, &ExcludeFromDocsAttribute_t1147_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30489_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5035_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30485_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30486_MethodInfo,
	&InternalEnumerator_1_Dispose_m30487_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30488_MethodInfo,
	&InternalEnumerator_1_get_Current_m30489_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30488_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30487_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5035_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30486_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30488_MethodInfo,
	&InternalEnumerator_1_Dispose_m30487_MethodInfo,
	&InternalEnumerator_1_get_Current_m30489_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5035_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7013_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5035_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7013_il2cpp_TypeInfo, 7},
};
extern TypeInfo ExcludeFromDocsAttribute_t1147_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5035_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30489_MethodInfo/* Method Usage */,
	&ExcludeFromDocsAttribute_t1147_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisExcludeFromDocsAttribute_t1147_m39368_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5035_0_0_0;
extern Il2CppType InternalEnumerator_1_t5035_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5035_GenericClass;
TypeInfo InternalEnumerator_1_t5035_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5035_MethodInfos/* methods */
	, InternalEnumerator_1_t5035_PropertyInfos/* properties */
	, InternalEnumerator_1_t5035_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5035_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5035_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5035_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5035_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5035_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5035_1_0_0/* this_arg */
	, InternalEnumerator_1_t5035_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5035_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5035_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5035)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8913_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern MethodInfo ICollection_1_get_Count_m50081_MethodInfo;
static PropertyInfo ICollection_1_t8913____Count_PropertyInfo = 
{
	&ICollection_1_t8913_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50081_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50082_MethodInfo;
static PropertyInfo ICollection_1_t8913____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8913_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50082_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8913_PropertyInfos[] =
{
	&ICollection_1_t8913____Count_PropertyInfo,
	&ICollection_1_t8913____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50081_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50081_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8913_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50081_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50082_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50082_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8913_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50082_GenericMethod/* genericMethod */

};
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
static ParameterInfo ICollection_1_t8913_ICollection_1_Add_m50083_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1147_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50083_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50083_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8913_ICollection_1_Add_m50083_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50083_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50084_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50084_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50084_GenericMethod/* genericMethod */

};
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
static ParameterInfo ICollection_1_t8913_ICollection_1_Contains_m50085_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1147_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50085_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50085_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8913_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8913_ICollection_1_Contains_m50085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50085_GenericMethod/* genericMethod */

};
extern Il2CppType ExcludeFromDocsAttributeU5BU5D_t5767_0_0_0;
extern Il2CppType ExcludeFromDocsAttributeU5BU5D_t5767_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8913_ICollection_1_CopyTo_m50086_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttributeU5BU5D_t5767_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50086_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50086_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8913_ICollection_1_CopyTo_m50086_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50086_GenericMethod/* genericMethod */

};
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
static ParameterInfo ICollection_1_t8913_ICollection_1_Remove_m50087_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1147_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50087_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50087_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8913_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8913_ICollection_1_Remove_m50087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50087_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8913_MethodInfos[] =
{
	&ICollection_1_get_Count_m50081_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50082_MethodInfo,
	&ICollection_1_Add_m50083_MethodInfo,
	&ICollection_1_Clear_m50084_MethodInfo,
	&ICollection_1_Contains_m50085_MethodInfo,
	&ICollection_1_CopyTo_m50086_MethodInfo,
	&ICollection_1_Remove_m50087_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8915_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8913_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8915_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8913_0_0_0;
extern Il2CppType ICollection_1_t8913_1_0_0;
struct ICollection_1_t8913;
extern Il2CppGenericClass ICollection_1_t8913_GenericClass;
TypeInfo ICollection_1_t8913_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8913_MethodInfos/* methods */
	, ICollection_1_t8913_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8913_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8913_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8913_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8913_0_0_0/* byval_arg */
	, &ICollection_1_t8913_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8913_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern Il2CppType IEnumerator_1_t7013_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50088_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50088_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8915_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7013_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50088_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8915_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50088_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8915_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8915_0_0_0;
extern Il2CppType IEnumerable_1_t8915_1_0_0;
struct IEnumerable_1_t8915;
extern Il2CppGenericClass IEnumerable_1_t8915_GenericClass;
TypeInfo IEnumerable_1_t8915_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8915_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8915_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8915_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8915_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8915_0_0_0/* byval_arg */
	, &IEnumerable_1_t8915_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8915_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8914_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern MethodInfo IList_1_get_Item_m50089_MethodInfo;
extern MethodInfo IList_1_set_Item_m50090_MethodInfo;
static PropertyInfo IList_1_t8914____Item_PropertyInfo = 
{
	&IList_1_t8914_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50089_MethodInfo/* get */
	, &IList_1_set_Item_m50090_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8914_PropertyInfos[] =
{
	&IList_1_t8914____Item_PropertyInfo,
	NULL
};
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
static ParameterInfo IList_1_t8914_IList_1_IndexOf_m50091_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1147_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50091_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50091_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8914_IList_1_IndexOf_m50091_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50091_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
static ParameterInfo IList_1_t8914_IList_1_Insert_m50092_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1147_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50092_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50092_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8914_IList_1_Insert_m50092_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50092_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8914_IList_1_RemoveAt_m50093_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50093_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50093_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8914_IList_1_RemoveAt_m50093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50093_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8914_IList_1_get_Item_m50089_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50089_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50089_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &ExcludeFromDocsAttribute_t1147_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8914_IList_1_get_Item_m50089_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50089_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ExcludeFromDocsAttribute_t1147_0_0_0;
static ParameterInfo IList_1_t8914_IList_1_set_Item_m50090_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1147_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50090_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50090_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8914_IList_1_set_Item_m50090_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50090_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8914_MethodInfos[] =
{
	&IList_1_IndexOf_m50091_MethodInfo,
	&IList_1_Insert_m50092_MethodInfo,
	&IList_1_RemoveAt_m50093_MethodInfo,
	&IList_1_get_Item_m50089_MethodInfo,
	&IList_1_set_Item_m50090_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8914_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8913_il2cpp_TypeInfo,
	&IEnumerable_1_t8915_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8914_0_0_0;
extern Il2CppType IList_1_t8914_1_0_0;
struct IList_1_t8914;
extern Il2CppGenericClass IList_1_t8914_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8914_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8914_MethodInfos/* methods */
	, IList_1_t8914_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8914_il2cpp_TypeInfo/* element_class */
	, IList_1_t8914_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8914_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8914_0_0_0/* byval_arg */
	, &IList_1_t8914_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8914_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7015_il2cpp_TypeInfo;

// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50094_MethodInfo;
static PropertyInfo IEnumerator_1_t7015____Current_PropertyInfo = 
{
	&IEnumerator_1_t7015_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50094_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7015_PropertyInfos[] =
{
	&IEnumerator_1_t7015____Current_PropertyInfo,
	NULL
};
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50094_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50094_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7015_il2cpp_TypeInfo/* declaring_type */
	, &FormerlySerializedAsAttribute_t468_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50094_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7015_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50094_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7015_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7015_0_0_0;
extern Il2CppType IEnumerator_1_t7015_1_0_0;
struct IEnumerator_1_t7015;
extern Il2CppGenericClass IEnumerator_1_t7015_GenericClass;
TypeInfo IEnumerator_1_t7015_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7015_MethodInfos/* methods */
	, IEnumerator_1_t7015_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7015_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7015_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7015_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7015_0_0_0/* byval_arg */
	, &IEnumerator_1_t7015_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7015_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_518.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5036_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_518MethodDeclarations.h"

extern TypeInfo FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30494_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFormerlySerializedAsAttribute_t468_m39379_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Serialization.FormerlySerializedAsAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Serialization.FormerlySerializedAsAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisFormerlySerializedAsAttribute_t468_m39379(__this, p0, method) (FormerlySerializedAsAttribute_t468 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5036____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5036_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5036, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5036____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5036_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5036, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5036_FieldInfos[] =
{
	&InternalEnumerator_1_t5036____array_0_FieldInfo,
	&InternalEnumerator_1_t5036____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30491_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5036____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5036_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30491_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5036____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5036_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30494_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5036_PropertyInfos[] =
{
	&InternalEnumerator_1_t5036____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5036____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5036_InternalEnumerator_1__ctor_m30490_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30490_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30490_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5036_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5036_InternalEnumerator_1__ctor_m30490_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30490_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30491_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30491_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5036_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30491_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30492_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30492_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5036_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30492_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30493_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30493_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5036_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30493_GenericMethod/* genericMethod */

};
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30494_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30494_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5036_il2cpp_TypeInfo/* declaring_type */
	, &FormerlySerializedAsAttribute_t468_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30494_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5036_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30490_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30491_MethodInfo,
	&InternalEnumerator_1_Dispose_m30492_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30493_MethodInfo,
	&InternalEnumerator_1_get_Current_m30494_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30493_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30492_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5036_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30491_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30493_MethodInfo,
	&InternalEnumerator_1_Dispose_m30492_MethodInfo,
	&InternalEnumerator_1_get_Current_m30494_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5036_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7015_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5036_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7015_il2cpp_TypeInfo, 7},
};
extern TypeInfo FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5036_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30494_MethodInfo/* Method Usage */,
	&FormerlySerializedAsAttribute_t468_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisFormerlySerializedAsAttribute_t468_m39379_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5036_0_0_0;
extern Il2CppType InternalEnumerator_1_t5036_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5036_GenericClass;
TypeInfo InternalEnumerator_1_t5036_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5036_MethodInfos/* methods */
	, InternalEnumerator_1_t5036_PropertyInfos/* properties */
	, InternalEnumerator_1_t5036_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5036_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5036_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5036_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5036_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5036_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5036_1_0_0/* this_arg */
	, InternalEnumerator_1_t5036_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5036_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5036_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5036)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8916_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern MethodInfo ICollection_1_get_Count_m50095_MethodInfo;
static PropertyInfo ICollection_1_t8916____Count_PropertyInfo = 
{
	&ICollection_1_t8916_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50095_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50096_MethodInfo;
static PropertyInfo ICollection_1_t8916____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8916_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50096_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8916_PropertyInfos[] =
{
	&ICollection_1_t8916____Count_PropertyInfo,
	&ICollection_1_t8916____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50095_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50095_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8916_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50095_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50096_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50096_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8916_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50096_GenericMethod/* genericMethod */

};
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
static ParameterInfo ICollection_1_t8916_ICollection_1_Add_m50097_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t468_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50097_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50097_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8916_ICollection_1_Add_m50097_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50097_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50098_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50098_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50098_GenericMethod/* genericMethod */

};
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
static ParameterInfo ICollection_1_t8916_ICollection_1_Contains_m50099_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t468_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50099_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50099_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8916_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8916_ICollection_1_Contains_m50099_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50099_GenericMethod/* genericMethod */

};
extern Il2CppType FormerlySerializedAsAttributeU5BU5D_t5768_0_0_0;
extern Il2CppType FormerlySerializedAsAttributeU5BU5D_t5768_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8916_ICollection_1_CopyTo_m50100_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttributeU5BU5D_t5768_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50100_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50100_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8916_ICollection_1_CopyTo_m50100_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50100_GenericMethod/* genericMethod */

};
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
static ParameterInfo ICollection_1_t8916_ICollection_1_Remove_m50101_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t468_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50101_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50101_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8916_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8916_ICollection_1_Remove_m50101_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50101_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8916_MethodInfos[] =
{
	&ICollection_1_get_Count_m50095_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50096_MethodInfo,
	&ICollection_1_Add_m50097_MethodInfo,
	&ICollection_1_Clear_m50098_MethodInfo,
	&ICollection_1_Contains_m50099_MethodInfo,
	&ICollection_1_CopyTo_m50100_MethodInfo,
	&ICollection_1_Remove_m50101_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8918_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8916_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8918_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8916_0_0_0;
extern Il2CppType ICollection_1_t8916_1_0_0;
struct ICollection_1_t8916;
extern Il2CppGenericClass ICollection_1_t8916_GenericClass;
TypeInfo ICollection_1_t8916_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8916_MethodInfos/* methods */
	, ICollection_1_t8916_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8916_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8916_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8916_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8916_0_0_0/* byval_arg */
	, &ICollection_1_t8916_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8916_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern Il2CppType IEnumerator_1_t7015_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50102_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50102_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8918_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7015_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50102_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8918_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50102_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8918_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8918_0_0_0;
extern Il2CppType IEnumerable_1_t8918_1_0_0;
struct IEnumerable_1_t8918;
extern Il2CppGenericClass IEnumerable_1_t8918_GenericClass;
TypeInfo IEnumerable_1_t8918_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8918_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8918_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8918_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8918_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8918_0_0_0/* byval_arg */
	, &IEnumerable_1_t8918_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8918_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8917_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern MethodInfo IList_1_get_Item_m50103_MethodInfo;
extern MethodInfo IList_1_set_Item_m50104_MethodInfo;
static PropertyInfo IList_1_t8917____Item_PropertyInfo = 
{
	&IList_1_t8917_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50103_MethodInfo/* get */
	, &IList_1_set_Item_m50104_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8917_PropertyInfos[] =
{
	&IList_1_t8917____Item_PropertyInfo,
	NULL
};
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
static ParameterInfo IList_1_t8917_IList_1_IndexOf_m50105_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t468_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50105_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50105_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8917_IList_1_IndexOf_m50105_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50105_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
static ParameterInfo IList_1_t8917_IList_1_Insert_m50106_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t468_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50106_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50106_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8917_IList_1_Insert_m50106_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50106_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8917_IList_1_RemoveAt_m50107_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50107_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50107_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8917_IList_1_RemoveAt_m50107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50107_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8917_IList_1_get_Item_m50103_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50103_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50103_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &FormerlySerializedAsAttribute_t468_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8917_IList_1_get_Item_m50103_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50103_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FormerlySerializedAsAttribute_t468_0_0_0;
static ParameterInfo IList_1_t8917_IList_1_set_Item_m50104_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t468_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50104_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50104_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8917_IList_1_set_Item_m50104_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50104_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8917_MethodInfos[] =
{
	&IList_1_IndexOf_m50105_MethodInfo,
	&IList_1_Insert_m50106_MethodInfo,
	&IList_1_RemoveAt_m50107_MethodInfo,
	&IList_1_get_Item_m50103_MethodInfo,
	&IList_1_set_Item_m50104_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8917_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8916_il2cpp_TypeInfo,
	&IEnumerable_1_t8918_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8917_0_0_0;
extern Il2CppType IList_1_t8917_1_0_0;
struct IList_1_t8917;
extern Il2CppGenericClass IList_1_t8917_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8917_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8917_MethodInfos/* methods */
	, IList_1_t8917_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8917_il2cpp_TypeInfo/* element_class */
	, IList_1_t8917_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8917_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8917_0_0_0/* byval_arg */
	, &IList_1_t8917_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8917_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7017_il2cpp_TypeInfo;

// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRules>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRules>
extern MethodInfo IEnumerator_1_get_Current_m50108_MethodInfo;
static PropertyInfo IEnumerator_1_t7017____Current_PropertyInfo = 
{
	&IEnumerator_1_t7017_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50108_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7017_PropertyInfos[] =
{
	&IEnumerator_1_t7017____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
extern void* RuntimeInvoker_TypeInferenceRules_t1148 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50108_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRules>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50108_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7017_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRules_t1148_0_0_0/* return_type */
	, RuntimeInvoker_TypeInferenceRules_t1148/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50108_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7017_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50108_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7017_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7017_0_0_0;
extern Il2CppType IEnumerator_1_t7017_1_0_0;
struct IEnumerator_1_t7017;
extern Il2CppGenericClass IEnumerator_1_t7017_GenericClass;
TypeInfo IEnumerator_1_t7017_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7017_MethodInfos/* methods */
	, IEnumerator_1_t7017_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7017_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7017_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7017_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7017_0_0_0/* byval_arg */
	, &IEnumerator_1_t7017_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7017_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_519.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5037_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_519MethodDeclarations.h"

extern TypeInfo TypeInferenceRules_t1148_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30499_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeInferenceRules_t1148_m39390_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngineInternal.TypeInferenceRules>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngineInternal.TypeInferenceRules>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTypeInferenceRules_t1148_m39390 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30495_MethodInfo;
 void InternalEnumerator_1__ctor_m30495 (InternalEnumerator_1_t5037 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30496_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30496 (InternalEnumerator_1_t5037 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30499(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30499_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TypeInferenceRules_t1148_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30497_MethodInfo;
 void InternalEnumerator_1_Dispose_m30497 (InternalEnumerator_1_t5037 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30498_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30498 (InternalEnumerator_1_t5037 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30499 (InternalEnumerator_1_t5037 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTypeInferenceRules_t1148_m39390(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTypeInferenceRules_t1148_m39390_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5037____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5037_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5037, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5037____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5037_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5037, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5037_FieldInfos[] =
{
	&InternalEnumerator_1_t5037____array_0_FieldInfo,
	&InternalEnumerator_1_t5037____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5037____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5037_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30496_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5037____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5037_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30499_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5037_PropertyInfos[] =
{
	&InternalEnumerator_1_t5037____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5037____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5037_InternalEnumerator_1__ctor_m30495_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30495_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30495_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30495/* method */
	, &InternalEnumerator_1_t5037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5037_InternalEnumerator_1__ctor_m30495_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30495_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30496_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30496_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30496/* method */
	, &InternalEnumerator_1_t5037_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30496_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30497_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30497_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30497/* method */
	, &InternalEnumerator_1_t5037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30497_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30498_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30498_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30498/* method */
	, &InternalEnumerator_1_t5037_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30498_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
extern void* RuntimeInvoker_TypeInferenceRules_t1148 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30499_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30499_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30499/* method */
	, &InternalEnumerator_1_t5037_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRules_t1148_0_0_0/* return_type */
	, RuntimeInvoker_TypeInferenceRules_t1148/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30499_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5037_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30495_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30496_MethodInfo,
	&InternalEnumerator_1_Dispose_m30497_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30498_MethodInfo,
	&InternalEnumerator_1_get_Current_m30499_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5037_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30496_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30498_MethodInfo,
	&InternalEnumerator_1_Dispose_m30497_MethodInfo,
	&InternalEnumerator_1_get_Current_m30499_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5037_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7017_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5037_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7017_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5037_0_0_0;
extern Il2CppType InternalEnumerator_1_t5037_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5037_GenericClass;
TypeInfo InternalEnumerator_1_t5037_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5037_MethodInfos/* methods */
	, InternalEnumerator_1_t5037_PropertyInfos/* properties */
	, InternalEnumerator_1_t5037_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5037_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5037_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5037_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5037_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5037_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5037_1_0_0/* this_arg */
	, InternalEnumerator_1_t5037_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5037_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5037)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8919_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>
extern MethodInfo ICollection_1_get_Count_m50109_MethodInfo;
static PropertyInfo ICollection_1_t8919____Count_PropertyInfo = 
{
	&ICollection_1_t8919_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50109_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50110_MethodInfo;
static PropertyInfo ICollection_1_t8919____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8919_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50110_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8919_PropertyInfos[] =
{
	&ICollection_1_t8919____Count_PropertyInfo,
	&ICollection_1_t8919____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50109_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::get_Count()
MethodInfo ICollection_1_get_Count_m50109_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8919_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50109_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50110_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50110_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8919_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50110_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
static ParameterInfo ICollection_1_t8919_ICollection_1_Add_m50111_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1148_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50111_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Add(T)
MethodInfo ICollection_1_Add_m50111_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8919_ICollection_1_Add_m50111_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50111_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50112_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Clear()
MethodInfo ICollection_1_Clear_m50112_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50112_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
static ParameterInfo ICollection_1_t8919_ICollection_1_Contains_m50113_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1148_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50113_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Contains(T)
MethodInfo ICollection_1_Contains_m50113_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8919_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8919_ICollection_1_Contains_m50113_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50113_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRulesU5BU5D_t5769_0_0_0;
extern Il2CppType TypeInferenceRulesU5BU5D_t5769_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8919_ICollection_1_CopyTo_m50114_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRulesU5BU5D_t5769_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50114_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50114_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8919_ICollection_1_CopyTo_m50114_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50114_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
static ParameterInfo ICollection_1_t8919_ICollection_1_Remove_m50115_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1148_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50115_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Remove(T)
MethodInfo ICollection_1_Remove_m50115_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8919_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8919_ICollection_1_Remove_m50115_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50115_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8919_MethodInfos[] =
{
	&ICollection_1_get_Count_m50109_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50110_MethodInfo,
	&ICollection_1_Add_m50111_MethodInfo,
	&ICollection_1_Clear_m50112_MethodInfo,
	&ICollection_1_Contains_m50113_MethodInfo,
	&ICollection_1_CopyTo_m50114_MethodInfo,
	&ICollection_1_Remove_m50115_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8921_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8919_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8921_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8919_0_0_0;
extern Il2CppType ICollection_1_t8919_1_0_0;
struct ICollection_1_t8919;
extern Il2CppGenericClass ICollection_1_t8919_GenericClass;
TypeInfo ICollection_1_t8919_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8919_MethodInfos/* methods */
	, ICollection_1_t8919_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8919_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8919_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8919_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8919_0_0_0/* byval_arg */
	, &ICollection_1_t8919_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8919_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRules>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRules>
extern Il2CppType IEnumerator_1_t7017_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50116_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRules>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50116_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8921_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7017_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50116_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8921_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50116_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8921_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8921_0_0_0;
extern Il2CppType IEnumerable_1_t8921_1_0_0;
struct IEnumerable_1_t8921;
extern Il2CppGenericClass IEnumerable_1_t8921_GenericClass;
TypeInfo IEnumerable_1_t8921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8921_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8921_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8921_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8921_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8921_0_0_0/* byval_arg */
	, &IEnumerable_1_t8921_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8921_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8920_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>
extern MethodInfo IList_1_get_Item_m50117_MethodInfo;
extern MethodInfo IList_1_set_Item_m50118_MethodInfo;
static PropertyInfo IList_1_t8920____Item_PropertyInfo = 
{
	&IList_1_t8920_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50117_MethodInfo/* get */
	, &IList_1_set_Item_m50118_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8920_PropertyInfos[] =
{
	&IList_1_t8920____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
static ParameterInfo IList_1_t8920_IList_1_IndexOf_m50119_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1148_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50119_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50119_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8920_IList_1_IndexOf_m50119_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50119_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
static ParameterInfo IList_1_t8920_IList_1_Insert_m50120_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1148_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50120_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50120_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8920_IList_1_Insert_m50120_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50120_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8920_IList_1_RemoveAt_m50121_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50121_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50121_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8920_IList_1_RemoveAt_m50121_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50121_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8920_IList_1_get_Item_m50117_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
extern void* RuntimeInvoker_TypeInferenceRules_t1148_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50117_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50117_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRules_t1148_0_0_0/* return_type */
	, RuntimeInvoker_TypeInferenceRules_t1148_Int32_t123/* invoker_method */
	, IList_1_t8920_IList_1_get_Item_m50117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50117_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeInferenceRules_t1148_0_0_0;
static ParameterInfo IList_1_t8920_IList_1_set_Item_m50118_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1148_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50118_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50118_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8920_IList_1_set_Item_m50118_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50118_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8920_MethodInfos[] =
{
	&IList_1_IndexOf_m50119_MethodInfo,
	&IList_1_Insert_m50120_MethodInfo,
	&IList_1_RemoveAt_m50121_MethodInfo,
	&IList_1_get_Item_m50117_MethodInfo,
	&IList_1_set_Item_m50118_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8920_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8919_il2cpp_TypeInfo,
	&IEnumerable_1_t8921_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8920_0_0_0;
extern Il2CppType IList_1_t8920_1_0_0;
struct IList_1_t8920;
extern Il2CppGenericClass IList_1_t8920_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8920_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8920_MethodInfos/* methods */
	, IList_1_t8920_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8920_il2cpp_TypeInfo/* element_class */
	, IList_1_t8920_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8920_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8920_0_0_0/* byval_arg */
	, &IList_1_t8920_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8920_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7019_il2cpp_TypeInfo;

// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50122_MethodInfo;
static PropertyInfo IEnumerator_1_t7019____Current_PropertyInfo = 
{
	&IEnumerator_1_t7019_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50122_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7019_PropertyInfos[] =
{
	&IEnumerator_1_t7019____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50122_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50122_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7019_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRuleAttribute_t1149_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50122_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7019_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50122_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7019_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7019_0_0_0;
extern Il2CppType IEnumerator_1_t7019_1_0_0;
struct IEnumerator_1_t7019;
extern Il2CppGenericClass IEnumerator_1_t7019_GenericClass;
TypeInfo IEnumerator_1_t7019_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7019_MethodInfos/* methods */
	, IEnumerator_1_t7019_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7019_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7019_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7019_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7019_0_0_0/* byval_arg */
	, &IEnumerator_1_t7019_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7019_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_520.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5038_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_520MethodDeclarations.h"

extern TypeInfo TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30504_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeInferenceRuleAttribute_t1149_m39401_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngineInternal.TypeInferenceRuleAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngineInternal.TypeInferenceRuleAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisTypeInferenceRuleAttribute_t1149_m39401(__this, p0, method) (TypeInferenceRuleAttribute_t1149 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5038____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5038_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5038, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5038____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5038_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5038, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5038_FieldInfos[] =
{
	&InternalEnumerator_1_t5038____array_0_FieldInfo,
	&InternalEnumerator_1_t5038____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30501_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5038____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5038_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30501_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5038____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5038_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30504_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5038_PropertyInfos[] =
{
	&InternalEnumerator_1_t5038____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5038____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5038_InternalEnumerator_1__ctor_m30500_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30500_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30500_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5038_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5038_InternalEnumerator_1__ctor_m30500_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30500_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30501_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30501_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5038_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30501_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30502_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30502_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5038_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30502_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30503_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30503_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5038_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30503_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30504_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30504_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5038_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRuleAttribute_t1149_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30504_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5038_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30500_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30501_MethodInfo,
	&InternalEnumerator_1_Dispose_m30502_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30503_MethodInfo,
	&InternalEnumerator_1_get_Current_m30504_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30503_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30502_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5038_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30501_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30503_MethodInfo,
	&InternalEnumerator_1_Dispose_m30502_MethodInfo,
	&InternalEnumerator_1_get_Current_m30504_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5038_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7019_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5038_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7019_il2cpp_TypeInfo, 7},
};
extern TypeInfo TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5038_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30504_MethodInfo/* Method Usage */,
	&TypeInferenceRuleAttribute_t1149_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTypeInferenceRuleAttribute_t1149_m39401_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5038_0_0_0;
extern Il2CppType InternalEnumerator_1_t5038_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5038_GenericClass;
TypeInfo InternalEnumerator_1_t5038_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5038_MethodInfos/* methods */
	, InternalEnumerator_1_t5038_PropertyInfos/* properties */
	, InternalEnumerator_1_t5038_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5038_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5038_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5038_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5038_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5038_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5038_1_0_0/* this_arg */
	, InternalEnumerator_1_t5038_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5038_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5038_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5038)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8922_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern MethodInfo ICollection_1_get_Count_m50123_MethodInfo;
static PropertyInfo ICollection_1_t8922____Count_PropertyInfo = 
{
	&ICollection_1_t8922_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50123_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50124_MethodInfo;
static PropertyInfo ICollection_1_t8922____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8922_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50124_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8922_PropertyInfos[] =
{
	&ICollection_1_t8922____Count_PropertyInfo,
	&ICollection_1_t8922____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50123_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50123_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8922_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50123_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50124_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50124_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8922_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50124_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
static ParameterInfo ICollection_1_t8922_ICollection_1_Add_m50125_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1149_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50125_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50125_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8922_ICollection_1_Add_m50125_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50125_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50126_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50126_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50126_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
static ParameterInfo ICollection_1_t8922_ICollection_1_Contains_m50127_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1149_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50127_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50127_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8922_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8922_ICollection_1_Contains_m50127_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50127_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRuleAttributeU5BU5D_t5770_0_0_0;
extern Il2CppType TypeInferenceRuleAttributeU5BU5D_t5770_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8922_ICollection_1_CopyTo_m50128_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttributeU5BU5D_t5770_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50128_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50128_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8922_ICollection_1_CopyTo_m50128_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50128_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
static ParameterInfo ICollection_1_t8922_ICollection_1_Remove_m50129_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1149_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50129_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50129_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8922_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8922_ICollection_1_Remove_m50129_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50129_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8922_MethodInfos[] =
{
	&ICollection_1_get_Count_m50123_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50124_MethodInfo,
	&ICollection_1_Add_m50125_MethodInfo,
	&ICollection_1_Clear_m50126_MethodInfo,
	&ICollection_1_Contains_m50127_MethodInfo,
	&ICollection_1_CopyTo_m50128_MethodInfo,
	&ICollection_1_Remove_m50129_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8924_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8922_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8924_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8922_0_0_0;
extern Il2CppType ICollection_1_t8922_1_0_0;
struct ICollection_1_t8922;
extern Il2CppGenericClass ICollection_1_t8922_GenericClass;
TypeInfo ICollection_1_t8922_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8922_MethodInfos/* methods */
	, ICollection_1_t8922_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8922_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8922_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8922_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8922_0_0_0/* byval_arg */
	, &ICollection_1_t8922_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8922_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRuleAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern Il2CppType IEnumerator_1_t7019_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50130_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRuleAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50130_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8924_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7019_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50130_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8924_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50130_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8924_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8924_0_0_0;
extern Il2CppType IEnumerable_1_t8924_1_0_0;
struct IEnumerable_1_t8924;
extern Il2CppGenericClass IEnumerable_1_t8924_GenericClass;
TypeInfo IEnumerable_1_t8924_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8924_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8924_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8924_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8924_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8924_0_0_0/* byval_arg */
	, &IEnumerable_1_t8924_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8924_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8923_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern MethodInfo IList_1_get_Item_m50131_MethodInfo;
extern MethodInfo IList_1_set_Item_m50132_MethodInfo;
static PropertyInfo IList_1_t8923____Item_PropertyInfo = 
{
	&IList_1_t8923_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50131_MethodInfo/* get */
	, &IList_1_set_Item_m50132_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8923_PropertyInfos[] =
{
	&IList_1_t8923____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
static ParameterInfo IList_1_t8923_IList_1_IndexOf_m50133_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1149_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50133_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50133_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8923_IList_1_IndexOf_m50133_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50133_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
static ParameterInfo IList_1_t8923_IList_1_Insert_m50134_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1149_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50134_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50134_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8923_IList_1_Insert_m50134_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50134_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8923_IList_1_RemoveAt_m50135_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50135_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50135_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8923_IList_1_RemoveAt_m50135_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50135_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8923_IList_1_get_Item_m50131_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50131_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50131_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRuleAttribute_t1149_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8923_IList_1_get_Item_m50131_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50131_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeInferenceRuleAttribute_t1149_0_0_0;
static ParameterInfo IList_1_t8923_IList_1_set_Item_m50132_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1149_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50132_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50132_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8923_IList_1_set_Item_m50132_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50132_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8923_MethodInfos[] =
{
	&IList_1_IndexOf_m50133_MethodInfo,
	&IList_1_Insert_m50134_MethodInfo,
	&IList_1_RemoveAt_m50135_MethodInfo,
	&IList_1_get_Item_m50131_MethodInfo,
	&IList_1_set_Item_m50132_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8923_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8922_il2cpp_TypeInfo,
	&IEnumerable_1_t8924_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8923_0_0_0;
extern Il2CppType IList_1_t8923_1_0_0;
struct IList_1_t8923;
extern Il2CppGenericClass IList_1_t8923_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8923_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8923_MethodInfos/* methods */
	, IList_1_t8923_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8923_il2cpp_TypeInfo/* element_class */
	, IList_1_t8923_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8923_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8923_0_0_0/* byval_arg */
	, &IList_1_t8923_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8923_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7021_il2cpp_TypeInfo;

// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50136_MethodInfo;
static PropertyInfo IEnumerator_1_t7021____Current_PropertyInfo = 
{
	&IEnumerator_1_t7021_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50136_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7021_PropertyInfos[] =
{
	&IEnumerator_1_t7021____Current_PropertyInfo,
	NULL
};
extern Il2CppType ExtensionAttribute_t822_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50136_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50136_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7021_il2cpp_TypeInfo/* declaring_type */
	, &ExtensionAttribute_t822_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50136_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7021_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50136_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7021_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7021_0_0_0;
extern Il2CppType IEnumerator_1_t7021_1_0_0;
struct IEnumerator_1_t7021;
extern Il2CppGenericClass IEnumerator_1_t7021_GenericClass;
TypeInfo IEnumerator_1_t7021_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7021_MethodInfos/* methods */
	, IEnumerator_1_t7021_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7021_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7021_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7021_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7021_0_0_0/* byval_arg */
	, &IEnumerator_1_t7021_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7021_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_521.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5039_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_521MethodDeclarations.h"

extern TypeInfo ExtensionAttribute_t822_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30509_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisExtensionAttribute_t822_m39412_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.ExtensionAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.ExtensionAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisExtensionAttribute_t822_m39412(__this, p0, method) (ExtensionAttribute_t822 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5039____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5039_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5039, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5039____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5039_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5039, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5039_FieldInfos[] =
{
	&InternalEnumerator_1_t5039____array_0_FieldInfo,
	&InternalEnumerator_1_t5039____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30506_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5039____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5039_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30506_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5039____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5039_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30509_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5039_PropertyInfos[] =
{
	&InternalEnumerator_1_t5039____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5039____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5039_InternalEnumerator_1__ctor_m30505_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30505_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30505_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5039_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5039_InternalEnumerator_1__ctor_m30505_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30505_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30506_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30506_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5039_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30506_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30507_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30507_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5039_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30507_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30508_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30508_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5039_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30508_GenericMethod/* genericMethod */

};
extern Il2CppType ExtensionAttribute_t822_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30509_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30509_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5039_il2cpp_TypeInfo/* declaring_type */
	, &ExtensionAttribute_t822_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30509_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5039_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30505_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30506_MethodInfo,
	&InternalEnumerator_1_Dispose_m30507_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30508_MethodInfo,
	&InternalEnumerator_1_get_Current_m30509_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30508_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30507_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5039_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30506_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30508_MethodInfo,
	&InternalEnumerator_1_Dispose_m30507_MethodInfo,
	&InternalEnumerator_1_get_Current_m30509_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5039_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7021_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5039_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7021_il2cpp_TypeInfo, 7},
};
extern TypeInfo ExtensionAttribute_t822_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5039_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30509_MethodInfo/* Method Usage */,
	&ExtensionAttribute_t822_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisExtensionAttribute_t822_m39412_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5039_0_0_0;
extern Il2CppType InternalEnumerator_1_t5039_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5039_GenericClass;
TypeInfo InternalEnumerator_1_t5039_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5039_MethodInfos/* methods */
	, InternalEnumerator_1_t5039_PropertyInfos/* properties */
	, InternalEnumerator_1_t5039_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5039_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5039_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5039_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5039_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5039_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5039_1_0_0/* this_arg */
	, InternalEnumerator_1_t5039_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5039_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5039_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5039)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8925_il2cpp_TypeInfo;

#include "System.Core_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern MethodInfo ICollection_1_get_Count_m50137_MethodInfo;
static PropertyInfo ICollection_1_t8925____Count_PropertyInfo = 
{
	&ICollection_1_t8925_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50137_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50138_MethodInfo;
static PropertyInfo ICollection_1_t8925____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8925_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50138_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8925_PropertyInfos[] =
{
	&ICollection_1_t8925____Count_PropertyInfo,
	&ICollection_1_t8925____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50137_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50137_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8925_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50137_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50138_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50138_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8925_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50138_GenericMethod/* genericMethod */

};
extern Il2CppType ExtensionAttribute_t822_0_0_0;
extern Il2CppType ExtensionAttribute_t822_0_0_0;
static ParameterInfo ICollection_1_t8925_ICollection_1_Add_m50139_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t822_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50139_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50139_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8925_ICollection_1_Add_m50139_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50139_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50140_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50140_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50140_GenericMethod/* genericMethod */

};
extern Il2CppType ExtensionAttribute_t822_0_0_0;
static ParameterInfo ICollection_1_t8925_ICollection_1_Contains_m50141_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t822_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50141_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50141_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8925_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8925_ICollection_1_Contains_m50141_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50141_GenericMethod/* genericMethod */

};
extern Il2CppType ExtensionAttributeU5BU5D_t5882_0_0_0;
extern Il2CppType ExtensionAttributeU5BU5D_t5882_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8925_ICollection_1_CopyTo_m50142_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ExtensionAttributeU5BU5D_t5882_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50142_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50142_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8925_ICollection_1_CopyTo_m50142_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50142_GenericMethod/* genericMethod */

};
extern Il2CppType ExtensionAttribute_t822_0_0_0;
static ParameterInfo ICollection_1_t8925_ICollection_1_Remove_m50143_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t822_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50143_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50143_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8925_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8925_ICollection_1_Remove_m50143_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50143_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8925_MethodInfos[] =
{
	&ICollection_1_get_Count_m50137_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50138_MethodInfo,
	&ICollection_1_Add_m50139_MethodInfo,
	&ICollection_1_Clear_m50140_MethodInfo,
	&ICollection_1_Contains_m50141_MethodInfo,
	&ICollection_1_CopyTo_m50142_MethodInfo,
	&ICollection_1_Remove_m50143_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8927_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8925_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8927_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8925_0_0_0;
extern Il2CppType ICollection_1_t8925_1_0_0;
struct ICollection_1_t8925;
extern Il2CppGenericClass ICollection_1_t8925_GenericClass;
TypeInfo ICollection_1_t8925_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8925_MethodInfos/* methods */
	, ICollection_1_t8925_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8925_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8925_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8925_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8925_0_0_0/* byval_arg */
	, &ICollection_1_t8925_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8925_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.ExtensionAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern Il2CppType IEnumerator_1_t7021_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50144_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.ExtensionAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50144_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8927_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7021_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50144_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8927_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50144_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8927_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8927_0_0_0;
extern Il2CppType IEnumerable_1_t8927_1_0_0;
struct IEnumerable_1_t8927;
extern Il2CppGenericClass IEnumerable_1_t8927_GenericClass;
TypeInfo IEnumerable_1_t8927_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8927_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8927_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8927_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8927_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8927_0_0_0/* byval_arg */
	, &IEnumerable_1_t8927_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8927_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8926_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern MethodInfo IList_1_get_Item_m50145_MethodInfo;
extern MethodInfo IList_1_set_Item_m50146_MethodInfo;
static PropertyInfo IList_1_t8926____Item_PropertyInfo = 
{
	&IList_1_t8926_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50145_MethodInfo/* get */
	, &IList_1_set_Item_m50146_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8926_PropertyInfos[] =
{
	&IList_1_t8926____Item_PropertyInfo,
	NULL
};
extern Il2CppType ExtensionAttribute_t822_0_0_0;
static ParameterInfo IList_1_t8926_IList_1_IndexOf_m50147_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t822_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50147_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50147_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8926_IList_1_IndexOf_m50147_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50147_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ExtensionAttribute_t822_0_0_0;
static ParameterInfo IList_1_t8926_IList_1_Insert_m50148_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t822_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50148_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50148_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8926_IList_1_Insert_m50148_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50148_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8926_IList_1_RemoveAt_m50149_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50149_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50149_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8926_IList_1_RemoveAt_m50149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50149_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8926_IList_1_get_Item_m50145_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ExtensionAttribute_t822_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50145_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50145_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &ExtensionAttribute_t822_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8926_IList_1_get_Item_m50145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50145_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ExtensionAttribute_t822_0_0_0;
static ParameterInfo IList_1_t8926_IList_1_set_Item_m50146_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t822_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50146_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50146_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8926_IList_1_set_Item_m50146_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50146_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8926_MethodInfos[] =
{
	&IList_1_IndexOf_m50147_MethodInfo,
	&IList_1_Insert_m50148_MethodInfo,
	&IList_1_RemoveAt_m50149_MethodInfo,
	&IList_1_get_Item_m50145_MethodInfo,
	&IList_1_set_Item_m50146_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8926_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8925_il2cpp_TypeInfo,
	&IEnumerable_1_t8927_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8926_0_0_0;
extern Il2CppType IList_1_t8926_1_0_0;
struct IList_1_t8926;
extern Il2CppGenericClass IList_1_t8926_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8926_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8926_MethodInfos/* methods */
	, IList_1_t8926_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8926_il2cpp_TypeInfo/* element_class */
	, IList_1_t8926_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8926_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8926_0_0_0/* byval_arg */
	, &IList_1_t8926_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8926_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7023_il2cpp_TypeInfo;

// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50150_MethodInfo;
static PropertyInfo IEnumerator_1_t7023____Current_PropertyInfo = 
{
	&IEnumerator_1_t7023_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50150_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7023_PropertyInfos[] =
{
	&IEnumerator_1_t7023____Current_PropertyInfo,
	NULL
};
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50150_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50150_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7023_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1271_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50150_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7023_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50150_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7023_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7023_0_0_0;
extern Il2CppType IEnumerator_1_t7023_1_0_0;
struct IEnumerator_1_t7023;
extern Il2CppGenericClass IEnumerator_1_t7023_GenericClass;
TypeInfo IEnumerator_1_t7023_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7023_MethodInfos/* methods */
	, IEnumerator_1_t7023_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7023_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7023_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7023_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7023_0_0_0/* byval_arg */
	, &IEnumerator_1_t7023_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7023_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_522.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5040_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_522MethodDeclarations.h"

extern TypeInfo MonoTODOAttribute_t1271_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30514_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMonoTODOAttribute_t1271_m39423_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.MonoTODOAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.MonoTODOAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisMonoTODOAttribute_t1271_m39423(__this, p0, method) (MonoTODOAttribute_t1271 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5040____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5040_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5040, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5040____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5040_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5040, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5040_FieldInfos[] =
{
	&InternalEnumerator_1_t5040____array_0_FieldInfo,
	&InternalEnumerator_1_t5040____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30511_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5040____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5040_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30511_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5040____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5040_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30514_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5040_PropertyInfos[] =
{
	&InternalEnumerator_1_t5040____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5040____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5040_InternalEnumerator_1__ctor_m30510_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30510_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30510_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5040_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5040_InternalEnumerator_1__ctor_m30510_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30510_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30511_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30511_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5040_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30511_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30512_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30512_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5040_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30512_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30513_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30513_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5040_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30513_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30514_GenericMethod;
// T System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30514_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5040_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1271_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30514_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5040_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30510_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30511_MethodInfo,
	&InternalEnumerator_1_Dispose_m30512_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30513_MethodInfo,
	&InternalEnumerator_1_get_Current_m30514_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30513_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30512_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5040_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30511_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30513_MethodInfo,
	&InternalEnumerator_1_Dispose_m30512_MethodInfo,
	&InternalEnumerator_1_get_Current_m30514_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5040_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7023_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5040_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7023_il2cpp_TypeInfo, 7},
};
extern TypeInfo MonoTODOAttribute_t1271_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5040_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30514_MethodInfo/* Method Usage */,
	&MonoTODOAttribute_t1271_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMonoTODOAttribute_t1271_m39423_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5040_0_0_0;
extern Il2CppType InternalEnumerator_1_t5040_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5040_GenericClass;
TypeInfo InternalEnumerator_1_t5040_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5040_MethodInfos/* methods */
	, InternalEnumerator_1_t5040_PropertyInfos/* properties */
	, InternalEnumerator_1_t5040_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5040_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5040_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5040_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5040_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5040_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5040_1_0_0/* this_arg */
	, InternalEnumerator_1_t5040_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5040_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5040_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5040)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8928_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>
extern MethodInfo ICollection_1_get_Count_m50151_MethodInfo;
static PropertyInfo ICollection_1_t8928____Count_PropertyInfo = 
{
	&ICollection_1_t8928_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50151_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50152_MethodInfo;
static PropertyInfo ICollection_1_t8928____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8928_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50152_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8928_PropertyInfos[] =
{
	&ICollection_1_t8928____Count_PropertyInfo,
	&ICollection_1_t8928____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50151_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50151_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8928_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50151_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50152_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50152_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8928_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50152_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
static ParameterInfo ICollection_1_t8928_ICollection_1_Add_m50153_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1271_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50153_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50153_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8928_ICollection_1_Add_m50153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50153_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50154_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50154_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50154_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
static ParameterInfo ICollection_1_t8928_ICollection_1_Contains_m50155_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1271_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50155_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50155_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8928_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8928_ICollection_1_Contains_m50155_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50155_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttributeU5BU5D_t5883_0_0_0;
extern Il2CppType MonoTODOAttributeU5BU5D_t5883_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8928_ICollection_1_CopyTo_m50156_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttributeU5BU5D_t5883_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50156_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50156_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8928_ICollection_1_CopyTo_m50156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50156_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
static ParameterInfo ICollection_1_t8928_ICollection_1_Remove_m50157_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1271_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50157_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50157_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8928_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8928_ICollection_1_Remove_m50157_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50157_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8928_MethodInfos[] =
{
	&ICollection_1_get_Count_m50151_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50152_MethodInfo,
	&ICollection_1_Add_m50153_MethodInfo,
	&ICollection_1_Clear_m50154_MethodInfo,
	&ICollection_1_Contains_m50155_MethodInfo,
	&ICollection_1_CopyTo_m50156_MethodInfo,
	&ICollection_1_Remove_m50157_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8930_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8928_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8930_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8928_0_0_0;
extern Il2CppType ICollection_1_t8928_1_0_0;
struct ICollection_1_t8928;
extern Il2CppGenericClass ICollection_1_t8928_GenericClass;
TypeInfo ICollection_1_t8928_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8928_MethodInfos/* methods */
	, ICollection_1_t8928_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8928_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8928_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8928_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8928_0_0_0/* byval_arg */
	, &ICollection_1_t8928_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8928_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>
extern Il2CppType IEnumerator_1_t7023_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50158_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50158_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8930_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7023_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50158_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8930_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50158_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8930_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8930_0_0_0;
extern Il2CppType IEnumerable_1_t8930_1_0_0;
struct IEnumerable_1_t8930;
extern Il2CppGenericClass IEnumerable_1_t8930_GenericClass;
TypeInfo IEnumerable_1_t8930_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8930_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8930_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8930_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8930_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8930_0_0_0/* byval_arg */
	, &IEnumerable_1_t8930_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8930_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8929_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.MonoTODOAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.MonoTODOAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.MonoTODOAttribute>
extern MethodInfo IList_1_get_Item_m50159_MethodInfo;
extern MethodInfo IList_1_set_Item_m50160_MethodInfo;
static PropertyInfo IList_1_t8929____Item_PropertyInfo = 
{
	&IList_1_t8929_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50159_MethodInfo/* get */
	, &IList_1_set_Item_m50160_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8929_PropertyInfos[] =
{
	&IList_1_t8929____Item_PropertyInfo,
	NULL
};
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
static ParameterInfo IList_1_t8929_IList_1_IndexOf_m50161_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1271_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50161_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.MonoTODOAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50161_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8929_IList_1_IndexOf_m50161_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50161_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
static ParameterInfo IList_1_t8929_IList_1_Insert_m50162_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1271_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50162_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50162_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8929_IList_1_Insert_m50162_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50162_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8929_IList_1_RemoveAt_m50163_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50163_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50163_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8929_IList_1_RemoveAt_m50163_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50163_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8929_IList_1_get_Item_m50159_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50159_GenericMethod;
// T System.Collections.Generic.IList`1<System.MonoTODOAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50159_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1271_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8929_IList_1_get_Item_m50159_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50159_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
static ParameterInfo IList_1_t8929_IList_1_set_Item_m50160_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1271_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50160_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50160_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8929_IList_1_set_Item_m50160_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50160_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8929_MethodInfos[] =
{
	&IList_1_IndexOf_m50161_MethodInfo,
	&IList_1_Insert_m50162_MethodInfo,
	&IList_1_RemoveAt_m50163_MethodInfo,
	&IList_1_get_Item_m50159_MethodInfo,
	&IList_1_set_Item_m50160_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8929_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8928_il2cpp_TypeInfo,
	&IEnumerable_1_t8930_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8929_0_0_0;
extern Il2CppType IList_1_t8929_1_0_0;
struct IList_1_t8929;
extern Il2CppGenericClass IList_1_t8929_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8929_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8929_MethodInfos/* methods */
	, IList_1_t8929_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8929_il2cpp_TypeInfo/* element_class */
	, IList_1_t8929_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8929_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8929_0_0_0/* byval_arg */
	, &IList_1_t8929_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8929_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Func`2<System.Object,System.Object>
#include "System_Core_System_Func_2_gen_2.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Func_2_t5041_il2cpp_TypeInfo;
// System.Func`2<System.Object,System.Object>
#include "System_Core_System_Func_2_gen_2MethodDeclarations.h"



// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern MethodInfo Func_2__ctor_m30515_MethodInfo;
 void Func_2__ctor_m30515_gshared (Func_2_t5041 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
extern MethodInfo Func_2_Invoke_m30516_MethodInfo;
 Object_t * Func_2_Invoke_m30516_gshared (Func_2_t5041 * __this, Object_t * ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Func_2_Invoke_m30516((Func_2_t5041 *)__this->___prev_9,___arg1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___arg1, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, Object_t * ___arg1, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern MethodInfo Func_2_BeginInvoke_m30517_MethodInfo;
 Object_t * Func_2_BeginInvoke_m30517_gshared (Func_2_t5041 * __this, Object_t * ___arg1, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg1;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TResult System.Func`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern MethodInfo Func_2_EndInvoke_m30518_MethodInfo;
 Object_t * Func_2_EndInvoke_m30518_gshared (Func_2_t5041 * __this, Object_t * ___result, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// Metadata Definition System.Func`2<System.Object,System.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Func_2_t5041_Func_2__ctor_m30515_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2__ctor_m30515_GenericMethod;
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
MethodInfo Func_2__ctor_m30515_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Func_2__ctor_m30515_gshared/* method */
	, &Func_2_t5041_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Func_2_t5041_Func_2__ctor_m30515_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2__ctor_m30515_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Func_2_t5041_Func_2_Invoke_m30516_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2_Invoke_m30516_GenericMethod;
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
MethodInfo Func_2_Invoke_m30516_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Func_2_Invoke_m30516_gshared/* method */
	, &Func_2_t5041_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Func_2_t5041_Func_2_Invoke_m30516_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2_Invoke_m30516_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Func_2_t5041_Func_2_BeginInvoke_m30517_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2_BeginInvoke_m30517_GenericMethod;
// System.IAsyncResult System.Func`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Func_2_BeginInvoke_m30517_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Func_2_BeginInvoke_m30517_gshared/* method */
	, &Func_2_t5041_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Func_2_t5041_Func_2_BeginInvoke_m30517_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2_BeginInvoke_m30517_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Func_2_t5041_Func_2_EndInvoke_m30518_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2_EndInvoke_m30518_GenericMethod;
// TResult System.Func`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
MethodInfo Func_2_EndInvoke_m30518_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Func_2_EndInvoke_m30518_gshared/* method */
	, &Func_2_t5041_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Func_2_t5041_Func_2_EndInvoke_m30518_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2_EndInvoke_m30518_GenericMethod/* genericMethod */

};
static MethodInfo* Func_2_t5041_MethodInfos[] =
{
	&Func_2__ctor_m30515_MethodInfo,
	&Func_2_Invoke_m30516_MethodInfo,
	&Func_2_BeginInvoke_m30517_MethodInfo,
	&Func_2_EndInvoke_m30518_MethodInfo,
	NULL
};
static MethodInfo* Func_2_t5041_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Func_2_Invoke_m30516_MethodInfo,
	&Func_2_BeginInvoke_m30517_MethodInfo,
	&Func_2_EndInvoke_m30518_MethodInfo,
};
static Il2CppInterfaceOffsetPair Func_2_t5041_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Func_2_t5041_0_0_0;
extern Il2CppType Func_2_t5041_1_0_0;
struct Func_2_t5041;
extern Il2CppGenericClass Func_2_t5041_GenericClass;
TypeInfo Func_2_t5041_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`2"/* name */
	, "System"/* namespaze */
	, Func_2_t5041_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Func_2_t5041_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Func_2_t5041_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Func_2_t5041_il2cpp_TypeInfo/* cast_class */
	, &Func_2_t5041_0_0_0/* byval_arg */
	, &Func_2_t5041_1_0_0/* this_arg */
	, Func_2_t5041_InterfacesOffsets/* interface_offsets */
	, &Func_2_t5041_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Func_2_t5041)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7025_il2cpp_TypeInfo;

// System.MonoTODOAttribute
#include "System_System_MonoTODOAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50164_MethodInfo;
static PropertyInfo IEnumerator_1_t7025____Current_PropertyInfo = 
{
	&IEnumerator_1_t7025_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50164_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7025_PropertyInfos[] =
{
	&IEnumerator_1_t7025____Current_PropertyInfo,
	NULL
};
extern Il2CppType MonoTODOAttribute_t1342_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50164_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50164_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7025_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1342_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50164_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7025_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50164_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7025_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7025_0_0_0;
extern Il2CppType IEnumerator_1_t7025_1_0_0;
struct IEnumerator_1_t7025;
extern Il2CppGenericClass IEnumerator_1_t7025_GenericClass;
TypeInfo IEnumerator_1_t7025_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7025_MethodInfos/* methods */
	, IEnumerator_1_t7025_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7025_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7025_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7025_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7025_0_0_0/* byval_arg */
	, &IEnumerator_1_t7025_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7025_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_523.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5042_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_523MethodDeclarations.h"

extern TypeInfo MonoTODOAttribute_t1342_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30523_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMonoTODOAttribute_t1342_m39434_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.MonoTODOAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.MonoTODOAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisMonoTODOAttribute_t1342_m39434(__this, p0, method) (MonoTODOAttribute_t1342 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5042____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5042_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5042, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5042____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5042_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5042, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5042_FieldInfos[] =
{
	&InternalEnumerator_1_t5042____array_0_FieldInfo,
	&InternalEnumerator_1_t5042____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30520_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5042____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5042_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30520_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5042____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5042_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30523_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5042_PropertyInfos[] =
{
	&InternalEnumerator_1_t5042____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5042____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5042_InternalEnumerator_1__ctor_m30519_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30519_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30519_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5042_InternalEnumerator_1__ctor_m30519_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30519_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30520_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30520_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5042_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30520_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30521_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30521_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30521_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30522_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30522_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5042_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30522_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1342_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30523_GenericMethod;
// T System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30523_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5042_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1342_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30523_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5042_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30519_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30520_MethodInfo,
	&InternalEnumerator_1_Dispose_m30521_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30522_MethodInfo,
	&InternalEnumerator_1_get_Current_m30523_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30522_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30521_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5042_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30520_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30522_MethodInfo,
	&InternalEnumerator_1_Dispose_m30521_MethodInfo,
	&InternalEnumerator_1_get_Current_m30523_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5042_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7025_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5042_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7025_il2cpp_TypeInfo, 7},
};
extern TypeInfo MonoTODOAttribute_t1342_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5042_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30523_MethodInfo/* Method Usage */,
	&MonoTODOAttribute_t1342_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMonoTODOAttribute_t1342_m39434_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5042_0_0_0;
extern Il2CppType InternalEnumerator_1_t5042_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5042_GenericClass;
TypeInfo InternalEnumerator_1_t5042_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5042_MethodInfos/* methods */
	, InternalEnumerator_1_t5042_PropertyInfos/* properties */
	, InternalEnumerator_1_t5042_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5042_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5042_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5042_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5042_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5042_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5042_1_0_0/* this_arg */
	, InternalEnumerator_1_t5042_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5042_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5042_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5042)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8931_il2cpp_TypeInfo;

#include "System_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>
extern MethodInfo ICollection_1_get_Count_m50165_MethodInfo;
static PropertyInfo ICollection_1_t8931____Count_PropertyInfo = 
{
	&ICollection_1_t8931_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50165_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50166_MethodInfo;
static PropertyInfo ICollection_1_t8931____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8931_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50166_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8931_PropertyInfos[] =
{
	&ICollection_1_t8931____Count_PropertyInfo,
	&ICollection_1_t8931____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50165_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50165_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8931_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50165_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50166_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50166_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8931_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50166_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1342_0_0_0;
extern Il2CppType MonoTODOAttribute_t1342_0_0_0;
static ParameterInfo ICollection_1_t8931_ICollection_1_Add_m50167_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1342_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50167_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50167_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8931_ICollection_1_Add_m50167_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50167_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50168_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50168_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50168_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1342_0_0_0;
static ParameterInfo ICollection_1_t8931_ICollection_1_Contains_m50169_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1342_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50169_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50169_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8931_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8931_ICollection_1_Contains_m50169_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50169_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttributeU5BU5D_t5884_0_0_0;
extern Il2CppType MonoTODOAttributeU5BU5D_t5884_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8931_ICollection_1_CopyTo_m50170_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttributeU5BU5D_t5884_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50170_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50170_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8931_ICollection_1_CopyTo_m50170_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50170_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1342_0_0_0;
static ParameterInfo ICollection_1_t8931_ICollection_1_Remove_m50171_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1342_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50171_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50171_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8931_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8931_ICollection_1_Remove_m50171_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50171_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8931_MethodInfos[] =
{
	&ICollection_1_get_Count_m50165_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50166_MethodInfo,
	&ICollection_1_Add_m50167_MethodInfo,
	&ICollection_1_Clear_m50168_MethodInfo,
	&ICollection_1_Contains_m50169_MethodInfo,
	&ICollection_1_CopyTo_m50170_MethodInfo,
	&ICollection_1_Remove_m50171_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8933_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8931_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8933_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8931_0_0_0;
extern Il2CppType ICollection_1_t8931_1_0_0;
struct ICollection_1_t8931;
extern Il2CppGenericClass ICollection_1_t8931_GenericClass;
TypeInfo ICollection_1_t8931_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8931_MethodInfos/* methods */
	, ICollection_1_t8931_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8931_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8931_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8931_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8931_0_0_0/* byval_arg */
	, &ICollection_1_t8931_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8931_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>
extern Il2CppType IEnumerator_1_t7025_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50172_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50172_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8933_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7025_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50172_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8933_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50172_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8933_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8933_0_0_0;
extern Il2CppType IEnumerable_1_t8933_1_0_0;
struct IEnumerable_1_t8933;
extern Il2CppGenericClass IEnumerable_1_t8933_GenericClass;
TypeInfo IEnumerable_1_t8933_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8933_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8933_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8933_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8933_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8933_0_0_0/* byval_arg */
	, &IEnumerable_1_t8933_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8933_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8932_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.MonoTODOAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.MonoTODOAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.MonoTODOAttribute>
extern MethodInfo IList_1_get_Item_m50173_MethodInfo;
extern MethodInfo IList_1_set_Item_m50174_MethodInfo;
static PropertyInfo IList_1_t8932____Item_PropertyInfo = 
{
	&IList_1_t8932_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50173_MethodInfo/* get */
	, &IList_1_set_Item_m50174_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8932_PropertyInfos[] =
{
	&IList_1_t8932____Item_PropertyInfo,
	NULL
};
extern Il2CppType MonoTODOAttribute_t1342_0_0_0;
static ParameterInfo IList_1_t8932_IList_1_IndexOf_m50175_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1342_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50175_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.MonoTODOAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50175_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8932_IList_1_IndexOf_m50175_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50175_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MonoTODOAttribute_t1342_0_0_0;
static ParameterInfo IList_1_t8932_IList_1_Insert_m50176_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1342_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50176_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50176_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8932_IList_1_Insert_m50176_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50176_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8932_IList_1_RemoveAt_m50177_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50177_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50177_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8932_IList_1_RemoveAt_m50177_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50177_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8932_IList_1_get_Item_m50173_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MonoTODOAttribute_t1342_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50173_GenericMethod;
// T System.Collections.Generic.IList`1<System.MonoTODOAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50173_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1342_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8932_IList_1_get_Item_m50173_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50173_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MonoTODOAttribute_t1342_0_0_0;
static ParameterInfo IList_1_t8932_IList_1_set_Item_m50174_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1342_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50174_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50174_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8932_IList_1_set_Item_m50174_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50174_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8932_MethodInfos[] =
{
	&IList_1_IndexOf_m50175_MethodInfo,
	&IList_1_Insert_m50176_MethodInfo,
	&IList_1_RemoveAt_m50177_MethodInfo,
	&IList_1_get_Item_m50173_MethodInfo,
	&IList_1_set_Item_m50174_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8932_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8931_il2cpp_TypeInfo,
	&IEnumerable_1_t8933_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8932_0_0_0;
extern Il2CppType IList_1_t8932_1_0_0;
struct IList_1_t8932;
extern Il2CppGenericClass IList_1_t8932_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8932_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8932_MethodInfos/* methods */
	, IList_1_t8932_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8932_il2cpp_TypeInfo/* element_class */
	, IList_1_t8932_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8932_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8932_0_0_0/* byval_arg */
	, &IList_1_t8932_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8932_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.LinkedList`1<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo LinkedList_1_t5044_il2cpp_TypeInfo;
// System.Collections.Generic.LinkedList`1<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_gen_0MethodDeclarations.h"

// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Collections.Generic.LinkedListNode`1<System.Object>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_0.h"
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
extern TypeInfo Enumerator_t5045_il2cpp_TypeInfo;
extern TypeInfo ArgumentNullException_t1224_il2cpp_TypeInfo;
extern TypeInfo LinkedListNode_1_t5043_il2cpp_TypeInfo;
extern TypeInfo ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo;
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.Collections.Generic.LinkedListNode`1<System.Object>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_0MethodDeclarations.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0MethodDeclarations.h"
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
extern MethodInfo Object__ctor_m312_MethodInfo;
extern MethodInfo LinkedList_1__ctor_m30524_MethodInfo;
extern MethodInfo LinkedList_1_AddLast_m30534_MethodInfo;
extern MethodInfo LinkedList_1_CopyTo_m30537_MethodInfo;
extern MethodInfo LinkedList_1_GetEnumerator_m30539_MethodInfo;
extern MethodInfo ArgumentNullException__ctor_m6710_MethodInfo;
extern MethodInfo LinkedListNode_1_get_List_m30549_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7835_MethodInfo;
extern MethodInfo LinkedListNode_1__ctor_m30546_MethodInfo;
extern MethodInfo LinkedListNode_1__ctor_m30547_MethodInfo;
extern MethodInfo LinkedListNode_1_get_Value_m30551_MethodInfo;
extern MethodInfo Array_GetLowerBound_m9762_MethodInfo;
extern MethodInfo ArgumentOutOfRangeException__ctor_m7836_MethodInfo;
extern MethodInfo Array_get_Rank_m7838_MethodInfo;
extern MethodInfo ArgumentException__ctor_m7834_MethodInfo;
extern MethodInfo Enumerator__ctor_m30552_MethodInfo;
extern MethodInfo SerializationInfo_AddValue_m7843_MethodInfo;
extern MethodInfo SerializationInfo_AddValue_m12001_MethodInfo;
extern MethodInfo SerializationInfo_GetValue_m7846_MethodInfo;
extern MethodInfo SerializationInfo_GetUInt32_m12004_MethodInfo;
extern MethodInfo LinkedList_1_Find_m30538_MethodInfo;
extern MethodInfo LinkedList_1_Remove_m30543_MethodInfo;
extern MethodInfo LinkedList_1_VerifyReferencedNode_m30533_MethodInfo;
extern MethodInfo LinkedListNode_1_Detach_m30548_MethodInfo;


// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
 void LinkedList_1__ctor_m30524_gshared (LinkedList_1_t5044 * __this, MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (InitializedTypeInfo(&Object_t_il2cpp_TypeInfo));
		Object__ctor_m312(L_0, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		__this->___syncRoot_2 = L_0;
		__this->___first_3 = (LinkedListNode_1_t5043 *)NULL;
		int32_t L_1 = 0;
		V_0 = L_1;
		__this->___version_1 = L_1;
		__this->___count_0 = V_0;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern MethodInfo LinkedList_1__ctor_m30525_MethodInfo;
 void LinkedList_1__ctor_m30525_gshared (LinkedList_1_t5044 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method)
{
	{
		(( void (*) (LinkedList_1_t5044 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___si_4 = ___info;
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (InitializedTypeInfo(&Object_t_il2cpp_TypeInfo));
		Object__ctor_m312(L_0, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		__this->___syncRoot_2 = L_0;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern MethodInfo LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_MethodInfo;
 void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_gshared (LinkedList_1_t5044 * __this, Object_t * ___value, MethodInfo* method)
{
	{
		(( LinkedListNode_1_t5043 * (*) (LinkedList_1_t5044 * __this, Object_t * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(__this, ___value, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern MethodInfo LinkedList_1_System_Collections_ICollection_CopyTo_m30527_MethodInfo;
 void LinkedList_1_System_Collections_ICollection_CopyTo_m30527_gshared (LinkedList_1_t5044 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method)
{
	ObjectU5BU5D_t130* V_0 = {0};
	{
		V_0 = ((ObjectU5BU5D_t130*)IsInst(___array, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		if (V_0)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t551 * L_0 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0018:
	{
		VirtActionInvoker2< ObjectU5BU5D_t130*, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), __this, V_0, ___index);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern MethodInfo LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528_MethodInfo;
 Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528_gshared (LinkedList_1_t5044 * __this, MethodInfo* method)
{
	{
		Enumerator_t5045  L_0 = (( Enumerator_t5045  (*) (LinkedList_1_t5044 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Enumerator_t5045  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern MethodInfo LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529_MethodInfo;
 Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529_gshared (LinkedList_1_t5044 * __this, MethodInfo* method)
{
	{
		Enumerator_t5045  L_0 = (( Enumerator_t5045  (*) (LinkedList_1_t5044 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Enumerator_t5045  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern MethodInfo LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_MethodInfo;
 bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_gshared (LinkedList_1_t5044 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern MethodInfo LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_MethodInfo;
 bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_gshared (LinkedList_1_t5044 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern MethodInfo LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_MethodInfo;
 Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_gshared (LinkedList_1_t5044 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___syncRoot_2);
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedList_1_VerifyReferencedNode_m30533_gshared (LinkedList_1_t5044 * __this, LinkedListNode_1_t5043 * ___node, MethodInfo* method)
{
	{
		if (___node)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral481, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		NullCheck(___node);
		LinkedList_1_t5044 * L_1 = (( LinkedList_1_t5044 * (*) (LinkedListNode_1_t5043 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(___node, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((LinkedList_1_t5044 *)L_1) == ((LinkedList_1_t5044 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1546 * L_2 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7835(L_2, /*hidden argument*/&InvalidOperationException__ctor_m7835_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
 LinkedListNode_1_t5043 * LinkedList_1_AddLast_m30534_gshared (LinkedList_1_t5044 * __this, Object_t * ___value, MethodInfo* method)
{
	LinkedListNode_1_t5043 * V_0 = {0};
	{
		LinkedListNode_1_t5043 * L_0 = (__this->___first_3);
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		LinkedListNode_1_t5043 * L_1 = (LinkedListNode_1_t5043 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (LinkedListNode_1_t5043 * __this, LinkedList_1_t5044 * p0, Object_t * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_1, __this, ___value, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = L_1;
		__this->___first_3 = V_0;
		goto IL_0038;
	}

IL_001f:
	{
		LinkedListNode_1_t5043 * L_2 = (__this->___first_3);
		NullCheck(L_2);
		LinkedListNode_1_t5043 * L_3 = (L_2->___back_3);
		LinkedListNode_1_t5043 * L_4 = (__this->___first_3);
		LinkedListNode_1_t5043 * L_5 = (LinkedListNode_1_t5043 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (LinkedListNode_1_t5043 * __this, LinkedList_1_t5044 * p0, Object_t * p1, LinkedListNode_1_t5043 * p2, LinkedListNode_1_t5043 * p3, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_5, __this, ___value, L_3, L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = L_5;
	}

IL_0038:
	{
		uint32_t L_6 = (__this->___count_0);
		__this->___count_0 = ((int32_t)(L_6+1));
		uint32_t L_7 = (__this->___version_1);
		__this->___version_1 = ((int32_t)(L_7+1));
		return V_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern MethodInfo LinkedList_1_Clear_m30535_MethodInfo;
 void LinkedList_1_Clear_m30535_gshared (LinkedList_1_t5044 * __this, MethodInfo* method)
{
	{
		__this->___count_0 = 0;
		__this->___first_3 = (LinkedListNode_1_t5043 *)NULL;
		uint32_t L_0 = (__this->___version_1);
		__this->___version_1 = ((int32_t)(L_0+1));
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern MethodInfo LinkedList_1_Contains_m30536_MethodInfo;
 bool LinkedList_1_Contains_m30536_gshared (LinkedList_1_t5044 * __this, Object_t * ___value, MethodInfo* method)
{
	LinkedListNode_1_t5043 * V_0 = {0};
	{
		LinkedListNode_1_t5043 * L_0 = (__this->___first_3);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		NullCheck(V_0);
		Object_t * L_1 = (( Object_t * (*) (LinkedListNode_1_t5043 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(V_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Object_t * L_2 = L_1;
		NullCheck((*(&___value)));
		bool L_3 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, (*(&___value)), ((Object_t *)L_2));
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		NullCheck(V_0);
		LinkedListNode_1_t5043 * L_4 = (V_0->___forward_2);
		V_0 = L_4;
		LinkedListNode_1_t5043 * L_5 = (__this->___first_3);
		if ((((LinkedListNode_1_t5043 *)V_0) != ((LinkedListNode_1_t5043 *)L_5)))
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
 void LinkedList_1_CopyTo_m30537_gshared (LinkedList_1_t5044 * __this, ObjectU5BU5D_t130* ___array, int32_t ___index, MethodInfo* method)
{
	LinkedListNode_1_t5043 * V_0 = {0};
	{
		if (___array)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		NullCheck(___array);
		int32_t L_1 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		if ((((uint32_t)___index) >= ((uint32_t)L_1)))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_2 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_2, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0029:
	{
		NullCheck(___array);
		int32_t L_3 = Array_get_Rank_m7838(___array, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t551 * L_4 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m7834(L_4, (String_t*) &_stringLiteral473, (String_t*) &_stringLiteral482, /*hidden argument*/&ArgumentException__ctor_m7834_MethodInfo);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0045:
	{
		NullCheck(___array);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		uint32_t L_6 = (__this->___count_0);
		if ((((int64_t)(((int64_t)((int32_t)(((int32_t)((((int32_t)(((Array_t *)___array)->max_length)))-___index))+L_5))))) >= ((int64_t)(((uint64_t)L_6)))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t551 * L_7 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_7, (String_t*) &_stringLiteral483, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_006a:
	{
		LinkedListNode_1_t5043 * L_8 = (__this->___first_3);
		V_0 = L_8;
		LinkedListNode_1_t5043 * L_9 = (__this->___first_3);
		if (L_9)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		NullCheck(V_0);
		Object_t * L_10 = (( Object_t * (*) (LinkedListNode_1_t5043 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(V_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck(___array);
		IL2CPP_ARRAY_BOUNDS_CHECK(___array, ___index);
		*((Object_t **)(Object_t **)SZArrayLdElema(___array, ___index)) = (Object_t *)L_10;
		___index = ((int32_t)(___index+1));
		NullCheck(V_0);
		LinkedListNode_1_t5043 * L_11 = (V_0->___forward_2);
		V_0 = L_11;
		LinkedListNode_1_t5043 * L_12 = (__this->___first_3);
		if ((((LinkedListNode_1_t5043 *)V_0) != ((LinkedListNode_1_t5043 *)L_12)))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
 LinkedListNode_1_t5043 * LinkedList_1_Find_m30538_gshared (LinkedList_1_t5044 * __this, Object_t * ___value, MethodInfo* method)
{
	LinkedListNode_1_t5043 * V_0 = {0};
	{
		LinkedListNode_1_t5043 * L_0 = (__this->___first_3);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t5043 *)NULL;
	}

IL_000f:
	{
		Object_t * L_1 = ___value;
		if (((Object_t *)L_1))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck(V_0);
		Object_t * L_2 = (( Object_t * (*) (LinkedListNode_1_t5043 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(V_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Object_t * L_3 = L_2;
		if (!((Object_t *)L_3))
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Object_t * L_4 = ___value;
		if (!((Object_t *)L_4))
		{
			goto IL_0054;
		}
	}
	{
		NullCheck(V_0);
		Object_t * L_5 = (( Object_t * (*) (LinkedListNode_1_t5043 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(V_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Object_t * L_6 = L_5;
		NullCheck((*(&___value)));
		bool L_7 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, (*(&___value)), ((Object_t *)L_6));
		if (!L_7)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		return V_0;
	}

IL_0054:
	{
		NullCheck(V_0);
		LinkedListNode_1_t5043 * L_8 = (V_0->___forward_2);
		V_0 = L_8;
		LinkedListNode_1_t5043 * L_9 = (__this->___first_3);
		if ((((LinkedListNode_1_t5043 *)V_0) != ((LinkedListNode_1_t5043 *)L_9)))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t5043 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
 Enumerator_t5045  LinkedList_1_GetEnumerator_m30539 (LinkedList_1_t5044 * __this, MethodInfo* method){
	{
		Enumerator_t5045  L_0 = {0};
		Enumerator__ctor_m30552(&L_0, __this, /*hidden argument*/&Enumerator__ctor_m30552_MethodInfo);
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern MethodInfo LinkedList_1_GetObjectData_m30540_MethodInfo;
 void LinkedList_1_GetObjectData_m30540_gshared (LinkedList_1_t5044 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method)
{
	ObjectU5BU5D_t130* V_0 = {0};
	{
		uint32_t L_0 = (__this->___count_0);
		V_0 = ((ObjectU5BU5D_t130*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (((uintptr_t)L_0))));
		VirtActionInvoker2< ObjectU5BU5D_t130*, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), __this, V_0, 0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(___info);
		SerializationInfo_AddValue_m7843(___info, (String_t*) &_stringLiteral484, (Object_t *)(Object_t *)V_0, L_1, /*hidden argument*/&SerializationInfo_AddValue_m7843_MethodInfo);
		uint32_t L_2 = (__this->___version_1);
		NullCheck(___info);
		SerializationInfo_AddValue_m12001(___info, (String_t*) &_stringLiteral485, L_2, /*hidden argument*/&SerializationInfo_AddValue_m12001_MethodInfo);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern MethodInfo LinkedList_1_OnDeserialization_m30541_MethodInfo;
 void LinkedList_1_OnDeserialization_m30541_gshared (LinkedList_1_t5044 * __this, Object_t * ___sender, MethodInfo* method)
{
	ObjectU5BU5D_t130* V_0 = {0};
	Object_t * V_1 = {0};
	ObjectU5BU5D_t130* V_2 = {0};
	int32_t V_3 = 0;
	{
		SerializationInfo_t1118 * L_0 = (__this->___si_4);
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t1118 * L_1 = (__this->___si_4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		NullCheck(L_1);
		Object_t * L_3 = SerializationInfo_GetValue_m7846(L_1, (String_t*) &_stringLiteral484, L_2, /*hidden argument*/&SerializationInfo_GetValue_m7846_MethodInfo);
		V_0 = ((ObjectU5BU5D_t130*)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		if (!V_0)
		{
			goto IL_0057;
		}
	}
	{
		V_2 = V_0;
		V_3 = 0;
		goto IL_004e;
	}

IL_003a:
	{
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, V_3);
		int32_t L_4 = V_3;
		V_1 = (*(Object_t **)(Object_t **)SZArrayLdElema(V_2, L_4));
		(( LinkedListNode_1_t5043 * (*) (LinkedList_1_t5044 * __this, Object_t * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(__this, V_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_3 = ((int32_t)(V_3+1));
	}

IL_004e:
	{
		NullCheck(V_2);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((Array_t *)V_2)->max_length))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t1118 * L_5 = (__this->___si_4);
		NullCheck(L_5);
		uint32_t L_6 = SerializationInfo_GetUInt32_m12004(L_5, (String_t*) &_stringLiteral485, /*hidden argument*/&SerializationInfo_GetUInt32_m12004_MethodInfo);
		__this->___version_1 = L_6;
		__this->___si_4 = (SerializationInfo_t1118 *)NULL;
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern MethodInfo LinkedList_1_Remove_m30542_MethodInfo;
 bool LinkedList_1_Remove_m30542_gshared (LinkedList_1_t5044 * __this, Object_t * ___value, MethodInfo* method)
{
	LinkedListNode_1_t5043 * V_0 = {0};
	{
		LinkedListNode_1_t5043 * L_0 = (( LinkedListNode_1_t5043 * (*) (LinkedList_1_t5044 * __this, Object_t * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(__this, ___value, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		V_0 = L_0;
		if (V_0)
		{
			goto IL_0010;
		}
	}
	{
		return 0;
	}

IL_0010:
	{
		(( void (*) (LinkedList_1_t5044 * __this, LinkedListNode_1_t5043 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(__this, V_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		return 1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedList_1_Remove_m30543_gshared (LinkedList_1_t5044 * __this, LinkedListNode_1_t5043 * ___node, MethodInfo* method)
{
	{
		(( void (*) (LinkedList_1_t5044 * __this, LinkedListNode_1_t5043 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)(__this, ___node, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		uint32_t L_0 = (__this->___count_0);
		__this->___count_0 = ((uint32_t)(L_0-1));
		uint32_t L_1 = (__this->___count_0);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		__this->___first_3 = (LinkedListNode_1_t5043 *)NULL;
	}

IL_0027:
	{
		LinkedListNode_1_t5043 * L_2 = (__this->___first_3);
		if ((((LinkedListNode_1_t5043 *)___node) != ((LinkedListNode_1_t5043 *)L_2)))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t5043 * L_3 = (__this->___first_3);
		NullCheck(L_3);
		LinkedListNode_1_t5043 * L_4 = (L_3->___forward_2);
		__this->___first_3 = L_4;
	}

IL_0044:
	{
		uint32_t L_5 = (__this->___version_1);
		__this->___version_1 = ((int32_t)(L_5+1));
		NullCheck(___node);
		(( void (*) (LinkedListNode_1_t5043 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(___node, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern MethodInfo LinkedList_1_get_Count_m30544_MethodInfo;
 int32_t LinkedList_1_get_Count_m30544_gshared (LinkedList_1_t5044 * __this, MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___count_0);
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern MethodInfo LinkedList_1_get_First_m30545_MethodInfo;
 LinkedListNode_1_t5043 * LinkedList_1_get_First_m30545_gshared (LinkedList_1_t5044 * __this, MethodInfo* method)
{
	{
		LinkedListNode_1_t5043 * L_0 = (__this->___first_3);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.LinkedList`1<System.Object>
extern Il2CppType UInt32_t1166_0_0_1;
FieldInfo LinkedList_1_t5044____count_0_FieldInfo = 
{
	"count"/* name */
	, &UInt32_t1166_0_0_1/* type */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedList_1_t5044, ___count_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UInt32_t1166_0_0_1;
FieldInfo LinkedList_1_t5044____version_1_FieldInfo = 
{
	"version"/* name */
	, &UInt32_t1166_0_0_1/* type */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedList_1_t5044, ___version_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_1;
FieldInfo LinkedList_1_t5044____syncRoot_2_FieldInfo = 
{
	"syncRoot"/* name */
	, &Object_t_0_0_1/* type */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedList_1_t5044, ___syncRoot_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkedListNode_1_t5043_0_0_3;
FieldInfo LinkedList_1_t5044____first_3_FieldInfo = 
{
	"first"/* name */
	, &LinkedListNode_1_t5043_0_0_3/* type */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedList_1_t5044, ___first_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType SerializationInfo_t1118_0_0_3;
FieldInfo LinkedList_1_t5044____si_4_FieldInfo = 
{
	"si"/* name */
	, &SerializationInfo_t1118_0_0_3/* type */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedList_1_t5044, ___si_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* LinkedList_1_t5044_FieldInfos[] =
{
	&LinkedList_1_t5044____count_0_FieldInfo,
	&LinkedList_1_t5044____version_1_FieldInfo,
	&LinkedList_1_t5044____syncRoot_2_FieldInfo,
	&LinkedList_1_t5044____first_3_FieldInfo,
	&LinkedList_1_t5044____si_4_FieldInfo,
	NULL
};
static PropertyInfo LinkedList_1_t5044____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&LinkedList_1_t5044_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedList_1_t5044____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&LinkedList_1_t5044_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.IsSynchronized"/* name */
	, &LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedList_1_t5044____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&LinkedList_1_t5044_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.SyncRoot"/* name */
	, &LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedList_1_t5044____Count_PropertyInfo = 
{
	&LinkedList_1_t5044_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &LinkedList_1_get_Count_m30544_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedList_1_t5044____First_PropertyInfo = 
{
	&LinkedList_1_t5044_il2cpp_TypeInfo/* parent */
	, "First"/* name */
	, &LinkedList_1_get_First_m30545_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* LinkedList_1_t5044_PropertyInfos[] =
{
	&LinkedList_1_t5044____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&LinkedList_1_t5044____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&LinkedList_1_t5044____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&LinkedList_1_t5044____Count_PropertyInfo,
	&LinkedList_1_t5044____First_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1__ctor_m30524_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
MethodInfo LinkedList_1__ctor_m30524_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkedList_1__ctor_m30524_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1__ctor_m30524_GenericMethod/* genericMethod */

};
extern Il2CppType SerializationInfo_t1118_0_0_0;
extern Il2CppType SerializationInfo_t1118_0_0_0;
extern Il2CppType StreamingContext_t1119_0_0_0;
extern Il2CppType StreamingContext_t1119_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1__ctor_m30525_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &SerializationInfo_t1118_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &StreamingContext_t1119_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_StreamingContext_t1119 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1__ctor_m30525_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo LinkedList_1__ctor_m30525_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkedList_1__ctor_m30525_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_StreamingContext_t1119/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1__ctor_m30525_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1__ctor_m30525_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
MethodInfo LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.Add"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_GenericMethod/* genericMethod */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_System_Collections_ICollection_CopyTo_m30527_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_ICollection_CopyTo_m30527_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
MethodInfo LinkedList_1_System_Collections_ICollection_CopyTo_m30527_MethodInfo = 
{
	"System.Collections.ICollection.CopyTo"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_ICollection_CopyTo_m30527_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_System_Collections_ICollection_CopyTo_m30527_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_ICollection_CopyTo_m30527_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_1_t499_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
MethodInfo LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t499_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_t318_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529_GenericMethod;
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
MethodInfo LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t318_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_GenericMethod;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
MethodInfo LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_GenericMethod;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
MethodInfo LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_MethodInfo = 
{
	"System.Collections.ICollection.get_IsSynchronized"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_GenericMethod;
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
MethodInfo LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_MethodInfo = 
{
	"System.Collections.ICollection.get_SyncRoot"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedListNode_1_t5043_0_0_0;
extern Il2CppType LinkedListNode_1_t5043_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_VerifyReferencedNode_m30533_ParameterInfos[] = 
{
	{"node", 0, 134217728, &EmptyCustomAttributesCache, &LinkedListNode_1_t5043_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_VerifyReferencedNode_m30533_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
MethodInfo LinkedList_1_VerifyReferencedNode_m30533_MethodInfo = 
{
	"VerifyReferencedNode"/* name */
	, (methodPointerType)&LinkedList_1_VerifyReferencedNode_m30533_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_VerifyReferencedNode_m30533_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_VerifyReferencedNode_m30533_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_AddLast_m30534_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType LinkedListNode_1_t5043_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_AddLast_m30534_GenericMethod;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
MethodInfo LinkedList_1_AddLast_m30534_MethodInfo = 
{
	"AddLast"/* name */
	, (methodPointerType)&LinkedList_1_AddLast_m30534_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &LinkedListNode_1_t5043_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_AddLast_m30534_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_AddLast_m30534_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_Clear_m30535_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
MethodInfo LinkedList_1_Clear_m30535_MethodInfo = 
{
	"Clear"/* name */
	, (methodPointerType)&LinkedList_1_Clear_m30535_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_Clear_m30535_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_Contains_m30536_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_Contains_m30536_GenericMethod;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
MethodInfo LinkedList_1_Contains_m30536_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&LinkedList_1_Contains_m30536_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_Contains_m30536_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_Contains_m30536_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_CopyTo_m30537_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_CopyTo_m30537_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
MethodInfo LinkedList_1_CopyTo_m30537_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&LinkedList_1_CopyTo_m30537_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_CopyTo_m30537_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_CopyTo_m30537_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_Find_m30538_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType LinkedListNode_1_t5043_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_Find_m30538_GenericMethod;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
MethodInfo LinkedList_1_Find_m30538_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&LinkedList_1_Find_m30538_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &LinkedListNode_1_t5043_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_Find_m30538_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_Find_m30538_GenericMethod/* genericMethod */

};
extern Il2CppType Enumerator_t5045_0_0_0;
extern void* RuntimeInvoker_Enumerator_t5045 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_GetEnumerator_m30539_GenericMethod;
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
MethodInfo LinkedList_1_GetEnumerator_m30539_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&LinkedList_1_GetEnumerator_m30539/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t5045_0_0_0/* return_type */
	, RuntimeInvoker_Enumerator_t5045/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_GetEnumerator_m30539_GenericMethod/* genericMethod */

};
extern Il2CppType SerializationInfo_t1118_0_0_0;
extern Il2CppType StreamingContext_t1119_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_GetObjectData_m30540_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &SerializationInfo_t1118_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &StreamingContext_t1119_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_StreamingContext_t1119 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_GetObjectData_m30540_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo LinkedList_1_GetObjectData_m30540_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&LinkedList_1_GetObjectData_m30540_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_StreamingContext_t1119/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_GetObjectData_m30540_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_GetObjectData_m30540_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_OnDeserialization_m30541_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_OnDeserialization_m30541_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
MethodInfo LinkedList_1_OnDeserialization_m30541_MethodInfo = 
{
	"OnDeserialization"/* name */
	, (methodPointerType)&LinkedList_1_OnDeserialization_m30541_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_OnDeserialization_m30541_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_OnDeserialization_m30541_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_Remove_m30542_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_Remove_m30542_GenericMethod;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
MethodInfo LinkedList_1_Remove_m30542_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&LinkedList_1_Remove_m30542_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_Remove_m30542_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_Remove_m30542_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedListNode_1_t5043_0_0_0;
static ParameterInfo LinkedList_1_t5044_LinkedList_1_Remove_m30543_ParameterInfos[] = 
{
	{"node", 0, 134217728, &EmptyCustomAttributesCache, &LinkedListNode_1_t5043_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_Remove_m30543_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
MethodInfo LinkedList_1_Remove_m30543_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&LinkedList_1_Remove_m30543_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, LinkedList_1_t5044_LinkedList_1_Remove_m30543_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_Remove_m30543_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_get_Count_m30544_GenericMethod;
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
MethodInfo LinkedList_1_get_Count_m30544_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&LinkedList_1_get_Count_m30544_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_get_Count_m30544_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedListNode_1_t5043_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_get_First_m30545_GenericMethod;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
MethodInfo LinkedList_1_get_First_m30545_MethodInfo = 
{
	"get_First"/* name */
	, (methodPointerType)&LinkedList_1_get_First_m30545_gshared/* method */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* declaring_type */
	, &LinkedListNode_1_t5043_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_get_First_m30545_GenericMethod/* genericMethod */

};
static MethodInfo* LinkedList_1_t5044_MethodInfos[] =
{
	&LinkedList_1__ctor_m30524_MethodInfo,
	&LinkedList_1__ctor_m30525_MethodInfo,
	&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_CopyTo_m30527_MethodInfo,
	&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528_MethodInfo,
	&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529_MethodInfo,
	&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_MethodInfo,
	&LinkedList_1_VerifyReferencedNode_m30533_MethodInfo,
	&LinkedList_1_AddLast_m30534_MethodInfo,
	&LinkedList_1_Clear_m30535_MethodInfo,
	&LinkedList_1_Contains_m30536_MethodInfo,
	&LinkedList_1_CopyTo_m30537_MethodInfo,
	&LinkedList_1_Find_m30538_MethodInfo,
	&LinkedList_1_GetEnumerator_m30539_MethodInfo,
	&LinkedList_1_GetObjectData_m30540_MethodInfo,
	&LinkedList_1_OnDeserialization_m30541_MethodInfo,
	&LinkedList_1_Remove_m30542_MethodInfo,
	&LinkedList_1_Remove_m30543_MethodInfo,
	&LinkedList_1_get_Count_m30544_MethodInfo,
	&LinkedList_1_get_First_m30545_MethodInfo,
	NULL
};
static MethodInfo* LinkedList_1_t5044_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30528_MethodInfo,
	&LinkedList_1_get_Count_m30544_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m30531_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m30532_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_CopyTo_m30527_MethodInfo,
	&LinkedList_1_OnDeserialization_m30541_MethodInfo,
	&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m30529_MethodInfo,
	&LinkedList_1_get_Count_m30544_MethodInfo,
	&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30530_MethodInfo,
	&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m30526_MethodInfo,
	&LinkedList_1_Clear_m30535_MethodInfo,
	&LinkedList_1_Contains_m30536_MethodInfo,
	&LinkedList_1_CopyTo_m30537_MethodInfo,
	&LinkedList_1_Remove_m30542_MethodInfo,
	&LinkedList_1_GetObjectData_m30540_MethodInfo,
	&LinkedList_1_GetObjectData_m30540_MethodInfo,
	&LinkedList_1_OnDeserialization_m30541_MethodInfo,
};
extern TypeInfo IEnumerable_1_t2834_il2cpp_TypeInfo;
extern TypeInfo ICollection_t1259_il2cpp_TypeInfo;
extern TypeInfo IDeserializationCallback_t1550_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t2835_il2cpp_TypeInfo;
static TypeInfo* LinkedList_1_t5044_InterfacesTypeInfos[] = 
{
	&IEnumerable_1_t2834_il2cpp_TypeInfo,
	&ICollection_t1259_il2cpp_TypeInfo,
	&IDeserializationCallback_t1550_il2cpp_TypeInfo,
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t2835_il2cpp_TypeInfo,
	&ISerializable_t526_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair LinkedList_1_t5044_InterfacesOffsets[] = 
{
	{ &IEnumerable_1_t2834_il2cpp_TypeInfo, 4},
	{ &ICollection_t1259_il2cpp_TypeInfo, 5},
	{ &IDeserializationCallback_t1550_il2cpp_TypeInfo, 9},
	{ &IEnumerable_t1179_il2cpp_TypeInfo, 10},
	{ &ICollection_1_t2835_il2cpp_TypeInfo, 11},
	{ &ISerializable_t526_il2cpp_TypeInfo, 18},
};
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t5045_il2cpp_TypeInfo;
extern TypeInfo LinkedListNode_1_t5043_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
static Il2CppRGCTXData LinkedList_1_t5044_RGCTXData[19] = 
{
	&LinkedList_1__ctor_m30524_MethodInfo/* Method Usage */,
	&LinkedList_1_AddLast_m30534_MethodInfo/* Method Usage */,
	&ObjectU5BU5D_t130_il2cpp_TypeInfo/* Class Usage */,
	&LinkedList_1_CopyTo_m30537_MethodInfo/* Method Usage */,
	&LinkedList_1_GetEnumerator_m30539_MethodInfo/* Method Usage */,
	&Enumerator_t5045_il2cpp_TypeInfo/* Class Usage */,
	&LinkedListNode_1_get_List_m30549_MethodInfo/* Method Usage */,
	&LinkedListNode_1_t5043_il2cpp_TypeInfo/* Class Usage */,
	&LinkedListNode_1__ctor_m30546_MethodInfo/* Method Usage */,
	&LinkedListNode_1__ctor_m30547_MethodInfo/* Method Usage */,
	&LinkedListNode_1_get_Value_m30551_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&Enumerator__ctor_m30552_MethodInfo/* Method Usage */,
	&ObjectU5BU5D_t130_il2cpp_TypeInfo/* Array Usage */,
	&ObjectU5BU5D_t130_0_0_0/* Type Usage */,
	&LinkedList_1_Find_m30538_MethodInfo/* Method Usage */,
	&LinkedList_1_Remove_m30543_MethodInfo/* Method Usage */,
	&LinkedList_1_VerifyReferencedNode_m30533_MethodInfo/* Method Usage */,
	&LinkedListNode_1_Detach_m30548_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType LinkedList_1_t5044_0_0_0;
extern Il2CppType LinkedList_1_t5044_1_0_0;
struct LinkedList_1_t5044;
extern Il2CppGenericClass LinkedList_1_t5044_GenericClass;
extern CustomAttributesCache LinkedList_1_t1344__CustomAttributeCache;
TypeInfo LinkedList_1_t5044_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkedList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, LinkedList_1_t5044_MethodInfos/* methods */
	, LinkedList_1_t5044_PropertyInfos/* properties */
	, LinkedList_1_t5044_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* element_class */
	, LinkedList_1_t5044_InterfacesTypeInfos/* implemented_interfaces */
	, LinkedList_1_t5044_VTable/* vtable */
	, &LinkedList_1_t1344__CustomAttributeCache/* custom_attributes_cache */
	, &LinkedList_1_t5044_il2cpp_TypeInfo/* cast_class */
	, &LinkedList_1_t5044_0_0_0/* byval_arg */
	, &LinkedList_1_t5044_1_0_0/* this_arg */
	, LinkedList_1_t5044_InterfacesOffsets/* interface_offsets */
	, &LinkedList_1_t5044_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, LinkedList_1_t5044_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkedList_1_t5044)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 22/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 21/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
 void LinkedListNode_1__ctor_m30546_gshared (LinkedListNode_1_t5043 * __this, LinkedList_1_t5044 * ___list, Object_t * ___value, MethodInfo* method)
{
	LinkedListNode_1_t5043 * V_0 = {0};
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		__this->___container_1 = ___list;
		__this->___item_0 = ___value;
		V_0 = __this;
		__this->___forward_2 = __this;
		__this->___back_3 = V_0;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedListNode_1__ctor_m30547_gshared (LinkedListNode_1_t5043 * __this, LinkedList_1_t5044 * ___list, Object_t * ___value, LinkedListNode_1_t5043 * ___previousNode, LinkedListNode_1_t5043 * ___nextNode, MethodInfo* method)
{
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		__this->___container_1 = ___list;
		__this->___item_0 = ___value;
		__this->___back_3 = ___previousNode;
		__this->___forward_2 = ___nextNode;
		NullCheck(___previousNode);
		___previousNode->___forward_2 = __this;
		NullCheck(___nextNode);
		___nextNode->___back_3 = __this;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
 void LinkedListNode_1_Detach_m30548_gshared (LinkedListNode_1_t5043 * __this, MethodInfo* method)
{
	LinkedListNode_1_t5043 * V_0 = {0};
	{
		LinkedListNode_1_t5043 * L_0 = (__this->___back_3);
		LinkedListNode_1_t5043 * L_1 = (__this->___forward_2);
		NullCheck(L_0);
		L_0->___forward_2 = L_1;
		LinkedListNode_1_t5043 * L_2 = (__this->___forward_2);
		LinkedListNode_1_t5043 * L_3 = (__this->___back_3);
		NullCheck(L_2);
		L_2->___back_3 = L_3;
		V_0 = (LinkedListNode_1_t5043 *)NULL;
		__this->___back_3 = (LinkedListNode_1_t5043 *)NULL;
		__this->___forward_2 = V_0;
		__this->___container_1 = (LinkedList_1_t5044 *)NULL;
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
 LinkedList_1_t5044 * LinkedListNode_1_get_List_m30549_gshared (LinkedListNode_1_t5043 * __this, MethodInfo* method)
{
	{
		LinkedList_1_t5044 * L_0 = (__this->___container_1);
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern MethodInfo LinkedListNode_1_get_Next_m30550_MethodInfo;
 LinkedListNode_1_t5043 * LinkedListNode_1_get_Next_m30550_gshared (LinkedListNode_1_t5043 * __this, MethodInfo* method)
{
	LinkedListNode_1_t5043 * G_B4_0 = {0};
	{
		LinkedList_1_t5044 * L_0 = (__this->___container_1);
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t5043 * L_1 = (__this->___forward_2);
		LinkedList_1_t5044 * L_2 = (__this->___container_1);
		NullCheck(L_2);
		LinkedListNode_1_t5043 * L_3 = (L_2->___first_3);
		if ((((LinkedListNode_1_t5043 *)L_1) == ((LinkedListNode_1_t5043 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t5043 * L_4 = (__this->___forward_2);
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t5043 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
 Object_t * LinkedListNode_1_get_Value_m30551_gshared (LinkedListNode_1_t5043 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___item_0);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.LinkedListNode`1<System.Object>
extern Il2CppType Object_t_0_0_1;
FieldInfo LinkedListNode_1_t5043____item_0_FieldInfo = 
{
	"item"/* name */
	, &Object_t_0_0_1/* type */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedListNode_1_t5043, ___item_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkedList_1_t5044_0_0_1;
FieldInfo LinkedListNode_1_t5043____container_1_FieldInfo = 
{
	"container"/* name */
	, &LinkedList_1_t5044_0_0_1/* type */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedListNode_1_t5043, ___container_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkedListNode_1_t5043_0_0_3;
FieldInfo LinkedListNode_1_t5043____forward_2_FieldInfo = 
{
	"forward"/* name */
	, &LinkedListNode_1_t5043_0_0_3/* type */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedListNode_1_t5043, ___forward_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkedListNode_1_t5043_0_0_3;
FieldInfo LinkedListNode_1_t5043____back_3_FieldInfo = 
{
	"back"/* name */
	, &LinkedListNode_1_t5043_0_0_3/* type */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedListNode_1_t5043, ___back_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* LinkedListNode_1_t5043_FieldInfos[] =
{
	&LinkedListNode_1_t5043____item_0_FieldInfo,
	&LinkedListNode_1_t5043____container_1_FieldInfo,
	&LinkedListNode_1_t5043____forward_2_FieldInfo,
	&LinkedListNode_1_t5043____back_3_FieldInfo,
	NULL
};
static PropertyInfo LinkedListNode_1_t5043____List_PropertyInfo = 
{
	&LinkedListNode_1_t5043_il2cpp_TypeInfo/* parent */
	, "List"/* name */
	, &LinkedListNode_1_get_List_m30549_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedListNode_1_t5043____Next_PropertyInfo = 
{
	&LinkedListNode_1_t5043_il2cpp_TypeInfo/* parent */
	, "Next"/* name */
	, &LinkedListNode_1_get_Next_m30550_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedListNode_1_t5043____Value_PropertyInfo = 
{
	&LinkedListNode_1_t5043_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &LinkedListNode_1_get_Value_m30551_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* LinkedListNode_1_t5043_PropertyInfos[] =
{
	&LinkedListNode_1_t5043____List_PropertyInfo,
	&LinkedListNode_1_t5043____Next_PropertyInfo,
	&LinkedListNode_1_t5043____Value_PropertyInfo,
	NULL
};
extern Il2CppType LinkedList_1_t5044_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedListNode_1_t5043_LinkedListNode_1__ctor_m30546_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &LinkedList_1_t5044_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1__ctor_m30546_GenericMethod;
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
MethodInfo LinkedListNode_1__ctor_m30546_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkedListNode_1__ctor_m30546_gshared/* method */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, LinkedListNode_1_t5043_LinkedListNode_1__ctor_m30546_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1__ctor_m30546_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedList_1_t5044_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType LinkedListNode_1_t5043_0_0_0;
extern Il2CppType LinkedListNode_1_t5043_0_0_0;
static ParameterInfo LinkedListNode_1_t5043_LinkedListNode_1__ctor_m30547_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &LinkedList_1_t5044_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"previousNode", 2, 134217728, &EmptyCustomAttributesCache, &LinkedListNode_1_t5043_0_0_0},
	{"nextNode", 3, 134217728, &EmptyCustomAttributesCache, &LinkedListNode_1_t5043_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1__ctor_m30547_GenericMethod;
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
MethodInfo LinkedListNode_1__ctor_m30547_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkedListNode_1__ctor_m30547_gshared/* method */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, LinkedListNode_1_t5043_LinkedListNode_1__ctor_m30547_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1__ctor_m30547_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1_Detach_m30548_GenericMethod;
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
MethodInfo LinkedListNode_1_Detach_m30548_MethodInfo = 
{
	"Detach"/* name */
	, (methodPointerType)&LinkedListNode_1_Detach_m30548_gshared/* method */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1_Detach_m30548_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedList_1_t5044_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1_get_List_m30549_GenericMethod;
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
MethodInfo LinkedListNode_1_get_List_m30549_MethodInfo = 
{
	"get_List"/* name */
	, (methodPointerType)&LinkedListNode_1_get_List_m30549_gshared/* method */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* declaring_type */
	, &LinkedList_1_t5044_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1_get_List_m30549_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedListNode_1_t5043_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1_get_Next_m30550_GenericMethod;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
MethodInfo LinkedListNode_1_get_Next_m30550_MethodInfo = 
{
	"get_Next"/* name */
	, (methodPointerType)&LinkedListNode_1_get_Next_m30550_gshared/* method */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* declaring_type */
	, &LinkedListNode_1_t5043_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1_get_Next_m30550_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1_get_Value_m30551_GenericMethod;
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
MethodInfo LinkedListNode_1_get_Value_m30551_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&LinkedListNode_1_get_Value_m30551_gshared/* method */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1_get_Value_m30551_GenericMethod/* genericMethod */

};
static MethodInfo* LinkedListNode_1_t5043_MethodInfos[] =
{
	&LinkedListNode_1__ctor_m30546_MethodInfo,
	&LinkedListNode_1__ctor_m30547_MethodInfo,
	&LinkedListNode_1_Detach_m30548_MethodInfo,
	&LinkedListNode_1_get_List_m30549_MethodInfo,
	&LinkedListNode_1_get_Next_m30550_MethodInfo,
	&LinkedListNode_1_get_Value_m30551_MethodInfo,
	NULL
};
static MethodInfo* LinkedListNode_1_t5043_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType LinkedListNode_1_t5043_1_0_0;
struct LinkedListNode_1_t5043;
extern Il2CppGenericClass LinkedListNode_1_t5043_GenericClass;
extern CustomAttributesCache LinkedListNode_1_t1345__CustomAttributeCache;
TypeInfo LinkedListNode_1_t5043_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkedListNode`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, LinkedListNode_1_t5043_MethodInfos/* methods */
	, LinkedListNode_1_t5043_PropertyInfos/* properties */
	, LinkedListNode_1_t5043_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, LinkedListNode_1_t5043_VTable/* vtable */
	, &LinkedListNode_1_t1345__CustomAttributeCache/* custom_attributes_cache */
	, &LinkedListNode_1_t5043_il2cpp_TypeInfo/* cast_class */
	, &LinkedListNode_1_t5043_0_0_0/* byval_arg */
	, &LinkedListNode_1_t5043_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &LinkedListNode_1_t5043_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkedListNode_1_t5043)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
extern TypeInfo ObjectDisposedException_t1718_il2cpp_TypeInfo;
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
extern MethodInfo Enumerator_get_Current_m30554_MethodInfo;
extern MethodInfo ObjectDisposedException__ctor_m9005_MethodInfo;


// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
 void Enumerator__ctor_m30552_gshared (Enumerator_t5045 * __this, LinkedList_1_t5044 * ___parent, MethodInfo* method)
{
	{
		__this->___list_0 = ___parent;
		__this->___current_1 = (LinkedListNode_1_t5043 *)NULL;
		__this->___index_2 = (-1);
		NullCheck(___parent);
		uint32_t L_0 = (___parent->___version_1);
		__this->___version_3 = L_0;
		return;
	}
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m30553_MethodInfo;
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m30553_gshared (Enumerator_t5045 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (Enumerator_t5045 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_1 = L_0;
		return ((Object_t *)L_1);
	}
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
 Object_t * Enumerator_get_Current_m30554_gshared (Enumerator_t5045 * __this, MethodInfo* method)
{
	{
		LinkedList_1_t5044 * L_0 = (__this->___list_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1718 * L_1 = (ObjectDisposedException_t1718 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ObjectDisposedException_t1718_il2cpp_TypeInfo));
		ObjectDisposedException__ctor_m9005(L_1, (String_t*)NULL, /*hidden argument*/&ObjectDisposedException__ctor_m9005_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t5043 * L_2 = (__this->___current_1);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7835(L_3, /*hidden argument*/&InvalidOperationException__ctor_m7835_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t5043 * L_4 = (__this->___current_1);
		NullCheck(L_4);
		Object_t * L_5 = (( Object_t * (*) (LinkedListNode_1_t5043 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern MethodInfo Enumerator_MoveNext_m30555_MethodInfo;
 bool Enumerator_MoveNext_m30555_gshared (Enumerator_t5045 * __this, MethodInfo* method)
{
	{
		LinkedList_1_t5044 * L_0 = (__this->___list_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1718 * L_1 = (ObjectDisposedException_t1718 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ObjectDisposedException_t1718_il2cpp_TypeInfo));
		ObjectDisposedException__ctor_m9005(L_1, (String_t*)NULL, /*hidden argument*/&ObjectDisposedException__ctor_m9005_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (__this->___version_3);
		LinkedList_1_t5044 * L_3 = (__this->___list_0);
		NullCheck(L_3);
		uint32_t L_4 = (L_3->___version_1);
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1546 * L_5 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_5, (String_t*) &_stringLiteral486, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t5043 * L_6 = (__this->___current_1);
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t5044 * L_7 = (__this->___list_0);
		NullCheck(L_7);
		LinkedListNode_1_t5043 * L_8 = (L_7->___first_3);
		__this->___current_1 = L_8;
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t5043 * L_9 = (__this->___current_1);
		NullCheck(L_9);
		LinkedListNode_1_t5043 * L_10 = (L_9->___forward_2);
		__this->___current_1 = L_10;
		LinkedListNode_1_t5043 * L_11 = (__this->___current_1);
		LinkedList_1_t5044 * L_12 = (__this->___list_0);
		NullCheck(L_12);
		LinkedListNode_1_t5043 * L_13 = (L_12->___first_3);
		if ((((LinkedListNode_1_t5043 *)L_11) != ((LinkedListNode_1_t5043 *)L_13)))
		{
			goto IL_0082;
		}
	}
	{
		__this->___current_1 = (LinkedListNode_1_t5043 *)NULL;
	}

IL_0082:
	{
		LinkedListNode_1_t5043 * L_14 = (__this->___current_1);
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->___index_2 = (-1);
		return 0;
	}

IL_0096:
	{
		int32_t L_15 = (__this->___index_2);
		__this->___index_2 = ((int32_t)(L_15+1));
		return 1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern MethodInfo Enumerator_Dispose_m30556_MethodInfo;
 void Enumerator_Dispose_m30556_gshared (Enumerator_t5045 * __this, MethodInfo* method)
{
	{
		LinkedList_1_t5044 * L_0 = (__this->___list_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1718 * L_1 = (ObjectDisposedException_t1718 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ObjectDisposedException_t1718_il2cpp_TypeInfo));
		ObjectDisposedException__ctor_m9005(L_1, (String_t*)NULL, /*hidden argument*/&ObjectDisposedException__ctor_m9005_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0012:
	{
		__this->___current_1 = (LinkedListNode_1_t5043 *)NULL;
		__this->___list_0 = (LinkedList_1_t5044 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
extern Il2CppType LinkedList_1_t5044_0_0_1;
FieldInfo Enumerator_t5045____list_0_FieldInfo = 
{
	"list"/* name */
	, &LinkedList_1_t5044_0_0_1/* type */
	, &Enumerator_t5045_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t5045, ___list_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkedListNode_1_t5043_0_0_1;
FieldInfo Enumerator_t5045____current_1_FieldInfo = 
{
	"current"/* name */
	, &LinkedListNode_1_t5043_0_0_1/* type */
	, &Enumerator_t5045_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t5045, ___current_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo Enumerator_t5045____index_2_FieldInfo = 
{
	"index"/* name */
	, &Int32_t123_0_0_1/* type */
	, &Enumerator_t5045_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t5045, ___index_2) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UInt32_t1166_0_0_1;
FieldInfo Enumerator_t5045____version_3_FieldInfo = 
{
	"version"/* name */
	, &UInt32_t1166_0_0_1/* type */
	, &Enumerator_t5045_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t5045, ___version_3) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Enumerator_t5045_FieldInfos[] =
{
	&Enumerator_t5045____list_0_FieldInfo,
	&Enumerator_t5045____current_1_FieldInfo,
	&Enumerator_t5045____index_2_FieldInfo,
	&Enumerator_t5045____version_3_FieldInfo,
	NULL
};
static PropertyInfo Enumerator_t5045____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&Enumerator_t5045_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m30553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Enumerator_t5045____Current_PropertyInfo = 
{
	&Enumerator_t5045_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m30554_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Enumerator_t5045_PropertyInfos[] =
{
	&Enumerator_t5045____System_Collections_IEnumerator_Current_PropertyInfo,
	&Enumerator_t5045____Current_PropertyInfo,
	NULL
};
extern Il2CppType LinkedList_1_t5044_0_0_0;
static ParameterInfo Enumerator_t5045_Enumerator__ctor_m30552_ParameterInfos[] = 
{
	{"parent", 0, 134217728, &EmptyCustomAttributesCache, &LinkedList_1_t5044_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator__ctor_m30552_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
MethodInfo Enumerator__ctor_m30552_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Enumerator__ctor_m30552_gshared/* method */
	, &Enumerator_t5045_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Enumerator_t5045_Enumerator__ctor_m30552_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator__ctor_m30552_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_System_Collections_IEnumerator_get_Current_m30553_GenericMethod;
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m30553_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m30553_gshared/* method */
	, &Enumerator_t5045_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m30553_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_get_Current_m30554_GenericMethod;
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
MethodInfo Enumerator_get_Current_m30554_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&Enumerator_get_Current_m30554_gshared/* method */
	, &Enumerator_t5045_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_get_Current_m30554_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_MoveNext_m30555_GenericMethod;
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
MethodInfo Enumerator_MoveNext_m30555_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&Enumerator_MoveNext_m30555_gshared/* method */
	, &Enumerator_t5045_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_MoveNext_m30555_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_Dispose_m30556_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
MethodInfo Enumerator_Dispose_m30556_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&Enumerator_Dispose_m30556_gshared/* method */
	, &Enumerator_t5045_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_Dispose_m30556_GenericMethod/* genericMethod */

};
static MethodInfo* Enumerator_t5045_MethodInfos[] =
{
	&Enumerator__ctor_m30552_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m30553_MethodInfo,
	&Enumerator_get_Current_m30554_MethodInfo,
	&Enumerator_MoveNext_m30555_MethodInfo,
	&Enumerator_Dispose_m30556_MethodInfo,
	NULL
};
static MethodInfo* Enumerator_t5045_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&Enumerator_get_Current_m30554_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m30553_MethodInfo,
	&Enumerator_MoveNext_m30555_MethodInfo,
	&Enumerator_Dispose_m30556_MethodInfo,
};
extern TypeInfo IEnumerator_1_t499_il2cpp_TypeInfo;
static TypeInfo* Enumerator_t5045_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t499_il2cpp_TypeInfo,
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Enumerator_t5045_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t499_il2cpp_TypeInfo, 4},
	{ &IEnumerator_t318_il2cpp_TypeInfo, 5},
	{ &IDisposable_t138_il2cpp_TypeInfo, 7},
};
extern TypeInfo Object_t_il2cpp_TypeInfo;
static Il2CppRGCTXData Enumerator_t5045_RGCTXData[3] = 
{
	&Enumerator_get_Current_m30554_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&LinkedListNode_1_get_Value_m30551_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Enumerator_t5045_0_0_0;
extern Il2CppType Enumerator_t5045_1_0_0;
extern Il2CppGenericClass Enumerator_t5045_GenericClass;
extern TypeInfo LinkedList_1_t1344_il2cpp_TypeInfo;
TypeInfo Enumerator_t5045_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t5045_MethodInfos/* methods */
	, Enumerator_t5045_PropertyInfos/* properties */
	, Enumerator_t5045_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &LinkedList_1_t1344_il2cpp_TypeInfo/* nested_in */
	, &Enumerator_t5045_il2cpp_TypeInfo/* element_class */
	, Enumerator_t5045_InterfacesTypeInfos/* implemented_interfaces */
	, Enumerator_t5045_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Enumerator_t5045_il2cpp_TypeInfo/* cast_class */
	, &Enumerator_t5045_0_0_0/* byval_arg */
	, &Enumerator_t5045_1_0_0/* this_arg */
	, Enumerator_t5045_InterfacesOffsets/* interface_offsets */
	, &Enumerator_t5045_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, Enumerator_t5045_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t5045)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7027_il2cpp_TypeInfo;

// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50178_MethodInfo;
static PropertyInfo IEnumerator_1_t7027____Current_PropertyInfo = 
{
	&IEnumerator_1_t7027_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50178_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7027_PropertyInfos[] =
{
	&IEnumerator_1_t7027____Current_PropertyInfo,
	NULL
};
extern Il2CppType EditorBrowsableAttribute_t1182_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50178_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50178_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7027_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableAttribute_t1182_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50178_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7027_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50178_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7027_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7027_0_0_0;
extern Il2CppType IEnumerator_1_t7027_1_0_0;
struct IEnumerator_1_t7027;
extern Il2CppGenericClass IEnumerator_1_t7027_GenericClass;
TypeInfo IEnumerator_1_t7027_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7027_MethodInfos/* methods */
	, IEnumerator_1_t7027_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7027_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7027_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7027_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7027_0_0_0/* byval_arg */
	, &IEnumerator_1_t7027_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7027_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_524.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5046_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_524MethodDeclarations.h"

extern TypeInfo EditorBrowsableAttribute_t1182_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30561_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEditorBrowsableAttribute_t1182_m39445_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.ComponentModel.EditorBrowsableAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.ComponentModel.EditorBrowsableAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisEditorBrowsableAttribute_t1182_m39445(__this, p0, method) (EditorBrowsableAttribute_t1182 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5046____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5046_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5046, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5046____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5046_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5046, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5046_FieldInfos[] =
{
	&InternalEnumerator_1_t5046____array_0_FieldInfo,
	&InternalEnumerator_1_t5046____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5046____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5046_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5046____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5046_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30561_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5046_PropertyInfos[] =
{
	&InternalEnumerator_1_t5046____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5046____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5046_InternalEnumerator_1__ctor_m30557_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30557_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30557_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5046_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5046_InternalEnumerator_1__ctor_m30557_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30557_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5046_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30559_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30559_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5046_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30559_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30560_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30560_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5046_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30560_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableAttribute_t1182_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30561_GenericMethod;
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30561_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5046_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableAttribute_t1182_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30561_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5046_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30557_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_MethodInfo,
	&InternalEnumerator_1_Dispose_m30559_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30560_MethodInfo,
	&InternalEnumerator_1_get_Current_m30561_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30560_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30559_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5046_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30560_MethodInfo,
	&InternalEnumerator_1_Dispose_m30559_MethodInfo,
	&InternalEnumerator_1_get_Current_m30561_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5046_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7027_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5046_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7027_il2cpp_TypeInfo, 7},
};
extern TypeInfo EditorBrowsableAttribute_t1182_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5046_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30561_MethodInfo/* Method Usage */,
	&EditorBrowsableAttribute_t1182_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisEditorBrowsableAttribute_t1182_m39445_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5046_0_0_0;
extern Il2CppType InternalEnumerator_1_t5046_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5046_GenericClass;
TypeInfo InternalEnumerator_1_t5046_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5046_MethodInfos/* methods */
	, InternalEnumerator_1_t5046_PropertyInfos/* properties */
	, InternalEnumerator_1_t5046_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5046_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5046_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5046_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5046_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5046_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5046_1_0_0/* this_arg */
	, InternalEnumerator_1_t5046_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5046_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5046_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5046)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8934_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>
extern MethodInfo ICollection_1_get_Count_m50179_MethodInfo;
static PropertyInfo ICollection_1_t8934____Count_PropertyInfo = 
{
	&ICollection_1_t8934_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50179_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50180_MethodInfo;
static PropertyInfo ICollection_1_t8934____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8934_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50180_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8934_PropertyInfos[] =
{
	&ICollection_1_t8934____Count_PropertyInfo,
	&ICollection_1_t8934____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50179_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50179_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8934_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50179_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50180_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50180_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50180_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableAttribute_t1182_0_0_0;
extern Il2CppType EditorBrowsableAttribute_t1182_0_0_0;
static ParameterInfo ICollection_1_t8934_ICollection_1_Add_m50181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1182_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50181_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50181_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8934_ICollection_1_Add_m50181_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50181_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50182_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50182_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50182_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableAttribute_t1182_0_0_0;
static ParameterInfo ICollection_1_t8934_ICollection_1_Contains_m50183_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1182_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50183_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50183_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8934_ICollection_1_Contains_m50183_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50183_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableAttributeU5BU5D_t5885_0_0_0;
extern Il2CppType EditorBrowsableAttributeU5BU5D_t5885_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8934_ICollection_1_CopyTo_m50184_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttributeU5BU5D_t5885_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50184_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50184_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8934_ICollection_1_CopyTo_m50184_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50184_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableAttribute_t1182_0_0_0;
static ParameterInfo ICollection_1_t8934_ICollection_1_Remove_m50185_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1182_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50185_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50185_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8934_ICollection_1_Remove_m50185_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50185_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8934_MethodInfos[] =
{
	&ICollection_1_get_Count_m50179_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50180_MethodInfo,
	&ICollection_1_Add_m50181_MethodInfo,
	&ICollection_1_Clear_m50182_MethodInfo,
	&ICollection_1_Contains_m50183_MethodInfo,
	&ICollection_1_CopyTo_m50184_MethodInfo,
	&ICollection_1_Remove_m50185_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8936_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8934_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8936_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8934_0_0_0;
extern Il2CppType ICollection_1_t8934_1_0_0;
struct ICollection_1_t8934;
extern Il2CppGenericClass ICollection_1_t8934_GenericClass;
TypeInfo ICollection_1_t8934_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8934_MethodInfos/* methods */
	, ICollection_1_t8934_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8934_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8934_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8934_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8934_0_0_0/* byval_arg */
	, &ICollection_1_t8934_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8934_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableAttribute>
extern Il2CppType IEnumerator_1_t7027_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50186_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50186_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8936_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7027_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50186_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8936_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50186_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8936_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8936_0_0_0;
extern Il2CppType IEnumerable_1_t8936_1_0_0;
struct IEnumerable_1_t8936;
extern Il2CppGenericClass IEnumerable_1_t8936_GenericClass;
TypeInfo IEnumerable_1_t8936_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8936_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8936_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8936_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8936_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8936_0_0_0/* byval_arg */
	, &IEnumerable_1_t8936_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8936_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8935_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>
extern MethodInfo IList_1_get_Item_m50187_MethodInfo;
extern MethodInfo IList_1_set_Item_m50188_MethodInfo;
static PropertyInfo IList_1_t8935____Item_PropertyInfo = 
{
	&IList_1_t8935_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50187_MethodInfo/* get */
	, &IList_1_set_Item_m50188_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8935_PropertyInfos[] =
{
	&IList_1_t8935____Item_PropertyInfo,
	NULL
};
extern Il2CppType EditorBrowsableAttribute_t1182_0_0_0;
static ParameterInfo IList_1_t8935_IList_1_IndexOf_m50189_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1182_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50189_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50189_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8935_IList_1_IndexOf_m50189_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50189_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EditorBrowsableAttribute_t1182_0_0_0;
static ParameterInfo IList_1_t8935_IList_1_Insert_m50190_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1182_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50190_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50190_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8935_IList_1_Insert_m50190_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50190_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8935_IList_1_RemoveAt_m50191_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50191_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50191_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8935_IList_1_RemoveAt_m50191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50191_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8935_IList_1_get_Item_m50187_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType EditorBrowsableAttribute_t1182_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50187_GenericMethod;
// T System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50187_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableAttribute_t1182_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8935_IList_1_get_Item_m50187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50187_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EditorBrowsableAttribute_t1182_0_0_0;
static ParameterInfo IList_1_t8935_IList_1_set_Item_m50188_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1182_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50188_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50188_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8935_IList_1_set_Item_m50188_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50188_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8935_MethodInfos[] =
{
	&IList_1_IndexOf_m50189_MethodInfo,
	&IList_1_Insert_m50190_MethodInfo,
	&IList_1_RemoveAt_m50191_MethodInfo,
	&IList_1_get_Item_m50187_MethodInfo,
	&IList_1_set_Item_m50188_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8935_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8934_il2cpp_TypeInfo,
	&IEnumerable_1_t8936_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8935_0_0_0;
extern Il2CppType IList_1_t8935_1_0_0;
struct IList_1_t8935;
extern Il2CppGenericClass IList_1_t8935_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8935_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8935_MethodInfos/* methods */
	, IList_1_t8935_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8935_il2cpp_TypeInfo/* element_class */
	, IList_1_t8935_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8935_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8935_0_0_0/* byval_arg */
	, &IList_1_t8935_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8935_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7029_il2cpp_TypeInfo;

// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"


// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableState>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableState>
extern MethodInfo IEnumerator_1_get_Current_m50192_MethodInfo;
static PropertyInfo IEnumerator_1_t7029____Current_PropertyInfo = 
{
	&IEnumerator_1_t7029_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50192_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7029_PropertyInfos[] =
{
	&IEnumerator_1_t7029____Current_PropertyInfo,
	NULL
};
extern Il2CppType EditorBrowsableState_t1365_0_0_0;
extern void* RuntimeInvoker_EditorBrowsableState_t1365 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50192_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableState>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50192_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7029_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableState_t1365_0_0_0/* return_type */
	, RuntimeInvoker_EditorBrowsableState_t1365/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50192_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7029_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50192_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7029_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7029_0_0_0;
extern Il2CppType IEnumerator_1_t7029_1_0_0;
struct IEnumerator_1_t7029;
extern Il2CppGenericClass IEnumerator_1_t7029_GenericClass;
TypeInfo IEnumerator_1_t7029_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7029_MethodInfos/* methods */
	, IEnumerator_1_t7029_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7029_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7029_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7029_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7029_0_0_0/* byval_arg */
	, &IEnumerator_1_t7029_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7029_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_525.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5047_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_525MethodDeclarations.h"

extern TypeInfo EditorBrowsableState_t1365_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30566_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEditorBrowsableState_t1365_m39456_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.ComponentModel.EditorBrowsableState>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.ComponentModel.EditorBrowsableState>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisEditorBrowsableState_t1365_m39456 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30562_MethodInfo;
 void InternalEnumerator_1__ctor_m30562 (InternalEnumerator_1_t5047 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563 (InternalEnumerator_1_t5047 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30566(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30566_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&EditorBrowsableState_t1365_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30564_MethodInfo;
 void InternalEnumerator_1_Dispose_m30564 (InternalEnumerator_1_t5047 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30565_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30565 (InternalEnumerator_1_t5047 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30566 (InternalEnumerator_1_t5047 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisEditorBrowsableState_t1365_m39456(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisEditorBrowsableState_t1365_m39456_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5047____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5047_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5047, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5047____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5047_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5047, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5047_FieldInfos[] =
{
	&InternalEnumerator_1_t5047____array_0_FieldInfo,
	&InternalEnumerator_1_t5047____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5047____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5047_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5047____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5047_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30566_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5047_PropertyInfos[] =
{
	&InternalEnumerator_1_t5047____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5047____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5047_InternalEnumerator_1__ctor_m30562_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30562_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30562_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30562/* method */
	, &InternalEnumerator_1_t5047_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5047_InternalEnumerator_1__ctor_m30562_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30562_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563/* method */
	, &InternalEnumerator_1_t5047_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30564_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30564_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30564/* method */
	, &InternalEnumerator_1_t5047_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30564_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30565_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30565_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30565/* method */
	, &InternalEnumerator_1_t5047_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30565_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableState_t1365_0_0_0;
extern void* RuntimeInvoker_EditorBrowsableState_t1365 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30566_GenericMethod;
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30566_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30566/* method */
	, &InternalEnumerator_1_t5047_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableState_t1365_0_0_0/* return_type */
	, RuntimeInvoker_EditorBrowsableState_t1365/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30566_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5047_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30562_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_MethodInfo,
	&InternalEnumerator_1_Dispose_m30564_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30565_MethodInfo,
	&InternalEnumerator_1_get_Current_m30566_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5047_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30565_MethodInfo,
	&InternalEnumerator_1_Dispose_m30564_MethodInfo,
	&InternalEnumerator_1_get_Current_m30566_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5047_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7029_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5047_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7029_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5047_0_0_0;
extern Il2CppType InternalEnumerator_1_t5047_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5047_GenericClass;
TypeInfo InternalEnumerator_1_t5047_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5047_MethodInfos/* methods */
	, InternalEnumerator_1_t5047_PropertyInfos/* properties */
	, InternalEnumerator_1_t5047_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5047_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5047_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5047_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5047_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5047_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5047_1_0_0/* this_arg */
	, InternalEnumerator_1_t5047_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5047_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5047)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8937_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>
extern MethodInfo ICollection_1_get_Count_m50193_MethodInfo;
static PropertyInfo ICollection_1_t8937____Count_PropertyInfo = 
{
	&ICollection_1_t8937_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50193_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50194_MethodInfo;
static PropertyInfo ICollection_1_t8937____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8937_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50194_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8937_PropertyInfos[] =
{
	&ICollection_1_t8937____Count_PropertyInfo,
	&ICollection_1_t8937____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50193_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::get_Count()
MethodInfo ICollection_1_get_Count_m50193_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8937_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50193_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50194_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50194_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50194_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableState_t1365_0_0_0;
extern Il2CppType EditorBrowsableState_t1365_0_0_0;
static ParameterInfo ICollection_1_t8937_ICollection_1_Add_m50195_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1365_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50195_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Add(T)
MethodInfo ICollection_1_Add_m50195_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8937_ICollection_1_Add_m50195_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50195_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50196_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Clear()
MethodInfo ICollection_1_Clear_m50196_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50196_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableState_t1365_0_0_0;
static ParameterInfo ICollection_1_t8937_ICollection_1_Contains_m50197_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1365_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50197_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Contains(T)
MethodInfo ICollection_1_Contains_m50197_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8937_ICollection_1_Contains_m50197_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50197_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableStateU5BU5D_t5886_0_0_0;
extern Il2CppType EditorBrowsableStateU5BU5D_t5886_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8937_ICollection_1_CopyTo_m50198_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableStateU5BU5D_t5886_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50198_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50198_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8937_ICollection_1_CopyTo_m50198_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50198_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableState_t1365_0_0_0;
static ParameterInfo ICollection_1_t8937_ICollection_1_Remove_m50199_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1365_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50199_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Remove(T)
MethodInfo ICollection_1_Remove_m50199_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8937_ICollection_1_Remove_m50199_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50199_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8937_MethodInfos[] =
{
	&ICollection_1_get_Count_m50193_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50194_MethodInfo,
	&ICollection_1_Add_m50195_MethodInfo,
	&ICollection_1_Clear_m50196_MethodInfo,
	&ICollection_1_Contains_m50197_MethodInfo,
	&ICollection_1_CopyTo_m50198_MethodInfo,
	&ICollection_1_Remove_m50199_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8939_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8937_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8939_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8937_0_0_0;
extern Il2CppType ICollection_1_t8937_1_0_0;
struct ICollection_1_t8937;
extern Il2CppGenericClass ICollection_1_t8937_GenericClass;
TypeInfo ICollection_1_t8937_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8937_MethodInfos/* methods */
	, ICollection_1_t8937_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8937_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8937_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8937_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8937_0_0_0/* byval_arg */
	, &ICollection_1_t8937_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8937_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableState>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableState>
extern Il2CppType IEnumerator_1_t7029_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50200_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableState>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50200_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8939_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50200_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8939_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50200_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8939_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8939_0_0_0;
extern Il2CppType IEnumerable_1_t8939_1_0_0;
struct IEnumerable_1_t8939;
extern Il2CppGenericClass IEnumerable_1_t8939_GenericClass;
TypeInfo IEnumerable_1_t8939_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8939_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8939_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8939_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8939_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8939_0_0_0/* byval_arg */
	, &IEnumerable_1_t8939_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8939_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8938_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>
extern MethodInfo IList_1_get_Item_m50201_MethodInfo;
extern MethodInfo IList_1_set_Item_m50202_MethodInfo;
static PropertyInfo IList_1_t8938____Item_PropertyInfo = 
{
	&IList_1_t8938_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50201_MethodInfo/* get */
	, &IList_1_set_Item_m50202_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8938_PropertyInfos[] =
{
	&IList_1_t8938____Item_PropertyInfo,
	NULL
};
extern Il2CppType EditorBrowsableState_t1365_0_0_0;
static ParameterInfo IList_1_t8938_IList_1_IndexOf_m50203_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1365_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50203_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50203_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8938_IList_1_IndexOf_m50203_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50203_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EditorBrowsableState_t1365_0_0_0;
static ParameterInfo IList_1_t8938_IList_1_Insert_m50204_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1365_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50204_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50204_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8938_IList_1_Insert_m50204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50204_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8938_IList_1_RemoveAt_m50205_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50205_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50205_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8938_IList_1_RemoveAt_m50205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50205_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8938_IList_1_get_Item_m50201_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType EditorBrowsableState_t1365_0_0_0;
extern void* RuntimeInvoker_EditorBrowsableState_t1365_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50201_GenericMethod;
// T System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50201_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableState_t1365_0_0_0/* return_type */
	, RuntimeInvoker_EditorBrowsableState_t1365_Int32_t123/* invoker_method */
	, IList_1_t8938_IList_1_get_Item_m50201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50201_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EditorBrowsableState_t1365_0_0_0;
static ParameterInfo IList_1_t8938_IList_1_set_Item_m50202_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1365_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50202_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50202_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8938_IList_1_set_Item_m50202_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50202_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8938_MethodInfos[] =
{
	&IList_1_IndexOf_m50203_MethodInfo,
	&IList_1_Insert_m50204_MethodInfo,
	&IList_1_RemoveAt_m50205_MethodInfo,
	&IList_1_get_Item_m50201_MethodInfo,
	&IList_1_set_Item_m50202_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8938_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8937_il2cpp_TypeInfo,
	&IEnumerable_1_t8939_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8938_0_0_0;
extern Il2CppType IList_1_t8938_1_0_0;
struct IList_1_t8938;
extern Il2CppGenericClass IList_1_t8938_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8938_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8938_MethodInfos/* methods */
	, IList_1_t8938_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8938_il2cpp_TypeInfo/* element_class */
	, IList_1_t8938_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8938_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8938_0_0_0/* byval_arg */
	, &IList_1_t8938_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8938_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7031_il2cpp_TypeInfo;

// System.ComponentModel.TypeConverterAttribute
#include "System_System_ComponentModel_TypeConverterAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.TypeConverterAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ComponentModel.TypeConverterAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50206_MethodInfo;
static PropertyInfo IEnumerator_1_t7031____Current_PropertyInfo = 
{
	&IEnumerator_1_t7031_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50206_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7031_PropertyInfos[] =
{
	&IEnumerator_1_t7031____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeConverterAttribute_t1367_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50206_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.TypeConverterAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50206_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7031_il2cpp_TypeInfo/* declaring_type */
	, &TypeConverterAttribute_t1367_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50206_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7031_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50206_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7031_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7031_0_0_0;
extern Il2CppType IEnumerator_1_t7031_1_0_0;
struct IEnumerator_1_t7031;
extern Il2CppGenericClass IEnumerator_1_t7031_GenericClass;
TypeInfo IEnumerator_1_t7031_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7031_MethodInfos/* methods */
	, IEnumerator_1_t7031_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7031_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7031_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7031_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7031_0_0_0/* byval_arg */
	, &IEnumerator_1_t7031_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7031_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_526.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5048_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_526MethodDeclarations.h"

extern TypeInfo TypeConverterAttribute_t1367_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30571_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeConverterAttribute_t1367_m39467_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.ComponentModel.TypeConverterAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.ComponentModel.TypeConverterAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisTypeConverterAttribute_t1367_m39467(__this, p0, method) (TypeConverterAttribute_t1367 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5048____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5048_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5048, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5048____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5048_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5048, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5048_FieldInfos[] =
{
	&InternalEnumerator_1_t5048____array_0_FieldInfo,
	&InternalEnumerator_1_t5048____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5048____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5048_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5048____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5048_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30571_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5048_PropertyInfos[] =
{
	&InternalEnumerator_1_t5048____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5048____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5048_InternalEnumerator_1__ctor_m30567_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30567_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30567_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5048_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5048_InternalEnumerator_1__ctor_m30567_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30567_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5048_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30569_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30569_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5048_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30569_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30570_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30570_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5048_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30570_GenericMethod/* genericMethod */

};
extern Il2CppType TypeConverterAttribute_t1367_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30571_GenericMethod;
// T System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30571_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5048_il2cpp_TypeInfo/* declaring_type */
	, &TypeConverterAttribute_t1367_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30571_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5048_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30567_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_MethodInfo,
	&InternalEnumerator_1_Dispose_m30569_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30570_MethodInfo,
	&InternalEnumerator_1_get_Current_m30571_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30570_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30569_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5048_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30570_MethodInfo,
	&InternalEnumerator_1_Dispose_m30569_MethodInfo,
	&InternalEnumerator_1_get_Current_m30571_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5048_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7031_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5048_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7031_il2cpp_TypeInfo, 7},
};
extern TypeInfo TypeConverterAttribute_t1367_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5048_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30571_MethodInfo/* Method Usage */,
	&TypeConverterAttribute_t1367_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTypeConverterAttribute_t1367_m39467_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5048_0_0_0;
extern Il2CppType InternalEnumerator_1_t5048_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5048_GenericClass;
TypeInfo InternalEnumerator_1_t5048_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5048_MethodInfos/* methods */
	, InternalEnumerator_1_t5048_PropertyInfos/* properties */
	, InternalEnumerator_1_t5048_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5048_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5048_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5048_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5048_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5048_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5048_1_0_0/* this_arg */
	, InternalEnumerator_1_t5048_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5048_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5048_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5048)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8940_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>
extern MethodInfo ICollection_1_get_Count_m50207_MethodInfo;
static PropertyInfo ICollection_1_t8940____Count_PropertyInfo = 
{
	&ICollection_1_t8940_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50207_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50208_MethodInfo;
static PropertyInfo ICollection_1_t8940____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8940_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50208_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8940_PropertyInfos[] =
{
	&ICollection_1_t8940____Count_PropertyInfo,
	&ICollection_1_t8940____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50207_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50207_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8940_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50207_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50208_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50208_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50208_GenericMethod/* genericMethod */

};
extern Il2CppType TypeConverterAttribute_t1367_0_0_0;
extern Il2CppType TypeConverterAttribute_t1367_0_0_0;
static ParameterInfo ICollection_1_t8940_ICollection_1_Add_m50209_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1367_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50209_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50209_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8940_ICollection_1_Add_m50209_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50209_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50210_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50210_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50210_GenericMethod/* genericMethod */

};
extern Il2CppType TypeConverterAttribute_t1367_0_0_0;
static ParameterInfo ICollection_1_t8940_ICollection_1_Contains_m50211_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1367_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50211_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50211_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8940_ICollection_1_Contains_m50211_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50211_GenericMethod/* genericMethod */

};
extern Il2CppType TypeConverterAttributeU5BU5D_t5887_0_0_0;
extern Il2CppType TypeConverterAttributeU5BU5D_t5887_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8940_ICollection_1_CopyTo_m50212_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttributeU5BU5D_t5887_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50212_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50212_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8940_ICollection_1_CopyTo_m50212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50212_GenericMethod/* genericMethod */

};
extern Il2CppType TypeConverterAttribute_t1367_0_0_0;
static ParameterInfo ICollection_1_t8940_ICollection_1_Remove_m50213_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1367_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50213_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50213_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8940_ICollection_1_Remove_m50213_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50213_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8940_MethodInfos[] =
{
	&ICollection_1_get_Count_m50207_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50208_MethodInfo,
	&ICollection_1_Add_m50209_MethodInfo,
	&ICollection_1_Clear_m50210_MethodInfo,
	&ICollection_1_Contains_m50211_MethodInfo,
	&ICollection_1_CopyTo_m50212_MethodInfo,
	&ICollection_1_Remove_m50213_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8942_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8940_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8942_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8940_0_0_0;
extern Il2CppType ICollection_1_t8940_1_0_0;
struct ICollection_1_t8940;
extern Il2CppGenericClass ICollection_1_t8940_GenericClass;
TypeInfo ICollection_1_t8940_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8940_MethodInfos/* methods */
	, ICollection_1_t8940_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8940_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8940_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8940_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8940_0_0_0/* byval_arg */
	, &ICollection_1_t8940_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8940_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.TypeConverterAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ComponentModel.TypeConverterAttribute>
extern Il2CppType IEnumerator_1_t7031_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50214_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.TypeConverterAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50214_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8942_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7031_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50214_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8942_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50214_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8942_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8942_0_0_0;
extern Il2CppType IEnumerable_1_t8942_1_0_0;
struct IEnumerable_1_t8942;
extern Il2CppGenericClass IEnumerable_1_t8942_GenericClass;
TypeInfo IEnumerable_1_t8942_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8942_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8942_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8942_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8942_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8942_0_0_0/* byval_arg */
	, &IEnumerable_1_t8942_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8942_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8941_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>
extern MethodInfo IList_1_get_Item_m50215_MethodInfo;
extern MethodInfo IList_1_set_Item_m50216_MethodInfo;
static PropertyInfo IList_1_t8941____Item_PropertyInfo = 
{
	&IList_1_t8941_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50215_MethodInfo/* get */
	, &IList_1_set_Item_m50216_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8941_PropertyInfos[] =
{
	&IList_1_t8941____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeConverterAttribute_t1367_0_0_0;
static ParameterInfo IList_1_t8941_IList_1_IndexOf_m50217_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1367_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50217_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50217_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8941_IList_1_IndexOf_m50217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50217_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeConverterAttribute_t1367_0_0_0;
static ParameterInfo IList_1_t8941_IList_1_Insert_m50218_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1367_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50218_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50218_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8941_IList_1_Insert_m50218_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50218_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8941_IList_1_RemoveAt_m50219_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50219_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50219_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8941_IList_1_RemoveAt_m50219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50219_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8941_IList_1_get_Item_m50215_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TypeConverterAttribute_t1367_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50215_GenericMethod;
// T System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50215_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &TypeConverterAttribute_t1367_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8941_IList_1_get_Item_m50215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50215_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeConverterAttribute_t1367_0_0_0;
static ParameterInfo IList_1_t8941_IList_1_set_Item_m50216_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1367_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50216_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50216_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8941_IList_1_set_Item_m50216_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50216_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8941_MethodInfos[] =
{
	&IList_1_IndexOf_m50217_MethodInfo,
	&IList_1_Insert_m50218_MethodInfo,
	&IList_1_RemoveAt_m50219_MethodInfo,
	&IList_1_get_Item_m50215_MethodInfo,
	&IList_1_set_Item_m50216_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8941_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8940_il2cpp_TypeInfo,
	&IEnumerable_1_t8942_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8941_0_0_0;
extern Il2CppType IList_1_t8941_1_0_0;
struct IList_1_t8941;
extern Il2CppGenericClass IList_1_t8941_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8941_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8941_MethodInfos/* methods */
	, IList_1_t8941_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8941_il2cpp_TypeInfo/* element_class */
	, IList_1_t8941_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8941_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8941_0_0_0/* byval_arg */
	, &IList_1_t8941_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8941_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7033_il2cpp_TypeInfo;

// System.Net.Security.AuthenticationLevel
#include "System_System_Net_Security_AuthenticationLevel.h"


// T System.Collections.Generic.IEnumerator`1<System.Net.Security.AuthenticationLevel>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Net.Security.AuthenticationLevel>
extern MethodInfo IEnumerator_1_get_Current_m50220_MethodInfo;
static PropertyInfo IEnumerator_1_t7033____Current_PropertyInfo = 
{
	&IEnumerator_1_t7033_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50220_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7033_PropertyInfos[] =
{
	&IEnumerator_1_t7033____Current_PropertyInfo,
	NULL
};
extern Il2CppType AuthenticationLevel_t1368_0_0_0;
extern void* RuntimeInvoker_AuthenticationLevel_t1368 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50220_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Net.Security.AuthenticationLevel>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50220_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7033_il2cpp_TypeInfo/* declaring_type */
	, &AuthenticationLevel_t1368_0_0_0/* return_type */
	, RuntimeInvoker_AuthenticationLevel_t1368/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50220_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7033_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50220_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7033_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7033_0_0_0;
extern Il2CppType IEnumerator_1_t7033_1_0_0;
struct IEnumerator_1_t7033;
extern Il2CppGenericClass IEnumerator_1_t7033_GenericClass;
TypeInfo IEnumerator_1_t7033_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7033_MethodInfos/* methods */
	, IEnumerator_1_t7033_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7033_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7033_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7033_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7033_0_0_0/* byval_arg */
	, &IEnumerator_1_t7033_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7033_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_527.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5049_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_527MethodDeclarations.h"

extern TypeInfo AuthenticationLevel_t1368_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30576_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAuthenticationLevel_t1368_m39478_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Net.Security.AuthenticationLevel>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Net.Security.AuthenticationLevel>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisAuthenticationLevel_t1368_m39478 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30572_MethodInfo;
 void InternalEnumerator_1__ctor_m30572 (InternalEnumerator_1_t5049 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573 (InternalEnumerator_1_t5049 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30576(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30576_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&AuthenticationLevel_t1368_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30574_MethodInfo;
 void InternalEnumerator_1_Dispose_m30574 (InternalEnumerator_1_t5049 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30575_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30575 (InternalEnumerator_1_t5049 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30576 (InternalEnumerator_1_t5049 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisAuthenticationLevel_t1368_m39478(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAuthenticationLevel_t1368_m39478_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5049____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5049_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5049, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5049____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5049_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5049, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5049_FieldInfos[] =
{
	&InternalEnumerator_1_t5049____array_0_FieldInfo,
	&InternalEnumerator_1_t5049____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5049____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5049_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5049____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5049_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30576_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5049_PropertyInfos[] =
{
	&InternalEnumerator_1_t5049____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5049____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5049_InternalEnumerator_1__ctor_m30572_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30572_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30572_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30572/* method */
	, &InternalEnumerator_1_t5049_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5049_InternalEnumerator_1__ctor_m30572_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30572_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573/* method */
	, &InternalEnumerator_1_t5049_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30574_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30574_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30574/* method */
	, &InternalEnumerator_1_t5049_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30574_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30575_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30575_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30575/* method */
	, &InternalEnumerator_1_t5049_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30575_GenericMethod/* genericMethod */

};
extern Il2CppType AuthenticationLevel_t1368_0_0_0;
extern void* RuntimeInvoker_AuthenticationLevel_t1368 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30576_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30576_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30576/* method */
	, &InternalEnumerator_1_t5049_il2cpp_TypeInfo/* declaring_type */
	, &AuthenticationLevel_t1368_0_0_0/* return_type */
	, RuntimeInvoker_AuthenticationLevel_t1368/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30576_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5049_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30572_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_MethodInfo,
	&InternalEnumerator_1_Dispose_m30574_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30575_MethodInfo,
	&InternalEnumerator_1_get_Current_m30576_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5049_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30575_MethodInfo,
	&InternalEnumerator_1_Dispose_m30574_MethodInfo,
	&InternalEnumerator_1_get_Current_m30576_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5049_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7033_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5049_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7033_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5049_0_0_0;
extern Il2CppType InternalEnumerator_1_t5049_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5049_GenericClass;
TypeInfo InternalEnumerator_1_t5049_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5049_MethodInfos/* methods */
	, InternalEnumerator_1_t5049_PropertyInfos/* properties */
	, InternalEnumerator_1_t5049_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5049_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5049_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5049_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5049_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5049_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5049_1_0_0/* this_arg */
	, InternalEnumerator_1_t5049_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5049_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5049)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8943_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>
extern MethodInfo ICollection_1_get_Count_m50221_MethodInfo;
static PropertyInfo ICollection_1_t8943____Count_PropertyInfo = 
{
	&ICollection_1_t8943_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50221_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50222_MethodInfo;
static PropertyInfo ICollection_1_t8943____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8943_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50222_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8943_PropertyInfos[] =
{
	&ICollection_1_t8943____Count_PropertyInfo,
	&ICollection_1_t8943____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50221_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::get_Count()
MethodInfo ICollection_1_get_Count_m50221_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8943_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50221_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50222_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50222_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8943_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50222_GenericMethod/* genericMethod */

};
extern Il2CppType AuthenticationLevel_t1368_0_0_0;
extern Il2CppType AuthenticationLevel_t1368_0_0_0;
static ParameterInfo ICollection_1_t8943_ICollection_1_Add_m50223_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1368_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50223_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Add(T)
MethodInfo ICollection_1_Add_m50223_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8943_ICollection_1_Add_m50223_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50223_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50224_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Clear()
MethodInfo ICollection_1_Clear_m50224_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50224_GenericMethod/* genericMethod */

};
extern Il2CppType AuthenticationLevel_t1368_0_0_0;
static ParameterInfo ICollection_1_t8943_ICollection_1_Contains_m50225_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1368_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50225_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Contains(T)
MethodInfo ICollection_1_Contains_m50225_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8943_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8943_ICollection_1_Contains_m50225_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50225_GenericMethod/* genericMethod */

};
extern Il2CppType AuthenticationLevelU5BU5D_t5888_0_0_0;
extern Il2CppType AuthenticationLevelU5BU5D_t5888_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8943_ICollection_1_CopyTo_m50226_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevelU5BU5D_t5888_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50226_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50226_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8943_ICollection_1_CopyTo_m50226_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50226_GenericMethod/* genericMethod */

};
extern Il2CppType AuthenticationLevel_t1368_0_0_0;
static ParameterInfo ICollection_1_t8943_ICollection_1_Remove_m50227_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1368_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50227_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Remove(T)
MethodInfo ICollection_1_Remove_m50227_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8943_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8943_ICollection_1_Remove_m50227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50227_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8943_MethodInfos[] =
{
	&ICollection_1_get_Count_m50221_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50222_MethodInfo,
	&ICollection_1_Add_m50223_MethodInfo,
	&ICollection_1_Clear_m50224_MethodInfo,
	&ICollection_1_Contains_m50225_MethodInfo,
	&ICollection_1_CopyTo_m50226_MethodInfo,
	&ICollection_1_Remove_m50227_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8945_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8943_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8945_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8943_0_0_0;
extern Il2CppType ICollection_1_t8943_1_0_0;
struct ICollection_1_t8943;
extern Il2CppGenericClass ICollection_1_t8943_GenericClass;
TypeInfo ICollection_1_t8943_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8943_MethodInfos/* methods */
	, ICollection_1_t8943_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8943_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8943_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8943_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8943_0_0_0/* byval_arg */
	, &ICollection_1_t8943_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8943_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Net.Security.AuthenticationLevel>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Net.Security.AuthenticationLevel>
extern Il2CppType IEnumerator_1_t7033_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50228_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Net.Security.AuthenticationLevel>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50228_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8945_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7033_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50228_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8945_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50228_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8945_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8945_0_0_0;
extern Il2CppType IEnumerable_1_t8945_1_0_0;
struct IEnumerable_1_t8945;
extern Il2CppGenericClass IEnumerable_1_t8945_GenericClass;
TypeInfo IEnumerable_1_t8945_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8945_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8945_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8945_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8945_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8945_0_0_0/* byval_arg */
	, &IEnumerable_1_t8945_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8945_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8944_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>
extern MethodInfo IList_1_get_Item_m50229_MethodInfo;
extern MethodInfo IList_1_set_Item_m50230_MethodInfo;
static PropertyInfo IList_1_t8944____Item_PropertyInfo = 
{
	&IList_1_t8944_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50229_MethodInfo/* get */
	, &IList_1_set_Item_m50230_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8944_PropertyInfos[] =
{
	&IList_1_t8944____Item_PropertyInfo,
	NULL
};
extern Il2CppType AuthenticationLevel_t1368_0_0_0;
static ParameterInfo IList_1_t8944_IList_1_IndexOf_m50231_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1368_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50231_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50231_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8944_IList_1_IndexOf_m50231_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50231_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AuthenticationLevel_t1368_0_0_0;
static ParameterInfo IList_1_t8944_IList_1_Insert_m50232_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1368_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50232_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50232_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8944_IList_1_Insert_m50232_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50232_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8944_IList_1_RemoveAt_m50233_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50233_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50233_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8944_IList_1_RemoveAt_m50233_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50233_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8944_IList_1_get_Item_m50229_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType AuthenticationLevel_t1368_0_0_0;
extern void* RuntimeInvoker_AuthenticationLevel_t1368_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50229_GenericMethod;
// T System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50229_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &AuthenticationLevel_t1368_0_0_0/* return_type */
	, RuntimeInvoker_AuthenticationLevel_t1368_Int32_t123/* invoker_method */
	, IList_1_t8944_IList_1_get_Item_m50229_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50229_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AuthenticationLevel_t1368_0_0_0;
static ParameterInfo IList_1_t8944_IList_1_set_Item_m50230_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1368_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50230_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50230_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8944_IList_1_set_Item_m50230_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50230_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8944_MethodInfos[] =
{
	&IList_1_IndexOf_m50231_MethodInfo,
	&IList_1_Insert_m50232_MethodInfo,
	&IList_1_RemoveAt_m50233_MethodInfo,
	&IList_1_get_Item_m50229_MethodInfo,
	&IList_1_set_Item_m50230_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8944_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8943_il2cpp_TypeInfo,
	&IEnumerable_1_t8945_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8944_0_0_0;
extern Il2CppType IList_1_t8944_1_0_0;
struct IList_1_t8944;
extern Il2CppGenericClass IList_1_t8944_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8944_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8944_MethodInfos/* methods */
	, IList_1_t8944_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8944_il2cpp_TypeInfo/* element_class */
	, IList_1_t8944_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8944_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8944_0_0_0/* byval_arg */
	, &IList_1_t8944_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8944_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7035_il2cpp_TypeInfo;

// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrors.h"


// T System.Collections.Generic.IEnumerator`1<System.Net.Security.SslPolicyErrors>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Net.Security.SslPolicyErrors>
extern MethodInfo IEnumerator_1_get_Current_m50234_MethodInfo;
static PropertyInfo IEnumerator_1_t7035____Current_PropertyInfo = 
{
	&IEnumerator_1_t7035_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50234_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7035_PropertyInfos[] =
{
	&IEnumerator_1_t7035____Current_PropertyInfo,
	NULL
};
extern Il2CppType SslPolicyErrors_t1369_0_0_0;
extern void* RuntimeInvoker_SslPolicyErrors_t1369 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50234_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Net.Security.SslPolicyErrors>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50234_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7035_il2cpp_TypeInfo/* declaring_type */
	, &SslPolicyErrors_t1369_0_0_0/* return_type */
	, RuntimeInvoker_SslPolicyErrors_t1369/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50234_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7035_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50234_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7035_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7035_0_0_0;
extern Il2CppType IEnumerator_1_t7035_1_0_0;
struct IEnumerator_1_t7035;
extern Il2CppGenericClass IEnumerator_1_t7035_GenericClass;
TypeInfo IEnumerator_1_t7035_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7035_MethodInfos/* methods */
	, IEnumerator_1_t7035_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7035_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7035_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7035_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7035_0_0_0/* byval_arg */
	, &IEnumerator_1_t7035_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7035_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_528.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5050_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_528MethodDeclarations.h"

extern TypeInfo SslPolicyErrors_t1369_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30581_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSslPolicyErrors_t1369_m39489_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Net.Security.SslPolicyErrors>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Net.Security.SslPolicyErrors>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSslPolicyErrors_t1369_m39489 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30577_MethodInfo;
 void InternalEnumerator_1__ctor_m30577 (InternalEnumerator_1_t5050 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578 (InternalEnumerator_1_t5050 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30581(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30581_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&SslPolicyErrors_t1369_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30579_MethodInfo;
 void InternalEnumerator_1_Dispose_m30579 (InternalEnumerator_1_t5050 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30580_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30580 (InternalEnumerator_1_t5050 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30581 (InternalEnumerator_1_t5050 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisSslPolicyErrors_t1369_m39489(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisSslPolicyErrors_t1369_m39489_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5050____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5050_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5050, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5050____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5050_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5050, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5050_FieldInfos[] =
{
	&InternalEnumerator_1_t5050____array_0_FieldInfo,
	&InternalEnumerator_1_t5050____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5050____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5050_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5050____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5050_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30581_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5050_PropertyInfos[] =
{
	&InternalEnumerator_1_t5050____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5050____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5050_InternalEnumerator_1__ctor_m30577_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30577_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30577_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30577/* method */
	, &InternalEnumerator_1_t5050_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5050_InternalEnumerator_1__ctor_m30577_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30577_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578/* method */
	, &InternalEnumerator_1_t5050_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30579_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30579_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30579/* method */
	, &InternalEnumerator_1_t5050_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30579_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30580_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30580_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30580/* method */
	, &InternalEnumerator_1_t5050_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30580_GenericMethod/* genericMethod */

};
extern Il2CppType SslPolicyErrors_t1369_0_0_0;
extern void* RuntimeInvoker_SslPolicyErrors_t1369 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30581_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30581_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30581/* method */
	, &InternalEnumerator_1_t5050_il2cpp_TypeInfo/* declaring_type */
	, &SslPolicyErrors_t1369_0_0_0/* return_type */
	, RuntimeInvoker_SslPolicyErrors_t1369/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30581_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5050_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30577_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_MethodInfo,
	&InternalEnumerator_1_Dispose_m30579_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30580_MethodInfo,
	&InternalEnumerator_1_get_Current_m30581_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5050_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30580_MethodInfo,
	&InternalEnumerator_1_Dispose_m30579_MethodInfo,
	&InternalEnumerator_1_get_Current_m30581_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5050_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7035_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5050_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7035_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5050_0_0_0;
extern Il2CppType InternalEnumerator_1_t5050_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5050_GenericClass;
TypeInfo InternalEnumerator_1_t5050_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5050_MethodInfos/* methods */
	, InternalEnumerator_1_t5050_PropertyInfos/* properties */
	, InternalEnumerator_1_t5050_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5050_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5050_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5050_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5050_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5050_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5050_1_0_0/* this_arg */
	, InternalEnumerator_1_t5050_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5050_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5050)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8946_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>
extern MethodInfo ICollection_1_get_Count_m50235_MethodInfo;
static PropertyInfo ICollection_1_t8946____Count_PropertyInfo = 
{
	&ICollection_1_t8946_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50235_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50236_MethodInfo;
static PropertyInfo ICollection_1_t8946____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8946_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50236_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8946_PropertyInfos[] =
{
	&ICollection_1_t8946____Count_PropertyInfo,
	&ICollection_1_t8946____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50235_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::get_Count()
MethodInfo ICollection_1_get_Count_m50235_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8946_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50235_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50236_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50236_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8946_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50236_GenericMethod/* genericMethod */

};
extern Il2CppType SslPolicyErrors_t1369_0_0_0;
extern Il2CppType SslPolicyErrors_t1369_0_0_0;
static ParameterInfo ICollection_1_t8946_ICollection_1_Add_m50237_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrors_t1369_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50237_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Add(T)
MethodInfo ICollection_1_Add_m50237_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8946_ICollection_1_Add_m50237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50237_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50238_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Clear()
MethodInfo ICollection_1_Clear_m50238_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50238_GenericMethod/* genericMethod */

};
extern Il2CppType SslPolicyErrors_t1369_0_0_0;
static ParameterInfo ICollection_1_t8946_ICollection_1_Contains_m50239_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrors_t1369_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50239_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Contains(T)
MethodInfo ICollection_1_Contains_m50239_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8946_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8946_ICollection_1_Contains_m50239_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50239_GenericMethod/* genericMethod */

};
extern Il2CppType SslPolicyErrorsU5BU5D_t5889_0_0_0;
extern Il2CppType SslPolicyErrorsU5BU5D_t5889_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8946_ICollection_1_CopyTo_m50240_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrorsU5BU5D_t5889_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50240_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50240_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8946_ICollection_1_CopyTo_m50240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50240_GenericMethod/* genericMethod */

};
extern Il2CppType SslPolicyErrors_t1369_0_0_0;
static ParameterInfo ICollection_1_t8946_ICollection_1_Remove_m50241_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrors_t1369_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50241_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Remove(T)
MethodInfo ICollection_1_Remove_m50241_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8946_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8946_ICollection_1_Remove_m50241_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50241_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8946_MethodInfos[] =
{
	&ICollection_1_get_Count_m50235_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50236_MethodInfo,
	&ICollection_1_Add_m50237_MethodInfo,
	&ICollection_1_Clear_m50238_MethodInfo,
	&ICollection_1_Contains_m50239_MethodInfo,
	&ICollection_1_CopyTo_m50240_MethodInfo,
	&ICollection_1_Remove_m50241_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8948_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8946_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8948_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8946_0_0_0;
extern Il2CppType ICollection_1_t8946_1_0_0;
struct ICollection_1_t8946;
extern Il2CppGenericClass ICollection_1_t8946_GenericClass;
TypeInfo ICollection_1_t8946_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8946_MethodInfos/* methods */
	, ICollection_1_t8946_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8946_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8946_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8946_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8946_0_0_0/* byval_arg */
	, &ICollection_1_t8946_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8946_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Net.Security.SslPolicyErrors>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Net.Security.SslPolicyErrors>
extern Il2CppType IEnumerator_1_t7035_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50242_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Net.Security.SslPolicyErrors>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50242_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8948_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7035_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50242_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8948_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50242_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8948_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8948_0_0_0;
extern Il2CppType IEnumerable_1_t8948_1_0_0;
struct IEnumerable_1_t8948;
extern Il2CppGenericClass IEnumerable_1_t8948_GenericClass;
TypeInfo IEnumerable_1_t8948_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8948_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8948_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8948_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8948_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8948_0_0_0/* byval_arg */
	, &IEnumerable_1_t8948_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8948_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
