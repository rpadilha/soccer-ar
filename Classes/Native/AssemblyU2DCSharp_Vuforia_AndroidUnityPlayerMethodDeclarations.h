﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.AndroidUnityPlayer
struct AndroidUnityPlayer_t24;
// System.String
struct String_t;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.AndroidUnityPlayer::.ctor()
 void AndroidUnityPlayer__ctor_m30 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
 void AndroidUnityPlayer_LoadNativeLibraries_m31 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
 void AndroidUnityPlayer_InitializePlatform_m32 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARUnity/InitError Vuforia.AndroidUnityPlayer::Start(System.String)
 int32_t AndroidUnityPlayer_Start_m33 (AndroidUnityPlayer_t24 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::Update()
 void AndroidUnityPlayer_Update_m34 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
 void AndroidUnityPlayer_OnPause_m35 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
 void AndroidUnityPlayer_OnResume_m36 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
 void AndroidUnityPlayer_OnDestroy_m37 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
 void AndroidUnityPlayer_Dispose_m38 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
 void AndroidUnityPlayer_LoadNativeLibrariesFromJava_m39 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
 void AndroidUnityPlayer_InitAndroidPlatform_m40 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.AndroidUnityPlayer::InitQCAR(System.String)
 int32_t AndroidUnityPlayer_InitQCAR_m41 (AndroidUnityPlayer_t24 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
 void AndroidUnityPlayer_InitializeSurface_m42 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
 void AndroidUnityPlayer_ResetUnityScreenOrientation_m43 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
 void AndroidUnityPlayer_CheckOrientation_m44 (AndroidUnityPlayer_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
