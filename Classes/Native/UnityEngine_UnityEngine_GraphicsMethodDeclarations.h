﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Graphics
struct Graphics_t985;
// UnityEngine.InternalDrawTextureArguments
#include "UnityEngine_UnityEngine_InternalDrawTextureArguments.h"

// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.InternalDrawTextureArguments&)
 void Graphics_DrawTexture_m5688 (Object_t * __this/* static, unused */, InternalDrawTextureArguments_t984 * ___arguments, MethodInfo* method) IL2CPP_METHOD_ATTR;
