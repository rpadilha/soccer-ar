﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Net.SecurityProtocolType>
struct InternalEnumerator_1_t5053;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolType.h"

// System.Void System.Array/InternalEnumerator`1<System.Net.SecurityProtocolType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30592 (InternalEnumerator_1_t5053 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Net.SecurityProtocolType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30593 (InternalEnumerator_1_t5053 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Net.SecurityProtocolType>::Dispose()
 void InternalEnumerator_1_Dispose_m30594 (InternalEnumerator_1_t5053 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Net.SecurityProtocolType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30595 (InternalEnumerator_1_t5053 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Net.SecurityProtocolType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30596 (InternalEnumerator_1_t5053 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
