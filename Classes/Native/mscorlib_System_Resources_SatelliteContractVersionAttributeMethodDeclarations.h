﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Resources.SatelliteContractVersionAttribute
struct SatelliteContractVersionAttribute_t1334;
// System.String
struct String_t;

// System.Void System.Resources.SatelliteContractVersionAttribute::.ctor(System.String)
 void SatelliteContractVersionAttribute__ctor_m6888 (SatelliteContractVersionAttribute_t1334 * __this, String_t* ___version, MethodInfo* method) IL2CPP_METHOD_ATTR;
