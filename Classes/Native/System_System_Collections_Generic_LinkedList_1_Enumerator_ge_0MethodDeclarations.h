﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
struct Enumerator_t5045;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t5044;

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
 void Enumerator__ctor_m30552_gshared (Enumerator_t5045 * __this, LinkedList_1_t5044 * ___parent, MethodInfo* method);
#define Enumerator__ctor_m30552(__this, ___parent, method) (void)Enumerator__ctor_m30552_gshared((Enumerator_t5045 *)__this, (LinkedList_1_t5044 *)___parent, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m30553_gshared (Enumerator_t5045 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m30553(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m30553_gshared((Enumerator_t5045 *)__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
 Object_t * Enumerator_get_Current_m30554_gshared (Enumerator_t5045 * __this, MethodInfo* method);
#define Enumerator_get_Current_m30554(__this, method) (Object_t *)Enumerator_get_Current_m30554_gshared((Enumerator_t5045 *)__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
 bool Enumerator_MoveNext_m30555_gshared (Enumerator_t5045 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m30555(__this, method) (bool)Enumerator_MoveNext_m30555_gshared((Enumerator_t5045 *)__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
 void Enumerator_Dispose_m30556_gshared (Enumerator_t5045 * __this, MethodInfo* method);
#define Enumerator_Dispose_m30556(__this, method) (void)Enumerator_Dispose_m30556_gshared((Enumerator_t5045 *)__this, method)
