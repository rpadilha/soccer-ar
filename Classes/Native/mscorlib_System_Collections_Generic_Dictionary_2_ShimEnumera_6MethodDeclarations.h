﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct ShimEnumerator_t3588;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct Dictionary_2_t351;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_3MethodDeclarations.h"
#define ShimEnumerator__ctor_m19450(__this, ___host, method) (void)ShimEnumerator__ctor_m18416_gshared((ShimEnumerator_t3466 *)__this, (Dictionary_2_t3452 *)___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::MoveNext()
#define ShimEnumerator_MoveNext_m19451(__this, method) (bool)ShimEnumerator_MoveNext_m18417_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Entry()
#define ShimEnumerator_get_Entry_m19452(__this, method) (DictionaryEntry_t1355 )ShimEnumerator_get_Entry_m18418_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Key()
#define ShimEnumerator_get_Key_m19453(__this, method) (Object_t *)ShimEnumerator_get_Key_m18419_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Value()
#define ShimEnumerator_get_Value_m19454(__this, method) (Object_t *)ShimEnumerator_get_Value_m18420_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Current()
#define ShimEnumerator_get_Current_m19455(__this, method) (Object_t *)ShimEnumerator_get_Current_m18421_gshared((ShimEnumerator_t3466 *)__this, method)
