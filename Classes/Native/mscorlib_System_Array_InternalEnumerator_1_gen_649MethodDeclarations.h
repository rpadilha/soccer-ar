﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>
struct InternalEnumerator_1_t5193;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Globalization.NumberStyles
#include "mscorlib_System_Globalization_NumberStyles.h"

// System.Void System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31339 (InternalEnumerator_1_t5193 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31340 (InternalEnumerator_1_t5193 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::Dispose()
 void InternalEnumerator_1_Dispose_m31341 (InternalEnumerator_1_t5193 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31342 (InternalEnumerator_1_t5193 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31343 (InternalEnumerator_1_t5193 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
