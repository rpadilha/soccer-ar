﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUI/ScrollViewState
struct ScrollViewState_t996;

// System.Void UnityEngine.GUI/ScrollViewState::.ctor()
 void ScrollViewState__ctor_m5731 (ScrollViewState_t996 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
