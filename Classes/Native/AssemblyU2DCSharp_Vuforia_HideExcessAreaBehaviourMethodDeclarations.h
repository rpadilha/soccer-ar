﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.HideExcessAreaBehaviour
struct HideExcessAreaBehaviour_t20;

// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
 void HideExcessAreaBehaviour__ctor_m28 (HideExcessAreaBehaviour_t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
