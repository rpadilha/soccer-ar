﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct ShimEnumerator_t4472;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Dictionary_2_t794;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m26901 (ShimEnumerator_t4472 * __this, Dictionary_2_t794 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
 bool ShimEnumerator_MoveNext_m26902 (ShimEnumerator_t4472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m26903 (ShimEnumerator_t4472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Key()
 Object_t * ShimEnumerator_get_Key_m26904 (ShimEnumerator_t4472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Value()
 Object_t * ShimEnumerator_get_Value_m26905 (ShimEnumerator_t4472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
 Object_t * ShimEnumerator_get_Current_m26906 (ShimEnumerator_t4472 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
