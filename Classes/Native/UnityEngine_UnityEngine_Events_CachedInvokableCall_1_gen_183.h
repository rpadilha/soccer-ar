﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_186.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>
struct CachedInvokableCall_1_t5031  : public InvokableCall_1_t5032
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
