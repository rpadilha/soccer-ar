﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// UnityEngine.UI.Selectable
struct Selectable_t324;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.Selectable>
struct UnityAction_1_t3640  : public MulticastDelegate_t373
{
};
