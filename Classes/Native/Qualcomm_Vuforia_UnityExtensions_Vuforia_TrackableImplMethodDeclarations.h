﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackableImpl
struct TrackableImpl_t609;
// System.String
struct String_t;

// System.Void Vuforia.TrackableImpl::.ctor(System.String,System.Int32)
 void TrackableImpl__ctor_m2855 (TrackableImpl_t609 * __this, String_t* ___name, int32_t ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.TrackableImpl::get_Name()
 String_t* TrackableImpl_get_Name_m2856 (TrackableImpl_t609 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableImpl::set_Name(System.String)
 void TrackableImpl_set_Name_m2857 (TrackableImpl_t609 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.TrackableImpl::get_ID()
 int32_t TrackableImpl_get_ID_m2858 (TrackableImpl_t609 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableImpl::set_ID(System.Int32)
 void TrackableImpl_set_ID_m2859 (TrackableImpl_t609 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
