﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Sphere
struct Sphere_t71;

// System.Void Sphere::.ctor()
 void Sphere__ctor_m190 (Sphere_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sphere::Start()
 void Sphere_Start_m191 (Sphere_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sphere::LateUpdate()
 void Sphere_LateUpdate_m192 (Sphere_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sphere::Update()
 void Sphere_Update_m193 (Sphere_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sphere::ActivateNearestOponent()
 void Sphere_ActivateNearestOponent_m194 (Sphere_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sphere::ActivateNearestPlayer()
 void Sphere_ActivateNearestPlayer_m195 (Sphere_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
