﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Joystick>
struct UnityAction_1_t3131;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Joystick>
struct InvokableCall_1_t3130  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Joystick>::Delegate
	UnityAction_1_t3131 * ___Delegate_0;
};
