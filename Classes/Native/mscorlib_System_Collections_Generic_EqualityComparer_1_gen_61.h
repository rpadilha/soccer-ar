﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.IVirtualButtonEventHandler>
struct EqualityComparer_1_t4591;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.IVirtualButtonEventHandler>
struct EqualityComparer_1_t4591  : public Object_t
{
};
struct EqualityComparer_1_t4591_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.IVirtualButtonEventHandler>::_default
	EqualityComparer_1_t4591 * ____default_0;
};
