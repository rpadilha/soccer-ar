﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<ZoomCamera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_65.h"
// UnityEngine.Events.CachedInvokableCall`1<ZoomCamera>
struct CachedInvokableCall_1_t3168  : public InvokableCall_1_t3169
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<ZoomCamera>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
