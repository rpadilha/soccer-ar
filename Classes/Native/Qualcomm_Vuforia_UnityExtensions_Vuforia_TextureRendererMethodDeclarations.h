﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextureRenderer
struct TextureRenderer_t785;
// UnityEngine.Texture
struct Texture_t107;
// UnityEngine.RenderTexture
struct RenderTexture_t786;
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"

// System.Int32 Vuforia.TextureRenderer::get_Width()
 int32_t TextureRenderer_get_Width_m4202 (TextureRenderer_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.TextureRenderer::get_Height()
 int32_t TextureRenderer_get_Height_m4203 (TextureRenderer_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextureRenderer::.ctor(UnityEngine.Texture,System.Int32,Vuforia.QCARRenderer/Vec2I)
 void TextureRenderer__ctor_m4204 (TextureRenderer_t785 * __this, Texture_t107 * ___textureToRender, int32_t ___renderTextureLayer, Vec2I_t675  ___requestedTextureSize, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture Vuforia.TextureRenderer::Render()
 RenderTexture_t786 * TextureRenderer_Render_m4205 (TextureRenderer_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextureRenderer::Destroy()
 void TextureRenderer_Destroy_m4206 (TextureRenderer_t785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
