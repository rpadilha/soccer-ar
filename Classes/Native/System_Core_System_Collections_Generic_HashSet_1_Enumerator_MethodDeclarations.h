﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>
struct Enumerator_t946;
// System.Object
struct Object_t;
// UnityEngine.MeshRenderer
struct MeshRenderer_t167;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t813;

// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::.ctor(System.Collections.Generic.HashSet`1<T>)
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0MethodDeclarations.h"
#define Enumerator__ctor_m27640(__this, ___hashset, method) (void)Enumerator__ctor_m27625_gshared((Enumerator_t4567 *)__this, (HashSet_1_t4564 *)___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27641(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m27626_gshared((Enumerator_t4567 *)__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::MoveNext()
#define Enumerator_MoveNext_m5546(__this, method) (bool)Enumerator_MoveNext_m27627_gshared((Enumerator_t4567 *)__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::get_Current()
#define Enumerator_get_Current_m5545(__this, method) (MeshRenderer_t167 *)Enumerator_get_Current_m27628_gshared((Enumerator_t4567 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::Dispose()
#define Enumerator_Dispose_m27642(__this, method) (void)Enumerator_Dispose_m27629_gshared((Enumerator_t4567 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::CheckState()
#define Enumerator_CheckState_m27643(__this, method) (void)Enumerator_CheckState_m27630_gshared((Enumerator_t4567 *)__this, method)
