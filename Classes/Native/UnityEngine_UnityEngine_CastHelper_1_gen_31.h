﻿#pragma once
#include <stdint.h>
// Vuforia.DataSetTrackableBehaviour
struct DataSetTrackableBehaviour_t596;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.DataSetTrackableBehaviour>
struct CastHelper_1_t4345 
{
	// T UnityEngine.CastHelper`1<Vuforia.DataSetTrackableBehaviour>::t
	DataSetTrackableBehaviour_t596 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.DataSetTrackableBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
