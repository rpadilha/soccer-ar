﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_18.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo InvokableCall_1_t2918_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_18MethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_25.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
#include "mscorlib_ArrayTypes.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
extern TypeInfo UnityAction_1_t2919_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
extern TypeInfo ReconstructionBehaviour_t11_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_25MethodDeclarations.h"
extern Il2CppType UnityAction_1_t2919_0_0_0;
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionBehaviour_t11_m33163_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo UnityAction_1_Invoke_m15046_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.ReconstructionBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.ReconstructionBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionBehaviour_t11_m33163(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)

// System.Array
#include "mscorlib_System_Array.h"

// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType UnityAction_1_t2919_0_0_1;
FieldInfo InvokableCall_1_t2918____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2919_0_0_1/* type */
	, &InvokableCall_1_t2918_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2918, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2918_FieldInfos[] =
{
	&InvokableCall_1_t2918____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2918_InvokableCall_1__ctor_m15041_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15041_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15041_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2918_InvokableCall_1__ctor_m15041_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15041_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2919_0_0_0;
static ParameterInfo InvokableCall_1_t2918_InvokableCall_1__ctor_m15042_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2919_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15042_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15042_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2918_InvokableCall_1__ctor_m15042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15042_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2918_InvokableCall_1_Invoke_m15043_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15043_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15043_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2918_InvokableCall_1_Invoke_m15043_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15043_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2918_InvokableCall_1_Find_m15044_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15044_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15044_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2918_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2918_InvokableCall_1_Find_m15044_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15044_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2918_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15041_MethodInfo,
	&InvokableCall_1__ctor_m15042_MethodInfo,
	&InvokableCall_1_Invoke_m15043_MethodInfo,
	&InvokableCall_1_Find_m15044_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15043_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15044_MethodInfo;
static MethodInfo* InvokableCall_1_t2918_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15043_MethodInfo,
	&InvokableCall_1_Find_m15044_MethodInfo,
};
extern TypeInfo UnityAction_1_t2919_il2cpp_TypeInfo;
extern TypeInfo ReconstructionBehaviour_t11_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2918_RGCTXData[5] = 
{
	&UnityAction_1_t2919_0_0_0/* Type Usage */,
	&UnityAction_1_t2919_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionBehaviour_t11_m33163_MethodInfo/* Method Usage */,
	&ReconstructionBehaviour_t11_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15046_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2918_0_0_0;
extern Il2CppType InvokableCall_1_t2918_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t2918;
extern Il2CppGenericClass InvokableCall_1_t2918_GenericClass;
TypeInfo InvokableCall_1_t2918_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2918_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2918_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2918_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2918_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2918_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2918_0_0_0/* byval_arg */
	, &InvokableCall_1_t2918_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2918_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2918_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2918)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2919_UnityAction_1__ctor_m15045_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15045_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15045_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2919_UnityAction_1__ctor_m15045_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15045_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
static ParameterInfo UnityAction_1_t2919_UnityAction_1_Invoke_m15046_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t11_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15046_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15046_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2919_UnityAction_1_Invoke_m15046_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15046_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t11_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2919_UnityAction_1_BeginInvoke_m15047_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t11_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15047_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15047_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2919_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2919_UnityAction_1_BeginInvoke_m15047_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15047_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2919_UnityAction_1_EndInvoke_m15048_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15048_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15048_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2919_UnityAction_1_EndInvoke_m15048_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15048_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2919_MethodInfos[] =
{
	&UnityAction_1__ctor_m15045_MethodInfo,
	&UnityAction_1_Invoke_m15046_MethodInfo,
	&UnityAction_1_BeginInvoke_m15047_MethodInfo,
	&UnityAction_1_EndInvoke_m15048_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m15047_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15048_MethodInfo;
static MethodInfo* UnityAction_1_t2919_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15046_MethodInfo,
	&UnityAction_1_BeginInvoke_m15047_MethodInfo,
	&UnityAction_1_EndInvoke_m15048_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2919_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2919_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_1_t2919;
extern Il2CppGenericClass UnityAction_1_t2919_GenericClass;
TypeInfo UnityAction_1_t2919_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2919_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2919_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2919_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2919_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2919_0_0_0/* byval_arg */
	, &UnityAction_1_t2919_1_0_0/* this_arg */
	, UnityAction_1_t2919_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2919_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2919)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6078_il2cpp_TypeInfo;

// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43480_MethodInfo;
static PropertyInfo IEnumerator_1_t6078____Current_PropertyInfo = 
{
	&IEnumerator_1_t6078_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43480_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6078_PropertyInfos[] =
{
	&IEnumerator_1_t6078____Current_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43480_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43480_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6078_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetBehaviour_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43480_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6078_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43480_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6078_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6078_0_0_0;
extern Il2CppType IEnumerator_1_t6078_1_0_0;
struct IEnumerator_1_t6078;
extern Il2CppGenericClass IEnumerator_1_t6078_GenericClass;
TypeInfo IEnumerator_1_t6078_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6078_MethodInfos/* methods */
	, IEnumerator_1_t6078_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6078_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6078_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6078_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6078_0_0_0/* byval_arg */
	, &IEnumerator_1_t6078_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6078_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_79.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2920_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_79MethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m15053_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReconstructionFromTargetBehaviour_t47_m33165_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionFromTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionFromTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisReconstructionFromTargetBehaviour_t47_m33165(__this, p0, method) (ReconstructionFromTargetBehaviour_t47 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2920____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2920_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2920, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2920____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2920_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2920, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2920_FieldInfos[] =
{
	&InternalEnumerator_1_t2920____array_0_FieldInfo,
	&InternalEnumerator_1_t2920____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15050_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2920____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2920_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15050_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2920____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2920_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15053_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2920_PropertyInfos[] =
{
	&InternalEnumerator_1_t2920____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2920____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2920_InternalEnumerator_1__ctor_m15049_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15049_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15049_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2920_InternalEnumerator_1__ctor_m15049_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15049_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15050_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15050_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2920_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15050_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15051_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15051_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15051_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15052_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15052_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2920_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15052_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15053_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15053_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2920_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetBehaviour_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15053_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2920_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15049_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15050_MethodInfo,
	&InternalEnumerator_1_Dispose_m15051_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15052_MethodInfo,
	&InternalEnumerator_1_get_Current_m15053_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m15052_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15051_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2920_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15050_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15052_MethodInfo,
	&InternalEnumerator_1_Dispose_m15051_MethodInfo,
	&InternalEnumerator_1_get_Current_m15053_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2920_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6078_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2920_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6078_il2cpp_TypeInfo, 7},
};
extern TypeInfo ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2920_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15053_MethodInfo/* Method Usage */,
	&ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisReconstructionFromTargetBehaviour_t47_m33165_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2920_0_0_0;
extern Il2CppType InternalEnumerator_1_t2920_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2920_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2920_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2920_MethodInfos/* methods */
	, InternalEnumerator_1_t2920_PropertyInfos/* properties */
	, InternalEnumerator_1_t2920_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2920_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2920_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2920_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2920_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2920_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2920_1_0_0/* this_arg */
	, InternalEnumerator_1_t2920_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2920_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2920_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2920)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7745_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m43481_MethodInfo;
static PropertyInfo ICollection_1_t7745____Count_PropertyInfo = 
{
	&ICollection_1_t7745_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43481_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43482_MethodInfo;
static PropertyInfo ICollection_1_t7745____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7745_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43482_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7745_PropertyInfos[] =
{
	&ICollection_1_t7745____Count_PropertyInfo,
	&ICollection_1_t7745____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43481_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43481_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7745_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43481_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43482_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43482_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7745_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43482_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
static ParameterInfo ICollection_1_t7745_ICollection_1_Add_m43483_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t47_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43483_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43483_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7745_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7745_ICollection_1_Add_m43483_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43483_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43484_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43484_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7745_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43484_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
static ParameterInfo ICollection_1_t7745_ICollection_1_Contains_m43485_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t47_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43485_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43485_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7745_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7745_ICollection_1_Contains_m43485_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43485_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviourU5BU5D_t5377_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviourU5BU5D_t5377_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7745_ICollection_1_CopyTo_m43486_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviourU5BU5D_t5377_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43486_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43486_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7745_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7745_ICollection_1_CopyTo_m43486_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43486_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
static ParameterInfo ICollection_1_t7745_ICollection_1_Remove_m43487_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t47_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43487_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43487_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7745_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7745_ICollection_1_Remove_m43487_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43487_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7745_MethodInfos[] =
{
	&ICollection_1_get_Count_m43481_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43482_MethodInfo,
	&ICollection_1_Add_m43483_MethodInfo,
	&ICollection_1_Clear_m43484_MethodInfo,
	&ICollection_1_Contains_m43485_MethodInfo,
	&ICollection_1_CopyTo_m43486_MethodInfo,
	&ICollection_1_Remove_m43487_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7747_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7745_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7747_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7745_0_0_0;
extern Il2CppType ICollection_1_t7745_1_0_0;
struct ICollection_1_t7745;
extern Il2CppGenericClass ICollection_1_t7745_GenericClass;
TypeInfo ICollection_1_t7745_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7745_MethodInfos/* methods */
	, ICollection_1_t7745_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7745_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7745_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7745_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7745_0_0_0/* byval_arg */
	, &ICollection_1_t7745_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7745_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetBehaviour>
extern Il2CppType IEnumerator_1_t6078_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43488_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43488_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7747_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6078_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43488_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7747_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43488_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7747_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7747_0_0_0;
extern Il2CppType IEnumerable_1_t7747_1_0_0;
struct IEnumerable_1_t7747;
extern Il2CppGenericClass IEnumerable_1_t7747_GenericClass;
TypeInfo IEnumerable_1_t7747_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7747_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7747_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7747_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7747_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7747_0_0_0/* byval_arg */
	, &IEnumerable_1_t7747_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7747_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7746_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>
extern MethodInfo IList_1_get_Item_m43489_MethodInfo;
extern MethodInfo IList_1_set_Item_m43490_MethodInfo;
static PropertyInfo IList_1_t7746____Item_PropertyInfo = 
{
	&IList_1_t7746_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43489_MethodInfo/* get */
	, &IList_1_set_Item_m43490_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7746_PropertyInfos[] =
{
	&IList_1_t7746____Item_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
static ParameterInfo IList_1_t7746_IList_1_IndexOf_m43491_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t47_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43491_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43491_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7746_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7746_IList_1_IndexOf_m43491_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43491_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
static ParameterInfo IList_1_t7746_IList_1_Insert_m43492_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t47_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43492_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43492_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7746_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7746_IList_1_Insert_m43492_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43492_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7746_IList_1_RemoveAt_m43493_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43493_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43493_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7746_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7746_IList_1_RemoveAt_m43493_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43493_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7746_IList_1_get_Item_m43489_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43489_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43489_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7746_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetBehaviour_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7746_IList_1_get_Item_m43489_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43489_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
static ParameterInfo IList_1_t7746_IList_1_set_Item_m43490_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t47_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43490_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43490_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7746_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7746_IList_1_set_Item_m43490_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43490_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7746_MethodInfos[] =
{
	&IList_1_IndexOf_m43491_MethodInfo,
	&IList_1_Insert_m43492_MethodInfo,
	&IList_1_RemoveAt_m43493_MethodInfo,
	&IList_1_get_Item_m43489_MethodInfo,
	&IList_1_set_Item_m43490_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7746_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7745_il2cpp_TypeInfo,
	&IEnumerable_1_t7747_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7746_0_0_0;
extern Il2CppType IList_1_t7746_1_0_0;
struct IList_1_t7746;
extern Il2CppGenericClass IList_1_t7746_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7746_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7746_MethodInfos/* methods */
	, IList_1_t7746_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7746_il2cpp_TypeInfo/* element_class */
	, IList_1_t7746_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7746_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7746_0_0_0/* byval_arg */
	, &IList_1_t7746_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7746_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7748_il2cpp_TypeInfo;

// Vuforia.ReconstructionFromTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43494_MethodInfo;
static PropertyInfo ICollection_1_t7748____Count_PropertyInfo = 
{
	&ICollection_1_t7748_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43494_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43495_MethodInfo;
static PropertyInfo ICollection_1_t7748____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7748_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43495_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7748_PropertyInfos[] =
{
	&ICollection_1_t7748____Count_PropertyInfo,
	&ICollection_1_t7748____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43494_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43494_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7748_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43494_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43495_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43495_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7748_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43495_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t48_0_0_0;
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t48_0_0_0;
static ParameterInfo ICollection_1_t7748_ICollection_1_Add_m43496_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43496_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43496_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7748_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7748_ICollection_1_Add_m43496_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43496_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43497_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43497_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7748_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43497_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t48_0_0_0;
static ParameterInfo ICollection_1_t7748_ICollection_1_Contains_m43498_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43498_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43498_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7748_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7748_ICollection_1_Contains_m43498_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43498_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetAbstractBehaviourU5BU5D_t5641_0_0_0;
extern Il2CppType ReconstructionFromTargetAbstractBehaviourU5BU5D_t5641_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7748_ICollection_1_CopyTo_m43499_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviourU5BU5D_t5641_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43499_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43499_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7748_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7748_ICollection_1_CopyTo_m43499_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43499_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t48_0_0_0;
static ParameterInfo ICollection_1_t7748_ICollection_1_Remove_m43500_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43500_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43500_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7748_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7748_ICollection_1_Remove_m43500_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43500_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7748_MethodInfos[] =
{
	&ICollection_1_get_Count_m43494_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43495_MethodInfo,
	&ICollection_1_Add_m43496_MethodInfo,
	&ICollection_1_Clear_m43497_MethodInfo,
	&ICollection_1_Contains_m43498_MethodInfo,
	&ICollection_1_CopyTo_m43499_MethodInfo,
	&ICollection_1_Remove_m43500_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7750_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7748_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7750_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7748_0_0_0;
extern Il2CppType ICollection_1_t7748_1_0_0;
struct ICollection_1_t7748;
extern Il2CppGenericClass ICollection_1_t7748_GenericClass;
TypeInfo ICollection_1_t7748_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7748_MethodInfos/* methods */
	, ICollection_1_t7748_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7748_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7748_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7748_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7748_0_0_0/* byval_arg */
	, &ICollection_1_t7748_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7748_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6080_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43501_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43501_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7750_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6080_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43501_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7750_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43501_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7750_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7750_0_0_0;
extern Il2CppType IEnumerable_1_t7750_1_0_0;
struct IEnumerable_1_t7750;
extern Il2CppGenericClass IEnumerable_1_t7750_GenericClass;
TypeInfo IEnumerable_1_t7750_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7750_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7750_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7750_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7750_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7750_0_0_0/* byval_arg */
	, &IEnumerable_1_t7750_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7750_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6080_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43502_MethodInfo;
static PropertyInfo IEnumerator_1_t6080____Current_PropertyInfo = 
{
	&IEnumerator_1_t6080_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43502_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6080_PropertyInfos[] =
{
	&IEnumerator_1_t6080____Current_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t48_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43502_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43502_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6080_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetAbstractBehaviour_t48_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43502_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6080_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43502_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6080_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6080_0_0_0;
extern Il2CppType IEnumerator_1_t6080_1_0_0;
struct IEnumerator_1_t6080;
extern Il2CppGenericClass IEnumerator_1_t6080_GenericClass;
TypeInfo IEnumerator_1_t6080_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6080_MethodInfos/* methods */
	, IEnumerator_1_t6080_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6080_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6080_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6080_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6080_0_0_0/* byval_arg */
	, &IEnumerator_1_t6080_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6080_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_80.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2921_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_80MethodDeclarations.h"

extern TypeInfo ReconstructionFromTargetAbstractBehaviour_t48_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15058_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReconstructionFromTargetAbstractBehaviour_t48_m33176_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionFromTargetAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionFromTargetAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisReconstructionFromTargetAbstractBehaviour_t48_m33176(__this, p0, method) (ReconstructionFromTargetAbstractBehaviour_t48 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2921____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2921, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2921____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2921, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2921_FieldInfos[] =
{
	&InternalEnumerator_1_t2921____array_0_FieldInfo,
	&InternalEnumerator_1_t2921____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15055_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2921____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2921_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15055_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2921____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2921_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15058_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2921_PropertyInfos[] =
{
	&InternalEnumerator_1_t2921____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2921____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2921_InternalEnumerator_1__ctor_m15054_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15054_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15054_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2921_InternalEnumerator_1__ctor_m15054_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15054_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15055_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15055_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15055_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15056_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15056_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15056_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15057_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15057_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15057_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t48_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15058_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15058_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetAbstractBehaviour_t48_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15058_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2921_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15054_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15055_MethodInfo,
	&InternalEnumerator_1_Dispose_m15056_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15057_MethodInfo,
	&InternalEnumerator_1_get_Current_m15058_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15057_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15056_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2921_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15055_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15057_MethodInfo,
	&InternalEnumerator_1_Dispose_m15056_MethodInfo,
	&InternalEnumerator_1_get_Current_m15058_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2921_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6080_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2921_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6080_il2cpp_TypeInfo, 7},
};
extern TypeInfo ReconstructionFromTargetAbstractBehaviour_t48_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2921_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15058_MethodInfo/* Method Usage */,
	&ReconstructionFromTargetAbstractBehaviour_t48_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisReconstructionFromTargetAbstractBehaviour_t48_m33176_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2921_0_0_0;
extern Il2CppType InternalEnumerator_1_t2921_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2921_GenericClass;
TypeInfo InternalEnumerator_1_t2921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2921_MethodInfos/* methods */
	, InternalEnumerator_1_t2921_PropertyInfos/* properties */
	, InternalEnumerator_1_t2921_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2921_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2921_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2921_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2921_1_0_0/* this_arg */
	, InternalEnumerator_1_t2921_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2921_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2921_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2921)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7749_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43503_MethodInfo;
extern MethodInfo IList_1_set_Item_m43504_MethodInfo;
static PropertyInfo IList_1_t7749____Item_PropertyInfo = 
{
	&IList_1_t7749_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43503_MethodInfo/* get */
	, &IList_1_set_Item_m43504_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7749_PropertyInfos[] =
{
	&IList_1_t7749____Item_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t48_0_0_0;
static ParameterInfo IList_1_t7749_IList_1_IndexOf_m43505_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43505_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43505_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7749_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7749_IList_1_IndexOf_m43505_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43505_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t48_0_0_0;
static ParameterInfo IList_1_t7749_IList_1_Insert_m43506_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43506_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43506_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7749_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7749_IList_1_Insert_m43506_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43506_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7749_IList_1_RemoveAt_m43507_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43507_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43507_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7749_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7749_IList_1_RemoveAt_m43507_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43507_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7749_IList_1_get_Item_m43503_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t48_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43503_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43503_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7749_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetAbstractBehaviour_t48_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7749_IList_1_get_Item_m43503_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43503_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t48_0_0_0;
static ParameterInfo IList_1_t7749_IList_1_set_Item_m43504_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43504_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43504_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7749_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7749_IList_1_set_Item_m43504_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43504_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7749_MethodInfos[] =
{
	&IList_1_IndexOf_m43505_MethodInfo,
	&IList_1_Insert_m43506_MethodInfo,
	&IList_1_RemoveAt_m43507_MethodInfo,
	&IList_1_get_Item_m43503_MethodInfo,
	&IList_1_set_Item_m43504_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7749_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7748_il2cpp_TypeInfo,
	&IEnumerable_1_t7750_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7749_0_0_0;
extern Il2CppType IList_1_t7749_1_0_0;
struct IList_1_t7749;
extern Il2CppGenericClass IList_1_t7749_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7749_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7749_MethodInfos/* methods */
	, IList_1_t7749_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7749_il2cpp_TypeInfo/* element_class */
	, IList_1_t7749_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7749_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7749_0_0_0/* byval_arg */
	, &IList_1_t7749_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7749_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_23.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2922_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_23MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_19.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2923_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_19MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15061_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15063_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2922____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2922_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2922, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2922_FieldInfos[] =
{
	&CachedInvokableCall_1_t2922____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2922_CachedInvokableCall_1__ctor_m15059_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t47_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15059_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15059_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2922_CachedInvokableCall_1__ctor_m15059_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15059_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2922_CachedInvokableCall_1_Invoke_m15060_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15060_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15060_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2922_CachedInvokableCall_1_Invoke_m15060_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15060_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2922_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15059_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15060_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15060_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15064_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2922_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15060_MethodInfo,
	&InvokableCall_1_Find_m15064_MethodInfo,
};
extern Il2CppType UnityAction_1_t2924_0_0_0;
extern TypeInfo UnityAction_1_t2924_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionFromTargetBehaviour_t47_m33186_MethodInfo;
extern TypeInfo ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15066_MethodInfo;
extern TypeInfo ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2922_RGCTXData[8] = 
{
	&UnityAction_1_t2924_0_0_0/* Type Usage */,
	&UnityAction_1_t2924_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionFromTargetBehaviour_t47_m33186_MethodInfo/* Method Usage */,
	&ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15066_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15061_MethodInfo/* Method Usage */,
	&ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15063_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2922_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2922_1_0_0;
struct CachedInvokableCall_1_t2922;
extern Il2CppGenericClass CachedInvokableCall_1_t2922_GenericClass;
TypeInfo CachedInvokableCall_1_t2922_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2922_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2922_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2923_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2922_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2922_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2922_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2922_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2922_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2922_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2922_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2922)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_26.h"
extern TypeInfo UnityAction_1_t2924_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_26MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.ReconstructionFromTargetBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.ReconstructionFromTargetBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionFromTargetBehaviour_t47_m33186(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
extern Il2CppType UnityAction_1_t2924_0_0_1;
FieldInfo InvokableCall_1_t2923____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2924_0_0_1/* type */
	, &InvokableCall_1_t2923_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2923, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2923_FieldInfos[] =
{
	&InvokableCall_1_t2923____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2923_InvokableCall_1__ctor_m15061_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15061_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15061_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2923_InvokableCall_1__ctor_m15061_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15061_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2924_0_0_0;
static ParameterInfo InvokableCall_1_t2923_InvokableCall_1__ctor_m15062_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2924_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15062_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15062_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2923_InvokableCall_1__ctor_m15062_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15062_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2923_InvokableCall_1_Invoke_m15063_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15063_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15063_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2923_InvokableCall_1_Invoke_m15063_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15063_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2923_InvokableCall_1_Find_m15064_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15064_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15064_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2923_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2923_InvokableCall_1_Find_m15064_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15064_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2923_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15061_MethodInfo,
	&InvokableCall_1__ctor_m15062_MethodInfo,
	&InvokableCall_1_Invoke_m15063_MethodInfo,
	&InvokableCall_1_Find_m15064_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2923_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15063_MethodInfo,
	&InvokableCall_1_Find_m15064_MethodInfo,
};
extern TypeInfo UnityAction_1_t2924_il2cpp_TypeInfo;
extern TypeInfo ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2923_RGCTXData[5] = 
{
	&UnityAction_1_t2924_0_0_0/* Type Usage */,
	&UnityAction_1_t2924_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionFromTargetBehaviour_t47_m33186_MethodInfo/* Method Usage */,
	&ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15066_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2923_0_0_0;
extern Il2CppType InvokableCall_1_t2923_1_0_0;
struct InvokableCall_1_t2923;
extern Il2CppGenericClass InvokableCall_1_t2923_GenericClass;
TypeInfo InvokableCall_1_t2923_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2923_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2923_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2923_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2923_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2923_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2923_0_0_0/* byval_arg */
	, &InvokableCall_1_t2923_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2923_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2923_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2923)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2924_UnityAction_1__ctor_m15065_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15065_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15065_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2924_UnityAction_1__ctor_m15065_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15065_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
static ParameterInfo UnityAction_1_t2924_UnityAction_1_Invoke_m15066_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t47_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15066_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15066_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2924_UnityAction_1_Invoke_m15066_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15066_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t47_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2924_UnityAction_1_BeginInvoke_m15067_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t47_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15067_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15067_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2924_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2924_UnityAction_1_BeginInvoke_m15067_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15067_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2924_UnityAction_1_EndInvoke_m15068_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15068_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15068_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2924_UnityAction_1_EndInvoke_m15068_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15068_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2924_MethodInfos[] =
{
	&UnityAction_1__ctor_m15065_MethodInfo,
	&UnityAction_1_Invoke_m15066_MethodInfo,
	&UnityAction_1_BeginInvoke_m15067_MethodInfo,
	&UnityAction_1_EndInvoke_m15068_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15067_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15068_MethodInfo;
static MethodInfo* UnityAction_1_t2924_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15066_MethodInfo,
	&UnityAction_1_BeginInvoke_m15067_MethodInfo,
	&UnityAction_1_EndInvoke_m15068_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2924_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2924_1_0_0;
struct UnityAction_1_t2924;
extern Il2CppGenericClass UnityAction_1_t2924_GenericClass;
TypeInfo UnityAction_1_t2924_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2924_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2924_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2924_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2924_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2924_0_0_0/* byval_arg */
	, &UnityAction_1_t2924_1_0_0/* this_arg */
	, UnityAction_1_t2924_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2924_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2924)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6082_il2cpp_TypeInfo;

// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43508_MethodInfo;
static PropertyInfo IEnumerator_1_t6082____Current_PropertyInfo = 
{
	&IEnumerator_1_t6082_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43508_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6082_PropertyInfos[] =
{
	&IEnumerator_1_t6082____Current_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43508_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43508_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6082_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerBehaviour_t49_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43508_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6082_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43508_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6082_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6082_0_0_0;
extern Il2CppType IEnumerator_1_t6082_1_0_0;
struct IEnumerator_1_t6082;
extern Il2CppGenericClass IEnumerator_1_t6082_GenericClass;
TypeInfo IEnumerator_1_t6082_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6082_MethodInfos/* methods */
	, IEnumerator_1_t6082_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6082_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6082_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6082_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6082_0_0_0/* byval_arg */
	, &IEnumerator_1_t6082_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6082_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_81.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2925_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_81MethodDeclarations.h"

extern TypeInfo SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15073_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSmartTerrainTrackerBehaviour_t49_m33188_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackerBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackerBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisSmartTerrainTrackerBehaviour_t49_m33188(__this, p0, method) (SmartTerrainTrackerBehaviour_t49 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2925____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2925_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2925, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2925____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2925_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2925, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2925_FieldInfos[] =
{
	&InternalEnumerator_1_t2925____array_0_FieldInfo,
	&InternalEnumerator_1_t2925____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15070_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2925____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2925_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15070_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2925____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2925_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15073_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2925_PropertyInfos[] =
{
	&InternalEnumerator_1_t2925____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2925____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2925_InternalEnumerator_1__ctor_m15069_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15069_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15069_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2925_InternalEnumerator_1__ctor_m15069_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15069_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15070_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15070_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2925_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15070_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15071_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15071_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15071_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15072_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15072_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2925_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15072_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15073_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15073_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2925_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerBehaviour_t49_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15073_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2925_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15069_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15070_MethodInfo,
	&InternalEnumerator_1_Dispose_m15071_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15072_MethodInfo,
	&InternalEnumerator_1_get_Current_m15073_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15072_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15071_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2925_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15070_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15072_MethodInfo,
	&InternalEnumerator_1_Dispose_m15071_MethodInfo,
	&InternalEnumerator_1_get_Current_m15073_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2925_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6082_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2925_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6082_il2cpp_TypeInfo, 7},
};
extern TypeInfo SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2925_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15073_MethodInfo/* Method Usage */,
	&SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSmartTerrainTrackerBehaviour_t49_m33188_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2925_0_0_0;
extern Il2CppType InternalEnumerator_1_t2925_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2925_GenericClass;
TypeInfo InternalEnumerator_1_t2925_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2925_MethodInfos/* methods */
	, InternalEnumerator_1_t2925_PropertyInfos/* properties */
	, InternalEnumerator_1_t2925_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2925_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2925_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2925_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2925_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2925_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2925_1_0_0/* this_arg */
	, InternalEnumerator_1_t2925_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2925_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2925_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2925)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7751_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>
extern MethodInfo ICollection_1_get_Count_m43509_MethodInfo;
static PropertyInfo ICollection_1_t7751____Count_PropertyInfo = 
{
	&ICollection_1_t7751_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43509_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43510_MethodInfo;
static PropertyInfo ICollection_1_t7751____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7751_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43510_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7751_PropertyInfos[] =
{
	&ICollection_1_t7751____Count_PropertyInfo,
	&ICollection_1_t7751____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43509_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43509_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7751_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43509_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43510_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43510_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7751_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43510_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
static ParameterInfo ICollection_1_t7751_ICollection_1_Add_m43511_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t49_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43511_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43511_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7751_ICollection_1_Add_m43511_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43511_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43512_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43512_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43512_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
static ParameterInfo ICollection_1_t7751_ICollection_1_Contains_m43513_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t49_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43513_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43513_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7751_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7751_ICollection_1_Contains_m43513_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43513_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviourU5BU5D_t5378_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviourU5BU5D_t5378_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7751_ICollection_1_CopyTo_m43514_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviourU5BU5D_t5378_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43514_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43514_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7751_ICollection_1_CopyTo_m43514_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43514_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
static ParameterInfo ICollection_1_t7751_ICollection_1_Remove_m43515_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t49_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43515_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43515_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7751_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7751_ICollection_1_Remove_m43515_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43515_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7751_MethodInfos[] =
{
	&ICollection_1_get_Count_m43509_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43510_MethodInfo,
	&ICollection_1_Add_m43511_MethodInfo,
	&ICollection_1_Clear_m43512_MethodInfo,
	&ICollection_1_Contains_m43513_MethodInfo,
	&ICollection_1_CopyTo_m43514_MethodInfo,
	&ICollection_1_Remove_m43515_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7753_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7751_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7753_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7751_0_0_0;
extern Il2CppType ICollection_1_t7751_1_0_0;
struct ICollection_1_t7751;
extern Il2CppGenericClass ICollection_1_t7751_GenericClass;
TypeInfo ICollection_1_t7751_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7751_MethodInfos/* methods */
	, ICollection_1_t7751_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7751_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7751_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7751_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7751_0_0_0/* byval_arg */
	, &ICollection_1_t7751_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7751_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerBehaviour>
extern Il2CppType IEnumerator_1_t6082_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43516_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43516_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7753_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6082_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43516_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7753_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43516_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7753_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7753_0_0_0;
extern Il2CppType IEnumerable_1_t7753_1_0_0;
struct IEnumerable_1_t7753;
extern Il2CppGenericClass IEnumerable_1_t7753_GenericClass;
TypeInfo IEnumerable_1_t7753_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7753_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7753_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7753_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7753_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7753_0_0_0/* byval_arg */
	, &IEnumerable_1_t7753_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7753_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7752_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>
extern MethodInfo IList_1_get_Item_m43517_MethodInfo;
extern MethodInfo IList_1_set_Item_m43518_MethodInfo;
static PropertyInfo IList_1_t7752____Item_PropertyInfo = 
{
	&IList_1_t7752_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43517_MethodInfo/* get */
	, &IList_1_set_Item_m43518_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7752_PropertyInfos[] =
{
	&IList_1_t7752____Item_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
static ParameterInfo IList_1_t7752_IList_1_IndexOf_m43519_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t49_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43519_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43519_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7752_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7752_IList_1_IndexOf_m43519_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43519_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
static ParameterInfo IList_1_t7752_IList_1_Insert_m43520_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t49_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43520_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43520_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7752_IList_1_Insert_m43520_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43520_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7752_IList_1_RemoveAt_m43521_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43521_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43521_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7752_IList_1_RemoveAt_m43521_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43521_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7752_IList_1_get_Item_m43517_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43517_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43517_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7752_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerBehaviour_t49_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7752_IList_1_get_Item_m43517_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43517_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
static ParameterInfo IList_1_t7752_IList_1_set_Item_m43518_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t49_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43518_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43518_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7752_IList_1_set_Item_m43518_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43518_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7752_MethodInfos[] =
{
	&IList_1_IndexOf_m43519_MethodInfo,
	&IList_1_Insert_m43520_MethodInfo,
	&IList_1_RemoveAt_m43521_MethodInfo,
	&IList_1_get_Item_m43517_MethodInfo,
	&IList_1_set_Item_m43518_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7752_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7751_il2cpp_TypeInfo,
	&IEnumerable_1_t7753_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7752_0_0_0;
extern Il2CppType IList_1_t7752_1_0_0;
struct IList_1_t7752;
extern Il2CppGenericClass IList_1_t7752_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7752_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7752_MethodInfos/* methods */
	, IList_1_t7752_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7752_il2cpp_TypeInfo/* element_class */
	, IList_1_t7752_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7752_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7752_0_0_0/* byval_arg */
	, &IList_1_t7752_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7752_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7754_il2cpp_TypeInfo;

// Vuforia.SmartTerrainTrackerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43522_MethodInfo;
static PropertyInfo ICollection_1_t7754____Count_PropertyInfo = 
{
	&ICollection_1_t7754_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43522_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43523_MethodInfo;
static PropertyInfo ICollection_1_t7754____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7754_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43523_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7754_PropertyInfos[] =
{
	&ICollection_1_t7754____Count_PropertyInfo,
	&ICollection_1_t7754____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43522_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43522_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7754_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43522_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43523_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43523_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7754_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43523_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t50_0_0_0;
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t50_0_0_0;
static ParameterInfo ICollection_1_t7754_ICollection_1_Add_m43524_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43524_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43524_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7754_ICollection_1_Add_m43524_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43524_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43525_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43525_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43525_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t50_0_0_0;
static ParameterInfo ICollection_1_t7754_ICollection_1_Contains_m43526_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43526_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43526_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7754_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7754_ICollection_1_Contains_m43526_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43526_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerAbstractBehaviourU5BU5D_t5642_0_0_0;
extern Il2CppType SmartTerrainTrackerAbstractBehaviourU5BU5D_t5642_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7754_ICollection_1_CopyTo_m43527_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviourU5BU5D_t5642_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43527_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43527_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7754_ICollection_1_CopyTo_m43527_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43527_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t50_0_0_0;
static ParameterInfo ICollection_1_t7754_ICollection_1_Remove_m43528_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43528_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43528_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7754_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7754_ICollection_1_Remove_m43528_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43528_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7754_MethodInfos[] =
{
	&ICollection_1_get_Count_m43522_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43523_MethodInfo,
	&ICollection_1_Add_m43524_MethodInfo,
	&ICollection_1_Clear_m43525_MethodInfo,
	&ICollection_1_Contains_m43526_MethodInfo,
	&ICollection_1_CopyTo_m43527_MethodInfo,
	&ICollection_1_Remove_m43528_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7756_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7754_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7756_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7754_0_0_0;
extern Il2CppType ICollection_1_t7754_1_0_0;
struct ICollection_1_t7754;
extern Il2CppGenericClass ICollection_1_t7754_GenericClass;
TypeInfo ICollection_1_t7754_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7754_MethodInfos/* methods */
	, ICollection_1_t7754_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7754_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7754_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7754_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7754_0_0_0/* byval_arg */
	, &ICollection_1_t7754_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7754_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6084_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43529_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43529_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7756_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6084_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43529_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7756_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43529_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7756_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7756_0_0_0;
extern Il2CppType IEnumerable_1_t7756_1_0_0;
struct IEnumerable_1_t7756;
extern Il2CppGenericClass IEnumerable_1_t7756_GenericClass;
TypeInfo IEnumerable_1_t7756_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7756_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7756_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7756_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7756_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7756_0_0_0/* byval_arg */
	, &IEnumerable_1_t7756_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7756_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6084_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43530_MethodInfo;
static PropertyInfo IEnumerator_1_t6084____Current_PropertyInfo = 
{
	&IEnumerator_1_t6084_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43530_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6084_PropertyInfos[] =
{
	&IEnumerator_1_t6084____Current_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t50_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43530_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43530_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6084_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerAbstractBehaviour_t50_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43530_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6084_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43530_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6084_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6084_0_0_0;
extern Il2CppType IEnumerator_1_t6084_1_0_0;
struct IEnumerator_1_t6084;
extern Il2CppGenericClass IEnumerator_1_t6084_GenericClass;
TypeInfo IEnumerator_1_t6084_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6084_MethodInfos/* methods */
	, IEnumerator_1_t6084_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6084_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6084_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6084_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6084_0_0_0/* byval_arg */
	, &IEnumerator_1_t6084_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6084_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_82.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2926_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_82MethodDeclarations.h"

extern TypeInfo SmartTerrainTrackerAbstractBehaviour_t50_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15078_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSmartTerrainTrackerAbstractBehaviour_t50_m33199_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackerAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackerAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisSmartTerrainTrackerAbstractBehaviour_t50_m33199(__this, p0, method) (SmartTerrainTrackerAbstractBehaviour_t50 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2926____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2926, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2926____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2926, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2926_FieldInfos[] =
{
	&InternalEnumerator_1_t2926____array_0_FieldInfo,
	&InternalEnumerator_1_t2926____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15075_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2926____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2926_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15075_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2926____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2926_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15078_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2926_PropertyInfos[] =
{
	&InternalEnumerator_1_t2926____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2926____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2926_InternalEnumerator_1__ctor_m15074_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15074_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15074_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2926_InternalEnumerator_1__ctor_m15074_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15074_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15075_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15075_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15075_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15076_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15076_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15076_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15077_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15077_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15077_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t50_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15078_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15078_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerAbstractBehaviour_t50_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15078_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2926_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15074_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15075_MethodInfo,
	&InternalEnumerator_1_Dispose_m15076_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15077_MethodInfo,
	&InternalEnumerator_1_get_Current_m15078_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15077_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15076_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2926_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15075_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15077_MethodInfo,
	&InternalEnumerator_1_Dispose_m15076_MethodInfo,
	&InternalEnumerator_1_get_Current_m15078_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2926_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6084_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2926_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6084_il2cpp_TypeInfo, 7},
};
extern TypeInfo SmartTerrainTrackerAbstractBehaviour_t50_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2926_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15078_MethodInfo/* Method Usage */,
	&SmartTerrainTrackerAbstractBehaviour_t50_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSmartTerrainTrackerAbstractBehaviour_t50_m33199_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2926_0_0_0;
extern Il2CppType InternalEnumerator_1_t2926_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2926_GenericClass;
TypeInfo InternalEnumerator_1_t2926_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2926_MethodInfos/* methods */
	, InternalEnumerator_1_t2926_PropertyInfos/* properties */
	, InternalEnumerator_1_t2926_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2926_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2926_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2926_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2926_1_0_0/* this_arg */
	, InternalEnumerator_1_t2926_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2926_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2926_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2926)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7755_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43531_MethodInfo;
extern MethodInfo IList_1_set_Item_m43532_MethodInfo;
static PropertyInfo IList_1_t7755____Item_PropertyInfo = 
{
	&IList_1_t7755_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43531_MethodInfo/* get */
	, &IList_1_set_Item_m43532_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7755_PropertyInfos[] =
{
	&IList_1_t7755____Item_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t50_0_0_0;
static ParameterInfo IList_1_t7755_IList_1_IndexOf_m43533_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43533_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43533_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7755_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7755_IList_1_IndexOf_m43533_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43533_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t50_0_0_0;
static ParameterInfo IList_1_t7755_IList_1_Insert_m43534_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43534_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43534_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7755_IList_1_Insert_m43534_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43534_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7755_IList_1_RemoveAt_m43535_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43535_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43535_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7755_IList_1_RemoveAt_m43535_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43535_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7755_IList_1_get_Item_m43531_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t50_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43531_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43531_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7755_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerAbstractBehaviour_t50_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7755_IList_1_get_Item_m43531_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43531_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t50_0_0_0;
static ParameterInfo IList_1_t7755_IList_1_set_Item_m43532_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43532_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43532_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7755_IList_1_set_Item_m43532_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43532_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7755_MethodInfos[] =
{
	&IList_1_IndexOf_m43533_MethodInfo,
	&IList_1_Insert_m43534_MethodInfo,
	&IList_1_RemoveAt_m43535_MethodInfo,
	&IList_1_get_Item_m43531_MethodInfo,
	&IList_1_set_Item_m43532_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7755_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7754_il2cpp_TypeInfo,
	&IEnumerable_1_t7756_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7755_0_0_0;
extern Il2CppType IList_1_t7755_1_0_0;
struct IList_1_t7755;
extern Il2CppGenericClass IList_1_t7755_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7755_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7755_MethodInfos/* methods */
	, IList_1_t7755_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7755_il2cpp_TypeInfo/* element_class */
	, IList_1_t7755_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7755_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7755_0_0_0/* byval_arg */
	, &IList_1_t7755_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7755_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7757_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
extern MethodInfo ICollection_1_get_Count_m43536_MethodInfo;
static PropertyInfo ICollection_1_t7757____Count_PropertyInfo = 
{
	&ICollection_1_t7757_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43536_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43537_MethodInfo;
static PropertyInfo ICollection_1_t7757____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7757_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43537_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7757_PropertyInfos[] =
{
	&ICollection_1_t7757____Count_PropertyInfo,
	&ICollection_1_t7757____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43536_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43536_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7757_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43536_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43537_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43537_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7757_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43537_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t164_0_0_0;
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t164_0_0_0;
static ParameterInfo ICollection_1_t7757_ICollection_1_Add_m43538_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t164_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43538_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43538_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7757_ICollection_1_Add_m43538_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43538_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43539_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43539_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43539_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t164_0_0_0;
static ParameterInfo ICollection_1_t7757_ICollection_1_Contains_m43540_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t164_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43540_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43540_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7757_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7757_ICollection_1_Contains_m43540_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43540_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSmartTerrainTrackerBehaviourU5BU5D_t5643_0_0_0;
extern Il2CppType IEditorSmartTerrainTrackerBehaviourU5BU5D_t5643_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7757_ICollection_1_CopyTo_m43541_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviourU5BU5D_t5643_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43541_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43541_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7757_ICollection_1_CopyTo_m43541_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43541_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t164_0_0_0;
static ParameterInfo ICollection_1_t7757_ICollection_1_Remove_m43542_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t164_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43542_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43542_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7757_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7757_ICollection_1_Remove_m43542_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43542_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7757_MethodInfos[] =
{
	&ICollection_1_get_Count_m43536_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43537_MethodInfo,
	&ICollection_1_Add_m43538_MethodInfo,
	&ICollection_1_Clear_m43539_MethodInfo,
	&ICollection_1_Contains_m43540_MethodInfo,
	&ICollection_1_CopyTo_m43541_MethodInfo,
	&ICollection_1_Remove_m43542_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7759_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7757_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7759_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7757_0_0_0;
extern Il2CppType ICollection_1_t7757_1_0_0;
struct ICollection_1_t7757;
extern Il2CppGenericClass ICollection_1_t7757_GenericClass;
TypeInfo ICollection_1_t7757_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7757_MethodInfos/* methods */
	, ICollection_1_t7757_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7757_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7757_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7757_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7757_0_0_0/* byval_arg */
	, &ICollection_1_t7757_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7757_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
extern Il2CppType IEnumerator_1_t6086_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43543_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43543_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7759_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6086_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43543_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7759_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43543_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7759_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7759_0_0_0;
extern Il2CppType IEnumerable_1_t7759_1_0_0;
struct IEnumerable_1_t7759;
extern Il2CppGenericClass IEnumerable_1_t7759_GenericClass;
TypeInfo IEnumerable_1_t7759_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7759_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7759_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7759_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7759_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7759_0_0_0/* byval_arg */
	, &IEnumerable_1_t7759_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7759_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6086_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43544_MethodInfo;
static PropertyInfo IEnumerator_1_t6086____Current_PropertyInfo = 
{
	&IEnumerator_1_t6086_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43544_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6086_PropertyInfos[] =
{
	&IEnumerator_1_t6086____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t164_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43544_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43544_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6086_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSmartTerrainTrackerBehaviour_t164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43544_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6086_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43544_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6086_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6086_0_0_0;
extern Il2CppType IEnumerator_1_t6086_1_0_0;
struct IEnumerator_1_t6086;
extern Il2CppGenericClass IEnumerator_1_t6086_GenericClass;
TypeInfo IEnumerator_1_t6086_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6086_MethodInfos/* methods */
	, IEnumerator_1_t6086_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6086_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6086_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6086_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6086_0_0_0/* byval_arg */
	, &IEnumerator_1_t6086_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6086_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_83.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2927_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_83MethodDeclarations.h"

extern TypeInfo IEditorSmartTerrainTrackerBehaviour_t164_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15083_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorSmartTerrainTrackerBehaviour_t164_m33210_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorSmartTerrainTrackerBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorSmartTerrainTrackerBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorSmartTerrainTrackerBehaviour_t164_m33210(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2927____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2927, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2927____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2927, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2927_FieldInfos[] =
{
	&InternalEnumerator_1_t2927____array_0_FieldInfo,
	&InternalEnumerator_1_t2927____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2927____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2927_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2927____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2927_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15083_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2927_PropertyInfos[] =
{
	&InternalEnumerator_1_t2927____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2927____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2927_InternalEnumerator_1__ctor_m15079_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15079_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15079_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2927_InternalEnumerator_1__ctor_m15079_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15079_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15081_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15081_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15081_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15082_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15082_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15082_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t164_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15083_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15083_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSmartTerrainTrackerBehaviour_t164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15083_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2927_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15079_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_MethodInfo,
	&InternalEnumerator_1_Dispose_m15081_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15082_MethodInfo,
	&InternalEnumerator_1_get_Current_m15083_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15082_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15081_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2927_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15082_MethodInfo,
	&InternalEnumerator_1_Dispose_m15081_MethodInfo,
	&InternalEnumerator_1_get_Current_m15083_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2927_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6086_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2927_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6086_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorSmartTerrainTrackerBehaviour_t164_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2927_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15083_MethodInfo/* Method Usage */,
	&IEditorSmartTerrainTrackerBehaviour_t164_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorSmartTerrainTrackerBehaviour_t164_m33210_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2927_0_0_0;
extern Il2CppType InternalEnumerator_1_t2927_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2927_GenericClass;
TypeInfo InternalEnumerator_1_t2927_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2927_MethodInfos/* methods */
	, InternalEnumerator_1_t2927_PropertyInfos/* properties */
	, InternalEnumerator_1_t2927_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2927_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2927_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2927_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2927_1_0_0/* this_arg */
	, InternalEnumerator_1_t2927_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2927_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2927_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2927)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7758_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
extern MethodInfo IList_1_get_Item_m43545_MethodInfo;
extern MethodInfo IList_1_set_Item_m43546_MethodInfo;
static PropertyInfo IList_1_t7758____Item_PropertyInfo = 
{
	&IList_1_t7758_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43545_MethodInfo/* get */
	, &IList_1_set_Item_m43546_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7758_PropertyInfos[] =
{
	&IList_1_t7758____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t164_0_0_0;
static ParameterInfo IList_1_t7758_IList_1_IndexOf_m43547_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t164_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43547_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43547_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7758_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7758_IList_1_IndexOf_m43547_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43547_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t164_0_0_0;
static ParameterInfo IList_1_t7758_IList_1_Insert_m43548_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t164_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43548_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43548_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7758_IList_1_Insert_m43548_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43548_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7758_IList_1_RemoveAt_m43549_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43549_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43549_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7758_IList_1_RemoveAt_m43549_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43549_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7758_IList_1_get_Item_m43545_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t164_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43545_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43545_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7758_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSmartTerrainTrackerBehaviour_t164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7758_IList_1_get_Item_m43545_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43545_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t164_0_0_0;
static ParameterInfo IList_1_t7758_IList_1_set_Item_m43546_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t164_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43546_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43546_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7758_IList_1_set_Item_m43546_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43546_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7758_MethodInfos[] =
{
	&IList_1_IndexOf_m43547_MethodInfo,
	&IList_1_Insert_m43548_MethodInfo,
	&IList_1_RemoveAt_m43549_MethodInfo,
	&IList_1_get_Item_m43545_MethodInfo,
	&IList_1_set_Item_m43546_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7758_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7757_il2cpp_TypeInfo,
	&IEnumerable_1_t7759_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7758_0_0_0;
extern Il2CppType IList_1_t7758_1_0_0;
struct IList_1_t7758;
extern Il2CppGenericClass IList_1_t7758_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7758_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7758_MethodInfos/* methods */
	, IList_1_t7758_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7758_il2cpp_TypeInfo/* element_class */
	, IList_1_t7758_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7758_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7758_0_0_0/* byval_arg */
	, &IList_1_t7758_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7758_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_24.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2928_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_24MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_20.h"
extern TypeInfo InvokableCall_1_t2929_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_20MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15086_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15088_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2928____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2928_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2928, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2928_FieldInfos[] =
{
	&CachedInvokableCall_1_t2928____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2928_CachedInvokableCall_1__ctor_m15084_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t49_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15084_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15084_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2928_CachedInvokableCall_1__ctor_m15084_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15084_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2928_CachedInvokableCall_1_Invoke_m15085_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15085_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15085_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2928_CachedInvokableCall_1_Invoke_m15085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15085_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2928_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15084_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15085_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15085_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15089_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2928_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15085_MethodInfo,
	&InvokableCall_1_Find_m15089_MethodInfo,
};
extern Il2CppType UnityAction_1_t2930_0_0_0;
extern TypeInfo UnityAction_1_t2930_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSmartTerrainTrackerBehaviour_t49_m33220_MethodInfo;
extern TypeInfo SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15091_MethodInfo;
extern TypeInfo SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2928_RGCTXData[8] = 
{
	&UnityAction_1_t2930_0_0_0/* Type Usage */,
	&UnityAction_1_t2930_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSmartTerrainTrackerBehaviour_t49_m33220_MethodInfo/* Method Usage */,
	&SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15091_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15086_MethodInfo/* Method Usage */,
	&SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15088_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2928_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2928_1_0_0;
struct CachedInvokableCall_1_t2928;
extern Il2CppGenericClass CachedInvokableCall_1_t2928_GenericClass;
TypeInfo CachedInvokableCall_1_t2928_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2928_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2928_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2929_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2928_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2928_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2928_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2928_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2928_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2928_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2928_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2928)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_27.h"
extern TypeInfo UnityAction_1_t2930_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_27MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.SmartTerrainTrackerBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.SmartTerrainTrackerBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSmartTerrainTrackerBehaviour_t49_m33220(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
extern Il2CppType UnityAction_1_t2930_0_0_1;
FieldInfo InvokableCall_1_t2929____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2930_0_0_1/* type */
	, &InvokableCall_1_t2929_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2929, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2929_FieldInfos[] =
{
	&InvokableCall_1_t2929____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2929_InvokableCall_1__ctor_m15086_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15086_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15086_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2929_InvokableCall_1__ctor_m15086_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15086_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2930_0_0_0;
static ParameterInfo InvokableCall_1_t2929_InvokableCall_1__ctor_m15087_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2930_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15087_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15087_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2929_InvokableCall_1__ctor_m15087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15087_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2929_InvokableCall_1_Invoke_m15088_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15088_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15088_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2929_InvokableCall_1_Invoke_m15088_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15088_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2929_InvokableCall_1_Find_m15089_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15089_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15089_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2929_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2929_InvokableCall_1_Find_m15089_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15089_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2929_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15086_MethodInfo,
	&InvokableCall_1__ctor_m15087_MethodInfo,
	&InvokableCall_1_Invoke_m15088_MethodInfo,
	&InvokableCall_1_Find_m15089_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2929_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15088_MethodInfo,
	&InvokableCall_1_Find_m15089_MethodInfo,
};
extern TypeInfo UnityAction_1_t2930_il2cpp_TypeInfo;
extern TypeInfo SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2929_RGCTXData[5] = 
{
	&UnityAction_1_t2930_0_0_0/* Type Usage */,
	&UnityAction_1_t2930_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSmartTerrainTrackerBehaviour_t49_m33220_MethodInfo/* Method Usage */,
	&SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15091_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2929_0_0_0;
extern Il2CppType InvokableCall_1_t2929_1_0_0;
struct InvokableCall_1_t2929;
extern Il2CppGenericClass InvokableCall_1_t2929_GenericClass;
TypeInfo InvokableCall_1_t2929_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2929_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2929_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2929_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2929_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2929_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2929_0_0_0/* byval_arg */
	, &InvokableCall_1_t2929_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2929_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2929_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2929)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2930_UnityAction_1__ctor_m15090_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15090_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15090_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2930_UnityAction_1__ctor_m15090_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15090_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
static ParameterInfo UnityAction_1_t2930_UnityAction_1_Invoke_m15091_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t49_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15091_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15091_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2930_UnityAction_1_Invoke_m15091_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15091_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t49_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2930_UnityAction_1_BeginInvoke_m15092_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t49_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15092_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15092_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2930_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2930_UnityAction_1_BeginInvoke_m15092_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15092_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2930_UnityAction_1_EndInvoke_m15093_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15093_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15093_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2930_UnityAction_1_EndInvoke_m15093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15093_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2930_MethodInfos[] =
{
	&UnityAction_1__ctor_m15090_MethodInfo,
	&UnityAction_1_Invoke_m15091_MethodInfo,
	&UnityAction_1_BeginInvoke_m15092_MethodInfo,
	&UnityAction_1_EndInvoke_m15093_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15092_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15093_MethodInfo;
static MethodInfo* UnityAction_1_t2930_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15091_MethodInfo,
	&UnityAction_1_BeginInvoke_m15092_MethodInfo,
	&UnityAction_1_EndInvoke_m15093_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2930_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2930_1_0_0;
struct UnityAction_1_t2930;
extern Il2CppGenericClass UnityAction_1_t2930_GenericClass;
TypeInfo UnityAction_1_t2930_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2930_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2930_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2930_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2930_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2930_0_0_0/* byval_arg */
	, &UnityAction_1_t2930_1_0_0/* this_arg */
	, UnityAction_1_t2930_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2930_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2930)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6088_il2cpp_TypeInfo;

// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43550_MethodInfo;
static PropertyInfo IEnumerator_1_t6088____Current_PropertyInfo = 
{
	&IEnumerator_1_t6088_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43550_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6088_PropertyInfos[] =
{
	&IEnumerator_1_t6088____Current_PropertyInfo,
	NULL
};
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43550_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43550_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6088_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceBehaviour_t13_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43550_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6088_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43550_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6088_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6088_0_0_0;
extern Il2CppType IEnumerator_1_t6088_1_0_0;
struct IEnumerator_1_t6088;
extern Il2CppGenericClass IEnumerator_1_t6088_GenericClass;
TypeInfo IEnumerator_1_t6088_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6088_MethodInfos/* methods */
	, IEnumerator_1_t6088_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6088_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6088_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6088_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6088_0_0_0/* byval_arg */
	, &IEnumerator_1_t6088_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6088_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_84.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2931_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_84MethodDeclarations.h"

extern TypeInfo SurfaceBehaviour_t13_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15098_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSurfaceBehaviour_t13_m33222_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.SurfaceBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.SurfaceBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisSurfaceBehaviour_t13_m33222(__this, p0, method) (SurfaceBehaviour_t13 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2931____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2931_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2931, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2931____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2931_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2931, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2931_FieldInfos[] =
{
	&InternalEnumerator_1_t2931____array_0_FieldInfo,
	&InternalEnumerator_1_t2931____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15095_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2931____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2931_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15095_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2931____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2931_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15098_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2931_PropertyInfos[] =
{
	&InternalEnumerator_1_t2931____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2931____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2931_InternalEnumerator_1__ctor_m15094_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15094_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15094_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2931_InternalEnumerator_1__ctor_m15094_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15094_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15095_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15095_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2931_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15095_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15096_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15096_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15096_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15097_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15097_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2931_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15097_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15098_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15098_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2931_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceBehaviour_t13_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15098_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2931_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15094_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15095_MethodInfo,
	&InternalEnumerator_1_Dispose_m15096_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15097_MethodInfo,
	&InternalEnumerator_1_get_Current_m15098_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15097_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15096_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2931_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15095_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15097_MethodInfo,
	&InternalEnumerator_1_Dispose_m15096_MethodInfo,
	&InternalEnumerator_1_get_Current_m15098_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2931_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6088_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2931_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6088_il2cpp_TypeInfo, 7},
};
extern TypeInfo SurfaceBehaviour_t13_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2931_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15098_MethodInfo/* Method Usage */,
	&SurfaceBehaviour_t13_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSurfaceBehaviour_t13_m33222_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2931_0_0_0;
extern Il2CppType InternalEnumerator_1_t2931_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2931_GenericClass;
TypeInfo InternalEnumerator_1_t2931_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2931_MethodInfos/* methods */
	, InternalEnumerator_1_t2931_PropertyInfos/* properties */
	, InternalEnumerator_1_t2931_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2931_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2931_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2931_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2931_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2931_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2931_1_0_0/* this_arg */
	, InternalEnumerator_1_t2931_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2931_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2931_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2931)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7760_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>
extern MethodInfo ICollection_1_get_Count_m43551_MethodInfo;
static PropertyInfo ICollection_1_t7760____Count_PropertyInfo = 
{
	&ICollection_1_t7760_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43551_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43552_MethodInfo;
static PropertyInfo ICollection_1_t7760____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7760_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43552_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7760_PropertyInfos[] =
{
	&ICollection_1_t7760____Count_PropertyInfo,
	&ICollection_1_t7760____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43551_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43551_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7760_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43551_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43552_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43552_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7760_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43552_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
static ParameterInfo ICollection_1_t7760_ICollection_1_Add_m43553_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t13_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43553_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43553_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7760_ICollection_1_Add_m43553_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43553_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43554_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43554_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43554_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
static ParameterInfo ICollection_1_t7760_ICollection_1_Contains_m43555_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t13_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43555_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43555_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7760_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7760_ICollection_1_Contains_m43555_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43555_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviourU5BU5D_t5379_0_0_0;
extern Il2CppType SurfaceBehaviourU5BU5D_t5379_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7760_ICollection_1_CopyTo_m43556_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviourU5BU5D_t5379_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43556_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43556_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7760_ICollection_1_CopyTo_m43556_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43556_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
static ParameterInfo ICollection_1_t7760_ICollection_1_Remove_m43557_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t13_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43557_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43557_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7760_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7760_ICollection_1_Remove_m43557_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43557_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7760_MethodInfos[] =
{
	&ICollection_1_get_Count_m43551_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43552_MethodInfo,
	&ICollection_1_Add_m43553_MethodInfo,
	&ICollection_1_Clear_m43554_MethodInfo,
	&ICollection_1_Contains_m43555_MethodInfo,
	&ICollection_1_CopyTo_m43556_MethodInfo,
	&ICollection_1_Remove_m43557_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7762_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7760_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7762_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7760_0_0_0;
extern Il2CppType ICollection_1_t7760_1_0_0;
struct ICollection_1_t7760;
extern Il2CppGenericClass ICollection_1_t7760_GenericClass;
TypeInfo ICollection_1_t7760_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7760_MethodInfos/* methods */
	, ICollection_1_t7760_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7760_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7760_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7760_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7760_0_0_0/* byval_arg */
	, &ICollection_1_t7760_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7760_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceBehaviour>
extern Il2CppType IEnumerator_1_t6088_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43558_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43558_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7762_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6088_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43558_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7762_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43558_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7762_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7762_0_0_0;
extern Il2CppType IEnumerable_1_t7762_1_0_0;
struct IEnumerable_1_t7762;
extern Il2CppGenericClass IEnumerable_1_t7762_GenericClass;
TypeInfo IEnumerable_1_t7762_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7762_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7762_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7762_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7762_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7762_0_0_0/* byval_arg */
	, &IEnumerable_1_t7762_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7762_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7761_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>
extern MethodInfo IList_1_get_Item_m43559_MethodInfo;
extern MethodInfo IList_1_set_Item_m43560_MethodInfo;
static PropertyInfo IList_1_t7761____Item_PropertyInfo = 
{
	&IList_1_t7761_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43559_MethodInfo/* get */
	, &IList_1_set_Item_m43560_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7761_PropertyInfos[] =
{
	&IList_1_t7761____Item_PropertyInfo,
	NULL
};
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
static ParameterInfo IList_1_t7761_IList_1_IndexOf_m43561_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t13_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43561_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43561_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7761_IList_1_IndexOf_m43561_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43561_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
static ParameterInfo IList_1_t7761_IList_1_Insert_m43562_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t13_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43562_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43562_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7761_IList_1_Insert_m43562_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43562_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7761_IList_1_RemoveAt_m43563_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43563_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43563_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7761_IList_1_RemoveAt_m43563_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43563_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7761_IList_1_get_Item_m43559_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43559_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43559_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceBehaviour_t13_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7761_IList_1_get_Item_m43559_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43559_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
static ParameterInfo IList_1_t7761_IList_1_set_Item_m43560_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t13_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43560_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43560_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7761_IList_1_set_Item_m43560_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43560_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7761_MethodInfos[] =
{
	&IList_1_IndexOf_m43561_MethodInfo,
	&IList_1_Insert_m43562_MethodInfo,
	&IList_1_RemoveAt_m43563_MethodInfo,
	&IList_1_get_Item_m43559_MethodInfo,
	&IList_1_set_Item_m43560_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7761_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7760_il2cpp_TypeInfo,
	&IEnumerable_1_t7762_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7761_0_0_0;
extern Il2CppType IList_1_t7761_1_0_0;
struct IList_1_t7761;
extern Il2CppGenericClass IList_1_t7761_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7761_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7761_MethodInfos/* methods */
	, IList_1_t7761_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7761_il2cpp_TypeInfo/* element_class */
	, IList_1_t7761_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7761_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7761_0_0_0/* byval_arg */
	, &IList_1_t7761_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7761_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7763_il2cpp_TypeInfo;

// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43564_MethodInfo;
static PropertyInfo ICollection_1_t7763____Count_PropertyInfo = 
{
	&ICollection_1_t7763_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43564_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43565_MethodInfo;
static PropertyInfo ICollection_1_t7763____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7763_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43565_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7763_PropertyInfos[] =
{
	&ICollection_1_t7763____Count_PropertyInfo,
	&ICollection_1_t7763____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43564_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43564_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7763_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43564_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43565_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43565_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7763_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43565_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceAbstractBehaviour_t51_0_0_0;
extern Il2CppType SurfaceAbstractBehaviour_t51_0_0_0;
static ParameterInfo ICollection_1_t7763_ICollection_1_Add_m43566_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t51_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43566_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43566_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7763_ICollection_1_Add_m43566_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43566_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43567_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43567_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43567_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceAbstractBehaviour_t51_0_0_0;
static ParameterInfo ICollection_1_t7763_ICollection_1_Contains_m43568_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t51_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43568_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43568_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7763_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7763_ICollection_1_Contains_m43568_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43568_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceAbstractBehaviourU5BU5D_t4267_0_0_0;
extern Il2CppType SurfaceAbstractBehaviourU5BU5D_t4267_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7763_ICollection_1_CopyTo_m43569_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviourU5BU5D_t4267_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43569_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43569_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7763_ICollection_1_CopyTo_m43569_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43569_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceAbstractBehaviour_t51_0_0_0;
static ParameterInfo ICollection_1_t7763_ICollection_1_Remove_m43570_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t51_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43570_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43570_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7763_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7763_ICollection_1_Remove_m43570_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43570_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7763_MethodInfos[] =
{
	&ICollection_1_get_Count_m43564_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43565_MethodInfo,
	&ICollection_1_Add_m43566_MethodInfo,
	&ICollection_1_Clear_m43567_MethodInfo,
	&ICollection_1_Contains_m43568_MethodInfo,
	&ICollection_1_CopyTo_m43569_MethodInfo,
	&ICollection_1_Remove_m43570_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7765_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7763_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7765_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7763_0_0_0;
extern Il2CppType ICollection_1_t7763_1_0_0;
struct ICollection_1_t7763;
extern Il2CppGenericClass ICollection_1_t7763_GenericClass;
TypeInfo ICollection_1_t7763_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7763_MethodInfos/* methods */
	, ICollection_1_t7763_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7763_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7763_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7763_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7763_0_0_0/* byval_arg */
	, &ICollection_1_t7763_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7763_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceAbstractBehaviour>
extern Il2CppType IEnumerator_1_t4277_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43571_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43571_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7765_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t4277_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43571_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7765_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43571_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7765_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7765_0_0_0;
extern Il2CppType IEnumerable_1_t7765_1_0_0;
struct IEnumerable_1_t7765;
extern Il2CppGenericClass IEnumerable_1_t7765_GenericClass;
TypeInfo IEnumerable_1_t7765_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7765_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7765_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7765_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7765_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7765_0_0_0/* byval_arg */
	, &IEnumerable_1_t7765_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7765_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t4277_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43572_MethodInfo;
static PropertyInfo IEnumerator_1_t4277____Current_PropertyInfo = 
{
	&IEnumerator_1_t4277_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43572_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t4277_PropertyInfos[] =
{
	&IEnumerator_1_t4277____Current_PropertyInfo,
	NULL
};
extern Il2CppType SurfaceAbstractBehaviour_t51_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43572_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43572_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t4277_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceAbstractBehaviour_t51_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43572_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t4277_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43572_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t4277_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t4277_0_0_0;
extern Il2CppType IEnumerator_1_t4277_1_0_0;
struct IEnumerator_1_t4277;
extern Il2CppGenericClass IEnumerator_1_t4277_GenericClass;
TypeInfo IEnumerator_1_t4277_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t4277_MethodInfos/* methods */
	, IEnumerator_1_t4277_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t4277_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t4277_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t4277_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t4277_0_0_0/* byval_arg */
	, &IEnumerator_1_t4277_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t4277_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_85.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2932_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_85MethodDeclarations.h"

extern TypeInfo SurfaceAbstractBehaviour_t51_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15103_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSurfaceAbstractBehaviour_t51_m33233_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.SurfaceAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.SurfaceAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisSurfaceAbstractBehaviour_t51_m33233(__this, p0, method) (SurfaceAbstractBehaviour_t51 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2932____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2932, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2932____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2932, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2932_FieldInfos[] =
{
	&InternalEnumerator_1_t2932____array_0_FieldInfo,
	&InternalEnumerator_1_t2932____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15100_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2932____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2932_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15100_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2932____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2932_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15103_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2932_PropertyInfos[] =
{
	&InternalEnumerator_1_t2932____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2932____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2932_InternalEnumerator_1__ctor_m15099_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15099_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15099_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2932_InternalEnumerator_1__ctor_m15099_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15099_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15100_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15100_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15100_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15101_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15101_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15101_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15102_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15102_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15102_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceAbstractBehaviour_t51_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15103_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15103_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceAbstractBehaviour_t51_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15103_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2932_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15099_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15100_MethodInfo,
	&InternalEnumerator_1_Dispose_m15101_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15102_MethodInfo,
	&InternalEnumerator_1_get_Current_m15103_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15102_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15101_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2932_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15100_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15102_MethodInfo,
	&InternalEnumerator_1_Dispose_m15101_MethodInfo,
	&InternalEnumerator_1_get_Current_m15103_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2932_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t4277_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2932_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t4277_il2cpp_TypeInfo, 7},
};
extern TypeInfo SurfaceAbstractBehaviour_t51_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2932_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15103_MethodInfo/* Method Usage */,
	&SurfaceAbstractBehaviour_t51_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSurfaceAbstractBehaviour_t51_m33233_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2932_0_0_0;
extern Il2CppType InternalEnumerator_1_t2932_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2932_GenericClass;
TypeInfo InternalEnumerator_1_t2932_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2932_MethodInfos/* methods */
	, InternalEnumerator_1_t2932_PropertyInfos/* properties */
	, InternalEnumerator_1_t2932_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2932_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2932_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2932_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2932_1_0_0/* this_arg */
	, InternalEnumerator_1_t2932_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2932_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2932_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2932)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7764_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43573_MethodInfo;
extern MethodInfo IList_1_set_Item_m43574_MethodInfo;
static PropertyInfo IList_1_t7764____Item_PropertyInfo = 
{
	&IList_1_t7764_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43573_MethodInfo/* get */
	, &IList_1_set_Item_m43574_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7764_PropertyInfos[] =
{
	&IList_1_t7764____Item_PropertyInfo,
	NULL
};
extern Il2CppType SurfaceAbstractBehaviour_t51_0_0_0;
static ParameterInfo IList_1_t7764_IList_1_IndexOf_m43575_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t51_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43575_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43575_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7764_IList_1_IndexOf_m43575_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43575_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SurfaceAbstractBehaviour_t51_0_0_0;
static ParameterInfo IList_1_t7764_IList_1_Insert_m43576_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t51_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43576_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43576_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7764_IList_1_Insert_m43576_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43576_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7764_IList_1_RemoveAt_m43577_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43577_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43577_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7764_IList_1_RemoveAt_m43577_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43577_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7764_IList_1_get_Item_m43573_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SurfaceAbstractBehaviour_t51_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43573_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43573_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceAbstractBehaviour_t51_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7764_IList_1_get_Item_m43573_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43573_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SurfaceAbstractBehaviour_t51_0_0_0;
static ParameterInfo IList_1_t7764_IList_1_set_Item_m43574_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t51_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43574_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43574_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7764_IList_1_set_Item_m43574_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43574_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7764_MethodInfos[] =
{
	&IList_1_IndexOf_m43575_MethodInfo,
	&IList_1_Insert_m43576_MethodInfo,
	&IList_1_RemoveAt_m43577_MethodInfo,
	&IList_1_get_Item_m43573_MethodInfo,
	&IList_1_set_Item_m43574_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7764_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7763_il2cpp_TypeInfo,
	&IEnumerable_1_t7765_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7764_0_0_0;
extern Il2CppType IList_1_t7764_1_0_0;
struct IList_1_t7764;
extern Il2CppGenericClass IList_1_t7764_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7764_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7764_MethodInfos/* methods */
	, IList_1_t7764_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7764_il2cpp_TypeInfo/* element_class */
	, IList_1_t7764_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7764_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7764_0_0_0/* byval_arg */
	, &IList_1_t7764_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7764_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7766_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>
extern MethodInfo ICollection_1_get_Count_m43578_MethodInfo;
static PropertyInfo ICollection_1_t7766____Count_PropertyInfo = 
{
	&ICollection_1_t7766_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43578_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43579_MethodInfo;
static PropertyInfo ICollection_1_t7766____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7766_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43579_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7766_PropertyInfos[] =
{
	&ICollection_1_t7766____Count_PropertyInfo,
	&ICollection_1_t7766____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43578_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43578_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7766_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43578_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43579_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43579_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7766_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43579_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSurfaceBehaviour_t165_0_0_0;
extern Il2CppType IEditorSurfaceBehaviour_t165_0_0_0;
static ParameterInfo ICollection_1_t7766_ICollection_1_Add_m43580_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t165_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43580_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43580_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7766_ICollection_1_Add_m43580_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43580_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43581_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43581_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43581_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSurfaceBehaviour_t165_0_0_0;
static ParameterInfo ICollection_1_t7766_ICollection_1_Contains_m43582_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t165_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43582_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43582_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7766_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7766_ICollection_1_Contains_m43582_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43582_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSurfaceBehaviourU5BU5D_t5644_0_0_0;
extern Il2CppType IEditorSurfaceBehaviourU5BU5D_t5644_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7766_ICollection_1_CopyTo_m43583_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviourU5BU5D_t5644_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43583_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43583_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7766_ICollection_1_CopyTo_m43583_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43583_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSurfaceBehaviour_t165_0_0_0;
static ParameterInfo ICollection_1_t7766_ICollection_1_Remove_m43584_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t165_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43584_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43584_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7766_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7766_ICollection_1_Remove_m43584_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43584_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7766_MethodInfos[] =
{
	&ICollection_1_get_Count_m43578_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43579_MethodInfo,
	&ICollection_1_Add_m43580_MethodInfo,
	&ICollection_1_Clear_m43581_MethodInfo,
	&ICollection_1_Contains_m43582_MethodInfo,
	&ICollection_1_CopyTo_m43583_MethodInfo,
	&ICollection_1_Remove_m43584_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7768_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7766_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7768_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7766_0_0_0;
extern Il2CppType ICollection_1_t7766_1_0_0;
struct ICollection_1_t7766;
extern Il2CppGenericClass ICollection_1_t7766_GenericClass;
TypeInfo ICollection_1_t7766_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7766_MethodInfos/* methods */
	, ICollection_1_t7766_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7766_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7766_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7766_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7766_0_0_0/* byval_arg */
	, &ICollection_1_t7766_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7766_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSurfaceBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSurfaceBehaviour>
extern Il2CppType IEnumerator_1_t6090_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43585_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSurfaceBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43585_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7768_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6090_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43585_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7768_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43585_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7768_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7768_0_0_0;
extern Il2CppType IEnumerable_1_t7768_1_0_0;
struct IEnumerable_1_t7768;
extern Il2CppGenericClass IEnumerable_1_t7768_GenericClass;
TypeInfo IEnumerable_1_t7768_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7768_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7768_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7768_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7768_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7768_0_0_0/* byval_arg */
	, &IEnumerable_1_t7768_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7768_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6090_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSurfaceBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43586_MethodInfo;
static PropertyInfo IEnumerator_1_t6090____Current_PropertyInfo = 
{
	&IEnumerator_1_t6090_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43586_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6090_PropertyInfos[] =
{
	&IEnumerator_1_t6090____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorSurfaceBehaviour_t165_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43586_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43586_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6090_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSurfaceBehaviour_t165_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43586_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6090_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43586_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6090_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6090_0_0_0;
extern Il2CppType IEnumerator_1_t6090_1_0_0;
struct IEnumerator_1_t6090;
extern Il2CppGenericClass IEnumerator_1_t6090_GenericClass;
TypeInfo IEnumerator_1_t6090_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6090_MethodInfos/* methods */
	, IEnumerator_1_t6090_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6090_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6090_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6090_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6090_0_0_0/* byval_arg */
	, &IEnumerator_1_t6090_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6090_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_86.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2933_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_86MethodDeclarations.h"

extern TypeInfo IEditorSurfaceBehaviour_t165_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15108_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorSurfaceBehaviour_t165_m33244_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorSurfaceBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorSurfaceBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorSurfaceBehaviour_t165_m33244(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2933____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2933, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2933____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2933, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2933_FieldInfos[] =
{
	&InternalEnumerator_1_t2933____array_0_FieldInfo,
	&InternalEnumerator_1_t2933____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15105_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2933____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2933_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15105_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2933____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2933_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15108_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2933_PropertyInfos[] =
{
	&InternalEnumerator_1_t2933____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2933____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2933_InternalEnumerator_1__ctor_m15104_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15104_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15104_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2933_InternalEnumerator_1__ctor_m15104_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15104_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15105_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15105_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15105_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15106_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15106_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15106_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15107_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15107_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15107_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSurfaceBehaviour_t165_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15108_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15108_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSurfaceBehaviour_t165_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15108_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2933_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15104_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15105_MethodInfo,
	&InternalEnumerator_1_Dispose_m15106_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15107_MethodInfo,
	&InternalEnumerator_1_get_Current_m15108_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15107_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15106_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2933_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15105_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15107_MethodInfo,
	&InternalEnumerator_1_Dispose_m15106_MethodInfo,
	&InternalEnumerator_1_get_Current_m15108_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2933_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6090_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2933_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6090_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorSurfaceBehaviour_t165_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2933_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15108_MethodInfo/* Method Usage */,
	&IEditorSurfaceBehaviour_t165_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorSurfaceBehaviour_t165_m33244_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2933_0_0_0;
extern Il2CppType InternalEnumerator_1_t2933_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2933_GenericClass;
TypeInfo InternalEnumerator_1_t2933_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2933_MethodInfos/* methods */
	, InternalEnumerator_1_t2933_PropertyInfos/* properties */
	, InternalEnumerator_1_t2933_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2933_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2933_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2933_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2933_1_0_0/* this_arg */
	, InternalEnumerator_1_t2933_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2933_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2933_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2933)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7767_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>
extern MethodInfo IList_1_get_Item_m43587_MethodInfo;
extern MethodInfo IList_1_set_Item_m43588_MethodInfo;
static PropertyInfo IList_1_t7767____Item_PropertyInfo = 
{
	&IList_1_t7767_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43587_MethodInfo/* get */
	, &IList_1_set_Item_m43588_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7767_PropertyInfos[] =
{
	&IList_1_t7767____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorSurfaceBehaviour_t165_0_0_0;
static ParameterInfo IList_1_t7767_IList_1_IndexOf_m43589_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t165_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43589_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43589_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7767_IList_1_IndexOf_m43589_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43589_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorSurfaceBehaviour_t165_0_0_0;
static ParameterInfo IList_1_t7767_IList_1_Insert_m43590_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t165_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43590_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43590_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7767_IList_1_Insert_m43590_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43590_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7767_IList_1_RemoveAt_m43591_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43591_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43591_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7767_IList_1_RemoveAt_m43591_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43591_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7767_IList_1_get_Item_m43587_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorSurfaceBehaviour_t165_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43587_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43587_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSurfaceBehaviour_t165_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7767_IList_1_get_Item_m43587_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43587_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorSurfaceBehaviour_t165_0_0_0;
static ParameterInfo IList_1_t7767_IList_1_set_Item_m43588_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t165_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43588_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43588_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7767_IList_1_set_Item_m43588_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43588_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7767_MethodInfos[] =
{
	&IList_1_IndexOf_m43589_MethodInfo,
	&IList_1_Insert_m43590_MethodInfo,
	&IList_1_RemoveAt_m43591_MethodInfo,
	&IList_1_get_Item_m43587_MethodInfo,
	&IList_1_set_Item_m43588_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7767_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7766_il2cpp_TypeInfo,
	&IEnumerable_1_t7768_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7767_0_0_0;
extern Il2CppType IList_1_t7767_1_0_0;
struct IList_1_t7767;
extern Il2CppGenericClass IList_1_t7767_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7767_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7767_MethodInfos/* methods */
	, IList_1_t7767_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7767_il2cpp_TypeInfo/* element_class */
	, IList_1_t7767_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7767_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7767_0_0_0/* byval_arg */
	, &IList_1_t7767_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7767_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_25.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2934_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_25MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_21.h"
extern TypeInfo InvokableCall_1_t2935_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_21MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15111_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15113_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2934____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2934_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2934, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2934_FieldInfos[] =
{
	&CachedInvokableCall_1_t2934____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2934_CachedInvokableCall_1__ctor_m15109_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t13_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15109_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15109_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2934_CachedInvokableCall_1__ctor_m15109_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15109_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2934_CachedInvokableCall_1_Invoke_m15110_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15110_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15110_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2934_CachedInvokableCall_1_Invoke_m15110_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15110_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2934_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15109_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15110_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15110_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15114_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2934_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15110_MethodInfo,
	&InvokableCall_1_Find_m15114_MethodInfo,
};
extern Il2CppType UnityAction_1_t2936_0_0_0;
extern TypeInfo UnityAction_1_t2936_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSurfaceBehaviour_t13_m33254_MethodInfo;
extern TypeInfo SurfaceBehaviour_t13_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15116_MethodInfo;
extern TypeInfo SurfaceBehaviour_t13_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2934_RGCTXData[8] = 
{
	&UnityAction_1_t2936_0_0_0/* Type Usage */,
	&UnityAction_1_t2936_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSurfaceBehaviour_t13_m33254_MethodInfo/* Method Usage */,
	&SurfaceBehaviour_t13_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15116_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15111_MethodInfo/* Method Usage */,
	&SurfaceBehaviour_t13_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15113_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2934_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2934_1_0_0;
struct CachedInvokableCall_1_t2934;
extern Il2CppGenericClass CachedInvokableCall_1_t2934_GenericClass;
TypeInfo CachedInvokableCall_1_t2934_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2934_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2934_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2935_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2934_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2934_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2934_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2934_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2934_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2934_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2934_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2934)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_28.h"
extern TypeInfo UnityAction_1_t2936_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_28MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.SurfaceBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.SurfaceBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSurfaceBehaviour_t13_m33254(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>
extern Il2CppType UnityAction_1_t2936_0_0_1;
FieldInfo InvokableCall_1_t2935____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2936_0_0_1/* type */
	, &InvokableCall_1_t2935_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2935, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2935_FieldInfos[] =
{
	&InvokableCall_1_t2935____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2935_InvokableCall_1__ctor_m15111_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15111_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15111_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2935_InvokableCall_1__ctor_m15111_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15111_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2936_0_0_0;
static ParameterInfo InvokableCall_1_t2935_InvokableCall_1__ctor_m15112_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2936_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15112_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15112_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2935_InvokableCall_1__ctor_m15112_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15112_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2935_InvokableCall_1_Invoke_m15113_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15113_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15113_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2935_InvokableCall_1_Invoke_m15113_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15113_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2935_InvokableCall_1_Find_m15114_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15114_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15114_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2935_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2935_InvokableCall_1_Find_m15114_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15114_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2935_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15111_MethodInfo,
	&InvokableCall_1__ctor_m15112_MethodInfo,
	&InvokableCall_1_Invoke_m15113_MethodInfo,
	&InvokableCall_1_Find_m15114_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2935_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15113_MethodInfo,
	&InvokableCall_1_Find_m15114_MethodInfo,
};
extern TypeInfo UnityAction_1_t2936_il2cpp_TypeInfo;
extern TypeInfo SurfaceBehaviour_t13_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2935_RGCTXData[5] = 
{
	&UnityAction_1_t2936_0_0_0/* Type Usage */,
	&UnityAction_1_t2936_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSurfaceBehaviour_t13_m33254_MethodInfo/* Method Usage */,
	&SurfaceBehaviour_t13_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15116_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2935_0_0_0;
extern Il2CppType InvokableCall_1_t2935_1_0_0;
struct InvokableCall_1_t2935;
extern Il2CppGenericClass InvokableCall_1_t2935_GenericClass;
TypeInfo InvokableCall_1_t2935_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2935_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2935_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2935_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2935_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2935_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2935_0_0_0/* byval_arg */
	, &InvokableCall_1_t2935_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2935_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2935_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2935)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2936_UnityAction_1__ctor_m15115_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15115_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15115_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2936_UnityAction_1__ctor_m15115_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15115_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
static ParameterInfo UnityAction_1_t2936_UnityAction_1_Invoke_m15116_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t13_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15116_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15116_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2936_UnityAction_1_Invoke_m15116_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15116_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t13_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2936_UnityAction_1_BeginInvoke_m15117_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t13_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15117_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15117_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2936_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2936_UnityAction_1_BeginInvoke_m15117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15117_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2936_UnityAction_1_EndInvoke_m15118_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15118_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15118_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2936_UnityAction_1_EndInvoke_m15118_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15118_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2936_MethodInfos[] =
{
	&UnityAction_1__ctor_m15115_MethodInfo,
	&UnityAction_1_Invoke_m15116_MethodInfo,
	&UnityAction_1_BeginInvoke_m15117_MethodInfo,
	&UnityAction_1_EndInvoke_m15118_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15117_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15118_MethodInfo;
static MethodInfo* UnityAction_1_t2936_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15116_MethodInfo,
	&UnityAction_1_BeginInvoke_m15117_MethodInfo,
	&UnityAction_1_EndInvoke_m15118_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2936_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2936_1_0_0;
struct UnityAction_1_t2936;
extern Il2CppGenericClass UnityAction_1_t2936_GenericClass;
TypeInfo UnityAction_1_t2936_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2936_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2936_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2936_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2936_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2936_0_0_0/* byval_arg */
	, &UnityAction_1_t2936_1_0_0/* this_arg */
	, UnityAction_1_t2936_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2936_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2936)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6092_il2cpp_TypeInfo;

// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43592_MethodInfo;
static PropertyInfo IEnumerator_1_t6092____Current_PropertyInfo = 
{
	&IEnumerator_1_t6092_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43592_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6092_PropertyInfos[] =
{
	&IEnumerator_1_t6092____Current_PropertyInfo,
	NULL
};
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43592_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43592_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6092_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoBehaviour_t52_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43592_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6092_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43592_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6092_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6092_0_0_0;
extern Il2CppType IEnumerator_1_t6092_1_0_0;
struct IEnumerator_1_t6092;
extern Il2CppGenericClass IEnumerator_1_t6092_GenericClass;
TypeInfo IEnumerator_1_t6092_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6092_MethodInfos/* methods */
	, IEnumerator_1_t6092_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6092_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6092_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6092_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6092_0_0_0/* byval_arg */
	, &IEnumerator_1_t6092_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6092_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_87.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2937_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_87MethodDeclarations.h"

extern TypeInfo TextRecoBehaviour_t52_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15123_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTextRecoBehaviour_t52_m33256_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TextRecoBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TextRecoBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTextRecoBehaviour_t52_m33256(__this, p0, method) (TextRecoBehaviour_t52 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2937____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2937_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2937, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2937____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2937_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2937, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2937_FieldInfos[] =
{
	&InternalEnumerator_1_t2937____array_0_FieldInfo,
	&InternalEnumerator_1_t2937____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15120_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2937____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2937_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15120_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2937____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2937_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15123_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2937_PropertyInfos[] =
{
	&InternalEnumerator_1_t2937____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2937____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2937_InternalEnumerator_1__ctor_m15119_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15119_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15119_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2937_InternalEnumerator_1__ctor_m15119_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15119_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15120_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15120_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2937_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15120_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15121_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15121_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15121_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15122_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15122_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15122_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15123_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15123_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2937_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoBehaviour_t52_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15123_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2937_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15119_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15120_MethodInfo,
	&InternalEnumerator_1_Dispose_m15121_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15122_MethodInfo,
	&InternalEnumerator_1_get_Current_m15123_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15122_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15121_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2937_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15120_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15122_MethodInfo,
	&InternalEnumerator_1_Dispose_m15121_MethodInfo,
	&InternalEnumerator_1_get_Current_m15123_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2937_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6092_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2937_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6092_il2cpp_TypeInfo, 7},
};
extern TypeInfo TextRecoBehaviour_t52_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2937_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15123_MethodInfo/* Method Usage */,
	&TextRecoBehaviour_t52_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTextRecoBehaviour_t52_m33256_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2937_0_0_0;
extern Il2CppType InternalEnumerator_1_t2937_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2937_GenericClass;
TypeInfo InternalEnumerator_1_t2937_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2937_MethodInfos/* methods */
	, InternalEnumerator_1_t2937_PropertyInfos/* properties */
	, InternalEnumerator_1_t2937_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2937_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2937_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2937_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2937_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2937_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2937_1_0_0/* this_arg */
	, InternalEnumerator_1_t2937_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2937_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2937_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2937)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7769_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>
extern MethodInfo ICollection_1_get_Count_m43593_MethodInfo;
static PropertyInfo ICollection_1_t7769____Count_PropertyInfo = 
{
	&ICollection_1_t7769_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43593_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43594_MethodInfo;
static PropertyInfo ICollection_1_t7769____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7769_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43594_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7769_PropertyInfos[] =
{
	&ICollection_1_t7769____Count_PropertyInfo,
	&ICollection_1_t7769____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43593_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43593_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7769_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43593_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43594_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43594_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43594_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
static ParameterInfo ICollection_1_t7769_ICollection_1_Add_m43595_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t52_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43595_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43595_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7769_ICollection_1_Add_m43595_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43595_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43596_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43596_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43596_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
static ParameterInfo ICollection_1_t7769_ICollection_1_Contains_m43597_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t52_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43597_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43597_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7769_ICollection_1_Contains_m43597_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43597_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviourU5BU5D_t5380_0_0_0;
extern Il2CppType TextRecoBehaviourU5BU5D_t5380_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7769_ICollection_1_CopyTo_m43598_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviourU5BU5D_t5380_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43598_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43598_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7769_ICollection_1_CopyTo_m43598_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43598_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
static ParameterInfo ICollection_1_t7769_ICollection_1_Remove_m43599_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t52_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43599_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43599_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7769_ICollection_1_Remove_m43599_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43599_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7769_MethodInfos[] =
{
	&ICollection_1_get_Count_m43593_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43594_MethodInfo,
	&ICollection_1_Add_m43595_MethodInfo,
	&ICollection_1_Clear_m43596_MethodInfo,
	&ICollection_1_Contains_m43597_MethodInfo,
	&ICollection_1_CopyTo_m43598_MethodInfo,
	&ICollection_1_Remove_m43599_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7771_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7769_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7771_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7769_0_0_0;
extern Il2CppType ICollection_1_t7769_1_0_0;
struct ICollection_1_t7769;
extern Il2CppGenericClass ICollection_1_t7769_GenericClass;
TypeInfo ICollection_1_t7769_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7769_MethodInfos/* methods */
	, ICollection_1_t7769_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7769_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7769_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7769_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7769_0_0_0/* byval_arg */
	, &ICollection_1_t7769_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7769_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoBehaviour>
extern Il2CppType IEnumerator_1_t6092_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43600_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43600_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7771_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6092_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43600_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7771_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43600_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7771_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7771_0_0_0;
extern Il2CppType IEnumerable_1_t7771_1_0_0;
struct IEnumerable_1_t7771;
extern Il2CppGenericClass IEnumerable_1_t7771_GenericClass;
TypeInfo IEnumerable_1_t7771_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7771_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7771_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7771_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7771_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7771_0_0_0/* byval_arg */
	, &IEnumerable_1_t7771_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7771_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7770_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>
extern MethodInfo IList_1_get_Item_m43601_MethodInfo;
extern MethodInfo IList_1_set_Item_m43602_MethodInfo;
static PropertyInfo IList_1_t7770____Item_PropertyInfo = 
{
	&IList_1_t7770_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43601_MethodInfo/* get */
	, &IList_1_set_Item_m43602_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7770_PropertyInfos[] =
{
	&IList_1_t7770____Item_PropertyInfo,
	NULL
};
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
static ParameterInfo IList_1_t7770_IList_1_IndexOf_m43603_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t52_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43603_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43603_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7770_IList_1_IndexOf_m43603_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43603_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
static ParameterInfo IList_1_t7770_IList_1_Insert_m43604_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t52_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43604_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43604_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7770_IList_1_Insert_m43604_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43604_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7770_IList_1_RemoveAt_m43605_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43605_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43605_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7770_IList_1_RemoveAt_m43605_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43605_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7770_IList_1_get_Item_m43601_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43601_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43601_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoBehaviour_t52_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7770_IList_1_get_Item_m43601_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43601_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
static ParameterInfo IList_1_t7770_IList_1_set_Item_m43602_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t52_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43602_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43602_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7770_IList_1_set_Item_m43602_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43602_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7770_MethodInfos[] =
{
	&IList_1_IndexOf_m43603_MethodInfo,
	&IList_1_Insert_m43604_MethodInfo,
	&IList_1_RemoveAt_m43605_MethodInfo,
	&IList_1_get_Item_m43601_MethodInfo,
	&IList_1_set_Item_m43602_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7770_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7769_il2cpp_TypeInfo,
	&IEnumerable_1_t7771_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7770_0_0_0;
extern Il2CppType IList_1_t7770_1_0_0;
struct IList_1_t7770;
extern Il2CppGenericClass IList_1_t7770_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7770_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7770_MethodInfos/* methods */
	, IList_1_t7770_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7770_il2cpp_TypeInfo/* element_class */
	, IList_1_t7770_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7770_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7770_0_0_0/* byval_arg */
	, &IList_1_t7770_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7770_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7772_il2cpp_TypeInfo;

// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43606_MethodInfo;
static PropertyInfo ICollection_1_t7772____Count_PropertyInfo = 
{
	&ICollection_1_t7772_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43606_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43607_MethodInfo;
static PropertyInfo ICollection_1_t7772____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7772_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43607_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7772_PropertyInfos[] =
{
	&ICollection_1_t7772____Count_PropertyInfo,
	&ICollection_1_t7772____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43606_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43606_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7772_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43606_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43607_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43607_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7772_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43607_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
static ParameterInfo ICollection_1_t7772_ICollection_1_Add_m43608_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t35_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43608_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43608_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7772_ICollection_1_Add_m43608_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43608_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43609_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43609_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43609_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
static ParameterInfo ICollection_1_t7772_ICollection_1_Contains_m43610_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t35_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43610_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43610_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7772_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7772_ICollection_1_Contains_m43610_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43610_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoAbstractBehaviourU5BU5D_t5645_0_0_0;
extern Il2CppType TextRecoAbstractBehaviourU5BU5D_t5645_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7772_ICollection_1_CopyTo_m43611_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviourU5BU5D_t5645_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43611_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43611_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7772_ICollection_1_CopyTo_m43611_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43611_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
static ParameterInfo ICollection_1_t7772_ICollection_1_Remove_m43612_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t35_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43612_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43612_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7772_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7772_ICollection_1_Remove_m43612_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43612_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7772_MethodInfos[] =
{
	&ICollection_1_get_Count_m43606_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43607_MethodInfo,
	&ICollection_1_Add_m43608_MethodInfo,
	&ICollection_1_Clear_m43609_MethodInfo,
	&ICollection_1_Contains_m43610_MethodInfo,
	&ICollection_1_CopyTo_m43611_MethodInfo,
	&ICollection_1_Remove_m43612_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7774_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7772_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7774_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7772_0_0_0;
extern Il2CppType ICollection_1_t7772_1_0_0;
struct ICollection_1_t7772;
extern Il2CppGenericClass ICollection_1_t7772_GenericClass;
TypeInfo ICollection_1_t7772_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7772_MethodInfos/* methods */
	, ICollection_1_t7772_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7772_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7772_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7772_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7772_0_0_0/* byval_arg */
	, &ICollection_1_t7772_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7772_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6094_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43613_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43613_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7774_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6094_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43613_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7774_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43613_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7774_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7774_0_0_0;
extern Il2CppType IEnumerable_1_t7774_1_0_0;
struct IEnumerable_1_t7774;
extern Il2CppGenericClass IEnumerable_1_t7774_GenericClass;
TypeInfo IEnumerable_1_t7774_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7774_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7774_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7774_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7774_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7774_0_0_0/* byval_arg */
	, &IEnumerable_1_t7774_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7774_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6094_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43614_MethodInfo;
static PropertyInfo IEnumerator_1_t6094____Current_PropertyInfo = 
{
	&IEnumerator_1_t6094_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43614_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6094_PropertyInfos[] =
{
	&IEnumerator_1_t6094____Current_PropertyInfo,
	NULL
};
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43614_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43614_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6094_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoAbstractBehaviour_t35_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43614_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6094_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43614_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6094_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6094_0_0_0;
extern Il2CppType IEnumerator_1_t6094_1_0_0;
struct IEnumerator_1_t6094;
extern Il2CppGenericClass IEnumerator_1_t6094_GenericClass;
TypeInfo IEnumerator_1_t6094_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6094_MethodInfos/* methods */
	, IEnumerator_1_t6094_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6094_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6094_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6094_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6094_0_0_0/* byval_arg */
	, &IEnumerator_1_t6094_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6094_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_88.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2938_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_88MethodDeclarations.h"

extern TypeInfo TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15128_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTextRecoAbstractBehaviour_t35_m33267_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TextRecoAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TextRecoAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTextRecoAbstractBehaviour_t35_m33267(__this, p0, method) (TextRecoAbstractBehaviour_t35 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2938____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2938, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2938____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2938, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2938_FieldInfos[] =
{
	&InternalEnumerator_1_t2938____array_0_FieldInfo,
	&InternalEnumerator_1_t2938____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15125_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2938____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2938_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15125_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2938____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2938_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15128_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2938_PropertyInfos[] =
{
	&InternalEnumerator_1_t2938____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2938____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2938_InternalEnumerator_1__ctor_m15124_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15124_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15124_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2938_InternalEnumerator_1__ctor_m15124_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15124_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15125_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15125_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15125_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15126_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15126_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15126_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15127_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15127_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15127_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15128_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15128_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoAbstractBehaviour_t35_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15128_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2938_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15124_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15125_MethodInfo,
	&InternalEnumerator_1_Dispose_m15126_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15127_MethodInfo,
	&InternalEnumerator_1_get_Current_m15128_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15127_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15126_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2938_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15125_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15127_MethodInfo,
	&InternalEnumerator_1_Dispose_m15126_MethodInfo,
	&InternalEnumerator_1_get_Current_m15128_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2938_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6094_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2938_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6094_il2cpp_TypeInfo, 7},
};
extern TypeInfo TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2938_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15128_MethodInfo/* Method Usage */,
	&TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTextRecoAbstractBehaviour_t35_m33267_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2938_0_0_0;
extern Il2CppType InternalEnumerator_1_t2938_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2938_GenericClass;
TypeInfo InternalEnumerator_1_t2938_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2938_MethodInfos/* methods */
	, InternalEnumerator_1_t2938_PropertyInfos/* properties */
	, InternalEnumerator_1_t2938_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2938_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2938_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2938_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2938_1_0_0/* this_arg */
	, InternalEnumerator_1_t2938_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2938_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2938_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2938)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7773_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43615_MethodInfo;
extern MethodInfo IList_1_set_Item_m43616_MethodInfo;
static PropertyInfo IList_1_t7773____Item_PropertyInfo = 
{
	&IList_1_t7773_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43615_MethodInfo/* get */
	, &IList_1_set_Item_m43616_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7773_PropertyInfos[] =
{
	&IList_1_t7773____Item_PropertyInfo,
	NULL
};
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
static ParameterInfo IList_1_t7773_IList_1_IndexOf_m43617_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t35_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43617_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43617_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7773_IList_1_IndexOf_m43617_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43617_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
static ParameterInfo IList_1_t7773_IList_1_Insert_m43618_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t35_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43618_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43618_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7773_IList_1_Insert_m43618_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43618_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7773_IList_1_RemoveAt_m43619_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43619_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43619_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7773_IList_1_RemoveAt_m43619_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43619_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7773_IList_1_get_Item_m43615_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43615_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43615_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoAbstractBehaviour_t35_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7773_IList_1_get_Item_m43615_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43615_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
static ParameterInfo IList_1_t7773_IList_1_set_Item_m43616_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t35_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43616_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43616_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7773_IList_1_set_Item_m43616_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43616_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7773_MethodInfos[] =
{
	&IList_1_IndexOf_m43617_MethodInfo,
	&IList_1_Insert_m43618_MethodInfo,
	&IList_1_RemoveAt_m43619_MethodInfo,
	&IList_1_get_Item_m43615_MethodInfo,
	&IList_1_set_Item_m43616_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7773_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7772_il2cpp_TypeInfo,
	&IEnumerable_1_t7774_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7773_0_0_0;
extern Il2CppType IList_1_t7773_1_0_0;
struct IList_1_t7773;
extern Il2CppGenericClass IList_1_t7773_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7773_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7773_MethodInfos/* methods */
	, IList_1_t7773_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7773_il2cpp_TypeInfo/* element_class */
	, IList_1_t7773_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7773_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7773_0_0_0/* byval_arg */
	, &IList_1_t7773_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7773_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7775_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>
extern MethodInfo ICollection_1_get_Count_m43620_MethodInfo;
static PropertyInfo ICollection_1_t7775____Count_PropertyInfo = 
{
	&ICollection_1_t7775_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43620_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43621_MethodInfo;
static PropertyInfo ICollection_1_t7775____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7775_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43621_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7775_PropertyInfos[] =
{
	&ICollection_1_t7775____Count_PropertyInfo,
	&ICollection_1_t7775____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43620_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43620_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7775_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43620_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43621_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43621_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43621_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTextRecoBehaviour_t166_0_0_0;
extern Il2CppType IEditorTextRecoBehaviour_t166_0_0_0;
static ParameterInfo ICollection_1_t7775_ICollection_1_Add_m43622_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t166_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43622_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43622_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7775_ICollection_1_Add_m43622_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43622_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43623_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43623_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43623_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTextRecoBehaviour_t166_0_0_0;
static ParameterInfo ICollection_1_t7775_ICollection_1_Contains_m43624_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t166_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43624_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43624_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7775_ICollection_1_Contains_m43624_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43624_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTextRecoBehaviourU5BU5D_t5646_0_0_0;
extern Il2CppType IEditorTextRecoBehaviourU5BU5D_t5646_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7775_ICollection_1_CopyTo_m43625_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviourU5BU5D_t5646_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43625_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43625_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7775_ICollection_1_CopyTo_m43625_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43625_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTextRecoBehaviour_t166_0_0_0;
static ParameterInfo ICollection_1_t7775_ICollection_1_Remove_m43626_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t166_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43626_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43626_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7775_ICollection_1_Remove_m43626_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43626_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7775_MethodInfos[] =
{
	&ICollection_1_get_Count_m43620_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43621_MethodInfo,
	&ICollection_1_Add_m43622_MethodInfo,
	&ICollection_1_Clear_m43623_MethodInfo,
	&ICollection_1_Contains_m43624_MethodInfo,
	&ICollection_1_CopyTo_m43625_MethodInfo,
	&ICollection_1_Remove_m43626_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7777_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7775_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7777_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7775_0_0_0;
extern Il2CppType ICollection_1_t7775_1_0_0;
struct ICollection_1_t7775;
extern Il2CppGenericClass ICollection_1_t7775_GenericClass;
TypeInfo ICollection_1_t7775_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7775_MethodInfos/* methods */
	, ICollection_1_t7775_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7775_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7775_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7775_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7775_0_0_0/* byval_arg */
	, &ICollection_1_t7775_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7775_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTextRecoBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTextRecoBehaviour>
extern Il2CppType IEnumerator_1_t6096_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43627_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTextRecoBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43627_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7777_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6096_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43627_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7777_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43627_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7777_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7777_0_0_0;
extern Il2CppType IEnumerable_1_t7777_1_0_0;
struct IEnumerable_1_t7777;
extern Il2CppGenericClass IEnumerable_1_t7777_GenericClass;
TypeInfo IEnumerable_1_t7777_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7777_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7777_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7777_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7777_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7777_0_0_0/* byval_arg */
	, &IEnumerable_1_t7777_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7777_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6096_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTextRecoBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43628_MethodInfo;
static PropertyInfo IEnumerator_1_t6096____Current_PropertyInfo = 
{
	&IEnumerator_1_t6096_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43628_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6096_PropertyInfos[] =
{
	&IEnumerator_1_t6096____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorTextRecoBehaviour_t166_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43628_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43628_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6096_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTextRecoBehaviour_t166_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43628_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6096_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43628_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6096_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6096_0_0_0;
extern Il2CppType IEnumerator_1_t6096_1_0_0;
struct IEnumerator_1_t6096;
extern Il2CppGenericClass IEnumerator_1_t6096_GenericClass;
TypeInfo IEnumerator_1_t6096_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6096_MethodInfos/* methods */
	, IEnumerator_1_t6096_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6096_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6096_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6096_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6096_0_0_0/* byval_arg */
	, &IEnumerator_1_t6096_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6096_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_89.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2939_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_89MethodDeclarations.h"

extern TypeInfo IEditorTextRecoBehaviour_t166_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15133_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorTextRecoBehaviour_t166_m33278_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorTextRecoBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorTextRecoBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorTextRecoBehaviour_t166_m33278(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2939____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2939, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2939____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2939, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2939_FieldInfos[] =
{
	&InternalEnumerator_1_t2939____array_0_FieldInfo,
	&InternalEnumerator_1_t2939____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15130_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2939____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2939_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15130_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2939____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2939_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15133_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2939_PropertyInfos[] =
{
	&InternalEnumerator_1_t2939____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2939____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2939_InternalEnumerator_1__ctor_m15129_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15129_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15129_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2939_InternalEnumerator_1__ctor_m15129_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15129_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15130_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15130_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15130_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15131_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15131_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15131_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15132_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15132_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15132_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTextRecoBehaviour_t166_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15133_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15133_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTextRecoBehaviour_t166_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15133_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2939_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15129_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15130_MethodInfo,
	&InternalEnumerator_1_Dispose_m15131_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15132_MethodInfo,
	&InternalEnumerator_1_get_Current_m15133_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15132_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15131_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2939_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15130_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15132_MethodInfo,
	&InternalEnumerator_1_Dispose_m15131_MethodInfo,
	&InternalEnumerator_1_get_Current_m15133_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2939_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6096_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2939_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6096_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorTextRecoBehaviour_t166_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2939_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15133_MethodInfo/* Method Usage */,
	&IEditorTextRecoBehaviour_t166_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorTextRecoBehaviour_t166_m33278_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2939_0_0_0;
extern Il2CppType InternalEnumerator_1_t2939_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2939_GenericClass;
TypeInfo InternalEnumerator_1_t2939_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2939_MethodInfos/* methods */
	, InternalEnumerator_1_t2939_PropertyInfos/* properties */
	, InternalEnumerator_1_t2939_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2939_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2939_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2939_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2939_1_0_0/* this_arg */
	, InternalEnumerator_1_t2939_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2939_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2939_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2939)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7776_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>
extern MethodInfo IList_1_get_Item_m43629_MethodInfo;
extern MethodInfo IList_1_set_Item_m43630_MethodInfo;
static PropertyInfo IList_1_t7776____Item_PropertyInfo = 
{
	&IList_1_t7776_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43629_MethodInfo/* get */
	, &IList_1_set_Item_m43630_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7776_PropertyInfos[] =
{
	&IList_1_t7776____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorTextRecoBehaviour_t166_0_0_0;
static ParameterInfo IList_1_t7776_IList_1_IndexOf_m43631_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t166_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43631_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43631_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7776_IList_1_IndexOf_m43631_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43631_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorTextRecoBehaviour_t166_0_0_0;
static ParameterInfo IList_1_t7776_IList_1_Insert_m43632_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t166_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43632_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43632_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7776_IList_1_Insert_m43632_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43632_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7776_IList_1_RemoveAt_m43633_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43633_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43633_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7776_IList_1_RemoveAt_m43633_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43633_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7776_IList_1_get_Item_m43629_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorTextRecoBehaviour_t166_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43629_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43629_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTextRecoBehaviour_t166_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7776_IList_1_get_Item_m43629_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43629_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorTextRecoBehaviour_t166_0_0_0;
static ParameterInfo IList_1_t7776_IList_1_set_Item_m43630_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t166_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43630_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43630_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7776_IList_1_set_Item_m43630_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43630_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7776_MethodInfos[] =
{
	&IList_1_IndexOf_m43631_MethodInfo,
	&IList_1_Insert_m43632_MethodInfo,
	&IList_1_RemoveAt_m43633_MethodInfo,
	&IList_1_get_Item_m43629_MethodInfo,
	&IList_1_set_Item_m43630_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7776_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7775_il2cpp_TypeInfo,
	&IEnumerable_1_t7777_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7776_0_0_0;
extern Il2CppType IList_1_t7776_1_0_0;
struct IList_1_t7776;
extern Il2CppGenericClass IList_1_t7776_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7776_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7776_MethodInfos/* methods */
	, IList_1_t7776_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7776_il2cpp_TypeInfo/* element_class */
	, IList_1_t7776_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7776_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7776_0_0_0/* byval_arg */
	, &IList_1_t7776_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7776_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_26.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2940_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_26MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_22.h"
extern TypeInfo InvokableCall_1_t2941_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_22MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15136_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15138_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2940____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2940_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2940, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2940_FieldInfos[] =
{
	&CachedInvokableCall_1_t2940____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2940_CachedInvokableCall_1__ctor_m15134_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t52_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15134_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15134_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2940_CachedInvokableCall_1__ctor_m15134_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15134_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2940_CachedInvokableCall_1_Invoke_m15135_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15135_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15135_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2940_CachedInvokableCall_1_Invoke_m15135_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15135_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2940_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15134_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15135_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15135_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15139_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2940_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15135_MethodInfo,
	&InvokableCall_1_Find_m15139_MethodInfo,
};
extern Il2CppType UnityAction_1_t2942_0_0_0;
extern TypeInfo UnityAction_1_t2942_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTextRecoBehaviour_t52_m33288_MethodInfo;
extern TypeInfo TextRecoBehaviour_t52_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15141_MethodInfo;
extern TypeInfo TextRecoBehaviour_t52_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2940_RGCTXData[8] = 
{
	&UnityAction_1_t2942_0_0_0/* Type Usage */,
	&UnityAction_1_t2942_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTextRecoBehaviour_t52_m33288_MethodInfo/* Method Usage */,
	&TextRecoBehaviour_t52_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15141_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15136_MethodInfo/* Method Usage */,
	&TextRecoBehaviour_t52_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15138_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2940_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2940_1_0_0;
struct CachedInvokableCall_1_t2940;
extern Il2CppGenericClass CachedInvokableCall_1_t2940_GenericClass;
TypeInfo CachedInvokableCall_1_t2940_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2940_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2940_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2941_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2940_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2940_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2940_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2940_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2940_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2940_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2940_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2940)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_29.h"
extern TypeInfo UnityAction_1_t2942_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_29MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TextRecoBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TextRecoBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisTextRecoBehaviour_t52_m33288(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>
extern Il2CppType UnityAction_1_t2942_0_0_1;
FieldInfo InvokableCall_1_t2941____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2942_0_0_1/* type */
	, &InvokableCall_1_t2941_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2941, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2941_FieldInfos[] =
{
	&InvokableCall_1_t2941____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2941_InvokableCall_1__ctor_m15136_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15136_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15136_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2941_InvokableCall_1__ctor_m15136_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15136_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2942_0_0_0;
static ParameterInfo InvokableCall_1_t2941_InvokableCall_1__ctor_m15137_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2942_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15137_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15137_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2941_InvokableCall_1__ctor_m15137_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15137_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2941_InvokableCall_1_Invoke_m15138_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15138_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15138_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2941_InvokableCall_1_Invoke_m15138_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15138_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2941_InvokableCall_1_Find_m15139_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15139_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15139_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2941_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2941_InvokableCall_1_Find_m15139_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15139_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2941_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15136_MethodInfo,
	&InvokableCall_1__ctor_m15137_MethodInfo,
	&InvokableCall_1_Invoke_m15138_MethodInfo,
	&InvokableCall_1_Find_m15139_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2941_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15138_MethodInfo,
	&InvokableCall_1_Find_m15139_MethodInfo,
};
extern TypeInfo UnityAction_1_t2942_il2cpp_TypeInfo;
extern TypeInfo TextRecoBehaviour_t52_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2941_RGCTXData[5] = 
{
	&UnityAction_1_t2942_0_0_0/* Type Usage */,
	&UnityAction_1_t2942_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTextRecoBehaviour_t52_m33288_MethodInfo/* Method Usage */,
	&TextRecoBehaviour_t52_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15141_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2941_0_0_0;
extern Il2CppType InvokableCall_1_t2941_1_0_0;
struct InvokableCall_1_t2941;
extern Il2CppGenericClass InvokableCall_1_t2941_GenericClass;
TypeInfo InvokableCall_1_t2941_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2941_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2941_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2941_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2941_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2941_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2941_0_0_0/* byval_arg */
	, &InvokableCall_1_t2941_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2941_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2941_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2941)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2942_UnityAction_1__ctor_m15140_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15140_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15140_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2942_UnityAction_1__ctor_m15140_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15140_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
static ParameterInfo UnityAction_1_t2942_UnityAction_1_Invoke_m15141_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t52_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15141_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15141_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2942_UnityAction_1_Invoke_m15141_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15141_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t52_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2942_UnityAction_1_BeginInvoke_m15142_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t52_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15142_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15142_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2942_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2942_UnityAction_1_BeginInvoke_m15142_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15142_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2942_UnityAction_1_EndInvoke_m15143_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15143_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15143_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2942_UnityAction_1_EndInvoke_m15143_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15143_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2942_MethodInfos[] =
{
	&UnityAction_1__ctor_m15140_MethodInfo,
	&UnityAction_1_Invoke_m15141_MethodInfo,
	&UnityAction_1_BeginInvoke_m15142_MethodInfo,
	&UnityAction_1_EndInvoke_m15143_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15142_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15143_MethodInfo;
static MethodInfo* UnityAction_1_t2942_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15141_MethodInfo,
	&UnityAction_1_BeginInvoke_m15142_MethodInfo,
	&UnityAction_1_EndInvoke_m15143_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2942_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2942_1_0_0;
struct UnityAction_1_t2942;
extern Il2CppGenericClass UnityAction_1_t2942_GenericClass;
TypeInfo UnityAction_1_t2942_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2942_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2942_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2942_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2942_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2942_0_0_0/* byval_arg */
	, &UnityAction_1_t2942_1_0_0/* this_arg */
	, UnityAction_1_t2942_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2942_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2942)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6098_il2cpp_TypeInfo;

// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43634_MethodInfo;
static PropertyInfo IEnumerator_1_t6098____Current_PropertyInfo = 
{
	&IEnumerator_1_t6098_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43634_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6098_PropertyInfos[] =
{
	&IEnumerator_1_t6098____Current_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43634_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43634_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6098_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffBehaviour_t53_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43634_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6098_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43634_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6098_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6098_0_0_0;
extern Il2CppType IEnumerator_1_t6098_1_0_0;
struct IEnumerator_1_t6098;
extern Il2CppGenericClass IEnumerator_1_t6098_GenericClass;
TypeInfo IEnumerator_1_t6098_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6098_MethodInfos/* methods */
	, IEnumerator_1_t6098_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6098_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6098_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6098_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6098_0_0_0/* byval_arg */
	, &IEnumerator_1_t6098_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6098_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_90.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2943_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_90MethodDeclarations.h"

extern TypeInfo TurnOffBehaviour_t53_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15148_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTurnOffBehaviour_t53_m33290_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTurnOffBehaviour_t53_m33290(__this, p0, method) (TurnOffBehaviour_t53 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2943____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2943_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2943, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2943____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2943_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2943, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2943_FieldInfos[] =
{
	&InternalEnumerator_1_t2943____array_0_FieldInfo,
	&InternalEnumerator_1_t2943____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15145_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2943____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2943_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15145_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2943____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2943_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15148_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2943_PropertyInfos[] =
{
	&InternalEnumerator_1_t2943____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2943____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2943_InternalEnumerator_1__ctor_m15144_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15144_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15144_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2943_InternalEnumerator_1__ctor_m15144_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15144_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15145_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15145_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2943_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15145_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15146_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15146_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15146_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15147_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15147_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2943_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15147_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15148_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15148_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2943_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffBehaviour_t53_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15148_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2943_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15144_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15145_MethodInfo,
	&InternalEnumerator_1_Dispose_m15146_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15147_MethodInfo,
	&InternalEnumerator_1_get_Current_m15148_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15147_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15146_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2943_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15145_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15147_MethodInfo,
	&InternalEnumerator_1_Dispose_m15146_MethodInfo,
	&InternalEnumerator_1_get_Current_m15148_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2943_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6098_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2943_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6098_il2cpp_TypeInfo, 7},
};
extern TypeInfo TurnOffBehaviour_t53_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2943_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15148_MethodInfo/* Method Usage */,
	&TurnOffBehaviour_t53_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTurnOffBehaviour_t53_m33290_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2943_0_0_0;
extern Il2CppType InternalEnumerator_1_t2943_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2943_GenericClass;
TypeInfo InternalEnumerator_1_t2943_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2943_MethodInfos/* methods */
	, InternalEnumerator_1_t2943_PropertyInfos/* properties */
	, InternalEnumerator_1_t2943_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2943_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2943_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2943_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2943_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2943_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2943_1_0_0/* this_arg */
	, InternalEnumerator_1_t2943_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2943_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2943_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2943)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7778_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>
extern MethodInfo ICollection_1_get_Count_m43635_MethodInfo;
static PropertyInfo ICollection_1_t7778____Count_PropertyInfo = 
{
	&ICollection_1_t7778_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43635_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43636_MethodInfo;
static PropertyInfo ICollection_1_t7778____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7778_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43636_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7778_PropertyInfos[] =
{
	&ICollection_1_t7778____Count_PropertyInfo,
	&ICollection_1_t7778____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43635_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43635_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7778_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43635_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43636_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43636_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7778_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43636_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
static ParameterInfo ICollection_1_t7778_ICollection_1_Add_m43637_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t53_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43637_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43637_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7778_ICollection_1_Add_m43637_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43637_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43638_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43638_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43638_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
static ParameterInfo ICollection_1_t7778_ICollection_1_Contains_m43639_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t53_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43639_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43639_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7778_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7778_ICollection_1_Contains_m43639_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43639_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviourU5BU5D_t5381_0_0_0;
extern Il2CppType TurnOffBehaviourU5BU5D_t5381_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7778_ICollection_1_CopyTo_m43640_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviourU5BU5D_t5381_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43640_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43640_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7778_ICollection_1_CopyTo_m43640_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43640_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
static ParameterInfo ICollection_1_t7778_ICollection_1_Remove_m43641_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t53_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43641_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43641_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7778_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7778_ICollection_1_Remove_m43641_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43641_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7778_MethodInfos[] =
{
	&ICollection_1_get_Count_m43635_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43636_MethodInfo,
	&ICollection_1_Add_m43637_MethodInfo,
	&ICollection_1_Clear_m43638_MethodInfo,
	&ICollection_1_Contains_m43639_MethodInfo,
	&ICollection_1_CopyTo_m43640_MethodInfo,
	&ICollection_1_Remove_m43641_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7780_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7778_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7780_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7778_0_0_0;
extern Il2CppType ICollection_1_t7778_1_0_0;
struct ICollection_1_t7778;
extern Il2CppGenericClass ICollection_1_t7778_GenericClass;
TypeInfo ICollection_1_t7778_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7778_MethodInfos/* methods */
	, ICollection_1_t7778_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7778_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7778_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7778_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7778_0_0_0/* byval_arg */
	, &ICollection_1_t7778_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7778_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffBehaviour>
extern Il2CppType IEnumerator_1_t6098_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43642_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43642_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7780_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6098_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43642_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7780_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43642_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7780_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7780_0_0_0;
extern Il2CppType IEnumerable_1_t7780_1_0_0;
struct IEnumerable_1_t7780;
extern Il2CppGenericClass IEnumerable_1_t7780_GenericClass;
TypeInfo IEnumerable_1_t7780_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7780_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7780_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7780_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7780_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7780_0_0_0/* byval_arg */
	, &IEnumerable_1_t7780_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7780_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7779_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>
extern MethodInfo IList_1_get_Item_m43643_MethodInfo;
extern MethodInfo IList_1_set_Item_m43644_MethodInfo;
static PropertyInfo IList_1_t7779____Item_PropertyInfo = 
{
	&IList_1_t7779_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43643_MethodInfo/* get */
	, &IList_1_set_Item_m43644_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7779_PropertyInfos[] =
{
	&IList_1_t7779____Item_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
static ParameterInfo IList_1_t7779_IList_1_IndexOf_m43645_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t53_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43645_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43645_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7779_IList_1_IndexOf_m43645_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43645_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
static ParameterInfo IList_1_t7779_IList_1_Insert_m43646_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t53_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43646_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43646_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7779_IList_1_Insert_m43646_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43646_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7779_IList_1_RemoveAt_m43647_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43647_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43647_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7779_IList_1_RemoveAt_m43647_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43647_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7779_IList_1_get_Item_m43643_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43643_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43643_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffBehaviour_t53_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7779_IList_1_get_Item_m43643_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43643_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
static ParameterInfo IList_1_t7779_IList_1_set_Item_m43644_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t53_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43644_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43644_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7779_IList_1_set_Item_m43644_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43644_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7779_MethodInfos[] =
{
	&IList_1_IndexOf_m43645_MethodInfo,
	&IList_1_Insert_m43646_MethodInfo,
	&IList_1_RemoveAt_m43647_MethodInfo,
	&IList_1_get_Item_m43643_MethodInfo,
	&IList_1_set_Item_m43644_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7779_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7778_il2cpp_TypeInfo,
	&IEnumerable_1_t7780_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7779_0_0_0;
extern Il2CppType IList_1_t7779_1_0_0;
struct IList_1_t7779;
extern Il2CppGenericClass IList_1_t7779_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7779_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7779_MethodInfos/* methods */
	, IList_1_t7779_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7779_il2cpp_TypeInfo/* element_class */
	, IList_1_t7779_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7779_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7779_0_0_0/* byval_arg */
	, &IList_1_t7779_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7779_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7781_il2cpp_TypeInfo;

// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43648_MethodInfo;
static PropertyInfo ICollection_1_t7781____Count_PropertyInfo = 
{
	&ICollection_1_t7781_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43648_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43649_MethodInfo;
static PropertyInfo ICollection_1_t7781____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7781_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43649_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7781_PropertyInfos[] =
{
	&ICollection_1_t7781____Count_PropertyInfo,
	&ICollection_1_t7781____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43648_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43648_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7781_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43648_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43649_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43649_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7781_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43649_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
static ParameterInfo ICollection_1_t7781_ICollection_1_Add_m43650_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t31_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43650_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43650_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7781_ICollection_1_Add_m43650_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43650_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43651_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43651_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43651_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
static ParameterInfo ICollection_1_t7781_ICollection_1_Contains_m43652_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t31_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43652_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43652_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7781_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7781_ICollection_1_Contains_m43652_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43652_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffAbstractBehaviourU5BU5D_t5647_0_0_0;
extern Il2CppType TurnOffAbstractBehaviourU5BU5D_t5647_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7781_ICollection_1_CopyTo_m43653_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviourU5BU5D_t5647_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43653_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43653_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7781_ICollection_1_CopyTo_m43653_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43653_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
static ParameterInfo ICollection_1_t7781_ICollection_1_Remove_m43654_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t31_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43654_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43654_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7781_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7781_ICollection_1_Remove_m43654_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43654_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7781_MethodInfos[] =
{
	&ICollection_1_get_Count_m43648_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43649_MethodInfo,
	&ICollection_1_Add_m43650_MethodInfo,
	&ICollection_1_Clear_m43651_MethodInfo,
	&ICollection_1_Contains_m43652_MethodInfo,
	&ICollection_1_CopyTo_m43653_MethodInfo,
	&ICollection_1_Remove_m43654_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7783_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7781_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7783_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7781_0_0_0;
extern Il2CppType ICollection_1_t7781_1_0_0;
struct ICollection_1_t7781;
extern Il2CppGenericClass ICollection_1_t7781_GenericClass;
TypeInfo ICollection_1_t7781_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7781_MethodInfos/* methods */
	, ICollection_1_t7781_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7781_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7781_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7781_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7781_0_0_0/* byval_arg */
	, &ICollection_1_t7781_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7781_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6100_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43655_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43655_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7783_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6100_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43655_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7783_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43655_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7783_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7783_0_0_0;
extern Il2CppType IEnumerable_1_t7783_1_0_0;
struct IEnumerable_1_t7783;
extern Il2CppGenericClass IEnumerable_1_t7783_GenericClass;
TypeInfo IEnumerable_1_t7783_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7783_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7783_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7783_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7783_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7783_0_0_0/* byval_arg */
	, &IEnumerable_1_t7783_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7783_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6100_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43656_MethodInfo;
static PropertyInfo IEnumerator_1_t6100____Current_PropertyInfo = 
{
	&IEnumerator_1_t6100_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43656_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6100_PropertyInfos[] =
{
	&IEnumerator_1_t6100____Current_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43656_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43656_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6100_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffAbstractBehaviour_t31_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43656_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6100_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43656_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6100_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6100_0_0_0;
extern Il2CppType IEnumerator_1_t6100_1_0_0;
struct IEnumerator_1_t6100;
extern Il2CppGenericClass IEnumerator_1_t6100_GenericClass;
TypeInfo IEnumerator_1_t6100_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6100_MethodInfos/* methods */
	, IEnumerator_1_t6100_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6100_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6100_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6100_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6100_0_0_0/* byval_arg */
	, &IEnumerator_1_t6100_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6100_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_91.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2944_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_91MethodDeclarations.h"

extern TypeInfo TurnOffAbstractBehaviour_t31_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15153_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTurnOffAbstractBehaviour_t31_m33301_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTurnOffAbstractBehaviour_t31_m33301(__this, p0, method) (TurnOffAbstractBehaviour_t31 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2944____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2944, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2944____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2944, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2944_FieldInfos[] =
{
	&InternalEnumerator_1_t2944____array_0_FieldInfo,
	&InternalEnumerator_1_t2944____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15150_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2944____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2944_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15150_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2944____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2944_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15153_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2944_PropertyInfos[] =
{
	&InternalEnumerator_1_t2944____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2944____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2944_InternalEnumerator_1__ctor_m15149_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15149_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15149_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2944_InternalEnumerator_1__ctor_m15149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15149_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15150_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15150_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15150_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15151_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15151_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15151_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15152_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15152_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15152_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15153_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15153_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffAbstractBehaviour_t31_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15153_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2944_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15149_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15150_MethodInfo,
	&InternalEnumerator_1_Dispose_m15151_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15152_MethodInfo,
	&InternalEnumerator_1_get_Current_m15153_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15152_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15151_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2944_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15150_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15152_MethodInfo,
	&InternalEnumerator_1_Dispose_m15151_MethodInfo,
	&InternalEnumerator_1_get_Current_m15153_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2944_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6100_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2944_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6100_il2cpp_TypeInfo, 7},
};
extern TypeInfo TurnOffAbstractBehaviour_t31_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2944_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15153_MethodInfo/* Method Usage */,
	&TurnOffAbstractBehaviour_t31_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTurnOffAbstractBehaviour_t31_m33301_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2944_0_0_0;
extern Il2CppType InternalEnumerator_1_t2944_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2944_GenericClass;
TypeInfo InternalEnumerator_1_t2944_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2944_MethodInfos/* methods */
	, InternalEnumerator_1_t2944_PropertyInfos/* properties */
	, InternalEnumerator_1_t2944_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2944_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2944_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2944_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2944_1_0_0/* this_arg */
	, InternalEnumerator_1_t2944_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2944_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2944_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2944)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7782_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43657_MethodInfo;
extern MethodInfo IList_1_set_Item_m43658_MethodInfo;
static PropertyInfo IList_1_t7782____Item_PropertyInfo = 
{
	&IList_1_t7782_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43657_MethodInfo/* get */
	, &IList_1_set_Item_m43658_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7782_PropertyInfos[] =
{
	&IList_1_t7782____Item_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
static ParameterInfo IList_1_t7782_IList_1_IndexOf_m43659_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t31_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43659_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43659_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7782_IList_1_IndexOf_m43659_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43659_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
static ParameterInfo IList_1_t7782_IList_1_Insert_m43660_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t31_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43660_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43660_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7782_IList_1_Insert_m43660_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43660_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7782_IList_1_RemoveAt_m43661_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43661_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43661_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7782_IList_1_RemoveAt_m43661_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43661_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7782_IList_1_get_Item_m43657_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43657_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43657_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffAbstractBehaviour_t31_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7782_IList_1_get_Item_m43657_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43657_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
static ParameterInfo IList_1_t7782_IList_1_set_Item_m43658_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t31_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43658_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43658_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7782_IList_1_set_Item_m43658_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43658_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7782_MethodInfos[] =
{
	&IList_1_IndexOf_m43659_MethodInfo,
	&IList_1_Insert_m43660_MethodInfo,
	&IList_1_RemoveAt_m43661_MethodInfo,
	&IList_1_get_Item_m43657_MethodInfo,
	&IList_1_set_Item_m43658_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7782_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7781_il2cpp_TypeInfo,
	&IEnumerable_1_t7783_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7782_0_0_0;
extern Il2CppType IList_1_t7782_1_0_0;
struct IList_1_t7782;
extern Il2CppGenericClass IList_1_t7782_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7782_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7782_MethodInfos/* methods */
	, IList_1_t7782_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7782_il2cpp_TypeInfo/* element_class */
	, IList_1_t7782_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7782_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7782_0_0_0/* byval_arg */
	, &IList_1_t7782_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7782_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_27.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2945_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_27MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_23.h"
extern TypeInfo InvokableCall_1_t2946_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_23MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15156_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15158_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2945____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2945_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2945, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2945_FieldInfos[] =
{
	&CachedInvokableCall_1_t2945____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2945_CachedInvokableCall_1__ctor_m15154_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t53_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15154_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15154_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2945_CachedInvokableCall_1__ctor_m15154_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15154_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2945_CachedInvokableCall_1_Invoke_m15155_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15155_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15155_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2945_CachedInvokableCall_1_Invoke_m15155_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15155_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2945_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15154_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15155_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15155_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15159_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2945_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15155_MethodInfo,
	&InvokableCall_1_Find_m15159_MethodInfo,
};
extern Il2CppType UnityAction_1_t2947_0_0_0;
extern TypeInfo UnityAction_1_t2947_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffBehaviour_t53_m33311_MethodInfo;
extern TypeInfo TurnOffBehaviour_t53_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15161_MethodInfo;
extern TypeInfo TurnOffBehaviour_t53_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2945_RGCTXData[8] = 
{
	&UnityAction_1_t2947_0_0_0/* Type Usage */,
	&UnityAction_1_t2947_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffBehaviour_t53_m33311_MethodInfo/* Method Usage */,
	&TurnOffBehaviour_t53_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15161_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15156_MethodInfo/* Method Usage */,
	&TurnOffBehaviour_t53_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15158_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2945_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2945_1_0_0;
struct CachedInvokableCall_1_t2945;
extern Il2CppGenericClass CachedInvokableCall_1_t2945_GenericClass;
TypeInfo CachedInvokableCall_1_t2945_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2945_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2945_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2946_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2945_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2945_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2945_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2945_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2945_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2945_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2945_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2945)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_30.h"
extern TypeInfo UnityAction_1_t2947_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_30MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TurnOffBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TurnOffBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffBehaviour_t53_m33311(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>
extern Il2CppType UnityAction_1_t2947_0_0_1;
FieldInfo InvokableCall_1_t2946____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2947_0_0_1/* type */
	, &InvokableCall_1_t2946_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2946, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2946_FieldInfos[] =
{
	&InvokableCall_1_t2946____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2946_InvokableCall_1__ctor_m15156_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15156_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15156_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2946_InvokableCall_1__ctor_m15156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15156_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2947_0_0_0;
static ParameterInfo InvokableCall_1_t2946_InvokableCall_1__ctor_m15157_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2947_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15157_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15157_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2946_InvokableCall_1__ctor_m15157_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15157_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2946_InvokableCall_1_Invoke_m15158_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15158_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15158_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2946_InvokableCall_1_Invoke_m15158_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15158_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2946_InvokableCall_1_Find_m15159_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15159_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15159_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2946_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2946_InvokableCall_1_Find_m15159_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15159_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2946_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15156_MethodInfo,
	&InvokableCall_1__ctor_m15157_MethodInfo,
	&InvokableCall_1_Invoke_m15158_MethodInfo,
	&InvokableCall_1_Find_m15159_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2946_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15158_MethodInfo,
	&InvokableCall_1_Find_m15159_MethodInfo,
};
extern TypeInfo UnityAction_1_t2947_il2cpp_TypeInfo;
extern TypeInfo TurnOffBehaviour_t53_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2946_RGCTXData[5] = 
{
	&UnityAction_1_t2947_0_0_0/* Type Usage */,
	&UnityAction_1_t2947_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffBehaviour_t53_m33311_MethodInfo/* Method Usage */,
	&TurnOffBehaviour_t53_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15161_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2946_0_0_0;
extern Il2CppType InvokableCall_1_t2946_1_0_0;
struct InvokableCall_1_t2946;
extern Il2CppGenericClass InvokableCall_1_t2946_GenericClass;
TypeInfo InvokableCall_1_t2946_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2946_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2946_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2946_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2946_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2946_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2946_0_0_0/* byval_arg */
	, &InvokableCall_1_t2946_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2946_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2946_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2946)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2947_UnityAction_1__ctor_m15160_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15160_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15160_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2947_UnityAction_1__ctor_m15160_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15160_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
static ParameterInfo UnityAction_1_t2947_UnityAction_1_Invoke_m15161_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t53_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15161_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15161_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2947_UnityAction_1_Invoke_m15161_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15161_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t53_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2947_UnityAction_1_BeginInvoke_m15162_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t53_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15162_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15162_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2947_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2947_UnityAction_1_BeginInvoke_m15162_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15162_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2947_UnityAction_1_EndInvoke_m15163_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15163_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15163_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2947_UnityAction_1_EndInvoke_m15163_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15163_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2947_MethodInfos[] =
{
	&UnityAction_1__ctor_m15160_MethodInfo,
	&UnityAction_1_Invoke_m15161_MethodInfo,
	&UnityAction_1_BeginInvoke_m15162_MethodInfo,
	&UnityAction_1_EndInvoke_m15163_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15162_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15163_MethodInfo;
static MethodInfo* UnityAction_1_t2947_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15161_MethodInfo,
	&UnityAction_1_BeginInvoke_m15162_MethodInfo,
	&UnityAction_1_EndInvoke_m15163_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2947_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2947_1_0_0;
struct UnityAction_1_t2947;
extern Il2CppGenericClass UnityAction_1_t2947_GenericClass;
TypeInfo UnityAction_1_t2947_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2947_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2947_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2947_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2947_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2947_0_0_0/* byval_arg */
	, &UnityAction_1_t2947_1_0_0/* this_arg */
	, UnityAction_1_t2947_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2947_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2947)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_3.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2948_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_3MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>
extern Il2CppType MeshRenderer_t167_0_0_6;
FieldInfo CastHelper_1_t2948____t_0_FieldInfo = 
{
	"t"/* name */
	, &MeshRenderer_t167_0_0_6/* type */
	, &CastHelper_1_t2948_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2948, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2948____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2948_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2948, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2948_FieldInfos[] =
{
	&CastHelper_1_t2948____t_0_FieldInfo,
	&CastHelper_1_t2948____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2948_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2948_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2948_0_0_0;
extern Il2CppType CastHelper_1_t2948_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2948_GenericClass;
TypeInfo CastHelper_1_t2948_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2948_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2948_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2948_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2948_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2948_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2948_0_0_0/* byval_arg */
	, &CastHelper_1_t2948_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2948_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2948)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.MeshFilter>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_4.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2949_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.MeshFilter>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_4MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.MeshFilter>
extern Il2CppType MeshFilter_t84_0_0_6;
FieldInfo CastHelper_1_t2949____t_0_FieldInfo = 
{
	"t"/* name */
	, &MeshFilter_t84_0_0_6/* type */
	, &CastHelper_1_t2949_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2949, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2949____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2949_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2949, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2949_FieldInfos[] =
{
	&CastHelper_1_t2949____t_0_FieldInfo,
	&CastHelper_1_t2949____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2949_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2949_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2949_0_0_0;
extern Il2CppType CastHelper_1_t2949_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2949_GenericClass;
TypeInfo CastHelper_1_t2949_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2949_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2949_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2949_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2949_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2949_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2949_0_0_0/* byval_arg */
	, &CastHelper_1_t2949_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2949_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2949)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6102_il2cpp_TypeInfo;

// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffWordBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffWordBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43662_MethodInfo;
static PropertyInfo IEnumerator_1_t6102____Current_PropertyInfo = 
{
	&IEnumerator_1_t6102_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43662_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6102_PropertyInfos[] =
{
	&IEnumerator_1_t6102____Current_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43662_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffWordBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43662_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6102_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffWordBehaviour_t54_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43662_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6102_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43662_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6102_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6102_0_0_0;
extern Il2CppType IEnumerator_1_t6102_1_0_0;
struct IEnumerator_1_t6102;
extern Il2CppGenericClass IEnumerator_1_t6102_GenericClass;
TypeInfo IEnumerator_1_t6102_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6102_MethodInfos/* methods */
	, IEnumerator_1_t6102_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6102_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6102_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6102_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6102_0_0_0/* byval_arg */
	, &IEnumerator_1_t6102_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6102_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_92.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2950_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_92MethodDeclarations.h"

extern TypeInfo TurnOffWordBehaviour_t54_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15168_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTurnOffWordBehaviour_t54_m33313_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffWordBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffWordBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTurnOffWordBehaviour_t54_m33313(__this, p0, method) (TurnOffWordBehaviour_t54 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2950____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2950_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2950, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2950____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2950_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2950, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2950_FieldInfos[] =
{
	&InternalEnumerator_1_t2950____array_0_FieldInfo,
	&InternalEnumerator_1_t2950____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15165_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2950____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2950_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15165_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2950____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2950_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15168_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2950_PropertyInfos[] =
{
	&InternalEnumerator_1_t2950____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2950____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2950_InternalEnumerator_1__ctor_m15164_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15164_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15164_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2950_InternalEnumerator_1__ctor_m15164_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15164_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15165_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15165_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2950_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15165_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15166_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15166_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15166_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15167_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15167_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2950_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15167_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15168_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15168_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2950_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffWordBehaviour_t54_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15168_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2950_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15164_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15165_MethodInfo,
	&InternalEnumerator_1_Dispose_m15166_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15167_MethodInfo,
	&InternalEnumerator_1_get_Current_m15168_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15167_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15166_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2950_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15165_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15167_MethodInfo,
	&InternalEnumerator_1_Dispose_m15166_MethodInfo,
	&InternalEnumerator_1_get_Current_m15168_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2950_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6102_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2950_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6102_il2cpp_TypeInfo, 7},
};
extern TypeInfo TurnOffWordBehaviour_t54_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2950_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15168_MethodInfo/* Method Usage */,
	&TurnOffWordBehaviour_t54_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTurnOffWordBehaviour_t54_m33313_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2950_0_0_0;
extern Il2CppType InternalEnumerator_1_t2950_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2950_GenericClass;
TypeInfo InternalEnumerator_1_t2950_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2950_MethodInfos/* methods */
	, InternalEnumerator_1_t2950_PropertyInfos/* properties */
	, InternalEnumerator_1_t2950_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2950_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2950_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2950_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2950_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2950_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2950_1_0_0/* this_arg */
	, InternalEnumerator_1_t2950_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2950_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2950_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2950)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7784_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>
extern MethodInfo ICollection_1_get_Count_m43663_MethodInfo;
static PropertyInfo ICollection_1_t7784____Count_PropertyInfo = 
{
	&ICollection_1_t7784_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43663_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43664_MethodInfo;
static PropertyInfo ICollection_1_t7784____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7784_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43664_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7784_PropertyInfos[] =
{
	&ICollection_1_t7784____Count_PropertyInfo,
	&ICollection_1_t7784____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43663_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43663_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7784_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43663_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43664_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43664_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7784_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43664_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
static ParameterInfo ICollection_1_t7784_ICollection_1_Add_m43665_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t54_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43665_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43665_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7784_ICollection_1_Add_m43665_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43665_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43666_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43666_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43666_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
static ParameterInfo ICollection_1_t7784_ICollection_1_Contains_m43667_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t54_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43667_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43667_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7784_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7784_ICollection_1_Contains_m43667_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43667_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviourU5BU5D_t5382_0_0_0;
extern Il2CppType TurnOffWordBehaviourU5BU5D_t5382_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7784_ICollection_1_CopyTo_m43668_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviourU5BU5D_t5382_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43668_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43668_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7784_ICollection_1_CopyTo_m43668_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43668_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
static ParameterInfo ICollection_1_t7784_ICollection_1_Remove_m43669_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t54_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43669_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43669_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7784_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7784_ICollection_1_Remove_m43669_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43669_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7784_MethodInfos[] =
{
	&ICollection_1_get_Count_m43663_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43664_MethodInfo,
	&ICollection_1_Add_m43665_MethodInfo,
	&ICollection_1_Clear_m43666_MethodInfo,
	&ICollection_1_Contains_m43667_MethodInfo,
	&ICollection_1_CopyTo_m43668_MethodInfo,
	&ICollection_1_Remove_m43669_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7786_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7784_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7786_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7784_0_0_0;
extern Il2CppType ICollection_1_t7784_1_0_0;
struct ICollection_1_t7784;
extern Il2CppGenericClass ICollection_1_t7784_GenericClass;
TypeInfo ICollection_1_t7784_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7784_MethodInfos/* methods */
	, ICollection_1_t7784_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7784_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7784_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7784_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7784_0_0_0/* byval_arg */
	, &ICollection_1_t7784_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7784_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffWordBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffWordBehaviour>
extern Il2CppType IEnumerator_1_t6102_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43670_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffWordBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43670_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7786_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6102_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43670_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7786_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43670_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7786_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7786_0_0_0;
extern Il2CppType IEnumerable_1_t7786_1_0_0;
struct IEnumerable_1_t7786;
extern Il2CppGenericClass IEnumerable_1_t7786_GenericClass;
TypeInfo IEnumerable_1_t7786_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7786_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7786_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7786_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7786_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7786_0_0_0/* byval_arg */
	, &IEnumerable_1_t7786_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7786_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7785_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>
extern MethodInfo IList_1_get_Item_m43671_MethodInfo;
extern MethodInfo IList_1_set_Item_m43672_MethodInfo;
static PropertyInfo IList_1_t7785____Item_PropertyInfo = 
{
	&IList_1_t7785_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43671_MethodInfo/* get */
	, &IList_1_set_Item_m43672_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7785_PropertyInfos[] =
{
	&IList_1_t7785____Item_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
static ParameterInfo IList_1_t7785_IList_1_IndexOf_m43673_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t54_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43673_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43673_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7785_IList_1_IndexOf_m43673_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43673_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
static ParameterInfo IList_1_t7785_IList_1_Insert_m43674_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t54_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43674_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43674_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7785_IList_1_Insert_m43674_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43674_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7785_IList_1_RemoveAt_m43675_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43675_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43675_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7785_IList_1_RemoveAt_m43675_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43675_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7785_IList_1_get_Item_m43671_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43671_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43671_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffWordBehaviour_t54_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7785_IList_1_get_Item_m43671_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43671_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
static ParameterInfo IList_1_t7785_IList_1_set_Item_m43672_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t54_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43672_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43672_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7785_IList_1_set_Item_m43672_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43672_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7785_MethodInfos[] =
{
	&IList_1_IndexOf_m43673_MethodInfo,
	&IList_1_Insert_m43674_MethodInfo,
	&IList_1_RemoveAt_m43675_MethodInfo,
	&IList_1_get_Item_m43671_MethodInfo,
	&IList_1_set_Item_m43672_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7785_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7784_il2cpp_TypeInfo,
	&IEnumerable_1_t7786_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7785_0_0_0;
extern Il2CppType IList_1_t7785_1_0_0;
struct IList_1_t7785;
extern Il2CppGenericClass IList_1_t7785_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7785_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7785_MethodInfos/* methods */
	, IList_1_t7785_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7785_il2cpp_TypeInfo/* element_class */
	, IList_1_t7785_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7785_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7785_0_0_0/* byval_arg */
	, &IList_1_t7785_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7785_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_28.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2951_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_28MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_24.h"
extern TypeInfo InvokableCall_1_t2952_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_24MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15171_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15173_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2951____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2951_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2951, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2951_FieldInfos[] =
{
	&CachedInvokableCall_1_t2951____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2951_CachedInvokableCall_1__ctor_m15169_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t54_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15169_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15169_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2951_CachedInvokableCall_1__ctor_m15169_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15169_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2951_CachedInvokableCall_1_Invoke_m15170_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15170_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15170_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2951_CachedInvokableCall_1_Invoke_m15170_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15170_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2951_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15169_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15170_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15170_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15174_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2951_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15170_MethodInfo,
	&InvokableCall_1_Find_m15174_MethodInfo,
};
extern Il2CppType UnityAction_1_t2953_0_0_0;
extern TypeInfo UnityAction_1_t2953_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffWordBehaviour_t54_m33323_MethodInfo;
extern TypeInfo TurnOffWordBehaviour_t54_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15176_MethodInfo;
extern TypeInfo TurnOffWordBehaviour_t54_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2951_RGCTXData[8] = 
{
	&UnityAction_1_t2953_0_0_0/* Type Usage */,
	&UnityAction_1_t2953_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffWordBehaviour_t54_m33323_MethodInfo/* Method Usage */,
	&TurnOffWordBehaviour_t54_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15176_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15171_MethodInfo/* Method Usage */,
	&TurnOffWordBehaviour_t54_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15173_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2951_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2951_1_0_0;
struct CachedInvokableCall_1_t2951;
extern Il2CppGenericClass CachedInvokableCall_1_t2951_GenericClass;
TypeInfo CachedInvokableCall_1_t2951_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2951_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2951_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2952_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2951_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2951_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2951_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2951_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2951_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2951_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2951_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2951)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_31.h"
extern TypeInfo UnityAction_1_t2953_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_31MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TurnOffWordBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TurnOffWordBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffWordBehaviour_t54_m33323(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>
extern Il2CppType UnityAction_1_t2953_0_0_1;
FieldInfo InvokableCall_1_t2952____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2953_0_0_1/* type */
	, &InvokableCall_1_t2952_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2952, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2952_FieldInfos[] =
{
	&InvokableCall_1_t2952____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2952_InvokableCall_1__ctor_m15171_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15171_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15171_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2952_InvokableCall_1__ctor_m15171_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15171_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2953_0_0_0;
static ParameterInfo InvokableCall_1_t2952_InvokableCall_1__ctor_m15172_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2953_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15172_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15172_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2952_InvokableCall_1__ctor_m15172_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15172_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2952_InvokableCall_1_Invoke_m15173_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15173_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15173_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2952_InvokableCall_1_Invoke_m15173_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15173_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2952_InvokableCall_1_Find_m15174_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15174_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15174_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2952_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2952_InvokableCall_1_Find_m15174_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15174_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2952_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15171_MethodInfo,
	&InvokableCall_1__ctor_m15172_MethodInfo,
	&InvokableCall_1_Invoke_m15173_MethodInfo,
	&InvokableCall_1_Find_m15174_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2952_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15173_MethodInfo,
	&InvokableCall_1_Find_m15174_MethodInfo,
};
extern TypeInfo UnityAction_1_t2953_il2cpp_TypeInfo;
extern TypeInfo TurnOffWordBehaviour_t54_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2952_RGCTXData[5] = 
{
	&UnityAction_1_t2953_0_0_0/* Type Usage */,
	&UnityAction_1_t2953_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffWordBehaviour_t54_m33323_MethodInfo/* Method Usage */,
	&TurnOffWordBehaviour_t54_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15176_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2952_0_0_0;
extern Il2CppType InvokableCall_1_t2952_1_0_0;
struct InvokableCall_1_t2952;
extern Il2CppGenericClass InvokableCall_1_t2952_GenericClass;
TypeInfo InvokableCall_1_t2952_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2952_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2952_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2952_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2952_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2952_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2952_0_0_0/* byval_arg */
	, &InvokableCall_1_t2952_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2952_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2952_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2952)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2953_UnityAction_1__ctor_m15175_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15175_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15175_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2953_UnityAction_1__ctor_m15175_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15175_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
static ParameterInfo UnityAction_1_t2953_UnityAction_1_Invoke_m15176_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t54_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15176_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15176_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2953_UnityAction_1_Invoke_m15176_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15176_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t54_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2953_UnityAction_1_BeginInvoke_m15177_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t54_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15177_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15177_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2953_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2953_UnityAction_1_BeginInvoke_m15177_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15177_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2953_UnityAction_1_EndInvoke_m15178_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15178_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15178_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2953_UnityAction_1_EndInvoke_m15178_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15178_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2953_MethodInfos[] =
{
	&UnityAction_1__ctor_m15175_MethodInfo,
	&UnityAction_1_Invoke_m15176_MethodInfo,
	&UnityAction_1_BeginInvoke_m15177_MethodInfo,
	&UnityAction_1_EndInvoke_m15178_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15177_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15178_MethodInfo;
static MethodInfo* UnityAction_1_t2953_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15176_MethodInfo,
	&UnityAction_1_BeginInvoke_m15177_MethodInfo,
	&UnityAction_1_EndInvoke_m15178_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2953_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2953_1_0_0;
struct UnityAction_1_t2953;
extern Il2CppGenericClass UnityAction_1_t2953_GenericClass;
TypeInfo UnityAction_1_t2953_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2953_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2953_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2953_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2953_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2953_0_0_0/* byval_arg */
	, &UnityAction_1_t2953_1_0_0/* this_arg */
	, UnityAction_1_t2953_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2953_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2953)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6104_il2cpp_TypeInfo;

// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43676_MethodInfo;
static PropertyInfo IEnumerator_1_t6104____Current_PropertyInfo = 
{
	&IEnumerator_1_t6104_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43676_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6104_PropertyInfos[] =
{
	&IEnumerator_1_t6104____Current_PropertyInfo,
	NULL
};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43676_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43676_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6104_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingBehaviour_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43676_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6104_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43676_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6104_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6104_0_0_0;
extern Il2CppType IEnumerator_1_t6104_1_0_0;
struct IEnumerator_1_t6104;
extern Il2CppGenericClass IEnumerator_1_t6104_GenericClass;
TypeInfo IEnumerator_1_t6104_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6104_MethodInfos/* methods */
	, IEnumerator_1_t6104_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6104_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6104_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6104_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6104_0_0_0/* byval_arg */
	, &IEnumerator_1_t6104_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6104_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_93.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2954_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_93MethodDeclarations.h"

extern TypeInfo UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15183_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUserDefinedTargetBuildingBehaviour_t55_m33325_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.UserDefinedTargetBuildingBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.UserDefinedTargetBuildingBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisUserDefinedTargetBuildingBehaviour_t55_m33325(__this, p0, method) (UserDefinedTargetBuildingBehaviour_t55 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2954____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2954_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2954, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2954____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2954_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2954, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2954_FieldInfos[] =
{
	&InternalEnumerator_1_t2954____array_0_FieldInfo,
	&InternalEnumerator_1_t2954____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15180_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2954____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2954_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15180_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2954____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2954_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15183_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2954_PropertyInfos[] =
{
	&InternalEnumerator_1_t2954____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2954____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2954_InternalEnumerator_1__ctor_m15179_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15179_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15179_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2954_InternalEnumerator_1__ctor_m15179_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15179_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15180_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15180_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2954_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15180_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15181_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15181_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15181_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15182_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15182_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2954_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15182_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15183_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15183_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2954_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingBehaviour_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15183_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2954_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15179_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15180_MethodInfo,
	&InternalEnumerator_1_Dispose_m15181_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15182_MethodInfo,
	&InternalEnumerator_1_get_Current_m15183_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15182_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15181_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2954_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15180_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15182_MethodInfo,
	&InternalEnumerator_1_Dispose_m15181_MethodInfo,
	&InternalEnumerator_1_get_Current_m15183_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2954_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6104_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2954_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6104_il2cpp_TypeInfo, 7},
};
extern TypeInfo UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2954_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15183_MethodInfo/* Method Usage */,
	&UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisUserDefinedTargetBuildingBehaviour_t55_m33325_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2954_0_0_0;
extern Il2CppType InternalEnumerator_1_t2954_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2954_GenericClass;
TypeInfo InternalEnumerator_1_t2954_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2954_MethodInfos/* methods */
	, InternalEnumerator_1_t2954_PropertyInfos/* properties */
	, InternalEnumerator_1_t2954_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2954_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2954_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2954_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2954_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2954_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2954_1_0_0/* this_arg */
	, InternalEnumerator_1_t2954_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2954_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2954_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2954)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7787_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern MethodInfo ICollection_1_get_Count_m43677_MethodInfo;
static PropertyInfo ICollection_1_t7787____Count_PropertyInfo = 
{
	&ICollection_1_t7787_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43677_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43678_MethodInfo;
static PropertyInfo ICollection_1_t7787____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7787_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43678_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7787_PropertyInfos[] =
{
	&ICollection_1_t7787____Count_PropertyInfo,
	&ICollection_1_t7787____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43677_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43677_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7787_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43677_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43678_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43678_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7787_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43678_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
static ParameterInfo ICollection_1_t7787_ICollection_1_Add_m43679_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t55_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43679_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43679_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7787_ICollection_1_Add_m43679_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43679_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43680_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43680_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43680_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
static ParameterInfo ICollection_1_t7787_ICollection_1_Contains_m43681_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t55_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43681_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43681_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7787_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7787_ICollection_1_Contains_m43681_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43681_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviourU5BU5D_t5383_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviourU5BU5D_t5383_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7787_ICollection_1_CopyTo_m43682_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviourU5BU5D_t5383_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43682_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43682_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7787_ICollection_1_CopyTo_m43682_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43682_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
static ParameterInfo ICollection_1_t7787_ICollection_1_Remove_m43683_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t55_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43683_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43683_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7787_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7787_ICollection_1_Remove_m43683_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43683_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7787_MethodInfos[] =
{
	&ICollection_1_get_Count_m43677_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43678_MethodInfo,
	&ICollection_1_Add_m43679_MethodInfo,
	&ICollection_1_Clear_m43680_MethodInfo,
	&ICollection_1_Contains_m43681_MethodInfo,
	&ICollection_1_CopyTo_m43682_MethodInfo,
	&ICollection_1_Remove_m43683_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7789_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7787_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7789_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7787_0_0_0;
extern Il2CppType ICollection_1_t7787_1_0_0;
struct ICollection_1_t7787;
extern Il2CppGenericClass ICollection_1_t7787_GenericClass;
TypeInfo ICollection_1_t7787_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7787_MethodInfos/* methods */
	, ICollection_1_t7787_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7787_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7787_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7787_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7787_0_0_0/* byval_arg */
	, &ICollection_1_t7787_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7787_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern Il2CppType IEnumerator_1_t6104_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43684_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43684_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7789_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6104_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43684_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7789_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43684_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7789_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7789_0_0_0;
extern Il2CppType IEnumerable_1_t7789_1_0_0;
struct IEnumerable_1_t7789;
extern Il2CppGenericClass IEnumerable_1_t7789_GenericClass;
TypeInfo IEnumerable_1_t7789_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7789_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7789_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7789_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7789_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7789_0_0_0/* byval_arg */
	, &IEnumerable_1_t7789_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7789_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7788_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern MethodInfo IList_1_get_Item_m43685_MethodInfo;
extern MethodInfo IList_1_set_Item_m43686_MethodInfo;
static PropertyInfo IList_1_t7788____Item_PropertyInfo = 
{
	&IList_1_t7788_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43685_MethodInfo/* get */
	, &IList_1_set_Item_m43686_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7788_PropertyInfos[] =
{
	&IList_1_t7788____Item_PropertyInfo,
	NULL
};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
static ParameterInfo IList_1_t7788_IList_1_IndexOf_m43687_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t55_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43687_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43687_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7788_IList_1_IndexOf_m43687_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43687_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
static ParameterInfo IList_1_t7788_IList_1_Insert_m43688_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t55_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43688_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43688_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7788_IList_1_Insert_m43688_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43688_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7788_IList_1_RemoveAt_m43689_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43689_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43689_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7788_IList_1_RemoveAt_m43689_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43689_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7788_IList_1_get_Item_m43685_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43685_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43685_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingBehaviour_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7788_IList_1_get_Item_m43685_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43685_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
static ParameterInfo IList_1_t7788_IList_1_set_Item_m43686_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t55_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43686_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43686_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7788_IList_1_set_Item_m43686_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43686_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7788_MethodInfos[] =
{
	&IList_1_IndexOf_m43687_MethodInfo,
	&IList_1_Insert_m43688_MethodInfo,
	&IList_1_RemoveAt_m43689_MethodInfo,
	&IList_1_get_Item_m43685_MethodInfo,
	&IList_1_set_Item_m43686_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7788_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7787_il2cpp_TypeInfo,
	&IEnumerable_1_t7789_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7788_0_0_0;
extern Il2CppType IList_1_t7788_1_0_0;
struct IList_1_t7788;
extern Il2CppGenericClass IList_1_t7788_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7788_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7788_MethodInfos/* methods */
	, IList_1_t7788_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7788_il2cpp_TypeInfo/* element_class */
	, IList_1_t7788_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7788_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7788_0_0_0/* byval_arg */
	, &IList_1_t7788_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7788_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7790_il2cpp_TypeInfo;

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBu.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43690_MethodInfo;
static PropertyInfo ICollection_1_t7790____Count_PropertyInfo = 
{
	&ICollection_1_t7790_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43690_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43691_MethodInfo;
static PropertyInfo ICollection_1_t7790____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7790_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43691_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7790_PropertyInfos[] =
{
	&ICollection_1_t7790____Count_PropertyInfo,
	&ICollection_1_t7790____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43690_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43690_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7790_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43690_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43691_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43691_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7790_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43691_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
static ParameterInfo ICollection_1_t7790_ICollection_1_Add_m43692_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43692_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43692_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7790_ICollection_1_Add_m43692_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43692_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43693_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43693_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43693_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
static ParameterInfo ICollection_1_t7790_ICollection_1_Contains_m43694_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43694_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43694_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7790_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7790_ICollection_1_Contains_m43694_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43694_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviourU5BU5D_t5648_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviourU5BU5D_t5648_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7790_ICollection_1_CopyTo_m43695_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviourU5BU5D_t5648_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43695_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43695_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7790_ICollection_1_CopyTo_m43695_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43695_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
static ParameterInfo ICollection_1_t7790_ICollection_1_Remove_m43696_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43696_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43696_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7790_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7790_ICollection_1_Remove_m43696_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43696_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7790_MethodInfos[] =
{
	&ICollection_1_get_Count_m43690_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43691_MethodInfo,
	&ICollection_1_Add_m43692_MethodInfo,
	&ICollection_1_Clear_m43693_MethodInfo,
	&ICollection_1_Contains_m43694_MethodInfo,
	&ICollection_1_CopyTo_m43695_MethodInfo,
	&ICollection_1_Remove_m43696_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7792_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7790_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7792_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7790_0_0_0;
extern Il2CppType ICollection_1_t7790_1_0_0;
struct ICollection_1_t7790;
extern Il2CppGenericClass ICollection_1_t7790_GenericClass;
TypeInfo ICollection_1_t7790_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7790_MethodInfos/* methods */
	, ICollection_1_t7790_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7790_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7790_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7790_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7790_0_0_0/* byval_arg */
	, &ICollection_1_t7790_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7790_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6106_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43697_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43697_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7792_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6106_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43697_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7792_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43697_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7792_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7792_0_0_0;
extern Il2CppType IEnumerable_1_t7792_1_0_0;
struct IEnumerable_1_t7792;
extern Il2CppGenericClass IEnumerable_1_t7792_GenericClass;
TypeInfo IEnumerable_1_t7792_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7792_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7792_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7792_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7792_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7792_0_0_0/* byval_arg */
	, &IEnumerable_1_t7792_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7792_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6106_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43698_MethodInfo;
static PropertyInfo IEnumerator_1_t6106____Current_PropertyInfo = 
{
	&IEnumerator_1_t6106_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43698_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6106_PropertyInfos[] =
{
	&IEnumerator_1_t6106____Current_PropertyInfo,
	NULL
};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43698_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43698_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6106_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43698_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6106_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43698_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6106_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6106_0_0_0;
extern Il2CppType IEnumerator_1_t6106_1_0_0;
struct IEnumerator_1_t6106;
extern Il2CppGenericClass IEnumerator_1_t6106_GenericClass;
TypeInfo IEnumerator_1_t6106_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6106_MethodInfos/* methods */
	, IEnumerator_1_t6106_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6106_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6106_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6106_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6106_0_0_0/* byval_arg */
	, &IEnumerator_1_t6106_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6106_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
