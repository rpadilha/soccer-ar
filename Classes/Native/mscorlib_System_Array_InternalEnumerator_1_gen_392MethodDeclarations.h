﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>
struct InternalEnumerator_1_t4568;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.HashSet`1/Link<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m27620 (InternalEnumerator_1_t4568 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27621 (InternalEnumerator_1_t4568 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
 void InternalEnumerator_1_Dispose_m27622 (InternalEnumerator_1_t4568 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m27623 (InternalEnumerator_1_t4568 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
 Link_t4566  InternalEnumerator_1_get_Current_m27624 (InternalEnumerator_1_t4568 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
