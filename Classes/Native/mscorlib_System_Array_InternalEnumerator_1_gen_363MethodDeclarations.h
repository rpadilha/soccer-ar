﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>>
struct InternalEnumerator_1_t4287;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m25170 (InternalEnumerator_1_t4287 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25171 (InternalEnumerator_1_t4287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>>::Dispose()
 void InternalEnumerator_1_Dispose_m25172 (InternalEnumerator_1_t4287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m25173 (InternalEnumerator_1_t4287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>>::get_Current()
 KeyValuePair_2_t890  InternalEnumerator_1_get_Current_m25174 (InternalEnumerator_1_t4287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
