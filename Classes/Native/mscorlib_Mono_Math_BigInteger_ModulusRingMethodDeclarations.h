﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t1809;
// Mono.Math.BigInteger
struct BigInteger_t1804;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
 void ModulusRing__ctor_m10056 (ModulusRing_t1809 * __this, BigInteger_t1804 * ___modulus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
 void ModulusRing_BarrettReduction_m10057 (ModulusRing_t1809 * __this, BigInteger_t1804 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1804 * ModulusRing_Multiply_m10058 (ModulusRing_t1809 * __this, BigInteger_t1804 * ___a, BigInteger_t1804 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1804 * ModulusRing_Difference_m10059 (ModulusRing_t1809 * __this, BigInteger_t1804 * ___a, BigInteger_t1804 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1804 * ModulusRing_Pow_m10060 (ModulusRing_t1809 * __this, BigInteger_t1804 * ___a, BigInteger_t1804 * ___k, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
 BigInteger_t1804 * ModulusRing_Pow_m10061 (ModulusRing_t1809 * __this, uint32_t ___b, BigInteger_t1804 * ___exp, MethodInfo* method) IL2CPP_METHOD_ATTR;
