﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
struct EnvoyTerminatorSink_t2065;

// System.Void System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::.ctor()
 void EnvoyTerminatorSink__ctor_m11701 (EnvoyTerminatorSink_t2065 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::.cctor()
 void EnvoyTerminatorSink__cctor_m11702 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
