﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t517;
// UnityEngine.UI.Graphic
struct Graphic_t344;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Graphic>
struct IEnumerator_1_t3546;
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t3544;
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t3548;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t348;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m2359(__this, method) (void)IndexedSet_1__ctor_m17827_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m19232(__this, method) (Object_t *)IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17829_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m2358(__this, ___item, method) (void)IndexedSet_1_Add_m17830_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m2361(__this, ___item, method) (bool)IndexedSet_1_Remove_m17831_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m19233(__this, method) (Object_t*)IndexedSet_1_GetEnumerator_m17833_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m19234(__this, method) (void)IndexedSet_1_Clear_m17834_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m19235(__this, ___item, method) (bool)IndexedSet_1_Contains_m17836_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m19236(__this, ___array, ___arrayIndex, method) (void)IndexedSet_1_CopyTo_m17838_gshared((IndexedSet_1_t3400 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m19237(__this, method) (int32_t)IndexedSet_1_get_Count_m17839_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m19238(__this, method) (bool)IndexedSet_1_get_IsReadOnly_m17841_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m19239(__this, ___item, method) (int32_t)IndexedSet_1_IndexOf_m17843_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m19240(__this, ___index, ___item, method) (void)IndexedSet_1_Insert_m17845_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m19241(__this, ___index, method) (void)IndexedSet_1_RemoveAt_m17847_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m19242(__this, ___index, method) (Graphic_t344 *)IndexedSet_1_get_Item_m17848_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m19243(__this, ___index, ___value, method) (void)IndexedSet_1_set_Item_m17850_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m19244(__this, ___match, method) (void)IndexedSet_1_RemoveAll_m17851_gshared((IndexedSet_1_t3400 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m19245(__this, ___sortLayoutFunction, method) (void)IndexedSet_1_Sort_m17852_gshared((IndexedSet_1_t3400 *)__this, (Comparison_1_t2833 *)___sortLayoutFunction, method)
