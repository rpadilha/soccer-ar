﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.Marker>
struct Comparer_1_t4068;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.Marker>
struct Comparer_1_t4068  : public Object_t
{
};
struct Comparer_1_t4068_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.Marker>::_default
	Comparer_1_t4068 * ____default_0;
};
