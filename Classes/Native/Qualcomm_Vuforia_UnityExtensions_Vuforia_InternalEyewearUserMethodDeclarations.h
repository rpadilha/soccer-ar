﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.InternalEyewearUserCalibrator
struct InternalEyewearUserCalibrator_t589;
// Vuforia.InternalEyewear/EyewearCalibrationReading[]
struct EyewearCalibrationReadingU5BU5D_t590;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Boolean Vuforia.InternalEyewearUserCalibrator::init(System.Int32,System.Int32,System.Int32,System.Int32)
 bool InternalEyewearUserCalibrator_init_m2785 (InternalEyewearUserCalibrator_t589 * __this, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.InternalEyewearUserCalibrator::getMinScaleHint()
 float InternalEyewearUserCalibrator_getMinScaleHint_m2786 (InternalEyewearUserCalibrator_t589 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.InternalEyewearUserCalibrator::getMaxScaleHint()
 float InternalEyewearUserCalibrator_getMaxScaleHint_m2787 (InternalEyewearUserCalibrator_t589 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearUserCalibrator::isStereoStretched()
 bool InternalEyewearUserCalibrator_isStereoStretched_m2788 (InternalEyewearUserCalibrator_t589 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearUserCalibrator::getProjectionMatrix(Vuforia.InternalEyewear/EyewearCalibrationReading[],UnityEngine.Matrix4x4)
 bool InternalEyewearUserCalibrator_getProjectionMatrix_m2789 (InternalEyewearUserCalibrator_t589 * __this, EyewearCalibrationReadingU5BU5D_t590* ___readings, Matrix4x4_t176  ___projectionMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.InternalEyewearUserCalibrator::.ctor()
 void InternalEyewearUserCalibrator__ctor_m2790 (InternalEyewearUserCalibrator_t589 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
