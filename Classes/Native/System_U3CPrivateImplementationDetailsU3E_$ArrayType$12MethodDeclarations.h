﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct $ArrayType$12_t1525;
struct $ArrayType$12_t1525_marshaled;

void $ArrayType$12_t1525_marshal(const $ArrayType$12_t1525& unmarshaled, $ArrayType$12_t1525_marshaled& marshaled);
void $ArrayType$12_t1525_marshal_back(const $ArrayType$12_t1525_marshaled& marshaled, $ArrayType$12_t1525& unmarshaled);
void $ArrayType$12_t1525_marshal_cleanup($ArrayType$12_t1525_marshaled& marshaled);
