﻿#pragma once
struct Object_t;
typedef Object_t Il2CppCodeGenObject;
// System.Array
#include "mscorlib_System_Array.h"
typedef Array_t Il2CppCodeGenArray;
struct String_t;
typedef String_t Il2CppCodeGenString;
struct Type_t;
typedef Type_t Il2CppCodeGenType;
struct Exception_t151;
typedef Exception_t151 Il2CppCodeGenException;
struct Exception_t151;
typedef Exception_t151 Il2CppCodeGenException;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
typedef RuntimeTypeHandle_t1754 Il2CppCodeGenRuntimeTypeHandle;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
typedef RuntimeFieldHandle_t1760 Il2CppCodeGenRuntimeFieldHandle;
// System.RuntimeArgumentHandle
#include "mscorlib_System_RuntimeArgumentHandle.h"
typedef RuntimeArgumentHandle_t1769 Il2CppCodeGenRuntimeArgumentHandle;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
typedef RuntimeMethodHandle_t1956 Il2CppCodeGenRuntimeMethodHandle;
struct StringBuilder_t466;
typedef StringBuilder_t466 Il2CppCodeGenStringBuilder;
struct MulticastDelegate_t373;
typedef MulticastDelegate_t373 Il2CppCodeGenMulticastDelegate;
