﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Joystick_Script
struct Joystick_Script_t102;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void Joystick_Script::.ctor()
 void Joystick_Script__ctor_m161 (Joystick_Script_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick_Script::.cctor()
 void Joystick_Script__cctor_m162 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Joystick_Script::get_isFingerDown()
 bool Joystick_Script_get_isFingerDown_m163 (Joystick_Script_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick_Script::set_latchedFinger(System.Int32)
 void Joystick_Script_set_latchedFinger_m164 (Joystick_Script_t102 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Joystick_Script::get_position()
 Vector2_t99  Joystick_Script_get_position_m165 (Joystick_Script_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick_Script::set_position(UnityEngine.Vector2)
 void Joystick_Script_set_position_m166 (Joystick_Script_t102 * __this, Vector2_t99  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick_Script::Reset()
 void Joystick_Script_Reset_m167 (Joystick_Script_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick_Script::Awake()
 void Joystick_Script_Awake_m168 (Joystick_Script_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick_Script::Enable()
 void Joystick_Script_Enable_m169 (Joystick_Script_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick_Script::Disable()
 void Joystick_Script_Disable_m170 (Joystick_Script_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick_Script::Restart()
 void Joystick_Script_Restart_m171 (Joystick_Script_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick_Script::Update()
 void Joystick_Script_Update_m172 (Joystick_Script_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
