﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.MeshRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_143.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshRenderer>
struct CachedInvokableCall_1_t4649  : public InvokableCall_1_t4650
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshRenderer>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
