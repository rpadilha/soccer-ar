﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.FilterMode>
struct InternalEnumerator_1_t4937;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29849 (InternalEnumerator_1_t4937 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850 (InternalEnumerator_1_t4937 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::Dispose()
 void InternalEnumerator_1_Dispose_m29851 (InternalEnumerator_1_t4937 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29852 (InternalEnumerator_1_t4937 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.FilterMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29853 (InternalEnumerator_1_t4937 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
