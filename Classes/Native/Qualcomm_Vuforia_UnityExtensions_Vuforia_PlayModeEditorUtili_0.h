﻿#pragma once
#include <stdint.h>
// Vuforia.IPlayModeEditorUtility
struct IPlayModeEditorUtility_t678;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.PlayModeEditorUtility
struct PlayModeEditorUtility_t679  : public Object_t
{
};
struct PlayModeEditorUtility_t679_StaticFields{
	// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::sInstance
	Object_t * ___sInstance_0;
};
