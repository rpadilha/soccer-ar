﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ChooseTeam
struct ChooseTeam_t88;

// System.Void ChooseTeam::.ctor()
 void ChooseTeam__ctor_m138 (ChooseTeam_t88 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChooseTeam::Start()
 void ChooseTeam_Start_m139 (ChooseTeam_t88 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChooseTeam::Update()
 void ChooseTeam_Update_m140 (ChooseTeam_t88 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
