﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.BitArray/BitArrayEnumerator
struct BitArrayEnumerator_t1879;
// System.Object
struct Object_t;
// System.Collections.BitArray
struct BitArray_t1479;

// System.Void System.Collections.BitArray/BitArrayEnumerator::.ctor(System.Collections.BitArray)
 void BitArrayEnumerator__ctor_m10506 (BitArrayEnumerator_t1879 * __this, BitArray_t1479 * ___ba, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.BitArray/BitArrayEnumerator::get_Current()
 Object_t * BitArrayEnumerator_get_Current_m10507 (BitArrayEnumerator_t1879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray/BitArrayEnumerator::MoveNext()
 bool BitArrayEnumerator_MoveNext_m10508 (BitArrayEnumerator_t1879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray/BitArrayEnumerator::checkVersion()
 void BitArrayEnumerator_checkVersion_m10509 (BitArrayEnumerator_t1879 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
