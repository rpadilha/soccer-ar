﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.QCARRenderer/Vec2I
#pragma pack(push, tp, 1)
struct Vec2I_t675 
{
	// System.Int32 Vuforia.QCARRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.QCARRenderer/Vec2I::y
	int32_t ___y_1;
};
#pragma pack(pop, tp)
