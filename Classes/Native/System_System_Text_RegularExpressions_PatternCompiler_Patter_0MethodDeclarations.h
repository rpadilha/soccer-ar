﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
struct PatternLinkStack_t1476;
// System.Object
struct Object_t;

// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::.ctor()
 void PatternLinkStack__ctor_m7431 (PatternLinkStack_t1476 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::set_BaseAddress(System.Int32)
 void PatternLinkStack_set_BaseAddress_m7432 (PatternLinkStack_t1476 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::get_OffsetAddress()
 int32_t PatternLinkStack_get_OffsetAddress_m7433 (PatternLinkStack_t1476 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::set_OffsetAddress(System.Int32)
 void PatternLinkStack_set_OffsetAddress_m7434 (PatternLinkStack_t1476 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::GetOffset(System.Int32)
 int32_t PatternLinkStack_GetOffset_m7435 (PatternLinkStack_t1476 * __this, int32_t ___target_addr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::GetCurrent()
 Object_t * PatternLinkStack_GetCurrent_m7436 (PatternLinkStack_t1476 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::SetCurrent(System.Object)
 void PatternLinkStack_SetCurrent_m7437 (PatternLinkStack_t1476 * __this, Object_t * ___l, MethodInfo* method) IL2CPP_METHOD_ATTR;
