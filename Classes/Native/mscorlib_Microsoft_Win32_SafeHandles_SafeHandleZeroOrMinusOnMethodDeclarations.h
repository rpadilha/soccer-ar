﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
struct SafeHandleZeroOrMinusOneIsInvalid_t1778;

// System.Void Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid::.ctor(System.Boolean)
 void SafeHandleZeroOrMinusOneIsInvalid__ctor_m9933 (SafeHandleZeroOrMinusOneIsInvalid_t1778 * __this, bool ___ownsHandle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid::get_IsInvalid()
 bool SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m9934 (SafeHandleZeroOrMinusOneIsInvalid_t1778 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
