﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARAbstractBehaviour
struct QCARAbstractBehaviour_t45;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t17;
// UnityEngine.Camera
struct Camera_t168;
// System.String
struct String_t;
// System.Action`1<Vuforia.QCARUnity/InitError>
struct Action_1_t117;
// System.Action
struct Action_t147;
// System.Action`1<System.Boolean>
struct Action_1_t802;
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t805;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t112;
// UnityEngine.GameObject
struct GameObject_t29;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t139;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio_0.h"
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
// Vuforia.QCARRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB.h"

// System.Boolean Vuforia.QCARAbstractBehaviour::get_AutoAdjustStereoCameraSkewing()
 bool QCARAbstractBehaviour_get_AutoAdjustStereoCameraSkewing_m4260 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARAbstractBehaviour::get_SceneScaleFactor()
 float QCARAbstractBehaviour_get_SceneScaleFactor_m4261 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_SceneScaleFactor(System.Single)
 void QCARAbstractBehaviour_set_SceneScaleFactor_m4262 (QCARAbstractBehaviour_t45 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARAbstractBehaviour::get_CameraOffset()
 float QCARAbstractBehaviour_get_CameraOffset_m4263 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_CameraOffset(System.Single)
 void QCARAbstractBehaviour_set_CameraOffset_m4264 (QCARAbstractBehaviour_t45 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode Vuforia.QCARAbstractBehaviour::get_WorldCenterModeSetting()
 int32_t QCARAbstractBehaviour_get_WorldCenterModeSetting_m4265 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TrackableBehaviour Vuforia.QCARAbstractBehaviour::get_WorldCenter()
 TrackableBehaviour_t17 * QCARAbstractBehaviour_get_WorldCenter_m4266 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::get_VideoBackGroundMirrored()
 bool QCARAbstractBehaviour_get_VideoBackGroundMirrored_m4267 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
 void QCARAbstractBehaviour_set_VideoBackGroundMirrored_m4268 (QCARAbstractBehaviour_t45 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/CameraDeviceMode Vuforia.QCARAbstractBehaviour::get_CameraDeviceMode()
 int32_t QCARAbstractBehaviour_get_CameraDeviceMode_m4269 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::get_HasStarted()
 bool QCARAbstractBehaviour_get_HasStarted_m4270 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::get_IsStereoRendering()
 bool QCARAbstractBehaviour_get_IsStereoRendering_m4271 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.QCARAbstractBehaviour::get_PrimaryCamera()
 Camera_t168 * QCARAbstractBehaviour_get_PrimaryCamera_m4272 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_PrimaryCamera(UnityEngine.Camera)
 void QCARAbstractBehaviour_set_PrimaryCamera_m4273 (QCARAbstractBehaviour_t45 * __this, Camera_t168 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.QCARAbstractBehaviour::get_SecondaryCamera()
 Camera_t168 * QCARAbstractBehaviour_get_SecondaryCamera_m4274 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_SecondaryCamera(UnityEngine.Camera)
 void QCARAbstractBehaviour_set_SecondaryCamera_m4275 (QCARAbstractBehaviour_t45 * __this, Camera_t168 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.QCARAbstractBehaviour::get_AppLicenseKey()
 String_t* QCARAbstractBehaviour_get_AppLicenseKey_m4276 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetAutoAdjustStereoCameraSkewing(System.Boolean)
 void QCARAbstractBehaviour_SetAutoAdjustStereoCameraSkewing_m4277 (QCARAbstractBehaviour_t45 * __this, bool ___setSkewing, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetSceneScaleFactor(System.Single)
 void QCARAbstractBehaviour_SetSceneScaleFactor_m4278 (QCARAbstractBehaviour_t45 * __this, float ___Scale, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterQCARInitErrorCallback(System.Action`1<Vuforia.QCARUnity/InitError>)
 void QCARAbstractBehaviour_RegisterQCARInitErrorCallback_m259 (QCARAbstractBehaviour_t45 * __this, Action_1_t117 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterQCARInitErrorCallback(System.Action`1<Vuforia.QCARUnity/InitError>)
 void QCARAbstractBehaviour_UnregisterQCARInitErrorCallback_m265 (QCARAbstractBehaviour_t45 * __this, Action_1_t117 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterQCARInitializedCallback(System.Action)
 void QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4279 (QCARAbstractBehaviour_t45 * __this, Action_t147 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterQCARInitializedCallback(System.Action)
 void QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4280 (QCARAbstractBehaviour_t45 * __this, Action_t147 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterQCARStartedCallback(System.Action)
 void QCARAbstractBehaviour_RegisterQCARStartedCallback_m4281 (QCARAbstractBehaviour_t45 * __this, Action_t147 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterQCARStartedCallback(System.Action)
 void QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4282 (QCARAbstractBehaviour_t45 * __this, Action_t147 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterTrackablesUpdatedCallback(System.Action)
 void QCARAbstractBehaviour_RegisterTrackablesUpdatedCallback_m4283 (QCARAbstractBehaviour_t45 * __this, Action_t147 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterTrackablesUpdatedCallback(System.Action)
 void QCARAbstractBehaviour_UnregisterTrackablesUpdatedCallback_m4284 (QCARAbstractBehaviour_t45 * __this, Action_t147 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterOnPauseCallback(System.Action`1<System.Boolean>)
 void QCARAbstractBehaviour_RegisterOnPauseCallback_m4285 (QCARAbstractBehaviour_t45 * __this, Action_1_t802 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterOnPauseCallback(System.Action`1<System.Boolean>)
 void QCARAbstractBehaviour_UnregisterOnPauseCallback_m4286 (QCARAbstractBehaviour_t45 * __this, Action_1_t802 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetEditorValues(System.Single)
 void QCARAbstractBehaviour_SetEditorValues_m4287 (QCARAbstractBehaviour_t45 * __this, float ___Offset, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterTrackerEventHandler(Vuforia.ITrackerEventHandler)
 void QCARAbstractBehaviour_RegisterTrackerEventHandler_m4288 (QCARAbstractBehaviour_t45 * __this, Object_t * ___trackerEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::UnregisterTrackerEventHandler(Vuforia.ITrackerEventHandler)
 bool QCARAbstractBehaviour_UnregisterTrackerEventHandler_m4289 (QCARAbstractBehaviour_t45 * __this, Object_t * ___trackerEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterVideoBgEventHandler(Vuforia.IVideoBackgroundEventHandler)
 void QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4290 (QCARAbstractBehaviour_t45 * __this, Object_t * ___videoBgEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::UnregisterVideoBgEventHandler(Vuforia.IVideoBackgroundEventHandler)
 bool QCARAbstractBehaviour_UnregisterVideoBgEventHandler_m4291 (QCARAbstractBehaviour_t45 * __this, Object_t * ___videoBgEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetWorldCenterMode(Vuforia.QCARAbstractBehaviour/WorldCenterMode)
 void QCARAbstractBehaviour_SetWorldCenterMode_m4292 (QCARAbstractBehaviour_t45 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetWorldCenter(Vuforia.TrackableBehaviour)
 void QCARAbstractBehaviour_SetWorldCenter_m4293 (QCARAbstractBehaviour_t45 * __this, TrackableBehaviour_t17 * ___trackable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetAppLicenseKey(System.String)
 void QCARAbstractBehaviour_SetAppLicenseKey_m4294 (QCARAbstractBehaviour_t45 * __this, String_t* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.QCARAbstractBehaviour::GetViewportRectangle()
 Rect_t103  QCARAbstractBehaviour_GetViewportRectangle_m4295 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.QCARAbstractBehaviour::GetSurfaceOrientation()
 int32_t QCARAbstractBehaviour_GetSurfaceOrientation_m4296 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ConfigureVideoBackground(System.Boolean)
 void QCARAbstractBehaviour_ConfigureVideoBackground_m4297 (QCARAbstractBehaviour_t45 * __this, bool ___forceReflectionSetting, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
 void QCARAbstractBehaviour_ResetBackgroundPlane_m4298 (QCARAbstractBehaviour_t45 * __this, bool ___disable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterRenderOnUpdateCallback(System.Action)
 void QCARAbstractBehaviour_RegisterRenderOnUpdateCallback_m4299 (QCARAbstractBehaviour_t45 * __this, Action_t147 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterRenderOnUpdateCallback(System.Action)
 void QCARAbstractBehaviour_UnregisterRenderOnUpdateCallback_m4300 (QCARAbstractBehaviour_t45 * __this, Action_t147 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ConfigureView()
 void QCARAbstractBehaviour_ConfigureView_m4301 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::EnableObjectRenderer(UnityEngine.GameObject,System.Boolean)
 void QCARAbstractBehaviour_EnableObjectRenderer_m4302 (QCARAbstractBehaviour_t45 * __this, GameObject_t29 * ___go, bool ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Start()
 void QCARAbstractBehaviour_Start_m4303 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnEnable()
 void QCARAbstractBehaviour_OnEnable_m4304 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UpdateView()
 void QCARAbstractBehaviour_UpdateView_m4305 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Update()
 void QCARAbstractBehaviour_Update_m4306 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnApplicationPause(System.Boolean)
 void QCARAbstractBehaviour_OnApplicationPause_m4307 (QCARAbstractBehaviour_t45 * __this, bool ___pause, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnDisable()
 void QCARAbstractBehaviour_OnDisable_m4308 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnDestroy()
 void QCARAbstractBehaviour_OnDestroy_m4309 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetUnityPlayerImplementation(Vuforia.IUnityPlayer)
 void QCARAbstractBehaviour_SetUnityPlayerImplementation_m418 (QCARAbstractBehaviour_t45 * __this, Object_t * ___implementation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::StartQCAR(System.Boolean,System.Boolean)
 bool QCARAbstractBehaviour_StartQCAR_m4310 (QCARAbstractBehaviour_t45 * __this, bool ___startObjectTracker, bool ___startMarkerTracker, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::StopQCAR()
 bool QCARAbstractBehaviour_StopQCAR_m4311 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UpdateStereoDepth()
 void QCARAbstractBehaviour_UpdateStereoDepth_m4312 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ProjectionMatricesUpdated()
 void QCARAbstractBehaviour_ProjectionMatricesUpdated_m4313 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ApplyMatrix(UnityEngine.Camera,UnityEngine.Matrix4x4)
 void QCARAbstractBehaviour_ApplyMatrix_m4314 (QCARAbstractBehaviour_t45 * __this, Camera_t168 * ___cam, Matrix4x4_t176  ___inputMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UpdateProjection(UnityEngine.ScreenOrientation)
 void QCARAbstractBehaviour_UpdateProjection_m4315 (QCARAbstractBehaviour_t45 * __this, int32_t ___orientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::DeinitRequestedTrackers()
 void QCARAbstractBehaviour_DeinitRequestedTrackers_m4316 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::CheckSceneScaleFactor()
 void QCARAbstractBehaviour_CheckSceneScaleFactor_m4317 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::CheckForSurfaceChanges()
 void QCARAbstractBehaviour_CheckForSurfaceChanges_m4318 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetCameraDeviceMode(Vuforia.CameraDevice/CameraDeviceMode)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDeviceMode_m421 (QCARAbstractBehaviour_t45 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetMaximumSimultaneousImageTargets()
 int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousImageTargets_m422 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetMaximumSimultaneousImageTargets(System.Int32)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousImageTargets_m423 (QCARAbstractBehaviour_t45 * __this, int32_t ___max, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetMaximumSimultaneousObjectTargets()
 int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousObjectTargets_m424 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetMaximumSimultaneousObjectTargets(System.Int32)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousObjectTargets_m425 (QCARAbstractBehaviour_t45 * __this, int32_t ___max, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetDelayedLoadingObjectTargets()
 bool QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetDelayedLoadingObjectTargets_m426 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetUseDelayedLoadingObjectTargets(System.Boolean)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetUseDelayedLoadingObjectTargets_m427 (QCARAbstractBehaviour_t45 * __this, bool ___useDelayedLoading, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/CameraDirection Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetCameraDirection()
 int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetCameraDirection_m428 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetCameraDirection(Vuforia.CameraDevice/CameraDirection)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDirection_m429 (QCARAbstractBehaviour_t45 * __this, int32_t ___cameraDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoBackgroundReflection Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetMirrorVideoBackground()
 int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMirrorVideoBackground_m430 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetMirrorVideoBackground(Vuforia.QCARRenderer/VideoBackgroundReflection)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMirrorVideoBackground_m431 (QCARAbstractBehaviour_t45 * __this, int32_t ___reflection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::.ctor()
 void QCARAbstractBehaviour__ctor_m413 (QCARAbstractBehaviour_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
