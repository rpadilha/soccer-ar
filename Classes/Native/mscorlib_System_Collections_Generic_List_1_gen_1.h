﻿#pragma once
#include <stdint.h>
// Joystick_Script[]
struct Joystick_ScriptU5BU5D_t3069;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Joystick_Script>
struct List_1_t101  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Joystick_Script>::_items
	Joystick_ScriptU5BU5D_t3069* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Joystick_Script>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Joystick_Script>::_version
	int32_t ____version_3;
};
struct List_1_t101_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Joystick_Script>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Joystick_Script>::EmptyArray
	Joystick_ScriptU5BU5D_t3069* ___EmptyArray_4;
};
