﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_t424;
// UnityEngine.RectTransform
struct RectTransform_t338;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void UnityEngine.UI.AspectRatioFitter::.ctor()
 void AspectRatioFitter__ctor_m1814 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::get_aspectMode()
 int32_t AspectRatioFitter_get_aspectMode_m1815 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectMode(UnityEngine.UI.AspectRatioFitter/AspectMode)
 void AspectRatioFitter_set_aspectMode_m1816 (AspectRatioFitter_t424 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.AspectRatioFitter::get_aspectRatio()
 float AspectRatioFitter_get_aspectRatio_m1817 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
 void AspectRatioFitter_set_aspectRatio_m1818 (AspectRatioFitter_t424 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::get_rectTransform()
 RectTransform_t338 * AspectRatioFitter_get_rectTransform_m1819 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::OnEnable()
 void AspectRatioFitter_OnEnable_m1820 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::OnDisable()
 void AspectRatioFitter_OnDisable_m1821 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::OnRectTransformDimensionsChange()
 void AspectRatioFitter_OnRectTransformDimensionsChange_m1822 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::UpdateRect()
 void AspectRatioFitter_UpdateRect_m1823 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.AspectRatioFitter::GetSizeDeltaToProduceSize(System.Single,System.Int32)
 float AspectRatioFitter_GetSizeDeltaToProduceSize_m1824 (AspectRatioFitter_t424 * __this, float ___size, int32_t ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.AspectRatioFitter::GetParentSize()
 Vector2_t99  AspectRatioFitter_GetParentSize_m1825 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutHorizontal()
 void AspectRatioFitter_SetLayoutHorizontal_m1826 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutVertical()
 void AspectRatioFitter_SetLayoutVertical_m1827 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::SetDirty()
 void AspectRatioFitter_SetDirty_m1828 (AspectRatioFitter_t424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
