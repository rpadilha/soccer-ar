﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
struct List_1_t808;
// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t809;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>
struct Enumerator_t943 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::l
	List_1_t808 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::current
	Object_t * ___current_3;
};
