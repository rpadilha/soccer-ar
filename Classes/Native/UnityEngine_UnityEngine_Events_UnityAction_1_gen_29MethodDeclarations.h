﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>
struct UnityAction_1_t2942;
// System.Object
struct Object_t;
// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t52;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::.ctor(System.Object,System.IntPtr)
// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6MethodDeclarations.h"
#define UnityAction_1__ctor_m15140(__this, ___object, ___method, method) (void)UnityAction_1__ctor_m14203_gshared((UnityAction_1_t2763 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::Invoke(T0)
#define UnityAction_1_Invoke_m15141(__this, ___arg0, method) (void)UnityAction_1_Invoke_m14204_gshared((UnityAction_1_t2763 *)__this, (Object_t *)___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m15142(__this, ___arg0, ___callback, ___object, method) (Object_t *)UnityAction_1_BeginInvoke_m14205_gshared((UnityAction_1_t2763 *)__this, (Object_t *)___arg0, (AsyncCallback_t251 *)___callback, (Object_t *)___object, method)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m15143(__this, ___result, method) (void)UnityAction_1_EndInvoke_m14206_gshared((UnityAction_1_t2763 *)__this, (Object_t *)___result, method)
