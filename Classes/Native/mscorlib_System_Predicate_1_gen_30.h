﻿#pragma once
#include <stdint.h>
// Vuforia.DataSet
struct DataSet_t612;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.DataSet>
struct Predicate_1_t4034  : public MulticastDelegate_t373
{
};
