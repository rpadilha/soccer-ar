﻿#pragma once
#include <stdint.h>
// Vuforia.ITrackerEventHandler[]
struct ITrackerEventHandlerU5BU5D_t4488;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>
struct List_1_t800  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::_items
	ITrackerEventHandlerU5BU5D_t4488* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t800_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::EmptyArray
	ITrackerEventHandlerU5BU5D_t4488* ___EmptyArray_4;
};
