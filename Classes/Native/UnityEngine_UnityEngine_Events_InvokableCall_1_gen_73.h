﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.TouchInputModule>
struct UnityAction_1_t3374;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.TouchInputModule>
struct InvokableCall_1_t3373  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.TouchInputModule>::Delegate
	UnityAction_1_t3374 * ___Delegate_0;
};
