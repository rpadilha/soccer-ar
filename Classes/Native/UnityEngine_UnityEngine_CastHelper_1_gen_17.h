﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t168;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.Camera>
struct CastHelper_1_t3138 
{
	// T UnityEngine.CastHelper`1<UnityEngine.Camera>::t
	Camera_t168 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.Camera>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
