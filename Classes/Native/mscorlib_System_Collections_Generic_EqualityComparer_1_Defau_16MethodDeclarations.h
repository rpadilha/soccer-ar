﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct DefaultComparer_t3500;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t506;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor()
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_0MethodDeclarations.h"
#define DefaultComparer__ctor_m18692(__this, method) (void)DefaultComparer__ctor_m14665_gshared((DefaultComparer_t2847 *)__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.List`1<UnityEngine.UI.Text>>::GetHashCode(T)
#define DefaultComparer_GetHashCode_m18693(__this, ___obj, method) (int32_t)DefaultComparer_GetHashCode_m14666_gshared((DefaultComparer_t2847 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Equals(T,T)
#define DefaultComparer_Equals_m18694(__this, ___x, ___y, method) (bool)DefaultComparer_Equals_m14667_gshared((DefaultComparer_t2847 *)__this, (Object_t *)___x, (Object_t *)___y, method)
