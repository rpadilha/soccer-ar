﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>
struct InvokableCall_1_t4797;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t141;
// UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>
struct UnityAction_1_t4798;
// System.Object[]
struct ObjectU5BU5D_t130;

// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::.ctor(System.Object,System.Reflection.MethodInfo)
// UnityEngine.Events.InvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_0MethodDeclarations.h"
#define InvokableCall_1__ctor_m29005(__this, ___target, ___theFunction, method) (void)InvokableCall_1__ctor_m14199_gshared((InvokableCall_1_t2762 *)__this, (Object_t *)___target, (MethodInfo_t141 *)___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
#define InvokableCall_1__ctor_m29006(__this, ___callback, method) (void)InvokableCall_1__ctor_m14200_gshared((InvokableCall_1_t2762 *)__this, (UnityAction_1_t2763 *)___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::Invoke(System.Object[])
#define InvokableCall_1_Invoke_m29007(__this, ___args, method) (void)InvokableCall_1_Invoke_m14201_gshared((InvokableCall_1_t2762 *)__this, (ObjectU5BU5D_t130*)___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::Find(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1_Find_m29008(__this, ___targetObj, ___method, method) (bool)InvokableCall_1_Find_m14202_gshared((InvokableCall_1_t2762 *)__this, (Object_t *)___targetObj, (MethodInfo_t141 *)___method, method)
