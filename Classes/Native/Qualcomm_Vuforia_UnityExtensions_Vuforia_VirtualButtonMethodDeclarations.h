﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VirtualButton
struct VirtualButton_t639;
// System.String
struct String_t;
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
// Vuforia.VirtualButton/Sensitivity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"

// System.String Vuforia.VirtualButton::get_Name()
// System.Int32 Vuforia.VirtualButton::get_ID()
// System.Boolean Vuforia.VirtualButton::get_Enabled()
// Vuforia.RectangleData Vuforia.VirtualButton::get_Area()
// System.Boolean Vuforia.VirtualButton::SetArea(Vuforia.RectangleData)
// System.Boolean Vuforia.VirtualButton::SetSensitivity(Vuforia.VirtualButton/Sensitivity)
// System.Boolean Vuforia.VirtualButton::SetEnabled(System.Boolean)
// System.Void Vuforia.VirtualButton::.ctor()
 void VirtualButton__ctor_m4212 (VirtualButton_t639 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
