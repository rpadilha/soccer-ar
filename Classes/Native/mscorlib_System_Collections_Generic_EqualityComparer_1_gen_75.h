﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct EqualityComparer_1_t5332;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct EqualityComparer_1_t5332  : public Object_t
{
};
struct EqualityComparer_1_t5332_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTime>::_default
	EqualityComparer_1_t5332 * ____default_0;
};
