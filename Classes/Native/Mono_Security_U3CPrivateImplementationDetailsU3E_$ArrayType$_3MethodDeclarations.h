﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$48
struct $ArrayType$48_t1704;
struct $ArrayType$48_t1704_marshaled;

void $ArrayType$48_t1704_marshal(const $ArrayType$48_t1704& unmarshaled, $ArrayType$48_t1704_marshaled& marshaled);
void $ArrayType$48_t1704_marshal_back(const $ArrayType$48_t1704_marshaled& marshaled, $ArrayType$48_t1704& unmarshaled);
void $ArrayType$48_t1704_marshal_cleanup($ArrayType$48_t1704_marshaled& marshaled);
