﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceBehaviour
struct SurfaceBehaviour_t13;

// System.Void Vuforia.SurfaceBehaviour::.ctor()
 void SurfaceBehaviour__ctor_m85 (SurfaceBehaviour_t13 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
