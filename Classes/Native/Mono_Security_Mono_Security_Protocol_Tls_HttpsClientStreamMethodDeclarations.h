﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.HttpsClientStream
struct HttpsClientStream_t1666;
// System.IO.Stream
struct Stream_t1652;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1388;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1389;
// System.Byte[]
struct ByteU5BU5D_t653;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1373;
// System.Int32[]
struct Int32U5BU5D_t175;
// System.String
struct String_t;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1403;

// System.Void Mono.Security.Protocol.Tls.HttpsClientStream::.ctor(System.IO.Stream,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Net.HttpWebRequest,System.Byte[])
 void HttpsClientStream__ctor_m8621 (HttpsClientStream_t1666 * __this, Stream_t1652 * ___stream, X509CertificateCollection_t1388 * ___clientCertificates, HttpWebRequest_t1389 * ___request, ByteU5BU5D_t653* ___buffer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.HttpsClientStream::get_TrustFailure()
 bool HttpsClientStream_get_TrustFailure_m8622 (HttpsClientStream_t1666 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.HttpsClientStream::RaiseServerCertificateValidation(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[])
 bool HttpsClientStream_RaiseServerCertificateValidation_m8623 (HttpsClientStream_t1666 * __this, X509Certificate_t1373 * ___certificate, Int32U5BU5D_t175* ___certificateErrors, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.HttpsClientStream::<HttpsClientStream>m__0(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection)
 X509Certificate_t1373 * HttpsClientStream_U3CHttpsClientStreamU3Em__0_m8624 (Object_t * __this/* static, unused */, X509CertificateCollection_t1388 * ___clientCerts, X509Certificate_t1373 * ___serverCertificate, String_t* ___targetHost, X509CertificateCollection_t1388 * ___serverRequestedCertificates, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.HttpsClientStream::<HttpsClientStream>m__1(System.Security.Cryptography.X509Certificates.X509Certificate,System.String)
 AsymmetricAlgorithm_t1403 * HttpsClientStream_U3CHttpsClientStreamU3Em__1_m8625 (Object_t * __this/* static, unused */, X509Certificate_t1373 * ___certificate, String_t* ___targetHost, MethodInfo* method) IL2CPP_METHOD_ATTR;
