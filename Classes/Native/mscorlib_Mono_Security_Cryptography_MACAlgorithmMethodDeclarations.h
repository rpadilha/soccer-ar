﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Cryptography.MACAlgorithm
struct MACAlgorithm_t1818;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t1619;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void Mono.Security.Cryptography.MACAlgorithm::.ctor(System.Security.Cryptography.SymmetricAlgorithm)
 void MACAlgorithm__ctor_m10193 (MACAlgorithm_t1818 * __this, SymmetricAlgorithm_t1619 * ___algorithm, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.MACAlgorithm::Initialize(System.Byte[])
 void MACAlgorithm_Initialize_m10194 (MACAlgorithm_t1818 * __this, ByteU5BU5D_t653* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.MACAlgorithm::Core(System.Byte[],System.Int32,System.Int32)
 void MACAlgorithm_Core_m10195 (MACAlgorithm_t1818 * __this, ByteU5BU5D_t653* ___rgb, int32_t ___ib, int32_t ___cb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.MACAlgorithm::Final()
 ByteU5BU5D_t653* MACAlgorithm_Final_m10196 (MACAlgorithm_t1818 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
