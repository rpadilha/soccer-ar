﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.ITrackerEventHandler>
struct EqualityComparer_1_t4498;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.ITrackerEventHandler>
struct EqualityComparer_1_t4498  : public Object_t
{
};
struct EqualityComparer_1_t4498_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.ITrackerEventHandler>::_default
	EqualityComparer_1_t4498 * ____default_0;
};
