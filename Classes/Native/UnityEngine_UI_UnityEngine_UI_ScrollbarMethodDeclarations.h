﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Scrollbar
struct Scrollbar_t392;
// UnityEngine.RectTransform
struct RectTransform_t338;
// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_t389;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t239;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t247;
// UnityEngine.UI.Selectable
struct Selectable_t324;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.UI.Scrollbar/Direction
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
// UnityEngine.UI.Scrollbar/Axis
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
// UnityEngine.UI.CanvasUpdate
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate.h"

// System.Void UnityEngine.UI.Scrollbar::.ctor()
 void Scrollbar__ctor_m1524 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::get_handleRect()
 RectTransform_t338 * Scrollbar_get_handleRect_m1525 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_handleRect(UnityEngine.RectTransform)
 void Scrollbar_set_handleRect_m1526 (Scrollbar_t392 * __this, RectTransform_t338 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Scrollbar/Direction UnityEngine.UI.Scrollbar::get_direction()
 int32_t Scrollbar_get_direction_m1527 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_direction(UnityEngine.UI.Scrollbar/Direction)
 void Scrollbar_set_direction_m1528 (Scrollbar_t392 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Scrollbar::get_value()
 float Scrollbar_get_value_m1529 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_value(System.Single)
 void Scrollbar_set_value_m1530 (Scrollbar_t392 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Scrollbar::get_size()
 float Scrollbar_get_size_m1531 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_size(System.Single)
 void Scrollbar_set_size_m1532 (Scrollbar_t392 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.Scrollbar::get_numberOfSteps()
 int32_t Scrollbar_get_numberOfSteps_m1533 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_numberOfSteps(System.Int32)
 void Scrollbar_set_numberOfSteps_m1534 (Scrollbar_t392 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Scrollbar/ScrollEvent UnityEngine.UI.Scrollbar::get_onValueChanged()
 ScrollEvent_t389 * Scrollbar_get_onValueChanged_m1535 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_onValueChanged(UnityEngine.UI.Scrollbar/ScrollEvent)
 void Scrollbar_set_onValueChanged_m1536 (Scrollbar_t392 * __this, ScrollEvent_t389 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Scrollbar::get_stepSize()
 float Scrollbar_get_stepSize_m1537 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::Rebuild(UnityEngine.UI.CanvasUpdate)
 void Scrollbar_Rebuild_m1538 (Scrollbar_t392 * __this, int32_t ___executing, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnEnable()
 void Scrollbar_OnEnable_m1539 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnDisable()
 void Scrollbar_OnDisable_m1540 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::UpdateCachedReferences()
 void Scrollbar_UpdateCachedReferences_m1541 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::Set(System.Single)
 void Scrollbar_Set_m1542 (Scrollbar_t392 * __this, float ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::Set(System.Single,System.Boolean)
 void Scrollbar_Set_m1543 (Scrollbar_t392 * __this, float ___input, bool ___sendCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnRectTransformDimensionsChange()
 void Scrollbar_OnRectTransformDimensionsChange_m1544 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Scrollbar/Axis UnityEngine.UI.Scrollbar::get_axis()
 int32_t Scrollbar_get_axis_m1545 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Scrollbar::get_reverseValue()
 bool Scrollbar_get_reverseValue_m1546 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::UpdateVisuals()
 void Scrollbar_UpdateVisuals_m1547 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::UpdateDrag(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_UpdateDrag_m1548 (Scrollbar_t392 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Scrollbar::MayDrag(UnityEngine.EventSystems.PointerEventData)
 bool Scrollbar_MayDrag_m1549 (Scrollbar_t392 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_OnBeginDrag_m1550 (Scrollbar_t392 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnDrag(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_OnDrag_m1551 (Scrollbar_t392 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_OnPointerDown_m1552 (Scrollbar_t392 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.Scrollbar::ClickRepeat(UnityEngine.EventSystems.PointerEventData)
 Object_t * Scrollbar_ClickRepeat_m1553 (Scrollbar_t392 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_OnPointerUp_m1554 (Scrollbar_t392 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnMove(UnityEngine.EventSystems.AxisEventData)
 void Scrollbar_OnMove_m1555 (Scrollbar_t392 * __this, AxisEventData_t247 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Scrollbar::FindSelectableOnLeft()
 Selectable_t324 * Scrollbar_FindSelectableOnLeft_m1556 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Scrollbar::FindSelectableOnRight()
 Selectable_t324 * Scrollbar_FindSelectableOnRight_m1557 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Scrollbar::FindSelectableOnUp()
 Selectable_t324 * Scrollbar_FindSelectableOnUp_m1558 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Scrollbar::FindSelectableOnDown()
 Selectable_t324 * Scrollbar_FindSelectableOnDown_m1559 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_OnInitializePotentialDrag_m1560 (Scrollbar_t392 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::SetDirection(UnityEngine.UI.Scrollbar/Direction,System.Boolean)
 void Scrollbar_SetDirection_m1561 (Scrollbar_t392 * __this, int32_t ___direction, bool ___includeRectLayouts, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Scrollbar::UnityEngine.UI.ICanvasElement.IsDestroyed()
 bool Scrollbar_UnityEngine_UI_ICanvasElement_IsDestroyed_m1562 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.UI.Scrollbar::UnityEngine.UI.ICanvasElement.get_transform()
 Transform_t74 * Scrollbar_UnityEngine_UI_ICanvasElement_get_transform_m1563 (Scrollbar_t392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
