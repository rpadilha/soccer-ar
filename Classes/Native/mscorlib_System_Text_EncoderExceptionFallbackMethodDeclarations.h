﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderExceptionFallback
struct EncoderExceptionFallback_t2202;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2194;
// System.Object
struct Object_t;

// System.Void System.Text.EncoderExceptionFallback::.ctor()
 void EncoderExceptionFallback__ctor_m12433 (EncoderExceptionFallback_t2202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.EncoderExceptionFallback::CreateFallbackBuffer()
 EncoderFallbackBuffer_t2194 * EncoderExceptionFallback_CreateFallbackBuffer_m12434 (EncoderExceptionFallback_t2202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.EncoderExceptionFallback::Equals(System.Object)
 bool EncoderExceptionFallback_Equals_m12435 (EncoderExceptionFallback_t2202 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncoderExceptionFallback::GetHashCode()
 int32_t EncoderExceptionFallback_GetHashCode_m12436 (EncoderExceptionFallback_t2202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
