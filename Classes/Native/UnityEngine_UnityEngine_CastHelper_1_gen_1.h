﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t17;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.TrackableBehaviour>
struct CastHelper_1_t2804 
{
	// T UnityEngine.CastHelper`1<Vuforia.TrackableBehaviour>::t
	TrackableBehaviour_t17 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.TrackableBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
