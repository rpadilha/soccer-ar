﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.OrientedBoundingBox
struct OrientedBoundingBox_t634;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void Vuforia.OrientedBoundingBox::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
 void OrientedBoundingBox__ctor_m2957 (OrientedBoundingBox_t634 * __this, Vector2_t99  ___center, Vector2_t99  ___halfExtents, float ___rotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::get_Center()
 Vector2_t99  OrientedBoundingBox_get_Center_m2958 (OrientedBoundingBox_t634 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox::set_Center(UnityEngine.Vector2)
 void OrientedBoundingBox_set_Center_m2959 (OrientedBoundingBox_t634 * __this, Vector2_t99  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::get_HalfExtents()
 Vector2_t99  OrientedBoundingBox_get_HalfExtents_m2960 (OrientedBoundingBox_t634 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox::set_HalfExtents(UnityEngine.Vector2)
 void OrientedBoundingBox_set_HalfExtents_m2961 (OrientedBoundingBox_t634 * __this, Vector2_t99  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.OrientedBoundingBox::get_Rotation()
 float OrientedBoundingBox_get_Rotation_m2962 (OrientedBoundingBox_t634 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox::set_Rotation(System.Single)
 void OrientedBoundingBox_set_Rotation_m2963 (OrientedBoundingBox_t634 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
