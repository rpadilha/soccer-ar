﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct Enumerator_t855;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t771;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
 void Enumerator__ctor_m23101 (Enumerator_t855 * __this, List_1_t771 * ___l, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23102 (Enumerator_t855 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
 void Enumerator_Dispose_m23103 (Enumerator_t855 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
 void Enumerator_VerifyState_m23104 (Enumerator_t855 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
 bool Enumerator_MoveNext_m5016 (Enumerator_t855 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
 int32_t Enumerator_get_Current_m5013 (Enumerator_t855 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
