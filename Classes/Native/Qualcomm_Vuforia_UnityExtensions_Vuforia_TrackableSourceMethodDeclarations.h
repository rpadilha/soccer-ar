﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackableSource
struct TrackableSource_t630;

// System.Void Vuforia.TrackableSource::.ctor()
 void TrackableSource__ctor_m4198 (TrackableSource_t630 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
