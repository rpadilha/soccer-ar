﻿#pragma once
#include <stdint.h>
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t1007;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.GUILayoutEntry>
struct Comparison_1_t4705  : public MulticastDelegate_t373
{
};
