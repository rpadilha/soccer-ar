﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
struct InternalEnumerator_1_t5112;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30934 (InternalEnumerator_1_t5112 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30935 (InternalEnumerator_1_t5112 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::Dispose()
 void InternalEnumerator_1_Dispose_m30936 (InternalEnumerator_1_t5112 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30937 (InternalEnumerator_1_t5112 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m30938 (InternalEnumerator_1_t5112 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
