﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Marker>
struct ShimEnumerator_t4059;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>
struct Dictionary_2_t669;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Marker>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m22873 (ShimEnumerator_t4059 * __this, Dictionary_2_t669 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Marker>::MoveNext()
 bool ShimEnumerator_MoveNext_m22874 (ShimEnumerator_t4059 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Marker>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m22875 (ShimEnumerator_t4059 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Marker>::get_Key()
 Object_t * ShimEnumerator_get_Key_m22876 (ShimEnumerator_t4059 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Marker>::get_Value()
 Object_t * ShimEnumerator_get_Value_m22877 (ShimEnumerator_t4059 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Marker>::get_Current()
 Object_t * ShimEnumerator_get_Current_m22878 (ShimEnumerator_t4059 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
