﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.EyewearComponentFactory/NullEyewearComponentFactory
struct NullEyewearComponentFactory_t584;
// System.Type
struct Type_t;

// System.Type Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::GetOVRInitControllerType()
 Type_t * NullEyewearComponentFactory_GetOVRInitControllerType_m2757 (NullEyewearComponentFactory_t584 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::.ctor()
 void NullEyewearComponentFactory__ctor_m2758 (NullEyewearComponentFactory_t584 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
