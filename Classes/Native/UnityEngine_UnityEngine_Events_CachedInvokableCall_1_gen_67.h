﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<SmoothFollow2D>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_63.h"
// UnityEngine.Events.CachedInvokableCall`1<SmoothFollow2D>
struct CachedInvokableCall_1_t3158  : public InvokableCall_1_t3159
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<SmoothFollow2D>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
