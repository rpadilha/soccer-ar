﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Joystick_Script>
struct UnityAction_1_t3068;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Joystick_Script>
struct InvokableCall_1_t3067  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Joystick_Script>::Delegate
	UnityAction_1_t3068 * ___Delegate_0;
};
