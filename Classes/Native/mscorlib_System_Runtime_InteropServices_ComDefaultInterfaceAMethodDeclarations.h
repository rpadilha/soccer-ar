﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
struct ComDefaultInterfaceAttribute_t2023;
// System.Type
struct Type_t;

// System.Void System.Runtime.InteropServices.ComDefaultInterfaceAttribute::.ctor(System.Type)
 void ComDefaultInterfaceAttribute__ctor_m11583 (ComDefaultInterfaceAttribute_t2023 * __this, Type_t * ___defaultInterface, MethodInfo* method) IL2CPP_METHOD_ATTR;
