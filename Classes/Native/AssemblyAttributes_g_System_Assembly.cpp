﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern TypeInfo NeutralResourcesLanguageAttribute_t1339_il2cpp_TypeInfo;
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
extern MethodInfo NeutralResourcesLanguageAttribute__ctor_m6894_MethodInfo;
extern TypeInfo CLSCompliantAttribute_t1338_il2cpp_TypeInfo;
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
extern MethodInfo CLSCompliantAttribute__ctor_m6893_MethodInfo;
extern TypeInfo AssemblyInformationalVersionAttribute_t1333_il2cpp_TypeInfo;
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
extern MethodInfo AssemblyInformationalVersionAttribute__ctor_m6887_MethodInfo;
extern TypeInfo SatelliteContractVersionAttribute_t1334_il2cpp_TypeInfo;
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
extern MethodInfo SatelliteContractVersionAttribute__ctor_m6888_MethodInfo;
extern TypeInfo AssemblyCopyrightAttribute_t578_il2cpp_TypeInfo;
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
extern MethodInfo AssemblyCopyrightAttribute__ctor_m2752_MethodInfo;
extern TypeInfo AssemblyProductAttribute_t577_il2cpp_TypeInfo;
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
extern MethodInfo AssemblyProductAttribute__ctor_m2751_MethodInfo;
extern TypeInfo AssemblyCompanyAttribute_t576_il2cpp_TypeInfo;
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
extern MethodInfo AssemblyCompanyAttribute__ctor_m2750_MethodInfo;
extern TypeInfo AssemblyDefaultAliasAttribute_t1335_il2cpp_TypeInfo;
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
extern MethodInfo AssemblyDefaultAliasAttribute__ctor_m6889_MethodInfo;
extern TypeInfo AssemblyDescriptionAttribute_t574_il2cpp_TypeInfo;
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
extern MethodInfo AssemblyDescriptionAttribute__ctor_m2748_MethodInfo;
extern TypeInfo ComVisibleAttribute_t581_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
extern MethodInfo ComVisibleAttribute__ctor_m2755_MethodInfo;
extern TypeInfo AssemblyTitleAttribute_t573_il2cpp_TypeInfo;
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
extern MethodInfo AssemblyTitleAttribute__ctor_m2747_MethodInfo;
extern TypeInfo RuntimeCompatibilityAttribute_t205_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
extern MethodInfo RuntimeCompatibilityAttribute__ctor_m729_MethodInfo;
extern MethodInfo RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m730_MethodInfo;
extern TypeInfo CompilationRelaxationsAttribute_t949_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
extern MethodInfo CompilationRelaxationsAttribute__ctor_m6890_MethodInfo;
extern TypeInfo DebuggableAttribute_t230_il2cpp_TypeInfo;
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
extern MethodInfo DebuggableAttribute__ctor_m861_MethodInfo;
extern TypeInfo AssemblyDelaySignAttribute_t1337_il2cpp_TypeInfo;
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
extern MethodInfo AssemblyDelaySignAttribute__ctor_m6892_MethodInfo;
extern TypeInfo AssemblyKeyFileAttribute_t1336_il2cpp_TypeInfo;
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
extern MethodInfo AssemblyKeyFileAttribute__ctor_m6891_MethodInfo;
extern TypeInfo InternalsVisibleToAttribute_t948_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
extern MethodInfo InternalsVisibleToAttribute__ctor_m5568_MethodInfo;
extern TypeInfo AssemblyFileVersionAttribute_t579_il2cpp_TypeInfo;
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
extern MethodInfo AssemblyFileVersionAttribute__ctor_m2753_MethodInfo;
void g_System_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		NeutralResourcesLanguageAttribute_t1339 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1339 *)il2cpp_codegen_object_new (&NeutralResourcesLanguageAttribute_t1339_il2cpp_TypeInfo);
		NeutralResourcesLanguageAttribute__ctor_m6894(tmp, il2cpp_codegen_string_new_wrapper("en-US"), &NeutralResourcesLanguageAttribute__ctor_m6894_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1338 * tmp;
		tmp = (CLSCompliantAttribute_t1338 *)il2cpp_codegen_object_new (&CLSCompliantAttribute_t1338_il2cpp_TypeInfo);
		CLSCompliantAttribute__ctor_m6893(tmp, true, &CLSCompliantAttribute__ctor_m6893_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyInformationalVersionAttribute_t1333 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t1333 *)il2cpp_codegen_object_new (&AssemblyInformationalVersionAttribute_t1333_il2cpp_TypeInfo);
		AssemblyInformationalVersionAttribute__ctor_m6887(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), &AssemblyInformationalVersionAttribute__ctor_m6887_MethodInfo);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t1334 * tmp;
		tmp = (SatelliteContractVersionAttribute_t1334 *)il2cpp_codegen_object_new (&SatelliteContractVersionAttribute_t1334_il2cpp_TypeInfo);
		SatelliteContractVersionAttribute__ctor_m6888(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), &SatelliteContractVersionAttribute__ctor_m6888_MethodInfo);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t578 * tmp;
		tmp = (AssemblyCopyrightAttribute_t578 *)il2cpp_codegen_object_new (&AssemblyCopyrightAttribute_t578_il2cpp_TypeInfo);
		AssemblyCopyrightAttribute__ctor_m2752(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), &AssemblyCopyrightAttribute__ctor_m2752_MethodInfo);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t577 * tmp;
		tmp = (AssemblyProductAttribute_t577 *)il2cpp_codegen_object_new (&AssemblyProductAttribute_t577_il2cpp_TypeInfo);
		AssemblyProductAttribute__ctor_m2751(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), &AssemblyProductAttribute__ctor_m2751_MethodInfo);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t576 * tmp;
		tmp = (AssemblyCompanyAttribute_t576 *)il2cpp_codegen_object_new (&AssemblyCompanyAttribute_t576_il2cpp_TypeInfo);
		AssemblyCompanyAttribute__ctor_m2750(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), &AssemblyCompanyAttribute__ctor_m2750_MethodInfo);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t1335 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t1335 *)il2cpp_codegen_object_new (&AssemblyDefaultAliasAttribute_t1335_il2cpp_TypeInfo);
		AssemblyDefaultAliasAttribute__ctor_m6889(tmp, il2cpp_codegen_string_new_wrapper("System.dll"), &AssemblyDefaultAliasAttribute__ctor_m6889_MethodInfo);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t574 * tmp;
		tmp = (AssemblyDescriptionAttribute_t574 *)il2cpp_codegen_object_new (&AssemblyDescriptionAttribute_t574_il2cpp_TypeInfo);
		AssemblyDescriptionAttribute__ctor_m2748(tmp, il2cpp_codegen_string_new_wrapper("System.dll"), &AssemblyDescriptionAttribute__ctor_m2748_MethodInfo);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t581 * tmp;
		tmp = (ComVisibleAttribute_t581 *)il2cpp_codegen_object_new (&ComVisibleAttribute_t581_il2cpp_TypeInfo);
		ComVisibleAttribute__ctor_m2755(tmp, false, &ComVisibleAttribute__ctor_m2755_MethodInfo);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t573 * tmp;
		tmp = (AssemblyTitleAttribute_t573 *)il2cpp_codegen_object_new (&AssemblyTitleAttribute_t573_il2cpp_TypeInfo);
		AssemblyTitleAttribute__ctor_m2747(tmp, il2cpp_codegen_string_new_wrapper("System.dll"), &AssemblyTitleAttribute__ctor_m2747_MethodInfo);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t205 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t205 *)il2cpp_codegen_object_new (&RuntimeCompatibilityAttribute_t205_il2cpp_TypeInfo);
		RuntimeCompatibilityAttribute__ctor_m729(tmp, &RuntimeCompatibilityAttribute__ctor_m729_MethodInfo);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m730(tmp, true, &RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m730_MethodInfo);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t949 * tmp;
		tmp = (CompilationRelaxationsAttribute_t949 *)il2cpp_codegen_object_new (&CompilationRelaxationsAttribute_t949_il2cpp_TypeInfo);
		CompilationRelaxationsAttribute__ctor_m6890(tmp, 8, &CompilationRelaxationsAttribute__ctor_m6890_MethodInfo);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t230 * tmp;
		tmp = (DebuggableAttribute_t230 *)il2cpp_codegen_object_new (&DebuggableAttribute_t230_il2cpp_TypeInfo);
		DebuggableAttribute__ctor_m861(tmp, 2, &DebuggableAttribute__ctor_m861_MethodInfo);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1337 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1337 *)il2cpp_codegen_object_new (&AssemblyDelaySignAttribute_t1337_il2cpp_TypeInfo);
		AssemblyDelaySignAttribute__ctor_m6892(tmp, true, &AssemblyDelaySignAttribute__ctor_m6892_MethodInfo);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1336 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1336 *)il2cpp_codegen_object_new (&AssemblyKeyFileAttribute_t1336_il2cpp_TypeInfo);
		AssemblyKeyFileAttribute__ctor_m6891(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), &AssemblyKeyFileAttribute__ctor_m6891_MethodInfo);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t948 * tmp;
		tmp = (InternalsVisibleToAttribute_t948 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t948_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5568(tmp, il2cpp_codegen_string_new_wrapper("System.Net, PublicKey=00240000048000009400000006020000002400005253413100040000010001008D56C76F9E8649383049F383C44BE0EC204181822A6C31CF5EB7EF486944D032188EA1D3920763712CCB12D75FB77E9811149E6148E5D32FBAAB37611C1878DDC19E20EF135D0CB2CFF2BFEC3D115810C3D9069638FE4BE215DBF795861920E5AB6F7DB2E2CEEF136AC23D5DD2BF031700AEC232F6C6B1C785B4305C123B37AB"), &InternalsVisibleToAttribute__ctor_m5568_MethodInfo);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t579 * tmp;
		tmp = (AssemblyFileVersionAttribute_t579 *)il2cpp_codegen_object_new (&AssemblyFileVersionAttribute_t579_il2cpp_TypeInfo);
		AssemblyFileVersionAttribute__ctor_m2753(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), &AssemblyFileVersionAttribute__ctor_m2753_MethodInfo);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache g_System_Assembly__CustomAttributeCache = {
18,
NULL,
&g_System_Assembly_CustomAttributesCacheGenerator
};
