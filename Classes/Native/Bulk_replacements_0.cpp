﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "replacements_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo U3CModuleU3E_t1582_il2cpp_TypeInfo;
// <Module>
#include "replacements_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Metadata Definition <Module>
static MethodInfo* U3CModuleU3E_t1582_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType U3CModuleU3E_t1582_0_0_0;
extern Il2CppType U3CModuleU3E_t1582_1_0_0;
struct U3CModuleU3E_t1582;
TypeInfo U3CModuleU3E_t1582_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t1582_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &U3CModuleU3E_t1582_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &U3CModuleU3E_t1582_il2cpp_TypeInfo/* cast_class */
	, &U3CModuleU3E_t1582_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1582_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1582)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Replacements.MSCompatUnicodeTable
#include "replacements_Replacements_MSCompatUnicodeTable.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo MSCompatUnicodeTable_t1583_il2cpp_TypeInfo;
// Replacements.MSCompatUnicodeTable
#include "replacements_Replacements_MSCompatUnicodeTableMethodDeclarations.h"

// System.Boolean
#include "mscorlib_System_Boolean.h"


// System.Boolean Replacements.MSCompatUnicodeTable::get_IsReady()
extern MethodInfo MSCompatUnicodeTable_get_IsReady_m8144_MethodInfo;
 bool MSCompatUnicodeTable_get_IsReady_m8144 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		return 0;
	}
}
// Metadata Definition Replacements.MSCompatUnicodeTable
static PropertyInfo MSCompatUnicodeTable_t1583____IsReady_PropertyInfo = 
{
	&MSCompatUnicodeTable_t1583_il2cpp_TypeInfo/* parent */
	, "IsReady"/* name */
	, &MSCompatUnicodeTable_get_IsReady_m8144_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* MSCompatUnicodeTable_t1583_PropertyInfos[] =
{
	&MSCompatUnicodeTable_t1583____IsReady_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Replacements.MSCompatUnicodeTable::get_IsReady()
MethodInfo MSCompatUnicodeTable_get_IsReady_m8144_MethodInfo = 
{
	"get_IsReady"/* name */
	, (methodPointerType)&MSCompatUnicodeTable_get_IsReady_m8144/* method */
	, &MSCompatUnicodeTable_t1583_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MSCompatUnicodeTable_t1583_MethodInfos[] =
{
	&MSCompatUnicodeTable_get_IsReady_m8144_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
static MethodInfo* MSCompatUnicodeTable_t1583_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType MSCompatUnicodeTable_t1583_0_0_0;
extern Il2CppType MSCompatUnicodeTable_t1583_1_0_0;
extern TypeInfo Object_t_il2cpp_TypeInfo;
struct MSCompatUnicodeTable_t1583;
TypeInfo MSCompatUnicodeTable_t1583_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "MSCompatUnicodeTable"/* name */
	, "Replacements"/* namespaze */
	, MSCompatUnicodeTable_t1583_MethodInfos/* methods */
	, MSCompatUnicodeTable_t1583_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &MSCompatUnicodeTable_t1583_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, MSCompatUnicodeTable_t1583_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &MSCompatUnicodeTable_t1583_il2cpp_TypeInfo/* cast_class */
	, &MSCompatUnicodeTable_t1583_0_0_0/* byval_arg */
	, &MSCompatUnicodeTable_t1583_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MSCompatUnicodeTable_t1583)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Replacements.SecurityElement
#include "replacements_Replacements_SecurityElement.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SecurityElement_t1584_il2cpp_TypeInfo;
// Replacements.SecurityElement
#include "replacements_Replacements_SecurityElementMethodDeclarations.h"

// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.Void
#include "mscorlib_System_Void.h"
extern TypeInfo NotSupportedException_t498_il2cpp_TypeInfo;
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern MethodInfo NotSupportedException__ctor_m7849_MethodInfo;


// System.String Replacements.SecurityElement::ToString(System.Object)
extern MethodInfo SecurityElement_ToString_m8145_MethodInfo;
 String_t* SecurityElement_ToString_m8145 (Object_t * __this/* static, unused */, Object_t * _____this, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral841, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition Replacements.SecurityElement
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo SecurityElement_t1584_SecurityElement_ToString_m8145_ParameterInfos[] = 
{
	{"__this", 0, 134217729, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Replacements.SecurityElement::ToString(System.Object)
MethodInfo SecurityElement_ToString_m8145_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&SecurityElement_ToString_m8145/* method */
	, &SecurityElement_t1584_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, SecurityElement_t1584_SecurityElement_ToString_m8145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SecurityElement_t1584_MethodInfos[] =
{
	&SecurityElement_ToString_m8145_MethodInfo,
	NULL
};
static MethodInfo* SecurityElement_t1584_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType SecurityElement_t1584_0_0_0;
extern Il2CppType SecurityElement_t1584_1_0_0;
struct SecurityElement_t1584;
TypeInfo SecurityElement_t1584_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityElement"/* name */
	, "Replacements"/* namespaze */
	, SecurityElement_t1584_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SecurityElement_t1584_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SecurityElement_t1584_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SecurityElement_t1584_il2cpp_TypeInfo/* cast_class */
	, &SecurityElement_t1584_0_0_0/* byval_arg */
	, &SecurityElement_t1584_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityElement_t1584)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Replacements.RemotingServices
#include "replacements_Replacements_RemotingServices.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RemotingServices_t1585_il2cpp_TypeInfo;
// Replacements.RemotingServices
#include "replacements_Replacements_RemotingServicesMethodDeclarations.h"

// System.Type
#include "mscorlib_System_Type.h"
#include "mscorlib_ArrayTypes.h"


// System.Runtime.Remoting.Messaging.IMessageSink Replacements.RemotingServices::GetClientChannelSinkChain(System.String,System.Object,System.String&)
extern MethodInfo RemotingServices_GetClientChannelSinkChain_m8146_MethodInfo;
 Object_t * RemotingServices_GetClientChannelSinkChain_m8146 (Object_t * __this/* static, unused */, String_t* ___url, Object_t * ___channelData, String_t** ___objectUri, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral842, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Object Replacements.RemotingServices::CreateClientProxy(System.Type,System.String,System.Object[])
extern MethodInfo RemotingServices_CreateClientProxy_m8147_MethodInfo;
 Object_t * RemotingServices_CreateClientProxy_m8147 (Object_t * __this/* static, unused */, Type_t * ___objectType, String_t* ___url, ObjectU5BU5D_t130* ___activationAttributes, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral843, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition Replacements.RemotingServices
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType String_t_1_0_2;
extern Il2CppType String_t_1_0_0;
static ParameterInfo RemotingServices_t1585_RemotingServices_GetClientChannelSinkChain_m8146_ParameterInfos[] = 
{
	{"url", 0, 134217730, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"channelData", 1, 134217731, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"objectUri", 2, 134217732, &EmptyCustomAttributesCache, &String_t_1_0_2},
};
extern Il2CppType IMessageSink_t1586_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t1223 (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink Replacements.RemotingServices::GetClientChannelSinkChain(System.String,System.Object,System.String&)
MethodInfo RemotingServices_GetClientChannelSinkChain_m8146_MethodInfo = 
{
	"GetClientChannelSinkChain"/* name */
	, (methodPointerType)&RemotingServices_GetClientChannelSinkChain_m8146/* method */
	, &RemotingServices_t1585_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1586_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t1223/* invoker_method */
	, RemotingServices_t1585_RemotingServices_GetClientChannelSinkChain_m8146_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo RemotingServices_t1585_RemotingServices_CreateClientProxy_m8147_ParameterInfos[] = 
{
	{"objectType", 0, 134217733, &EmptyCustomAttributesCache, &Type_t_0_0_0},
	{"url", 1, 134217734, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"activationAttributes", 2, 134217735, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object Replacements.RemotingServices::CreateClientProxy(System.Type,System.String,System.Object[])
MethodInfo RemotingServices_CreateClientProxy_m8147_MethodInfo = 
{
	"CreateClientProxy"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxy_m8147/* method */
	, &RemotingServices_t1585_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1585_RemotingServices_CreateClientProxy_m8147_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RemotingServices_t1585_MethodInfos[] =
{
	&RemotingServices_GetClientChannelSinkChain_m8146_MethodInfo,
	&RemotingServices_CreateClientProxy_m8147_MethodInfo,
	NULL
};
static MethodInfo* RemotingServices_t1585_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType RemotingServices_t1585_0_0_0;
extern Il2CppType RemotingServices_t1585_1_0_0;
struct RemotingServices_t1585;
TypeInfo RemotingServices_t1585_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingServices"/* name */
	, "Replacements"/* namespaze */
	, RemotingServices_t1585_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &RemotingServices_t1585_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, RemotingServices_t1585_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &RemotingServices_t1585_il2cpp_TypeInfo/* cast_class */
	, &RemotingServices_t1585_0_0_0/* byval_arg */
	, &RemotingServices_t1585_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingServices_t1585)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
