﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetAbstractBehaviour>
struct UnityAction_1_t3907;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetAbstractBehaviour>
struct InvokableCall_1_t3906  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetAbstractBehaviour>::Delegate
	UnityAction_1_t3907 * ___Delegate_0;
};
