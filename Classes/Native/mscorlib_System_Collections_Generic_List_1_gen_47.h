﻿#pragma once
#include <stdint.h>
// Vuforia.IVirtualButtonEventHandler[]
struct IVirtualButtonEventHandlerU5BU5D_t4581;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_t814  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::_items
	IVirtualButtonEventHandlerU5BU5D_t4581* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t814_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::EmptyArray
	IVirtualButtonEventHandlerU5BU5D_t4581* ___EmptyArray_4;
};
