﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t51;
// Vuforia.Surface
struct Surface_t16;
// UnityEngine.MeshFilter
struct MeshFilter_t84;
// UnityEngine.MeshCollider
struct MeshCollider_t626;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.GameObject
struct GameObject_t29;

// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::get_Surface()
 Object_t * SurfaceAbstractBehaviour_get_Surface_m2938 (SurfaceAbstractBehaviour_t51 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::InternalUnregisterTrackable()
 void SurfaceAbstractBehaviour_InternalUnregisterTrackable_m458 (SurfaceAbstractBehaviour_t51 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.InitializeSurface(Vuforia.Surface)
 void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m461 (SurfaceAbstractBehaviour_t51 * __this, Object_t * ___surface, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshFilterToUpdate(UnityEngine.MeshFilter)
 void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m462 (SurfaceAbstractBehaviour_t51 * __this, MeshFilter_t84 * ___meshFilterToUpdate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshFilter Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshFilterToUpdate()
 MeshFilter_t84 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m463 (SurfaceAbstractBehaviour_t51 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshColliderToUpdate(UnityEngine.MeshCollider)
 void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m464 (SurfaceAbstractBehaviour_t51 * __this, MeshCollider_t626 * ___meshColliderToUpdate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshCollider Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshColliderToUpdate()
 MeshCollider_t626 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m465 (SurfaceAbstractBehaviour_t51 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::.ctor()
 void SurfaceAbstractBehaviour__ctor_m453 (SurfaceAbstractBehaviour_t51 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m454 (SurfaceAbstractBehaviour_t51 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m455 (SurfaceAbstractBehaviour_t51 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t74 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m456 (SurfaceAbstractBehaviour_t51 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t29 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m457 (SurfaceAbstractBehaviour_t51 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
