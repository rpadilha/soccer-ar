﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$16
struct $ArrayType$16_t2317;
struct $ArrayType$16_t2317_marshaled;

void $ArrayType$16_t2317_marshal(const $ArrayType$16_t2317& unmarshaled, $ArrayType$16_t2317_marshaled& marshaled);
void $ArrayType$16_t2317_marshal_back(const $ArrayType$16_t2317_marshaled& marshaled, $ArrayType$16_t2317& unmarshaled);
void $ArrayType$16_t2317_marshal_cleanup($ArrayType$16_t2317_marshaled& marshaled);
