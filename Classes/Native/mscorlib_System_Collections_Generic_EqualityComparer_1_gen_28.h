﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Type>
struct EqualityComparer_1_t3898;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Type>
struct EqualityComparer_1_t3898  : public Object_t
{
};
struct EqualityComparer_1_t3898_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Type>::_default
	EqualityComparer_1_t3898 * ____default_0;
};
