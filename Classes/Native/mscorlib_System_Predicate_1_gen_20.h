﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t410;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct Predicate_1_t3686  : public MulticastDelegate_t373
{
};
