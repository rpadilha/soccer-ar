﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.PropAbstractBehaviour,Vuforia.PropAbstractBehaviour>
struct Transform_1_t4307;
// System.Object
struct Object_t;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t43;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.PropAbstractBehaviour,Vuforia.PropAbstractBehaviour>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m25357 (Transform_1_t4307 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.PropAbstractBehaviour,Vuforia.PropAbstractBehaviour>::Invoke(TKey,TValue)
 PropAbstractBehaviour_t43 * Transform_1_Invoke_m25358 (Transform_1_t4307 * __this, int32_t ___key, PropAbstractBehaviour_t43 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.PropAbstractBehaviour,Vuforia.PropAbstractBehaviour>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m25359 (Transform_1_t4307 * __this, int32_t ___key, PropAbstractBehaviour_t43 * ___value, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.PropAbstractBehaviour,Vuforia.PropAbstractBehaviour>::EndInvoke(System.IAsyncResult)
 PropAbstractBehaviour_t43 * Transform_1_EndInvoke_m25360 (Transform_1_t4307 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
