﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>
struct InternalEnumerator_1_t5251;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.CompilerServices.LoadHint
#include "mscorlib_System_Runtime_CompilerServices_LoadHint.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31627 (InternalEnumerator_1_t5251 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31628 (InternalEnumerator_1_t5251 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::Dispose()
 void InternalEnumerator_1_Dispose_m31629 (InternalEnumerator_1_t5251 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31630 (InternalEnumerator_1_t5251 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31631 (InternalEnumerator_1_t5251 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
