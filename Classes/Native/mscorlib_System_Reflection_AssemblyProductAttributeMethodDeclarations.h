﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t577;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
 void AssemblyProductAttribute__ctor_m2751 (AssemblyProductAttribute_t577 * __this, String_t* ___product, MethodInfo* method) IL2CPP_METHOD_ATTR;
