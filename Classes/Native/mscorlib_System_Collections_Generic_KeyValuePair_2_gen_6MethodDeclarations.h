﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct KeyValuePair_2_t3458;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m18343_gshared (KeyValuePair_2_t3458 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2__ctor_m18343(__this, ___key, ___value, method) (void)KeyValuePair_2__ctor_m18343_gshared((KeyValuePair_2_t3458 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
 Object_t * KeyValuePair_2_get_Key_m18344_gshared (KeyValuePair_2_t3458 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Key_m18344(__this, method) (Object_t *)KeyValuePair_2_get_Key_m18344_gshared((KeyValuePair_2_t3458 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m18345_gshared (KeyValuePair_2_t3458 * __this, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2_set_Key_m18345(__this, ___value, method) (void)KeyValuePair_2_set_Key_m18345_gshared((KeyValuePair_2_t3458 *)__this, (Object_t *)___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
 Object_t * KeyValuePair_2_get_Value_m18346_gshared (KeyValuePair_2_t3458 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Value_m18346(__this, method) (Object_t *)KeyValuePair_2_get_Value_m18346_gshared((KeyValuePair_2_t3458 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m18347_gshared (KeyValuePair_2_t3458 * __this, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2_set_Value_m18347(__this, ___value, method) (void)KeyValuePair_2_set_Value_m18347_gshared((KeyValuePair_2_t3458 *)__this, (Object_t *)___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
 String_t* KeyValuePair_2_ToString_m18348_gshared (KeyValuePair_2_t3458 * __this, MethodInfo* method);
#define KeyValuePair_2_ToString_m18348(__this, method) (String_t*)KeyValuePair_2_ToString_m18348_gshared((KeyValuePair_2_t3458 *)__this, method)
