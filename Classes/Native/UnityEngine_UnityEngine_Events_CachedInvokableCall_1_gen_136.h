﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.AssetBundle>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_138.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AssetBundle>
struct CachedInvokableCall_1_t4604  : public InvokableCall_1_t4605
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AssetBundle>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
