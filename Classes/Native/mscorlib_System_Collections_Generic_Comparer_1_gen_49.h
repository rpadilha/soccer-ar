﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct Comparer_1_t4634;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct Comparer_1_t4634  : public Object_t
{
};
struct Comparer_1_t4634_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::_default
	Comparer_1_t4634 * ____default_0;
};
