﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>
struct ShimEnumerator_t5068;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1397;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m30717 (ShimEnumerator_t5068 * __this, Dictionary_2_t1397 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::MoveNext()
 bool ShimEnumerator_MoveNext_m30718 (ShimEnumerator_t5068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m30719 (ShimEnumerator_t5068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::get_Key()
 Object_t * ShimEnumerator_get_Key_m30720 (ShimEnumerator_t5068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::get_Value()
 Object_t * ShimEnumerator_get_Value_m30721 (ShimEnumerator_t5068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::get_Current()
 Object_t * ShimEnumerator_get_Current_m30722 (ShimEnumerator_t5068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
