﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.KeepAliveBehaviour
struct KeepAliveBehaviour_t37;

// System.Void Vuforia.KeepAliveBehaviour::.ctor()
 void KeepAliveBehaviour__ctor_m73 (KeepAliveBehaviour_t37 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
