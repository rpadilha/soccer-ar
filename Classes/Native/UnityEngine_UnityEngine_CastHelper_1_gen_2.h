﻿#pragma once
#include <stdint.h>
// UnityEngine.Renderer
struct Renderer_t129;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.Renderer>
struct CastHelper_1_t2890 
{
	// T UnityEngine.CastHelper`1<UnityEngine.Renderer>::t
	Renderer_t129 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.Renderer>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
