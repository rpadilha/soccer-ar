﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_16.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>
struct CachedInvokableCall_1_t2907  : public InvokableCall_1_t2908
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
