﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Toggle
struct Toggle_t418;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t417;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t239;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t235;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.UI.CanvasUpdate
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate.h"

// System.Void UnityEngine.UI.Toggle::.ctor()
 void Toggle__ctor_m1784 (Toggle_t418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::get_group()
 ToggleGroup_t417 * Toggle_get_group_m1785 (Toggle_t418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::set_group(UnityEngine.UI.ToggleGroup)
 void Toggle_set_group_m1786 (Toggle_t418 * __this, ToggleGroup_t417 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::Rebuild(UnityEngine.UI.CanvasUpdate)
 void Toggle_Rebuild_m1787 (Toggle_t418 * __this, int32_t ___executing, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::OnEnable()
 void Toggle_OnEnable_m1788 (Toggle_t418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::OnDisable()
 void Toggle_OnDisable_m1789 (Toggle_t418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::SetToggleGroup(UnityEngine.UI.ToggleGroup,System.Boolean)
 void Toggle_SetToggleGroup_m1790 (Toggle_t418 * __this, ToggleGroup_t417 * ___newGroup, bool ___setMemberValue, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Toggle::get_isOn()
 bool Toggle_get_isOn_m1791 (Toggle_t418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
 void Toggle_set_isOn_m1792 (Toggle_t418 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean)
 void Toggle_Set_m1793 (Toggle_t418 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean,System.Boolean)
 void Toggle_Set_m1794 (Toggle_t418 * __this, bool ___value, bool ___sendCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::PlayEffect(System.Boolean)
 void Toggle_PlayEffect_m1795 (Toggle_t418 * __this, bool ___instant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::Start()
 void Toggle_Start_m1796 (Toggle_t418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::InternalToggle()
 void Toggle_InternalToggle_m1797 (Toggle_t418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
 void Toggle_OnPointerClick_m1798 (Toggle_t418 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::OnSubmit(UnityEngine.EventSystems.BaseEventData)
 void Toggle_OnSubmit_m1799 (Toggle_t418 * __this, BaseEventData_t235 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.IsDestroyed()
 bool Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1800 (Toggle_t418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.get_transform()
 Transform_t74 * Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1801 (Toggle_t418 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
