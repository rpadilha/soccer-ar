﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.Rijndael
struct Rijndael_t1732;
// System.String
struct String_t;

// System.Void System.Security.Cryptography.Rijndael::.ctor()
 void Rijndael__ctor_m12206 (Rijndael_t1732 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Rijndael System.Security.Cryptography.Rijndael::Create()
 Rijndael_t1732 * Rijndael_Create_m9057 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Rijndael System.Security.Cryptography.Rijndael::Create(System.String)
 Rijndael_t1732 * Rijndael_Create_m12207 (Object_t * __this/* static, unused */, String_t* ___algName, MethodInfo* method) IL2CPP_METHOD_ATTR;
