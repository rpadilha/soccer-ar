﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
struct DefaultComparer_t2847;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>::.ctor()
 void DefaultComparer__ctor_m14665_gshared (DefaultComparer_t2847 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m14665(__this, method) (void)DefaultComparer__ctor_m14665_gshared((DefaultComparer_t2847 *)__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m14666_gshared (DefaultComparer_t2847 * __this, Object_t * ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m14666(__this, ___obj, method) (int32_t)DefaultComparer_GetHashCode_m14666_gshared((DefaultComparer_t2847 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>::Equals(T,T)
 bool DefaultComparer_Equals_m14667_gshared (DefaultComparer_t2847 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define DefaultComparer_Equals_m14667(__this, ___x, ___y, method) (bool)DefaultComparer_Equals_m14667_gshared((DefaultComparer_t2847 *)__this, (Object_t *)___x, (Object_t *)___y, method)
