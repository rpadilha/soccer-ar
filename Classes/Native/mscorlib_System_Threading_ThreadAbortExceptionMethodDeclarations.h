﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Threading.ThreadAbortException
struct ThreadAbortException_t2226;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Threading.ThreadAbortException::.ctor()
 void ThreadAbortException__ctor_m12661 (ThreadAbortException_t2226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadAbortException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void ThreadAbortException__ctor_m12662 (ThreadAbortException_t2226 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___sc, MethodInfo* method) IL2CPP_METHOD_ATTR;
