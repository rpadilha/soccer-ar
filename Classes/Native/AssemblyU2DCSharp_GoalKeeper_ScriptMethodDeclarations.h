﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GoalKeeper_Script
struct GoalKeeper_Script_t77;
// UnityEngine.Collision
struct Collision_t82;

// System.Void GoalKeeper_Script::.ctor()
 void GoalKeeper_Script__ctor_m129 (GoalKeeper_Script_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoalKeeper_Script::Start()
 void GoalKeeper_Script_Start_m130 (GoalKeeper_Script_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoalKeeper_Script::Update()
 void GoalKeeper_Script_Update_m131 (GoalKeeper_Script_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoalKeeper_Script::OnCollisionStay(UnityEngine.Collision)
 void GoalKeeper_Script_OnCollisionStay_m132 (GoalKeeper_Script_t77 * __this, Collision_t82 * ___coll, MethodInfo* method) IL2CPP_METHOD_ATTR;
