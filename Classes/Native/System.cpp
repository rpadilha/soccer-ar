﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo U3CModuleU3E_t1340_il2cpp_TypeInfo;
extern TypeInfo Locale_t1341_il2cpp_TypeInfo;
extern TypeInfo MonoTODOAttribute_t1342_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t1343_il2cpp_TypeInfo;
extern TypeInfo LinkedList_1_t1344_il2cpp_TypeInfo;
extern TypeInfo LinkedListNode_1_t1345_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t1346_il2cpp_TypeInfo;
extern TypeInfo Stack_1_t1347_il2cpp_TypeInfo;
extern TypeInfo HybridDictionary_t1350_il2cpp_TypeInfo;
extern TypeInfo DictionaryNode_t1353_il2cpp_TypeInfo;
extern TypeInfo DictionaryNodeEnumerator_t1354_il2cpp_TypeInfo;
extern TypeInfo ListDictionary_t1349_il2cpp_TypeInfo;
extern TypeInfo _Item_t1357_il2cpp_TypeInfo;
extern TypeInfo _KeysEnumerator_t1359_il2cpp_TypeInfo;
extern TypeInfo KeysCollection_t1360_il2cpp_TypeInfo;
extern TypeInfo NameObjectCollectionBase_t1358_il2cpp_TypeInfo;
extern TypeInfo NameValueCollection_t1364_il2cpp_TypeInfo;
extern TypeInfo EditorBrowsableAttribute_t1182_il2cpp_TypeInfo;
extern TypeInfo EditorBrowsableState_t1365_il2cpp_TypeInfo;
extern TypeInfo TypeConverter_t1366_il2cpp_TypeInfo;
extern TypeInfo TypeConverterAttribute_t1367_il2cpp_TypeInfo;
extern TypeInfo AuthenticationLevel_t1368_il2cpp_TypeInfo;
extern TypeInfo SslPolicyErrors_t1369_il2cpp_TypeInfo;
extern TypeInfo AddressFamily_t1370_il2cpp_TypeInfo;
extern TypeInfo DefaultCertificatePolicy_t1371_il2cpp_TypeInfo;
extern TypeInfo FileWebRequest_t1378_il2cpp_TypeInfo;
extern TypeInfo FileWebRequestCreator_t1379_il2cpp_TypeInfo;
extern TypeInfo FtpRequestCreator_t1380_il2cpp_TypeInfo;
extern TypeInfo FtpWebRequest_t1382_il2cpp_TypeInfo;
extern TypeInfo GlobalProxySelection_t1384_il2cpp_TypeInfo;
extern TypeInfo HttpRequestCreator_t1385_il2cpp_TypeInfo;
extern TypeInfo HttpVersion_t1387_il2cpp_TypeInfo;
extern TypeInfo HttpWebRequest_t1389_il2cpp_TypeInfo;
extern TypeInfo ICertificatePolicy_t1395_il2cpp_TypeInfo;
extern TypeInfo ICredentials_t1399_il2cpp_TypeInfo;
extern TypeInfo IPAddress_t1391_il2cpp_TypeInfo;
extern TypeInfo IPv6Address_t1392_il2cpp_TypeInfo;
extern TypeInfo IWebProxy_t1377_il2cpp_TypeInfo;
extern TypeInfo IWebRequestCreate_t1552_il2cpp_TypeInfo;
extern TypeInfo SecurityProtocolType_t1393_il2cpp_TypeInfo;
extern TypeInfo ServicePoint_t1372_il2cpp_TypeInfo;
extern TypeInfo SPKey_t1394_il2cpp_TypeInfo;
extern TypeInfo ServicePointManager_t1396_il2cpp_TypeInfo;
extern TypeInfo WebHeaderCollection_t1376_il2cpp_TypeInfo;
extern TypeInfo WebProxy_t1400_il2cpp_TypeInfo;
extern TypeInfo WebRequest_t1374_il2cpp_TypeInfo;
extern TypeInfo OpenFlags_t1402_il2cpp_TypeInfo;
extern TypeInfo PublicKey_t1406_il2cpp_TypeInfo;
extern TypeInfo StoreLocation_t1410_il2cpp_TypeInfo;
extern TypeInfo StoreName_t1411_il2cpp_TypeInfo;
extern TypeInfo X500DistinguishedName_t1412_il2cpp_TypeInfo;
extern TypeInfo X500DistinguishedNameFlags_t1413_il2cpp_TypeInfo;
extern TypeInfo X509BasicConstraintsExtension_t1414_il2cpp_TypeInfo;
extern TypeInfo X509Certificate2_t1417_il2cpp_TypeInfo;
extern TypeInfo X509Certificate2Collection_t1419_il2cpp_TypeInfo;
extern TypeInfo X509Certificate2Enumerator_t1420_il2cpp_TypeInfo;
extern TypeInfo X509CertificateEnumerator_t1421_il2cpp_TypeInfo;
extern TypeInfo X509CertificateCollection_t1388_il2cpp_TypeInfo;
extern TypeInfo X509Chain_t1383_il2cpp_TypeInfo;
extern TypeInfo X509ChainElement_t1428_il2cpp_TypeInfo;
extern TypeInfo X509ChainElementCollection_t1424_il2cpp_TypeInfo;
extern TypeInfo X509ChainElementEnumerator_t1433_il2cpp_TypeInfo;
extern TypeInfo X509ChainPolicy_t1425_il2cpp_TypeInfo;
extern TypeInfo X509ChainStatus_t1427_il2cpp_TypeInfo;
extern TypeInfo X509ChainStatusFlags_t1435_il2cpp_TypeInfo;
extern TypeInfo X509EnhancedKeyUsageExtension_t1436_il2cpp_TypeInfo;
extern TypeInfo X509Extension_t1415_il2cpp_TypeInfo;
extern TypeInfo X509ExtensionCollection_t1416_il2cpp_TypeInfo;
extern TypeInfo X509ExtensionEnumerator_t1437_il2cpp_TypeInfo;
extern TypeInfo X509FindType_t1438_il2cpp_TypeInfo;
extern TypeInfo X509KeyUsageExtension_t1439_il2cpp_TypeInfo;
extern TypeInfo X509KeyUsageFlags_t1440_il2cpp_TypeInfo;
extern TypeInfo X509NameType_t1441_il2cpp_TypeInfo;
extern TypeInfo X509RevocationFlag_t1442_il2cpp_TypeInfo;
extern TypeInfo X509RevocationMode_t1443_il2cpp_TypeInfo;
extern TypeInfo X509Store_t1429_il2cpp_TypeInfo;
extern TypeInfo X509SubjectKeyIdentifierExtension_t1446_il2cpp_TypeInfo;
extern TypeInfo X509SubjectKeyIdentifierHashAlgorithm_t1447_il2cpp_TypeInfo;
extern TypeInfo X509VerificationFlags_t1448_il2cpp_TypeInfo;
extern TypeInfo AsnDecodeStatus_t1449_il2cpp_TypeInfo;
extern TypeInfo AsnEncodedData_t1404_il2cpp_TypeInfo;
extern TypeInfo Oid_t1405_il2cpp_TypeInfo;
extern TypeInfo OidCollection_t1434_il2cpp_TypeInfo;
extern TypeInfo OidEnumerator_t1450_il2cpp_TypeInfo;
extern TypeInfo BaseMachine_t1451_il2cpp_TypeInfo;
extern TypeInfo Capture_t1453_il2cpp_TypeInfo;
extern TypeInfo CaptureCollection_t1455_il2cpp_TypeInfo;
extern TypeInfo Group_t1456_il2cpp_TypeInfo;
extern TypeInfo GroupCollection_t1458_il2cpp_TypeInfo;
extern TypeInfo Match_t1452_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t1461_il2cpp_TypeInfo;
extern TypeInfo MatchCollection_t1460_il2cpp_TypeInfo;
extern TypeInfo Regex_t861_il2cpp_TypeInfo;
extern TypeInfo RegexOptions_t1464_il2cpp_TypeInfo;
extern TypeInfo OpCode_t1465_il2cpp_TypeInfo;
extern TypeInfo OpFlags_t1466_il2cpp_TypeInfo;
extern TypeInfo Position_t1467_il2cpp_TypeInfo;
extern TypeInfo IMachine_t1459_il2cpp_TypeInfo;
extern TypeInfo IMachineFactory_t1463_il2cpp_TypeInfo;
extern TypeInfo Key_t1468_il2cpp_TypeInfo;
extern TypeInfo FactoryCache_t1462_il2cpp_TypeInfo;
extern TypeInfo Node_t1470_il2cpp_TypeInfo;
extern TypeInfo MRUList_t1469_il2cpp_TypeInfo;
extern TypeInfo Category_t1471_il2cpp_TypeInfo;
extern TypeInfo CategoryUtils_t1472_il2cpp_TypeInfo;
extern TypeInfo LinkRef_t1473_il2cpp_TypeInfo;
extern TypeInfo ICompiler_t1499_il2cpp_TypeInfo;
extern TypeInfo InterpreterFactory_t1474_il2cpp_TypeInfo;
extern TypeInfo Link_t1475_il2cpp_TypeInfo;
extern TypeInfo PatternLinkStack_t1476_il2cpp_TypeInfo;
extern TypeInfo PatternCompiler_t1478_il2cpp_TypeInfo;
extern TypeInfo LinkStack_t1477_il2cpp_TypeInfo;
extern TypeInfo Mark_t1480_il2cpp_TypeInfo;
extern TypeInfo IntStack_t1481_il2cpp_TypeInfo;
extern TypeInfo RepeatContext_t1482_il2cpp_TypeInfo;
extern TypeInfo Mode_t1483_il2cpp_TypeInfo;
extern TypeInfo Interpreter_t1486_il2cpp_TypeInfo;
extern TypeInfo Interval_t1487_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t1489_il2cpp_TypeInfo;
extern TypeInfo CostDelegate_t1490_il2cpp_TypeInfo;
extern TypeInfo IntervalCollection_t1491_il2cpp_TypeInfo;
extern TypeInfo Parser_t1492_il2cpp_TypeInfo;
extern TypeInfo QuickSearch_t1484_il2cpp_TypeInfo;
extern TypeInfo ExpressionCollection_t1498_il2cpp_TypeInfo;
extern TypeInfo Expression_t1496_il2cpp_TypeInfo;
extern TypeInfo CompositeExpression_t1501_il2cpp_TypeInfo;
extern TypeInfo Group_t1494_il2cpp_TypeInfo;
extern TypeInfo RegularExpression_t1493_il2cpp_TypeInfo;
extern TypeInfo CapturingGroup_t1502_il2cpp_TypeInfo;
extern TypeInfo BalancingGroup_t1503_il2cpp_TypeInfo;
extern TypeInfo NonBacktrackingGroup_t1504_il2cpp_TypeInfo;
extern TypeInfo Repetition_t1505_il2cpp_TypeInfo;
extern TypeInfo Assertion_t1495_il2cpp_TypeInfo;
extern TypeInfo CaptureAssertion_t1507_il2cpp_TypeInfo;
extern TypeInfo ExpressionAssertion_t1497_il2cpp_TypeInfo;
extern TypeInfo Alternation_t1508_il2cpp_TypeInfo;
extern TypeInfo Literal_t1506_il2cpp_TypeInfo;
extern TypeInfo PositionAssertion_t1509_il2cpp_TypeInfo;
extern TypeInfo Reference_t1510_il2cpp_TypeInfo;
extern TypeInfo BackslashNumber_t1511_il2cpp_TypeInfo;
extern TypeInfo CharacterClass_t1512_il2cpp_TypeInfo;
extern TypeInfo AnchorInfo_t1500_il2cpp_TypeInfo;
extern TypeInfo DefaultUriParser_t1513_il2cpp_TypeInfo;
extern TypeInfo GenericUriParser_t1515_il2cpp_TypeInfo;
extern TypeInfo UriScheme_t1516_il2cpp_TypeInfo;
extern TypeInfo Uri_t1375_il2cpp_TypeInfo;
extern TypeInfo UriFormatException_t1518_il2cpp_TypeInfo;
extern TypeInfo UriHostNameType_t1520_il2cpp_TypeInfo;
extern TypeInfo UriKind_t1521_il2cpp_TypeInfo;
extern TypeInfo UriParser_t1514_il2cpp_TypeInfo;
extern TypeInfo UriPartial_t1522_il2cpp_TypeInfo;
extern TypeInfo UriTypeConverter_t1523_il2cpp_TypeInfo;
extern TypeInfo RemoteCertificateValidationCallback_t1381_il2cpp_TypeInfo;
extern TypeInfo $ArrayType$128_t1524_il2cpp_TypeInfo;
extern TypeInfo $ArrayType$12_t1525_il2cpp_TypeInfo;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1526_il2cpp_TypeInfo;
#include "utils/RegisterRuntimeInitializeAndCleanup.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_System_Assembly_Types[157] = 
{
	&U3CModuleU3E_t1340_il2cpp_TypeInfo,
	&Locale_t1341_il2cpp_TypeInfo,
	&MonoTODOAttribute_t1342_il2cpp_TypeInfo,
	&Enumerator_t1343_il2cpp_TypeInfo,
	&LinkedList_1_t1344_il2cpp_TypeInfo,
	&LinkedListNode_1_t1345_il2cpp_TypeInfo,
	&Enumerator_t1346_il2cpp_TypeInfo,
	&Stack_1_t1347_il2cpp_TypeInfo,
	&HybridDictionary_t1350_il2cpp_TypeInfo,
	&DictionaryNode_t1353_il2cpp_TypeInfo,
	&DictionaryNodeEnumerator_t1354_il2cpp_TypeInfo,
	&ListDictionary_t1349_il2cpp_TypeInfo,
	&_Item_t1357_il2cpp_TypeInfo,
	&_KeysEnumerator_t1359_il2cpp_TypeInfo,
	&KeysCollection_t1360_il2cpp_TypeInfo,
	&NameObjectCollectionBase_t1358_il2cpp_TypeInfo,
	&NameValueCollection_t1364_il2cpp_TypeInfo,
	&EditorBrowsableAttribute_t1182_il2cpp_TypeInfo,
	&EditorBrowsableState_t1365_il2cpp_TypeInfo,
	&TypeConverter_t1366_il2cpp_TypeInfo,
	&TypeConverterAttribute_t1367_il2cpp_TypeInfo,
	&AuthenticationLevel_t1368_il2cpp_TypeInfo,
	&SslPolicyErrors_t1369_il2cpp_TypeInfo,
	&AddressFamily_t1370_il2cpp_TypeInfo,
	&DefaultCertificatePolicy_t1371_il2cpp_TypeInfo,
	&FileWebRequest_t1378_il2cpp_TypeInfo,
	&FileWebRequestCreator_t1379_il2cpp_TypeInfo,
	&FtpRequestCreator_t1380_il2cpp_TypeInfo,
	&FtpWebRequest_t1382_il2cpp_TypeInfo,
	&GlobalProxySelection_t1384_il2cpp_TypeInfo,
	&HttpRequestCreator_t1385_il2cpp_TypeInfo,
	&HttpVersion_t1387_il2cpp_TypeInfo,
	&HttpWebRequest_t1389_il2cpp_TypeInfo,
	&ICertificatePolicy_t1395_il2cpp_TypeInfo,
	&ICredentials_t1399_il2cpp_TypeInfo,
	&IPAddress_t1391_il2cpp_TypeInfo,
	&IPv6Address_t1392_il2cpp_TypeInfo,
	&IWebProxy_t1377_il2cpp_TypeInfo,
	&IWebRequestCreate_t1552_il2cpp_TypeInfo,
	&SecurityProtocolType_t1393_il2cpp_TypeInfo,
	&ServicePoint_t1372_il2cpp_TypeInfo,
	&SPKey_t1394_il2cpp_TypeInfo,
	&ServicePointManager_t1396_il2cpp_TypeInfo,
	&WebHeaderCollection_t1376_il2cpp_TypeInfo,
	&WebProxy_t1400_il2cpp_TypeInfo,
	&WebRequest_t1374_il2cpp_TypeInfo,
	&OpenFlags_t1402_il2cpp_TypeInfo,
	&PublicKey_t1406_il2cpp_TypeInfo,
	&StoreLocation_t1410_il2cpp_TypeInfo,
	&StoreName_t1411_il2cpp_TypeInfo,
	&X500DistinguishedName_t1412_il2cpp_TypeInfo,
	&X500DistinguishedNameFlags_t1413_il2cpp_TypeInfo,
	&X509BasicConstraintsExtension_t1414_il2cpp_TypeInfo,
	&X509Certificate2_t1417_il2cpp_TypeInfo,
	&X509Certificate2Collection_t1419_il2cpp_TypeInfo,
	&X509Certificate2Enumerator_t1420_il2cpp_TypeInfo,
	&X509CertificateEnumerator_t1421_il2cpp_TypeInfo,
	&X509CertificateCollection_t1388_il2cpp_TypeInfo,
	&X509Chain_t1383_il2cpp_TypeInfo,
	&X509ChainElement_t1428_il2cpp_TypeInfo,
	&X509ChainElementCollection_t1424_il2cpp_TypeInfo,
	&X509ChainElementEnumerator_t1433_il2cpp_TypeInfo,
	&X509ChainPolicy_t1425_il2cpp_TypeInfo,
	&X509ChainStatus_t1427_il2cpp_TypeInfo,
	&X509ChainStatusFlags_t1435_il2cpp_TypeInfo,
	&X509EnhancedKeyUsageExtension_t1436_il2cpp_TypeInfo,
	&X509Extension_t1415_il2cpp_TypeInfo,
	&X509ExtensionCollection_t1416_il2cpp_TypeInfo,
	&X509ExtensionEnumerator_t1437_il2cpp_TypeInfo,
	&X509FindType_t1438_il2cpp_TypeInfo,
	&X509KeyUsageExtension_t1439_il2cpp_TypeInfo,
	&X509KeyUsageFlags_t1440_il2cpp_TypeInfo,
	&X509NameType_t1441_il2cpp_TypeInfo,
	&X509RevocationFlag_t1442_il2cpp_TypeInfo,
	&X509RevocationMode_t1443_il2cpp_TypeInfo,
	&X509Store_t1429_il2cpp_TypeInfo,
	&X509SubjectKeyIdentifierExtension_t1446_il2cpp_TypeInfo,
	&X509SubjectKeyIdentifierHashAlgorithm_t1447_il2cpp_TypeInfo,
	&X509VerificationFlags_t1448_il2cpp_TypeInfo,
	&AsnDecodeStatus_t1449_il2cpp_TypeInfo,
	&AsnEncodedData_t1404_il2cpp_TypeInfo,
	&Oid_t1405_il2cpp_TypeInfo,
	&OidCollection_t1434_il2cpp_TypeInfo,
	&OidEnumerator_t1450_il2cpp_TypeInfo,
	&BaseMachine_t1451_il2cpp_TypeInfo,
	&Capture_t1453_il2cpp_TypeInfo,
	&CaptureCollection_t1455_il2cpp_TypeInfo,
	&Group_t1456_il2cpp_TypeInfo,
	&GroupCollection_t1458_il2cpp_TypeInfo,
	&Match_t1452_il2cpp_TypeInfo,
	&Enumerator_t1461_il2cpp_TypeInfo,
	&MatchCollection_t1460_il2cpp_TypeInfo,
	&Regex_t861_il2cpp_TypeInfo,
	&RegexOptions_t1464_il2cpp_TypeInfo,
	&OpCode_t1465_il2cpp_TypeInfo,
	&OpFlags_t1466_il2cpp_TypeInfo,
	&Position_t1467_il2cpp_TypeInfo,
	&IMachine_t1459_il2cpp_TypeInfo,
	&IMachineFactory_t1463_il2cpp_TypeInfo,
	&Key_t1468_il2cpp_TypeInfo,
	&FactoryCache_t1462_il2cpp_TypeInfo,
	&Node_t1470_il2cpp_TypeInfo,
	&MRUList_t1469_il2cpp_TypeInfo,
	&Category_t1471_il2cpp_TypeInfo,
	&CategoryUtils_t1472_il2cpp_TypeInfo,
	&LinkRef_t1473_il2cpp_TypeInfo,
	&ICompiler_t1499_il2cpp_TypeInfo,
	&InterpreterFactory_t1474_il2cpp_TypeInfo,
	&Link_t1475_il2cpp_TypeInfo,
	&PatternLinkStack_t1476_il2cpp_TypeInfo,
	&PatternCompiler_t1478_il2cpp_TypeInfo,
	&LinkStack_t1477_il2cpp_TypeInfo,
	&Mark_t1480_il2cpp_TypeInfo,
	&IntStack_t1481_il2cpp_TypeInfo,
	&RepeatContext_t1482_il2cpp_TypeInfo,
	&Mode_t1483_il2cpp_TypeInfo,
	&Interpreter_t1486_il2cpp_TypeInfo,
	&Interval_t1487_il2cpp_TypeInfo,
	&Enumerator_t1489_il2cpp_TypeInfo,
	&CostDelegate_t1490_il2cpp_TypeInfo,
	&IntervalCollection_t1491_il2cpp_TypeInfo,
	&Parser_t1492_il2cpp_TypeInfo,
	&QuickSearch_t1484_il2cpp_TypeInfo,
	&ExpressionCollection_t1498_il2cpp_TypeInfo,
	&Expression_t1496_il2cpp_TypeInfo,
	&CompositeExpression_t1501_il2cpp_TypeInfo,
	&Group_t1494_il2cpp_TypeInfo,
	&RegularExpression_t1493_il2cpp_TypeInfo,
	&CapturingGroup_t1502_il2cpp_TypeInfo,
	&BalancingGroup_t1503_il2cpp_TypeInfo,
	&NonBacktrackingGroup_t1504_il2cpp_TypeInfo,
	&Repetition_t1505_il2cpp_TypeInfo,
	&Assertion_t1495_il2cpp_TypeInfo,
	&CaptureAssertion_t1507_il2cpp_TypeInfo,
	&ExpressionAssertion_t1497_il2cpp_TypeInfo,
	&Alternation_t1508_il2cpp_TypeInfo,
	&Literal_t1506_il2cpp_TypeInfo,
	&PositionAssertion_t1509_il2cpp_TypeInfo,
	&Reference_t1510_il2cpp_TypeInfo,
	&BackslashNumber_t1511_il2cpp_TypeInfo,
	&CharacterClass_t1512_il2cpp_TypeInfo,
	&AnchorInfo_t1500_il2cpp_TypeInfo,
	&DefaultUriParser_t1513_il2cpp_TypeInfo,
	&GenericUriParser_t1515_il2cpp_TypeInfo,
	&UriScheme_t1516_il2cpp_TypeInfo,
	&Uri_t1375_il2cpp_TypeInfo,
	&UriFormatException_t1518_il2cpp_TypeInfo,
	&UriHostNameType_t1520_il2cpp_TypeInfo,
	&UriKind_t1521_il2cpp_TypeInfo,
	&UriParser_t1514_il2cpp_TypeInfo,
	&UriPartial_t1522_il2cpp_TypeInfo,
	&UriTypeConverter_t1523_il2cpp_TypeInfo,
	&RemoteCertificateValidationCallback_t1381_il2cpp_TypeInfo,
	&$ArrayType$128_t1524_il2cpp_TypeInfo,
	&$ArrayType$12_t1525_il2cpp_TypeInfo,
	&U3CPrivateImplementationDetailsU3E_t1526_il2cpp_TypeInfo,
	NULL,
};
extern Il2CppImage g_System_dll_Image;
extern CustomAttributesCache g_System_Assembly__CustomAttributeCache;
Il2CppAssembly g_System_Assembly = 
{
	{ "System", 0, 0, "\x0\x24\x0\x0\x4\x80\x0\x0\x94\x0\x0\x0\x6\x2\x0\x0\x0\x24\x0\x0\x52\x53\x41\x31\x0\x4\x0\x0\x1\x0\x1\x0\x8D\x56\xC7\x6F\x9E\x86\x49\x38\x30\x49\xF3\x83\xC4\x4B\xE0\xEC\x20\x41\x81\x82\x2A\x6C\x31\xCF\x5E\xB7\xEF\x48\x69\x44\xD0\x32\x18\x8E\xA1\xD3\x92\x7\x63\x71\x2C\xCB\x12\xD7\x5F\xB7\x7E\x98\x11\x14\x9E\x61\x48\xE5\xD3\x2F\xBA\xAB\x37\x61\x1C\x18\x78\xDD\xC1\x9E\x20\xEF\x13\x5D\xC\xB2\xCF\xF2\xBF\xEC\x3D\x11\x58\x10\xC3\xD9\x6\x96\x38\xFE\x4B\xE2\x15\xDB\xF7\x95\x86\x19\x20\xE5\xAB\x6F\x7D\xB2\xE2\xCE\xEF\x13\x6A\xC2\x3D\x5D\xD2\xBF\x3\x17\x0\xAE\xC2\x32\xF6\xC6\xB1\xC7\x85\xB4\x30\x5C\x12\x3B\x37\xAB", { 0x7C, 0xEC, 0x85, 0xD7, 0xBE, 0xA7, 0x79, 0x8E }, 32772, 0, 1, 2, 0, 5, 0 },
	&g_System_dll_Image,
	&g_System_Assembly__CustomAttributeCache,
};
Il2CppImage g_System_dll_Image = 
{
	 "System.dll" ,
	&g_System_Assembly,
	g_System_Assembly_Types,
	156,
	NULL,
};
static void s_SystemRegistration()
{
	RegisterAssembly (&g_System_Assembly);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_SystemRegistrationVariable(&s_SystemRegistration, NULL);
