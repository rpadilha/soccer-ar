﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.CryptographicException
#include "mscorlib_System_Security_Cryptography_CryptographicException.h"
// System.Security.Cryptography.CryptographicUnexpectedOperationException
struct CryptographicUnexpectedOperationException_t1570  : public CryptographicException_t1563
{
};
