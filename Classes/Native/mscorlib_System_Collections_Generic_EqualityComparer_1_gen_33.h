﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.Trackable>
struct EqualityComparer_1_t3988;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.Trackable>
struct EqualityComparer_1_t3988  : public Object_t
{
};
struct EqualityComparer_1_t3988_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.Trackable>::_default
	EqualityComparer_1_t3988 * ____default_0;
};
