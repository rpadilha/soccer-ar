﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImpl.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vuforia.CloudRecoImageTargetImpl
struct CloudRecoImageTargetImpl_t638  : public TrackableImpl_t609
{
	// UnityEngine.Vector3 Vuforia.CloudRecoImageTargetImpl::mSize
	Vector3_t73  ___mSize_2;
};
