﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>
struct Enumerator_t3802;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t444;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct Stack_1_t3799;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m20784(__this, ___t, method) (void)Enumerator__ctor_m16415_gshared((Enumerator_t3210 *)__this, (Stack_1_t3209 *)___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20785(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m16416_gshared((Enumerator_t3210 *)__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::Dispose()
#define Enumerator_Dispose_m20786(__this, method) (void)Enumerator_Dispose_m16417_gshared((Enumerator_t3210 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::MoveNext()
#define Enumerator_MoveNext_m20787(__this, method) (bool)Enumerator_MoveNext_m16418_gshared((Enumerator_t3210 *)__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Current()
#define Enumerator_get_Current_m20788(__this, method) (List_1_t444 *)Enumerator_get_Current_m16419_gshared((Enumerator_t3210 *)__this, method)
