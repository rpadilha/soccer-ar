﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA384
struct HMACSHA384_t2152;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void System.Security.Cryptography.HMACSHA384::.ctor()
 void HMACSHA384__ctor_m12099 (HMACSHA384_t2152 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::.ctor(System.Byte[])
 void HMACSHA384__ctor_m12100 (HMACSHA384_t2152 * __this, ByteU5BU5D_t653* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::.cctor()
 void HMACSHA384__cctor_m12101 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::set_ProduceLegacyHmacValues(System.Boolean)
 void HMACSHA384_set_ProduceLegacyHmacValues_m12102 (HMACSHA384_t2152 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
