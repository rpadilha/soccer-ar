﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.TrackedReference
struct TrackedReference_t1072;
struct TrackedReference_t1072_marshaled;
// System.Object
struct Object_t;

// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
 bool TrackedReference_Equals_m6523 (TrackedReference_t1072 * __this, Object_t * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
 int32_t TrackedReference_GetHashCode_m6524 (TrackedReference_t1072 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
 bool TrackedReference_op_Equality_m6525 (Object_t * __this/* static, unused */, TrackedReference_t1072 * ___x, TrackedReference_t1072 * ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
void TrackedReference_t1072_marshal(const TrackedReference_t1072& unmarshaled, TrackedReference_t1072_marshaled& marshaled);
void TrackedReference_t1072_marshal_back(const TrackedReference_t1072_marshaled& marshaled, TrackedReference_t1072& unmarshaled);
void TrackedReference_t1072_marshal_cleanup(TrackedReference_t1072_marshaled& marshaled);
