﻿#pragma once
#include <stdint.h>
// Vuforia.Marker
struct Marker_t667;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Marker>
struct Comparison_1_t4065  : public MulticastDelegate_t373
{
};
