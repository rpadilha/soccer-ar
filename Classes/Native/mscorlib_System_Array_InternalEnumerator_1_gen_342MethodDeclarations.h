﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARRenderer/VideoBackgroundReflection>
struct InternalEnumerator_1_t4086;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARRenderer/VideoBackgroundReflection>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m23203 (InternalEnumerator_1_t4086 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARRenderer/VideoBackgroundReflection>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23204 (InternalEnumerator_1_t4086 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARRenderer/VideoBackgroundReflection>::Dispose()
 void InternalEnumerator_1_Dispose_m23205 (InternalEnumerator_1_t4086 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARRenderer/VideoBackgroundReflection>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m23206 (InternalEnumerator_1_t4086 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARRenderer/VideoBackgroundReflection>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m23207 (InternalEnumerator_1_t4086 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
