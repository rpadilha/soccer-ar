﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Joystick_Script>
struct Enumerator_t200;
// System.Object
struct Object_t;
// Joystick_Script
struct Joystick_Script_t102;
// System.Collections.Generic.List`1<Joystick_Script>
struct List_1_t101;

// System.Void System.Collections.Generic.List`1/Enumerator<Joystick_Script>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26MethodDeclarations.h"
#define Enumerator__ctor_m15651(__this, ___l, method) (void)Enumerator__ctor_m14558_gshared((Enumerator_t2837 *)__this, (List_1_t149 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Joystick_Script>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15652(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14559_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Joystick_Script>::Dispose()
#define Enumerator_Dispose_m15653(__this, method) (void)Enumerator_Dispose_m14560_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Joystick_Script>::VerifyState()
#define Enumerator_VerifyState_m15654(__this, method) (void)Enumerator_VerifyState_m14561_gshared((Enumerator_t2837 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Joystick_Script>::MoveNext()
#define Enumerator_MoveNext_m695(__this, method) (bool)Enumerator_MoveNext_m14562_gshared((Enumerator_t2837 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<Joystick_Script>::get_Current()
#define Enumerator_get_Current_m694(__this, method) (Joystick_Script_t102 *)Enumerator_get_Current_m14563_gshared((Enumerator_t2837 *)__this, method)
