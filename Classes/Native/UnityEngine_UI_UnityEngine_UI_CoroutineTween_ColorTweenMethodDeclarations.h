﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t314;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t315;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"

// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::get_startColor()
 Color_t66  ColorTween_get_startColor_m1131 (ColorTween_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_startColor(UnityEngine.Color)
 void ColorTween_set_startColor_m1132 (ColorTween_t314 * __this, Color_t66  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::get_targetColor()
 Color_t66  ColorTween_get_targetColor_m1133 (ColorTween_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_targetColor(UnityEngine.Color)
 void ColorTween_set_targetColor_m1134 (ColorTween_t314 * __this, Color_t66  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::get_tweenMode()
 int32_t ColorTween_get_tweenMode_m1135 (ColorTween_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_tweenMode(UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode)
 void ColorTween_set_tweenMode_m1136 (ColorTween_t314 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::get_duration()
 float ColorTween_get_duration_m1137 (ColorTween_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_duration(System.Single)
 void ColorTween_set_duration_m1138 (ColorTween_t314 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::get_ignoreTimeScale()
 bool ColorTween_get_ignoreTimeScale_m1139 (ColorTween_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_ignoreTimeScale(System.Boolean)
 void ColorTween_set_ignoreTimeScale_m1140 (ColorTween_t314 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::TweenValue(System.Single)
 void ColorTween_TweenValue_m1141 (ColorTween_t314 * __this, float ___floatPercentage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::AddOnChangedCallback(UnityEngine.Events.UnityAction`1<UnityEngine.Color>)
 void ColorTween_AddOnChangedCallback_m1142 (ColorTween_t314 * __this, UnityAction_1_t315 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::GetIgnoreTimescale()
 bool ColorTween_GetIgnoreTimescale_m1143 (ColorTween_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::GetDuration()
 float ColorTween_GetDuration_m1144 (ColorTween_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::ValidTarget()
 bool ColorTween_ValidTarget_m1145 (ColorTween_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
