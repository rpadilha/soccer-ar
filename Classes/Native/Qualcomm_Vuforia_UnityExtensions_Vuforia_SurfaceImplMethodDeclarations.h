﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceImpl
struct SurfaceImpl_t713;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t625;
// UnityEngine.Mesh
struct Mesh_t174;
// System.Int32[]
struct Int32U5BU5D_t175;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void Vuforia.SurfaceImpl::.ctor(System.Int32,Vuforia.SmartTerrainTrackable)
 void SurfaceImpl__ctor_m3210 (SurfaceImpl_t713 * __this, int32_t ___id, Object_t * ___parent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetID(System.Int32)
 void SurfaceImpl_SetID_m3211 (SurfaceImpl_t713 * __this, int32_t ___trackableID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetMesh(System.Int32,UnityEngine.Mesh,UnityEngine.Mesh,System.Int32[])
 void SurfaceImpl_SetMesh_m3212 (SurfaceImpl_t713 * __this, int32_t ___meshRev, Mesh_t174 * ___mesh, Mesh_t174 * ___navMesh, Int32U5BU5D_t175* ___meshBoundaries, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetBoundingBox(UnityEngine.Rect)
 void SurfaceImpl_SetBoundingBox_m3213 (SurfaceImpl_t713 * __this, Rect_t103  ___boundingBox, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh Vuforia.SurfaceImpl::GetNavMesh()
 Mesh_t174 * SurfaceImpl_GetNavMesh_m3214 (SurfaceImpl_t713 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Vuforia.SurfaceImpl::GetMeshBoundaries()
 Int32U5BU5D_t175* SurfaceImpl_GetMeshBoundaries_m3215 (SurfaceImpl_t713 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.SurfaceImpl::get_BoundingBox()
 Rect_t103  SurfaceImpl_get_BoundingBox_m3216 (SurfaceImpl_t713 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.SurfaceImpl::GetArea()
 float SurfaceImpl_GetArea_m3217 (SurfaceImpl_t713 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
