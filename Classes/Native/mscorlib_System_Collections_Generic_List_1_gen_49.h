﻿#pragma once
#include <stdint.h>
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t4699;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_t1008  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::_items
	GUILayoutEntryU5BU5D_t4699* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::_version
	int32_t ____version_3;
};
struct List_1_t1008_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::EmptyArray
	GUILayoutEntryU5BU5D_t4699* ___EmptyArray_4;
};
