﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>
struct DefaultComparer_t3960;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::.ctor()
 void DefaultComparer__ctor_m21898 (DefaultComparer_t3960 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m21899 (DefaultComparer_t3960 * __this, int32_t ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::Equals(T,T)
 bool DefaultComparer_Equals_m21900 (DefaultComparer_t3960 * __this, int32_t ___x, int32_t ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
