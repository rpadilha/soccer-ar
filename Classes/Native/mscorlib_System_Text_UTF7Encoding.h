﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t653;
// System.SByte[]
struct SByteU5BU5D_t2213;
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// System.Text.UTF7Encoding
struct UTF7Encoding_t2214  : public Encoding_t1566
{
	// System.Boolean System.Text.UTF7Encoding::allowOptionals
	bool ___allowOptionals_28;
};
struct UTF7Encoding_t2214_StaticFields{
	// System.Byte[] System.Text.UTF7Encoding::encodingRules
	ByteU5BU5D_t653* ___encodingRules_29;
	// System.SByte[] System.Text.UTF7Encoding::base64Values
	SByteU5BU5D_t2213* ___base64Values_30;
};
