﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>
struct InternalEnumerator_1_t5235;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31549 (InternalEnumerator_1_t5235 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31550 (InternalEnumerator_1_t5235 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::Dispose()
 void InternalEnumerator_1_Dispose_m31551 (InternalEnumerator_1_t5235 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31552 (InternalEnumerator_1_t5235 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31553 (InternalEnumerator_1_t5235 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
