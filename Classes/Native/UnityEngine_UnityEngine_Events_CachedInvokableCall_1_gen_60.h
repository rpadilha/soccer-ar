﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<FollowTransform>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_56.h"
// UnityEngine.Events.CachedInvokableCall`1<FollowTransform>
struct CachedInvokableCall_1_t3125  : public InvokableCall_1_t3126
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<FollowTransform>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
