﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Joystick_Script
struct Joystick_Script_t102;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Joystick_Script>
struct UnityAction_1_t3068  : public MulticastDelegate_t373
{
};
