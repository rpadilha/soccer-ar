﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct Enumerator_t3213;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t291;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct Stack_1_t3207;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m16446(__this, ___t, method) (void)Enumerator__ctor_m16415_gshared((Enumerator_t3210 *)__this, (Stack_1_t3209 *)___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16447(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m16416_gshared((Enumerator_t3210 *)__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Dispose()
#define Enumerator_Dispose_m16448(__this, method) (void)Enumerator_Dispose_m16417_gshared((Enumerator_t3210 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::MoveNext()
#define Enumerator_MoveNext_m16449(__this, method) (bool)Enumerator_MoveNext_m16418_gshared((Enumerator_t3210 *)__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_Current()
#define Enumerator_get_Current_m16450(__this, method) (List_1_t291 *)Enumerator_get_Current_m16419_gshared((Enumerator_t3210 *)__this, method)
