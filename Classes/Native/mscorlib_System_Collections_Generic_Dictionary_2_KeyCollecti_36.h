﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct Enumerator_t4275 
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::host_enumerator
	Enumerator_t4273  ___host_enumerator_0;
};
