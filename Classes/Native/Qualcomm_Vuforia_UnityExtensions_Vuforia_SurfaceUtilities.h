﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// Vuforia.SurfaceUtilities
struct SurfaceUtilities_t136  : public Object_t
{
};
struct SurfaceUtilities_t136_StaticFields{
	// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::mScreenOrientation
	int32_t ___mScreenOrientation_0;
};
