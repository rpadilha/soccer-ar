﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t444;
// UnityEngine.Component
struct Component_t128;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>
struct Enumerator_t3223 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::l
	List_1_t444 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::current
	Component_t128 * ___current_3;
};
