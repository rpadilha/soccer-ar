﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButton[]
struct VirtualButtonU5BU5D_t3925;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.VirtualButton>
struct List_1_t833  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.VirtualButton>::_items
	VirtualButtonU5BU5D_t3925* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::_version
	int32_t ____version_3;
};
struct List_1_t833_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.VirtualButton>::EmptyArray
	VirtualButtonU5BU5D_t3925* ___EmptyArray_4;
};
