﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<ScorerTimeHUD>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_43.h"
// UnityEngine.Events.CachedInvokableCall`1<ScorerTimeHUD>
struct CachedInvokableCall_1_t3048  : public InvokableCall_1_t3049
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<ScorerTimeHUD>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
