﻿#pragma once
#include <stdint.h>
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t189;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.BoxCollider2D>
struct CastHelper_1_t3041 
{
	// T UnityEngine.CastHelper`1<UnityEngine.BoxCollider2D>::t
	BoxCollider2D_t189 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.BoxCollider2D>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
