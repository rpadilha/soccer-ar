﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.UI.Graphic,System.Int32>
struct ShimEnumerator_t3579;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.Graphic,System.Int32>
struct Dictionary_2_t518;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.UI.Graphic,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m19368 (ShimEnumerator_t3579 * __this, Dictionary_2_t518 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.UI.Graphic,System.Int32>::MoveNext()
 bool ShimEnumerator_MoveNext_m19369 (ShimEnumerator_t3579 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.UI.Graphic,System.Int32>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m19370 (ShimEnumerator_t3579 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.UI.Graphic,System.Int32>::get_Key()
 Object_t * ShimEnumerator_get_Key_m19371 (ShimEnumerator_t3579 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.UI.Graphic,System.Int32>::get_Value()
 Object_t * ShimEnumerator_get_Value_m19372 (ShimEnumerator_t3579 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.UI.Graphic,System.Int32>::get_Current()
 Object_t * ShimEnumerator_get_Current_m19373 (ShimEnumerator_t3579 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
