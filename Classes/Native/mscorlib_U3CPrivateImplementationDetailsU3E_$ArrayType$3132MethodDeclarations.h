﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$3132
struct $ArrayType$3132_t2319;
struct $ArrayType$3132_t2319_marshaled;

void $ArrayType$3132_t2319_marshal(const $ArrayType$3132_t2319& unmarshaled, $ArrayType$3132_t2319_marshaled& marshaled);
void $ArrayType$3132_t2319_marshal_back(const $ArrayType$3132_t2319_marshaled& marshaled, $ArrayType$3132_t2319& unmarshaled);
void $ArrayType$3132_t2319_marshal_cleanup($ArrayType$3132_t2319_marshaled& marshaled);
