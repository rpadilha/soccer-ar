﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo ICollection_1_t8739_il2cpp_TypeInfo;

// System.Int32
#include "mscorlib_System_Int32.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.BoxCollider2D
#include "UnityEngine_UnityEngine_BoxCollider2D.h"
#include "UnityEngine_ArrayTypes.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>
extern MethodInfo ICollection_1_get_Count_m49204_MethodInfo;
static PropertyInfo ICollection_1_t8739____Count_PropertyInfo = 
{
	&ICollection_1_t8739_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49204_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49205_MethodInfo;
static PropertyInfo ICollection_1_t8739____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8739_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49205_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8739_PropertyInfos[] =
{
	&ICollection_1_t8739____Count_PropertyInfo,
	&ICollection_1_t8739____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49204_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::get_Count()
MethodInfo ICollection_1_get_Count_m49204_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8739_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49204_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49205_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49205_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8739_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49205_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider2D_t189_0_0_0;
extern Il2CppType BoxCollider2D_t189_0_0_0;
static ParameterInfo ICollection_1_t8739_ICollection_1_Add_m49206_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider2D_t189_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49206_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::Add(T)
MethodInfo ICollection_1_Add_m49206_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8739_ICollection_1_Add_m49206_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49206_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49207_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::Clear()
MethodInfo ICollection_1_Clear_m49207_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49207_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider2D_t189_0_0_0;
static ParameterInfo ICollection_1_t8739_ICollection_1_Contains_m49208_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider2D_t189_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49208_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::Contains(T)
MethodInfo ICollection_1_Contains_m49208_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8739_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8739_ICollection_1_Contains_m49208_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49208_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider2DU5BU5D_t5723_0_0_0;
extern Il2CppType BoxCollider2DU5BU5D_t5723_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8739_ICollection_1_CopyTo_m49209_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider2DU5BU5D_t5723_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49209_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49209_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8739_ICollection_1_CopyTo_m49209_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49209_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider2D_t189_0_0_0;
static ParameterInfo ICollection_1_t8739_ICollection_1_Remove_m49210_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider2D_t189_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49210_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider2D>::Remove(T)
MethodInfo ICollection_1_Remove_m49210_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8739_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8739_ICollection_1_Remove_m49210_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49210_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8739_MethodInfos[] =
{
	&ICollection_1_get_Count_m49204_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49205_MethodInfo,
	&ICollection_1_Add_m49206_MethodInfo,
	&ICollection_1_Clear_m49207_MethodInfo,
	&ICollection_1_Contains_m49208_MethodInfo,
	&ICollection_1_CopyTo_m49209_MethodInfo,
	&ICollection_1_Remove_m49210_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8741_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8739_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8741_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8739_0_0_0;
extern Il2CppType ICollection_1_t8739_1_0_0;
struct ICollection_1_t8739;
extern Il2CppGenericClass ICollection_1_t8739_GenericClass;
TypeInfo ICollection_1_t8739_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8739_MethodInfos/* methods */
	, ICollection_1_t8739_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8739_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8739_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8739_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8739_0_0_0/* byval_arg */
	, &ICollection_1_t8739_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8739_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.BoxCollider2D>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.BoxCollider2D>
extern Il2CppType IEnumerator_1_t6889_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49211_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.BoxCollider2D>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49211_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8741_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6889_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49211_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8741_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49211_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8741_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8741_0_0_0;
extern Il2CppType IEnumerable_1_t8741_1_0_0;
struct IEnumerable_1_t8741;
extern Il2CppGenericClass IEnumerable_1_t8741_GenericClass;
TypeInfo IEnumerable_1_t8741_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8741_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8741_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8741_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8741_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8741_0_0_0/* byval_arg */
	, &IEnumerable_1_t8741_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8741_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8740_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>
extern MethodInfo IList_1_get_Item_m49212_MethodInfo;
extern MethodInfo IList_1_set_Item_m49213_MethodInfo;
static PropertyInfo IList_1_t8740____Item_PropertyInfo = 
{
	&IList_1_t8740_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49212_MethodInfo/* get */
	, &IList_1_set_Item_m49213_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8740_PropertyInfos[] =
{
	&IList_1_t8740____Item_PropertyInfo,
	NULL
};
extern Il2CppType BoxCollider2D_t189_0_0_0;
static ParameterInfo IList_1_t8740_IList_1_IndexOf_m49214_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider2D_t189_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49214_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49214_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8740_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8740_IList_1_IndexOf_m49214_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49214_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BoxCollider2D_t189_0_0_0;
static ParameterInfo IList_1_t8740_IList_1_Insert_m49215_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BoxCollider2D_t189_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49215_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49215_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8740_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8740_IList_1_Insert_m49215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49215_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8740_IList_1_RemoveAt_m49216_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49216_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49216_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8740_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8740_IList_1_RemoveAt_m49216_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49216_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8740_IList_1_get_Item_m49212_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType BoxCollider2D_t189_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49212_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49212_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8740_il2cpp_TypeInfo/* declaring_type */
	, &BoxCollider2D_t189_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8740_IList_1_get_Item_m49212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49212_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BoxCollider2D_t189_0_0_0;
static ParameterInfo IList_1_t8740_IList_1_set_Item_m49213_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BoxCollider2D_t189_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49213_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider2D>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49213_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8740_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8740_IList_1_set_Item_m49213_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49213_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8740_MethodInfos[] =
{
	&IList_1_IndexOf_m49214_MethodInfo,
	&IList_1_Insert_m49215_MethodInfo,
	&IList_1_RemoveAt_m49216_MethodInfo,
	&IList_1_get_Item_m49212_MethodInfo,
	&IList_1_set_Item_m49213_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8740_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8739_il2cpp_TypeInfo,
	&IEnumerable_1_t8741_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8740_0_0_0;
extern Il2CppType IList_1_t8740_1_0_0;
struct IList_1_t8740;
extern Il2CppGenericClass IList_1_t8740_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8740_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8740_MethodInfos/* methods */
	, IList_1_t8740_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8740_il2cpp_TypeInfo/* element_class */
	, IList_1_t8740_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8740_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8740_0_0_0/* byval_arg */
	, &IList_1_t8740_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8740_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider2D>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_170.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4850_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider2D>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_170MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_ArrayTypes.h"
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_172.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo BoxCollider2D_t189_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4851_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_172MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m29298_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m29300_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider2D>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider2D>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider2D>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4850____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4850_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4850, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4850_FieldInfos[] =
{
	&CachedInvokableCall_1_t4850____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType BoxCollider2D_t189_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4850_CachedInvokableCall_1__ctor_m29296_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &BoxCollider2D_t189_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m29296_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider2D>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m29296_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4850_CachedInvokableCall_1__ctor_m29296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m29296_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4850_CachedInvokableCall_1_Invoke_m29297_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m29297_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider2D>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m29297_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4850_CachedInvokableCall_1_Invoke_m29297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m29297_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4850_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m29296_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29297_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m29297_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m29301_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4850_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29297_MethodInfo,
	&InvokableCall_1_Find_m29301_MethodInfo,
};
extern Il2CppType UnityAction_1_t4852_0_0_0;
extern TypeInfo UnityAction_1_t4852_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisBoxCollider2D_t189_m38633_MethodInfo;
extern TypeInfo BoxCollider2D_t189_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m29303_MethodInfo;
extern TypeInfo BoxCollider2D_t189_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4850_RGCTXData[8] = 
{
	&UnityAction_1_t4852_0_0_0/* Type Usage */,
	&UnityAction_1_t4852_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBoxCollider2D_t189_m38633_MethodInfo/* Method Usage */,
	&BoxCollider2D_t189_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29303_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m29298_MethodInfo/* Method Usage */,
	&BoxCollider2D_t189_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m29300_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4850_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4850_1_0_0;
struct CachedInvokableCall_1_t4850;
extern Il2CppGenericClass CachedInvokableCall_1_t4850_GenericClass;
TypeInfo CachedInvokableCall_1_t4850_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4850_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4850_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4851_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4850_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4850_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4850_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4850_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4850_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4850_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4850_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4850)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_176.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t4852_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_176MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.BoxCollider2D>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.BoxCollider2D>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisBoxCollider2D_t189_m38633(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>
extern Il2CppType UnityAction_1_t4852_0_0_1;
FieldInfo InvokableCall_1_t4851____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4852_0_0_1/* type */
	, &InvokableCall_1_t4851_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4851, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4851_FieldInfos[] =
{
	&InvokableCall_1_t4851____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4851_InvokableCall_1__ctor_m29298_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29298_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m29298_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4851_InvokableCall_1__ctor_m29298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29298_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4852_0_0_0;
static ParameterInfo InvokableCall_1_t4851_InvokableCall_1__ctor_m29299_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4852_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29299_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m29299_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4851_InvokableCall_1__ctor_m29299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29299_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4851_InvokableCall_1_Invoke_m29300_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m29300_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m29300_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4851_InvokableCall_1_Invoke_m29300_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m29300_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4851_InvokableCall_1_Find_m29301_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m29301_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider2D>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m29301_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4851_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4851_InvokableCall_1_Find_m29301_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m29301_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4851_MethodInfos[] =
{
	&InvokableCall_1__ctor_m29298_MethodInfo,
	&InvokableCall_1__ctor_m29299_MethodInfo,
	&InvokableCall_1_Invoke_m29300_MethodInfo,
	&InvokableCall_1_Find_m29301_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4851_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m29300_MethodInfo,
	&InvokableCall_1_Find_m29301_MethodInfo,
};
extern TypeInfo UnityAction_1_t4852_il2cpp_TypeInfo;
extern TypeInfo BoxCollider2D_t189_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4851_RGCTXData[5] = 
{
	&UnityAction_1_t4852_0_0_0/* Type Usage */,
	&UnityAction_1_t4852_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBoxCollider2D_t189_m38633_MethodInfo/* Method Usage */,
	&BoxCollider2D_t189_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29303_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4851_0_0_0;
extern Il2CppType InvokableCall_1_t4851_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t4851;
extern Il2CppGenericClass InvokableCall_1_t4851_GenericClass;
TypeInfo InvokableCall_1_t4851_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4851_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4851_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4851_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4851_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4851_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4851_0_0_0/* byval_arg */
	, &InvokableCall_1_t4851_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4851_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4851_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4851)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4852_UnityAction_1__ctor_m29302_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m29302_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m29302_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4852_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4852_UnityAction_1__ctor_m29302_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m29302_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider2D_t189_0_0_0;
static ParameterInfo UnityAction_1_t4852_UnityAction_1_Invoke_m29303_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider2D_t189_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m29303_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m29303_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4852_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4852_UnityAction_1_Invoke_m29303_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m29303_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider2D_t189_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4852_UnityAction_1_BeginInvoke_m29304_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider2D_t189_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m29304_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m29304_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4852_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4852_UnityAction_1_BeginInvoke_m29304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m29304_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4852_UnityAction_1_EndInvoke_m29305_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m29305_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider2D>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m29305_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4852_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4852_UnityAction_1_EndInvoke_m29305_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m29305_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4852_MethodInfos[] =
{
	&UnityAction_1__ctor_m29302_MethodInfo,
	&UnityAction_1_Invoke_m29303_MethodInfo,
	&UnityAction_1_BeginInvoke_m29304_MethodInfo,
	&UnityAction_1_EndInvoke_m29305_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m29304_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m29305_MethodInfo;
static MethodInfo* UnityAction_1_t4852_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m29303_MethodInfo,
	&UnityAction_1_BeginInvoke_m29304_MethodInfo,
	&UnityAction_1_EndInvoke_m29305_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t4852_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4852_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_1_t4852;
extern Il2CppGenericClass UnityAction_1_t4852_GenericClass;
TypeInfo UnityAction_1_t4852_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4852_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4852_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4852_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4852_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4852_0_0_0/* byval_arg */
	, &UnityAction_1_t4852_1_0_0/* this_arg */
	, UnityAction_1_t4852_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4852_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4852)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6891_il2cpp_TypeInfo;

// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.AudioClip>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.AudioClip>
extern MethodInfo IEnumerator_1_get_Current_m49217_MethodInfo;
static PropertyInfo IEnumerator_1_t6891____Current_PropertyInfo = 
{
	&IEnumerator_1_t6891_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49217_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6891_PropertyInfos[] =
{
	&IEnumerator_1_t6891____Current_PropertyInfo,
	NULL
};
extern Il2CppType AudioClip_t1061_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49217_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.AudioClip>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49217_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6891_il2cpp_TypeInfo/* declaring_type */
	, &AudioClip_t1061_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49217_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6891_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49217_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6891_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6891_0_0_0;
extern Il2CppType IEnumerator_1_t6891_1_0_0;
struct IEnumerator_1_t6891;
extern Il2CppGenericClass IEnumerator_1_t6891_GenericClass;
TypeInfo IEnumerator_1_t6891_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6891_MethodInfos/* methods */
	, IEnumerator_1_t6891_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6891_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6891_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6891_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6891_0_0_0/* byval_arg */
	, &IEnumerator_1_t6891_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6891_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.AudioClip>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_458.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4853_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.AudioClip>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_458MethodDeclarations.h"

// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo AudioClip_t1061_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m29310_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAudioClip_t1061_m38635_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.AudioClip>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.AudioClip>(System.Int32)
#define Array_InternalArray__get_Item_TisAudioClip_t1061_m38635(__this, p0, method) (AudioClip_t1061 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.AudioClip>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.AudioClip>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AudioClip>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.AudioClip>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.AudioClip>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.AudioClip>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4853____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4853_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4853, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4853____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4853_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4853, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4853_FieldInfos[] =
{
	&InternalEnumerator_1_t4853____array_0_FieldInfo,
	&InternalEnumerator_1_t4853____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29307_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4853____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4853_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29307_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4853____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4853_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29310_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4853_PropertyInfos[] =
{
	&InternalEnumerator_1_t4853____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4853____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4853_InternalEnumerator_1__ctor_m29306_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29306_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AudioClip>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29306_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4853_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4853_InternalEnumerator_1__ctor_m29306_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29306_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29307_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.AudioClip>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29307_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4853_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29307_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29308_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AudioClip>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29308_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4853_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29308_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29309_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.AudioClip>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29309_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4853_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29309_GenericMethod/* genericMethod */

};
extern Il2CppType AudioClip_t1061_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29310_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.AudioClip>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29310_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4853_il2cpp_TypeInfo/* declaring_type */
	, &AudioClip_t1061_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29310_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4853_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29306_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29307_MethodInfo,
	&InternalEnumerator_1_Dispose_m29308_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29309_MethodInfo,
	&InternalEnumerator_1_get_Current_m29310_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m29309_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29308_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4853_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29307_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29309_MethodInfo,
	&InternalEnumerator_1_Dispose_m29308_MethodInfo,
	&InternalEnumerator_1_get_Current_m29310_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4853_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6891_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4853_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6891_il2cpp_TypeInfo, 7},
};
extern TypeInfo AudioClip_t1061_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4853_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29310_MethodInfo/* Method Usage */,
	&AudioClip_t1061_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAudioClip_t1061_m38635_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4853_0_0_0;
extern Il2CppType InternalEnumerator_1_t4853_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t4853_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t4853_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4853_MethodInfos/* methods */
	, InternalEnumerator_1_t4853_PropertyInfos/* properties */
	, InternalEnumerator_1_t4853_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4853_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4853_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4853_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4853_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4853_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4853_1_0_0/* this_arg */
	, InternalEnumerator_1_t4853_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4853_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4853_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4853)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8742_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>
extern MethodInfo ICollection_1_get_Count_m49218_MethodInfo;
static PropertyInfo ICollection_1_t8742____Count_PropertyInfo = 
{
	&ICollection_1_t8742_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49218_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49219_MethodInfo;
static PropertyInfo ICollection_1_t8742____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8742_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49219_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8742_PropertyInfos[] =
{
	&ICollection_1_t8742____Count_PropertyInfo,
	&ICollection_1_t8742____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49218_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::get_Count()
MethodInfo ICollection_1_get_Count_m49218_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8742_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49218_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49219_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49219_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8742_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49219_GenericMethod/* genericMethod */

};
extern Il2CppType AudioClip_t1061_0_0_0;
extern Il2CppType AudioClip_t1061_0_0_0;
static ParameterInfo ICollection_1_t8742_ICollection_1_Add_m49220_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AudioClip_t1061_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49220_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::Add(T)
MethodInfo ICollection_1_Add_m49220_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8742_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8742_ICollection_1_Add_m49220_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49220_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49221_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::Clear()
MethodInfo ICollection_1_Clear_m49221_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8742_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49221_GenericMethod/* genericMethod */

};
extern Il2CppType AudioClip_t1061_0_0_0;
static ParameterInfo ICollection_1_t8742_ICollection_1_Contains_m49222_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AudioClip_t1061_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49222_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::Contains(T)
MethodInfo ICollection_1_Contains_m49222_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8742_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8742_ICollection_1_Contains_m49222_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49222_GenericMethod/* genericMethod */

};
extern Il2CppType AudioClipU5BU5D_t5724_0_0_0;
extern Il2CppType AudioClipU5BU5D_t5724_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8742_ICollection_1_CopyTo_m49223_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AudioClipU5BU5D_t5724_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49223_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49223_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8742_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8742_ICollection_1_CopyTo_m49223_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49223_GenericMethod/* genericMethod */

};
extern Il2CppType AudioClip_t1061_0_0_0;
static ParameterInfo ICollection_1_t8742_ICollection_1_Remove_m49224_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AudioClip_t1061_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49224_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>::Remove(T)
MethodInfo ICollection_1_Remove_m49224_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8742_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8742_ICollection_1_Remove_m49224_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49224_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8742_MethodInfos[] =
{
	&ICollection_1_get_Count_m49218_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49219_MethodInfo,
	&ICollection_1_Add_m49220_MethodInfo,
	&ICollection_1_Clear_m49221_MethodInfo,
	&ICollection_1_Contains_m49222_MethodInfo,
	&ICollection_1_CopyTo_m49223_MethodInfo,
	&ICollection_1_Remove_m49224_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8744_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8742_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8744_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8742_0_0_0;
extern Il2CppType ICollection_1_t8742_1_0_0;
struct ICollection_1_t8742;
extern Il2CppGenericClass ICollection_1_t8742_GenericClass;
TypeInfo ICollection_1_t8742_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8742_MethodInfos/* methods */
	, ICollection_1_t8742_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8742_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8742_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8742_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8742_0_0_0/* byval_arg */
	, &ICollection_1_t8742_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8742_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.AudioClip>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.AudioClip>
extern Il2CppType IEnumerator_1_t6891_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49225_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.AudioClip>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49225_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8744_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6891_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49225_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8744_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49225_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8744_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8744_0_0_0;
extern Il2CppType IEnumerable_1_t8744_1_0_0;
struct IEnumerable_1_t8744;
extern Il2CppGenericClass IEnumerable_1_t8744_GenericClass;
TypeInfo IEnumerable_1_t8744_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8744_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8744_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8744_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8744_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8744_0_0_0/* byval_arg */
	, &IEnumerable_1_t8744_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8744_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8743_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.AudioClip>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.AudioClip>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.AudioClip>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.AudioClip>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.AudioClip>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.AudioClip>
extern MethodInfo IList_1_get_Item_m49226_MethodInfo;
extern MethodInfo IList_1_set_Item_m49227_MethodInfo;
static PropertyInfo IList_1_t8743____Item_PropertyInfo = 
{
	&IList_1_t8743_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49226_MethodInfo/* get */
	, &IList_1_set_Item_m49227_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8743_PropertyInfos[] =
{
	&IList_1_t8743____Item_PropertyInfo,
	NULL
};
extern Il2CppType AudioClip_t1061_0_0_0;
static ParameterInfo IList_1_t8743_IList_1_IndexOf_m49228_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AudioClip_t1061_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49228_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.AudioClip>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49228_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8743_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8743_IList_1_IndexOf_m49228_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49228_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AudioClip_t1061_0_0_0;
static ParameterInfo IList_1_t8743_IList_1_Insert_m49229_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AudioClip_t1061_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49229_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.AudioClip>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49229_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8743_IList_1_Insert_m49229_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49229_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8743_IList_1_RemoveAt_m49230_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49230_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.AudioClip>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49230_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8743_IList_1_RemoveAt_m49230_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49230_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8743_IList_1_get_Item_m49226_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType AudioClip_t1061_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49226_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.AudioClip>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49226_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8743_il2cpp_TypeInfo/* declaring_type */
	, &AudioClip_t1061_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8743_IList_1_get_Item_m49226_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49226_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AudioClip_t1061_0_0_0;
static ParameterInfo IList_1_t8743_IList_1_set_Item_m49227_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AudioClip_t1061_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49227_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.AudioClip>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49227_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8743_IList_1_set_Item_m49227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49227_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8743_MethodInfos[] =
{
	&IList_1_IndexOf_m49228_MethodInfo,
	&IList_1_Insert_m49229_MethodInfo,
	&IList_1_RemoveAt_m49230_MethodInfo,
	&IList_1_get_Item_m49226_MethodInfo,
	&IList_1_set_Item_m49227_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8743_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8742_il2cpp_TypeInfo,
	&IEnumerable_1_t8744_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8743_0_0_0;
extern Il2CppType IList_1_t8743_1_0_0;
struct IList_1_t8743;
extern Il2CppGenericClass IList_1_t8743_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8743_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8743_MethodInfos/* methods */
	, IList_1_t8743_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8743_il2cpp_TypeInfo/* element_class */
	, IList_1_t8743_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8743_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8743_0_0_0/* byval_arg */
	, &IList_1_t8743_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8743_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AudioClip>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_171.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4854_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AudioClip>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_171MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_173.h"
extern TypeInfo InvokableCall_1_t4855_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_173MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m29313_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m29315_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AudioClip>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AudioClip>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AudioClip>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4854____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4854_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4854, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4854_FieldInfos[] =
{
	&CachedInvokableCall_1_t4854____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType AudioClip_t1061_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4854_CachedInvokableCall_1__ctor_m29311_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &AudioClip_t1061_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m29311_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AudioClip>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m29311_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4854_CachedInvokableCall_1__ctor_m29311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m29311_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4854_CachedInvokableCall_1_Invoke_m29312_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m29312_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AudioClip>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m29312_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4854_CachedInvokableCall_1_Invoke_m29312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m29312_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4854_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m29311_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29312_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m29312_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m29316_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4854_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29312_MethodInfo,
	&InvokableCall_1_Find_m29316_MethodInfo,
};
extern Il2CppType UnityAction_1_t4856_0_0_0;
extern TypeInfo UnityAction_1_t4856_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisAudioClip_t1061_m38645_MethodInfo;
extern TypeInfo AudioClip_t1061_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m29318_MethodInfo;
extern TypeInfo AudioClip_t1061_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4854_RGCTXData[8] = 
{
	&UnityAction_1_t4856_0_0_0/* Type Usage */,
	&UnityAction_1_t4856_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisAudioClip_t1061_m38645_MethodInfo/* Method Usage */,
	&AudioClip_t1061_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29318_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m29313_MethodInfo/* Method Usage */,
	&AudioClip_t1061_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m29315_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4854_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4854_1_0_0;
struct CachedInvokableCall_1_t4854;
extern Il2CppGenericClass CachedInvokableCall_1_t4854_GenericClass;
TypeInfo CachedInvokableCall_1_t4854_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4854_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4854_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4855_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4854_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4854_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4854_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4854_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4854_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4854_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4854_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4854)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_177.h"
extern TypeInfo UnityAction_1_t4856_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_177MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.AudioClip>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.AudioClip>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisAudioClip_t1061_m38645(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>
extern Il2CppType UnityAction_1_t4856_0_0_1;
FieldInfo InvokableCall_1_t4855____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4856_0_0_1/* type */
	, &InvokableCall_1_t4855_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4855, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4855_FieldInfos[] =
{
	&InvokableCall_1_t4855____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4855_InvokableCall_1__ctor_m29313_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29313_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m29313_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4855_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4855_InvokableCall_1__ctor_m29313_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29313_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4856_0_0_0;
static ParameterInfo InvokableCall_1_t4855_InvokableCall_1__ctor_m29314_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4856_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29314_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m29314_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4855_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4855_InvokableCall_1__ctor_m29314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29314_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4855_InvokableCall_1_Invoke_m29315_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m29315_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m29315_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4855_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4855_InvokableCall_1_Invoke_m29315_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m29315_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4855_InvokableCall_1_Find_m29316_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m29316_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m29316_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4855_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4855_InvokableCall_1_Find_m29316_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m29316_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4855_MethodInfos[] =
{
	&InvokableCall_1__ctor_m29313_MethodInfo,
	&InvokableCall_1__ctor_m29314_MethodInfo,
	&InvokableCall_1_Invoke_m29315_MethodInfo,
	&InvokableCall_1_Find_m29316_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4855_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m29315_MethodInfo,
	&InvokableCall_1_Find_m29316_MethodInfo,
};
extern TypeInfo UnityAction_1_t4856_il2cpp_TypeInfo;
extern TypeInfo AudioClip_t1061_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4855_RGCTXData[5] = 
{
	&UnityAction_1_t4856_0_0_0/* Type Usage */,
	&UnityAction_1_t4856_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisAudioClip_t1061_m38645_MethodInfo/* Method Usage */,
	&AudioClip_t1061_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29318_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4855_0_0_0;
extern Il2CppType InvokableCall_1_t4855_1_0_0;
struct InvokableCall_1_t4855;
extern Il2CppGenericClass InvokableCall_1_t4855_GenericClass;
TypeInfo InvokableCall_1_t4855_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4855_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4855_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4855_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4855_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4855_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4855_0_0_0/* byval_arg */
	, &InvokableCall_1_t4855_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4855_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4855_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4855)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4856_UnityAction_1__ctor_m29317_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m29317_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m29317_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4856_UnityAction_1__ctor_m29317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m29317_GenericMethod/* genericMethod */

};
extern Il2CppType AudioClip_t1061_0_0_0;
static ParameterInfo UnityAction_1_t4856_UnityAction_1_Invoke_m29318_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &AudioClip_t1061_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m29318_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m29318_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4856_UnityAction_1_Invoke_m29318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m29318_GenericMethod/* genericMethod */

};
extern Il2CppType AudioClip_t1061_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4856_UnityAction_1_BeginInvoke_m29319_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &AudioClip_t1061_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m29319_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m29319_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4856_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4856_UnityAction_1_BeginInvoke_m29319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m29319_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4856_UnityAction_1_EndInvoke_m29320_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m29320_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m29320_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4856_UnityAction_1_EndInvoke_m29320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m29320_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4856_MethodInfos[] =
{
	&UnityAction_1__ctor_m29317_MethodInfo,
	&UnityAction_1_Invoke_m29318_MethodInfo,
	&UnityAction_1_BeginInvoke_m29319_MethodInfo,
	&UnityAction_1_EndInvoke_m29320_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m29319_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m29320_MethodInfo;
static MethodInfo* UnityAction_1_t4856_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m29318_MethodInfo,
	&UnityAction_1_BeginInvoke_m29319_MethodInfo,
	&UnityAction_1_EndInvoke_m29320_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4856_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4856_1_0_0;
struct UnityAction_1_t4856;
extern Il2CppGenericClass UnityAction_1_t4856_GenericClass;
TypeInfo UnityAction_1_t4856_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4856_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4856_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4856_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4856_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4856_0_0_0/* byval_arg */
	, &UnityAction_1_t4856_1_0_0/* this_arg */
	, UnityAction_1_t4856_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4856_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4856)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6893_il2cpp_TypeInfo;

// UnityEngine.WebCamTexture
#include "UnityEngine_UnityEngine_WebCamTexture.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.WebCamTexture>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.WebCamTexture>
extern MethodInfo IEnumerator_1_get_Current_m49231_MethodInfo;
static PropertyInfo IEnumerator_1_t6893____Current_PropertyInfo = 
{
	&IEnumerator_1_t6893_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49231_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6893_PropertyInfos[] =
{
	&IEnumerator_1_t6893____Current_PropertyInfo,
	NULL
};
extern Il2CppType WebCamTexture_t728_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49231_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.WebCamTexture>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49231_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6893_il2cpp_TypeInfo/* declaring_type */
	, &WebCamTexture_t728_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49231_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6893_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49231_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6893_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6893_0_0_0;
extern Il2CppType IEnumerator_1_t6893_1_0_0;
struct IEnumerator_1_t6893;
extern Il2CppGenericClass IEnumerator_1_t6893_GenericClass;
TypeInfo IEnumerator_1_t6893_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6893_MethodInfos/* methods */
	, IEnumerator_1_t6893_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6893_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6893_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6893_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6893_0_0_0/* byval_arg */
	, &IEnumerator_1_t6893_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6893_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_459.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4857_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_459MethodDeclarations.h"

extern TypeInfo WebCamTexture_t728_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29325_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWebCamTexture_t728_m38647_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.WebCamTexture>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.WebCamTexture>(System.Int32)
#define Array_InternalArray__get_Item_TisWebCamTexture_t728_m38647(__this, p0, method) (WebCamTexture_t728 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4857____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4857_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4857, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4857____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4857_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4857, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4857_FieldInfos[] =
{
	&InternalEnumerator_1_t4857____array_0_FieldInfo,
	&InternalEnumerator_1_t4857____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29322_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4857____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4857_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29322_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4857____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4857_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29325_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4857_PropertyInfos[] =
{
	&InternalEnumerator_1_t4857____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4857____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4857_InternalEnumerator_1__ctor_m29321_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29321_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29321_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4857_InternalEnumerator_1__ctor_m29321_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29321_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29322_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29322_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4857_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29322_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29323_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29323_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29323_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29324_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29324_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4857_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29324_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamTexture_t728_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29325_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.WebCamTexture>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29325_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4857_il2cpp_TypeInfo/* declaring_type */
	, &WebCamTexture_t728_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29325_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4857_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29321_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29322_MethodInfo,
	&InternalEnumerator_1_Dispose_m29323_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29324_MethodInfo,
	&InternalEnumerator_1_get_Current_m29325_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29324_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29323_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4857_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29322_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29324_MethodInfo,
	&InternalEnumerator_1_Dispose_m29323_MethodInfo,
	&InternalEnumerator_1_get_Current_m29325_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4857_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6893_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4857_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6893_il2cpp_TypeInfo, 7},
};
extern TypeInfo WebCamTexture_t728_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4857_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29325_MethodInfo/* Method Usage */,
	&WebCamTexture_t728_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWebCamTexture_t728_m38647_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4857_0_0_0;
extern Il2CppType InternalEnumerator_1_t4857_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4857_GenericClass;
TypeInfo InternalEnumerator_1_t4857_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4857_MethodInfos/* methods */
	, InternalEnumerator_1_t4857_PropertyInfos/* properties */
	, InternalEnumerator_1_t4857_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4857_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4857_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4857_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4857_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4857_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4857_1_0_0/* this_arg */
	, InternalEnumerator_1_t4857_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4857_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4857_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4857)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8745_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>
extern MethodInfo ICollection_1_get_Count_m49232_MethodInfo;
static PropertyInfo ICollection_1_t8745____Count_PropertyInfo = 
{
	&ICollection_1_t8745_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49232_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49233_MethodInfo;
static PropertyInfo ICollection_1_t8745____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8745_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49233_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8745_PropertyInfos[] =
{
	&ICollection_1_t8745____Count_PropertyInfo,
	&ICollection_1_t8745____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49232_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::get_Count()
MethodInfo ICollection_1_get_Count_m49232_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8745_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49232_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49233_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49233_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8745_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49233_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamTexture_t728_0_0_0;
extern Il2CppType WebCamTexture_t728_0_0_0;
static ParameterInfo ICollection_1_t8745_ICollection_1_Add_m49234_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamTexture_t728_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49234_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::Add(T)
MethodInfo ICollection_1_Add_m49234_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8745_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8745_ICollection_1_Add_m49234_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49234_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49235_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::Clear()
MethodInfo ICollection_1_Clear_m49235_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8745_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49235_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamTexture_t728_0_0_0;
static ParameterInfo ICollection_1_t8745_ICollection_1_Contains_m49236_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamTexture_t728_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49236_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::Contains(T)
MethodInfo ICollection_1_Contains_m49236_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8745_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8745_ICollection_1_Contains_m49236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49236_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamTextureU5BU5D_t5725_0_0_0;
extern Il2CppType WebCamTextureU5BU5D_t5725_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8745_ICollection_1_CopyTo_m49237_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WebCamTextureU5BU5D_t5725_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49237_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49237_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8745_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8745_ICollection_1_CopyTo_m49237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49237_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamTexture_t728_0_0_0;
static ParameterInfo ICollection_1_t8745_ICollection_1_Remove_m49238_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamTexture_t728_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49238_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.WebCamTexture>::Remove(T)
MethodInfo ICollection_1_Remove_m49238_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8745_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8745_ICollection_1_Remove_m49238_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49238_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8745_MethodInfos[] =
{
	&ICollection_1_get_Count_m49232_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49233_MethodInfo,
	&ICollection_1_Add_m49234_MethodInfo,
	&ICollection_1_Clear_m49235_MethodInfo,
	&ICollection_1_Contains_m49236_MethodInfo,
	&ICollection_1_CopyTo_m49237_MethodInfo,
	&ICollection_1_Remove_m49238_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8747_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8745_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8747_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8745_0_0_0;
extern Il2CppType ICollection_1_t8745_1_0_0;
struct ICollection_1_t8745;
extern Il2CppGenericClass ICollection_1_t8745_GenericClass;
TypeInfo ICollection_1_t8745_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8745_MethodInfos/* methods */
	, ICollection_1_t8745_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8745_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8745_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8745_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8745_0_0_0/* byval_arg */
	, &ICollection_1_t8745_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8745_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.WebCamTexture>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.WebCamTexture>
extern Il2CppType IEnumerator_1_t6893_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49239_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.WebCamTexture>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49239_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8747_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6893_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49239_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8747_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49239_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8747_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8747_0_0_0;
extern Il2CppType IEnumerable_1_t8747_1_0_0;
struct IEnumerable_1_t8747;
extern Il2CppGenericClass IEnumerable_1_t8747_GenericClass;
TypeInfo IEnumerable_1_t8747_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8747_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8747_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8747_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8747_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8747_0_0_0/* byval_arg */
	, &IEnumerable_1_t8747_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8747_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8746_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>
extern MethodInfo IList_1_get_Item_m49240_MethodInfo;
extern MethodInfo IList_1_set_Item_m49241_MethodInfo;
static PropertyInfo IList_1_t8746____Item_PropertyInfo = 
{
	&IList_1_t8746_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49240_MethodInfo/* get */
	, &IList_1_set_Item_m49241_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8746_PropertyInfos[] =
{
	&IList_1_t8746____Item_PropertyInfo,
	NULL
};
extern Il2CppType WebCamTexture_t728_0_0_0;
static ParameterInfo IList_1_t8746_IList_1_IndexOf_m49242_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamTexture_t728_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49242_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49242_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8746_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8746_IList_1_IndexOf_m49242_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49242_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WebCamTexture_t728_0_0_0;
static ParameterInfo IList_1_t8746_IList_1_Insert_m49243_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WebCamTexture_t728_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49243_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49243_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8746_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8746_IList_1_Insert_m49243_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49243_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8746_IList_1_RemoveAt_m49244_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49244_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49244_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8746_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8746_IList_1_RemoveAt_m49244_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49244_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8746_IList_1_get_Item_m49240_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType WebCamTexture_t728_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49240_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49240_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8746_il2cpp_TypeInfo/* declaring_type */
	, &WebCamTexture_t728_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8746_IList_1_get_Item_m49240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49240_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WebCamTexture_t728_0_0_0;
static ParameterInfo IList_1_t8746_IList_1_set_Item_m49241_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WebCamTexture_t728_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49241_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.WebCamTexture>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49241_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8746_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8746_IList_1_set_Item_m49241_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49241_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8746_MethodInfos[] =
{
	&IList_1_IndexOf_m49242_MethodInfo,
	&IList_1_Insert_m49243_MethodInfo,
	&IList_1_RemoveAt_m49244_MethodInfo,
	&IList_1_get_Item_m49240_MethodInfo,
	&IList_1_set_Item_m49241_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8746_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8745_il2cpp_TypeInfo,
	&IEnumerable_1_t8747_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8746_0_0_0;
extern Il2CppType IList_1_t8746_1_0_0;
struct IList_1_t8746;
extern Il2CppGenericClass IList_1_t8746_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8746_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8746_MethodInfos/* methods */
	, IList_1_t8746_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8746_il2cpp_TypeInfo/* element_class */
	, IList_1_t8746_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8746_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8746_0_0_0/* byval_arg */
	, &IList_1_t8746_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8746_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.WebCamTexture>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_172.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4858_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.WebCamTexture>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_172MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_174.h"
extern TypeInfo InvokableCall_1_t4859_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_174MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m29328_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m29330_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.WebCamTexture>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.WebCamTexture>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.WebCamTexture>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4858____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4858_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4858, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4858_FieldInfos[] =
{
	&CachedInvokableCall_1_t4858____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType WebCamTexture_t728_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4858_CachedInvokableCall_1__ctor_m29326_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &WebCamTexture_t728_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m29326_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.WebCamTexture>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m29326_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4858_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4858_CachedInvokableCall_1__ctor_m29326_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m29326_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4858_CachedInvokableCall_1_Invoke_m29327_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m29327_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.WebCamTexture>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m29327_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4858_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4858_CachedInvokableCall_1_Invoke_m29327_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m29327_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4858_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m29326_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29327_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m29327_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m29331_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4858_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29327_MethodInfo,
	&InvokableCall_1_Find_m29331_MethodInfo,
};
extern Il2CppType UnityAction_1_t4860_0_0_0;
extern TypeInfo UnityAction_1_t4860_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisWebCamTexture_t728_m38657_MethodInfo;
extern TypeInfo WebCamTexture_t728_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m29333_MethodInfo;
extern TypeInfo WebCamTexture_t728_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4858_RGCTXData[8] = 
{
	&UnityAction_1_t4860_0_0_0/* Type Usage */,
	&UnityAction_1_t4860_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWebCamTexture_t728_m38657_MethodInfo/* Method Usage */,
	&WebCamTexture_t728_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29333_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m29328_MethodInfo/* Method Usage */,
	&WebCamTexture_t728_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m29330_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4858_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4858_1_0_0;
struct CachedInvokableCall_1_t4858;
extern Il2CppGenericClass CachedInvokableCall_1_t4858_GenericClass;
TypeInfo CachedInvokableCall_1_t4858_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4858_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4858_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4859_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4858_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4858_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4858_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4858_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4858_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4858_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4858_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4858)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_178.h"
extern TypeInfo UnityAction_1_t4860_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_178MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.WebCamTexture>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.WebCamTexture>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisWebCamTexture_t728_m38657(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>
extern Il2CppType UnityAction_1_t4860_0_0_1;
FieldInfo InvokableCall_1_t4859____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4860_0_0_1/* type */
	, &InvokableCall_1_t4859_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4859, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4859_FieldInfos[] =
{
	&InvokableCall_1_t4859____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4859_InvokableCall_1__ctor_m29328_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29328_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m29328_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4859_InvokableCall_1__ctor_m29328_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29328_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4860_0_0_0;
static ParameterInfo InvokableCall_1_t4859_InvokableCall_1__ctor_m29329_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4860_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29329_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m29329_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4859_InvokableCall_1__ctor_m29329_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29329_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4859_InvokableCall_1_Invoke_m29330_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m29330_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m29330_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4859_InvokableCall_1_Invoke_m29330_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m29330_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4859_InvokableCall_1_Find_m29331_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m29331_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m29331_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4859_InvokableCall_1_Find_m29331_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m29331_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4859_MethodInfos[] =
{
	&InvokableCall_1__ctor_m29328_MethodInfo,
	&InvokableCall_1__ctor_m29329_MethodInfo,
	&InvokableCall_1_Invoke_m29330_MethodInfo,
	&InvokableCall_1_Find_m29331_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4859_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m29330_MethodInfo,
	&InvokableCall_1_Find_m29331_MethodInfo,
};
extern TypeInfo UnityAction_1_t4860_il2cpp_TypeInfo;
extern TypeInfo WebCamTexture_t728_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4859_RGCTXData[5] = 
{
	&UnityAction_1_t4860_0_0_0/* Type Usage */,
	&UnityAction_1_t4860_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWebCamTexture_t728_m38657_MethodInfo/* Method Usage */,
	&WebCamTexture_t728_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29333_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4859_0_0_0;
extern Il2CppType InvokableCall_1_t4859_1_0_0;
struct InvokableCall_1_t4859;
extern Il2CppGenericClass InvokableCall_1_t4859_GenericClass;
TypeInfo InvokableCall_1_t4859_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4859_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4859_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4859_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4859_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4859_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4859_0_0_0/* byval_arg */
	, &InvokableCall_1_t4859_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4859_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4859_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4859)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4860_UnityAction_1__ctor_m29332_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m29332_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m29332_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4860_UnityAction_1__ctor_m29332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m29332_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamTexture_t728_0_0_0;
static ParameterInfo UnityAction_1_t4860_UnityAction_1_Invoke_m29333_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WebCamTexture_t728_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m29333_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m29333_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4860_UnityAction_1_Invoke_m29333_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m29333_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamTexture_t728_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4860_UnityAction_1_BeginInvoke_m29334_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WebCamTexture_t728_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m29334_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m29334_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4860_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4860_UnityAction_1_BeginInvoke_m29334_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m29334_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4860_UnityAction_1_EndInvoke_m29335_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m29335_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m29335_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4860_UnityAction_1_EndInvoke_m29335_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m29335_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4860_MethodInfos[] =
{
	&UnityAction_1__ctor_m29332_MethodInfo,
	&UnityAction_1_Invoke_m29333_MethodInfo,
	&UnityAction_1_BeginInvoke_m29334_MethodInfo,
	&UnityAction_1_EndInvoke_m29335_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m29334_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m29335_MethodInfo;
static MethodInfo* UnityAction_1_t4860_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m29333_MethodInfo,
	&UnityAction_1_BeginInvoke_m29334_MethodInfo,
	&UnityAction_1_EndInvoke_m29335_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4860_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4860_1_0_0;
struct UnityAction_1_t4860;
extern Il2CppGenericClass UnityAction_1_t4860_GenericClass;
TypeInfo UnityAction_1_t4860_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4860_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4860_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4860_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4860_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4860_0_0_0/* byval_arg */
	, &UnityAction_1_t4860_1_0_0/* this_arg */
	, UnityAction_1_t4860_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4860_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4860)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6895_il2cpp_TypeInfo;

// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSource.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.AnimationEventSource>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.AnimationEventSource>
extern MethodInfo IEnumerator_1_get_Current_m49245_MethodInfo;
static PropertyInfo IEnumerator_1_t6895____Current_PropertyInfo = 
{
	&IEnumerator_1_t6895_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49245_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6895_PropertyInfos[] =
{
	&IEnumerator_1_t6895____Current_PropertyInfo,
	NULL
};
extern Il2CppType AnimationEventSource_t1062_0_0_0;
extern void* RuntimeInvoker_AnimationEventSource_t1062 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49245_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.AnimationEventSource>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49245_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6895_il2cpp_TypeInfo/* declaring_type */
	, &AnimationEventSource_t1062_0_0_0/* return_type */
	, RuntimeInvoker_AnimationEventSource_t1062/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49245_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6895_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49245_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6895_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6895_0_0_0;
extern Il2CppType IEnumerator_1_t6895_1_0_0;
struct IEnumerator_1_t6895;
extern Il2CppGenericClass IEnumerator_1_t6895_GenericClass;
TypeInfo IEnumerator_1_t6895_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6895_MethodInfos/* methods */
	, IEnumerator_1_t6895_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6895_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6895_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6895_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6895_0_0_0/* byval_arg */
	, &IEnumerator_1_t6895_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6895_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_460.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4861_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_460MethodDeclarations.h"

extern TypeInfo AnimationEventSource_t1062_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29340_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAnimationEventSource_t1062_m38659_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.AnimationEventSource>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.AnimationEventSource>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisAnimationEventSource_t1062_m38659 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29336_MethodInfo;
 void InternalEnumerator_1__ctor_m29336 (InternalEnumerator_1_t4861 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29337_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29337 (InternalEnumerator_1_t4861 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29340(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29340_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&AnimationEventSource_t1062_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29338_MethodInfo;
 void InternalEnumerator_1_Dispose_m29338 (InternalEnumerator_1_t4861 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29339_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29339 (InternalEnumerator_1_t4861 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29340 (InternalEnumerator_1_t4861 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisAnimationEventSource_t1062_m38659(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAnimationEventSource_t1062_m38659_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4861____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4861_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4861, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4861____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4861_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4861, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4861_FieldInfos[] =
{
	&InternalEnumerator_1_t4861____array_0_FieldInfo,
	&InternalEnumerator_1_t4861____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4861____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4861_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29337_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4861____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4861_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29340_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4861_PropertyInfos[] =
{
	&InternalEnumerator_1_t4861____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4861____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4861_InternalEnumerator_1__ctor_m29336_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29336_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29336_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29336/* method */
	, &InternalEnumerator_1_t4861_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4861_InternalEnumerator_1__ctor_m29336_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29336_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29337_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29337_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29337/* method */
	, &InternalEnumerator_1_t4861_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29337_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29338_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29338_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29338/* method */
	, &InternalEnumerator_1_t4861_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29338_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29339_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29339_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29339/* method */
	, &InternalEnumerator_1_t4861_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29339_GenericMethod/* genericMethod */

};
extern Il2CppType AnimationEventSource_t1062_0_0_0;
extern void* RuntimeInvoker_AnimationEventSource_t1062 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29340_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.AnimationEventSource>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29340_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29340/* method */
	, &InternalEnumerator_1_t4861_il2cpp_TypeInfo/* declaring_type */
	, &AnimationEventSource_t1062_0_0_0/* return_type */
	, RuntimeInvoker_AnimationEventSource_t1062/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29340_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4861_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29336_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29337_MethodInfo,
	&InternalEnumerator_1_Dispose_m29338_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29339_MethodInfo,
	&InternalEnumerator_1_get_Current_m29340_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4861_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29337_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29339_MethodInfo,
	&InternalEnumerator_1_Dispose_m29338_MethodInfo,
	&InternalEnumerator_1_get_Current_m29340_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4861_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6895_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4861_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6895_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4861_0_0_0;
extern Il2CppType InternalEnumerator_1_t4861_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4861_GenericClass;
TypeInfo InternalEnumerator_1_t4861_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4861_MethodInfos/* methods */
	, InternalEnumerator_1_t4861_PropertyInfos/* properties */
	, InternalEnumerator_1_t4861_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4861_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4861_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4861_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4861_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4861_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4861_1_0_0/* this_arg */
	, InternalEnumerator_1_t4861_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4861_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4861)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8748_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>
extern MethodInfo ICollection_1_get_Count_m49246_MethodInfo;
static PropertyInfo ICollection_1_t8748____Count_PropertyInfo = 
{
	&ICollection_1_t8748_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49246_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49247_MethodInfo;
static PropertyInfo ICollection_1_t8748____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8748_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49247_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8748_PropertyInfos[] =
{
	&ICollection_1_t8748____Count_PropertyInfo,
	&ICollection_1_t8748____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49246_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::get_Count()
MethodInfo ICollection_1_get_Count_m49246_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8748_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49246_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49247_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49247_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8748_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49247_GenericMethod/* genericMethod */

};
extern Il2CppType AnimationEventSource_t1062_0_0_0;
extern Il2CppType AnimationEventSource_t1062_0_0_0;
static ParameterInfo ICollection_1_t8748_ICollection_1_Add_m49248_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AnimationEventSource_t1062_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49248_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::Add(T)
MethodInfo ICollection_1_Add_m49248_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8748_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8748_ICollection_1_Add_m49248_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49248_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49249_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::Clear()
MethodInfo ICollection_1_Clear_m49249_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8748_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49249_GenericMethod/* genericMethod */

};
extern Il2CppType AnimationEventSource_t1062_0_0_0;
static ParameterInfo ICollection_1_t8748_ICollection_1_Contains_m49250_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AnimationEventSource_t1062_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49250_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::Contains(T)
MethodInfo ICollection_1_Contains_m49250_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8748_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8748_ICollection_1_Contains_m49250_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49250_GenericMethod/* genericMethod */

};
extern Il2CppType AnimationEventSourceU5BU5D_t5726_0_0_0;
extern Il2CppType AnimationEventSourceU5BU5D_t5726_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8748_ICollection_1_CopyTo_m49251_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AnimationEventSourceU5BU5D_t5726_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49251_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49251_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8748_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8748_ICollection_1_CopyTo_m49251_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49251_GenericMethod/* genericMethod */

};
extern Il2CppType AnimationEventSource_t1062_0_0_0;
static ParameterInfo ICollection_1_t8748_ICollection_1_Remove_m49252_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AnimationEventSource_t1062_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49252_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.AnimationEventSource>::Remove(T)
MethodInfo ICollection_1_Remove_m49252_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8748_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8748_ICollection_1_Remove_m49252_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49252_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8748_MethodInfos[] =
{
	&ICollection_1_get_Count_m49246_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49247_MethodInfo,
	&ICollection_1_Add_m49248_MethodInfo,
	&ICollection_1_Clear_m49249_MethodInfo,
	&ICollection_1_Contains_m49250_MethodInfo,
	&ICollection_1_CopyTo_m49251_MethodInfo,
	&ICollection_1_Remove_m49252_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8750_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8748_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8750_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8748_0_0_0;
extern Il2CppType ICollection_1_t8748_1_0_0;
struct ICollection_1_t8748;
extern Il2CppGenericClass ICollection_1_t8748_GenericClass;
TypeInfo ICollection_1_t8748_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8748_MethodInfos/* methods */
	, ICollection_1_t8748_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8748_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8748_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8748_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8748_0_0_0/* byval_arg */
	, &ICollection_1_t8748_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8748_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.AnimationEventSource>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.AnimationEventSource>
extern Il2CppType IEnumerator_1_t6895_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49253_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.AnimationEventSource>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49253_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8750_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6895_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49253_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8750_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49253_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8750_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8750_0_0_0;
extern Il2CppType IEnumerable_1_t8750_1_0_0;
struct IEnumerable_1_t8750;
extern Il2CppGenericClass IEnumerable_1_t8750_GenericClass;
TypeInfo IEnumerable_1_t8750_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8750_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8750_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8750_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8750_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8750_0_0_0/* byval_arg */
	, &IEnumerable_1_t8750_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8750_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8749_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>
extern MethodInfo IList_1_get_Item_m49254_MethodInfo;
extern MethodInfo IList_1_set_Item_m49255_MethodInfo;
static PropertyInfo IList_1_t8749____Item_PropertyInfo = 
{
	&IList_1_t8749_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49254_MethodInfo/* get */
	, &IList_1_set_Item_m49255_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8749_PropertyInfos[] =
{
	&IList_1_t8749____Item_PropertyInfo,
	NULL
};
extern Il2CppType AnimationEventSource_t1062_0_0_0;
static ParameterInfo IList_1_t8749_IList_1_IndexOf_m49256_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AnimationEventSource_t1062_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49256_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49256_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8749_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8749_IList_1_IndexOf_m49256_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49256_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AnimationEventSource_t1062_0_0_0;
static ParameterInfo IList_1_t8749_IList_1_Insert_m49257_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AnimationEventSource_t1062_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49257_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49257_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8749_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8749_IList_1_Insert_m49257_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49257_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8749_IList_1_RemoveAt_m49258_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49258_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49258_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8749_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8749_IList_1_RemoveAt_m49258_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49258_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8749_IList_1_get_Item_m49254_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType AnimationEventSource_t1062_0_0_0;
extern void* RuntimeInvoker_AnimationEventSource_t1062_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49254_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49254_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8749_il2cpp_TypeInfo/* declaring_type */
	, &AnimationEventSource_t1062_0_0_0/* return_type */
	, RuntimeInvoker_AnimationEventSource_t1062_Int32_t123/* invoker_method */
	, IList_1_t8749_IList_1_get_Item_m49254_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49254_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType AnimationEventSource_t1062_0_0_0;
static ParameterInfo IList_1_t8749_IList_1_set_Item_m49255_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AnimationEventSource_t1062_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49255_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.AnimationEventSource>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49255_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8749_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8749_IList_1_set_Item_m49255_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49255_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8749_MethodInfos[] =
{
	&IList_1_IndexOf_m49256_MethodInfo,
	&IList_1_Insert_m49257_MethodInfo,
	&IList_1_RemoveAt_m49258_MethodInfo,
	&IList_1_get_Item_m49254_MethodInfo,
	&IList_1_set_Item_m49255_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8749_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8748_il2cpp_TypeInfo,
	&IEnumerable_1_t8750_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8749_0_0_0;
extern Il2CppType IList_1_t8749_1_0_0;
struct IList_1_t8749;
extern Il2CppGenericClass IList_1_t8749_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8749_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8749_MethodInfos/* methods */
	, IList_1_t8749_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8749_il2cpp_TypeInfo/* element_class */
	, IList_1_t8749_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8749_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8749_0_0_0/* byval_arg */
	, &IList_1_t8749_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8749_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6897_il2cpp_TypeInfo;

// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Keyframe>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Keyframe>
extern MethodInfo IEnumerator_1_get_Current_m49259_MethodInfo;
static PropertyInfo IEnumerator_1_t6897____Current_PropertyInfo = 
{
	&IEnumerator_1_t6897_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49259_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6897_PropertyInfos[] =
{
	&IEnumerator_1_t6897____Current_PropertyInfo,
	NULL
};
extern Il2CppType Keyframe_t1066_0_0_0;
extern void* RuntimeInvoker_Keyframe_t1066 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49259_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Keyframe>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49259_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6897_il2cpp_TypeInfo/* declaring_type */
	, &Keyframe_t1066_0_0_0/* return_type */
	, RuntimeInvoker_Keyframe_t1066/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49259_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6897_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49259_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6897_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6897_0_0_0;
extern Il2CppType IEnumerator_1_t6897_1_0_0;
struct IEnumerator_1_t6897;
extern Il2CppGenericClass IEnumerator_1_t6897_GenericClass;
TypeInfo IEnumerator_1_t6897_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6897_MethodInfos/* methods */
	, IEnumerator_1_t6897_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6897_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6897_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6897_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6897_0_0_0/* byval_arg */
	, &IEnumerator_1_t6897_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6897_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Keyframe>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_461.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4862_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Keyframe>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_461MethodDeclarations.h"

extern TypeInfo Keyframe_t1066_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29345_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyframe_t1066_m38670_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
 Keyframe_t1066  Array_InternalArray__get_Item_TisKeyframe_t1066_m38670 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29341_MethodInfo;
 void InternalEnumerator_1__ctor_m29341 (InternalEnumerator_1_t4862 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29342_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29342 (InternalEnumerator_1_t4862 * __this, MethodInfo* method){
	{
		Keyframe_t1066  L_0 = InternalEnumerator_1_get_Current_m29345(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29345_MethodInfo);
		Keyframe_t1066  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Keyframe_t1066_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29343_MethodInfo;
 void InternalEnumerator_1_Dispose_m29343 (InternalEnumerator_1_t4862 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29344_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29344 (InternalEnumerator_1_t4862 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
 Keyframe_t1066  InternalEnumerator_1_get_Current_m29345 (InternalEnumerator_1_t4862 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		Keyframe_t1066  L_8 = Array_InternalArray__get_Item_TisKeyframe_t1066_m38670(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisKeyframe_t1066_m38670_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Keyframe>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4862____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4862_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4862, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4862____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4862_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4862, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4862_FieldInfos[] =
{
	&InternalEnumerator_1_t4862____array_0_FieldInfo,
	&InternalEnumerator_1_t4862____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4862____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4862_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29342_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4862____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4862_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29345_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4862_PropertyInfos[] =
{
	&InternalEnumerator_1_t4862____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4862____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4862_InternalEnumerator_1__ctor_m29341_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29341_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29341_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29341/* method */
	, &InternalEnumerator_1_t4862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4862_InternalEnumerator_1__ctor_m29341_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29341_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29342_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29342_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29342/* method */
	, &InternalEnumerator_1_t4862_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29342_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29343_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29343_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29343/* method */
	, &InternalEnumerator_1_t4862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29343_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29344_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29344_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29344/* method */
	, &InternalEnumerator_1_t4862_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29344_GenericMethod/* genericMethod */

};
extern Il2CppType Keyframe_t1066_0_0_0;
extern void* RuntimeInvoker_Keyframe_t1066 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29345_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29345_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29345/* method */
	, &InternalEnumerator_1_t4862_il2cpp_TypeInfo/* declaring_type */
	, &Keyframe_t1066_0_0_0/* return_type */
	, RuntimeInvoker_Keyframe_t1066/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29345_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4862_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29341_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29342_MethodInfo,
	&InternalEnumerator_1_Dispose_m29343_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29344_MethodInfo,
	&InternalEnumerator_1_get_Current_m29345_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4862_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29342_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29344_MethodInfo,
	&InternalEnumerator_1_Dispose_m29343_MethodInfo,
	&InternalEnumerator_1_get_Current_m29345_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4862_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6897_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4862_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6897_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4862_0_0_0;
extern Il2CppType InternalEnumerator_1_t4862_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4862_GenericClass;
TypeInfo InternalEnumerator_1_t4862_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4862_MethodInfos/* methods */
	, InternalEnumerator_1_t4862_PropertyInfos/* properties */
	, InternalEnumerator_1_t4862_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4862_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4862_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4862_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4862_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4862_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4862_1_0_0/* this_arg */
	, InternalEnumerator_1_t4862_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4862_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4862)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8751_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>
extern MethodInfo ICollection_1_get_Count_m49260_MethodInfo;
static PropertyInfo ICollection_1_t8751____Count_PropertyInfo = 
{
	&ICollection_1_t8751_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49260_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49261_MethodInfo;
static PropertyInfo ICollection_1_t8751____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8751_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49261_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8751_PropertyInfos[] =
{
	&ICollection_1_t8751____Count_PropertyInfo,
	&ICollection_1_t8751____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49260_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::get_Count()
MethodInfo ICollection_1_get_Count_m49260_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8751_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49260_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49261_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49261_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8751_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49261_GenericMethod/* genericMethod */

};
extern Il2CppType Keyframe_t1066_0_0_0;
extern Il2CppType Keyframe_t1066_0_0_0;
static ParameterInfo ICollection_1_t8751_ICollection_1_Add_m49262_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Keyframe_t1066_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Keyframe_t1066 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49262_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Add(T)
MethodInfo ICollection_1_Add_m49262_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Keyframe_t1066/* invoker_method */
	, ICollection_1_t8751_ICollection_1_Add_m49262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49262_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49263_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Clear()
MethodInfo ICollection_1_Clear_m49263_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49263_GenericMethod/* genericMethod */

};
extern Il2CppType Keyframe_t1066_0_0_0;
static ParameterInfo ICollection_1_t8751_ICollection_1_Contains_m49264_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Keyframe_t1066_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Keyframe_t1066 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49264_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Contains(T)
MethodInfo ICollection_1_Contains_m49264_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8751_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Keyframe_t1066/* invoker_method */
	, ICollection_1_t8751_ICollection_1_Contains_m49264_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49264_GenericMethod/* genericMethod */

};
extern Il2CppType KeyframeU5BU5D_t1068_0_0_0;
extern Il2CppType KeyframeU5BU5D_t1068_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8751_ICollection_1_CopyTo_m49265_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &KeyframeU5BU5D_t1068_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49265_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49265_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8751_ICollection_1_CopyTo_m49265_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49265_GenericMethod/* genericMethod */

};
extern Il2CppType Keyframe_t1066_0_0_0;
static ParameterInfo ICollection_1_t8751_ICollection_1_Remove_m49266_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Keyframe_t1066_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Keyframe_t1066 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49266_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Remove(T)
MethodInfo ICollection_1_Remove_m49266_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8751_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Keyframe_t1066/* invoker_method */
	, ICollection_1_t8751_ICollection_1_Remove_m49266_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49266_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8751_MethodInfos[] =
{
	&ICollection_1_get_Count_m49260_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49261_MethodInfo,
	&ICollection_1_Add_m49262_MethodInfo,
	&ICollection_1_Clear_m49263_MethodInfo,
	&ICollection_1_Contains_m49264_MethodInfo,
	&ICollection_1_CopyTo_m49265_MethodInfo,
	&ICollection_1_Remove_m49266_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8753_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8751_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8753_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8751_0_0_0;
extern Il2CppType ICollection_1_t8751_1_0_0;
struct ICollection_1_t8751;
extern Il2CppGenericClass ICollection_1_t8751_GenericClass;
TypeInfo ICollection_1_t8751_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8751_MethodInfos/* methods */
	, ICollection_1_t8751_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8751_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8751_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8751_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8751_0_0_0/* byval_arg */
	, &ICollection_1_t8751_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8751_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Keyframe>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Keyframe>
extern Il2CppType IEnumerator_1_t6897_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49267_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Keyframe>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49267_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8753_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6897_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49267_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8753_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49267_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8753_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8753_0_0_0;
extern Il2CppType IEnumerable_1_t8753_1_0_0;
struct IEnumerable_1_t8753;
extern Il2CppGenericClass IEnumerable_1_t8753_GenericClass;
TypeInfo IEnumerable_1_t8753_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8753_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8753_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8753_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8753_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8753_0_0_0/* byval_arg */
	, &IEnumerable_1_t8753_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8753_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8752_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Keyframe>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Keyframe>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Keyframe>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Keyframe>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Keyframe>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Keyframe>
extern MethodInfo IList_1_get_Item_m49268_MethodInfo;
extern MethodInfo IList_1_set_Item_m49269_MethodInfo;
static PropertyInfo IList_1_t8752____Item_PropertyInfo = 
{
	&IList_1_t8752_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49268_MethodInfo/* get */
	, &IList_1_set_Item_m49269_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8752_PropertyInfos[] =
{
	&IList_1_t8752____Item_PropertyInfo,
	NULL
};
extern Il2CppType Keyframe_t1066_0_0_0;
static ParameterInfo IList_1_t8752_IList_1_IndexOf_m49270_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Keyframe_t1066_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Keyframe_t1066 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49270_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Keyframe>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49270_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8752_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Keyframe_t1066/* invoker_method */
	, IList_1_t8752_IList_1_IndexOf_m49270_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49270_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Keyframe_t1066_0_0_0;
static ParameterInfo IList_1_t8752_IList_1_Insert_m49271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Keyframe_t1066_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Keyframe_t1066 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49271_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Keyframe>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49271_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Keyframe_t1066/* invoker_method */
	, IList_1_t8752_IList_1_Insert_m49271_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49271_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8752_IList_1_RemoveAt_m49272_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49272_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Keyframe>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49272_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8752_IList_1_RemoveAt_m49272_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49272_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8752_IList_1_get_Item_m49268_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Keyframe_t1066_0_0_0;
extern void* RuntimeInvoker_Keyframe_t1066_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49268_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Keyframe>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49268_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8752_il2cpp_TypeInfo/* declaring_type */
	, &Keyframe_t1066_0_0_0/* return_type */
	, RuntimeInvoker_Keyframe_t1066_Int32_t123/* invoker_method */
	, IList_1_t8752_IList_1_get_Item_m49268_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49268_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Keyframe_t1066_0_0_0;
static ParameterInfo IList_1_t8752_IList_1_set_Item_m49269_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Keyframe_t1066_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Keyframe_t1066 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49269_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Keyframe>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49269_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Keyframe_t1066/* invoker_method */
	, IList_1_t8752_IList_1_set_Item_m49269_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49269_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8752_MethodInfos[] =
{
	&IList_1_IndexOf_m49270_MethodInfo,
	&IList_1_Insert_m49271_MethodInfo,
	&IList_1_RemoveAt_m49272_MethodInfo,
	&IList_1_get_Item_m49268_MethodInfo,
	&IList_1_set_Item_m49269_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8752_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8751_il2cpp_TypeInfo,
	&IEnumerable_1_t8753_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8752_0_0_0;
extern Il2CppType IList_1_t8752_1_0_0;
struct IList_1_t8752;
extern Il2CppGenericClass IList_1_t8752_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8752_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8752_MethodInfos/* methods */
	, IList_1_t8752_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8752_il2cpp_TypeInfo/* element_class */
	, IList_1_t8752_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8752_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8752_0_0_0/* byval_arg */
	, &IList_1_t8752_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8752_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6899_il2cpp_TypeInfo;

// UnityEngine.PlayMode
#include "UnityEngine_UnityEngine_PlayMode.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.PlayMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.PlayMode>
extern MethodInfo IEnumerator_1_get_Current_m49273_MethodInfo;
static PropertyInfo IEnumerator_1_t6899____Current_PropertyInfo = 
{
	&IEnumerator_1_t6899_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49273_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6899_PropertyInfos[] =
{
	&IEnumerator_1_t6899____Current_PropertyInfo,
	NULL
};
extern Il2CppType PlayMode_t1069_0_0_0;
extern void* RuntimeInvoker_PlayMode_t1069 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49273_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.PlayMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49273_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6899_il2cpp_TypeInfo/* declaring_type */
	, &PlayMode_t1069_0_0_0/* return_type */
	, RuntimeInvoker_PlayMode_t1069/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49273_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6899_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49273_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6899_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6899_0_0_0;
extern Il2CppType IEnumerator_1_t6899_1_0_0;
struct IEnumerator_1_t6899;
extern Il2CppGenericClass IEnumerator_1_t6899_GenericClass;
TypeInfo IEnumerator_1_t6899_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6899_MethodInfos/* methods */
	, IEnumerator_1_t6899_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6899_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6899_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6899_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6899_0_0_0/* byval_arg */
	, &IEnumerator_1_t6899_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6899_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.PlayMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_462.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4863_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.PlayMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_462MethodDeclarations.h"

extern TypeInfo PlayMode_t1069_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29350_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPlayMode_t1069_m38681_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.PlayMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.PlayMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisPlayMode_t1069_m38681 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.PlayMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29346_MethodInfo;
 void InternalEnumerator_1__ctor_m29346 (InternalEnumerator_1_t4863 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.PlayMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29347_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29347 (InternalEnumerator_1_t4863 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29350(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29350_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&PlayMode_t1069_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.PlayMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29348_MethodInfo;
 void InternalEnumerator_1_Dispose_m29348 (InternalEnumerator_1_t4863 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.PlayMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29349_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29349 (InternalEnumerator_1_t4863 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.PlayMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29350 (InternalEnumerator_1_t4863 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisPlayMode_t1069_m38681(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisPlayMode_t1069_m38681_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.PlayMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4863____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4863_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4863, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4863____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4863_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4863, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4863_FieldInfos[] =
{
	&InternalEnumerator_1_t4863____array_0_FieldInfo,
	&InternalEnumerator_1_t4863____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4863____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4863_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29347_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4863____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4863_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29350_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4863_PropertyInfos[] =
{
	&InternalEnumerator_1_t4863____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4863____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4863_InternalEnumerator_1__ctor_m29346_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29346_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.PlayMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29346_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29346/* method */
	, &InternalEnumerator_1_t4863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4863_InternalEnumerator_1__ctor_m29346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29346_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29347_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.PlayMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29347_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29347/* method */
	, &InternalEnumerator_1_t4863_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29347_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29348_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.PlayMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29348_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29348/* method */
	, &InternalEnumerator_1_t4863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29348_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29349_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.PlayMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29349_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29349/* method */
	, &InternalEnumerator_1_t4863_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29349_GenericMethod/* genericMethod */

};
extern Il2CppType PlayMode_t1069_0_0_0;
extern void* RuntimeInvoker_PlayMode_t1069 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29350_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.PlayMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29350_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29350/* method */
	, &InternalEnumerator_1_t4863_il2cpp_TypeInfo/* declaring_type */
	, &PlayMode_t1069_0_0_0/* return_type */
	, RuntimeInvoker_PlayMode_t1069/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29350_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4863_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29346_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29347_MethodInfo,
	&InternalEnumerator_1_Dispose_m29348_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29349_MethodInfo,
	&InternalEnumerator_1_get_Current_m29350_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4863_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29347_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29349_MethodInfo,
	&InternalEnumerator_1_Dispose_m29348_MethodInfo,
	&InternalEnumerator_1_get_Current_m29350_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4863_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6899_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4863_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6899_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4863_0_0_0;
extern Il2CppType InternalEnumerator_1_t4863_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4863_GenericClass;
TypeInfo InternalEnumerator_1_t4863_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4863_MethodInfos/* methods */
	, InternalEnumerator_1_t4863_PropertyInfos/* properties */
	, InternalEnumerator_1_t4863_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4863_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4863_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4863_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4863_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4863_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4863_1_0_0/* this_arg */
	, InternalEnumerator_1_t4863_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4863_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4863)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8754_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>
extern MethodInfo ICollection_1_get_Count_m49274_MethodInfo;
static PropertyInfo ICollection_1_t8754____Count_PropertyInfo = 
{
	&ICollection_1_t8754_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49274_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49275_MethodInfo;
static PropertyInfo ICollection_1_t8754____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8754_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49275_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8754_PropertyInfos[] =
{
	&ICollection_1_t8754____Count_PropertyInfo,
	&ICollection_1_t8754____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49274_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::get_Count()
MethodInfo ICollection_1_get_Count_m49274_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8754_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49274_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49275_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49275_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8754_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49275_GenericMethod/* genericMethod */

};
extern Il2CppType PlayMode_t1069_0_0_0;
extern Il2CppType PlayMode_t1069_0_0_0;
static ParameterInfo ICollection_1_t8754_ICollection_1_Add_m49276_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlayMode_t1069_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49276_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::Add(T)
MethodInfo ICollection_1_Add_m49276_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8754_ICollection_1_Add_m49276_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49276_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49277_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::Clear()
MethodInfo ICollection_1_Clear_m49277_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49277_GenericMethod/* genericMethod */

};
extern Il2CppType PlayMode_t1069_0_0_0;
static ParameterInfo ICollection_1_t8754_ICollection_1_Contains_m49278_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlayMode_t1069_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49278_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::Contains(T)
MethodInfo ICollection_1_Contains_m49278_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8754_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8754_ICollection_1_Contains_m49278_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49278_GenericMethod/* genericMethod */

};
extern Il2CppType PlayModeU5BU5D_t5727_0_0_0;
extern Il2CppType PlayModeU5BU5D_t5727_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8754_ICollection_1_CopyTo_m49279_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PlayModeU5BU5D_t5727_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49279_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49279_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8754_ICollection_1_CopyTo_m49279_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49279_GenericMethod/* genericMethod */

};
extern Il2CppType PlayMode_t1069_0_0_0;
static ParameterInfo ICollection_1_t8754_ICollection_1_Remove_m49280_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlayMode_t1069_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49280_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.PlayMode>::Remove(T)
MethodInfo ICollection_1_Remove_m49280_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8754_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8754_ICollection_1_Remove_m49280_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49280_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8754_MethodInfos[] =
{
	&ICollection_1_get_Count_m49274_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49275_MethodInfo,
	&ICollection_1_Add_m49276_MethodInfo,
	&ICollection_1_Clear_m49277_MethodInfo,
	&ICollection_1_Contains_m49278_MethodInfo,
	&ICollection_1_CopyTo_m49279_MethodInfo,
	&ICollection_1_Remove_m49280_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8756_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8754_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8756_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8754_0_0_0;
extern Il2CppType ICollection_1_t8754_1_0_0;
struct ICollection_1_t8754;
extern Il2CppGenericClass ICollection_1_t8754_GenericClass;
TypeInfo ICollection_1_t8754_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8754_MethodInfos/* methods */
	, ICollection_1_t8754_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8754_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8754_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8754_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8754_0_0_0/* byval_arg */
	, &ICollection_1_t8754_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8754_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.PlayMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.PlayMode>
extern Il2CppType IEnumerator_1_t6899_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49281_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.PlayMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49281_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8756_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6899_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49281_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8756_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49281_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8756_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8756_0_0_0;
extern Il2CppType IEnumerable_1_t8756_1_0_0;
struct IEnumerable_1_t8756;
extern Il2CppGenericClass IEnumerable_1_t8756_GenericClass;
TypeInfo IEnumerable_1_t8756_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8756_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8756_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8756_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8756_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8756_0_0_0/* byval_arg */
	, &IEnumerable_1_t8756_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8756_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8755_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.PlayMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.PlayMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.PlayMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.PlayMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.PlayMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.PlayMode>
extern MethodInfo IList_1_get_Item_m49282_MethodInfo;
extern MethodInfo IList_1_set_Item_m49283_MethodInfo;
static PropertyInfo IList_1_t8755____Item_PropertyInfo = 
{
	&IList_1_t8755_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49282_MethodInfo/* get */
	, &IList_1_set_Item_m49283_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8755_PropertyInfos[] =
{
	&IList_1_t8755____Item_PropertyInfo,
	NULL
};
extern Il2CppType PlayMode_t1069_0_0_0;
static ParameterInfo IList_1_t8755_IList_1_IndexOf_m49284_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlayMode_t1069_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49284_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.PlayMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49284_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8755_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8755_IList_1_IndexOf_m49284_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49284_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PlayMode_t1069_0_0_0;
static ParameterInfo IList_1_t8755_IList_1_Insert_m49285_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PlayMode_t1069_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49285_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.PlayMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49285_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8755_IList_1_Insert_m49285_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49285_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8755_IList_1_RemoveAt_m49286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49286_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.PlayMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49286_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8755_IList_1_RemoveAt_m49286_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49286_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8755_IList_1_get_Item_m49282_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType PlayMode_t1069_0_0_0;
extern void* RuntimeInvoker_PlayMode_t1069_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49282_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.PlayMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49282_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8755_il2cpp_TypeInfo/* declaring_type */
	, &PlayMode_t1069_0_0_0/* return_type */
	, RuntimeInvoker_PlayMode_t1069_Int32_t123/* invoker_method */
	, IList_1_t8755_IList_1_get_Item_m49282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49282_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PlayMode_t1069_0_0_0;
static ParameterInfo IList_1_t8755_IList_1_set_Item_m49283_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PlayMode_t1069_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49283_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.PlayMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49283_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8755_IList_1_set_Item_m49283_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49283_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8755_MethodInfos[] =
{
	&IList_1_IndexOf_m49284_MethodInfo,
	&IList_1_Insert_m49285_MethodInfo,
	&IList_1_RemoveAt_m49286_MethodInfo,
	&IList_1_get_Item_m49282_MethodInfo,
	&IList_1_set_Item_m49283_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8755_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8754_il2cpp_TypeInfo,
	&IEnumerable_1_t8756_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8755_0_0_0;
extern Il2CppType IList_1_t8755_1_0_0;
struct IList_1_t8755;
extern Il2CppGenericClass IList_1_t8755_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8755_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8755_MethodInfos/* methods */
	, IList_1_t8755_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8755_il2cpp_TypeInfo/* element_class */
	, IList_1_t8755_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8755_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8755_0_0_0/* byval_arg */
	, &IList_1_t8755_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8755_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6901_il2cpp_TypeInfo;

// UnityEngine.QueueMode
#include "UnityEngine_UnityEngine_QueueMode.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.QueueMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.QueueMode>
extern MethodInfo IEnumerator_1_get_Current_m49287_MethodInfo;
static PropertyInfo IEnumerator_1_t6901____Current_PropertyInfo = 
{
	&IEnumerator_1_t6901_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49287_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6901_PropertyInfos[] =
{
	&IEnumerator_1_t6901____Current_PropertyInfo,
	NULL
};
extern Il2CppType QueueMode_t1070_0_0_0;
extern void* RuntimeInvoker_QueueMode_t1070 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49287_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.QueueMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49287_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6901_il2cpp_TypeInfo/* declaring_type */
	, &QueueMode_t1070_0_0_0/* return_type */
	, RuntimeInvoker_QueueMode_t1070/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49287_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6901_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49287_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6901_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6901_0_0_0;
extern Il2CppType IEnumerator_1_t6901_1_0_0;
struct IEnumerator_1_t6901;
extern Il2CppGenericClass IEnumerator_1_t6901_GenericClass;
TypeInfo IEnumerator_1_t6901_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6901_MethodInfos/* methods */
	, IEnumerator_1_t6901_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6901_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6901_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6901_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6901_0_0_0/* byval_arg */
	, &IEnumerator_1_t6901_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6901_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.QueueMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_463.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4864_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.QueueMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_463MethodDeclarations.h"

extern TypeInfo QueueMode_t1070_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29355_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisQueueMode_t1070_m38692_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.QueueMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.QueueMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisQueueMode_t1070_m38692 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29351_MethodInfo;
 void InternalEnumerator_1__ctor_m29351 (InternalEnumerator_1_t4864 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29352_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29352 (InternalEnumerator_1_t4864 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29355(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29355_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&QueueMode_t1070_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29353_MethodInfo;
 void InternalEnumerator_1_Dispose_m29353 (InternalEnumerator_1_t4864 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29354_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29354 (InternalEnumerator_1_t4864 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29355 (InternalEnumerator_1_t4864 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisQueueMode_t1070_m38692(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisQueueMode_t1070_m38692_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.QueueMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4864____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4864_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4864, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4864____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4864_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4864, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4864_FieldInfos[] =
{
	&InternalEnumerator_1_t4864____array_0_FieldInfo,
	&InternalEnumerator_1_t4864____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4864____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4864_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29352_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4864____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4864_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29355_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4864_PropertyInfos[] =
{
	&InternalEnumerator_1_t4864____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4864____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4864_InternalEnumerator_1__ctor_m29351_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29351_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29351_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29351/* method */
	, &InternalEnumerator_1_t4864_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4864_InternalEnumerator_1__ctor_m29351_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29351_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29352_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29352_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29352/* method */
	, &InternalEnumerator_1_t4864_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29352_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29353_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29353_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29353/* method */
	, &InternalEnumerator_1_t4864_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29353_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29354_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29354_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29354/* method */
	, &InternalEnumerator_1_t4864_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29354_GenericMethod/* genericMethod */

};
extern Il2CppType QueueMode_t1070_0_0_0;
extern void* RuntimeInvoker_QueueMode_t1070 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29355_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.QueueMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29355_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29355/* method */
	, &InternalEnumerator_1_t4864_il2cpp_TypeInfo/* declaring_type */
	, &QueueMode_t1070_0_0_0/* return_type */
	, RuntimeInvoker_QueueMode_t1070/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29355_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4864_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29351_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29352_MethodInfo,
	&InternalEnumerator_1_Dispose_m29353_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29354_MethodInfo,
	&InternalEnumerator_1_get_Current_m29355_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4864_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29352_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29354_MethodInfo,
	&InternalEnumerator_1_Dispose_m29353_MethodInfo,
	&InternalEnumerator_1_get_Current_m29355_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4864_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6901_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4864_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6901_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4864_0_0_0;
extern Il2CppType InternalEnumerator_1_t4864_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4864_GenericClass;
TypeInfo InternalEnumerator_1_t4864_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4864_MethodInfos/* methods */
	, InternalEnumerator_1_t4864_PropertyInfos/* properties */
	, InternalEnumerator_1_t4864_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4864_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4864_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4864_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4864_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4864_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4864_1_0_0/* this_arg */
	, InternalEnumerator_1_t4864_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4864_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4864)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8757_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>
extern MethodInfo ICollection_1_get_Count_m49288_MethodInfo;
static PropertyInfo ICollection_1_t8757____Count_PropertyInfo = 
{
	&ICollection_1_t8757_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49289_MethodInfo;
static PropertyInfo ICollection_1_t8757____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8757_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49289_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8757_PropertyInfos[] =
{
	&ICollection_1_t8757____Count_PropertyInfo,
	&ICollection_1_t8757____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49288_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::get_Count()
MethodInfo ICollection_1_get_Count_m49288_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8757_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49288_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49289_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49289_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8757_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49289_GenericMethod/* genericMethod */

};
extern Il2CppType QueueMode_t1070_0_0_0;
extern Il2CppType QueueMode_t1070_0_0_0;
static ParameterInfo ICollection_1_t8757_ICollection_1_Add_m49290_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QueueMode_t1070_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49290_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::Add(T)
MethodInfo ICollection_1_Add_m49290_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8757_ICollection_1_Add_m49290_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49290_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49291_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::Clear()
MethodInfo ICollection_1_Clear_m49291_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49291_GenericMethod/* genericMethod */

};
extern Il2CppType QueueMode_t1070_0_0_0;
static ParameterInfo ICollection_1_t8757_ICollection_1_Contains_m49292_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QueueMode_t1070_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49292_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::Contains(T)
MethodInfo ICollection_1_Contains_m49292_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8757_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8757_ICollection_1_Contains_m49292_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49292_GenericMethod/* genericMethod */

};
extern Il2CppType QueueModeU5BU5D_t5728_0_0_0;
extern Il2CppType QueueModeU5BU5D_t5728_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8757_ICollection_1_CopyTo_m49293_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &QueueModeU5BU5D_t5728_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49293_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49293_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8757_ICollection_1_CopyTo_m49293_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49293_GenericMethod/* genericMethod */

};
extern Il2CppType QueueMode_t1070_0_0_0;
static ParameterInfo ICollection_1_t8757_ICollection_1_Remove_m49294_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QueueMode_t1070_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49294_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.QueueMode>::Remove(T)
MethodInfo ICollection_1_Remove_m49294_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8757_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8757_ICollection_1_Remove_m49294_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49294_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8757_MethodInfos[] =
{
	&ICollection_1_get_Count_m49288_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49289_MethodInfo,
	&ICollection_1_Add_m49290_MethodInfo,
	&ICollection_1_Clear_m49291_MethodInfo,
	&ICollection_1_Contains_m49292_MethodInfo,
	&ICollection_1_CopyTo_m49293_MethodInfo,
	&ICollection_1_Remove_m49294_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8759_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8757_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8759_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8757_0_0_0;
extern Il2CppType ICollection_1_t8757_1_0_0;
struct ICollection_1_t8757;
extern Il2CppGenericClass ICollection_1_t8757_GenericClass;
TypeInfo ICollection_1_t8757_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8757_MethodInfos/* methods */
	, ICollection_1_t8757_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8757_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8757_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8757_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8757_0_0_0/* byval_arg */
	, &ICollection_1_t8757_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8757_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.QueueMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.QueueMode>
extern Il2CppType IEnumerator_1_t6901_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49295_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.QueueMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49295_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8759_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6901_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49295_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8759_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49295_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8759_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8759_0_0_0;
extern Il2CppType IEnumerable_1_t8759_1_0_0;
struct IEnumerable_1_t8759;
extern Il2CppGenericClass IEnumerable_1_t8759_GenericClass;
TypeInfo IEnumerable_1_t8759_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8759_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8759_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8759_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8759_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8759_0_0_0/* byval_arg */
	, &IEnumerable_1_t8759_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8759_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8758_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.QueueMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.QueueMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.QueueMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.QueueMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.QueueMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.QueueMode>
extern MethodInfo IList_1_get_Item_m49296_MethodInfo;
extern MethodInfo IList_1_set_Item_m49297_MethodInfo;
static PropertyInfo IList_1_t8758____Item_PropertyInfo = 
{
	&IList_1_t8758_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49296_MethodInfo/* get */
	, &IList_1_set_Item_m49297_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8758_PropertyInfos[] =
{
	&IList_1_t8758____Item_PropertyInfo,
	NULL
};
extern Il2CppType QueueMode_t1070_0_0_0;
static ParameterInfo IList_1_t8758_IList_1_IndexOf_m49298_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QueueMode_t1070_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49298_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.QueueMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49298_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8758_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8758_IList_1_IndexOf_m49298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49298_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType QueueMode_t1070_0_0_0;
static ParameterInfo IList_1_t8758_IList_1_Insert_m49299_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &QueueMode_t1070_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49299_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.QueueMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49299_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8758_IList_1_Insert_m49299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49299_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8758_IList_1_RemoveAt_m49300_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49300_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.QueueMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49300_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8758_IList_1_RemoveAt_m49300_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49300_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8758_IList_1_get_Item_m49296_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType QueueMode_t1070_0_0_0;
extern void* RuntimeInvoker_QueueMode_t1070_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49296_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.QueueMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49296_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8758_il2cpp_TypeInfo/* declaring_type */
	, &QueueMode_t1070_0_0_0/* return_type */
	, RuntimeInvoker_QueueMode_t1070_Int32_t123/* invoker_method */
	, IList_1_t8758_IList_1_get_Item_m49296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49296_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType QueueMode_t1070_0_0_0;
static ParameterInfo IList_1_t8758_IList_1_set_Item_m49297_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &QueueMode_t1070_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49297_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.QueueMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49297_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8758_IList_1_set_Item_m49297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49297_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8758_MethodInfos[] =
{
	&IList_1_IndexOf_m49298_MethodInfo,
	&IList_1_Insert_m49299_MethodInfo,
	&IList_1_RemoveAt_m49300_MethodInfo,
	&IList_1_get_Item_m49296_MethodInfo,
	&IList_1_set_Item_m49297_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8758_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8757_il2cpp_TypeInfo,
	&IEnumerable_1_t8759_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8758_0_0_0;
extern Il2CppType IList_1_t8758_1_0_0;
struct IList_1_t8758;
extern Il2CppGenericClass IList_1_t8758_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8758_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8758_MethodInfos/* methods */
	, IList_1_t8758_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8758_il2cpp_TypeInfo/* element_class */
	, IList_1_t8758_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8758_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8758_0_0_0/* byval_arg */
	, &IList_1_t8758_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8758_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6903_il2cpp_TypeInfo;

// UnityEngine.Animation
#include "UnityEngine_UnityEngine_Animation.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Animation>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Animation>
extern MethodInfo IEnumerator_1_get_Current_m49301_MethodInfo;
static PropertyInfo IEnumerator_1_t6903____Current_PropertyInfo = 
{
	&IEnumerator_1_t6903_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49301_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6903_PropertyInfos[] =
{
	&IEnumerator_1_t6903____Current_PropertyInfo,
	NULL
};
extern Il2CppType Animation_t181_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49301_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Animation>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49301_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6903_il2cpp_TypeInfo/* declaring_type */
	, &Animation_t181_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49301_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6903_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49301_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6903_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6903_0_0_0;
extern Il2CppType IEnumerator_1_t6903_1_0_0;
struct IEnumerator_1_t6903;
extern Il2CppGenericClass IEnumerator_1_t6903_GenericClass;
TypeInfo IEnumerator_1_t6903_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6903_MethodInfos/* methods */
	, IEnumerator_1_t6903_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6903_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6903_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6903_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6903_0_0_0/* byval_arg */
	, &IEnumerator_1_t6903_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6903_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Animation>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_464.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4865_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Animation>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_464MethodDeclarations.h"

extern TypeInfo Animation_t181_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29360_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAnimation_t181_m38703_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Animation>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Animation>(System.Int32)
#define Array_InternalArray__get_Item_TisAnimation_t181_m38703(__this, p0, method) (Animation_t181 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Animation>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Animation>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Animation>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Animation>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Animation>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Animation>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4865____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4865_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4865, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4865____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4865_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4865, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4865_FieldInfos[] =
{
	&InternalEnumerator_1_t4865____array_0_FieldInfo,
	&InternalEnumerator_1_t4865____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29357_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4865____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4865_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29357_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4865____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4865_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29360_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4865_PropertyInfos[] =
{
	&InternalEnumerator_1_t4865____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4865____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4865_InternalEnumerator_1__ctor_m29356_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29356_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Animation>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29356_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4865_InternalEnumerator_1__ctor_m29356_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29356_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29357_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Animation>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29357_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4865_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29357_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29358_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Animation>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29358_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29358_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29359_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Animation>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29359_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29359_GenericMethod/* genericMethod */

};
extern Il2CppType Animation_t181_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29360_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Animation>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29360_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4865_il2cpp_TypeInfo/* declaring_type */
	, &Animation_t181_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29360_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4865_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29356_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29357_MethodInfo,
	&InternalEnumerator_1_Dispose_m29358_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29359_MethodInfo,
	&InternalEnumerator_1_get_Current_m29360_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29359_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29358_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4865_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29357_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29359_MethodInfo,
	&InternalEnumerator_1_Dispose_m29358_MethodInfo,
	&InternalEnumerator_1_get_Current_m29360_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4865_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6903_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4865_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6903_il2cpp_TypeInfo, 7},
};
extern TypeInfo Animation_t181_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4865_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29360_MethodInfo/* Method Usage */,
	&Animation_t181_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAnimation_t181_m38703_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4865_0_0_0;
extern Il2CppType InternalEnumerator_1_t4865_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4865_GenericClass;
TypeInfo InternalEnumerator_1_t4865_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4865_MethodInfos/* methods */
	, InternalEnumerator_1_t4865_PropertyInfos/* properties */
	, InternalEnumerator_1_t4865_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4865_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4865_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4865_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4865_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4865_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4865_1_0_0/* this_arg */
	, InternalEnumerator_1_t4865_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4865_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4865_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4865)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8760_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Animation>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animation>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animation>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animation>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animation>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animation>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animation>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Animation>
extern MethodInfo ICollection_1_get_Count_m49302_MethodInfo;
static PropertyInfo ICollection_1_t8760____Count_PropertyInfo = 
{
	&ICollection_1_t8760_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49302_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49303_MethodInfo;
static PropertyInfo ICollection_1_t8760____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8760_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49303_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8760_PropertyInfos[] =
{
	&ICollection_1_t8760____Count_PropertyInfo,
	&ICollection_1_t8760____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49302_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Animation>::get_Count()
MethodInfo ICollection_1_get_Count_m49302_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8760_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49302_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49303_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animation>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49303_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8760_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49303_GenericMethod/* genericMethod */

};
extern Il2CppType Animation_t181_0_0_0;
extern Il2CppType Animation_t181_0_0_0;
static ParameterInfo ICollection_1_t8760_ICollection_1_Add_m49304_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Animation_t181_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49304_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animation>::Add(T)
MethodInfo ICollection_1_Add_m49304_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8760_ICollection_1_Add_m49304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49304_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49305_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animation>::Clear()
MethodInfo ICollection_1_Clear_m49305_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49305_GenericMethod/* genericMethod */

};
extern Il2CppType Animation_t181_0_0_0;
static ParameterInfo ICollection_1_t8760_ICollection_1_Contains_m49306_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Animation_t181_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49306_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animation>::Contains(T)
MethodInfo ICollection_1_Contains_m49306_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8760_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8760_ICollection_1_Contains_m49306_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49306_GenericMethod/* genericMethod */

};
extern Il2CppType AnimationU5BU5D_t5729_0_0_0;
extern Il2CppType AnimationU5BU5D_t5729_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8760_ICollection_1_CopyTo_m49307_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AnimationU5BU5D_t5729_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49307_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animation>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49307_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8760_ICollection_1_CopyTo_m49307_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49307_GenericMethod/* genericMethod */

};
extern Il2CppType Animation_t181_0_0_0;
static ParameterInfo ICollection_1_t8760_ICollection_1_Remove_m49308_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Animation_t181_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49308_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animation>::Remove(T)
MethodInfo ICollection_1_Remove_m49308_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8760_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8760_ICollection_1_Remove_m49308_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49308_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8760_MethodInfos[] =
{
	&ICollection_1_get_Count_m49302_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49303_MethodInfo,
	&ICollection_1_Add_m49304_MethodInfo,
	&ICollection_1_Clear_m49305_MethodInfo,
	&ICollection_1_Contains_m49306_MethodInfo,
	&ICollection_1_CopyTo_m49307_MethodInfo,
	&ICollection_1_Remove_m49308_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8762_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8760_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8762_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8760_0_0_0;
extern Il2CppType ICollection_1_t8760_1_0_0;
struct ICollection_1_t8760;
extern Il2CppGenericClass ICollection_1_t8760_GenericClass;
TypeInfo ICollection_1_t8760_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8760_MethodInfos/* methods */
	, ICollection_1_t8760_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8760_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8760_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8760_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8760_0_0_0/* byval_arg */
	, &ICollection_1_t8760_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8760_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Animation>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Animation>
extern Il2CppType IEnumerator_1_t6903_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49309_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Animation>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49309_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8762_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6903_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49309_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8762_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49309_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8762_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8762_0_0_0;
extern Il2CppType IEnumerable_1_t8762_1_0_0;
struct IEnumerable_1_t8762;
extern Il2CppGenericClass IEnumerable_1_t8762_GenericClass;
TypeInfo IEnumerable_1_t8762_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8762_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8762_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8762_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8762_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8762_0_0_0/* byval_arg */
	, &IEnumerable_1_t8762_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8762_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8761_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Animation>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animation>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animation>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Animation>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animation>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Animation>
extern MethodInfo IList_1_get_Item_m49310_MethodInfo;
extern MethodInfo IList_1_set_Item_m49311_MethodInfo;
static PropertyInfo IList_1_t8761____Item_PropertyInfo = 
{
	&IList_1_t8761_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49310_MethodInfo/* get */
	, &IList_1_set_Item_m49311_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8761_PropertyInfos[] =
{
	&IList_1_t8761____Item_PropertyInfo,
	NULL
};
extern Il2CppType Animation_t181_0_0_0;
static ParameterInfo IList_1_t8761_IList_1_IndexOf_m49312_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Animation_t181_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49312_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Animation>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49312_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8761_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8761_IList_1_IndexOf_m49312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49312_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Animation_t181_0_0_0;
static ParameterInfo IList_1_t8761_IList_1_Insert_m49313_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Animation_t181_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49313_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animation>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49313_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8761_IList_1_Insert_m49313_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49313_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8761_IList_1_RemoveAt_m49314_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49314_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animation>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49314_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8761_IList_1_RemoveAt_m49314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49314_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8761_IList_1_get_Item_m49310_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Animation_t181_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49310_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Animation>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49310_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8761_il2cpp_TypeInfo/* declaring_type */
	, &Animation_t181_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8761_IList_1_get_Item_m49310_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49310_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Animation_t181_0_0_0;
static ParameterInfo IList_1_t8761_IList_1_set_Item_m49311_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Animation_t181_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49311_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animation>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49311_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8761_IList_1_set_Item_m49311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49311_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8761_MethodInfos[] =
{
	&IList_1_IndexOf_m49312_MethodInfo,
	&IList_1_Insert_m49313_MethodInfo,
	&IList_1_RemoveAt_m49314_MethodInfo,
	&IList_1_get_Item_m49310_MethodInfo,
	&IList_1_set_Item_m49311_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8761_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8760_il2cpp_TypeInfo,
	&IEnumerable_1_t8762_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8761_0_0_0;
extern Il2CppType IList_1_t8761_1_0_0;
struct IList_1_t8761;
extern Il2CppGenericClass IList_1_t8761_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8761_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8761_MethodInfos/* methods */
	, IList_1_t8761_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8761_il2cpp_TypeInfo/* element_class */
	, IList_1_t8761_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8761_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8761_0_0_0/* byval_arg */
	, &IList_1_t8761_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8761_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animation>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_173.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4866_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animation>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_173MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_175.h"
extern TypeInfo InvokableCall_1_t4867_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_175MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m29363_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m29365_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animation>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animation>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animation>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4866____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4866_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4866, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4866_FieldInfos[] =
{
	&CachedInvokableCall_1_t4866____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Animation_t181_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4866_CachedInvokableCall_1__ctor_m29361_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Animation_t181_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m29361_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animation>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m29361_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4866_CachedInvokableCall_1__ctor_m29361_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m29361_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4866_CachedInvokableCall_1_Invoke_m29362_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m29362_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animation>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m29362_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4866_CachedInvokableCall_1_Invoke_m29362_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m29362_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4866_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m29361_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29362_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m29362_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m29366_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4866_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29362_MethodInfo,
	&InvokableCall_1_Find_m29366_MethodInfo,
};
extern Il2CppType UnityAction_1_t4868_0_0_0;
extern TypeInfo UnityAction_1_t4868_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisAnimation_t181_m38713_MethodInfo;
extern TypeInfo Animation_t181_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m29368_MethodInfo;
extern TypeInfo Animation_t181_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4866_RGCTXData[8] = 
{
	&UnityAction_1_t4868_0_0_0/* Type Usage */,
	&UnityAction_1_t4868_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisAnimation_t181_m38713_MethodInfo/* Method Usage */,
	&Animation_t181_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29368_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m29363_MethodInfo/* Method Usage */,
	&Animation_t181_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m29365_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4866_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4866_1_0_0;
struct CachedInvokableCall_1_t4866;
extern Il2CppGenericClass CachedInvokableCall_1_t4866_GenericClass;
TypeInfo CachedInvokableCall_1_t4866_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4866_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4866_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4867_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4866_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4866_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4866_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4866_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4866_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4866_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4866_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4866)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Animation>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_179.h"
extern TypeInfo UnityAction_1_t4868_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Animation>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_179MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Animation>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Animation>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisAnimation_t181_m38713(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>
extern Il2CppType UnityAction_1_t4868_0_0_1;
FieldInfo InvokableCall_1_t4867____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4868_0_0_1/* type */
	, &InvokableCall_1_t4867_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4867, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4867_FieldInfos[] =
{
	&InvokableCall_1_t4867____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4867_InvokableCall_1__ctor_m29363_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29363_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m29363_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4867_InvokableCall_1__ctor_m29363_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29363_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4868_0_0_0;
static ParameterInfo InvokableCall_1_t4867_InvokableCall_1__ctor_m29364_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4868_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29364_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m29364_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4867_InvokableCall_1__ctor_m29364_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29364_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4867_InvokableCall_1_Invoke_m29365_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m29365_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m29365_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4867_InvokableCall_1_Invoke_m29365_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m29365_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4867_InvokableCall_1_Find_m29366_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m29366_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Animation>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m29366_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4867_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4867_InvokableCall_1_Find_m29366_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m29366_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4867_MethodInfos[] =
{
	&InvokableCall_1__ctor_m29363_MethodInfo,
	&InvokableCall_1__ctor_m29364_MethodInfo,
	&InvokableCall_1_Invoke_m29365_MethodInfo,
	&InvokableCall_1_Find_m29366_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4867_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m29365_MethodInfo,
	&InvokableCall_1_Find_m29366_MethodInfo,
};
extern TypeInfo UnityAction_1_t4868_il2cpp_TypeInfo;
extern TypeInfo Animation_t181_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4867_RGCTXData[5] = 
{
	&UnityAction_1_t4868_0_0_0/* Type Usage */,
	&UnityAction_1_t4868_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisAnimation_t181_m38713_MethodInfo/* Method Usage */,
	&Animation_t181_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29368_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4867_0_0_0;
extern Il2CppType InvokableCall_1_t4867_1_0_0;
struct InvokableCall_1_t4867;
extern Il2CppGenericClass InvokableCall_1_t4867_GenericClass;
TypeInfo InvokableCall_1_t4867_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4867_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4867_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4867_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4867_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4867_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4867_0_0_0/* byval_arg */
	, &InvokableCall_1_t4867_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4867_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4867_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4867)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animation>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animation>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Animation>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animation>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Animation>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4868_UnityAction_1__ctor_m29367_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m29367_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animation>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m29367_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4868_UnityAction_1__ctor_m29367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m29367_GenericMethod/* genericMethod */

};
extern Il2CppType Animation_t181_0_0_0;
static ParameterInfo UnityAction_1_t4868_UnityAction_1_Invoke_m29368_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Animation_t181_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m29368_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animation>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m29368_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4868_UnityAction_1_Invoke_m29368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m29368_GenericMethod/* genericMethod */

};
extern Il2CppType Animation_t181_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4868_UnityAction_1_BeginInvoke_m29369_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Animation_t181_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m29369_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Animation>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m29369_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4868_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4868_UnityAction_1_BeginInvoke_m29369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m29369_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4868_UnityAction_1_EndInvoke_m29370_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m29370_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animation>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m29370_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4868_UnityAction_1_EndInvoke_m29370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m29370_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4868_MethodInfos[] =
{
	&UnityAction_1__ctor_m29367_MethodInfo,
	&UnityAction_1_Invoke_m29368_MethodInfo,
	&UnityAction_1_BeginInvoke_m29369_MethodInfo,
	&UnityAction_1_EndInvoke_m29370_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m29369_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m29370_MethodInfo;
static MethodInfo* UnityAction_1_t4868_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m29368_MethodInfo,
	&UnityAction_1_BeginInvoke_m29369_MethodInfo,
	&UnityAction_1_EndInvoke_m29370_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4868_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4868_1_0_0;
struct UnityAction_1_t4868;
extern Il2CppGenericClass UnityAction_1_t4868_GenericClass;
TypeInfo UnityAction_1_t4868_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4868_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4868_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4868_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4868_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4868_0_0_0/* byval_arg */
	, &UnityAction_1_t4868_1_0_0/* this_arg */
	, UnityAction_1_t4868_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4868_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4868)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6905_il2cpp_TypeInfo;

// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Animator>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Animator>
extern MethodInfo IEnumerator_1_get_Current_m49315_MethodInfo;
static PropertyInfo IEnumerator_1_t6905____Current_PropertyInfo = 
{
	&IEnumerator_1_t6905_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49315_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6905_PropertyInfos[] =
{
	&IEnumerator_1_t6905____Current_PropertyInfo,
	NULL
};
extern Il2CppType Animator_t404_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49315_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Animator>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49315_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6905_il2cpp_TypeInfo/* declaring_type */
	, &Animator_t404_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49315_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6905_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49315_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6905_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6905_0_0_0;
extern Il2CppType IEnumerator_1_t6905_1_0_0;
struct IEnumerator_1_t6905;
extern Il2CppGenericClass IEnumerator_1_t6905_GenericClass;
TypeInfo IEnumerator_1_t6905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6905_MethodInfos/* methods */
	, IEnumerator_1_t6905_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6905_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6905_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6905_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6905_0_0_0/* byval_arg */
	, &IEnumerator_1_t6905_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6905_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Animator>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_465.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4869_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Animator>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_465MethodDeclarations.h"

extern TypeInfo Animator_t404_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29375_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAnimator_t404_m38715_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Animator>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Animator>(System.Int32)
#define Array_InternalArray__get_Item_TisAnimator_t404_m38715(__this, p0, method) (Animator_t404 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Animator>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Animator>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Animator>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Animator>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Animator>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Animator>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4869____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4869_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4869, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4869____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4869_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4869, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4869_FieldInfos[] =
{
	&InternalEnumerator_1_t4869____array_0_FieldInfo,
	&InternalEnumerator_1_t4869____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29372_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4869____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4869_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29372_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4869____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4869_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29375_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4869_PropertyInfos[] =
{
	&InternalEnumerator_1_t4869____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4869____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4869_InternalEnumerator_1__ctor_m29371_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29371_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Animator>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29371_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4869_InternalEnumerator_1__ctor_m29371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29371_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29372_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Animator>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29372_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4869_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29372_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29373_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Animator>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29373_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29373_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29374_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Animator>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29374_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4869_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29374_GenericMethod/* genericMethod */

};
extern Il2CppType Animator_t404_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29375_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Animator>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29375_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4869_il2cpp_TypeInfo/* declaring_type */
	, &Animator_t404_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29375_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4869_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29371_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29372_MethodInfo,
	&InternalEnumerator_1_Dispose_m29373_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29374_MethodInfo,
	&InternalEnumerator_1_get_Current_m29375_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29374_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29373_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4869_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29372_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29374_MethodInfo,
	&InternalEnumerator_1_Dispose_m29373_MethodInfo,
	&InternalEnumerator_1_get_Current_m29375_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4869_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6905_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4869_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6905_il2cpp_TypeInfo, 7},
};
extern TypeInfo Animator_t404_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4869_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29375_MethodInfo/* Method Usage */,
	&Animator_t404_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAnimator_t404_m38715_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4869_0_0_0;
extern Il2CppType InternalEnumerator_1_t4869_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4869_GenericClass;
TypeInfo InternalEnumerator_1_t4869_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4869_MethodInfos/* methods */
	, InternalEnumerator_1_t4869_PropertyInfos/* properties */
	, InternalEnumerator_1_t4869_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4869_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4869_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4869_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4869_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4869_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4869_1_0_0/* this_arg */
	, InternalEnumerator_1_t4869_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4869_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4869_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4869)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8763_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Animator>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animator>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animator>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animator>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animator>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animator>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animator>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Animator>
extern MethodInfo ICollection_1_get_Count_m49316_MethodInfo;
static PropertyInfo ICollection_1_t8763____Count_PropertyInfo = 
{
	&ICollection_1_t8763_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49316_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49317_MethodInfo;
static PropertyInfo ICollection_1_t8763____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8763_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49317_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8763_PropertyInfos[] =
{
	&ICollection_1_t8763____Count_PropertyInfo,
	&ICollection_1_t8763____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49316_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Animator>::get_Count()
MethodInfo ICollection_1_get_Count_m49316_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8763_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49316_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49317_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animator>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49317_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8763_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49317_GenericMethod/* genericMethod */

};
extern Il2CppType Animator_t404_0_0_0;
extern Il2CppType Animator_t404_0_0_0;
static ParameterInfo ICollection_1_t8763_ICollection_1_Add_m49318_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49318_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animator>::Add(T)
MethodInfo ICollection_1_Add_m49318_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8763_ICollection_1_Add_m49318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49318_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49319_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animator>::Clear()
MethodInfo ICollection_1_Clear_m49319_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49319_GenericMethod/* genericMethod */

};
extern Il2CppType Animator_t404_0_0_0;
static ParameterInfo ICollection_1_t8763_ICollection_1_Contains_m49320_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49320_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animator>::Contains(T)
MethodInfo ICollection_1_Contains_m49320_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8763_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8763_ICollection_1_Contains_m49320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49320_GenericMethod/* genericMethod */

};
extern Il2CppType AnimatorU5BU5D_t5730_0_0_0;
extern Il2CppType AnimatorU5BU5D_t5730_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8763_ICollection_1_CopyTo_m49321_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AnimatorU5BU5D_t5730_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49321_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Animator>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49321_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8763_ICollection_1_CopyTo_m49321_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49321_GenericMethod/* genericMethod */

};
extern Il2CppType Animator_t404_0_0_0;
static ParameterInfo ICollection_1_t8763_ICollection_1_Remove_m49322_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49322_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Animator>::Remove(T)
MethodInfo ICollection_1_Remove_m49322_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8763_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8763_ICollection_1_Remove_m49322_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49322_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8763_MethodInfos[] =
{
	&ICollection_1_get_Count_m49316_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49317_MethodInfo,
	&ICollection_1_Add_m49318_MethodInfo,
	&ICollection_1_Clear_m49319_MethodInfo,
	&ICollection_1_Contains_m49320_MethodInfo,
	&ICollection_1_CopyTo_m49321_MethodInfo,
	&ICollection_1_Remove_m49322_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8765_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8763_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8765_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8763_0_0_0;
extern Il2CppType ICollection_1_t8763_1_0_0;
struct ICollection_1_t8763;
extern Il2CppGenericClass ICollection_1_t8763_GenericClass;
TypeInfo ICollection_1_t8763_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8763_MethodInfos/* methods */
	, ICollection_1_t8763_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8763_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8763_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8763_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8763_0_0_0/* byval_arg */
	, &ICollection_1_t8763_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8763_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Animator>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Animator>
extern Il2CppType IEnumerator_1_t6905_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49323_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Animator>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49323_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8765_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6905_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49323_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8765_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49323_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8765_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8765_0_0_0;
extern Il2CppType IEnumerable_1_t8765_1_0_0;
struct IEnumerable_1_t8765;
extern Il2CppGenericClass IEnumerable_1_t8765_GenericClass;
TypeInfo IEnumerable_1_t8765_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8765_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8765_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8765_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8765_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8765_0_0_0/* byval_arg */
	, &IEnumerable_1_t8765_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8765_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8764_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Animator>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animator>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animator>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Animator>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animator>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Animator>
extern MethodInfo IList_1_get_Item_m49324_MethodInfo;
extern MethodInfo IList_1_set_Item_m49325_MethodInfo;
static PropertyInfo IList_1_t8764____Item_PropertyInfo = 
{
	&IList_1_t8764_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49324_MethodInfo/* get */
	, &IList_1_set_Item_m49325_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8764_PropertyInfos[] =
{
	&IList_1_t8764____Item_PropertyInfo,
	NULL
};
extern Il2CppType Animator_t404_0_0_0;
static ParameterInfo IList_1_t8764_IList_1_IndexOf_m49326_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49326_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Animator>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49326_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8764_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8764_IList_1_IndexOf_m49326_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49326_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Animator_t404_0_0_0;
static ParameterInfo IList_1_t8764_IList_1_Insert_m49327_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49327_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animator>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49327_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8764_IList_1_Insert_m49327_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49327_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8764_IList_1_RemoveAt_m49328_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49328_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animator>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49328_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8764_IList_1_RemoveAt_m49328_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49328_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8764_IList_1_get_Item_m49324_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Animator_t404_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49324_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Animator>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49324_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8764_il2cpp_TypeInfo/* declaring_type */
	, &Animator_t404_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8764_IList_1_get_Item_m49324_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49324_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Animator_t404_0_0_0;
static ParameterInfo IList_1_t8764_IList_1_set_Item_m49325_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49325_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Animator>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49325_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8764_IList_1_set_Item_m49325_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49325_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8764_MethodInfos[] =
{
	&IList_1_IndexOf_m49326_MethodInfo,
	&IList_1_Insert_m49327_MethodInfo,
	&IList_1_RemoveAt_m49328_MethodInfo,
	&IList_1_get_Item_m49324_MethodInfo,
	&IList_1_set_Item_m49325_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8764_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8763_il2cpp_TypeInfo,
	&IEnumerable_1_t8765_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8764_0_0_0;
extern Il2CppType IList_1_t8764_1_0_0;
struct IList_1_t8764;
extern Il2CppGenericClass IList_1_t8764_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8764_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8764_MethodInfos/* methods */
	, IList_1_t8764_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8764_il2cpp_TypeInfo/* element_class */
	, IList_1_t8764_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8764_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8764_0_0_0/* byval_arg */
	, &IList_1_t8764_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8764_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animator>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_174.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4870_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animator>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_174MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_176.h"
extern TypeInfo InvokableCall_1_t4871_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_176MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m29378_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m29380_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animator>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animator>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animator>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4870____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4870_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4870, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4870_FieldInfos[] =
{
	&CachedInvokableCall_1_t4870____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Animator_t404_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4870_CachedInvokableCall_1__ctor_m29376_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m29376_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animator>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m29376_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4870_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4870_CachedInvokableCall_1__ctor_m29376_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m29376_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4870_CachedInvokableCall_1_Invoke_m29377_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m29377_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animator>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m29377_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4870_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4870_CachedInvokableCall_1_Invoke_m29377_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m29377_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4870_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m29376_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29377_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m29377_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m29381_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4870_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29377_MethodInfo,
	&InvokableCall_1_Find_m29381_MethodInfo,
};
extern Il2CppType UnityAction_1_t4872_0_0_0;
extern TypeInfo UnityAction_1_t4872_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisAnimator_t404_m38725_MethodInfo;
extern TypeInfo Animator_t404_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m29383_MethodInfo;
extern TypeInfo Animator_t404_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4870_RGCTXData[8] = 
{
	&UnityAction_1_t4872_0_0_0/* Type Usage */,
	&UnityAction_1_t4872_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisAnimator_t404_m38725_MethodInfo/* Method Usage */,
	&Animator_t404_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29383_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m29378_MethodInfo/* Method Usage */,
	&Animator_t404_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m29380_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4870_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4870_1_0_0;
struct CachedInvokableCall_1_t4870;
extern Il2CppGenericClass CachedInvokableCall_1_t4870_GenericClass;
TypeInfo CachedInvokableCall_1_t4870_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4870_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4870_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4871_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4870_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4870_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4870_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4870_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4870_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4870_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4870_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4870)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Animator>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_180.h"
extern TypeInfo UnityAction_1_t4872_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Animator>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_180MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Animator>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Animator>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisAnimator_t404_m38725(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>
extern Il2CppType UnityAction_1_t4872_0_0_1;
FieldInfo InvokableCall_1_t4871____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4872_0_0_1/* type */
	, &InvokableCall_1_t4871_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4871, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4871_FieldInfos[] =
{
	&InvokableCall_1_t4871____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4871_InvokableCall_1__ctor_m29378_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29378_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m29378_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4871_InvokableCall_1__ctor_m29378_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29378_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4872_0_0_0;
static ParameterInfo InvokableCall_1_t4871_InvokableCall_1__ctor_m29379_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4872_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29379_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m29379_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4871_InvokableCall_1__ctor_m29379_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29379_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4871_InvokableCall_1_Invoke_m29380_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m29380_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m29380_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4871_InvokableCall_1_Invoke_m29380_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m29380_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4871_InvokableCall_1_Find_m29381_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m29381_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m29381_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4871_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4871_InvokableCall_1_Find_m29381_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m29381_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4871_MethodInfos[] =
{
	&InvokableCall_1__ctor_m29378_MethodInfo,
	&InvokableCall_1__ctor_m29379_MethodInfo,
	&InvokableCall_1_Invoke_m29380_MethodInfo,
	&InvokableCall_1_Find_m29381_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4871_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m29380_MethodInfo,
	&InvokableCall_1_Find_m29381_MethodInfo,
};
extern TypeInfo UnityAction_1_t4872_il2cpp_TypeInfo;
extern TypeInfo Animator_t404_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4871_RGCTXData[5] = 
{
	&UnityAction_1_t4872_0_0_0/* Type Usage */,
	&UnityAction_1_t4872_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisAnimator_t404_m38725_MethodInfo/* Method Usage */,
	&Animator_t404_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29383_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4871_0_0_0;
extern Il2CppType InvokableCall_1_t4871_1_0_0;
struct InvokableCall_1_t4871;
extern Il2CppGenericClass InvokableCall_1_t4871_GenericClass;
TypeInfo InvokableCall_1_t4871_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4871_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4871_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4871_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4871_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4871_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4871_0_0_0/* byval_arg */
	, &InvokableCall_1_t4871_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4871_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4871_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4871)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animator>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animator>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Animator>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animator>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Animator>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4872_UnityAction_1__ctor_m29382_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m29382_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animator>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m29382_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4872_UnityAction_1__ctor_m29382_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m29382_GenericMethod/* genericMethod */

};
extern Il2CppType Animator_t404_0_0_0;
static ParameterInfo UnityAction_1_t4872_UnityAction_1_Invoke_m29383_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m29383_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animator>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m29383_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4872_UnityAction_1_Invoke_m29383_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m29383_GenericMethod/* genericMethod */

};
extern Il2CppType Animator_t404_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4872_UnityAction_1_BeginInvoke_m29384_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Animator_t404_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m29384_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Animator>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m29384_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4872_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4872_UnityAction_1_BeginInvoke_m29384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m29384_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4872_UnityAction_1_EndInvoke_m29385_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m29385_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Animator>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m29385_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4872_UnityAction_1_EndInvoke_m29385_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m29385_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4872_MethodInfos[] =
{
	&UnityAction_1__ctor_m29382_MethodInfo,
	&UnityAction_1_Invoke_m29383_MethodInfo,
	&UnityAction_1_BeginInvoke_m29384_MethodInfo,
	&UnityAction_1_EndInvoke_m29385_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m29384_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m29385_MethodInfo;
static MethodInfo* UnityAction_1_t4872_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m29383_MethodInfo,
	&UnityAction_1_BeginInvoke_m29384_MethodInfo,
	&UnityAction_1_EndInvoke_m29385_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4872_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4872_1_0_0;
struct UnityAction_1_t4872;
extern Il2CppGenericClass UnityAction_1_t4872_GenericClass;
TypeInfo UnityAction_1_t4872_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4872_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4872_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4872_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4872_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4872_0_0_0/* byval_arg */
	, &UnityAction_1_t4872_1_0_0/* this_arg */
	, UnityAction_1_t4872_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4872_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4872)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6907_il2cpp_TypeInfo;

// UnityEngine.RuntimeAnimatorController
#include "UnityEngine_UnityEngine_RuntimeAnimatorController.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.RuntimeAnimatorController>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RuntimeAnimatorController>
extern MethodInfo IEnumerator_1_get_Current_m49329_MethodInfo;
static PropertyInfo IEnumerator_1_t6907____Current_PropertyInfo = 
{
	&IEnumerator_1_t6907_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49329_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6907_PropertyInfos[] =
{
	&IEnumerator_1_t6907____Current_PropertyInfo,
	NULL
};
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49329_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.RuntimeAnimatorController>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49329_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6907_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeAnimatorController_t543_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49329_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6907_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49329_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6907_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6907_0_0_0;
extern Il2CppType IEnumerator_1_t6907_1_0_0;
struct IEnumerator_1_t6907;
extern Il2CppGenericClass IEnumerator_1_t6907_GenericClass;
TypeInfo IEnumerator_1_t6907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6907_MethodInfos/* methods */
	, IEnumerator_1_t6907_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6907_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6907_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6907_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6907_0_0_0/* byval_arg */
	, &IEnumerator_1_t6907_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6907_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_466.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4873_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_466MethodDeclarations.h"

extern TypeInfo RuntimeAnimatorController_t543_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29390_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRuntimeAnimatorController_t543_m38727_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.RuntimeAnimatorController>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RuntimeAnimatorController>(System.Int32)
#define Array_InternalArray__get_Item_TisRuntimeAnimatorController_t543_m38727(__this, p0, method) (RuntimeAnimatorController_t543 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4873____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4873_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4873, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4873____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4873_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4873, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4873_FieldInfos[] =
{
	&InternalEnumerator_1_t4873____array_0_FieldInfo,
	&InternalEnumerator_1_t4873____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29387_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4873____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4873_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29387_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4873____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4873_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29390_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4873_PropertyInfos[] =
{
	&InternalEnumerator_1_t4873____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4873____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4873_InternalEnumerator_1__ctor_m29386_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29386_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29386_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4873_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4873_InternalEnumerator_1__ctor_m29386_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29386_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29387_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29387_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4873_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29387_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29388_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29388_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4873_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29388_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29389_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29389_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4873_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29389_GenericMethod/* genericMethod */

};
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29390_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.RuntimeAnimatorController>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29390_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4873_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeAnimatorController_t543_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29390_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4873_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29386_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29387_MethodInfo,
	&InternalEnumerator_1_Dispose_m29388_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29389_MethodInfo,
	&InternalEnumerator_1_get_Current_m29390_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29389_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29388_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4873_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29387_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29389_MethodInfo,
	&InternalEnumerator_1_Dispose_m29388_MethodInfo,
	&InternalEnumerator_1_get_Current_m29390_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4873_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6907_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4873_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6907_il2cpp_TypeInfo, 7},
};
extern TypeInfo RuntimeAnimatorController_t543_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4873_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29390_MethodInfo/* Method Usage */,
	&RuntimeAnimatorController_t543_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisRuntimeAnimatorController_t543_m38727_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4873_0_0_0;
extern Il2CppType InternalEnumerator_1_t4873_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4873_GenericClass;
TypeInfo InternalEnumerator_1_t4873_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4873_MethodInfos/* methods */
	, InternalEnumerator_1_t4873_PropertyInfos/* properties */
	, InternalEnumerator_1_t4873_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4873_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4873_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4873_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4873_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4873_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4873_1_0_0/* this_arg */
	, InternalEnumerator_1_t4873_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4873_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4873_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4873)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8766_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>
extern MethodInfo ICollection_1_get_Count_m49330_MethodInfo;
static PropertyInfo ICollection_1_t8766____Count_PropertyInfo = 
{
	&ICollection_1_t8766_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49330_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49331_MethodInfo;
static PropertyInfo ICollection_1_t8766____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8766_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49331_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8766_PropertyInfos[] =
{
	&ICollection_1_t8766____Count_PropertyInfo,
	&ICollection_1_t8766____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49330_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::get_Count()
MethodInfo ICollection_1_get_Count_m49330_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8766_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49330_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49331_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49331_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8766_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49331_GenericMethod/* genericMethod */

};
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
static ParameterInfo ICollection_1_t8766_ICollection_1_Add_m49332_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RuntimeAnimatorController_t543_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49332_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::Add(T)
MethodInfo ICollection_1_Add_m49332_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8766_ICollection_1_Add_m49332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49332_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49333_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::Clear()
MethodInfo ICollection_1_Clear_m49333_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49333_GenericMethod/* genericMethod */

};
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
static ParameterInfo ICollection_1_t8766_ICollection_1_Contains_m49334_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RuntimeAnimatorController_t543_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49334_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::Contains(T)
MethodInfo ICollection_1_Contains_m49334_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8766_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8766_ICollection_1_Contains_m49334_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49334_GenericMethod/* genericMethod */

};
extern Il2CppType RuntimeAnimatorControllerU5BU5D_t5731_0_0_0;
extern Il2CppType RuntimeAnimatorControllerU5BU5D_t5731_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8766_ICollection_1_CopyTo_m49335_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RuntimeAnimatorControllerU5BU5D_t5731_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49335_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49335_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8766_ICollection_1_CopyTo_m49335_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49335_GenericMethod/* genericMethod */

};
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
static ParameterInfo ICollection_1_t8766_ICollection_1_Remove_m49336_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RuntimeAnimatorController_t543_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49336_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.RuntimeAnimatorController>::Remove(T)
MethodInfo ICollection_1_Remove_m49336_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8766_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8766_ICollection_1_Remove_m49336_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49336_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8766_MethodInfos[] =
{
	&ICollection_1_get_Count_m49330_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49331_MethodInfo,
	&ICollection_1_Add_m49332_MethodInfo,
	&ICollection_1_Clear_m49333_MethodInfo,
	&ICollection_1_Contains_m49334_MethodInfo,
	&ICollection_1_CopyTo_m49335_MethodInfo,
	&ICollection_1_Remove_m49336_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8768_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8766_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8768_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8766_0_0_0;
extern Il2CppType ICollection_1_t8766_1_0_0;
struct ICollection_1_t8766;
extern Il2CppGenericClass ICollection_1_t8766_GenericClass;
TypeInfo ICollection_1_t8766_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8766_MethodInfos/* methods */
	, ICollection_1_t8766_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8766_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8766_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8766_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8766_0_0_0/* byval_arg */
	, &ICollection_1_t8766_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8766_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RuntimeAnimatorController>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RuntimeAnimatorController>
extern Il2CppType IEnumerator_1_t6907_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49337_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.RuntimeAnimatorController>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49337_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8768_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6907_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49337_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8768_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49337_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8768_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8768_0_0_0;
extern Il2CppType IEnumerable_1_t8768_1_0_0;
struct IEnumerable_1_t8768;
extern Il2CppGenericClass IEnumerable_1_t8768_GenericClass;
TypeInfo IEnumerable_1_t8768_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8768_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8768_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8768_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8768_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8768_0_0_0/* byval_arg */
	, &IEnumerable_1_t8768_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8768_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8767_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>
extern MethodInfo IList_1_get_Item_m49338_MethodInfo;
extern MethodInfo IList_1_set_Item_m49339_MethodInfo;
static PropertyInfo IList_1_t8767____Item_PropertyInfo = 
{
	&IList_1_t8767_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49338_MethodInfo/* get */
	, &IList_1_set_Item_m49339_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8767_PropertyInfos[] =
{
	&IList_1_t8767____Item_PropertyInfo,
	NULL
};
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
static ParameterInfo IList_1_t8767_IList_1_IndexOf_m49340_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RuntimeAnimatorController_t543_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49340_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49340_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8767_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8767_IList_1_IndexOf_m49340_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49340_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
static ParameterInfo IList_1_t8767_IList_1_Insert_m49341_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &RuntimeAnimatorController_t543_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49341_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49341_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8767_IList_1_Insert_m49341_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49341_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8767_IList_1_RemoveAt_m49342_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49342_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49342_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8767_IList_1_RemoveAt_m49342_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49342_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8767_IList_1_get_Item_m49338_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49338_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49338_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8767_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeAnimatorController_t543_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8767_IList_1_get_Item_m49338_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49338_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
static ParameterInfo IList_1_t8767_IList_1_set_Item_m49339_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &RuntimeAnimatorController_t543_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49339_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.RuntimeAnimatorController>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49339_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8767_IList_1_set_Item_m49339_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49339_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8767_MethodInfos[] =
{
	&IList_1_IndexOf_m49340_MethodInfo,
	&IList_1_Insert_m49341_MethodInfo,
	&IList_1_RemoveAt_m49342_MethodInfo,
	&IList_1_get_Item_m49338_MethodInfo,
	&IList_1_set_Item_m49339_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8767_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8766_il2cpp_TypeInfo,
	&IEnumerable_1_t8768_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8767_0_0_0;
extern Il2CppType IList_1_t8767_1_0_0;
struct IList_1_t8767;
extern Il2CppGenericClass IList_1_t8767_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8767_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8767_MethodInfos/* methods */
	, IList_1_t8767_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8767_il2cpp_TypeInfo/* element_class */
	, IList_1_t8767_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8767_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8767_0_0_0/* byval_arg */
	, &IList_1_t8767_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8767_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RuntimeAnimatorController>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_175.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4874_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RuntimeAnimatorController>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_175MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_177.h"
extern TypeInfo InvokableCall_1_t4875_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_177MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m29393_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m29395_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RuntimeAnimatorController>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RuntimeAnimatorController>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RuntimeAnimatorController>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4874____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4874_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4874, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4874_FieldInfos[] =
{
	&CachedInvokableCall_1_t4874____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4874_CachedInvokableCall_1__ctor_m29391_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &RuntimeAnimatorController_t543_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m29391_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RuntimeAnimatorController>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m29391_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4874_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4874_CachedInvokableCall_1__ctor_m29391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m29391_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4874_CachedInvokableCall_1_Invoke_m29392_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m29392_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RuntimeAnimatorController>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m29392_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4874_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4874_CachedInvokableCall_1_Invoke_m29392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m29392_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4874_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m29391_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29392_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m29392_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m29396_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4874_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29392_MethodInfo,
	&InvokableCall_1_Find_m29396_MethodInfo,
};
extern Il2CppType UnityAction_1_t4876_0_0_0;
extern TypeInfo UnityAction_1_t4876_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisRuntimeAnimatorController_t543_m38737_MethodInfo;
extern TypeInfo RuntimeAnimatorController_t543_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m29398_MethodInfo;
extern TypeInfo RuntimeAnimatorController_t543_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4874_RGCTXData[8] = 
{
	&UnityAction_1_t4876_0_0_0/* Type Usage */,
	&UnityAction_1_t4876_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRuntimeAnimatorController_t543_m38737_MethodInfo/* Method Usage */,
	&RuntimeAnimatorController_t543_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29398_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m29393_MethodInfo/* Method Usage */,
	&RuntimeAnimatorController_t543_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m29395_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4874_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4874_1_0_0;
struct CachedInvokableCall_1_t4874;
extern Il2CppGenericClass CachedInvokableCall_1_t4874_GenericClass;
TypeInfo CachedInvokableCall_1_t4874_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4874_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4874_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4875_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4874_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4874_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4874_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4874_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4874_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4874_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4874_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4874)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_181.h"
extern TypeInfo UnityAction_1_t4876_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_181MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.RuntimeAnimatorController>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.RuntimeAnimatorController>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisRuntimeAnimatorController_t543_m38737(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>
extern Il2CppType UnityAction_1_t4876_0_0_1;
FieldInfo InvokableCall_1_t4875____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4876_0_0_1/* type */
	, &InvokableCall_1_t4875_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4875, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4875_FieldInfos[] =
{
	&InvokableCall_1_t4875____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4875_InvokableCall_1__ctor_m29393_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29393_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m29393_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4875_InvokableCall_1__ctor_m29393_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29393_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4876_0_0_0;
static ParameterInfo InvokableCall_1_t4875_InvokableCall_1__ctor_m29394_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4876_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29394_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m29394_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4875_InvokableCall_1__ctor_m29394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29394_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4875_InvokableCall_1_Invoke_m29395_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m29395_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m29395_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4875_InvokableCall_1_Invoke_m29395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m29395_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4875_InvokableCall_1_Find_m29396_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m29396_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m29396_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4875_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4875_InvokableCall_1_Find_m29396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m29396_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4875_MethodInfos[] =
{
	&InvokableCall_1__ctor_m29393_MethodInfo,
	&InvokableCall_1__ctor_m29394_MethodInfo,
	&InvokableCall_1_Invoke_m29395_MethodInfo,
	&InvokableCall_1_Find_m29396_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4875_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m29395_MethodInfo,
	&InvokableCall_1_Find_m29396_MethodInfo,
};
extern TypeInfo UnityAction_1_t4876_il2cpp_TypeInfo;
extern TypeInfo RuntimeAnimatorController_t543_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4875_RGCTXData[5] = 
{
	&UnityAction_1_t4876_0_0_0/* Type Usage */,
	&UnityAction_1_t4876_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRuntimeAnimatorController_t543_m38737_MethodInfo/* Method Usage */,
	&RuntimeAnimatorController_t543_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29398_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4875_0_0_0;
extern Il2CppType InvokableCall_1_t4875_1_0_0;
struct InvokableCall_1_t4875;
extern Il2CppGenericClass InvokableCall_1_t4875_GenericClass;
TypeInfo InvokableCall_1_t4875_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4875_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4875_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4875_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4875_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4875_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4875_0_0_0/* byval_arg */
	, &InvokableCall_1_t4875_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4875_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4875_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4875)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4876_UnityAction_1__ctor_m29397_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m29397_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m29397_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4876_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4876_UnityAction_1__ctor_m29397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m29397_GenericMethod/* genericMethod */

};
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
static ParameterInfo UnityAction_1_t4876_UnityAction_1_Invoke_m29398_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &RuntimeAnimatorController_t543_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m29398_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m29398_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4876_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4876_UnityAction_1_Invoke_m29398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m29398_GenericMethod/* genericMethod */

};
extern Il2CppType RuntimeAnimatorController_t543_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4876_UnityAction_1_BeginInvoke_m29399_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &RuntimeAnimatorController_t543_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m29399_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m29399_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4876_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4876_UnityAction_1_BeginInvoke_m29399_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m29399_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4876_UnityAction_1_EndInvoke_m29400_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m29400_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.RuntimeAnimatorController>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m29400_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4876_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4876_UnityAction_1_EndInvoke_m29400_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m29400_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4876_MethodInfos[] =
{
	&UnityAction_1__ctor_m29397_MethodInfo,
	&UnityAction_1_Invoke_m29398_MethodInfo,
	&UnityAction_1_BeginInvoke_m29399_MethodInfo,
	&UnityAction_1_EndInvoke_m29400_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m29399_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m29400_MethodInfo;
static MethodInfo* UnityAction_1_t4876_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m29398_MethodInfo,
	&UnityAction_1_BeginInvoke_m29399_MethodInfo,
	&UnityAction_1_EndInvoke_m29400_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4876_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4876_1_0_0;
struct UnityAction_1_t4876;
extern Il2CppGenericClass UnityAction_1_t4876_GenericClass;
TypeInfo UnityAction_1_t4876_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4876_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4876_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4876_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4876_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4876_0_0_0/* byval_arg */
	, &UnityAction_1_t4876_1_0_0/* this_arg */
	, UnityAction_1_t4876_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4876_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4876)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6909_il2cpp_TypeInfo;

// UnityEngine.Terrain
#include "UnityEngine_UnityEngine_Terrain.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Terrain>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Terrain>
extern MethodInfo IEnumerator_1_get_Current_m49343_MethodInfo;
static PropertyInfo IEnumerator_1_t6909____Current_PropertyInfo = 
{
	&IEnumerator_1_t6909_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49343_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6909_PropertyInfos[] =
{
	&IEnumerator_1_t6909____Current_PropertyInfo,
	NULL
};
extern Il2CppType Terrain_t1077_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49343_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Terrain>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49343_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6909_il2cpp_TypeInfo/* declaring_type */
	, &Terrain_t1077_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49343_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6909_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49343_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6909_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6909_0_0_0;
extern Il2CppType IEnumerator_1_t6909_1_0_0;
struct IEnumerator_1_t6909;
extern Il2CppGenericClass IEnumerator_1_t6909_GenericClass;
TypeInfo IEnumerator_1_t6909_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6909_MethodInfos/* methods */
	, IEnumerator_1_t6909_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6909_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6909_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6909_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6909_0_0_0/* byval_arg */
	, &IEnumerator_1_t6909_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6909_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Terrain>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_467.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4877_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Terrain>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_467MethodDeclarations.h"

extern TypeInfo Terrain_t1077_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29405_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTerrain_t1077_m38739_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Terrain>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Terrain>(System.Int32)
#define Array_InternalArray__get_Item_TisTerrain_t1077_m38739(__this, p0, method) (Terrain_t1077 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Terrain>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Terrain>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Terrain>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Terrain>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Terrain>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Terrain>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4877____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4877_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4877, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4877____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4877_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4877, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4877_FieldInfos[] =
{
	&InternalEnumerator_1_t4877____array_0_FieldInfo,
	&InternalEnumerator_1_t4877____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29402_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4877____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4877_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29402_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4877____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4877_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29405_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4877_PropertyInfos[] =
{
	&InternalEnumerator_1_t4877____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4877____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4877_InternalEnumerator_1__ctor_m29401_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29401_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Terrain>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29401_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4877_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4877_InternalEnumerator_1__ctor_m29401_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29401_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29402_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Terrain>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29402_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4877_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29402_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29403_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Terrain>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29403_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4877_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29403_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29404_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Terrain>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29404_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4877_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29404_GenericMethod/* genericMethod */

};
extern Il2CppType Terrain_t1077_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29405_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Terrain>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29405_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4877_il2cpp_TypeInfo/* declaring_type */
	, &Terrain_t1077_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29405_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4877_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29401_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29402_MethodInfo,
	&InternalEnumerator_1_Dispose_m29403_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29404_MethodInfo,
	&InternalEnumerator_1_get_Current_m29405_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29404_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29403_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4877_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29402_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29404_MethodInfo,
	&InternalEnumerator_1_Dispose_m29403_MethodInfo,
	&InternalEnumerator_1_get_Current_m29405_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4877_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6909_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4877_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6909_il2cpp_TypeInfo, 7},
};
extern TypeInfo Terrain_t1077_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4877_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29405_MethodInfo/* Method Usage */,
	&Terrain_t1077_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTerrain_t1077_m38739_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4877_0_0_0;
extern Il2CppType InternalEnumerator_1_t4877_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4877_GenericClass;
TypeInfo InternalEnumerator_1_t4877_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4877_MethodInfos/* methods */
	, InternalEnumerator_1_t4877_PropertyInfos/* properties */
	, InternalEnumerator_1_t4877_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4877_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4877_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4877_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4877_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4877_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4877_1_0_0/* this_arg */
	, InternalEnumerator_1_t4877_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4877_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4877_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4877)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8769_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Terrain>
extern MethodInfo ICollection_1_get_Count_m49344_MethodInfo;
static PropertyInfo ICollection_1_t8769____Count_PropertyInfo = 
{
	&ICollection_1_t8769_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49344_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49345_MethodInfo;
static PropertyInfo ICollection_1_t8769____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8769_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49345_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8769_PropertyInfos[] =
{
	&ICollection_1_t8769____Count_PropertyInfo,
	&ICollection_1_t8769____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49344_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::get_Count()
MethodInfo ICollection_1_get_Count_m49344_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8769_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49344_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49345_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49345_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49345_GenericMethod/* genericMethod */

};
extern Il2CppType Terrain_t1077_0_0_0;
extern Il2CppType Terrain_t1077_0_0_0;
static ParameterInfo ICollection_1_t8769_ICollection_1_Add_m49346_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Terrain_t1077_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49346_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::Add(T)
MethodInfo ICollection_1_Add_m49346_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8769_ICollection_1_Add_m49346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49346_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49347_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::Clear()
MethodInfo ICollection_1_Clear_m49347_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49347_GenericMethod/* genericMethod */

};
extern Il2CppType Terrain_t1077_0_0_0;
static ParameterInfo ICollection_1_t8769_ICollection_1_Contains_m49348_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Terrain_t1077_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49348_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::Contains(T)
MethodInfo ICollection_1_Contains_m49348_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8769_ICollection_1_Contains_m49348_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49348_GenericMethod/* genericMethod */

};
extern Il2CppType TerrainU5BU5D_t5732_0_0_0;
extern Il2CppType TerrainU5BU5D_t5732_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8769_ICollection_1_CopyTo_m49349_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TerrainU5BU5D_t5732_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49349_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49349_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8769_ICollection_1_CopyTo_m49349_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49349_GenericMethod/* genericMethod */

};
extern Il2CppType Terrain_t1077_0_0_0;
static ParameterInfo ICollection_1_t8769_ICollection_1_Remove_m49350_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Terrain_t1077_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49350_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Terrain>::Remove(T)
MethodInfo ICollection_1_Remove_m49350_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8769_ICollection_1_Remove_m49350_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49350_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8769_MethodInfos[] =
{
	&ICollection_1_get_Count_m49344_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49345_MethodInfo,
	&ICollection_1_Add_m49346_MethodInfo,
	&ICollection_1_Clear_m49347_MethodInfo,
	&ICollection_1_Contains_m49348_MethodInfo,
	&ICollection_1_CopyTo_m49349_MethodInfo,
	&ICollection_1_Remove_m49350_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8771_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8769_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8771_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8769_0_0_0;
extern Il2CppType ICollection_1_t8769_1_0_0;
struct ICollection_1_t8769;
extern Il2CppGenericClass ICollection_1_t8769_GenericClass;
TypeInfo ICollection_1_t8769_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8769_MethodInfos/* methods */
	, ICollection_1_t8769_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8769_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8769_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8769_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8769_0_0_0/* byval_arg */
	, &ICollection_1_t8769_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8769_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Terrain>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Terrain>
extern Il2CppType IEnumerator_1_t6909_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49351_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Terrain>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49351_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8771_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6909_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49351_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8771_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49351_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8771_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8771_0_0_0;
extern Il2CppType IEnumerable_1_t8771_1_0_0;
struct IEnumerable_1_t8771;
extern Il2CppGenericClass IEnumerable_1_t8771_GenericClass;
TypeInfo IEnumerable_1_t8771_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8771_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8771_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8771_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8771_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8771_0_0_0/* byval_arg */
	, &IEnumerable_1_t8771_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8771_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8770_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Terrain>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Terrain>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Terrain>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Terrain>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Terrain>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Terrain>
extern MethodInfo IList_1_get_Item_m49352_MethodInfo;
extern MethodInfo IList_1_set_Item_m49353_MethodInfo;
static PropertyInfo IList_1_t8770____Item_PropertyInfo = 
{
	&IList_1_t8770_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49352_MethodInfo/* get */
	, &IList_1_set_Item_m49353_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8770_PropertyInfos[] =
{
	&IList_1_t8770____Item_PropertyInfo,
	NULL
};
extern Il2CppType Terrain_t1077_0_0_0;
static ParameterInfo IList_1_t8770_IList_1_IndexOf_m49354_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Terrain_t1077_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49354_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Terrain>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49354_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8770_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8770_IList_1_IndexOf_m49354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49354_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Terrain_t1077_0_0_0;
static ParameterInfo IList_1_t8770_IList_1_Insert_m49355_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Terrain_t1077_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49355_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Terrain>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49355_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8770_IList_1_Insert_m49355_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49355_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8770_IList_1_RemoveAt_m49356_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49356_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Terrain>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49356_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8770_IList_1_RemoveAt_m49356_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49356_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8770_IList_1_get_Item_m49352_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Terrain_t1077_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49352_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Terrain>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49352_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8770_il2cpp_TypeInfo/* declaring_type */
	, &Terrain_t1077_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8770_IList_1_get_Item_m49352_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49352_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Terrain_t1077_0_0_0;
static ParameterInfo IList_1_t8770_IList_1_set_Item_m49353_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Terrain_t1077_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49353_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Terrain>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49353_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8770_IList_1_set_Item_m49353_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49353_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8770_MethodInfos[] =
{
	&IList_1_IndexOf_m49354_MethodInfo,
	&IList_1_Insert_m49355_MethodInfo,
	&IList_1_RemoveAt_m49356_MethodInfo,
	&IList_1_get_Item_m49352_MethodInfo,
	&IList_1_set_Item_m49353_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8770_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8769_il2cpp_TypeInfo,
	&IEnumerable_1_t8771_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8770_0_0_0;
extern Il2CppType IList_1_t8770_1_0_0;
struct IList_1_t8770;
extern Il2CppGenericClass IList_1_t8770_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8770_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8770_MethodInfos/* methods */
	, IList_1_t8770_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8770_il2cpp_TypeInfo/* element_class */
	, IList_1_t8770_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8770_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8770_0_0_0/* byval_arg */
	, &IList_1_t8770_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8770_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Terrain>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_176.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4878_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Terrain>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_176MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_178.h"
extern TypeInfo InvokableCall_1_t4879_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_178MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m29408_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m29410_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Terrain>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Terrain>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Terrain>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4878____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4878_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4878, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4878_FieldInfos[] =
{
	&CachedInvokableCall_1_t4878____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Terrain_t1077_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4878_CachedInvokableCall_1__ctor_m29406_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Terrain_t1077_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m29406_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Terrain>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m29406_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4878_CachedInvokableCall_1__ctor_m29406_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m29406_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4878_CachedInvokableCall_1_Invoke_m29407_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m29407_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Terrain>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m29407_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4878_CachedInvokableCall_1_Invoke_m29407_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m29407_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4878_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m29406_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29407_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m29407_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m29411_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4878_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29407_MethodInfo,
	&InvokableCall_1_Find_m29411_MethodInfo,
};
extern Il2CppType UnityAction_1_t4880_0_0_0;
extern TypeInfo UnityAction_1_t4880_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTerrain_t1077_m38749_MethodInfo;
extern TypeInfo Terrain_t1077_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m29413_MethodInfo;
extern TypeInfo Terrain_t1077_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4878_RGCTXData[8] = 
{
	&UnityAction_1_t4880_0_0_0/* Type Usage */,
	&UnityAction_1_t4880_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTerrain_t1077_m38749_MethodInfo/* Method Usage */,
	&Terrain_t1077_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29413_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m29408_MethodInfo/* Method Usage */,
	&Terrain_t1077_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m29410_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4878_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4878_1_0_0;
struct CachedInvokableCall_1_t4878;
extern Il2CppGenericClass CachedInvokableCall_1_t4878_GenericClass;
TypeInfo CachedInvokableCall_1_t4878_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4878_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4878_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4879_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4878_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4878_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4878_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4878_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4878_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4878_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4878_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4878)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_182.h"
extern TypeInfo UnityAction_1_t4880_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_182MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Terrain>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Terrain>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisTerrain_t1077_m38749(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>
extern Il2CppType UnityAction_1_t4880_0_0_1;
FieldInfo InvokableCall_1_t4879____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4880_0_0_1/* type */
	, &InvokableCall_1_t4879_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4879, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4879_FieldInfos[] =
{
	&InvokableCall_1_t4879____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4879_InvokableCall_1__ctor_m29408_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29408_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m29408_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4879_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4879_InvokableCall_1__ctor_m29408_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29408_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4880_0_0_0;
static ParameterInfo InvokableCall_1_t4879_InvokableCall_1__ctor_m29409_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4880_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29409_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m29409_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4879_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4879_InvokableCall_1__ctor_m29409_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29409_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4879_InvokableCall_1_Invoke_m29410_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m29410_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m29410_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4879_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4879_InvokableCall_1_Invoke_m29410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m29410_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4879_InvokableCall_1_Find_m29411_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m29411_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m29411_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4879_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4879_InvokableCall_1_Find_m29411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m29411_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4879_MethodInfos[] =
{
	&InvokableCall_1__ctor_m29408_MethodInfo,
	&InvokableCall_1__ctor_m29409_MethodInfo,
	&InvokableCall_1_Invoke_m29410_MethodInfo,
	&InvokableCall_1_Find_m29411_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4879_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m29410_MethodInfo,
	&InvokableCall_1_Find_m29411_MethodInfo,
};
extern TypeInfo UnityAction_1_t4880_il2cpp_TypeInfo;
extern TypeInfo Terrain_t1077_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4879_RGCTXData[5] = 
{
	&UnityAction_1_t4880_0_0_0/* Type Usage */,
	&UnityAction_1_t4880_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTerrain_t1077_m38749_MethodInfo/* Method Usage */,
	&Terrain_t1077_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29413_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4879_0_0_0;
extern Il2CppType InvokableCall_1_t4879_1_0_0;
struct InvokableCall_1_t4879;
extern Il2CppGenericClass InvokableCall_1_t4879_GenericClass;
TypeInfo InvokableCall_1_t4879_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4879_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4879_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4879_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4879_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4879_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4879_0_0_0/* byval_arg */
	, &InvokableCall_1_t4879_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4879_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4879_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4879)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4880_UnityAction_1__ctor_m29412_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m29412_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m29412_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4880_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4880_UnityAction_1__ctor_m29412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m29412_GenericMethod/* genericMethod */

};
extern Il2CppType Terrain_t1077_0_0_0;
static ParameterInfo UnityAction_1_t4880_UnityAction_1_Invoke_m29413_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Terrain_t1077_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m29413_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m29413_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4880_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4880_UnityAction_1_Invoke_m29413_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m29413_GenericMethod/* genericMethod */

};
extern Il2CppType Terrain_t1077_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4880_UnityAction_1_BeginInvoke_m29414_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Terrain_t1077_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m29414_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m29414_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4880_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4880_UnityAction_1_BeginInvoke_m29414_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m29414_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4880_UnityAction_1_EndInvoke_m29415_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m29415_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Terrain>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m29415_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4880_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4880_UnityAction_1_EndInvoke_m29415_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m29415_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4880_MethodInfos[] =
{
	&UnityAction_1__ctor_m29412_MethodInfo,
	&UnityAction_1_Invoke_m29413_MethodInfo,
	&UnityAction_1_BeginInvoke_m29414_MethodInfo,
	&UnityAction_1_EndInvoke_m29415_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m29414_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m29415_MethodInfo;
static MethodInfo* UnityAction_1_t4880_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m29413_MethodInfo,
	&UnityAction_1_BeginInvoke_m29414_MethodInfo,
	&UnityAction_1_EndInvoke_m29415_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4880_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4880_1_0_0;
struct UnityAction_1_t4880;
extern Il2CppGenericClass UnityAction_1_t4880_GenericClass;
TypeInfo UnityAction_1_t4880_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4880_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4880_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4880_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4880_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4880_0_0_0/* byval_arg */
	, &UnityAction_1_t4880_1_0_0/* this_arg */
	, UnityAction_1_t4880_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4880_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4880)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6910_il2cpp_TypeInfo;

// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.TextAnchor>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TextAnchor>
extern MethodInfo IEnumerator_1_get_Current_m49357_MethodInfo;
static PropertyInfo IEnumerator_1_t6910____Current_PropertyInfo = 
{
	&IEnumerator_1_t6910_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49357_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6910_PropertyInfos[] =
{
	&IEnumerator_1_t6910____Current_PropertyInfo,
	NULL
};
extern Il2CppType TextAnchor_t503_0_0_0;
extern void* RuntimeInvoker_TextAnchor_t503 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49357_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.TextAnchor>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49357_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6910_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t503_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t503/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49357_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6910_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49357_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6910_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6910_0_0_0;
extern Il2CppType IEnumerator_1_t6910_1_0_0;
struct IEnumerator_1_t6910;
extern Il2CppGenericClass IEnumerator_1_t6910_GenericClass;
TypeInfo IEnumerator_1_t6910_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6910_MethodInfos/* methods */
	, IEnumerator_1_t6910_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6910_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6910_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6910_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6910_0_0_0/* byval_arg */
	, &IEnumerator_1_t6910_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6910_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_468.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4881_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_468MethodDeclarations.h"

extern TypeInfo TextAnchor_t503_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29420_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTextAnchor_t503_m38751_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.TextAnchor>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.TextAnchor>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTextAnchor_t503_m38751 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29416_MethodInfo;
 void InternalEnumerator_1__ctor_m29416 (InternalEnumerator_1_t4881 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29417_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29417 (InternalEnumerator_1_t4881 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29420(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29420_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TextAnchor_t503_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29418_MethodInfo;
 void InternalEnumerator_1_Dispose_m29418 (InternalEnumerator_1_t4881 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29419_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29419 (InternalEnumerator_1_t4881 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29420 (InternalEnumerator_1_t4881 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTextAnchor_t503_m38751(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTextAnchor_t503_m38751_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4881____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4881_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4881, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4881____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4881_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4881, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4881_FieldInfos[] =
{
	&InternalEnumerator_1_t4881____array_0_FieldInfo,
	&InternalEnumerator_1_t4881____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4881____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4881_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29417_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4881____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4881_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29420_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4881_PropertyInfos[] =
{
	&InternalEnumerator_1_t4881____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4881____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4881_InternalEnumerator_1__ctor_m29416_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29416_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29416_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29416/* method */
	, &InternalEnumerator_1_t4881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4881_InternalEnumerator_1__ctor_m29416_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29416_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29417_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29417_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29417/* method */
	, &InternalEnumerator_1_t4881_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29417_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29418_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29418_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29418/* method */
	, &InternalEnumerator_1_t4881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29418_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29419_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29419_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29419/* method */
	, &InternalEnumerator_1_t4881_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29419_GenericMethod/* genericMethod */

};
extern Il2CppType TextAnchor_t503_0_0_0;
extern void* RuntimeInvoker_TextAnchor_t503 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29420_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.TextAnchor>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29420_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29420/* method */
	, &InternalEnumerator_1_t4881_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t503_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t503/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29420_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4881_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29416_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29417_MethodInfo,
	&InternalEnumerator_1_Dispose_m29418_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29419_MethodInfo,
	&InternalEnumerator_1_get_Current_m29420_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4881_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29417_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29419_MethodInfo,
	&InternalEnumerator_1_Dispose_m29418_MethodInfo,
	&InternalEnumerator_1_get_Current_m29420_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4881_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6910_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4881_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6910_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4881_0_0_0;
extern Il2CppType InternalEnumerator_1_t4881_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4881_GenericClass;
TypeInfo InternalEnumerator_1_t4881_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4881_MethodInfos/* methods */
	, InternalEnumerator_1_t4881_PropertyInfos/* properties */
	, InternalEnumerator_1_t4881_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4881_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4881_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4881_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4881_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4881_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4881_1_0_0/* this_arg */
	, InternalEnumerator_1_t4881_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4881_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4881)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8772_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>
extern MethodInfo ICollection_1_get_Count_m49358_MethodInfo;
static PropertyInfo ICollection_1_t8772____Count_PropertyInfo = 
{
	&ICollection_1_t8772_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49358_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49359_MethodInfo;
static PropertyInfo ICollection_1_t8772____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8772_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49359_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8772_PropertyInfos[] =
{
	&ICollection_1_t8772____Count_PropertyInfo,
	&ICollection_1_t8772____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49358_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::get_Count()
MethodInfo ICollection_1_get_Count_m49358_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8772_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49358_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49359_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49359_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8772_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49359_GenericMethod/* genericMethod */

};
extern Il2CppType TextAnchor_t503_0_0_0;
extern Il2CppType TextAnchor_t503_0_0_0;
static ParameterInfo ICollection_1_t8772_ICollection_1_Add_m49360_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextAnchor_t503_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49360_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::Add(T)
MethodInfo ICollection_1_Add_m49360_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8772_ICollection_1_Add_m49360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49360_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49361_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::Clear()
MethodInfo ICollection_1_Clear_m49361_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49361_GenericMethod/* genericMethod */

};
extern Il2CppType TextAnchor_t503_0_0_0;
static ParameterInfo ICollection_1_t8772_ICollection_1_Contains_m49362_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextAnchor_t503_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49362_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::Contains(T)
MethodInfo ICollection_1_Contains_m49362_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8772_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8772_ICollection_1_Contains_m49362_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49362_GenericMethod/* genericMethod */

};
extern Il2CppType TextAnchorU5BU5D_t5733_0_0_0;
extern Il2CppType TextAnchorU5BU5D_t5733_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8772_ICollection_1_CopyTo_m49363_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TextAnchorU5BU5D_t5733_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49363_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49363_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8772_ICollection_1_CopyTo_m49363_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49363_GenericMethod/* genericMethod */

};
extern Il2CppType TextAnchor_t503_0_0_0;
static ParameterInfo ICollection_1_t8772_ICollection_1_Remove_m49364_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextAnchor_t503_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49364_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TextAnchor>::Remove(T)
MethodInfo ICollection_1_Remove_m49364_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8772_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8772_ICollection_1_Remove_m49364_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49364_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8772_MethodInfos[] =
{
	&ICollection_1_get_Count_m49358_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49359_MethodInfo,
	&ICollection_1_Add_m49360_MethodInfo,
	&ICollection_1_Clear_m49361_MethodInfo,
	&ICollection_1_Contains_m49362_MethodInfo,
	&ICollection_1_CopyTo_m49363_MethodInfo,
	&ICollection_1_Remove_m49364_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8774_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8772_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8774_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8772_0_0_0;
extern Il2CppType ICollection_1_t8772_1_0_0;
struct ICollection_1_t8772;
extern Il2CppGenericClass ICollection_1_t8772_GenericClass;
TypeInfo ICollection_1_t8772_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8772_MethodInfos/* methods */
	, ICollection_1_t8772_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8772_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8772_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8772_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8772_0_0_0/* byval_arg */
	, &ICollection_1_t8772_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8772_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TextAnchor>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TextAnchor>
extern Il2CppType IEnumerator_1_t6910_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49365_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TextAnchor>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49365_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8774_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6910_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49365_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8774_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49365_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8774_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8774_0_0_0;
extern Il2CppType IEnumerable_1_t8774_1_0_0;
struct IEnumerable_1_t8774;
extern Il2CppGenericClass IEnumerable_1_t8774_GenericClass;
TypeInfo IEnumerable_1_t8774_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8774_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8774_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8774_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8774_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8774_0_0_0/* byval_arg */
	, &IEnumerable_1_t8774_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8774_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8773_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TextAnchor>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAnchor>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAnchor>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.TextAnchor>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAnchor>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TextAnchor>
extern MethodInfo IList_1_get_Item_m49366_MethodInfo;
extern MethodInfo IList_1_set_Item_m49367_MethodInfo;
static PropertyInfo IList_1_t8773____Item_PropertyInfo = 
{
	&IList_1_t8773_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49366_MethodInfo/* get */
	, &IList_1_set_Item_m49367_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8773_PropertyInfos[] =
{
	&IList_1_t8773____Item_PropertyInfo,
	NULL
};
extern Il2CppType TextAnchor_t503_0_0_0;
static ParameterInfo IList_1_t8773_IList_1_IndexOf_m49368_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextAnchor_t503_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49368_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TextAnchor>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49368_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8773_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8773_IList_1_IndexOf_m49368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49368_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextAnchor_t503_0_0_0;
static ParameterInfo IList_1_t8773_IList_1_Insert_m49369_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TextAnchor_t503_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49369_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAnchor>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49369_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8773_IList_1_Insert_m49369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49369_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8773_IList_1_RemoveAt_m49370_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49370_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAnchor>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49370_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8773_IList_1_RemoveAt_m49370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49370_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8773_IList_1_get_Item_m49366_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TextAnchor_t503_0_0_0;
extern void* RuntimeInvoker_TextAnchor_t503_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49366_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.TextAnchor>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49366_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8773_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t503_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t503_Int32_t123/* invoker_method */
	, IList_1_t8773_IList_1_get_Item_m49366_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49366_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TextAnchor_t503_0_0_0;
static ParameterInfo IList_1_t8773_IList_1_set_Item_m49367_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TextAnchor_t503_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49367_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TextAnchor>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49367_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8773_IList_1_set_Item_m49367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49367_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8773_MethodInfos[] =
{
	&IList_1_IndexOf_m49368_MethodInfo,
	&IList_1_Insert_m49369_MethodInfo,
	&IList_1_RemoveAt_m49370_MethodInfo,
	&IList_1_get_Item_m49366_MethodInfo,
	&IList_1_set_Item_m49367_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8773_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8772_il2cpp_TypeInfo,
	&IEnumerable_1_t8774_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8773_0_0_0;
extern Il2CppType IList_1_t8773_1_0_0;
struct IList_1_t8773;
extern Il2CppGenericClass IList_1_t8773_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8773_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8773_MethodInfos/* methods */
	, IList_1_t8773_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8773_il2cpp_TypeInfo/* element_class */
	, IList_1_t8773_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8773_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8773_0_0_0/* byval_arg */
	, &IList_1_t8773_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8773_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6912_il2cpp_TypeInfo;

// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.HorizontalWrapMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.HorizontalWrapMode>
extern MethodInfo IEnumerator_1_get_Current_m49371_MethodInfo;
static PropertyInfo IEnumerator_1_t6912____Current_PropertyInfo = 
{
	&IEnumerator_1_t6912_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49371_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6912_PropertyInfos[] =
{
	&IEnumerator_1_t6912____Current_PropertyInfo,
	NULL
};
extern Il2CppType HorizontalWrapMode_t504_0_0_0;
extern void* RuntimeInvoker_HorizontalWrapMode_t504 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49371_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.HorizontalWrapMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49371_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6912_il2cpp_TypeInfo/* declaring_type */
	, &HorizontalWrapMode_t504_0_0_0/* return_type */
	, RuntimeInvoker_HorizontalWrapMode_t504/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49371_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6912_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49371_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6912_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6912_0_0_0;
extern Il2CppType IEnumerator_1_t6912_1_0_0;
struct IEnumerator_1_t6912;
extern Il2CppGenericClass IEnumerator_1_t6912_GenericClass;
TypeInfo IEnumerator_1_t6912_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6912_MethodInfos/* methods */
	, IEnumerator_1_t6912_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6912_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6912_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6912_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6912_0_0_0/* byval_arg */
	, &IEnumerator_1_t6912_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6912_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_469.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4882_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_469MethodDeclarations.h"

extern TypeInfo HorizontalWrapMode_t504_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29425_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHorizontalWrapMode_t504_m38762_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.HorizontalWrapMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.HorizontalWrapMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisHorizontalWrapMode_t504_m38762 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29421_MethodInfo;
 void InternalEnumerator_1__ctor_m29421 (InternalEnumerator_1_t4882 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29422_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29422 (InternalEnumerator_1_t4882 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29425(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29425_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&HorizontalWrapMode_t504_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29423_MethodInfo;
 void InternalEnumerator_1_Dispose_m29423 (InternalEnumerator_1_t4882 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29424_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29424 (InternalEnumerator_1_t4882 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29425 (InternalEnumerator_1_t4882 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisHorizontalWrapMode_t504_m38762(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisHorizontalWrapMode_t504_m38762_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4882____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4882_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4882, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4882____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4882_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4882, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4882_FieldInfos[] =
{
	&InternalEnumerator_1_t4882____array_0_FieldInfo,
	&InternalEnumerator_1_t4882____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4882____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4882_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29422_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4882____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4882_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29425_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4882_PropertyInfos[] =
{
	&InternalEnumerator_1_t4882____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4882____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4882_InternalEnumerator_1__ctor_m29421_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29421_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29421_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29421/* method */
	, &InternalEnumerator_1_t4882_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4882_InternalEnumerator_1__ctor_m29421_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29421_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29422_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29422_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29422/* method */
	, &InternalEnumerator_1_t4882_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29422_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29423_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29423_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29423/* method */
	, &InternalEnumerator_1_t4882_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29423_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29424_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29424_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29424/* method */
	, &InternalEnumerator_1_t4882_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29424_GenericMethod/* genericMethod */

};
extern Il2CppType HorizontalWrapMode_t504_0_0_0;
extern void* RuntimeInvoker_HorizontalWrapMode_t504 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29425_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29425_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29425/* method */
	, &InternalEnumerator_1_t4882_il2cpp_TypeInfo/* declaring_type */
	, &HorizontalWrapMode_t504_0_0_0/* return_type */
	, RuntimeInvoker_HorizontalWrapMode_t504/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29425_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4882_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29421_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29422_MethodInfo,
	&InternalEnumerator_1_Dispose_m29423_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29424_MethodInfo,
	&InternalEnumerator_1_get_Current_m29425_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4882_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29422_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29424_MethodInfo,
	&InternalEnumerator_1_Dispose_m29423_MethodInfo,
	&InternalEnumerator_1_get_Current_m29425_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4882_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6912_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4882_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6912_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4882_0_0_0;
extern Il2CppType InternalEnumerator_1_t4882_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4882_GenericClass;
TypeInfo InternalEnumerator_1_t4882_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4882_MethodInfos/* methods */
	, InternalEnumerator_1_t4882_PropertyInfos/* properties */
	, InternalEnumerator_1_t4882_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4882_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4882_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4882_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4882_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4882_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4882_1_0_0/* this_arg */
	, InternalEnumerator_1_t4882_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4882_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4882)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8775_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>
extern MethodInfo ICollection_1_get_Count_m49372_MethodInfo;
static PropertyInfo ICollection_1_t8775____Count_PropertyInfo = 
{
	&ICollection_1_t8775_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49372_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49373_MethodInfo;
static PropertyInfo ICollection_1_t8775____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8775_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49373_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8775_PropertyInfos[] =
{
	&ICollection_1_t8775____Count_PropertyInfo,
	&ICollection_1_t8775____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49372_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::get_Count()
MethodInfo ICollection_1_get_Count_m49372_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8775_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49372_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49373_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49373_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49373_GenericMethod/* genericMethod */

};
extern Il2CppType HorizontalWrapMode_t504_0_0_0;
extern Il2CppType HorizontalWrapMode_t504_0_0_0;
static ParameterInfo ICollection_1_t8775_ICollection_1_Add_m49374_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HorizontalWrapMode_t504_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49374_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::Add(T)
MethodInfo ICollection_1_Add_m49374_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8775_ICollection_1_Add_m49374_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49374_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49375_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::Clear()
MethodInfo ICollection_1_Clear_m49375_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49375_GenericMethod/* genericMethod */

};
extern Il2CppType HorizontalWrapMode_t504_0_0_0;
static ParameterInfo ICollection_1_t8775_ICollection_1_Contains_m49376_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HorizontalWrapMode_t504_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49376_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::Contains(T)
MethodInfo ICollection_1_Contains_m49376_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8775_ICollection_1_Contains_m49376_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49376_GenericMethod/* genericMethod */

};
extern Il2CppType HorizontalWrapModeU5BU5D_t5734_0_0_0;
extern Il2CppType HorizontalWrapModeU5BU5D_t5734_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8775_ICollection_1_CopyTo_m49377_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HorizontalWrapModeU5BU5D_t5734_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49377_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49377_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8775_ICollection_1_CopyTo_m49377_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49377_GenericMethod/* genericMethod */

};
extern Il2CppType HorizontalWrapMode_t504_0_0_0;
static ParameterInfo ICollection_1_t8775_ICollection_1_Remove_m49378_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HorizontalWrapMode_t504_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49378_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HorizontalWrapMode>::Remove(T)
MethodInfo ICollection_1_Remove_m49378_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8775_ICollection_1_Remove_m49378_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49378_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8775_MethodInfos[] =
{
	&ICollection_1_get_Count_m49372_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49373_MethodInfo,
	&ICollection_1_Add_m49374_MethodInfo,
	&ICollection_1_Clear_m49375_MethodInfo,
	&ICollection_1_Contains_m49376_MethodInfo,
	&ICollection_1_CopyTo_m49377_MethodInfo,
	&ICollection_1_Remove_m49378_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8777_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8775_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8777_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8775_0_0_0;
extern Il2CppType ICollection_1_t8775_1_0_0;
struct ICollection_1_t8775;
extern Il2CppGenericClass ICollection_1_t8775_GenericClass;
TypeInfo ICollection_1_t8775_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8775_MethodInfos/* methods */
	, ICollection_1_t8775_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8775_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8775_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8775_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8775_0_0_0/* byval_arg */
	, &ICollection_1_t8775_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8775_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.HorizontalWrapMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.HorizontalWrapMode>
extern Il2CppType IEnumerator_1_t6912_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49379_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.HorizontalWrapMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49379_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8777_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6912_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49379_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8777_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49379_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8777_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8777_0_0_0;
extern Il2CppType IEnumerable_1_t8777_1_0_0;
struct IEnumerable_1_t8777;
extern Il2CppGenericClass IEnumerable_1_t8777_GenericClass;
TypeInfo IEnumerable_1_t8777_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8777_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8777_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8777_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8777_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8777_0_0_0/* byval_arg */
	, &IEnumerable_1_t8777_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8777_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8776_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>
extern MethodInfo IList_1_get_Item_m49380_MethodInfo;
extern MethodInfo IList_1_set_Item_m49381_MethodInfo;
static PropertyInfo IList_1_t8776____Item_PropertyInfo = 
{
	&IList_1_t8776_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49380_MethodInfo/* get */
	, &IList_1_set_Item_m49381_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8776_PropertyInfos[] =
{
	&IList_1_t8776____Item_PropertyInfo,
	NULL
};
extern Il2CppType HorizontalWrapMode_t504_0_0_0;
static ParameterInfo IList_1_t8776_IList_1_IndexOf_m49382_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HorizontalWrapMode_t504_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49382_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49382_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8776_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8776_IList_1_IndexOf_m49382_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49382_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HorizontalWrapMode_t504_0_0_0;
static ParameterInfo IList_1_t8776_IList_1_Insert_m49383_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HorizontalWrapMode_t504_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49383_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49383_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8776_IList_1_Insert_m49383_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49383_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8776_IList_1_RemoveAt_m49384_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49384_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49384_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8776_IList_1_RemoveAt_m49384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49384_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8776_IList_1_get_Item_m49380_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType HorizontalWrapMode_t504_0_0_0;
extern void* RuntimeInvoker_HorizontalWrapMode_t504_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49380_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49380_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8776_il2cpp_TypeInfo/* declaring_type */
	, &HorizontalWrapMode_t504_0_0_0/* return_type */
	, RuntimeInvoker_HorizontalWrapMode_t504_Int32_t123/* invoker_method */
	, IList_1_t8776_IList_1_get_Item_m49380_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49380_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HorizontalWrapMode_t504_0_0_0;
static ParameterInfo IList_1_t8776_IList_1_set_Item_m49381_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HorizontalWrapMode_t504_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49381_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.HorizontalWrapMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49381_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8776_IList_1_set_Item_m49381_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49381_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8776_MethodInfos[] =
{
	&IList_1_IndexOf_m49382_MethodInfo,
	&IList_1_Insert_m49383_MethodInfo,
	&IList_1_RemoveAt_m49384_MethodInfo,
	&IList_1_get_Item_m49380_MethodInfo,
	&IList_1_set_Item_m49381_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8776_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8775_il2cpp_TypeInfo,
	&IEnumerable_1_t8777_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8776_0_0_0;
extern Il2CppType IList_1_t8776_1_0_0;
struct IList_1_t8776;
extern Il2CppGenericClass IList_1_t8776_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8776_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8776_MethodInfos/* methods */
	, IList_1_t8776_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8776_il2cpp_TypeInfo/* element_class */
	, IList_1_t8776_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8776_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8776_0_0_0/* byval_arg */
	, &IList_1_t8776_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8776_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6914_il2cpp_TypeInfo;

// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.VerticalWrapMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.VerticalWrapMode>
extern MethodInfo IEnumerator_1_get_Current_m49385_MethodInfo;
static PropertyInfo IEnumerator_1_t6914____Current_PropertyInfo = 
{
	&IEnumerator_1_t6914_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49385_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6914_PropertyInfos[] =
{
	&IEnumerator_1_t6914____Current_PropertyInfo,
	NULL
};
extern Il2CppType VerticalWrapMode_t505_0_0_0;
extern void* RuntimeInvoker_VerticalWrapMode_t505 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49385_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.VerticalWrapMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49385_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6914_il2cpp_TypeInfo/* declaring_type */
	, &VerticalWrapMode_t505_0_0_0/* return_type */
	, RuntimeInvoker_VerticalWrapMode_t505/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49385_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6914_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49385_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6914_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6914_0_0_0;
extern Il2CppType IEnumerator_1_t6914_1_0_0;
struct IEnumerator_1_t6914;
extern Il2CppGenericClass IEnumerator_1_t6914_GenericClass;
TypeInfo IEnumerator_1_t6914_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6914_MethodInfos/* methods */
	, IEnumerator_1_t6914_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6914_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6914_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6914_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6914_0_0_0/* byval_arg */
	, &IEnumerator_1_t6914_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6914_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_470.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4883_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_470MethodDeclarations.h"

extern TypeInfo VerticalWrapMode_t505_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29430_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVerticalWrapMode_t505_m38773_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.VerticalWrapMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.VerticalWrapMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisVerticalWrapMode_t505_m38773 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29426_MethodInfo;
 void InternalEnumerator_1__ctor_m29426 (InternalEnumerator_1_t4883 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427 (InternalEnumerator_1_t4883 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29430(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29430_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&VerticalWrapMode_t505_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29428_MethodInfo;
 void InternalEnumerator_1_Dispose_m29428 (InternalEnumerator_1_t4883 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29429_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29429 (InternalEnumerator_1_t4883 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29430 (InternalEnumerator_1_t4883 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisVerticalWrapMode_t505_m38773(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisVerticalWrapMode_t505_m38773_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4883____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4883_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4883, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4883____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4883_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4883, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4883_FieldInfos[] =
{
	&InternalEnumerator_1_t4883____array_0_FieldInfo,
	&InternalEnumerator_1_t4883____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4883____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4883_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4883____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4883_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29430_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4883_PropertyInfos[] =
{
	&InternalEnumerator_1_t4883____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4883____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4883_InternalEnumerator_1__ctor_m29426_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29426_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29426_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29426/* method */
	, &InternalEnumerator_1_t4883_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4883_InternalEnumerator_1__ctor_m29426_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29426_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427/* method */
	, &InternalEnumerator_1_t4883_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29428_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29428_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29428/* method */
	, &InternalEnumerator_1_t4883_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29428_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29429_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29429_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29429/* method */
	, &InternalEnumerator_1_t4883_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29429_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalWrapMode_t505_0_0_0;
extern void* RuntimeInvoker_VerticalWrapMode_t505 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29430_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.VerticalWrapMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29430_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29430/* method */
	, &InternalEnumerator_1_t4883_il2cpp_TypeInfo/* declaring_type */
	, &VerticalWrapMode_t505_0_0_0/* return_type */
	, RuntimeInvoker_VerticalWrapMode_t505/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29430_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4883_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29426_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_MethodInfo,
	&InternalEnumerator_1_Dispose_m29428_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29429_MethodInfo,
	&InternalEnumerator_1_get_Current_m29430_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4883_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29427_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29429_MethodInfo,
	&InternalEnumerator_1_Dispose_m29428_MethodInfo,
	&InternalEnumerator_1_get_Current_m29430_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4883_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6914_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4883_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6914_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4883_0_0_0;
extern Il2CppType InternalEnumerator_1_t4883_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4883_GenericClass;
TypeInfo InternalEnumerator_1_t4883_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4883_MethodInfos/* methods */
	, InternalEnumerator_1_t4883_PropertyInfos/* properties */
	, InternalEnumerator_1_t4883_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4883_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4883_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4883_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4883_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4883_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4883_1_0_0/* this_arg */
	, InternalEnumerator_1_t4883_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4883_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4883)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8778_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>
extern MethodInfo ICollection_1_get_Count_m49386_MethodInfo;
static PropertyInfo ICollection_1_t8778____Count_PropertyInfo = 
{
	&ICollection_1_t8778_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49386_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49387_MethodInfo;
static PropertyInfo ICollection_1_t8778____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8778_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49387_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8778_PropertyInfos[] =
{
	&ICollection_1_t8778____Count_PropertyInfo,
	&ICollection_1_t8778____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49386_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::get_Count()
MethodInfo ICollection_1_get_Count_m49386_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8778_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49386_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49387_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49387_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8778_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49387_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalWrapMode_t505_0_0_0;
extern Il2CppType VerticalWrapMode_t505_0_0_0;
static ParameterInfo ICollection_1_t8778_ICollection_1_Add_m49388_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VerticalWrapMode_t505_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49388_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::Add(T)
MethodInfo ICollection_1_Add_m49388_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t8778_ICollection_1_Add_m49388_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49388_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49389_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::Clear()
MethodInfo ICollection_1_Clear_m49389_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49389_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalWrapMode_t505_0_0_0;
static ParameterInfo ICollection_1_t8778_ICollection_1_Contains_m49390_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VerticalWrapMode_t505_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49390_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::Contains(T)
MethodInfo ICollection_1_Contains_m49390_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8778_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8778_ICollection_1_Contains_m49390_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49390_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalWrapModeU5BU5D_t5735_0_0_0;
extern Il2CppType VerticalWrapModeU5BU5D_t5735_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8778_ICollection_1_CopyTo_m49391_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VerticalWrapModeU5BU5D_t5735_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49391_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49391_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8778_ICollection_1_CopyTo_m49391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49391_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalWrapMode_t505_0_0_0;
static ParameterInfo ICollection_1_t8778_ICollection_1_Remove_m49392_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VerticalWrapMode_t505_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49392_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.VerticalWrapMode>::Remove(T)
MethodInfo ICollection_1_Remove_m49392_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8778_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t8778_ICollection_1_Remove_m49392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49392_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8778_MethodInfos[] =
{
	&ICollection_1_get_Count_m49386_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49387_MethodInfo,
	&ICollection_1_Add_m49388_MethodInfo,
	&ICollection_1_Clear_m49389_MethodInfo,
	&ICollection_1_Contains_m49390_MethodInfo,
	&ICollection_1_CopyTo_m49391_MethodInfo,
	&ICollection_1_Remove_m49392_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8780_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8778_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8780_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8778_0_0_0;
extern Il2CppType ICollection_1_t8778_1_0_0;
struct ICollection_1_t8778;
extern Il2CppGenericClass ICollection_1_t8778_GenericClass;
TypeInfo ICollection_1_t8778_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8778_MethodInfos/* methods */
	, ICollection_1_t8778_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8778_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8778_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8778_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8778_0_0_0/* byval_arg */
	, &ICollection_1_t8778_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8778_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.VerticalWrapMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.VerticalWrapMode>
extern Il2CppType IEnumerator_1_t6914_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49393_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.VerticalWrapMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49393_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8780_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6914_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49393_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8780_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49393_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8780_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8780_0_0_0;
extern Il2CppType IEnumerable_1_t8780_1_0_0;
struct IEnumerable_1_t8780;
extern Il2CppGenericClass IEnumerable_1_t8780_GenericClass;
TypeInfo IEnumerable_1_t8780_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8780_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8780_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8780_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8780_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8780_0_0_0/* byval_arg */
	, &IEnumerable_1_t8780_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8780_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8779_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>
extern MethodInfo IList_1_get_Item_m49394_MethodInfo;
extern MethodInfo IList_1_set_Item_m49395_MethodInfo;
static PropertyInfo IList_1_t8779____Item_PropertyInfo = 
{
	&IList_1_t8779_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49394_MethodInfo/* get */
	, &IList_1_set_Item_m49395_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8779_PropertyInfos[] =
{
	&IList_1_t8779____Item_PropertyInfo,
	NULL
};
extern Il2CppType VerticalWrapMode_t505_0_0_0;
static ParameterInfo IList_1_t8779_IList_1_IndexOf_m49396_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VerticalWrapMode_t505_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49396_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49396_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8779_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8779_IList_1_IndexOf_m49396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49396_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VerticalWrapMode_t505_0_0_0;
static ParameterInfo IList_1_t8779_IList_1_Insert_m49397_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VerticalWrapMode_t505_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49397_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49397_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8779_IList_1_Insert_m49397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49397_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8779_IList_1_RemoveAt_m49398_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49398_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49398_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8779_IList_1_RemoveAt_m49398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49398_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8779_IList_1_get_Item_m49394_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType VerticalWrapMode_t505_0_0_0;
extern void* RuntimeInvoker_VerticalWrapMode_t505_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49394_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49394_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8779_il2cpp_TypeInfo/* declaring_type */
	, &VerticalWrapMode_t505_0_0_0/* return_type */
	, RuntimeInvoker_VerticalWrapMode_t505_Int32_t123/* invoker_method */
	, IList_1_t8779_IList_1_get_Item_m49394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49394_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VerticalWrapMode_t505_0_0_0;
static ParameterInfo IList_1_t8779_IList_1_set_Item_m49395_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VerticalWrapMode_t505_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49395_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.VerticalWrapMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49395_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t8779_IList_1_set_Item_m49395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49395_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8779_MethodInfos[] =
{
	&IList_1_IndexOf_m49396_MethodInfo,
	&IList_1_Insert_m49397_MethodInfo,
	&IList_1_RemoveAt_m49398_MethodInfo,
	&IList_1_get_Item_m49394_MethodInfo,
	&IList_1_set_Item_m49395_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8779_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8778_il2cpp_TypeInfo,
	&IEnumerable_1_t8780_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8779_0_0_0;
extern Il2CppType IList_1_t8779_1_0_0;
struct IList_1_t8779;
extern Il2CppGenericClass IList_1_t8779_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8779_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8779_MethodInfos/* methods */
	, IList_1_t8779_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8779_il2cpp_TypeInfo/* element_class */
	, IList_1_t8779_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8779_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8779_0_0_0/* byval_arg */
	, &IList_1_t8779_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8779_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6916_il2cpp_TypeInfo;

// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUIText.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.GUIText>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUIText>
extern MethodInfo IEnumerator_1_get_Current_m49399_MethodInfo;
static PropertyInfo IEnumerator_1_t6916____Current_PropertyInfo = 
{
	&IEnumerator_1_t6916_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49399_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6916_PropertyInfos[] =
{
	&IEnumerator_1_t6916____Current_PropertyInfo,
	NULL
};
extern Il2CppType GUIText_t192_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49399_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.GUIText>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49399_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6916_il2cpp_TypeInfo/* declaring_type */
	, &GUIText_t192_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49399_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6916_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49399_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6916_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6916_0_0_0;
extern Il2CppType IEnumerator_1_t6916_1_0_0;
struct IEnumerator_1_t6916;
extern Il2CppGenericClass IEnumerator_1_t6916_GenericClass;
TypeInfo IEnumerator_1_t6916_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6916_MethodInfos/* methods */
	, IEnumerator_1_t6916_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6916_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6916_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6916_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6916_0_0_0/* byval_arg */
	, &IEnumerator_1_t6916_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6916_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.GUIText>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_471.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4884_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.GUIText>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_471MethodDeclarations.h"

extern TypeInfo GUIText_t192_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29435_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGUIText_t192_m38784_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.GUIText>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.GUIText>(System.Int32)
#define Array_InternalArray__get_Item_TisGUIText_t192_m38784(__this, p0, method) (GUIText_t192 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIText>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUIText>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIText>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUIText>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.GUIText>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUIText>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4884____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4884_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4884, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t4884____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t4884_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4884, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4884_FieldInfos[] =
{
	&InternalEnumerator_1_t4884____array_0_FieldInfo,
	&InternalEnumerator_1_t4884____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29432_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4884____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4884_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29432_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4884____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4884_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29435_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4884_PropertyInfos[] =
{
	&InternalEnumerator_1_t4884____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4884____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4884_InternalEnumerator_1__ctor_m29431_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29431_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIText>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29431_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t4884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t4884_InternalEnumerator_1__ctor_m29431_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29431_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29432_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUIText>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29432_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t4884_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29432_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29433_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIText>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29433_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t4884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29433_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29434_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUIText>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29434_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t4884_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29434_GenericMethod/* genericMethod */

};
extern Il2CppType GUIText_t192_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29435_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.GUIText>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29435_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t4884_il2cpp_TypeInfo/* declaring_type */
	, &GUIText_t192_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29435_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4884_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29431_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29432_MethodInfo,
	&InternalEnumerator_1_Dispose_m29433_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29434_MethodInfo,
	&InternalEnumerator_1_get_Current_m29435_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29434_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29433_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4884_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29432_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29434_MethodInfo,
	&InternalEnumerator_1_Dispose_m29433_MethodInfo,
	&InternalEnumerator_1_get_Current_m29435_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4884_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6916_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4884_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6916_il2cpp_TypeInfo, 7},
};
extern TypeInfo GUIText_t192_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4884_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29435_MethodInfo/* Method Usage */,
	&GUIText_t192_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGUIText_t192_m38784_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4884_0_0_0;
extern Il2CppType InternalEnumerator_1_t4884_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4884_GenericClass;
TypeInfo InternalEnumerator_1_t4884_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4884_MethodInfos/* methods */
	, InternalEnumerator_1_t4884_PropertyInfos/* properties */
	, InternalEnumerator_1_t4884_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4884_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4884_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4884_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4884_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4884_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4884_1_0_0/* this_arg */
	, InternalEnumerator_1_t4884_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4884_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4884_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4884)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8781_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUIText>
extern MethodInfo ICollection_1_get_Count_m49400_MethodInfo;
static PropertyInfo ICollection_1_t8781____Count_PropertyInfo = 
{
	&ICollection_1_t8781_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49400_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49401_MethodInfo;
static PropertyInfo ICollection_1_t8781____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8781_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49401_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8781_PropertyInfos[] =
{
	&ICollection_1_t8781____Count_PropertyInfo,
	&ICollection_1_t8781____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49400_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::get_Count()
MethodInfo ICollection_1_get_Count_m49400_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8781_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49400_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49401_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49401_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8781_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49401_GenericMethod/* genericMethod */

};
extern Il2CppType GUIText_t192_0_0_0;
extern Il2CppType GUIText_t192_0_0_0;
static ParameterInfo ICollection_1_t8781_ICollection_1_Add_m49402_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUIText_t192_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49402_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::Add(T)
MethodInfo ICollection_1_Add_m49402_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8781_ICollection_1_Add_m49402_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49402_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49403_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::Clear()
MethodInfo ICollection_1_Clear_m49403_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49403_GenericMethod/* genericMethod */

};
extern Il2CppType GUIText_t192_0_0_0;
static ParameterInfo ICollection_1_t8781_ICollection_1_Contains_m49404_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUIText_t192_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49404_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::Contains(T)
MethodInfo ICollection_1_Contains_m49404_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8781_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8781_ICollection_1_Contains_m49404_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49404_GenericMethod/* genericMethod */

};
extern Il2CppType GUITextU5BU5D_t5736_0_0_0;
extern Il2CppType GUITextU5BU5D_t5736_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8781_ICollection_1_CopyTo_m49405_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GUITextU5BU5D_t5736_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49405_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49405_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8781_ICollection_1_CopyTo_m49405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49405_GenericMethod/* genericMethod */

};
extern Il2CppType GUIText_t192_0_0_0;
static ParameterInfo ICollection_1_t8781_ICollection_1_Remove_m49406_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUIText_t192_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49406_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIText>::Remove(T)
MethodInfo ICollection_1_Remove_m49406_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8781_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8781_ICollection_1_Remove_m49406_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49406_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8781_MethodInfos[] =
{
	&ICollection_1_get_Count_m49400_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49401_MethodInfo,
	&ICollection_1_Add_m49402_MethodInfo,
	&ICollection_1_Clear_m49403_MethodInfo,
	&ICollection_1_Contains_m49404_MethodInfo,
	&ICollection_1_CopyTo_m49405_MethodInfo,
	&ICollection_1_Remove_m49406_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8783_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8781_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8783_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8781_0_0_0;
extern Il2CppType ICollection_1_t8781_1_0_0;
struct ICollection_1_t8781;
extern Il2CppGenericClass ICollection_1_t8781_GenericClass;
TypeInfo ICollection_1_t8781_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8781_MethodInfos/* methods */
	, ICollection_1_t8781_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8781_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8781_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8781_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8781_0_0_0/* byval_arg */
	, &ICollection_1_t8781_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8781_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.GUIText>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUIText>
extern Il2CppType IEnumerator_1_t6916_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49407_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.GUIText>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49407_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8783_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6916_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49407_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8783_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49407_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8783_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8783_0_0_0;
extern Il2CppType IEnumerable_1_t8783_1_0_0;
struct IEnumerable_1_t8783;
extern Il2CppGenericClass IEnumerable_1_t8783_GenericClass;
TypeInfo IEnumerable_1_t8783_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8783_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8783_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8783_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8783_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8783_0_0_0/* byval_arg */
	, &IEnumerable_1_t8783_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8783_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8782_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.GUIText>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIText>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIText>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.GUIText>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIText>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUIText>
extern MethodInfo IList_1_get_Item_m49408_MethodInfo;
extern MethodInfo IList_1_set_Item_m49409_MethodInfo;
static PropertyInfo IList_1_t8782____Item_PropertyInfo = 
{
	&IList_1_t8782_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49408_MethodInfo/* get */
	, &IList_1_set_Item_m49409_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8782_PropertyInfos[] =
{
	&IList_1_t8782____Item_PropertyInfo,
	NULL
};
extern Il2CppType GUIText_t192_0_0_0;
static ParameterInfo IList_1_t8782_IList_1_IndexOf_m49410_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUIText_t192_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49410_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.GUIText>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49410_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8782_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8782_IList_1_IndexOf_m49410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49410_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GUIText_t192_0_0_0;
static ParameterInfo IList_1_t8782_IList_1_Insert_m49411_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GUIText_t192_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49411_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIText>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49411_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8782_IList_1_Insert_m49411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49411_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8782_IList_1_RemoveAt_m49412_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49412_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIText>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49412_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8782_IList_1_RemoveAt_m49412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49412_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8782_IList_1_get_Item_m49408_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType GUIText_t192_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49408_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.GUIText>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49408_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8782_il2cpp_TypeInfo/* declaring_type */
	, &GUIText_t192_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8782_IList_1_get_Item_m49408_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49408_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GUIText_t192_0_0_0;
static ParameterInfo IList_1_t8782_IList_1_set_Item_m49409_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GUIText_t192_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49409_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIText>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49409_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8782_IList_1_set_Item_m49409_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49409_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8782_MethodInfos[] =
{
	&IList_1_IndexOf_m49410_MethodInfo,
	&IList_1_Insert_m49411_MethodInfo,
	&IList_1_RemoveAt_m49412_MethodInfo,
	&IList_1_get_Item_m49408_MethodInfo,
	&IList_1_set_Item_m49409_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8782_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8781_il2cpp_TypeInfo,
	&IEnumerable_1_t8783_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8782_0_0_0;
extern Il2CppType IList_1_t8782_1_0_0;
struct IList_1_t8782;
extern Il2CppGenericClass IList_1_t8782_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8782_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8782_MethodInfos/* methods */
	, IList_1_t8782_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8782_il2cpp_TypeInfo/* element_class */
	, IList_1_t8782_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8782_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8782_0_0_0/* byval_arg */
	, &IList_1_t8782_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8782_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUIText>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_177.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4885_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUIText>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_177MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_179.h"
extern TypeInfo InvokableCall_1_t4886_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_179MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m29438_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m29440_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUIText>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUIText>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUIText>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4885____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4885_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4885, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4885_FieldInfos[] =
{
	&CachedInvokableCall_1_t4885____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType GUIText_t192_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4885_CachedInvokableCall_1__ctor_m29436_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &GUIText_t192_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m29436_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUIText>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m29436_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4885_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4885_CachedInvokableCall_1__ctor_m29436_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m29436_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4885_CachedInvokableCall_1_Invoke_m29437_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m29437_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUIText>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m29437_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4885_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4885_CachedInvokableCall_1_Invoke_m29437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m29437_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4885_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m29436_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29437_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m29437_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m29441_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4885_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29437_MethodInfo,
	&InvokableCall_1_Find_m29441_MethodInfo,
};
extern Il2CppType UnityAction_1_t4887_0_0_0;
extern TypeInfo UnityAction_1_t4887_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisGUIText_t192_m38794_MethodInfo;
extern TypeInfo GUIText_t192_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m29443_MethodInfo;
extern TypeInfo GUIText_t192_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4885_RGCTXData[8] = 
{
	&UnityAction_1_t4887_0_0_0/* Type Usage */,
	&UnityAction_1_t4887_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGUIText_t192_m38794_MethodInfo/* Method Usage */,
	&GUIText_t192_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29443_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m29438_MethodInfo/* Method Usage */,
	&GUIText_t192_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m29440_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4885_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4885_1_0_0;
struct CachedInvokableCall_1_t4885;
extern Il2CppGenericClass CachedInvokableCall_1_t4885_GenericClass;
TypeInfo CachedInvokableCall_1_t4885_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4885_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4885_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4886_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4885_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4885_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4885_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4885_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4885_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4885_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4885_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4885)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_183.h"
extern TypeInfo UnityAction_1_t4887_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_183MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.GUIText>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.GUIText>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisGUIText_t192_m38794(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>
extern Il2CppType UnityAction_1_t4887_0_0_1;
FieldInfo InvokableCall_1_t4886____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4887_0_0_1/* type */
	, &InvokableCall_1_t4886_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4886, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4886_FieldInfos[] =
{
	&InvokableCall_1_t4886____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4886_InvokableCall_1__ctor_m29438_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29438_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m29438_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4886_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4886_InvokableCall_1__ctor_m29438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29438_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4887_0_0_0;
static ParameterInfo InvokableCall_1_t4886_InvokableCall_1__ctor_m29439_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4887_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29439_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m29439_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4886_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4886_InvokableCall_1__ctor_m29439_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29439_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4886_InvokableCall_1_Invoke_m29440_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m29440_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m29440_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4886_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4886_InvokableCall_1_Invoke_m29440_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m29440_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4886_InvokableCall_1_Find_m29441_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m29441_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.GUIText>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m29441_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4886_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4886_InvokableCall_1_Find_m29441_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m29441_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4886_MethodInfos[] =
{
	&InvokableCall_1__ctor_m29438_MethodInfo,
	&InvokableCall_1__ctor_m29439_MethodInfo,
	&InvokableCall_1_Invoke_m29440_MethodInfo,
	&InvokableCall_1_Find_m29441_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4886_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m29440_MethodInfo,
	&InvokableCall_1_Find_m29441_MethodInfo,
};
extern TypeInfo UnityAction_1_t4887_il2cpp_TypeInfo;
extern TypeInfo GUIText_t192_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4886_RGCTXData[5] = 
{
	&UnityAction_1_t4887_0_0_0/* Type Usage */,
	&UnityAction_1_t4887_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGUIText_t192_m38794_MethodInfo/* Method Usage */,
	&GUIText_t192_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29443_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4886_0_0_0;
extern Il2CppType InvokableCall_1_t4886_1_0_0;
struct InvokableCall_1_t4886;
extern Il2CppGenericClass InvokableCall_1_t4886_GenericClass;
TypeInfo InvokableCall_1_t4886_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4886_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4886_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4886_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4886_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4886_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4886_0_0_0/* byval_arg */
	, &InvokableCall_1_t4886_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4886_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4886_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4886)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4887_UnityAction_1__ctor_m29442_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m29442_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m29442_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4887_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4887_UnityAction_1__ctor_m29442_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m29442_GenericMethod/* genericMethod */

};
extern Il2CppType GUIText_t192_0_0_0;
static ParameterInfo UnityAction_1_t4887_UnityAction_1_Invoke_m29443_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GUIText_t192_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m29443_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m29443_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4887_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4887_UnityAction_1_Invoke_m29443_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m29443_GenericMethod/* genericMethod */

};
extern Il2CppType GUIText_t192_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4887_UnityAction_1_BeginInvoke_m29444_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GUIText_t192_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m29444_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m29444_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4887_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4887_UnityAction_1_BeginInvoke_m29444_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m29444_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4887_UnityAction_1_EndInvoke_m29445_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m29445_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GUIText>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m29445_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4887_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4887_UnityAction_1_EndInvoke_m29445_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m29445_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4887_MethodInfos[] =
{
	&UnityAction_1__ctor_m29442_MethodInfo,
	&UnityAction_1_Invoke_m29443_MethodInfo,
	&UnityAction_1_BeginInvoke_m29444_MethodInfo,
	&UnityAction_1_EndInvoke_m29445_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m29444_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m29445_MethodInfo;
static MethodInfo* UnityAction_1_t4887_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m29443_MethodInfo,
	&UnityAction_1_BeginInvoke_m29444_MethodInfo,
	&UnityAction_1_EndInvoke_m29445_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4887_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4887_1_0_0;
struct UnityAction_1_t4887;
extern Il2CppGenericClass UnityAction_1_t4887_GenericClass;
TypeInfo UnityAction_1_t4887_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4887_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4887_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4887_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4887_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4887_0_0_0/* byval_arg */
	, &UnityAction_1_t4887_1_0_0/* this_arg */
	, UnityAction_1_t4887_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4887_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4887)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Font>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_178.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4888_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Font>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_178MethodDeclarations.h"

// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Font>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_180.h"
extern TypeInfo Font_t332_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4889_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Font>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_180MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m29448_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m29450_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Font>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Font>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Font>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t4888____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t4888_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4888, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4888_FieldInfos[] =
{
	&CachedInvokableCall_1_t4888____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Font_t332_0_0_0;
extern Il2CppType Font_t332_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4888_CachedInvokableCall_1__ctor_m29446_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Font_t332_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m29446_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Font>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m29446_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t4888_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4888_CachedInvokableCall_1__ctor_m29446_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m29446_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4888_CachedInvokableCall_1_Invoke_m29447_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m29447_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Font>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m29447_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t4888_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4888_CachedInvokableCall_1_Invoke_m29447_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m29447_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4888_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m29446_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29447_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m29447_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m29451_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4888_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29447_MethodInfo,
	&InvokableCall_1_Find_m29451_MethodInfo,
};
extern Il2CppType UnityAction_1_t4890_0_0_0;
extern TypeInfo UnityAction_1_t4890_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisFont_t332_m38795_MethodInfo;
extern TypeInfo Font_t332_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m29453_MethodInfo;
extern TypeInfo Font_t332_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4888_RGCTXData[8] = 
{
	&UnityAction_1_t4890_0_0_0/* Type Usage */,
	&UnityAction_1_t4890_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisFont_t332_m38795_MethodInfo/* Method Usage */,
	&Font_t332_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29453_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m29448_MethodInfo/* Method Usage */,
	&Font_t332_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m29450_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4888_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4888_1_0_0;
struct CachedInvokableCall_1_t4888;
extern Il2CppGenericClass CachedInvokableCall_1_t4888_GenericClass;
TypeInfo CachedInvokableCall_1_t4888_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4888_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4888_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4889_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4888_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4888_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4888_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4888_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4888_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4888_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4888_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4888)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Font>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_184.h"
extern TypeInfo UnityAction_1_t4890_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Font>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_184MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Font>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Font>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisFont_t332_m38795(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Font>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Font>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Font>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Font>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Font>
extern Il2CppType UnityAction_1_t4890_0_0_1;
FieldInfo InvokableCall_1_t4889____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4890_0_0_1/* type */
	, &InvokableCall_1_t4889_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4889, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4889_FieldInfos[] =
{
	&InvokableCall_1_t4889____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4889_InvokableCall_1__ctor_m29448_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29448_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Font>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m29448_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t4889_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4889_InvokableCall_1__ctor_m29448_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29448_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4890_0_0_0;
static ParameterInfo InvokableCall_1_t4889_InvokableCall_1__ctor_m29449_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4890_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29449_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Font>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m29449_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t4889_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4889_InvokableCall_1__ctor_m29449_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29449_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t4889_InvokableCall_1_Invoke_m29450_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m29450_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Font>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m29450_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t4889_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t4889_InvokableCall_1_Invoke_m29450_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m29450_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t4889_InvokableCall_1_Find_m29451_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m29451_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Font>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m29451_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t4889_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4889_InvokableCall_1_Find_m29451_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m29451_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4889_MethodInfos[] =
{
	&InvokableCall_1__ctor_m29448_MethodInfo,
	&InvokableCall_1__ctor_m29449_MethodInfo,
	&InvokableCall_1_Invoke_m29450_MethodInfo,
	&InvokableCall_1_Find_m29451_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4889_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m29450_MethodInfo,
	&InvokableCall_1_Find_m29451_MethodInfo,
};
extern TypeInfo UnityAction_1_t4890_il2cpp_TypeInfo;
extern TypeInfo Font_t332_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4889_RGCTXData[5] = 
{
	&UnityAction_1_t4890_0_0_0/* Type Usage */,
	&UnityAction_1_t4890_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisFont_t332_m38795_MethodInfo/* Method Usage */,
	&Font_t332_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29453_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4889_0_0_0;
extern Il2CppType InvokableCall_1_t4889_1_0_0;
struct InvokableCall_1_t4889;
extern Il2CppGenericClass InvokableCall_1_t4889_GenericClass;
TypeInfo InvokableCall_1_t4889_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4889_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4889_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4889_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4889_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4889_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4889_0_0_0/* byval_arg */
	, &InvokableCall_1_t4889_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4889_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4889_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4889)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Font>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Font>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Font>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Font>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Font>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4890_UnityAction_1__ctor_m29452_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m29452_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Font>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m29452_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t4890_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4890_UnityAction_1__ctor_m29452_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m29452_GenericMethod/* genericMethod */

};
extern Il2CppType Font_t332_0_0_0;
static ParameterInfo UnityAction_1_t4890_UnityAction_1_Invoke_m29453_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Font_t332_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m29453_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Font>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m29453_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t4890_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4890_UnityAction_1_Invoke_m29453_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m29453_GenericMethod/* genericMethod */

};
extern Il2CppType Font_t332_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4890_UnityAction_1_BeginInvoke_m29454_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Font_t332_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m29454_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Font>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m29454_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t4890_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4890_UnityAction_1_BeginInvoke_m29454_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m29454_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t4890_UnityAction_1_EndInvoke_m29455_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m29455_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Font>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m29455_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t4890_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t4890_UnityAction_1_EndInvoke_m29455_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m29455_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4890_MethodInfos[] =
{
	&UnityAction_1__ctor_m29452_MethodInfo,
	&UnityAction_1_Invoke_m29453_MethodInfo,
	&UnityAction_1_BeginInvoke_m29454_MethodInfo,
	&UnityAction_1_EndInvoke_m29455_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m29454_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m29455_MethodInfo;
static MethodInfo* UnityAction_1_t4890_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m29453_MethodInfo,
	&UnityAction_1_BeginInvoke_m29454_MethodInfo,
	&UnityAction_1_EndInvoke_m29455_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4890_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4890_1_0_0;
struct UnityAction_1_t4890;
extern Il2CppGenericClass UnityAction_1_t4890_GenericClass;
TypeInfo UnityAction_1_t4890_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4890_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4890_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4890_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4890_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4890_0_0_0/* byval_arg */
	, &UnityAction_1_t4890_1_0_0/* this_arg */
	, UnityAction_1_t4890_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4890_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4890)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
