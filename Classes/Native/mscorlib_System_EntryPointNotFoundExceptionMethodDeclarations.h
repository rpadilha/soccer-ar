﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.EntryPointNotFoundException
struct EntryPointNotFoundException_t2261;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.EntryPointNotFoundException::.ctor()
 void EntryPointNotFoundException__ctor_m13069 (EntryPointNotFoundException_t2261 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EntryPointNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void EntryPointNotFoundException__ctor_m13070 (EntryPointNotFoundException_t2261 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
