﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.YieldInstruction
struct YieldInstruction_t960;
struct YieldInstruction_t960_marshaled;

// System.Void UnityEngine.YieldInstruction::.ctor()
 void YieldInstruction__ctor_m6229 (YieldInstruction_t960 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void YieldInstruction_t960_marshal(const YieldInstruction_t960& unmarshaled, YieldInstruction_t960_marshaled& marshaled);
void YieldInstruction_t960_marshal_back(const YieldInstruction_t960_marshaled& marshaled, YieldInstruction_t960& unmarshaled);
void YieldInstruction_t960_marshal_cleanup(YieldInstruction_t960_marshaled& marshaled);
