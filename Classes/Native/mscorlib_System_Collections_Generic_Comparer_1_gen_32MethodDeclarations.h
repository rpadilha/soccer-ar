﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<Vuforia.ReconstructionAbstractBehaviour>
struct Comparer_1_t4108;
// System.Object
struct Object_t;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t46;

// System.Void System.Collections.Generic.Comparer`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor()
// System.Collections.Generic.Comparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_genMethodDeclarations.h"
#define Comparer_1__ctor_m23474(__this, method) (void)Comparer_1__ctor_m14672_gshared((Comparer_1_t2848 *)__this, method)
// System.Void System.Collections.Generic.Comparer`1<Vuforia.ReconstructionAbstractBehaviour>::.cctor()
#define Comparer_1__cctor_m23475(__this/* static, unused */, method) (void)Comparer_1__cctor_m14673_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IComparer.Compare(System.Object,System.Object)
#define Comparer_1_System_Collections_IComparer_Compare_m23476(__this, ___x, ___y, method) (int32_t)Comparer_1_System_Collections_IComparer_Compare_m14674_gshared((Comparer_1_t2848 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.ReconstructionAbstractBehaviour>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ReconstructionAbstractBehaviour>::get_Default()
#define Comparer_1_get_Default_m23477(__this/* static, unused */, method) (Comparer_1_t4108 *)Comparer_1_get_Default_m14675_gshared((Object_t *)__this/* static, unused */, method)
