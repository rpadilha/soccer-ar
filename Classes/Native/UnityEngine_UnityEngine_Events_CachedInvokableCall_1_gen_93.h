﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.ToggleGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_95.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ToggleGroup>
struct CachedInvokableCall_1_t3707  : public InvokableCall_1_t3708
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ToggleGroup>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
