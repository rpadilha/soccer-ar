﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1595;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Security.Cryptography.KeyBuilder
struct KeyBuilder_t1813  : public Object_t
{
};
struct KeyBuilder_t1813_StaticFields{
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::rng
	RandomNumberGenerator_t1595 * ___rng_0;
};
