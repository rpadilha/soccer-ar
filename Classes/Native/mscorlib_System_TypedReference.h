﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.TypedReference
struct TypedReference_t1770 
{
	// System.RuntimeTypeHandle System.TypedReference::type
	RuntimeTypeHandle_t1754  ___type_0;
	// System.IntPtr System.TypedReference::value
	IntPtr_t121 ___value_1;
	// System.IntPtr System.TypedReference::klass
	IntPtr_t121 ___klass_2;
};
