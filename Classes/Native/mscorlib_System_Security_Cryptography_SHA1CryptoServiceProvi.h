﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.SHA1Internal
struct SHA1Internal_t2165;
// System.Security.Cryptography.SHA1
#include "mscorlib_System_Security_Cryptography_SHA1.h"
// System.Security.Cryptography.SHA1CryptoServiceProvider
struct SHA1CryptoServiceProvider_t2166  : public SHA1_t1574
{
	// System.Security.Cryptography.SHA1Internal System.Security.Cryptography.SHA1CryptoServiceProvider::sha
	SHA1Internal_t2165 * ___sha_4;
};
