﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_4.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
struct CachedInvokableCall_1_t2787  : public InvokableCall_1_t2788
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
