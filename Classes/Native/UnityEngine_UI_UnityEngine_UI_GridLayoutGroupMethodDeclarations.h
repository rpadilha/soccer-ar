﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t434;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"

// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
 void GridLayoutGroup__ctor_m1873 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
 int32_t GridLayoutGroup_get_startCorner_m1874 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
 void GridLayoutGroup_set_startCorner_m1875 (GridLayoutGroup_t434 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
 int32_t GridLayoutGroup_get_startAxis_m1876 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
 void GridLayoutGroup_set_startAxis_m1877 (GridLayoutGroup_t434 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
 Vector2_t99  GridLayoutGroup_get_cellSize_m1878 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
 void GridLayoutGroup_set_cellSize_m1879 (GridLayoutGroup_t434 * __this, Vector2_t99  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
 Vector2_t99  GridLayoutGroup_get_spacing_m1880 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
 void GridLayoutGroup_set_spacing_m1881 (GridLayoutGroup_t434 * __this, Vector2_t99  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
 int32_t GridLayoutGroup_get_constraint_m1882 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
 void GridLayoutGroup_set_constraint_m1883 (GridLayoutGroup_t434 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
 int32_t GridLayoutGroup_get_constraintCount_m1884 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
 void GridLayoutGroup_set_constraintCount_m1885 (GridLayoutGroup_t434 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
 void GridLayoutGroup_CalculateLayoutInputHorizontal_m1886 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
 void GridLayoutGroup_CalculateLayoutInputVertical_m1887 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
 void GridLayoutGroup_SetLayoutHorizontal_m1888 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
 void GridLayoutGroup_SetLayoutVertical_m1889 (GridLayoutGroup_t434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
 void GridLayoutGroup_SetCellsAlongAxis_m1890 (GridLayoutGroup_t434 * __this, int32_t ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
