﻿#pragma once
#include <stdint.h>
// Vuforia.MultiTarget
struct MultiTarget_t617;
// Vuforia.DataSetTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTrackableBeh.h"
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t33  : public DataSetTrackableBehaviour_t596
{
	// Vuforia.MultiTarget Vuforia.MultiTargetAbstractBehaviour::mMultiTarget
	Object_t * ___mMultiTarget_20;
};
