﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IEventSystemHandler
struct IEventSystemHandler_t465;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.IEventSystemHandler>
struct Predicate_1_t3198  : public MulticastDelegate_t373
{
};
