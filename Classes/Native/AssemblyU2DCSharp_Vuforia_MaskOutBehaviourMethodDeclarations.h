﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MaskOutBehaviour
struct MaskOutBehaviour_t40;

// System.Void Vuforia.MaskOutBehaviour::.ctor()
 void MaskOutBehaviour__ctor_m75 (MaskOutBehaviour_t40 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MaskOutBehaviour::Start()
 void MaskOutBehaviour_Start_m76 (MaskOutBehaviour_t40 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
