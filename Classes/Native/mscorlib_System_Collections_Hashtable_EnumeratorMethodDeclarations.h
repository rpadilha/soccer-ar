﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Hashtable/Enumerator
struct Enumerator_t1885;
// System.Object
struct Object_t;
// System.Collections.Hashtable
struct Hashtable_t1348;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Hashtable/EnumeratorMode
#include "mscorlib_System_Collections_Hashtable_EnumeratorMode.h"

// System.Void System.Collections.Hashtable/Enumerator::.ctor(System.Collections.Hashtable,System.Collections.Hashtable/EnumeratorMode)
 void Enumerator__ctor_m10537 (Enumerator_t1885 * __this, Hashtable_t1348 * ___host, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/Enumerator::.cctor()
 void Enumerator__cctor_m10538 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/Enumerator::FailFast()
 void Enumerator_FailFast_m10539 (Enumerator_t1885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/Enumerator::Reset()
 void Enumerator_Reset_m10540 (Enumerator_t1885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Hashtable/Enumerator::MoveNext()
 bool Enumerator_MoveNext_m10541 (Enumerator_t1885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Hashtable/Enumerator::get_Entry()
 DictionaryEntry_t1355  Enumerator_get_Entry_m10542 (Enumerator_t1885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Hashtable/Enumerator::get_Key()
 Object_t * Enumerator_get_Key_m10543 (Enumerator_t1885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Hashtable/Enumerator::get_Value()
 Object_t * Enumerator_get_Value_m10544 (Enumerator_t1885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Hashtable/Enumerator::get_Current()
 Object_t * Enumerator_get_Current_m10545 (Enumerator_t1885 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
