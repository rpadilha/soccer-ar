﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$4
struct $ArrayType$4_t1708;
struct $ArrayType$4_t1708_marshaled;

void $ArrayType$4_t1708_marshal(const $ArrayType$4_t1708& unmarshaled, $ArrayType$4_t1708_marshaled& marshaled);
void $ArrayType$4_t1708_marshal_back(const $ArrayType$4_t1708_marshaled& marshaled, $ArrayType$4_t1708& unmarshaled);
void $ArrayType$4_t1708_marshal_cleanup($ArrayType$4_t1708_marshaled& marshaled);
