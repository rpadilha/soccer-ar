﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>
struct InternalEnumerator_1_t3597;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.Image/Type
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m19489 (InternalEnumerator_1_t3597 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19490 (InternalEnumerator_1_t3597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::Dispose()
 void InternalEnumerator_1_Dispose_m19491 (InternalEnumerator_1_t3597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m19492 (InternalEnumerator_1_t3597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m19493 (InternalEnumerator_1_t3597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
