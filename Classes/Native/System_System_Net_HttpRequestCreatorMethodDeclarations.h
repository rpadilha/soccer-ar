﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.HttpRequestCreator
struct HttpRequestCreator_t1385;
// System.Net.WebRequest
struct WebRequest_t1374;
// System.Uri
struct Uri_t1375;

// System.Void System.Net.HttpRequestCreator::.ctor()
 void HttpRequestCreator__ctor_m6997 (HttpRequestCreator_t1385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.HttpRequestCreator::Create(System.Uri)
 WebRequest_t1374 * HttpRequestCreator_Create_m6998 (HttpRequestCreator_t1385 * __this, Uri_t1375 * ___uri, MethodInfo* method) IL2CPP_METHOD_ATTR;
