﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Area_Script>
struct UnityAction_1_t2999;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Area_Script>
struct InvokableCall_1_t2998  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Area_Script>::Delegate
	UnityAction_1_t2999 * ___Delegate_0;
};
