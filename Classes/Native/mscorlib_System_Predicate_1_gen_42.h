﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t16;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Surface>
struct Predicate_1_t4322  : public MulticastDelegate_t373
{
};
