﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Runtime
struct Runtime_t1850;
// System.String
struct String_t;

// System.String Mono.Runtime::GetDisplayName()
 String_t* Runtime_GetDisplayName_m10417 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
