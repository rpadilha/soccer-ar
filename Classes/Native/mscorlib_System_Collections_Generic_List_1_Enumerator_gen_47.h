﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t663;
// Vuforia.DataSet
struct DataSet_t612;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>
struct Enumerator_t4036 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::l
	List_1_t663 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::current
	DataSet_t612 * ___current_3;
};
