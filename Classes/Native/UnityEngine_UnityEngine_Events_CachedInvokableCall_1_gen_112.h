﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_114.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoAbstractBehaviour>
struct CachedInvokableCall_1_t3859  : public InvokableCall_1_t3860
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
