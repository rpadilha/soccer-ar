﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordManagerImpl
struct WordManagerImpl_t744;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>
struct IEnumerable_1_t734;
// System.Collections.Generic.IEnumerable`1<Vuforia.Word>
struct IEnumerable_1_t735;
// Vuforia.Word
struct Word_t736;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t34;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour>
struct IEnumerable_1_t737;
// UnityEngine.Transform
struct Transform_t74;
// Vuforia.QCARManagerImpl/WordData[]
struct WordDataU5BU5D_t699;
// Vuforia.QCARManagerImpl/WordResultData[]
struct WordResultDataU5BU5D_t700;
// System.Collections.Generic.IEnumerable`1<Vuforia.QCARManagerImpl/WordData>
struct IEnumerable_1_t745;
// System.Collections.Generic.IEnumerable`1<Vuforia.QCARManagerImpl/WordResultData>
struct IEnumerable_1_t746;
// Vuforia.WordResult
struct WordResult_t747;
// Vuforia.WordPrefabCreationMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"

// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult> Vuforia.WordManagerImpl::GetActiveWordResults()
 Object_t* WordManagerImpl_GetActiveWordResults_m3264 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult> Vuforia.WordManagerImpl::GetNewWords()
 Object_t* WordManagerImpl_GetNewWords_m3265 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Word> Vuforia.WordManagerImpl::GetLostWords()
 Object_t* WordManagerImpl_GetLostWords_m3266 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordManagerImpl::TryGetWordBehaviour(Vuforia.Word,Vuforia.WordAbstractBehaviour&)
 bool WordManagerImpl_TryGetWordBehaviour_m3267 (WordManagerImpl_t744 * __this, Object_t * ___word, WordAbstractBehaviour_t34 ** ___behaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour> Vuforia.WordManagerImpl::GetTrackableBehaviours()
 Object_t* WordManagerImpl_GetTrackableBehaviours_m3268 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::DestroyWordBehaviour(Vuforia.WordAbstractBehaviour,System.Boolean)
 void WordManagerImpl_DestroyWordBehaviour_m3269 (WordManagerImpl_t744 * __this, WordAbstractBehaviour_t34 * ___behaviour, bool ___destroyGameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::InitializeWordBehaviourTemplates(Vuforia.WordPrefabCreationMode,System.Int32)
 void WordManagerImpl_InitializeWordBehaviourTemplates_m3270 (WordManagerImpl_t744 * __this, int32_t ___wordPrefabCreationMode, int32_t ___maxInstances, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::InitializeWordBehaviourTemplates()
 void WordManagerImpl_InitializeWordBehaviourTemplates_m3271 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::RemoveDestroyedTrackables()
 void WordManagerImpl_RemoveDestroyedTrackables_m3272 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::UpdateWords(UnityEngine.Transform,Vuforia.QCARManagerImpl/WordData[],Vuforia.QCARManagerImpl/WordResultData[])
 void WordManagerImpl_UpdateWords_m3273 (WordManagerImpl_t744 * __this, Transform_t74 * ___arCameraTransform, WordDataU5BU5D_t699* ___newWordData, WordResultDataU5BU5D_t700* ___wordResults, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::SetWordBehavioursToNotFound()
 void WordManagerImpl_SetWordBehavioursToNotFound_m3274 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::UpdateWords(System.Collections.Generic.IEnumerable`1<Vuforia.QCARManagerImpl/WordData>,System.Collections.Generic.IEnumerable`1<Vuforia.QCARManagerImpl/WordResultData>)
 void WordManagerImpl_UpdateWords_m3275 (WordManagerImpl_t744 * __this, Object_t* ___newWordData, Object_t* ___wordResults, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::UpdateWordResultPoses(UnityEngine.Transform,System.Collections.Generic.IEnumerable`1<Vuforia.QCARManagerImpl/WordResultData>)
 void WordManagerImpl_UpdateWordResultPoses_m3276 (WordManagerImpl_t744 * __this, Transform_t74 * ___arCameraTransform, Object_t* ___wordResults, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::AssociateWordResultsWithBehaviours()
 void WordManagerImpl_AssociateWordResultsWithBehaviours_m3277 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::UnregisterLostWords()
 void WordManagerImpl_UnregisterLostWords_m3278 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::UpdateWordBehaviourPoses()
 void WordManagerImpl_UpdateWordBehaviourPoses_m3279 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::AssociateWordBehaviour(Vuforia.WordResult)
 WordAbstractBehaviour_t34 * WordManagerImpl_AssociateWordBehaviour_m3280 (WordManagerImpl_t744 * __this, WordResult_t747 * ___wordResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::AssociateWordBehaviour(Vuforia.WordResult,Vuforia.WordAbstractBehaviour)
 WordAbstractBehaviour_t34 * WordManagerImpl_AssociateWordBehaviour_m3281 (WordManagerImpl_t744 * __this, WordResult_t747 * ___wordResult, WordAbstractBehaviour_t34 * ___wordBehaviourTemplate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::InstantiateWordBehaviour(Vuforia.WordAbstractBehaviour)
 WordAbstractBehaviour_t34 * WordManagerImpl_InstantiateWordBehaviour_m3282 (Object_t * __this/* static, unused */, WordAbstractBehaviour_t34 * ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::CreateWordBehaviour()
 WordAbstractBehaviour_t34 * WordManagerImpl_CreateWordBehaviour_m3283 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::.ctor()
 void WordManagerImpl__ctor_m3284 (WordManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
