﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityAction`1<Jump_Button>
struct UnityAction_1_t3112;
// System.Object
struct Object_t;
// Jump_Button
struct Jump_Button_t207;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.Events.UnityAction`1<Jump_Button>::.ctor(System.Object,System.IntPtr)
// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6MethodDeclarations.h"
#define UnityAction_1__ctor_m15839(__this, ___object, ___method, method) (void)UnityAction_1__ctor_m14203_gshared((UnityAction_1_t2763 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Void UnityEngine.Events.UnityAction`1<Jump_Button>::Invoke(T0)
#define UnityAction_1_Invoke_m15840(__this, ___arg0, method) (void)UnityAction_1_Invoke_m14204_gshared((UnityAction_1_t2763 *)__this, (Object_t *)___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Jump_Button>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m15841(__this, ___arg0, ___callback, ___object, method) (Object_t *)UnityAction_1_BeginInvoke_m14205_gshared((UnityAction_1_t2763 *)__this, (Object_t *)___arg0, (AsyncCallback_t251 *)___callback, (Object_t *)___object, method)
// System.Void UnityEngine.Events.UnityAction`1<Jump_Button>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m15842(__this, ___result, method) (void)UnityAction_1_EndInvoke_m14206_gshared((UnityAction_1_t2763 *)__this, (Object_t *)___result, method)
