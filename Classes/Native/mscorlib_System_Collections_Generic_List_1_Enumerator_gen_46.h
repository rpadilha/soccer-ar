﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t647;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>
struct Enumerator_t3968 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::l
	List_1_t647 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::current
	int32_t ___current_3;
};
