﻿#pragma once
#include <stdint.h>
// Vuforia.DataSet
struct DataSet_t612;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.DataSet>
struct Comparison_1_t4035  : public MulticastDelegate_t373
{
};
