﻿#pragma once
#include <stdint.h>
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t134;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.ITrackableEventHandler>
struct Comparison_1_t3840  : public MulticastDelegate_t373
{
};
