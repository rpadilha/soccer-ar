﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DESTransform
struct DESTransform_t2146;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t1619;
// System.Byte[]
struct ByteU5BU5D_t653;
// System.UInt32[]
struct UInt32U5BU5D_t1594;

// System.Void System.Security.Cryptography.DESTransform::.ctor(System.Security.Cryptography.SymmetricAlgorithm,System.Boolean,System.Byte[],System.Byte[])
 void DESTransform__ctor_m12043 (DESTransform_t2146 * __this, SymmetricAlgorithm_t1619 * ___symmAlgo, bool ___encryption, ByteU5BU5D_t653* ___key, ByteU5BU5D_t653* ___iv, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DESTransform::.cctor()
 void DESTransform__cctor_m12044 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Security.Cryptography.DESTransform::CipherFunct(System.UInt32,System.Int32)
 uint32_t DESTransform_CipherFunct_m12045 (DESTransform_t2146 * __this, uint32_t ___r, int32_t ___n, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DESTransform::Permutation(System.Byte[],System.Byte[],System.UInt32[],System.Boolean)
 void DESTransform_Permutation_m12046 (Object_t * __this/* static, unused */, ByteU5BU5D_t653* ___input, ByteU5BU5D_t653* ___output, UInt32U5BU5D_t1594* ___permTab, bool ___preSwap, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DESTransform::BSwap(System.Byte[])
 void DESTransform_BSwap_m12047 (Object_t * __this/* static, unused */, ByteU5BU5D_t653* ___byteBuff, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DESTransform::SetKey(System.Byte[])
 void DESTransform_SetKey_m12048 (DESTransform_t2146 * __this, ByteU5BU5D_t653* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DESTransform::ProcessBlock(System.Byte[],System.Byte[])
 void DESTransform_ProcessBlock_m12049 (DESTransform_t2146 * __this, ByteU5BU5D_t653* ___input, ByteU5BU5D_t653* ___output, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DESTransform::ECB(System.Byte[],System.Byte[])
 void DESTransform_ECB_m12050 (DESTransform_t2146 * __this, ByteU5BU5D_t653* ___input, ByteU5BU5D_t653* ___output, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.DESTransform::GetStrongKey()
 ByteU5BU5D_t653* DESTransform_GetStrongKey_m12051 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
