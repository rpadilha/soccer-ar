﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t1093;

// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
 void AssemblyIsEditorAssembly__ctor_m6419 (AssemblyIsEditorAssembly_t1093 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
