﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct EqualityComparer_1_t4403;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct EqualityComparer_1_t4403  : public Object_t
{
};
struct EqualityComparer_1_t4403_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>::_default
	EqualityComparer_1_t4403 * ____default_0;
};
