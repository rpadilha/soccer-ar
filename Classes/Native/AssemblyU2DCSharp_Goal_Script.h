﻿#pragma once
#include <stdint.h>
// Sphere
struct Sphere_t71;
// UnityEngine.GameObject
struct GameObject_t29;
// InGameState_Script
struct InGameState_Script_t83;
// UnityEngine.MeshFilter
struct MeshFilter_t84;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t85;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Goal_Script
struct Goal_Script_t86  : public MonoBehaviour_t10
{
	// Sphere Goal_Script::sphere
	Sphere_t71 * ___sphere_2;
	// UnityEngine.GameObject Goal_Script::goalKeeper
	GameObject_t29 * ___goalKeeper_3;
	// InGameState_Script Goal_Script::ingame
	InGameState_Script_t83 * ___ingame_4;
	// UnityEngine.MeshFilter Goal_Script::red
	MeshFilter_t84 * ___red_5;
	// UnityEngine.Vector3[] Goal_Script::arrayOriginalVertices
	Vector3U5BU5D_t85* ___arrayOriginalVertices_6;
};
