﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Hashtable/KeyMarker
struct KeyMarker_t1883;

// System.Void System.Collections.Hashtable/KeyMarker::.ctor()
 void KeyMarker__ctor_m10535 (KeyMarker_t1883 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/KeyMarker::.cctor()
 void KeyMarker__cctor_m10536 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
