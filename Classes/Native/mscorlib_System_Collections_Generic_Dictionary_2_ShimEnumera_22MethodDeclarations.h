﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>
struct ShimEnumerator_t4422;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ImageTarget>
struct Dictionary_2_t782;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m26471 (ShimEnumerator_t4422 * __this, Dictionary_2_t782 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::MoveNext()
 bool ShimEnumerator_MoveNext_m26472 (ShimEnumerator_t4422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m26473 (ShimEnumerator_t4422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::get_Key()
 Object_t * ShimEnumerator_get_Key_m26474 (ShimEnumerator_t4422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::get_Value()
 Object_t * ShimEnumerator_get_Value_m26475 (ShimEnumerator_t4422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::get_Current()
 Object_t * ShimEnumerator_get_Current_m26476 (ShimEnumerator_t4422 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
