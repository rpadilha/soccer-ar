﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.Image/PIXEL_FORMAT>
struct IList_1_t3969;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>
struct Collection_1_t3970  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::syncRoot
	Object_t * ___syncRoot_1;
};
