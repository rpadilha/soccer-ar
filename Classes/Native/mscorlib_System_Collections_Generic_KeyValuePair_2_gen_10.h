﻿#pragma once
#include <stdint.h>
// Vuforia.Trackable
struct Trackable_t594;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>
struct KeyValuePair_2_t3977 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::value
	Object_t * ___value_1;
};
