﻿#pragma once
#include <stdint.h>
// Vuforia.WordResult[]
struct WordResultU5BU5D_t4135;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t739  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.WordResult>::_items
	WordResultU5BU5D_t4135* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::_version
	int32_t ____version_3;
};
struct List_1_t739_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.WordResult>::EmptyArray
	WordResultU5BU5D_t4135* ___EmptyArray_4;
};
