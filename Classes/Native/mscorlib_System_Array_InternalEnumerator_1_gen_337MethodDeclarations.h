﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>
struct InternalEnumerator_1_t4071;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/WordData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor_0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m23024 (InternalEnumerator_1_t4071 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23025 (InternalEnumerator_1_t4071 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::Dispose()
 void InternalEnumerator_1_Dispose_m23026 (InternalEnumerator_1_t4071 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m23027 (InternalEnumerator_1_t4071 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::get_Current()
 WordData_t689  InternalEnumerator_1_get_Current_m23028 (InternalEnumerator_1_t4071 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
