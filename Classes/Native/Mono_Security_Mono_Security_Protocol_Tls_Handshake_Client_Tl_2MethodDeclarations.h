﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello
struct TlsClientHello_t1692;
// Mono.Security.Protocol.Tls.Context
struct Context_t1642;

// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello::.ctor(Mono.Security.Protocol.Tls.Context)
 void TlsClientHello__ctor_m8896 (TlsClientHello_t1692 * __this, Context_t1642 * ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello::Update()
 void TlsClientHello_Update_m8897 (TlsClientHello_t1692 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello::ProcessAsSsl3()
 void TlsClientHello_ProcessAsSsl3_m8898 (TlsClientHello_t1692 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello::ProcessAsTls1()
 void TlsClientHello_ProcessAsTls1_m8899 (TlsClientHello_t1692 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
