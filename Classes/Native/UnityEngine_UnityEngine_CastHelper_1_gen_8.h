﻿#pragma once
#include <stdint.h>
// UnityEngine.Rigidbody
struct Rigidbody_t180;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.Rigidbody>
struct CastHelper_1_t3020 
{
	// T UnityEngine.CastHelper`1<UnityEngine.Rigidbody>::t
	Rigidbody_t180 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.Rigidbody>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
