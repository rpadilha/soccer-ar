﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.Single
#include "mscorlib_System_Single.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Single_t170_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t541  : public MulticastDelegate_t373
{
};
