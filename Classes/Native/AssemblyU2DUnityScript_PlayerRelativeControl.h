﻿#pragma once
#include <stdint.h>
// Joystick
struct Joystick_t208;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.CharacterController
struct CharacterController_t209;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// PlayerRelativeControl
struct PlayerRelativeControl_t217  : public MonoBehaviour_t10
{
	// Joystick PlayerRelativeControl::moveJoystick
	Joystick_t208 * ___moveJoystick_2;
	// Joystick PlayerRelativeControl::rotateJoystick
	Joystick_t208 * ___rotateJoystick_3;
	// UnityEngine.Transform PlayerRelativeControl::cameraPivot
	Transform_t74 * ___cameraPivot_4;
	// System.Single PlayerRelativeControl::forwardSpeed
	float ___forwardSpeed_5;
	// System.Single PlayerRelativeControl::backwardSpeed
	float ___backwardSpeed_6;
	// System.Single PlayerRelativeControl::sidestepSpeed
	float ___sidestepSpeed_7;
	// System.Single PlayerRelativeControl::jumpSpeed
	float ___jumpSpeed_8;
	// System.Single PlayerRelativeControl::inAirMultiplier
	float ___inAirMultiplier_9;
	// UnityEngine.Vector2 PlayerRelativeControl::rotationSpeed
	Vector2_t99  ___rotationSpeed_10;
	// UnityEngine.Transform PlayerRelativeControl::thisTransform
	Transform_t74 * ___thisTransform_11;
	// UnityEngine.CharacterController PlayerRelativeControl::character
	CharacterController_t209 * ___character_12;
	// UnityEngine.Vector3 PlayerRelativeControl::cameraVelocity
	Vector3_t73  ___cameraVelocity_13;
	// UnityEngine.Vector3 PlayerRelativeControl::velocity
	Vector3_t73  ___velocity_14;
};
