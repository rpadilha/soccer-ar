﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerable_1_t9527_il2cpp_TypeInfo;


// System.Array
#include "mscorlib_System_Array.h"

// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
extern Il2CppType IEnumerator_1_t7410_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52965_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52965_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9527_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7410_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52965_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9527_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52965_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
static TypeInfo* IEnumerable_1_t9527_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9527_0_0_0;
extern Il2CppType IEnumerable_1_t9527_1_0_0;
struct IEnumerable_1_t9527;
extern Il2CppGenericClass IEnumerable_1_t9527_GenericClass;
TypeInfo IEnumerable_1_t9527_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9527_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9527_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9527_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9527_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9527_0_0_0/* byval_arg */
	, &IEnumerable_1_t9527_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9527_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9526_il2cpp_TypeInfo;

// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Void
#include "mscorlib_System_Void.h"


// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
extern MethodInfo IList_1_get_Item_m52966_MethodInfo;
extern MethodInfo IList_1_set_Item_m52967_MethodInfo;
static PropertyInfo IList_1_t9526____Item_PropertyInfo = 
{
	&IList_1_t9526_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52966_MethodInfo/* get */
	, &IList_1_set_Item_m52967_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9526_PropertyInfos[] =
{
	&IList_1_t9526____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
static ParameterInfo IList_1_t9526_IList_1_IndexOf_m52968_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeLibVersionAttribute_t2031_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52968_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52968_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9526_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9526_IList_1_IndexOf_m52968_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52968_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
static ParameterInfo IList_1_t9526_IList_1_Insert_m52969_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeLibVersionAttribute_t2031_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52969_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52969_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9526_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9526_IList_1_Insert_m52969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52969_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9526_IList_1_RemoveAt_m52970_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52970_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52970_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9526_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9526_IList_1_RemoveAt_m52970_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52970_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9526_IList_1_get_Item_m52966_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52966_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52966_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9526_il2cpp_TypeInfo/* declaring_type */
	, &TypeLibVersionAttribute_t2031_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9526_IList_1_get_Item_m52966_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52966_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeLibVersionAttribute_t2031_0_0_0;
static ParameterInfo IList_1_t9526_IList_1_set_Item_m52967_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeLibVersionAttribute_t2031_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52967_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52967_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9526_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9526_IList_1_set_Item_m52967_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52967_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9526_MethodInfos[] =
{
	&IList_1_IndexOf_m52968_MethodInfo,
	&IList_1_Insert_m52969_MethodInfo,
	&IList_1_RemoveAt_m52970_MethodInfo,
	&IList_1_get_Item_m52966_MethodInfo,
	&IList_1_set_Item_m52967_MethodInfo,
	NULL
};
extern TypeInfo ICollection_1_t9525_il2cpp_TypeInfo;
static TypeInfo* IList_1_t9526_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9525_il2cpp_TypeInfo,
	&IEnumerable_1_t9527_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9526_0_0_0;
extern Il2CppType IList_1_t9526_1_0_0;
struct IList_1_t9526;
extern Il2CppGenericClass IList_1_t9526_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9526_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9526_MethodInfos/* methods */
	, IList_1_t9526_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9526_il2cpp_TypeInfo/* element_class */
	, IList_1_t9526_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9526_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9526_0_0_0/* byval_arg */
	, &IList_1_t9526_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9526_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7412_il2cpp_TypeInfo;

// System.Runtime.InteropServices.UnmanagedType
#include "mscorlib_System_Runtime_InteropServices_UnmanagedType.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.UnmanagedType>
extern MethodInfo IEnumerator_1_get_Current_m52971_MethodInfo;
static PropertyInfo IEnumerator_1_t7412____Current_PropertyInfo = 
{
	&IEnumerator_1_t7412_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52971_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7412_PropertyInfos[] =
{
	&IEnumerator_1_t7412____Current_PropertyInfo,
	NULL
};
extern Il2CppType UnmanagedType_t2032_0_0_0;
extern void* RuntimeInvoker_UnmanagedType_t2032 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52971_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52971_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7412_il2cpp_TypeInfo/* declaring_type */
	, &UnmanagedType_t2032_0_0_0/* return_type */
	, RuntimeInvoker_UnmanagedType_t2032/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52971_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7412_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52971_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t7412_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7412_0_0_0;
extern Il2CppType IEnumerator_1_t7412_1_0_0;
struct IEnumerator_1_t7412;
extern Il2CppGenericClass IEnumerator_1_t7412_GenericClass;
TypeInfo IEnumerator_1_t7412_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7412_MethodInfos/* methods */
	, IEnumerator_1_t7412_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7412_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7412_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7412_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7412_0_0_0/* byval_arg */
	, &IEnumerator_1_t7412_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7412_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_722.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5268_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_722MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo UnmanagedType_t2032_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m31716_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUnmanagedType_t2032_m41664_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.UnmanagedType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.UnmanagedType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisUnmanagedType_t2032_m41664 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31712_MethodInfo;
 void InternalEnumerator_1__ctor_m31712 (InternalEnumerator_1_t5268 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31713_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31713 (InternalEnumerator_1_t5268 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31716(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31716_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&UnmanagedType_t2032_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31714_MethodInfo;
 void InternalEnumerator_1_Dispose_m31714 (InternalEnumerator_1_t5268 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31715_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31715 (InternalEnumerator_1_t5268 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31716 (InternalEnumerator_1_t5268 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisUnmanagedType_t2032_m41664(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisUnmanagedType_t2032_m41664_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5268____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5268_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5268, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5268____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5268_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5268, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5268_FieldInfos[] =
{
	&InternalEnumerator_1_t5268____array_0_FieldInfo,
	&InternalEnumerator_1_t5268____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5268____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5268_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31713_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5268____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5268_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31716_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5268_PropertyInfos[] =
{
	&InternalEnumerator_1_t5268____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5268____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5268_InternalEnumerator_1__ctor_m31712_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31712_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31712_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31712/* method */
	, &InternalEnumerator_1_t5268_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5268_InternalEnumerator_1__ctor_m31712_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31712_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31713_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31713_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31713/* method */
	, &InternalEnumerator_1_t5268_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31713_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31714_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31714_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31714/* method */
	, &InternalEnumerator_1_t5268_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31714_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31715_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31715_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31715/* method */
	, &InternalEnumerator_1_t5268_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31715_GenericMethod/* genericMethod */

};
extern Il2CppType UnmanagedType_t2032_0_0_0;
extern void* RuntimeInvoker_UnmanagedType_t2032 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31716_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31716_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31716/* method */
	, &InternalEnumerator_1_t5268_il2cpp_TypeInfo/* declaring_type */
	, &UnmanagedType_t2032_0_0_0/* return_type */
	, RuntimeInvoker_UnmanagedType_t2032/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31716_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5268_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31712_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31713_MethodInfo,
	&InternalEnumerator_1_Dispose_m31714_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31715_MethodInfo,
	&InternalEnumerator_1_get_Current_m31716_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5268_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31713_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31715_MethodInfo,
	&InternalEnumerator_1_Dispose_m31714_MethodInfo,
	&InternalEnumerator_1_get_Current_m31716_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5268_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7412_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5268_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7412_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5268_0_0_0;
extern Il2CppType InternalEnumerator_1_t5268_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t5268_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t5268_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5268_MethodInfos/* methods */
	, InternalEnumerator_1_t5268_PropertyInfos/* properties */
	, InternalEnumerator_1_t5268_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5268_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5268_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5268_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5268_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5268_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5268_1_0_0/* this_arg */
	, InternalEnumerator_1_t5268_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5268_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5268)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9528_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>
extern MethodInfo ICollection_1_get_Count_m52972_MethodInfo;
static PropertyInfo ICollection_1_t9528____Count_PropertyInfo = 
{
	&ICollection_1_t9528_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52972_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52973_MethodInfo;
static PropertyInfo ICollection_1_t9528____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9528_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52973_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9528_PropertyInfos[] =
{
	&ICollection_1_t9528____Count_PropertyInfo,
	&ICollection_1_t9528____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52972_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::get_Count()
MethodInfo ICollection_1_get_Count_m52972_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9528_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52972_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52973_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52973_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9528_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52973_GenericMethod/* genericMethod */

};
extern Il2CppType UnmanagedType_t2032_0_0_0;
extern Il2CppType UnmanagedType_t2032_0_0_0;
static ParameterInfo ICollection_1_t9528_ICollection_1_Add_m52974_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnmanagedType_t2032_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52974_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::Add(T)
MethodInfo ICollection_1_Add_m52974_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9528_ICollection_1_Add_m52974_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52974_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52975_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::Clear()
MethodInfo ICollection_1_Clear_m52975_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52975_GenericMethod/* genericMethod */

};
extern Il2CppType UnmanagedType_t2032_0_0_0;
static ParameterInfo ICollection_1_t9528_ICollection_1_Contains_m52976_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnmanagedType_t2032_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52976_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::Contains(T)
MethodInfo ICollection_1_Contains_m52976_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9528_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9528_ICollection_1_Contains_m52976_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52976_GenericMethod/* genericMethod */

};
extern Il2CppType UnmanagedTypeU5BU5D_t5567_0_0_0;
extern Il2CppType UnmanagedTypeU5BU5D_t5567_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9528_ICollection_1_CopyTo_m52977_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UnmanagedTypeU5BU5D_t5567_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52977_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52977_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9528_ICollection_1_CopyTo_m52977_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52977_GenericMethod/* genericMethod */

};
extern Il2CppType UnmanagedType_t2032_0_0_0;
static ParameterInfo ICollection_1_t9528_ICollection_1_Remove_m52978_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnmanagedType_t2032_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52978_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>::Remove(T)
MethodInfo ICollection_1_Remove_m52978_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9528_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9528_ICollection_1_Remove_m52978_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52978_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9528_MethodInfos[] =
{
	&ICollection_1_get_Count_m52972_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52973_MethodInfo,
	&ICollection_1_Add_m52974_MethodInfo,
	&ICollection_1_Clear_m52975_MethodInfo,
	&ICollection_1_Contains_m52976_MethodInfo,
	&ICollection_1_CopyTo_m52977_MethodInfo,
	&ICollection_1_Remove_m52978_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9530_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9528_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9530_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9528_0_0_0;
extern Il2CppType ICollection_1_t9528_1_0_0;
struct ICollection_1_t9528;
extern Il2CppGenericClass ICollection_1_t9528_GenericClass;
TypeInfo ICollection_1_t9528_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9528_MethodInfos/* methods */
	, ICollection_1_t9528_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9528_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9528_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9528_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9528_0_0_0/* byval_arg */
	, &ICollection_1_t9528_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9528_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.UnmanagedType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.UnmanagedType>
extern Il2CppType IEnumerator_1_t7412_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52979_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.UnmanagedType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52979_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9530_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7412_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52979_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9530_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52979_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9530_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9530_0_0_0;
extern Il2CppType IEnumerable_1_t9530_1_0_0;
struct IEnumerable_1_t9530;
extern Il2CppGenericClass IEnumerable_1_t9530_GenericClass;
TypeInfo IEnumerable_1_t9530_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9530_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9530_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9530_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9530_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9530_0_0_0/* byval_arg */
	, &IEnumerable_1_t9530_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9530_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9529_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>
extern MethodInfo IList_1_get_Item_m52980_MethodInfo;
extern MethodInfo IList_1_set_Item_m52981_MethodInfo;
static PropertyInfo IList_1_t9529____Item_PropertyInfo = 
{
	&IList_1_t9529_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52980_MethodInfo/* get */
	, &IList_1_set_Item_m52981_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9529_PropertyInfos[] =
{
	&IList_1_t9529____Item_PropertyInfo,
	NULL
};
extern Il2CppType UnmanagedType_t2032_0_0_0;
static ParameterInfo IList_1_t9529_IList_1_IndexOf_m52982_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnmanagedType_t2032_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52982_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52982_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9529_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9529_IList_1_IndexOf_m52982_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52982_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UnmanagedType_t2032_0_0_0;
static ParameterInfo IList_1_t9529_IList_1_Insert_m52983_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UnmanagedType_t2032_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52983_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52983_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9529_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9529_IList_1_Insert_m52983_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52983_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9529_IList_1_RemoveAt_m52984_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52984_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52984_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9529_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9529_IList_1_RemoveAt_m52984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52984_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9529_IList_1_get_Item_m52980_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType UnmanagedType_t2032_0_0_0;
extern void* RuntimeInvoker_UnmanagedType_t2032_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52980_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52980_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9529_il2cpp_TypeInfo/* declaring_type */
	, &UnmanagedType_t2032_0_0_0/* return_type */
	, RuntimeInvoker_UnmanagedType_t2032_Int32_t123/* invoker_method */
	, IList_1_t9529_IList_1_get_Item_m52980_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52980_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UnmanagedType_t2032_0_0_0;
static ParameterInfo IList_1_t9529_IList_1_set_Item_m52981_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UnmanagedType_t2032_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52981_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52981_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9529_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9529_IList_1_set_Item_m52981_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52981_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9529_MethodInfos[] =
{
	&IList_1_IndexOf_m52982_MethodInfo,
	&IList_1_Insert_m52983_MethodInfo,
	&IList_1_RemoveAt_m52984_MethodInfo,
	&IList_1_get_Item_m52980_MethodInfo,
	&IList_1_set_Item_m52981_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9529_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9528_il2cpp_TypeInfo,
	&IEnumerable_1_t9530_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9529_0_0_0;
extern Il2CppType IList_1_t9529_1_0_0;
struct IList_1_t9529;
extern Il2CppGenericClass IList_1_t9529_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9529_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9529_MethodInfos/* methods */
	, IList_1_t9529_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9529_il2cpp_TypeInfo/* element_class */
	, IList_1_t9529_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9529_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9529_0_0_0/* byval_arg */
	, &IList_1_t9529_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9529_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7414_il2cpp_TypeInfo;

// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>
extern MethodInfo IEnumerator_1_get_Current_m52985_MethodInfo;
static PropertyInfo IEnumerator_1_t7414____Current_PropertyInfo = 
{
	&IEnumerator_1_t7414_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52985_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7414_PropertyInfos[] =
{
	&IEnumerator_1_t7414____Current_PropertyInfo,
	NULL
};
extern Il2CppType UrlAttribute_t2040_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52985_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52985_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7414_il2cpp_TypeInfo/* declaring_type */
	, &UrlAttribute_t2040_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52985_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7414_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52985_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7414_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7414_0_0_0;
extern Il2CppType IEnumerator_1_t7414_1_0_0;
struct IEnumerator_1_t7414;
extern Il2CppGenericClass IEnumerator_1_t7414_GenericClass;
TypeInfo IEnumerator_1_t7414_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7414_MethodInfos/* methods */
	, IEnumerator_1_t7414_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7414_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7414_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7414_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7414_0_0_0/* byval_arg */
	, &IEnumerator_1_t7414_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7414_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_723.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5269_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_723MethodDeclarations.h"

extern TypeInfo UrlAttribute_t2040_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31721_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUrlAttribute_t2040_m41675_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Activation.UrlAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Activation.UrlAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisUrlAttribute_t2040_m41675(__this, p0, method) (UrlAttribute_t2040 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5269____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5269_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5269, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5269____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5269_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5269, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5269_FieldInfos[] =
{
	&InternalEnumerator_1_t5269____array_0_FieldInfo,
	&InternalEnumerator_1_t5269____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31718_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5269____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5269_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31718_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5269____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5269_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31721_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5269_PropertyInfos[] =
{
	&InternalEnumerator_1_t5269____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5269____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5269_InternalEnumerator_1__ctor_m31717_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31717_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31717_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5269_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5269_InternalEnumerator_1__ctor_m31717_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31717_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31718_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31718_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5269_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31718_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31719_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31719_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5269_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31719_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31720_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31720_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5269_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31720_GenericMethod/* genericMethod */

};
extern Il2CppType UrlAttribute_t2040_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31721_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31721_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5269_il2cpp_TypeInfo/* declaring_type */
	, &UrlAttribute_t2040_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31721_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5269_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31717_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31718_MethodInfo,
	&InternalEnumerator_1_Dispose_m31719_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31720_MethodInfo,
	&InternalEnumerator_1_get_Current_m31721_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31720_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31719_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5269_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31718_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31720_MethodInfo,
	&InternalEnumerator_1_Dispose_m31719_MethodInfo,
	&InternalEnumerator_1_get_Current_m31721_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5269_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7414_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5269_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7414_il2cpp_TypeInfo, 7},
};
extern TypeInfo UrlAttribute_t2040_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5269_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31721_MethodInfo/* Method Usage */,
	&UrlAttribute_t2040_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisUrlAttribute_t2040_m41675_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5269_0_0_0;
extern Il2CppType InternalEnumerator_1_t5269_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5269_GenericClass;
TypeInfo InternalEnumerator_1_t5269_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5269_MethodInfos/* methods */
	, InternalEnumerator_1_t5269_PropertyInfos/* properties */
	, InternalEnumerator_1_t5269_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5269_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5269_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5269_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5269_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5269_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5269_1_0_0/* this_arg */
	, InternalEnumerator_1_t5269_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5269_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5269_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5269)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9531_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>
extern MethodInfo ICollection_1_get_Count_m52986_MethodInfo;
static PropertyInfo ICollection_1_t9531____Count_PropertyInfo = 
{
	&ICollection_1_t9531_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52986_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52987_MethodInfo;
static PropertyInfo ICollection_1_t9531____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9531_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52987_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9531_PropertyInfos[] =
{
	&ICollection_1_t9531____Count_PropertyInfo,
	&ICollection_1_t9531____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52986_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52986_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9531_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52986_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52987_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52987_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9531_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52987_GenericMethod/* genericMethod */

};
extern Il2CppType UrlAttribute_t2040_0_0_0;
extern Il2CppType UrlAttribute_t2040_0_0_0;
static ParameterInfo ICollection_1_t9531_ICollection_1_Add_m52988_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UrlAttribute_t2040_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52988_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::Add(T)
MethodInfo ICollection_1_Add_m52988_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9531_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9531_ICollection_1_Add_m52988_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52988_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52989_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::Clear()
MethodInfo ICollection_1_Clear_m52989_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9531_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52989_GenericMethod/* genericMethod */

};
extern Il2CppType UrlAttribute_t2040_0_0_0;
static ParameterInfo ICollection_1_t9531_ICollection_1_Contains_m52990_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UrlAttribute_t2040_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52990_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m52990_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9531_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9531_ICollection_1_Contains_m52990_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52990_GenericMethod/* genericMethod */

};
extern Il2CppType UrlAttributeU5BU5D_t5568_0_0_0;
extern Il2CppType UrlAttributeU5BU5D_t5568_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9531_ICollection_1_CopyTo_m52991_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UrlAttributeU5BU5D_t5568_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52991_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52991_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9531_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9531_ICollection_1_CopyTo_m52991_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52991_GenericMethod/* genericMethod */

};
extern Il2CppType UrlAttribute_t2040_0_0_0;
static ParameterInfo ICollection_1_t9531_ICollection_1_Remove_m52992_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UrlAttribute_t2040_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52992_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m52992_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9531_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9531_ICollection_1_Remove_m52992_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52992_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9531_MethodInfos[] =
{
	&ICollection_1_get_Count_m52986_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52987_MethodInfo,
	&ICollection_1_Add_m52988_MethodInfo,
	&ICollection_1_Clear_m52989_MethodInfo,
	&ICollection_1_Contains_m52990_MethodInfo,
	&ICollection_1_CopyTo_m52991_MethodInfo,
	&ICollection_1_Remove_m52992_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9533_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9531_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9533_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9531_0_0_0;
extern Il2CppType ICollection_1_t9531_1_0_0;
struct ICollection_1_t9531;
extern Il2CppGenericClass ICollection_1_t9531_GenericClass;
TypeInfo ICollection_1_t9531_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9531_MethodInfos/* methods */
	, ICollection_1_t9531_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9531_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9531_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9531_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9531_0_0_0/* byval_arg */
	, &ICollection_1_t9531_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9531_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Activation.UrlAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Activation.UrlAttribute>
extern Il2CppType IEnumerator_1_t7414_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52993_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Activation.UrlAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52993_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9533_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7414_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52993_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9533_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52993_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9533_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9533_0_0_0;
extern Il2CppType IEnumerable_1_t9533_1_0_0;
struct IEnumerable_1_t9533;
extern Il2CppGenericClass IEnumerable_1_t9533_GenericClass;
TypeInfo IEnumerable_1_t9533_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9533_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9533_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9533_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9533_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9533_0_0_0/* byval_arg */
	, &IEnumerable_1_t9533_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9533_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9532_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>
extern MethodInfo IList_1_get_Item_m52994_MethodInfo;
extern MethodInfo IList_1_set_Item_m52995_MethodInfo;
static PropertyInfo IList_1_t9532____Item_PropertyInfo = 
{
	&IList_1_t9532_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52994_MethodInfo/* get */
	, &IList_1_set_Item_m52995_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9532_PropertyInfos[] =
{
	&IList_1_t9532____Item_PropertyInfo,
	NULL
};
extern Il2CppType UrlAttribute_t2040_0_0_0;
static ParameterInfo IList_1_t9532_IList_1_IndexOf_m52996_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UrlAttribute_t2040_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52996_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52996_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9532_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9532_IList_1_IndexOf_m52996_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52996_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UrlAttribute_t2040_0_0_0;
static ParameterInfo IList_1_t9532_IList_1_Insert_m52997_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UrlAttribute_t2040_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52997_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52997_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9532_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9532_IList_1_Insert_m52997_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52997_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9532_IList_1_RemoveAt_m52998_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52998_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52998_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9532_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9532_IList_1_RemoveAt_m52998_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52998_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9532_IList_1_get_Item_m52994_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType UrlAttribute_t2040_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52994_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52994_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9532_il2cpp_TypeInfo/* declaring_type */
	, &UrlAttribute_t2040_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9532_IList_1_get_Item_m52994_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52994_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UrlAttribute_t2040_0_0_0;
static ParameterInfo IList_1_t9532_IList_1_set_Item_m52995_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UrlAttribute_t2040_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52995_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52995_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9532_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9532_IList_1_set_Item_m52995_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52995_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9532_MethodInfos[] =
{
	&IList_1_IndexOf_m52996_MethodInfo,
	&IList_1_Insert_m52997_MethodInfo,
	&IList_1_RemoveAt_m52998_MethodInfo,
	&IList_1_get_Item_m52994_MethodInfo,
	&IList_1_set_Item_m52995_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9532_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9531_il2cpp_TypeInfo,
	&IEnumerable_1_t9533_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9532_0_0_0;
extern Il2CppType IList_1_t9532_1_0_0;
struct IList_1_t9532;
extern Il2CppGenericClass IList_1_t9532_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9532_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9532_MethodInfos/* methods */
	, IList_1_t9532_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9532_il2cpp_TypeInfo/* element_class */
	, IList_1_t9532_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9532_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9532_0_0_0/* byval_arg */
	, &IList_1_t9532_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9532_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9534_il2cpp_TypeInfo;

// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttribute.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>
extern MethodInfo ICollection_1_get_Count_m52999_MethodInfo;
static PropertyInfo ICollection_1_t9534____Count_PropertyInfo = 
{
	&ICollection_1_t9534_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52999_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53000_MethodInfo;
static PropertyInfo ICollection_1_t9534____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9534_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53000_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9534_PropertyInfos[] =
{
	&ICollection_1_t9534____Count_PropertyInfo,
	&ICollection_1_t9534____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52999_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m52999_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9534_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52999_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53000_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53000_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9534_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53000_GenericMethod/* genericMethod */

};
extern Il2CppType ContextAttribute_t2041_0_0_0;
extern Il2CppType ContextAttribute_t2041_0_0_0;
static ParameterInfo ICollection_1_t9534_ICollection_1_Add_m53001_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContextAttribute_t2041_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53001_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Add(T)
MethodInfo ICollection_1_Add_m53001_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9534_ICollection_1_Add_m53001_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53001_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53002_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Clear()
MethodInfo ICollection_1_Clear_m53002_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53002_GenericMethod/* genericMethod */

};
extern Il2CppType ContextAttribute_t2041_0_0_0;
static ParameterInfo ICollection_1_t9534_ICollection_1_Contains_m53003_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContextAttribute_t2041_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53003_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m53003_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9534_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9534_ICollection_1_Contains_m53003_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53003_GenericMethod/* genericMethod */

};
extern Il2CppType ContextAttributeU5BU5D_t5569_0_0_0;
extern Il2CppType ContextAttributeU5BU5D_t5569_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9534_ICollection_1_CopyTo_m53004_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ContextAttributeU5BU5D_t5569_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53004_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53004_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9534_ICollection_1_CopyTo_m53004_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53004_GenericMethod/* genericMethod */

};
extern Il2CppType ContextAttribute_t2041_0_0_0;
static ParameterInfo ICollection_1_t9534_ICollection_1_Remove_m53005_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContextAttribute_t2041_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53005_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m53005_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9534_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9534_ICollection_1_Remove_m53005_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53005_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9534_MethodInfos[] =
{
	&ICollection_1_get_Count_m52999_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53000_MethodInfo,
	&ICollection_1_Add_m53001_MethodInfo,
	&ICollection_1_Clear_m53002_MethodInfo,
	&ICollection_1_Contains_m53003_MethodInfo,
	&ICollection_1_CopyTo_m53004_MethodInfo,
	&ICollection_1_Remove_m53005_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9536_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9534_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9536_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9534_0_0_0;
extern Il2CppType ICollection_1_t9534_1_0_0;
struct ICollection_1_t9534;
extern Il2CppGenericClass ICollection_1_t9534_GenericClass;
TypeInfo ICollection_1_t9534_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9534_MethodInfos/* methods */
	, ICollection_1_t9534_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9534_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9534_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9534_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9534_0_0_0/* byval_arg */
	, &ICollection_1_t9534_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9534_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.ContextAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.ContextAttribute>
extern Il2CppType IEnumerator_1_t7416_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53006_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.ContextAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53006_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9536_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7416_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53006_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9536_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53006_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9536_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9536_0_0_0;
extern Il2CppType IEnumerable_1_t9536_1_0_0;
struct IEnumerable_1_t9536;
extern Il2CppGenericClass IEnumerable_1_t9536_GenericClass;
TypeInfo IEnumerable_1_t9536_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9536_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9536_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9536_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9536_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9536_0_0_0/* byval_arg */
	, &IEnumerable_1_t9536_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9536_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7416_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>
extern MethodInfo IEnumerator_1_get_Current_m53007_MethodInfo;
static PropertyInfo IEnumerator_1_t7416____Current_PropertyInfo = 
{
	&IEnumerator_1_t7416_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53007_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7416_PropertyInfos[] =
{
	&IEnumerator_1_t7416____Current_PropertyInfo,
	NULL
};
extern Il2CppType ContextAttribute_t2041_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53007_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53007_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7416_il2cpp_TypeInfo/* declaring_type */
	, &ContextAttribute_t2041_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53007_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7416_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53007_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7416_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7416_0_0_0;
extern Il2CppType IEnumerator_1_t7416_1_0_0;
struct IEnumerator_1_t7416;
extern Il2CppGenericClass IEnumerator_1_t7416_GenericClass;
TypeInfo IEnumerator_1_t7416_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7416_MethodInfos/* methods */
	, IEnumerator_1_t7416_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7416_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7416_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7416_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7416_0_0_0/* byval_arg */
	, &IEnumerator_1_t7416_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7416_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_724.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5270_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_724MethodDeclarations.h"

extern TypeInfo ContextAttribute_t2041_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31726_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisContextAttribute_t2041_m41686_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.ContextAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.ContextAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisContextAttribute_t2041_m41686(__this, p0, method) (ContextAttribute_t2041 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5270____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5270_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5270, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5270____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5270_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5270, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5270_FieldInfos[] =
{
	&InternalEnumerator_1_t5270____array_0_FieldInfo,
	&InternalEnumerator_1_t5270____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31723_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5270____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5270_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31723_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5270____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5270_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31726_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5270_PropertyInfos[] =
{
	&InternalEnumerator_1_t5270____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5270____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5270_InternalEnumerator_1__ctor_m31722_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31722_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31722_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5270_InternalEnumerator_1__ctor_m31722_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31722_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31723_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31723_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5270_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31723_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31724_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31724_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31724_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31725_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31725_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5270_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31725_GenericMethod/* genericMethod */

};
extern Il2CppType ContextAttribute_t2041_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31726_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31726_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5270_il2cpp_TypeInfo/* declaring_type */
	, &ContextAttribute_t2041_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31726_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5270_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31722_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31723_MethodInfo,
	&InternalEnumerator_1_Dispose_m31724_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31725_MethodInfo,
	&InternalEnumerator_1_get_Current_m31726_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31725_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31724_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5270_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31723_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31725_MethodInfo,
	&InternalEnumerator_1_Dispose_m31724_MethodInfo,
	&InternalEnumerator_1_get_Current_m31726_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5270_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7416_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5270_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7416_il2cpp_TypeInfo, 7},
};
extern TypeInfo ContextAttribute_t2041_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5270_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31726_MethodInfo/* Method Usage */,
	&ContextAttribute_t2041_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisContextAttribute_t2041_m41686_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5270_0_0_0;
extern Il2CppType InternalEnumerator_1_t5270_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5270_GenericClass;
TypeInfo InternalEnumerator_1_t5270_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5270_MethodInfos/* methods */
	, InternalEnumerator_1_t5270_PropertyInfos/* properties */
	, InternalEnumerator_1_t5270_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5270_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5270_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5270_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5270_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5270_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5270_1_0_0/* this_arg */
	, InternalEnumerator_1_t5270_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5270_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5270_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5270)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9535_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>
extern MethodInfo IList_1_get_Item_m53008_MethodInfo;
extern MethodInfo IList_1_set_Item_m53009_MethodInfo;
static PropertyInfo IList_1_t9535____Item_PropertyInfo = 
{
	&IList_1_t9535_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53008_MethodInfo/* get */
	, &IList_1_set_Item_m53009_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9535_PropertyInfos[] =
{
	&IList_1_t9535____Item_PropertyInfo,
	NULL
};
extern Il2CppType ContextAttribute_t2041_0_0_0;
static ParameterInfo IList_1_t9535_IList_1_IndexOf_m53010_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContextAttribute_t2041_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53010_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53010_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9535_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9535_IList_1_IndexOf_m53010_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53010_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ContextAttribute_t2041_0_0_0;
static ParameterInfo IList_1_t9535_IList_1_Insert_m53011_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ContextAttribute_t2041_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53011_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53011_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9535_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9535_IList_1_Insert_m53011_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53011_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9535_IList_1_RemoveAt_m53012_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53012_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53012_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9535_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9535_IList_1_RemoveAt_m53012_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53012_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9535_IList_1_get_Item_m53008_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ContextAttribute_t2041_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53008_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53008_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9535_il2cpp_TypeInfo/* declaring_type */
	, &ContextAttribute_t2041_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9535_IList_1_get_Item_m53008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53008_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ContextAttribute_t2041_0_0_0;
static ParameterInfo IList_1_t9535_IList_1_set_Item_m53009_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ContextAttribute_t2041_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53009_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53009_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9535_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9535_IList_1_set_Item_m53009_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53009_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9535_MethodInfos[] =
{
	&IList_1_IndexOf_m53010_MethodInfo,
	&IList_1_Insert_m53011_MethodInfo,
	&IList_1_RemoveAt_m53012_MethodInfo,
	&IList_1_get_Item_m53008_MethodInfo,
	&IList_1_set_Item_m53009_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9535_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9534_il2cpp_TypeInfo,
	&IEnumerable_1_t9536_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9535_0_0_0;
extern Il2CppType IList_1_t9535_1_0_0;
struct IList_1_t9535;
extern Il2CppGenericClass IList_1_t9535_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9535_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9535_MethodInfos/* methods */
	, IList_1_t9535_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9535_il2cpp_TypeInfo/* element_class */
	, IList_1_t9535_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9535_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9535_0_0_0/* byval_arg */
	, &IList_1_t9535_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9535_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9537_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>
extern MethodInfo ICollection_1_get_Count_m53013_MethodInfo;
static PropertyInfo ICollection_1_t9537____Count_PropertyInfo = 
{
	&ICollection_1_t9537_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53013_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53014_MethodInfo;
static PropertyInfo ICollection_1_t9537____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9537_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53014_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9537_PropertyInfos[] =
{
	&ICollection_1_t9537____Count_PropertyInfo,
	&ICollection_1_t9537____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53013_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m53013_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9537_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53013_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53014_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53014_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9537_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53014_GenericMethod/* genericMethod */

};
extern Il2CppType IContextAttribute_t2681_0_0_0;
extern Il2CppType IContextAttribute_t2681_0_0_0;
static ParameterInfo ICollection_1_t9537_ICollection_1_Add_m53015_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContextAttribute_t2681_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53015_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Add(T)
MethodInfo ICollection_1_Add_m53015_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9537_ICollection_1_Add_m53015_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53015_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53016_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Clear()
MethodInfo ICollection_1_Clear_m53016_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53016_GenericMethod/* genericMethod */

};
extern Il2CppType IContextAttribute_t2681_0_0_0;
static ParameterInfo ICollection_1_t9537_ICollection_1_Contains_m53017_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContextAttribute_t2681_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53017_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m53017_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9537_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9537_ICollection_1_Contains_m53017_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53017_GenericMethod/* genericMethod */

};
extern Il2CppType IContextAttributeU5BU5D_t2092_0_0_0;
extern Il2CppType IContextAttributeU5BU5D_t2092_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9537_ICollection_1_CopyTo_m53018_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IContextAttributeU5BU5D_t2092_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53018_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53018_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9537_ICollection_1_CopyTo_m53018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53018_GenericMethod/* genericMethod */

};
extern Il2CppType IContextAttribute_t2681_0_0_0;
static ParameterInfo ICollection_1_t9537_ICollection_1_Remove_m53019_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContextAttribute_t2681_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53019_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m53019_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9537_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9537_ICollection_1_Remove_m53019_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53019_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9537_MethodInfos[] =
{
	&ICollection_1_get_Count_m53013_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53014_MethodInfo,
	&ICollection_1_Add_m53015_MethodInfo,
	&ICollection_1_Clear_m53016_MethodInfo,
	&ICollection_1_Contains_m53017_MethodInfo,
	&ICollection_1_CopyTo_m53018_MethodInfo,
	&ICollection_1_Remove_m53019_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9539_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9537_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9539_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9537_0_0_0;
extern Il2CppType ICollection_1_t9537_1_0_0;
struct ICollection_1_t9537;
extern Il2CppGenericClass ICollection_1_t9537_GenericClass;
TypeInfo ICollection_1_t9537_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9537_MethodInfos/* methods */
	, ICollection_1_t9537_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9537_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9537_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9537_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9537_0_0_0/* byval_arg */
	, &ICollection_1_t9537_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9537_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContextAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContextAttribute>
extern Il2CppType IEnumerator_1_t7418_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53020_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContextAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53020_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9539_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7418_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53020_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9539_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53020_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9539_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9539_0_0_0;
extern Il2CppType IEnumerable_1_t9539_1_0_0;
struct IEnumerable_1_t9539;
extern Il2CppGenericClass IEnumerable_1_t9539_GenericClass;
TypeInfo IEnumerable_1_t9539_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9539_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9539_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9539_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9539_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9539_0_0_0/* byval_arg */
	, &IEnumerable_1_t9539_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9539_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7418_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>
extern MethodInfo IEnumerator_1_get_Current_m53021_MethodInfo;
static PropertyInfo IEnumerator_1_t7418____Current_PropertyInfo = 
{
	&IEnumerator_1_t7418_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53021_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7418_PropertyInfos[] =
{
	&IEnumerator_1_t7418____Current_PropertyInfo,
	NULL
};
extern Il2CppType IContextAttribute_t2681_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53021_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53021_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7418_il2cpp_TypeInfo/* declaring_type */
	, &IContextAttribute_t2681_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53021_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7418_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53021_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7418_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7418_0_0_0;
extern Il2CppType IEnumerator_1_t7418_1_0_0;
struct IEnumerator_1_t7418;
extern Il2CppGenericClass IEnumerator_1_t7418_GenericClass;
TypeInfo IEnumerator_1_t7418_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7418_MethodInfos/* methods */
	, IEnumerator_1_t7418_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7418_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7418_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7418_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7418_0_0_0/* byval_arg */
	, &IEnumerator_1_t7418_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7418_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_725.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5271_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_725MethodDeclarations.h"

extern TypeInfo IContextAttribute_t2681_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31731_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIContextAttribute_t2681_m41697_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContextAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContextAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisIContextAttribute_t2681_m41697(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5271____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5271_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5271, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5271____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5271_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5271, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5271_FieldInfos[] =
{
	&InternalEnumerator_1_t5271____array_0_FieldInfo,
	&InternalEnumerator_1_t5271____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31728_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5271____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5271_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31728_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5271____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5271_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31731_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5271_PropertyInfos[] =
{
	&InternalEnumerator_1_t5271____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5271____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5271_InternalEnumerator_1__ctor_m31727_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31727_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31727_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5271_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5271_InternalEnumerator_1__ctor_m31727_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31727_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31728_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31728_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5271_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31728_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31729_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31729_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5271_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31729_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31730_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31730_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5271_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31730_GenericMethod/* genericMethod */

};
extern Il2CppType IContextAttribute_t2681_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31731_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31731_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5271_il2cpp_TypeInfo/* declaring_type */
	, &IContextAttribute_t2681_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31731_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5271_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31727_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31728_MethodInfo,
	&InternalEnumerator_1_Dispose_m31729_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31730_MethodInfo,
	&InternalEnumerator_1_get_Current_m31731_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31730_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31729_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5271_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31728_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31730_MethodInfo,
	&InternalEnumerator_1_Dispose_m31729_MethodInfo,
	&InternalEnumerator_1_get_Current_m31731_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5271_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7418_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5271_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7418_il2cpp_TypeInfo, 7},
};
extern TypeInfo IContextAttribute_t2681_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5271_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31731_MethodInfo/* Method Usage */,
	&IContextAttribute_t2681_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIContextAttribute_t2681_m41697_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5271_0_0_0;
extern Il2CppType InternalEnumerator_1_t5271_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5271_GenericClass;
TypeInfo InternalEnumerator_1_t5271_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5271_MethodInfos/* methods */
	, InternalEnumerator_1_t5271_PropertyInfos/* properties */
	, InternalEnumerator_1_t5271_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5271_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5271_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5271_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5271_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5271_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5271_1_0_0/* this_arg */
	, InternalEnumerator_1_t5271_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5271_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5271_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5271)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9538_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>
extern MethodInfo IList_1_get_Item_m53022_MethodInfo;
extern MethodInfo IList_1_set_Item_m53023_MethodInfo;
static PropertyInfo IList_1_t9538____Item_PropertyInfo = 
{
	&IList_1_t9538_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53022_MethodInfo/* get */
	, &IList_1_set_Item_m53023_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9538_PropertyInfos[] =
{
	&IList_1_t9538____Item_PropertyInfo,
	NULL
};
extern Il2CppType IContextAttribute_t2681_0_0_0;
static ParameterInfo IList_1_t9538_IList_1_IndexOf_m53024_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContextAttribute_t2681_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53024_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53024_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9538_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9538_IList_1_IndexOf_m53024_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53024_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IContextAttribute_t2681_0_0_0;
static ParameterInfo IList_1_t9538_IList_1_Insert_m53025_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IContextAttribute_t2681_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53025_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53025_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9538_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9538_IList_1_Insert_m53025_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53025_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9538_IList_1_RemoveAt_m53026_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53026_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53026_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9538_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9538_IList_1_RemoveAt_m53026_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53026_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9538_IList_1_get_Item_m53022_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IContextAttribute_t2681_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53022_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53022_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9538_il2cpp_TypeInfo/* declaring_type */
	, &IContextAttribute_t2681_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9538_IList_1_get_Item_m53022_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53022_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IContextAttribute_t2681_0_0_0;
static ParameterInfo IList_1_t9538_IList_1_set_Item_m53023_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IContextAttribute_t2681_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53023_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53023_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9538_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9538_IList_1_set_Item_m53023_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53023_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9538_MethodInfos[] =
{
	&IList_1_IndexOf_m53024_MethodInfo,
	&IList_1_Insert_m53025_MethodInfo,
	&IList_1_RemoveAt_m53026_MethodInfo,
	&IList_1_get_Item_m53022_MethodInfo,
	&IList_1_set_Item_m53023_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9538_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9537_il2cpp_TypeInfo,
	&IEnumerable_1_t9539_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9538_0_0_0;
extern Il2CppType IList_1_t9538_1_0_0;
struct IList_1_t9538;
extern Il2CppGenericClass IList_1_t9538_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9538_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9538_MethodInfos/* methods */
	, IList_1_t9538_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9538_il2cpp_TypeInfo/* element_class */
	, IList_1_t9538_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9538_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9538_0_0_0/* byval_arg */
	, &IList_1_t9538_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9538_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9540_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>
extern MethodInfo ICollection_1_get_Count_m53027_MethodInfo;
static PropertyInfo ICollection_1_t9540____Count_PropertyInfo = 
{
	&ICollection_1_t9540_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53027_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53028_MethodInfo;
static PropertyInfo ICollection_1_t9540____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9540_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53028_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9540_PropertyInfos[] =
{
	&ICollection_1_t9540____Count_PropertyInfo,
	&ICollection_1_t9540____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53027_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::get_Count()
MethodInfo ICollection_1_get_Count_m53027_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9540_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53027_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53028_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53028_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9540_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53028_GenericMethod/* genericMethod */

};
extern Il2CppType IContextProperty_t2051_0_0_0;
extern Il2CppType IContextProperty_t2051_0_0_0;
static ParameterInfo ICollection_1_t9540_ICollection_1_Add_m53029_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContextProperty_t2051_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53029_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::Add(T)
MethodInfo ICollection_1_Add_m53029_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9540_ICollection_1_Add_m53029_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53029_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53030_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::Clear()
MethodInfo ICollection_1_Clear_m53030_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53030_GenericMethod/* genericMethod */

};
extern Il2CppType IContextProperty_t2051_0_0_0;
static ParameterInfo ICollection_1_t9540_ICollection_1_Contains_m53031_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContextProperty_t2051_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53031_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::Contains(T)
MethodInfo ICollection_1_Contains_m53031_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9540_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9540_ICollection_1_Contains_m53031_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53031_GenericMethod/* genericMethod */

};
extern Il2CppType IContextPropertyU5BU5D_t5570_0_0_0;
extern Il2CppType IContextPropertyU5BU5D_t5570_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9540_ICollection_1_CopyTo_m53032_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IContextPropertyU5BU5D_t5570_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53032_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53032_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9540_ICollection_1_CopyTo_m53032_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53032_GenericMethod/* genericMethod */

};
extern Il2CppType IContextProperty_t2051_0_0_0;
static ParameterInfo ICollection_1_t9540_ICollection_1_Remove_m53033_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContextProperty_t2051_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53033_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>::Remove(T)
MethodInfo ICollection_1_Remove_m53033_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9540_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9540_ICollection_1_Remove_m53033_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53033_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9540_MethodInfos[] =
{
	&ICollection_1_get_Count_m53027_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53028_MethodInfo,
	&ICollection_1_Add_m53029_MethodInfo,
	&ICollection_1_Clear_m53030_MethodInfo,
	&ICollection_1_Contains_m53031_MethodInfo,
	&ICollection_1_CopyTo_m53032_MethodInfo,
	&ICollection_1_Remove_m53033_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9542_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9540_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9542_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9540_0_0_0;
extern Il2CppType ICollection_1_t9540_1_0_0;
struct ICollection_1_t9540;
extern Il2CppGenericClass ICollection_1_t9540_GenericClass;
TypeInfo ICollection_1_t9540_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9540_MethodInfos/* methods */
	, ICollection_1_t9540_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9540_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9540_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9540_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9540_0_0_0/* byval_arg */
	, &ICollection_1_t9540_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9540_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContextProperty>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContextProperty>
extern Il2CppType IEnumerator_1_t7420_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53034_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContextProperty>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53034_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9542_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7420_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53034_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9542_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53034_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9542_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9542_0_0_0;
extern Il2CppType IEnumerable_1_t9542_1_0_0;
struct IEnumerable_1_t9542;
extern Il2CppGenericClass IEnumerable_1_t9542_GenericClass;
TypeInfo IEnumerable_1_t9542_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9542_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9542_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9542_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9542_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9542_0_0_0/* byval_arg */
	, &IEnumerable_1_t9542_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9542_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7420_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>
extern MethodInfo IEnumerator_1_get_Current_m53035_MethodInfo;
static PropertyInfo IEnumerator_1_t7420____Current_PropertyInfo = 
{
	&IEnumerator_1_t7420_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53035_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7420_PropertyInfos[] =
{
	&IEnumerator_1_t7420____Current_PropertyInfo,
	NULL
};
extern Il2CppType IContextProperty_t2051_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53035_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53035_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7420_il2cpp_TypeInfo/* declaring_type */
	, &IContextProperty_t2051_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53035_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7420_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53035_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7420_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7420_0_0_0;
extern Il2CppType IEnumerator_1_t7420_1_0_0;
struct IEnumerator_1_t7420;
extern Il2CppGenericClass IEnumerator_1_t7420_GenericClass;
TypeInfo IEnumerator_1_t7420_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7420_MethodInfos/* methods */
	, IEnumerator_1_t7420_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7420_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7420_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7420_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7420_0_0_0/* byval_arg */
	, &IEnumerator_1_t7420_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7420_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_726.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5272_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_726MethodDeclarations.h"

extern TypeInfo IContextProperty_t2051_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31736_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIContextProperty_t2051_m41708_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContextProperty>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContextProperty>(System.Int32)
#define Array_InternalArray__get_Item_TisIContextProperty_t2051_m41708(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5272____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5272_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5272, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5272____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5272_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5272, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5272_FieldInfos[] =
{
	&InternalEnumerator_1_t5272____array_0_FieldInfo,
	&InternalEnumerator_1_t5272____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31733_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5272____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5272_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31733_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5272____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5272_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31736_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5272_PropertyInfos[] =
{
	&InternalEnumerator_1_t5272____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5272____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5272_InternalEnumerator_1__ctor_m31732_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31732_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31732_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5272_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5272_InternalEnumerator_1__ctor_m31732_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31732_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31733_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31733_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5272_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31733_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31734_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31734_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5272_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31734_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31735_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31735_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5272_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31735_GenericMethod/* genericMethod */

};
extern Il2CppType IContextProperty_t2051_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31736_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31736_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5272_il2cpp_TypeInfo/* declaring_type */
	, &IContextProperty_t2051_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31736_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5272_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31732_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31733_MethodInfo,
	&InternalEnumerator_1_Dispose_m31734_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31735_MethodInfo,
	&InternalEnumerator_1_get_Current_m31736_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31735_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31734_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5272_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31733_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31735_MethodInfo,
	&InternalEnumerator_1_Dispose_m31734_MethodInfo,
	&InternalEnumerator_1_get_Current_m31736_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5272_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7420_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5272_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7420_il2cpp_TypeInfo, 7},
};
extern TypeInfo IContextProperty_t2051_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5272_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31736_MethodInfo/* Method Usage */,
	&IContextProperty_t2051_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIContextProperty_t2051_m41708_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5272_0_0_0;
extern Il2CppType InternalEnumerator_1_t5272_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5272_GenericClass;
TypeInfo InternalEnumerator_1_t5272_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5272_MethodInfos/* methods */
	, InternalEnumerator_1_t5272_PropertyInfos/* properties */
	, InternalEnumerator_1_t5272_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5272_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5272_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5272_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5272_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5272_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5272_1_0_0/* this_arg */
	, InternalEnumerator_1_t5272_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5272_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5272_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5272)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9541_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>
extern MethodInfo IList_1_get_Item_m53036_MethodInfo;
extern MethodInfo IList_1_set_Item_m53037_MethodInfo;
static PropertyInfo IList_1_t9541____Item_PropertyInfo = 
{
	&IList_1_t9541_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53036_MethodInfo/* get */
	, &IList_1_set_Item_m53037_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9541_PropertyInfos[] =
{
	&IList_1_t9541____Item_PropertyInfo,
	NULL
};
extern Il2CppType IContextProperty_t2051_0_0_0;
static ParameterInfo IList_1_t9541_IList_1_IndexOf_m53038_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContextProperty_t2051_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53038_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53038_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9541_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9541_IList_1_IndexOf_m53038_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53038_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IContextProperty_t2051_0_0_0;
static ParameterInfo IList_1_t9541_IList_1_Insert_m53039_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IContextProperty_t2051_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53039_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53039_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9541_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9541_IList_1_Insert_m53039_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53039_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9541_IList_1_RemoveAt_m53040_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53040_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53040_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9541_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9541_IList_1_RemoveAt_m53040_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53040_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9541_IList_1_get_Item_m53036_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IContextProperty_t2051_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53036_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53036_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9541_il2cpp_TypeInfo/* declaring_type */
	, &IContextProperty_t2051_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9541_IList_1_get_Item_m53036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53036_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IContextProperty_t2051_0_0_0;
static ParameterInfo IList_1_t9541_IList_1_set_Item_m53037_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IContextProperty_t2051_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53037_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53037_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9541_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9541_IList_1_set_Item_m53037_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53037_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9541_MethodInfos[] =
{
	&IList_1_IndexOf_m53038_MethodInfo,
	&IList_1_Insert_m53039_MethodInfo,
	&IList_1_RemoveAt_m53040_MethodInfo,
	&IList_1_get_Item_m53036_MethodInfo,
	&IList_1_set_Item_m53037_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9541_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9540_il2cpp_TypeInfo,
	&IEnumerable_1_t9542_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9541_0_0_0;
extern Il2CppType IList_1_t9541_1_0_0;
struct IList_1_t9541;
extern Il2CppGenericClass IList_1_t9541_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9541_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9541_MethodInfos/* methods */
	, IList_1_t9541_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9541_il2cpp_TypeInfo/* element_class */
	, IList_1_t9541_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9541_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9541_0_0_0/* byval_arg */
	, &IList_1_t9541_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9541_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7422_il2cpp_TypeInfo;

// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAtt.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
extern MethodInfo IEnumerator_1_get_Current_m53041_MethodInfo;
static PropertyInfo IEnumerator_1_t7422____Current_PropertyInfo = 
{
	&IEnumerator_1_t7422_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53041_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7422_PropertyInfos[] =
{
	&IEnumerator_1_t7422____Current_PropertyInfo,
	NULL
};
extern Il2CppType SynchronizationAttribute_t2054_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53041_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53041_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7422_il2cpp_TypeInfo/* declaring_type */
	, &SynchronizationAttribute_t2054_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53041_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7422_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53041_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7422_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7422_0_0_0;
extern Il2CppType IEnumerator_1_t7422_1_0_0;
struct IEnumerator_1_t7422;
extern Il2CppGenericClass IEnumerator_1_t7422_GenericClass;
TypeInfo IEnumerator_1_t7422_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7422_MethodInfos/* methods */
	, IEnumerator_1_t7422_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7422_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7422_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7422_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7422_0_0_0/* byval_arg */
	, &IEnumerator_1_t7422_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7422_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_727.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5273_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_727MethodDeclarations.h"

extern TypeInfo SynchronizationAttribute_t2054_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31741_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSynchronizationAttribute_t2054_m41719_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisSynchronizationAttribute_t2054_m41719(__this, p0, method) (SynchronizationAttribute_t2054 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5273____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5273_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5273, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5273____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5273_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5273, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5273_FieldInfos[] =
{
	&InternalEnumerator_1_t5273____array_0_FieldInfo,
	&InternalEnumerator_1_t5273____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31738_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5273____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5273_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31738_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5273____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5273_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31741_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5273_PropertyInfos[] =
{
	&InternalEnumerator_1_t5273____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5273____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5273_InternalEnumerator_1__ctor_m31737_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31737_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31737_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5273_InternalEnumerator_1__ctor_m31737_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31737_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31738_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31738_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5273_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31738_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31739_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31739_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31739_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31740_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31740_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31740_GenericMethod/* genericMethod */

};
extern Il2CppType SynchronizationAttribute_t2054_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31741_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31741_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5273_il2cpp_TypeInfo/* declaring_type */
	, &SynchronizationAttribute_t2054_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31741_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5273_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31737_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31738_MethodInfo,
	&InternalEnumerator_1_Dispose_m31739_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31740_MethodInfo,
	&InternalEnumerator_1_get_Current_m31741_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31740_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31739_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5273_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31738_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31740_MethodInfo,
	&InternalEnumerator_1_Dispose_m31739_MethodInfo,
	&InternalEnumerator_1_get_Current_m31741_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5273_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7422_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5273_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7422_il2cpp_TypeInfo, 7},
};
extern TypeInfo SynchronizationAttribute_t2054_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5273_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31741_MethodInfo/* Method Usage */,
	&SynchronizationAttribute_t2054_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSynchronizationAttribute_t2054_m41719_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5273_0_0_0;
extern Il2CppType InternalEnumerator_1_t5273_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5273_GenericClass;
TypeInfo InternalEnumerator_1_t5273_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5273_MethodInfos/* methods */
	, InternalEnumerator_1_t5273_PropertyInfos/* properties */
	, InternalEnumerator_1_t5273_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5273_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5273_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5273_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5273_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5273_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5273_1_0_0/* this_arg */
	, InternalEnumerator_1_t5273_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5273_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5273_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5273)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9543_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
extern MethodInfo ICollection_1_get_Count_m53042_MethodInfo;
static PropertyInfo ICollection_1_t9543____Count_PropertyInfo = 
{
	&ICollection_1_t9543_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53042_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53043_MethodInfo;
static PropertyInfo ICollection_1_t9543____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9543_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53043_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9543_PropertyInfos[] =
{
	&ICollection_1_t9543____Count_PropertyInfo,
	&ICollection_1_t9543____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53042_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m53042_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9543_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53042_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53043_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53043_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9543_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53043_GenericMethod/* genericMethod */

};
extern Il2CppType SynchronizationAttribute_t2054_0_0_0;
extern Il2CppType SynchronizationAttribute_t2054_0_0_0;
static ParameterInfo ICollection_1_t9543_ICollection_1_Add_m53044_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SynchronizationAttribute_t2054_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53044_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Add(T)
MethodInfo ICollection_1_Add_m53044_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9543_ICollection_1_Add_m53044_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53044_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53045_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Clear()
MethodInfo ICollection_1_Clear_m53045_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53045_GenericMethod/* genericMethod */

};
extern Il2CppType SynchronizationAttribute_t2054_0_0_0;
static ParameterInfo ICollection_1_t9543_ICollection_1_Contains_m53046_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SynchronizationAttribute_t2054_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53046_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m53046_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9543_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9543_ICollection_1_Contains_m53046_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53046_GenericMethod/* genericMethod */

};
extern Il2CppType SynchronizationAttributeU5BU5D_t5571_0_0_0;
extern Il2CppType SynchronizationAttributeU5BU5D_t5571_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9543_ICollection_1_CopyTo_m53047_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SynchronizationAttributeU5BU5D_t5571_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53047_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53047_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9543_ICollection_1_CopyTo_m53047_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53047_GenericMethod/* genericMethod */

};
extern Il2CppType SynchronizationAttribute_t2054_0_0_0;
static ParameterInfo ICollection_1_t9543_ICollection_1_Remove_m53048_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SynchronizationAttribute_t2054_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53048_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m53048_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9543_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9543_ICollection_1_Remove_m53048_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53048_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9543_MethodInfos[] =
{
	&ICollection_1_get_Count_m53042_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53043_MethodInfo,
	&ICollection_1_Add_m53044_MethodInfo,
	&ICollection_1_Clear_m53045_MethodInfo,
	&ICollection_1_Contains_m53046_MethodInfo,
	&ICollection_1_CopyTo_m53047_MethodInfo,
	&ICollection_1_Remove_m53048_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9545_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9543_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9545_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9543_0_0_0;
extern Il2CppType ICollection_1_t9543_1_0_0;
struct ICollection_1_t9543;
extern Il2CppGenericClass ICollection_1_t9543_GenericClass;
TypeInfo ICollection_1_t9543_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9543_MethodInfos/* methods */
	, ICollection_1_t9543_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9543_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9543_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9543_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9543_0_0_0/* byval_arg */
	, &ICollection_1_t9543_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9543_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
extern Il2CppType IEnumerator_1_t7422_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53049_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53049_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9545_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7422_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53049_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9545_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53049_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9545_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9545_0_0_0;
extern Il2CppType IEnumerable_1_t9545_1_0_0;
struct IEnumerable_1_t9545;
extern Il2CppGenericClass IEnumerable_1_t9545_GenericClass;
TypeInfo IEnumerable_1_t9545_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9545_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9545_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9545_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9545_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9545_0_0_0/* byval_arg */
	, &IEnumerable_1_t9545_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9545_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9544_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
extern MethodInfo IList_1_get_Item_m53050_MethodInfo;
extern MethodInfo IList_1_set_Item_m53051_MethodInfo;
static PropertyInfo IList_1_t9544____Item_PropertyInfo = 
{
	&IList_1_t9544_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53050_MethodInfo/* get */
	, &IList_1_set_Item_m53051_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9544_PropertyInfos[] =
{
	&IList_1_t9544____Item_PropertyInfo,
	NULL
};
extern Il2CppType SynchronizationAttribute_t2054_0_0_0;
static ParameterInfo IList_1_t9544_IList_1_IndexOf_m53052_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SynchronizationAttribute_t2054_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53052_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53052_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9544_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9544_IList_1_IndexOf_m53052_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53052_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SynchronizationAttribute_t2054_0_0_0;
static ParameterInfo IList_1_t9544_IList_1_Insert_m53053_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SynchronizationAttribute_t2054_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53053_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53053_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9544_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9544_IList_1_Insert_m53053_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53053_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9544_IList_1_RemoveAt_m53054_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53054_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53054_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9544_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9544_IList_1_RemoveAt_m53054_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53054_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9544_IList_1_get_Item_m53050_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SynchronizationAttribute_t2054_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53050_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53050_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9544_il2cpp_TypeInfo/* declaring_type */
	, &SynchronizationAttribute_t2054_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9544_IList_1_get_Item_m53050_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53050_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SynchronizationAttribute_t2054_0_0_0;
static ParameterInfo IList_1_t9544_IList_1_set_Item_m53051_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SynchronizationAttribute_t2054_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53051_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53051_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9544_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9544_IList_1_set_Item_m53051_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53051_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9544_MethodInfos[] =
{
	&IList_1_IndexOf_m53052_MethodInfo,
	&IList_1_Insert_m53053_MethodInfo,
	&IList_1_RemoveAt_m53054_MethodInfo,
	&IList_1_get_Item_m53050_MethodInfo,
	&IList_1_set_Item_m53051_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9544_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9543_il2cpp_TypeInfo,
	&IEnumerable_1_t9545_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9544_0_0_0;
extern Il2CppType IList_1_t9544_1_0_0;
struct IList_1_t9544;
extern Il2CppGenericClass IList_1_t9544_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9544_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9544_MethodInfos/* methods */
	, IList_1_t9544_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9544_il2cpp_TypeInfo/* element_class */
	, IList_1_t9544_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9544_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9544_0_0_0/* byval_arg */
	, &IList_1_t9544_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9544_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9546_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
extern MethodInfo ICollection_1_get_Count_m53055_MethodInfo;
static PropertyInfo ICollection_1_t9546____Count_PropertyInfo = 
{
	&ICollection_1_t9546_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53055_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53056_MethodInfo;
static PropertyInfo ICollection_1_t9546____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9546_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53056_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9546_PropertyInfos[] =
{
	&ICollection_1_t9546____Count_PropertyInfo,
	&ICollection_1_t9546____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53055_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::get_Count()
MethodInfo ICollection_1_get_Count_m53055_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9546_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53055_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53056_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53056_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9546_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53056_GenericMethod/* genericMethod */

};
extern Il2CppType IContributeClientContextSink_t2685_0_0_0;
extern Il2CppType IContributeClientContextSink_t2685_0_0_0;
static ParameterInfo ICollection_1_t9546_ICollection_1_Add_m53057_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContributeClientContextSink_t2685_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53057_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Add(T)
MethodInfo ICollection_1_Add_m53057_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9546_ICollection_1_Add_m53057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53057_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53058_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Clear()
MethodInfo ICollection_1_Clear_m53058_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53058_GenericMethod/* genericMethod */

};
extern Il2CppType IContributeClientContextSink_t2685_0_0_0;
static ParameterInfo ICollection_1_t9546_ICollection_1_Contains_m53059_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContributeClientContextSink_t2685_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53059_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Contains(T)
MethodInfo ICollection_1_Contains_m53059_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9546_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9546_ICollection_1_Contains_m53059_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53059_GenericMethod/* genericMethod */

};
extern Il2CppType IContributeClientContextSinkU5BU5D_t5572_0_0_0;
extern Il2CppType IContributeClientContextSinkU5BU5D_t5572_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9546_ICollection_1_CopyTo_m53060_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IContributeClientContextSinkU5BU5D_t5572_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53060_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53060_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9546_ICollection_1_CopyTo_m53060_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53060_GenericMethod/* genericMethod */

};
extern Il2CppType IContributeClientContextSink_t2685_0_0_0;
static ParameterInfo ICollection_1_t9546_ICollection_1_Remove_m53061_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContributeClientContextSink_t2685_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53061_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Remove(T)
MethodInfo ICollection_1_Remove_m53061_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9546_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9546_ICollection_1_Remove_m53061_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53061_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9546_MethodInfos[] =
{
	&ICollection_1_get_Count_m53055_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53056_MethodInfo,
	&ICollection_1_Add_m53057_MethodInfo,
	&ICollection_1_Clear_m53058_MethodInfo,
	&ICollection_1_Contains_m53059_MethodInfo,
	&ICollection_1_CopyTo_m53060_MethodInfo,
	&ICollection_1_Remove_m53061_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9548_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9546_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9548_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9546_0_0_0;
extern Il2CppType ICollection_1_t9546_1_0_0;
struct ICollection_1_t9546;
extern Il2CppGenericClass ICollection_1_t9546_GenericClass;
TypeInfo ICollection_1_t9546_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9546_MethodInfos/* methods */
	, ICollection_1_t9546_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9546_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9546_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9546_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9546_0_0_0/* byval_arg */
	, &ICollection_1_t9546_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9546_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
extern Il2CppType IEnumerator_1_t7424_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53062_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53062_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9548_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7424_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53062_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9548_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53062_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9548_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9548_0_0_0;
extern Il2CppType IEnumerable_1_t9548_1_0_0;
struct IEnumerable_1_t9548;
extern Il2CppGenericClass IEnumerable_1_t9548_GenericClass;
TypeInfo IEnumerable_1_t9548_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9548_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9548_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9548_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9548_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9548_0_0_0/* byval_arg */
	, &IEnumerable_1_t9548_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9548_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7424_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
extern MethodInfo IEnumerator_1_get_Current_m53063_MethodInfo;
static PropertyInfo IEnumerator_1_t7424____Current_PropertyInfo = 
{
	&IEnumerator_1_t7424_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53063_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7424_PropertyInfos[] =
{
	&IEnumerator_1_t7424____Current_PropertyInfo,
	NULL
};
extern Il2CppType IContributeClientContextSink_t2685_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53063_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53063_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7424_il2cpp_TypeInfo/* declaring_type */
	, &IContributeClientContextSink_t2685_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53063_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7424_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53063_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7424_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7424_0_0_0;
extern Il2CppType IEnumerator_1_t7424_1_0_0;
struct IEnumerator_1_t7424;
extern Il2CppGenericClass IEnumerator_1_t7424_GenericClass;
TypeInfo IEnumerator_1_t7424_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7424_MethodInfos/* methods */
	, IEnumerator_1_t7424_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7424_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7424_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7424_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7424_0_0_0/* byval_arg */
	, &IEnumerator_1_t7424_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7424_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_728.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5274_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_728MethodDeclarations.h"

extern TypeInfo IContributeClientContextSink_t2685_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31746_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIContributeClientContextSink_t2685_m41730_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(System.Int32)
#define Array_InternalArray__get_Item_TisIContributeClientContextSink_t2685_m41730(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5274____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5274_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5274, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5274____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5274_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5274, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5274_FieldInfos[] =
{
	&InternalEnumerator_1_t5274____array_0_FieldInfo,
	&InternalEnumerator_1_t5274____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31743_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5274____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5274_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31743_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5274____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5274_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31746_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5274_PropertyInfos[] =
{
	&InternalEnumerator_1_t5274____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5274____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5274_InternalEnumerator_1__ctor_m31742_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31742_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31742_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5274_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5274_InternalEnumerator_1__ctor_m31742_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31742_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31743_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31743_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5274_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31743_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31744_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31744_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5274_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31744_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31745_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31745_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5274_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31745_GenericMethod/* genericMethod */

};
extern Il2CppType IContributeClientContextSink_t2685_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31746_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31746_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5274_il2cpp_TypeInfo/* declaring_type */
	, &IContributeClientContextSink_t2685_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31746_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5274_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31742_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31743_MethodInfo,
	&InternalEnumerator_1_Dispose_m31744_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31745_MethodInfo,
	&InternalEnumerator_1_get_Current_m31746_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31745_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31744_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5274_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31743_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31745_MethodInfo,
	&InternalEnumerator_1_Dispose_m31744_MethodInfo,
	&InternalEnumerator_1_get_Current_m31746_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5274_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7424_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5274_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7424_il2cpp_TypeInfo, 7},
};
extern TypeInfo IContributeClientContextSink_t2685_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5274_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31746_MethodInfo/* Method Usage */,
	&IContributeClientContextSink_t2685_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIContributeClientContextSink_t2685_m41730_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5274_0_0_0;
extern Il2CppType InternalEnumerator_1_t5274_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5274_GenericClass;
TypeInfo InternalEnumerator_1_t5274_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5274_MethodInfos/* methods */
	, InternalEnumerator_1_t5274_PropertyInfos/* properties */
	, InternalEnumerator_1_t5274_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5274_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5274_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5274_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5274_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5274_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5274_1_0_0/* this_arg */
	, InternalEnumerator_1_t5274_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5274_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5274_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5274)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9547_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
extern MethodInfo IList_1_get_Item_m53064_MethodInfo;
extern MethodInfo IList_1_set_Item_m53065_MethodInfo;
static PropertyInfo IList_1_t9547____Item_PropertyInfo = 
{
	&IList_1_t9547_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53064_MethodInfo/* get */
	, &IList_1_set_Item_m53065_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9547_PropertyInfos[] =
{
	&IList_1_t9547____Item_PropertyInfo,
	NULL
};
extern Il2CppType IContributeClientContextSink_t2685_0_0_0;
static ParameterInfo IList_1_t9547_IList_1_IndexOf_m53066_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContributeClientContextSink_t2685_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53066_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53066_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9547_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9547_IList_1_IndexOf_m53066_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53066_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IContributeClientContextSink_t2685_0_0_0;
static ParameterInfo IList_1_t9547_IList_1_Insert_m53067_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IContributeClientContextSink_t2685_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53067_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53067_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9547_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9547_IList_1_Insert_m53067_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53067_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9547_IList_1_RemoveAt_m53068_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53068_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53068_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9547_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9547_IList_1_RemoveAt_m53068_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53068_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9547_IList_1_get_Item_m53064_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IContributeClientContextSink_t2685_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53064_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53064_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9547_il2cpp_TypeInfo/* declaring_type */
	, &IContributeClientContextSink_t2685_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9547_IList_1_get_Item_m53064_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53064_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IContributeClientContextSink_t2685_0_0_0;
static ParameterInfo IList_1_t9547_IList_1_set_Item_m53065_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IContributeClientContextSink_t2685_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53065_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53065_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9547_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9547_IList_1_set_Item_m53065_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53065_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9547_MethodInfos[] =
{
	&IList_1_IndexOf_m53066_MethodInfo,
	&IList_1_Insert_m53067_MethodInfo,
	&IList_1_RemoveAt_m53068_MethodInfo,
	&IList_1_get_Item_m53064_MethodInfo,
	&IList_1_set_Item_m53065_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9547_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9546_il2cpp_TypeInfo,
	&IEnumerable_1_t9548_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9547_0_0_0;
extern Il2CppType IList_1_t9547_1_0_0;
struct IList_1_t9547;
extern Il2CppGenericClass IList_1_t9547_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9547_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9547_MethodInfos/* methods */
	, IList_1_t9547_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9547_il2cpp_TypeInfo/* element_class */
	, IList_1_t9547_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9547_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9547_0_0_0/* byval_arg */
	, &IList_1_t9547_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9547_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9549_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
extern MethodInfo ICollection_1_get_Count_m53069_MethodInfo;
static PropertyInfo ICollection_1_t9549____Count_PropertyInfo = 
{
	&ICollection_1_t9549_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53069_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53070_MethodInfo;
static PropertyInfo ICollection_1_t9549____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9549_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53070_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9549_PropertyInfos[] =
{
	&ICollection_1_t9549____Count_PropertyInfo,
	&ICollection_1_t9549____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53069_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::get_Count()
MethodInfo ICollection_1_get_Count_m53069_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9549_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53069_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53070_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53070_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9549_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53070_GenericMethod/* genericMethod */

};
extern Il2CppType IContributeServerContextSink_t2686_0_0_0;
extern Il2CppType IContributeServerContextSink_t2686_0_0_0;
static ParameterInfo ICollection_1_t9549_ICollection_1_Add_m53071_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContributeServerContextSink_t2686_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53071_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Add(T)
MethodInfo ICollection_1_Add_m53071_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9549_ICollection_1_Add_m53071_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53071_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53072_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Clear()
MethodInfo ICollection_1_Clear_m53072_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53072_GenericMethod/* genericMethod */

};
extern Il2CppType IContributeServerContextSink_t2686_0_0_0;
static ParameterInfo ICollection_1_t9549_ICollection_1_Contains_m53073_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContributeServerContextSink_t2686_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53073_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Contains(T)
MethodInfo ICollection_1_Contains_m53073_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9549_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9549_ICollection_1_Contains_m53073_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53073_GenericMethod/* genericMethod */

};
extern Il2CppType IContributeServerContextSinkU5BU5D_t5573_0_0_0;
extern Il2CppType IContributeServerContextSinkU5BU5D_t5573_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9549_ICollection_1_CopyTo_m53074_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IContributeServerContextSinkU5BU5D_t5573_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53074_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53074_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9549_ICollection_1_CopyTo_m53074_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53074_GenericMethod/* genericMethod */

};
extern Il2CppType IContributeServerContextSink_t2686_0_0_0;
static ParameterInfo ICollection_1_t9549_ICollection_1_Remove_m53075_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContributeServerContextSink_t2686_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53075_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Remove(T)
MethodInfo ICollection_1_Remove_m53075_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9549_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9549_ICollection_1_Remove_m53075_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53075_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9549_MethodInfos[] =
{
	&ICollection_1_get_Count_m53069_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53070_MethodInfo,
	&ICollection_1_Add_m53071_MethodInfo,
	&ICollection_1_Clear_m53072_MethodInfo,
	&ICollection_1_Contains_m53073_MethodInfo,
	&ICollection_1_CopyTo_m53074_MethodInfo,
	&ICollection_1_Remove_m53075_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9551_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9549_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9551_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9549_0_0_0;
extern Il2CppType ICollection_1_t9549_1_0_0;
struct ICollection_1_t9549;
extern Il2CppGenericClass ICollection_1_t9549_GenericClass;
TypeInfo ICollection_1_t9549_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9549_MethodInfos/* methods */
	, ICollection_1_t9549_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9549_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9549_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9549_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9549_0_0_0/* byval_arg */
	, &ICollection_1_t9549_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9549_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
extern Il2CppType IEnumerator_1_t7426_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53076_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53076_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9551_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7426_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53076_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9551_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53076_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9551_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9551_0_0_0;
extern Il2CppType IEnumerable_1_t9551_1_0_0;
struct IEnumerable_1_t9551;
extern Il2CppGenericClass IEnumerable_1_t9551_GenericClass;
TypeInfo IEnumerable_1_t9551_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9551_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9551_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9551_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9551_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9551_0_0_0/* byval_arg */
	, &IEnumerable_1_t9551_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9551_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7426_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
extern MethodInfo IEnumerator_1_get_Current_m53077_MethodInfo;
static PropertyInfo IEnumerator_1_t7426____Current_PropertyInfo = 
{
	&IEnumerator_1_t7426_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53077_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7426_PropertyInfos[] =
{
	&IEnumerator_1_t7426____Current_PropertyInfo,
	NULL
};
extern Il2CppType IContributeServerContextSink_t2686_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53077_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53077_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7426_il2cpp_TypeInfo/* declaring_type */
	, &IContributeServerContextSink_t2686_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53077_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7426_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53077_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7426_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7426_0_0_0;
extern Il2CppType IEnumerator_1_t7426_1_0_0;
struct IEnumerator_1_t7426;
extern Il2CppGenericClass IEnumerator_1_t7426_GenericClass;
TypeInfo IEnumerator_1_t7426_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7426_MethodInfos/* methods */
	, IEnumerator_1_t7426_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7426_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7426_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7426_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7426_0_0_0/* byval_arg */
	, &IEnumerator_1_t7426_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7426_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_729.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5275_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_729MethodDeclarations.h"

extern TypeInfo IContributeServerContextSink_t2686_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31751_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIContributeServerContextSink_t2686_m41741_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(System.Int32)
#define Array_InternalArray__get_Item_TisIContributeServerContextSink_t2686_m41741(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5275____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5275_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5275, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5275____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5275_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5275, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5275_FieldInfos[] =
{
	&InternalEnumerator_1_t5275____array_0_FieldInfo,
	&InternalEnumerator_1_t5275____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31748_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5275____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5275_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31748_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5275____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5275_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31751_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5275_PropertyInfos[] =
{
	&InternalEnumerator_1_t5275____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5275____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5275_InternalEnumerator_1__ctor_m31747_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31747_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31747_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5275_InternalEnumerator_1__ctor_m31747_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31747_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31748_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31748_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5275_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31748_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31749_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31749_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31749_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31750_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31750_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5275_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31750_GenericMethod/* genericMethod */

};
extern Il2CppType IContributeServerContextSink_t2686_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31751_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31751_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5275_il2cpp_TypeInfo/* declaring_type */
	, &IContributeServerContextSink_t2686_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31751_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5275_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31747_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31748_MethodInfo,
	&InternalEnumerator_1_Dispose_m31749_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31750_MethodInfo,
	&InternalEnumerator_1_get_Current_m31751_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31750_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31749_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5275_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31748_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31750_MethodInfo,
	&InternalEnumerator_1_Dispose_m31749_MethodInfo,
	&InternalEnumerator_1_get_Current_m31751_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5275_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7426_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5275_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7426_il2cpp_TypeInfo, 7},
};
extern TypeInfo IContributeServerContextSink_t2686_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5275_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31751_MethodInfo/* Method Usage */,
	&IContributeServerContextSink_t2686_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIContributeServerContextSink_t2686_m41741_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5275_0_0_0;
extern Il2CppType InternalEnumerator_1_t5275_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5275_GenericClass;
TypeInfo InternalEnumerator_1_t5275_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5275_MethodInfos/* methods */
	, InternalEnumerator_1_t5275_PropertyInfos/* properties */
	, InternalEnumerator_1_t5275_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5275_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5275_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5275_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5275_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5275_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5275_1_0_0/* this_arg */
	, InternalEnumerator_1_t5275_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5275_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5275_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5275)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9550_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
extern MethodInfo IList_1_get_Item_m53078_MethodInfo;
extern MethodInfo IList_1_set_Item_m53079_MethodInfo;
static PropertyInfo IList_1_t9550____Item_PropertyInfo = 
{
	&IList_1_t9550_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53078_MethodInfo/* get */
	, &IList_1_set_Item_m53079_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9550_PropertyInfos[] =
{
	&IList_1_t9550____Item_PropertyInfo,
	NULL
};
extern Il2CppType IContributeServerContextSink_t2686_0_0_0;
static ParameterInfo IList_1_t9550_IList_1_IndexOf_m53080_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IContributeServerContextSink_t2686_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53080_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53080_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9550_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9550_IList_1_IndexOf_m53080_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53080_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IContributeServerContextSink_t2686_0_0_0;
static ParameterInfo IList_1_t9550_IList_1_Insert_m53081_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IContributeServerContextSink_t2686_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53081_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53081_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9550_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9550_IList_1_Insert_m53081_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53081_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9550_IList_1_RemoveAt_m53082_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53082_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53082_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9550_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9550_IList_1_RemoveAt_m53082_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53082_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9550_IList_1_get_Item_m53078_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IContributeServerContextSink_t2686_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53078_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53078_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9550_il2cpp_TypeInfo/* declaring_type */
	, &IContributeServerContextSink_t2686_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9550_IList_1_get_Item_m53078_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53078_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IContributeServerContextSink_t2686_0_0_0;
static ParameterInfo IList_1_t9550_IList_1_set_Item_m53079_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IContributeServerContextSink_t2686_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53079_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53079_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9550_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9550_IList_1_set_Item_m53079_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53079_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9550_MethodInfos[] =
{
	&IList_1_IndexOf_m53080_MethodInfo,
	&IList_1_Insert_m53081_MethodInfo,
	&IList_1_RemoveAt_m53082_MethodInfo,
	&IList_1_get_Item_m53078_MethodInfo,
	&IList_1_set_Item_m53079_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9550_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9549_il2cpp_TypeInfo,
	&IEnumerable_1_t9551_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9550_0_0_0;
extern Il2CppType IList_1_t9550_1_0_0;
struct IList_1_t9550;
extern Il2CppGenericClass IList_1_t9550_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9550_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9550_MethodInfos/* methods */
	, IList_1_t9550_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9550_il2cpp_TypeInfo/* element_class */
	, IList_1_t9550_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9550_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9550_0_0_0/* byval_arg */
	, &IList_1_t9550_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9550_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7428_il2cpp_TypeInfo;

// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoType.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>
extern MethodInfo IEnumerator_1_get_Current_m53083_MethodInfo;
static PropertyInfo IEnumerator_1_t7428____Current_PropertyInfo = 
{
	&IEnumerator_1_t7428_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53083_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7428_PropertyInfos[] =
{
	&IEnumerator_1_t7428____Current_PropertyInfo,
	NULL
};
extern Il2CppType ArgInfoType_t2055_0_0_0;
extern void* RuntimeInvoker_ArgInfoType_t2055 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53083_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53083_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7428_il2cpp_TypeInfo/* declaring_type */
	, &ArgInfoType_t2055_0_0_0/* return_type */
	, RuntimeInvoker_ArgInfoType_t2055/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53083_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7428_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53083_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7428_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7428_0_0_0;
extern Il2CppType IEnumerator_1_t7428_1_0_0;
struct IEnumerator_1_t7428;
extern Il2CppGenericClass IEnumerator_1_t7428_GenericClass;
TypeInfo IEnumerator_1_t7428_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7428_MethodInfos/* methods */
	, IEnumerator_1_t7428_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7428_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7428_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7428_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7428_0_0_0/* byval_arg */
	, &IEnumerator_1_t7428_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7428_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_730.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5276_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_730MethodDeclarations.h"

extern TypeInfo ArgInfoType_t2055_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31756_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisArgInfoType_t2055_m41752_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Messaging.ArgInfoType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Messaging.ArgInfoType>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisArgInfoType_t2055_m41752 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31752_MethodInfo;
 void InternalEnumerator_1__ctor_m31752 (InternalEnumerator_1_t5276 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31753_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31753 (InternalEnumerator_1_t5276 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m31756(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31756_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ArgInfoType_t2055_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31754_MethodInfo;
 void InternalEnumerator_1_Dispose_m31754 (InternalEnumerator_1_t5276 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31755_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31755 (InternalEnumerator_1_t5276 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m31756 (InternalEnumerator_1_t5276 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisArgInfoType_t2055_m41752(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisArgInfoType_t2055_m41752_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5276____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5276_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5276, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5276____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5276_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5276, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5276_FieldInfos[] =
{
	&InternalEnumerator_1_t5276____array_0_FieldInfo,
	&InternalEnumerator_1_t5276____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5276____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5276_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31753_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5276____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5276_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31756_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5276_PropertyInfos[] =
{
	&InternalEnumerator_1_t5276____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5276____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5276_InternalEnumerator_1__ctor_m31752_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31752_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31752_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31752/* method */
	, &InternalEnumerator_1_t5276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5276_InternalEnumerator_1__ctor_m31752_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31752_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31753_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31753_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31753/* method */
	, &InternalEnumerator_1_t5276_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31753_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31754_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31754_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31754/* method */
	, &InternalEnumerator_1_t5276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31754_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31755_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31755_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31755/* method */
	, &InternalEnumerator_1_t5276_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31755_GenericMethod/* genericMethod */

};
extern Il2CppType ArgInfoType_t2055_0_0_0;
extern void* RuntimeInvoker_ArgInfoType_t2055 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31756_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31756_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31756/* method */
	, &InternalEnumerator_1_t5276_il2cpp_TypeInfo/* declaring_type */
	, &ArgInfoType_t2055_0_0_0/* return_type */
	, RuntimeInvoker_ArgInfoType_t2055/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31756_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5276_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31752_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31753_MethodInfo,
	&InternalEnumerator_1_Dispose_m31754_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31755_MethodInfo,
	&InternalEnumerator_1_get_Current_m31756_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5276_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31753_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31755_MethodInfo,
	&InternalEnumerator_1_Dispose_m31754_MethodInfo,
	&InternalEnumerator_1_get_Current_m31756_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5276_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7428_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5276_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7428_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5276_0_0_0;
extern Il2CppType InternalEnumerator_1_t5276_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5276_GenericClass;
TypeInfo InternalEnumerator_1_t5276_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5276_MethodInfos/* methods */
	, InternalEnumerator_1_t5276_PropertyInfos/* properties */
	, InternalEnumerator_1_t5276_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5276_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5276_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5276_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5276_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5276_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5276_1_0_0/* this_arg */
	, InternalEnumerator_1_t5276_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5276_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5276)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9552_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>
extern MethodInfo ICollection_1_get_Count_m53084_MethodInfo;
static PropertyInfo ICollection_1_t9552____Count_PropertyInfo = 
{
	&ICollection_1_t9552_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53084_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53085_MethodInfo;
static PropertyInfo ICollection_1_t9552____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9552_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53085_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9552_PropertyInfos[] =
{
	&ICollection_1_t9552____Count_PropertyInfo,
	&ICollection_1_t9552____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53084_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::get_Count()
MethodInfo ICollection_1_get_Count_m53084_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9552_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53084_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53085_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53085_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9552_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53085_GenericMethod/* genericMethod */

};
extern Il2CppType ArgInfoType_t2055_0_0_0;
extern Il2CppType ArgInfoType_t2055_0_0_0;
static ParameterInfo ICollection_1_t9552_ICollection_1_Add_m53086_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ArgInfoType_t2055_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53086_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Add(T)
MethodInfo ICollection_1_Add_m53086_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Byte_t838/* invoker_method */
	, ICollection_1_t9552_ICollection_1_Add_m53086_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53086_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53087_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Clear()
MethodInfo ICollection_1_Clear_m53087_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53087_GenericMethod/* genericMethod */

};
extern Il2CppType ArgInfoType_t2055_0_0_0;
static ParameterInfo ICollection_1_t9552_ICollection_1_Contains_m53088_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ArgInfoType_t2055_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53088_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Contains(T)
MethodInfo ICollection_1_Contains_m53088_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9552_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9552_ICollection_1_Contains_m53088_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53088_GenericMethod/* genericMethod */

};
extern Il2CppType ArgInfoTypeU5BU5D_t5574_0_0_0;
extern Il2CppType ArgInfoTypeU5BU5D_t5574_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9552_ICollection_1_CopyTo_m53089_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ArgInfoTypeU5BU5D_t5574_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53089_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53089_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9552_ICollection_1_CopyTo_m53089_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53089_GenericMethod/* genericMethod */

};
extern Il2CppType ArgInfoType_t2055_0_0_0;
static ParameterInfo ICollection_1_t9552_ICollection_1_Remove_m53090_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ArgInfoType_t2055_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53090_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Remove(T)
MethodInfo ICollection_1_Remove_m53090_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9552_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9552_ICollection_1_Remove_m53090_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53090_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9552_MethodInfos[] =
{
	&ICollection_1_get_Count_m53084_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53085_MethodInfo,
	&ICollection_1_Add_m53086_MethodInfo,
	&ICollection_1_Clear_m53087_MethodInfo,
	&ICollection_1_Contains_m53088_MethodInfo,
	&ICollection_1_CopyTo_m53089_MethodInfo,
	&ICollection_1_Remove_m53090_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9554_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9552_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9554_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9552_0_0_0;
extern Il2CppType ICollection_1_t9552_1_0_0;
struct ICollection_1_t9552;
extern Il2CppGenericClass ICollection_1_t9552_GenericClass;
TypeInfo ICollection_1_t9552_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9552_MethodInfos/* methods */
	, ICollection_1_t9552_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9552_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9552_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9552_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9552_0_0_0/* byval_arg */
	, &ICollection_1_t9552_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9552_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Messaging.ArgInfoType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Messaging.ArgInfoType>
extern Il2CppType IEnumerator_1_t7428_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53091_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Messaging.ArgInfoType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53091_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9554_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7428_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53091_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9554_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53091_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9554_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9554_0_0_0;
extern Il2CppType IEnumerable_1_t9554_1_0_0;
struct IEnumerable_1_t9554;
extern Il2CppGenericClass IEnumerable_1_t9554_GenericClass;
TypeInfo IEnumerable_1_t9554_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9554_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9554_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9554_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9554_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9554_0_0_0/* byval_arg */
	, &IEnumerable_1_t9554_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9554_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9553_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>
extern MethodInfo IList_1_get_Item_m53092_MethodInfo;
extern MethodInfo IList_1_set_Item_m53093_MethodInfo;
static PropertyInfo IList_1_t9553____Item_PropertyInfo = 
{
	&IList_1_t9553_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53092_MethodInfo/* get */
	, &IList_1_set_Item_m53093_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9553_PropertyInfos[] =
{
	&IList_1_t9553____Item_PropertyInfo,
	NULL
};
extern Il2CppType ArgInfoType_t2055_0_0_0;
static ParameterInfo IList_1_t9553_IList_1_IndexOf_m53094_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ArgInfoType_t2055_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53094_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53094_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9553_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9553_IList_1_IndexOf_m53094_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53094_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ArgInfoType_t2055_0_0_0;
static ParameterInfo IList_1_t9553_IList_1_Insert_m53095_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ArgInfoType_t2055_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53095_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53095_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9553_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9553_IList_1_Insert_m53095_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53095_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9553_IList_1_RemoveAt_m53096_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53096_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53096_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9553_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9553_IList_1_RemoveAt_m53096_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53096_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9553_IList_1_get_Item_m53092_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ArgInfoType_t2055_0_0_0;
extern void* RuntimeInvoker_ArgInfoType_t2055_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53092_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53092_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9553_il2cpp_TypeInfo/* declaring_type */
	, &ArgInfoType_t2055_0_0_0/* return_type */
	, RuntimeInvoker_ArgInfoType_t2055_Int32_t123/* invoker_method */
	, IList_1_t9553_IList_1_get_Item_m53092_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53092_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ArgInfoType_t2055_0_0_0;
static ParameterInfo IList_1_t9553_IList_1_set_Item_m53093_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ArgInfoType_t2055_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53093_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53093_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9553_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9553_IList_1_set_Item_m53093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53093_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9553_MethodInfos[] =
{
	&IList_1_IndexOf_m53094_MethodInfo,
	&IList_1_Insert_m53095_MethodInfo,
	&IList_1_RemoveAt_m53096_MethodInfo,
	&IList_1_get_Item_m53092_MethodInfo,
	&IList_1_set_Item_m53093_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9553_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9552_il2cpp_TypeInfo,
	&IEnumerable_1_t9554_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9553_0_0_0;
extern Il2CppType IList_1_t9553_1_0_0;
struct IList_1_t9553;
extern Il2CppGenericClass IList_1_t9553_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9553_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9553_MethodInfos/* methods */
	, IList_1_t9553_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9553_il2cpp_TypeInfo/* element_class */
	, IList_1_t9553_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9553_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9553_0_0_0/* byval_arg */
	, &IList_1_t9553_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9553_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7430_il2cpp_TypeInfo;

// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_Header.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Messaging.Header>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Messaging.Header>
extern MethodInfo IEnumerator_1_get_Current_m53097_MethodInfo;
static PropertyInfo IEnumerator_1_t7430____Current_PropertyInfo = 
{
	&IEnumerator_1_t7430_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53097_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7430_PropertyInfos[] =
{
	&IEnumerator_1_t7430____Current_PropertyInfo,
	NULL
};
extern Il2CppType Header_t2066_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53097_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Messaging.Header>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53097_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7430_il2cpp_TypeInfo/* declaring_type */
	, &Header_t2066_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53097_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7430_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53097_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7430_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7430_0_0_0;
extern Il2CppType IEnumerator_1_t7430_1_0_0;
struct IEnumerator_1_t7430;
extern Il2CppGenericClass IEnumerator_1_t7430_GenericClass;
TypeInfo IEnumerator_1_t7430_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7430_MethodInfos/* methods */
	, IEnumerator_1_t7430_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7430_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7430_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7430_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7430_0_0_0/* byval_arg */
	, &IEnumerator_1_t7430_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7430_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_731.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5277_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_731MethodDeclarations.h"

extern TypeInfo Header_t2066_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31761_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHeader_t2066_m41763_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Messaging.Header>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Messaging.Header>(System.Int32)
#define Array_InternalArray__get_Item_TisHeader_t2066_m41763(__this, p0, method) (Header_t2066 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5277____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5277_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5277, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5277____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5277_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5277, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5277_FieldInfos[] =
{
	&InternalEnumerator_1_t5277____array_0_FieldInfo,
	&InternalEnumerator_1_t5277____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31758_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5277____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5277_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31758_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5277____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5277_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31761_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5277_PropertyInfos[] =
{
	&InternalEnumerator_1_t5277____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5277____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5277_InternalEnumerator_1__ctor_m31757_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31757_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31757_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5277_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5277_InternalEnumerator_1__ctor_m31757_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31757_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31758_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31758_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5277_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31758_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31759_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31759_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5277_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31759_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31760_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31760_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5277_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31760_GenericMethod/* genericMethod */

};
extern Il2CppType Header_t2066_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31761_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31761_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5277_il2cpp_TypeInfo/* declaring_type */
	, &Header_t2066_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31761_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5277_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31757_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31758_MethodInfo,
	&InternalEnumerator_1_Dispose_m31759_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31760_MethodInfo,
	&InternalEnumerator_1_get_Current_m31761_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31760_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31759_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5277_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31758_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31760_MethodInfo,
	&InternalEnumerator_1_Dispose_m31759_MethodInfo,
	&InternalEnumerator_1_get_Current_m31761_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5277_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7430_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5277_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7430_il2cpp_TypeInfo, 7},
};
extern TypeInfo Header_t2066_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5277_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31761_MethodInfo/* Method Usage */,
	&Header_t2066_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisHeader_t2066_m41763_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5277_0_0_0;
extern Il2CppType InternalEnumerator_1_t5277_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5277_GenericClass;
TypeInfo InternalEnumerator_1_t5277_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5277_MethodInfos/* methods */
	, InternalEnumerator_1_t5277_PropertyInfos/* properties */
	, InternalEnumerator_1_t5277_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5277_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5277_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5277_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5277_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5277_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5277_1_0_0/* this_arg */
	, InternalEnumerator_1_t5277_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5277_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5277_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5277)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9555_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>
extern MethodInfo ICollection_1_get_Count_m53098_MethodInfo;
static PropertyInfo ICollection_1_t9555____Count_PropertyInfo = 
{
	&ICollection_1_t9555_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53098_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53099_MethodInfo;
static PropertyInfo ICollection_1_t9555____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9555_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53099_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9555_PropertyInfos[] =
{
	&ICollection_1_t9555____Count_PropertyInfo,
	&ICollection_1_t9555____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53098_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::get_Count()
MethodInfo ICollection_1_get_Count_m53098_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9555_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53098_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53099_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53099_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9555_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53099_GenericMethod/* genericMethod */

};
extern Il2CppType Header_t2066_0_0_0;
extern Il2CppType Header_t2066_0_0_0;
static ParameterInfo ICollection_1_t9555_ICollection_1_Add_m53100_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Header_t2066_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53100_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::Add(T)
MethodInfo ICollection_1_Add_m53100_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9555_ICollection_1_Add_m53100_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53100_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53101_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::Clear()
MethodInfo ICollection_1_Clear_m53101_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53101_GenericMethod/* genericMethod */

};
extern Il2CppType Header_t2066_0_0_0;
static ParameterInfo ICollection_1_t9555_ICollection_1_Contains_m53102_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Header_t2066_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53102_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::Contains(T)
MethodInfo ICollection_1_Contains_m53102_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9555_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9555_ICollection_1_Contains_m53102_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53102_GenericMethod/* genericMethod */

};
extern Il2CppType HeaderU5BU5D_t2069_0_0_0;
extern Il2CppType HeaderU5BU5D_t2069_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9555_ICollection_1_CopyTo_m53103_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HeaderU5BU5D_t2069_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53103_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53103_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9555_ICollection_1_CopyTo_m53103_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53103_GenericMethod/* genericMethod */

};
extern Il2CppType Header_t2066_0_0_0;
static ParameterInfo ICollection_1_t9555_ICollection_1_Remove_m53104_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Header_t2066_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53104_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>::Remove(T)
MethodInfo ICollection_1_Remove_m53104_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9555_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9555_ICollection_1_Remove_m53104_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53104_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9555_MethodInfos[] =
{
	&ICollection_1_get_Count_m53098_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53099_MethodInfo,
	&ICollection_1_Add_m53100_MethodInfo,
	&ICollection_1_Clear_m53101_MethodInfo,
	&ICollection_1_Contains_m53102_MethodInfo,
	&ICollection_1_CopyTo_m53103_MethodInfo,
	&ICollection_1_Remove_m53104_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9557_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9555_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9557_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9555_0_0_0;
extern Il2CppType ICollection_1_t9555_1_0_0;
struct ICollection_1_t9555;
extern Il2CppGenericClass ICollection_1_t9555_GenericClass;
TypeInfo ICollection_1_t9555_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9555_MethodInfos/* methods */
	, ICollection_1_t9555_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9555_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9555_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9555_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9555_0_0_0/* byval_arg */
	, &ICollection_1_t9555_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9555_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Messaging.Header>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Messaging.Header>
extern Il2CppType IEnumerator_1_t7430_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53105_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Messaging.Header>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53105_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9557_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7430_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53105_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9557_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53105_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9557_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9557_0_0_0;
extern Il2CppType IEnumerable_1_t9557_1_0_0;
struct IEnumerable_1_t9557;
extern Il2CppGenericClass IEnumerable_1_t9557_GenericClass;
TypeInfo IEnumerable_1_t9557_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9557_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9557_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9557_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9557_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9557_0_0_0/* byval_arg */
	, &IEnumerable_1_t9557_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9557_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9556_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>
extern MethodInfo IList_1_get_Item_m53106_MethodInfo;
extern MethodInfo IList_1_set_Item_m53107_MethodInfo;
static PropertyInfo IList_1_t9556____Item_PropertyInfo = 
{
	&IList_1_t9556_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53106_MethodInfo/* get */
	, &IList_1_set_Item_m53107_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9556_PropertyInfos[] =
{
	&IList_1_t9556____Item_PropertyInfo,
	NULL
};
extern Il2CppType Header_t2066_0_0_0;
static ParameterInfo IList_1_t9556_IList_1_IndexOf_m53108_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Header_t2066_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53108_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53108_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9556_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9556_IList_1_IndexOf_m53108_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53108_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Header_t2066_0_0_0;
static ParameterInfo IList_1_t9556_IList_1_Insert_m53109_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Header_t2066_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53109_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53109_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9556_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9556_IList_1_Insert_m53109_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53109_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9556_IList_1_RemoveAt_m53110_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53110_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53110_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9556_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9556_IList_1_RemoveAt_m53110_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53110_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9556_IList_1_get_Item_m53106_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Header_t2066_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53106_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53106_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9556_il2cpp_TypeInfo/* declaring_type */
	, &Header_t2066_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9556_IList_1_get_Item_m53106_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53106_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Header_t2066_0_0_0;
static ParameterInfo IList_1_t9556_IList_1_set_Item_m53107_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Header_t2066_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53107_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53107_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9556_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9556_IList_1_set_Item_m53107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53107_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9556_MethodInfos[] =
{
	&IList_1_IndexOf_m53108_MethodInfo,
	&IList_1_Insert_m53109_MethodInfo,
	&IList_1_RemoveAt_m53110_MethodInfo,
	&IList_1_get_Item_m53106_MethodInfo,
	&IList_1_set_Item_m53107_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9556_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9555_il2cpp_TypeInfo,
	&IEnumerable_1_t9557_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9556_0_0_0;
extern Il2CppType IList_1_t9556_1_0_0;
struct IList_1_t9556;
extern Il2CppGenericClass IList_1_t9556_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9556_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9556_MethodInfos/* methods */
	, IList_1_t9556_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9556_il2cpp_TypeInfo/* element_class */
	, IList_1_t9556_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9556_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9556_0_0_0/* byval_arg */
	, &IList_1_t9556_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9556_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7432_il2cpp_TypeInfo;

// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
extern MethodInfo IEnumerator_1_get_Current_m53111_MethodInfo;
static PropertyInfo IEnumerator_1_t7432____Current_PropertyInfo = 
{
	&IEnumerator_1_t7432_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53111_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7432_PropertyInfos[] =
{
	&IEnumerator_1_t7432____Current_PropertyInfo,
	NULL
};
extern Il2CppType ProxyAttribute_t2082_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53111_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53111_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7432_il2cpp_TypeInfo/* declaring_type */
	, &ProxyAttribute_t2082_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53111_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7432_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53111_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7432_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7432_0_0_0;
extern Il2CppType IEnumerator_1_t7432_1_0_0;
struct IEnumerator_1_t7432;
extern Il2CppGenericClass IEnumerator_1_t7432_GenericClass;
TypeInfo IEnumerator_1_t7432_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7432_MethodInfos/* methods */
	, IEnumerator_1_t7432_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7432_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7432_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7432_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7432_0_0_0/* byval_arg */
	, &IEnumerator_1_t7432_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7432_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_732.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5278_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_732MethodDeclarations.h"

extern TypeInfo ProxyAttribute_t2082_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31766_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisProxyAttribute_t2082_m41774_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Proxies.ProxyAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Proxies.ProxyAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisProxyAttribute_t2082_m41774(__this, p0, method) (ProxyAttribute_t2082 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5278____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5278_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5278, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5278____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5278_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5278, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5278_FieldInfos[] =
{
	&InternalEnumerator_1_t5278____array_0_FieldInfo,
	&InternalEnumerator_1_t5278____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31763_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5278____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5278_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31763_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5278____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5278_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31766_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5278_PropertyInfos[] =
{
	&InternalEnumerator_1_t5278____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5278____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5278_InternalEnumerator_1__ctor_m31762_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31762_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31762_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5278_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5278_InternalEnumerator_1__ctor_m31762_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31762_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31763_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31763_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5278_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31763_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31764_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31764_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5278_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31764_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31765_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31765_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5278_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31765_GenericMethod/* genericMethod */

};
extern Il2CppType ProxyAttribute_t2082_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31766_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31766_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5278_il2cpp_TypeInfo/* declaring_type */
	, &ProxyAttribute_t2082_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31766_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5278_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31762_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31763_MethodInfo,
	&InternalEnumerator_1_Dispose_m31764_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31765_MethodInfo,
	&InternalEnumerator_1_get_Current_m31766_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31765_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31764_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5278_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31763_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31765_MethodInfo,
	&InternalEnumerator_1_Dispose_m31764_MethodInfo,
	&InternalEnumerator_1_get_Current_m31766_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5278_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7432_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5278_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7432_il2cpp_TypeInfo, 7},
};
extern TypeInfo ProxyAttribute_t2082_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5278_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31766_MethodInfo/* Method Usage */,
	&ProxyAttribute_t2082_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisProxyAttribute_t2082_m41774_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5278_0_0_0;
extern Il2CppType InternalEnumerator_1_t5278_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5278_GenericClass;
TypeInfo InternalEnumerator_1_t5278_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5278_MethodInfos/* methods */
	, InternalEnumerator_1_t5278_PropertyInfos/* properties */
	, InternalEnumerator_1_t5278_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5278_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5278_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5278_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5278_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5278_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5278_1_0_0/* this_arg */
	, InternalEnumerator_1_t5278_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5278_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5278_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5278)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9558_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
extern MethodInfo ICollection_1_get_Count_m53112_MethodInfo;
static PropertyInfo ICollection_1_t9558____Count_PropertyInfo = 
{
	&ICollection_1_t9558_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53112_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53113_MethodInfo;
static PropertyInfo ICollection_1_t9558____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9558_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53113_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9558_PropertyInfos[] =
{
	&ICollection_1_t9558____Count_PropertyInfo,
	&ICollection_1_t9558____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53112_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m53112_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9558_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53112_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53113_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53113_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9558_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53113_GenericMethod/* genericMethod */

};
extern Il2CppType ProxyAttribute_t2082_0_0_0;
extern Il2CppType ProxyAttribute_t2082_0_0_0;
static ParameterInfo ICollection_1_t9558_ICollection_1_Add_m53114_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ProxyAttribute_t2082_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53114_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Add(T)
MethodInfo ICollection_1_Add_m53114_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9558_ICollection_1_Add_m53114_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53114_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53115_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Clear()
MethodInfo ICollection_1_Clear_m53115_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53115_GenericMethod/* genericMethod */

};
extern Il2CppType ProxyAttribute_t2082_0_0_0;
static ParameterInfo ICollection_1_t9558_ICollection_1_Contains_m53116_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ProxyAttribute_t2082_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53116_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m53116_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9558_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9558_ICollection_1_Contains_m53116_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53116_GenericMethod/* genericMethod */

};
extern Il2CppType ProxyAttributeU5BU5D_t5575_0_0_0;
extern Il2CppType ProxyAttributeU5BU5D_t5575_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9558_ICollection_1_CopyTo_m53117_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ProxyAttributeU5BU5D_t5575_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53117_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53117_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9558_ICollection_1_CopyTo_m53117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53117_GenericMethod/* genericMethod */

};
extern Il2CppType ProxyAttribute_t2082_0_0_0;
static ParameterInfo ICollection_1_t9558_ICollection_1_Remove_m53118_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ProxyAttribute_t2082_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53118_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m53118_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9558_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9558_ICollection_1_Remove_m53118_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53118_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9558_MethodInfos[] =
{
	&ICollection_1_get_Count_m53112_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53113_MethodInfo,
	&ICollection_1_Add_m53114_MethodInfo,
	&ICollection_1_Clear_m53115_MethodInfo,
	&ICollection_1_Contains_m53116_MethodInfo,
	&ICollection_1_CopyTo_m53117_MethodInfo,
	&ICollection_1_Remove_m53118_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9560_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9558_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9560_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9558_0_0_0;
extern Il2CppType ICollection_1_t9558_1_0_0;
struct ICollection_1_t9558;
extern Il2CppGenericClass ICollection_1_t9558_GenericClass;
TypeInfo ICollection_1_t9558_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9558_MethodInfos/* methods */
	, ICollection_1_t9558_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9558_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9558_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9558_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9558_0_0_0/* byval_arg */
	, &ICollection_1_t9558_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9558_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
extern Il2CppType IEnumerator_1_t7432_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53119_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53119_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9560_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7432_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53119_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9560_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53119_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9560_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9560_0_0_0;
extern Il2CppType IEnumerable_1_t9560_1_0_0;
struct IEnumerable_1_t9560;
extern Il2CppGenericClass IEnumerable_1_t9560_GenericClass;
TypeInfo IEnumerable_1_t9560_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9560_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9560_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9560_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9560_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9560_0_0_0/* byval_arg */
	, &IEnumerable_1_t9560_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9560_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9559_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
extern MethodInfo IList_1_get_Item_m53120_MethodInfo;
extern MethodInfo IList_1_set_Item_m53121_MethodInfo;
static PropertyInfo IList_1_t9559____Item_PropertyInfo = 
{
	&IList_1_t9559_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53120_MethodInfo/* get */
	, &IList_1_set_Item_m53121_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9559_PropertyInfos[] =
{
	&IList_1_t9559____Item_PropertyInfo,
	NULL
};
extern Il2CppType ProxyAttribute_t2082_0_0_0;
static ParameterInfo IList_1_t9559_IList_1_IndexOf_m53122_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ProxyAttribute_t2082_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53122_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53122_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9559_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9559_IList_1_IndexOf_m53122_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53122_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ProxyAttribute_t2082_0_0_0;
static ParameterInfo IList_1_t9559_IList_1_Insert_m53123_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ProxyAttribute_t2082_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53123_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53123_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9559_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9559_IList_1_Insert_m53123_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53123_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9559_IList_1_RemoveAt_m53124_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53124_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53124_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9559_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9559_IList_1_RemoveAt_m53124_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53124_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9559_IList_1_get_Item_m53120_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ProxyAttribute_t2082_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53120_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53120_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9559_il2cpp_TypeInfo/* declaring_type */
	, &ProxyAttribute_t2082_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9559_IList_1_get_Item_m53120_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53120_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ProxyAttribute_t2082_0_0_0;
static ParameterInfo IList_1_t9559_IList_1_set_Item_m53121_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ProxyAttribute_t2082_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53121_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53121_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9559_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9559_IList_1_set_Item_m53121_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53121_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9559_MethodInfos[] =
{
	&IList_1_IndexOf_m53122_MethodInfo,
	&IList_1_Insert_m53123_MethodInfo,
	&IList_1_RemoveAt_m53124_MethodInfo,
	&IList_1_get_Item_m53120_MethodInfo,
	&IList_1_set_Item_m53121_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9559_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9558_il2cpp_TypeInfo,
	&IEnumerable_1_t9560_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9559_0_0_0;
extern Il2CppType IList_1_t9559_1_0_0;
struct IList_1_t9559;
extern Il2CppGenericClass IList_1_t9559_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9559_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9559_MethodInfos/* methods */
	, IList_1_t9559_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9559_il2cpp_TypeInfo/* element_class */
	, IList_1_t9559_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9559_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9559_0_0_0/* byval_arg */
	, &IList_1_t9559_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9559_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7434_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>
extern MethodInfo IEnumerator_1_get_Current_m53125_MethodInfo;
static PropertyInfo IEnumerator_1_t7434____Current_PropertyInfo = 
{
	&IEnumerator_1_t7434_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53125_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7434_PropertyInfos[] =
{
	&IEnumerator_1_t7434____Current_PropertyInfo,
	NULL
};
extern Il2CppType ITrackingHandler_t2692_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53125_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53125_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7434_il2cpp_TypeInfo/* declaring_type */
	, &ITrackingHandler_t2692_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53125_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7434_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53125_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7434_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7434_0_0_0;
extern Il2CppType IEnumerator_1_t7434_1_0_0;
struct IEnumerator_1_t7434;
extern Il2CppGenericClass IEnumerator_1_t7434_GenericClass;
TypeInfo IEnumerator_1_t7434_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7434_MethodInfos/* methods */
	, IEnumerator_1_t7434_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7434_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7434_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7434_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7434_0_0_0/* byval_arg */
	, &IEnumerator_1_t7434_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7434_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_733.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5279_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_733MethodDeclarations.h"

extern TypeInfo ITrackingHandler_t2692_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31771_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisITrackingHandler_t2692_m41785_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Services.ITrackingHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.Services.ITrackingHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisITrackingHandler_t2692_m41785(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5279____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5279_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5279, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5279____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5279_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5279, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5279_FieldInfos[] =
{
	&InternalEnumerator_1_t5279____array_0_FieldInfo,
	&InternalEnumerator_1_t5279____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31768_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5279____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5279_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31768_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5279____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5279_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31771_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5279_PropertyInfos[] =
{
	&InternalEnumerator_1_t5279____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5279____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5279_InternalEnumerator_1__ctor_m31767_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31767_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31767_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5279_InternalEnumerator_1__ctor_m31767_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31767_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31768_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31768_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5279_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31768_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31769_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31769_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31769_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31770_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31770_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5279_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31770_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackingHandler_t2692_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31771_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31771_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5279_il2cpp_TypeInfo/* declaring_type */
	, &ITrackingHandler_t2692_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31771_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5279_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31767_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31768_MethodInfo,
	&InternalEnumerator_1_Dispose_m31769_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31770_MethodInfo,
	&InternalEnumerator_1_get_Current_m31771_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31770_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31769_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5279_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31768_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31770_MethodInfo,
	&InternalEnumerator_1_Dispose_m31769_MethodInfo,
	&InternalEnumerator_1_get_Current_m31771_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5279_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7434_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5279_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7434_il2cpp_TypeInfo, 7},
};
extern TypeInfo ITrackingHandler_t2692_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5279_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31771_MethodInfo/* Method Usage */,
	&ITrackingHandler_t2692_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisITrackingHandler_t2692_m41785_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5279_0_0_0;
extern Il2CppType InternalEnumerator_1_t5279_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5279_GenericClass;
TypeInfo InternalEnumerator_1_t5279_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5279_MethodInfos/* methods */
	, InternalEnumerator_1_t5279_PropertyInfos/* properties */
	, InternalEnumerator_1_t5279_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5279_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5279_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5279_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5279_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5279_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5279_1_0_0/* this_arg */
	, InternalEnumerator_1_t5279_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5279_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5279_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5279)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9561_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>
extern MethodInfo ICollection_1_get_Count_m53126_MethodInfo;
static PropertyInfo ICollection_1_t9561____Count_PropertyInfo = 
{
	&ICollection_1_t9561_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53126_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53127_MethodInfo;
static PropertyInfo ICollection_1_t9561____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9561_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53127_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9561_PropertyInfos[] =
{
	&ICollection_1_t9561____Count_PropertyInfo,
	&ICollection_1_t9561____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53126_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m53126_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9561_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53126_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53127_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53127_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9561_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53127_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackingHandler_t2692_0_0_0;
extern Il2CppType ITrackingHandler_t2692_0_0_0;
static ParameterInfo ICollection_1_t9561_ICollection_1_Add_m53128_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackingHandler_t2692_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53128_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::Add(T)
MethodInfo ICollection_1_Add_m53128_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9561_ICollection_1_Add_m53128_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53128_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53129_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::Clear()
MethodInfo ICollection_1_Clear_m53129_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53129_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackingHandler_t2692_0_0_0;
static ParameterInfo ICollection_1_t9561_ICollection_1_Contains_m53130_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackingHandler_t2692_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53130_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m53130_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9561_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9561_ICollection_1_Contains_m53130_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53130_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackingHandlerU5BU5D_t2693_0_0_0;
extern Il2CppType ITrackingHandlerU5BU5D_t2693_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9561_ICollection_1_CopyTo_m53131_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ITrackingHandlerU5BU5D_t2693_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53131_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53131_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9561_ICollection_1_CopyTo_m53131_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53131_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackingHandler_t2692_0_0_0;
static ParameterInfo ICollection_1_t9561_ICollection_1_Remove_m53132_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackingHandler_t2692_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53132_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m53132_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9561_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9561_ICollection_1_Remove_m53132_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53132_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9561_MethodInfos[] =
{
	&ICollection_1_get_Count_m53126_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53127_MethodInfo,
	&ICollection_1_Add_m53128_MethodInfo,
	&ICollection_1_Clear_m53129_MethodInfo,
	&ICollection_1_Contains_m53130_MethodInfo,
	&ICollection_1_CopyTo_m53131_MethodInfo,
	&ICollection_1_Remove_m53132_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9563_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9561_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9563_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9561_0_0_0;
extern Il2CppType ICollection_1_t9561_1_0_0;
struct ICollection_1_t9561;
extern Il2CppGenericClass ICollection_1_t9561_GenericClass;
TypeInfo ICollection_1_t9561_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9561_MethodInfos/* methods */
	, ICollection_1_t9561_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9561_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9561_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9561_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9561_0_0_0/* byval_arg */
	, &ICollection_1_t9561_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9561_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Services.ITrackingHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Services.ITrackingHandler>
extern Il2CppType IEnumerator_1_t7434_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53133_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Services.ITrackingHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53133_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9563_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53133_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9563_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53133_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9563_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9563_0_0_0;
extern Il2CppType IEnumerable_1_t9563_1_0_0;
struct IEnumerable_1_t9563;
extern Il2CppGenericClass IEnumerable_1_t9563_GenericClass;
TypeInfo IEnumerable_1_t9563_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9563_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9563_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9563_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9563_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9563_0_0_0/* byval_arg */
	, &IEnumerable_1_t9563_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9563_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9562_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>
extern MethodInfo IList_1_get_Item_m53134_MethodInfo;
extern MethodInfo IList_1_set_Item_m53135_MethodInfo;
static PropertyInfo IList_1_t9562____Item_PropertyInfo = 
{
	&IList_1_t9562_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53134_MethodInfo/* get */
	, &IList_1_set_Item_m53135_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9562_PropertyInfos[] =
{
	&IList_1_t9562____Item_PropertyInfo,
	NULL
};
extern Il2CppType ITrackingHandler_t2692_0_0_0;
static ParameterInfo IList_1_t9562_IList_1_IndexOf_m53136_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackingHandler_t2692_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53136_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53136_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9562_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9562_IList_1_IndexOf_m53136_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53136_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ITrackingHandler_t2692_0_0_0;
static ParameterInfo IList_1_t9562_IList_1_Insert_m53137_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ITrackingHandler_t2692_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53137_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53137_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9562_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9562_IList_1_Insert_m53137_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53137_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9562_IList_1_RemoveAt_m53138_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53138_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53138_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9562_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9562_IList_1_RemoveAt_m53138_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53138_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9562_IList_1_get_Item_m53134_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ITrackingHandler_t2692_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53134_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53134_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9562_il2cpp_TypeInfo/* declaring_type */
	, &ITrackingHandler_t2692_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9562_IList_1_get_Item_m53134_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53134_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ITrackingHandler_t2692_0_0_0;
static ParameterInfo IList_1_t9562_IList_1_set_Item_m53135_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ITrackingHandler_t2692_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53135_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53135_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9562_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9562_IList_1_set_Item_m53135_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53135_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9562_MethodInfos[] =
{
	&IList_1_IndexOf_m53136_MethodInfo,
	&IList_1_Insert_m53137_MethodInfo,
	&IList_1_RemoveAt_m53138_MethodInfo,
	&IList_1_get_Item_m53134_MethodInfo,
	&IList_1_set_Item_m53135_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9562_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9561_il2cpp_TypeInfo,
	&IEnumerable_1_t9563_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9562_0_0_0;
extern Il2CppType IList_1_t9562_1_0_0;
struct IList_1_t9562;
extern Il2CppGenericClass IList_1_t9562_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9562_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9562_MethodInfos/* methods */
	, IList_1_t9562_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9562_il2cpp_TypeInfo/* element_class */
	, IList_1_t9562_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9562_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9562_0_0_0/* byval_arg */
	, &IList_1_t9562_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9562_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7436_il2cpp_TypeInfo;

// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>
extern MethodInfo IEnumerator_1_get_Current_m53139_MethodInfo;
static PropertyInfo IEnumerator_1_t7436____Current_PropertyInfo = 
{
	&IEnumerator_1_t7436_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53139_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7436_PropertyInfos[] =
{
	&IEnumerator_1_t7436____Current_PropertyInfo,
	NULL
};
extern Il2CppType WellKnownObjectMode_t2106_0_0_0;
extern void* RuntimeInvoker_WellKnownObjectMode_t2106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53139_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53139_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7436_il2cpp_TypeInfo/* declaring_type */
	, &WellKnownObjectMode_t2106_0_0_0/* return_type */
	, RuntimeInvoker_WellKnownObjectMode_t2106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53139_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7436_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53139_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7436_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7436_0_0_0;
extern Il2CppType IEnumerator_1_t7436_1_0_0;
struct IEnumerator_1_t7436;
extern Il2CppGenericClass IEnumerator_1_t7436_GenericClass;
TypeInfo IEnumerator_1_t7436_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7436_MethodInfos/* methods */
	, IEnumerator_1_t7436_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7436_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7436_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7436_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7436_0_0_0/* byval_arg */
	, &IEnumerator_1_t7436_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7436_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_734.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5280_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_734MethodDeclarations.h"

extern TypeInfo WellKnownObjectMode_t2106_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31776_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWellKnownObjectMode_t2106_m41796_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.WellKnownObjectMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Remoting.WellKnownObjectMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisWellKnownObjectMode_t2106_m41796 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31772_MethodInfo;
 void InternalEnumerator_1__ctor_m31772 (InternalEnumerator_1_t5280 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31773_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31773 (InternalEnumerator_1_t5280 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31776(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31776_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&WellKnownObjectMode_t2106_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31774_MethodInfo;
 void InternalEnumerator_1_Dispose_m31774 (InternalEnumerator_1_t5280 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31775_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31775 (InternalEnumerator_1_t5280 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31776 (InternalEnumerator_1_t5280 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisWellKnownObjectMode_t2106_m41796(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisWellKnownObjectMode_t2106_m41796_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5280____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5280_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5280, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5280____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5280_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5280, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5280_FieldInfos[] =
{
	&InternalEnumerator_1_t5280____array_0_FieldInfo,
	&InternalEnumerator_1_t5280____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5280____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5280_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31773_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5280____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5280_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31776_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5280_PropertyInfos[] =
{
	&InternalEnumerator_1_t5280____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5280____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5280_InternalEnumerator_1__ctor_m31772_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31772_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31772_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31772/* method */
	, &InternalEnumerator_1_t5280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5280_InternalEnumerator_1__ctor_m31772_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31772_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31773_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31773_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31773/* method */
	, &InternalEnumerator_1_t5280_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31773_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31774_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31774_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31774/* method */
	, &InternalEnumerator_1_t5280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31774_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31775_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31775_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31775/* method */
	, &InternalEnumerator_1_t5280_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31775_GenericMethod/* genericMethod */

};
extern Il2CppType WellKnownObjectMode_t2106_0_0_0;
extern void* RuntimeInvoker_WellKnownObjectMode_t2106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31776_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31776_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31776/* method */
	, &InternalEnumerator_1_t5280_il2cpp_TypeInfo/* declaring_type */
	, &WellKnownObjectMode_t2106_0_0_0/* return_type */
	, RuntimeInvoker_WellKnownObjectMode_t2106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31776_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5280_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31772_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31773_MethodInfo,
	&InternalEnumerator_1_Dispose_m31774_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31775_MethodInfo,
	&InternalEnumerator_1_get_Current_m31776_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5280_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31773_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31775_MethodInfo,
	&InternalEnumerator_1_Dispose_m31774_MethodInfo,
	&InternalEnumerator_1_get_Current_m31776_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5280_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7436_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5280_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7436_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5280_0_0_0;
extern Il2CppType InternalEnumerator_1_t5280_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5280_GenericClass;
TypeInfo InternalEnumerator_1_t5280_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5280_MethodInfos/* methods */
	, InternalEnumerator_1_t5280_PropertyInfos/* properties */
	, InternalEnumerator_1_t5280_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5280_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5280_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5280_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5280_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5280_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5280_1_0_0/* this_arg */
	, InternalEnumerator_1_t5280_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5280_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5280)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9564_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>
extern MethodInfo ICollection_1_get_Count_m53140_MethodInfo;
static PropertyInfo ICollection_1_t9564____Count_PropertyInfo = 
{
	&ICollection_1_t9564_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53140_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53141_MethodInfo;
static PropertyInfo ICollection_1_t9564____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9564_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53141_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9564_PropertyInfos[] =
{
	&ICollection_1_t9564____Count_PropertyInfo,
	&ICollection_1_t9564____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53140_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::get_Count()
MethodInfo ICollection_1_get_Count_m53140_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9564_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53140_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53141_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53141_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9564_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53141_GenericMethod/* genericMethod */

};
extern Il2CppType WellKnownObjectMode_t2106_0_0_0;
extern Il2CppType WellKnownObjectMode_t2106_0_0_0;
static ParameterInfo ICollection_1_t9564_ICollection_1_Add_m53142_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WellKnownObjectMode_t2106_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53142_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::Add(T)
MethodInfo ICollection_1_Add_m53142_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9564_ICollection_1_Add_m53142_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53142_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53143_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::Clear()
MethodInfo ICollection_1_Clear_m53143_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53143_GenericMethod/* genericMethod */

};
extern Il2CppType WellKnownObjectMode_t2106_0_0_0;
static ParameterInfo ICollection_1_t9564_ICollection_1_Contains_m53144_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WellKnownObjectMode_t2106_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53144_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::Contains(T)
MethodInfo ICollection_1_Contains_m53144_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9564_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9564_ICollection_1_Contains_m53144_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53144_GenericMethod/* genericMethod */

};
extern Il2CppType WellKnownObjectModeU5BU5D_t5576_0_0_0;
extern Il2CppType WellKnownObjectModeU5BU5D_t5576_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9564_ICollection_1_CopyTo_m53145_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WellKnownObjectModeU5BU5D_t5576_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53145_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53145_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9564_ICollection_1_CopyTo_m53145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53145_GenericMethod/* genericMethod */

};
extern Il2CppType WellKnownObjectMode_t2106_0_0_0;
static ParameterInfo ICollection_1_t9564_ICollection_1_Remove_m53146_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WellKnownObjectMode_t2106_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53146_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>::Remove(T)
MethodInfo ICollection_1_Remove_m53146_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9564_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9564_ICollection_1_Remove_m53146_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53146_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9564_MethodInfos[] =
{
	&ICollection_1_get_Count_m53140_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53141_MethodInfo,
	&ICollection_1_Add_m53142_MethodInfo,
	&ICollection_1_Clear_m53143_MethodInfo,
	&ICollection_1_Contains_m53144_MethodInfo,
	&ICollection_1_CopyTo_m53145_MethodInfo,
	&ICollection_1_Remove_m53146_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9566_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9564_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9566_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9564_0_0_0;
extern Il2CppType ICollection_1_t9564_1_0_0;
struct ICollection_1_t9564;
extern Il2CppGenericClass ICollection_1_t9564_GenericClass;
TypeInfo ICollection_1_t9564_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9564_MethodInfos/* methods */
	, ICollection_1_t9564_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9564_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9564_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9564_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9564_0_0_0/* byval_arg */
	, &ICollection_1_t9564_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9564_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.WellKnownObjectMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.WellKnownObjectMode>
extern Il2CppType IEnumerator_1_t7436_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53147_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.WellKnownObjectMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53147_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9566_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7436_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53147_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9566_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53147_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9566_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9566_0_0_0;
extern Il2CppType IEnumerable_1_t9566_1_0_0;
struct IEnumerable_1_t9566;
extern Il2CppGenericClass IEnumerable_1_t9566_GenericClass;
TypeInfo IEnumerable_1_t9566_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9566_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9566_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9566_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9566_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9566_0_0_0/* byval_arg */
	, &IEnumerable_1_t9566_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9566_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9565_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>
extern MethodInfo IList_1_get_Item_m53148_MethodInfo;
extern MethodInfo IList_1_set_Item_m53149_MethodInfo;
static PropertyInfo IList_1_t9565____Item_PropertyInfo = 
{
	&IList_1_t9565_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53148_MethodInfo/* get */
	, &IList_1_set_Item_m53149_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9565_PropertyInfos[] =
{
	&IList_1_t9565____Item_PropertyInfo,
	NULL
};
extern Il2CppType WellKnownObjectMode_t2106_0_0_0;
static ParameterInfo IList_1_t9565_IList_1_IndexOf_m53150_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WellKnownObjectMode_t2106_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53150_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53150_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9565_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9565_IList_1_IndexOf_m53150_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53150_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WellKnownObjectMode_t2106_0_0_0;
static ParameterInfo IList_1_t9565_IList_1_Insert_m53151_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WellKnownObjectMode_t2106_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53151_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53151_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9565_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9565_IList_1_Insert_m53151_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53151_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9565_IList_1_RemoveAt_m53152_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53152_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53152_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9565_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9565_IList_1_RemoveAt_m53152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53152_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9565_IList_1_get_Item_m53148_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType WellKnownObjectMode_t2106_0_0_0;
extern void* RuntimeInvoker_WellKnownObjectMode_t2106_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53148_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53148_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9565_il2cpp_TypeInfo/* declaring_type */
	, &WellKnownObjectMode_t2106_0_0_0/* return_type */
	, RuntimeInvoker_WellKnownObjectMode_t2106_Int32_t123/* invoker_method */
	, IList_1_t9565_IList_1_get_Item_m53148_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53148_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WellKnownObjectMode_t2106_0_0_0;
static ParameterInfo IList_1_t9565_IList_1_set_Item_m53149_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WellKnownObjectMode_t2106_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53149_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53149_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9565_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9565_IList_1_set_Item_m53149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53149_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9565_MethodInfos[] =
{
	&IList_1_IndexOf_m53150_MethodInfo,
	&IList_1_Insert_m53151_MethodInfo,
	&IList_1_RemoveAt_m53152_MethodInfo,
	&IList_1_get_Item_m53148_MethodInfo,
	&IList_1_set_Item_m53149_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9565_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9564_il2cpp_TypeInfo,
	&IEnumerable_1_t9566_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9565_0_0_0;
extern Il2CppType IList_1_t9565_1_0_0;
struct IList_1_t9565;
extern Il2CppGenericClass IList_1_t9565_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9565_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9565_MethodInfos/* methods */
	, IList_1_t9565_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9565_il2cpp_TypeInfo/* element_class */
	, IList_1_t9565_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9565_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9565_0_0_0/* byval_arg */
	, &IList_1_t9565_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9565_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7438_il2cpp_TypeInfo;

// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
extern MethodInfo IEnumerator_1_get_Current_m53153_MethodInfo;
static PropertyInfo IEnumerator_1_t7438____Current_PropertyInfo = 
{
	&IEnumerator_1_t7438_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53153_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7438_PropertyInfos[] =
{
	&IEnumerator_1_t7438____Current_PropertyInfo,
	NULL
};
extern Il2CppType BinaryElement_t2108_0_0_0;
extern void* RuntimeInvoker_BinaryElement_t2108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53153_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53153_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7438_il2cpp_TypeInfo/* declaring_type */
	, &BinaryElement_t2108_0_0_0/* return_type */
	, RuntimeInvoker_BinaryElement_t2108/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53153_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7438_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53153_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7438_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7438_0_0_0;
extern Il2CppType IEnumerator_1_t7438_1_0_0;
struct IEnumerator_1_t7438;
extern Il2CppGenericClass IEnumerator_1_t7438_GenericClass;
TypeInfo IEnumerator_1_t7438_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7438_MethodInfos/* methods */
	, IEnumerator_1_t7438_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7438_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7438_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7438_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7438_0_0_0/* byval_arg */
	, &IEnumerator_1_t7438_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7438_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_735.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5281_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_735MethodDeclarations.h"

extern TypeInfo BinaryElement_t2108_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31781_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBinaryElement_t2108_m41807_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisBinaryElement_t2108_m41807 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31777_MethodInfo;
 void InternalEnumerator_1__ctor_m31777 (InternalEnumerator_1_t5281 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31778_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31778 (InternalEnumerator_1_t5281 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m31781(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31781_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&BinaryElement_t2108_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31779_MethodInfo;
 void InternalEnumerator_1_Dispose_m31779 (InternalEnumerator_1_t5281 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31780_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31780 (InternalEnumerator_1_t5281 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m31781 (InternalEnumerator_1_t5281 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisBinaryElement_t2108_m41807(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisBinaryElement_t2108_m41807_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5281____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5281_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5281, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5281____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5281_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5281, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5281_FieldInfos[] =
{
	&InternalEnumerator_1_t5281____array_0_FieldInfo,
	&InternalEnumerator_1_t5281____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5281____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5281_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31778_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5281____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5281_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31781_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5281_PropertyInfos[] =
{
	&InternalEnumerator_1_t5281____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5281____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5281_InternalEnumerator_1__ctor_m31777_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31777_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31777_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31777/* method */
	, &InternalEnumerator_1_t5281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5281_InternalEnumerator_1__ctor_m31777_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31777_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31778_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31778_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31778/* method */
	, &InternalEnumerator_1_t5281_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31778_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31779_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31779_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31779/* method */
	, &InternalEnumerator_1_t5281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31779_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31780_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31780_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31780/* method */
	, &InternalEnumerator_1_t5281_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31780_GenericMethod/* genericMethod */

};
extern Il2CppType BinaryElement_t2108_0_0_0;
extern void* RuntimeInvoker_BinaryElement_t2108 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31781_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31781_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31781/* method */
	, &InternalEnumerator_1_t5281_il2cpp_TypeInfo/* declaring_type */
	, &BinaryElement_t2108_0_0_0/* return_type */
	, RuntimeInvoker_BinaryElement_t2108/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31781_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5281_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31777_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31778_MethodInfo,
	&InternalEnumerator_1_Dispose_m31779_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31780_MethodInfo,
	&InternalEnumerator_1_get_Current_m31781_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5281_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31778_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31780_MethodInfo,
	&InternalEnumerator_1_Dispose_m31779_MethodInfo,
	&InternalEnumerator_1_get_Current_m31781_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5281_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7438_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5281_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7438_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5281_0_0_0;
extern Il2CppType InternalEnumerator_1_t5281_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5281_GenericClass;
TypeInfo InternalEnumerator_1_t5281_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5281_MethodInfos/* methods */
	, InternalEnumerator_1_t5281_PropertyInfos/* properties */
	, InternalEnumerator_1_t5281_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5281_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5281_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5281_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5281_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5281_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5281_1_0_0/* this_arg */
	, InternalEnumerator_1_t5281_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5281_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5281)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9567_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
extern MethodInfo ICollection_1_get_Count_m53154_MethodInfo;
static PropertyInfo ICollection_1_t9567____Count_PropertyInfo = 
{
	&ICollection_1_t9567_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53154_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53155_MethodInfo;
static PropertyInfo ICollection_1_t9567____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9567_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53155_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9567_PropertyInfos[] =
{
	&ICollection_1_t9567____Count_PropertyInfo,
	&ICollection_1_t9567____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53154_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::get_Count()
MethodInfo ICollection_1_get_Count_m53154_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9567_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53154_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53155_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53155_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9567_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53155_GenericMethod/* genericMethod */

};
extern Il2CppType BinaryElement_t2108_0_0_0;
extern Il2CppType BinaryElement_t2108_0_0_0;
static ParameterInfo ICollection_1_t9567_ICollection_1_Add_m53156_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BinaryElement_t2108_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53156_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Add(T)
MethodInfo ICollection_1_Add_m53156_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9567_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Byte_t838/* invoker_method */
	, ICollection_1_t9567_ICollection_1_Add_m53156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53156_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53157_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Clear()
MethodInfo ICollection_1_Clear_m53157_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9567_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53157_GenericMethod/* genericMethod */

};
extern Il2CppType BinaryElement_t2108_0_0_0;
static ParameterInfo ICollection_1_t9567_ICollection_1_Contains_m53158_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BinaryElement_t2108_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53158_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Contains(T)
MethodInfo ICollection_1_Contains_m53158_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9567_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9567_ICollection_1_Contains_m53158_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53158_GenericMethod/* genericMethod */

};
extern Il2CppType BinaryElementU5BU5D_t5577_0_0_0;
extern Il2CppType BinaryElementU5BU5D_t5577_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9567_ICollection_1_CopyTo_m53159_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BinaryElementU5BU5D_t5577_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53159_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53159_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9567_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9567_ICollection_1_CopyTo_m53159_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53159_GenericMethod/* genericMethod */

};
extern Il2CppType BinaryElement_t2108_0_0_0;
static ParameterInfo ICollection_1_t9567_ICollection_1_Remove_m53160_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BinaryElement_t2108_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53160_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Remove(T)
MethodInfo ICollection_1_Remove_m53160_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9567_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9567_ICollection_1_Remove_m53160_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53160_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9567_MethodInfos[] =
{
	&ICollection_1_get_Count_m53154_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53155_MethodInfo,
	&ICollection_1_Add_m53156_MethodInfo,
	&ICollection_1_Clear_m53157_MethodInfo,
	&ICollection_1_Contains_m53158_MethodInfo,
	&ICollection_1_CopyTo_m53159_MethodInfo,
	&ICollection_1_Remove_m53160_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9569_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9567_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9569_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9567_0_0_0;
extern Il2CppType ICollection_1_t9567_1_0_0;
struct ICollection_1_t9567;
extern Il2CppGenericClass ICollection_1_t9567_GenericClass;
TypeInfo ICollection_1_t9567_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9567_MethodInfos/* methods */
	, ICollection_1_t9567_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9567_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9567_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9567_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9567_0_0_0/* byval_arg */
	, &ICollection_1_t9567_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9567_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
extern Il2CppType IEnumerator_1_t7438_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53161_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53161_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9569_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7438_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53161_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9569_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53161_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9569_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9569_0_0_0;
extern Il2CppType IEnumerable_1_t9569_1_0_0;
struct IEnumerable_1_t9569;
extern Il2CppGenericClass IEnumerable_1_t9569_GenericClass;
TypeInfo IEnumerable_1_t9569_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9569_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9569_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9569_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9569_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9569_0_0_0/* byval_arg */
	, &IEnumerable_1_t9569_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9569_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9568_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
extern MethodInfo IList_1_get_Item_m53162_MethodInfo;
extern MethodInfo IList_1_set_Item_m53163_MethodInfo;
static PropertyInfo IList_1_t9568____Item_PropertyInfo = 
{
	&IList_1_t9568_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53162_MethodInfo/* get */
	, &IList_1_set_Item_m53163_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9568_PropertyInfos[] =
{
	&IList_1_t9568____Item_PropertyInfo,
	NULL
};
extern Il2CppType BinaryElement_t2108_0_0_0;
static ParameterInfo IList_1_t9568_IList_1_IndexOf_m53164_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BinaryElement_t2108_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53164_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53164_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9568_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9568_IList_1_IndexOf_m53164_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53164_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BinaryElement_t2108_0_0_0;
static ParameterInfo IList_1_t9568_IList_1_Insert_m53165_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BinaryElement_t2108_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53165_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53165_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9568_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9568_IList_1_Insert_m53165_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53165_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9568_IList_1_RemoveAt_m53166_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53166_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53166_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9568_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9568_IList_1_RemoveAt_m53166_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53166_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9568_IList_1_get_Item_m53162_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType BinaryElement_t2108_0_0_0;
extern void* RuntimeInvoker_BinaryElement_t2108_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53162_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53162_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9568_il2cpp_TypeInfo/* declaring_type */
	, &BinaryElement_t2108_0_0_0/* return_type */
	, RuntimeInvoker_BinaryElement_t2108_Int32_t123/* invoker_method */
	, IList_1_t9568_IList_1_get_Item_m53162_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53162_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BinaryElement_t2108_0_0_0;
static ParameterInfo IList_1_t9568_IList_1_set_Item_m53163_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BinaryElement_t2108_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53163_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53163_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9568_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9568_IList_1_set_Item_m53163_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53163_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9568_MethodInfos[] =
{
	&IList_1_IndexOf_m53164_MethodInfo,
	&IList_1_Insert_m53165_MethodInfo,
	&IList_1_RemoveAt_m53166_MethodInfo,
	&IList_1_get_Item_m53162_MethodInfo,
	&IList_1_set_Item_m53163_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9568_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9567_il2cpp_TypeInfo,
	&IEnumerable_1_t9569_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9568_0_0_0;
extern Il2CppType IList_1_t9568_1_0_0;
struct IList_1_t9568;
extern Il2CppGenericClass IList_1_t9568_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9568_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9568_MethodInfos/* methods */
	, IList_1_t9568_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9568_il2cpp_TypeInfo/* element_class */
	, IList_1_t9568_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9568_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9568_0_0_0/* byval_arg */
	, &IList_1_t9568_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9568_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7440_il2cpp_TypeInfo;

// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
extern MethodInfo IEnumerator_1_get_Current_m53167_MethodInfo;
static PropertyInfo IEnumerator_1_t7440____Current_PropertyInfo = 
{
	&IEnumerator_1_t7440_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53167_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7440_PropertyInfos[] =
{
	&IEnumerator_1_t7440____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeTag_t2109_0_0_0;
extern void* RuntimeInvoker_TypeTag_t2109 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53167_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53167_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7440_il2cpp_TypeInfo/* declaring_type */
	, &TypeTag_t2109_0_0_0/* return_type */
	, RuntimeInvoker_TypeTag_t2109/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53167_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7440_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53167_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7440_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7440_0_0_0;
extern Il2CppType IEnumerator_1_t7440_1_0_0;
struct IEnumerator_1_t7440;
extern Il2CppGenericClass IEnumerator_1_t7440_GenericClass;
TypeInfo IEnumerator_1_t7440_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7440_MethodInfos/* methods */
	, IEnumerator_1_t7440_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7440_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7440_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7440_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7440_0_0_0/* byval_arg */
	, &IEnumerator_1_t7440_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7440_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_736.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5282_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_736MethodDeclarations.h"

extern TypeInfo TypeTag_t2109_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31786_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeTag_t2109_m41818_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisTypeTag_t2109_m41818 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31782_MethodInfo;
 void InternalEnumerator_1__ctor_m31782 (InternalEnumerator_1_t5282 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31783_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31783 (InternalEnumerator_1_t5282 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m31786(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31786_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TypeTag_t2109_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31784_MethodInfo;
 void InternalEnumerator_1_Dispose_m31784 (InternalEnumerator_1_t5282 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31785_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31785 (InternalEnumerator_1_t5282 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m31786 (InternalEnumerator_1_t5282 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisTypeTag_t2109_m41818(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTypeTag_t2109_m41818_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5282____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5282_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5282, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5282____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5282_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5282, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5282_FieldInfos[] =
{
	&InternalEnumerator_1_t5282____array_0_FieldInfo,
	&InternalEnumerator_1_t5282____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5282____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5282_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31783_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5282____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5282_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31786_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5282_PropertyInfos[] =
{
	&InternalEnumerator_1_t5282____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5282____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5282_InternalEnumerator_1__ctor_m31782_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31782_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31782_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31782/* method */
	, &InternalEnumerator_1_t5282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5282_InternalEnumerator_1__ctor_m31782_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31782_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31783_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31783_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31783/* method */
	, &InternalEnumerator_1_t5282_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31783_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31784_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31784_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31784/* method */
	, &InternalEnumerator_1_t5282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31784_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31785_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31785_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31785/* method */
	, &InternalEnumerator_1_t5282_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31785_GenericMethod/* genericMethod */

};
extern Il2CppType TypeTag_t2109_0_0_0;
extern void* RuntimeInvoker_TypeTag_t2109 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31786_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31786_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31786/* method */
	, &InternalEnumerator_1_t5282_il2cpp_TypeInfo/* declaring_type */
	, &TypeTag_t2109_0_0_0/* return_type */
	, RuntimeInvoker_TypeTag_t2109/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31786_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5282_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31782_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31783_MethodInfo,
	&InternalEnumerator_1_Dispose_m31784_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31785_MethodInfo,
	&InternalEnumerator_1_get_Current_m31786_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5282_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31783_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31785_MethodInfo,
	&InternalEnumerator_1_Dispose_m31784_MethodInfo,
	&InternalEnumerator_1_get_Current_m31786_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5282_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7440_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5282_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7440_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5282_0_0_0;
extern Il2CppType InternalEnumerator_1_t5282_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5282_GenericClass;
TypeInfo InternalEnumerator_1_t5282_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5282_MethodInfos/* methods */
	, InternalEnumerator_1_t5282_PropertyInfos/* properties */
	, InternalEnumerator_1_t5282_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5282_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5282_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5282_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5282_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5282_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5282_1_0_0/* this_arg */
	, InternalEnumerator_1_t5282_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5282_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5282)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9570_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
extern MethodInfo ICollection_1_get_Count_m53168_MethodInfo;
static PropertyInfo ICollection_1_t9570____Count_PropertyInfo = 
{
	&ICollection_1_t9570_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53168_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53169_MethodInfo;
static PropertyInfo ICollection_1_t9570____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9570_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53169_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9570_PropertyInfos[] =
{
	&ICollection_1_t9570____Count_PropertyInfo,
	&ICollection_1_t9570____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53168_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Count()
MethodInfo ICollection_1_get_Count_m53168_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9570_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53168_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53169_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53169_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9570_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53169_GenericMethod/* genericMethod */

};
extern Il2CppType TypeTag_t2109_0_0_0;
extern Il2CppType TypeTag_t2109_0_0_0;
static ParameterInfo ICollection_1_t9570_ICollection_1_Add_m53170_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeTag_t2109_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53170_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Add(T)
MethodInfo ICollection_1_Add_m53170_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9570_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Byte_t838/* invoker_method */
	, ICollection_1_t9570_ICollection_1_Add_m53170_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53170_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53171_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Clear()
MethodInfo ICollection_1_Clear_m53171_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9570_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53171_GenericMethod/* genericMethod */

};
extern Il2CppType TypeTag_t2109_0_0_0;
static ParameterInfo ICollection_1_t9570_ICollection_1_Contains_m53172_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeTag_t2109_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53172_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Contains(T)
MethodInfo ICollection_1_Contains_m53172_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9570_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9570_ICollection_1_Contains_m53172_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53172_GenericMethod/* genericMethod */

};
extern Il2CppType TypeTagU5BU5D_t2697_0_0_0;
extern Il2CppType TypeTagU5BU5D_t2697_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9570_ICollection_1_CopyTo_m53173_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeTagU5BU5D_t2697_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53173_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53173_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9570_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9570_ICollection_1_CopyTo_m53173_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53173_GenericMethod/* genericMethod */

};
extern Il2CppType TypeTag_t2109_0_0_0;
static ParameterInfo ICollection_1_t9570_ICollection_1_Remove_m53174_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeTag_t2109_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53174_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Remove(T)
MethodInfo ICollection_1_Remove_m53174_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9570_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9570_ICollection_1_Remove_m53174_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53174_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9570_MethodInfos[] =
{
	&ICollection_1_get_Count_m53168_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53169_MethodInfo,
	&ICollection_1_Add_m53170_MethodInfo,
	&ICollection_1_Clear_m53171_MethodInfo,
	&ICollection_1_Contains_m53172_MethodInfo,
	&ICollection_1_CopyTo_m53173_MethodInfo,
	&ICollection_1_Remove_m53174_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9572_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9570_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9572_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9570_0_0_0;
extern Il2CppType ICollection_1_t9570_1_0_0;
struct ICollection_1_t9570;
extern Il2CppGenericClass ICollection_1_t9570_GenericClass;
TypeInfo ICollection_1_t9570_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9570_MethodInfos/* methods */
	, ICollection_1_t9570_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9570_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9570_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9570_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9570_0_0_0/* byval_arg */
	, &ICollection_1_t9570_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9570_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
extern Il2CppType IEnumerator_1_t7440_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53175_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53175_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9572_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53175_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9572_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53175_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9572_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9572_0_0_0;
extern Il2CppType IEnumerable_1_t9572_1_0_0;
struct IEnumerable_1_t9572;
extern Il2CppGenericClass IEnumerable_1_t9572_GenericClass;
TypeInfo IEnumerable_1_t9572_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9572_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9572_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9572_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9572_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9572_0_0_0/* byval_arg */
	, &IEnumerable_1_t9572_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9572_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9571_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
extern MethodInfo IList_1_get_Item_m53176_MethodInfo;
extern MethodInfo IList_1_set_Item_m53177_MethodInfo;
static PropertyInfo IList_1_t9571____Item_PropertyInfo = 
{
	&IList_1_t9571_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53176_MethodInfo/* get */
	, &IList_1_set_Item_m53177_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9571_PropertyInfos[] =
{
	&IList_1_t9571____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeTag_t2109_0_0_0;
static ParameterInfo IList_1_t9571_IList_1_IndexOf_m53178_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeTag_t2109_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53178_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53178_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9571_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9571_IList_1_IndexOf_m53178_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53178_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeTag_t2109_0_0_0;
static ParameterInfo IList_1_t9571_IList_1_Insert_m53179_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeTag_t2109_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53179_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53179_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9571_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9571_IList_1_Insert_m53179_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53179_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9571_IList_1_RemoveAt_m53180_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53180_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53180_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9571_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9571_IList_1_RemoveAt_m53180_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53180_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9571_IList_1_get_Item_m53176_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TypeTag_t2109_0_0_0;
extern void* RuntimeInvoker_TypeTag_t2109_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53176_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53176_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9571_il2cpp_TypeInfo/* declaring_type */
	, &TypeTag_t2109_0_0_0/* return_type */
	, RuntimeInvoker_TypeTag_t2109_Int32_t123/* invoker_method */
	, IList_1_t9571_IList_1_get_Item_m53176_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53176_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeTag_t2109_0_0_0;
static ParameterInfo IList_1_t9571_IList_1_set_Item_m53177_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeTag_t2109_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53177_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53177_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9571_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9571_IList_1_set_Item_m53177_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53177_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9571_MethodInfos[] =
{
	&IList_1_IndexOf_m53178_MethodInfo,
	&IList_1_Insert_m53179_MethodInfo,
	&IList_1_RemoveAt_m53180_MethodInfo,
	&IList_1_get_Item_m53176_MethodInfo,
	&IList_1_set_Item_m53177_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9571_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9570_il2cpp_TypeInfo,
	&IEnumerable_1_t9572_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9571_0_0_0;
extern Il2CppType IList_1_t9571_1_0_0;
struct IList_1_t9571;
extern Il2CppGenericClass IList_1_t9571_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9571_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9571_MethodInfos/* methods */
	, IList_1_t9571_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9571_il2cpp_TypeInfo/* element_class */
	, IList_1_t9571_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9571_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9571_0_0_0/* byval_arg */
	, &IList_1_t9571_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9571_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7442_il2cpp_TypeInfo;

// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Meth.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
extern MethodInfo IEnumerator_1_get_Current_m53181_MethodInfo;
static PropertyInfo IEnumerator_1_t7442____Current_PropertyInfo = 
{
	&IEnumerator_1_t7442_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53181_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7442_PropertyInfos[] =
{
	&IEnumerator_1_t7442____Current_PropertyInfo,
	NULL
};
extern Il2CppType MethodFlags_t2110_0_0_0;
extern void* RuntimeInvoker_MethodFlags_t2110 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53181_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53181_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7442_il2cpp_TypeInfo/* declaring_type */
	, &MethodFlags_t2110_0_0_0/* return_type */
	, RuntimeInvoker_MethodFlags_t2110/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53181_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7442_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53181_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7442_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7442_0_0_0;
extern Il2CppType IEnumerator_1_t7442_1_0_0;
struct IEnumerator_1_t7442;
extern Il2CppGenericClass IEnumerator_1_t7442_GenericClass;
TypeInfo IEnumerator_1_t7442_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7442_MethodInfos/* methods */
	, IEnumerator_1_t7442_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7442_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7442_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7442_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7442_0_0_0/* byval_arg */
	, &IEnumerator_1_t7442_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7442_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_737.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5283_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_737MethodDeclarations.h"

extern TypeInfo MethodFlags_t2110_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31791_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMethodFlags_t2110_m41829_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisMethodFlags_t2110_m41829 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31787_MethodInfo;
 void InternalEnumerator_1__ctor_m31787 (InternalEnumerator_1_t5283 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31788_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31788 (InternalEnumerator_1_t5283 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31791(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31791_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&MethodFlags_t2110_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31789_MethodInfo;
 void InternalEnumerator_1_Dispose_m31789 (InternalEnumerator_1_t5283 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31790_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31790 (InternalEnumerator_1_t5283 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31791 (InternalEnumerator_1_t5283 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisMethodFlags_t2110_m41829(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisMethodFlags_t2110_m41829_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5283____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5283_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5283, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5283____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5283_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5283, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5283_FieldInfos[] =
{
	&InternalEnumerator_1_t5283____array_0_FieldInfo,
	&InternalEnumerator_1_t5283____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5283____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5283_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31788_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5283____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5283_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31791_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5283_PropertyInfos[] =
{
	&InternalEnumerator_1_t5283____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5283____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5283_InternalEnumerator_1__ctor_m31787_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31787_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31787_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31787/* method */
	, &InternalEnumerator_1_t5283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5283_InternalEnumerator_1__ctor_m31787_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31787_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31788_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31788_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31788/* method */
	, &InternalEnumerator_1_t5283_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31788_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31789_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31789_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31789/* method */
	, &InternalEnumerator_1_t5283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31789_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31790_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31790_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31790/* method */
	, &InternalEnumerator_1_t5283_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31790_GenericMethod/* genericMethod */

};
extern Il2CppType MethodFlags_t2110_0_0_0;
extern void* RuntimeInvoker_MethodFlags_t2110 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31791_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31791_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31791/* method */
	, &InternalEnumerator_1_t5283_il2cpp_TypeInfo/* declaring_type */
	, &MethodFlags_t2110_0_0_0/* return_type */
	, RuntimeInvoker_MethodFlags_t2110/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31791_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5283_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31787_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31788_MethodInfo,
	&InternalEnumerator_1_Dispose_m31789_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31790_MethodInfo,
	&InternalEnumerator_1_get_Current_m31791_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5283_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31788_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31790_MethodInfo,
	&InternalEnumerator_1_Dispose_m31789_MethodInfo,
	&InternalEnumerator_1_get_Current_m31791_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5283_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7442_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5283_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7442_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5283_0_0_0;
extern Il2CppType InternalEnumerator_1_t5283_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5283_GenericClass;
TypeInfo InternalEnumerator_1_t5283_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5283_MethodInfos/* methods */
	, InternalEnumerator_1_t5283_PropertyInfos/* properties */
	, InternalEnumerator_1_t5283_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5283_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5283_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5283_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5283_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5283_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5283_1_0_0/* this_arg */
	, InternalEnumerator_1_t5283_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5283_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5283)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9573_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
extern MethodInfo ICollection_1_get_Count_m53182_MethodInfo;
static PropertyInfo ICollection_1_t9573____Count_PropertyInfo = 
{
	&ICollection_1_t9573_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53182_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53183_MethodInfo;
static PropertyInfo ICollection_1_t9573____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9573_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53183_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9573_PropertyInfos[] =
{
	&ICollection_1_t9573____Count_PropertyInfo,
	&ICollection_1_t9573____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53182_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::get_Count()
MethodInfo ICollection_1_get_Count_m53182_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9573_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53182_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53183_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53183_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9573_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53183_GenericMethod/* genericMethod */

};
extern Il2CppType MethodFlags_t2110_0_0_0;
extern Il2CppType MethodFlags_t2110_0_0_0;
static ParameterInfo ICollection_1_t9573_ICollection_1_Add_m53184_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodFlags_t2110_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53184_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Add(T)
MethodInfo ICollection_1_Add_m53184_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9573_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9573_ICollection_1_Add_m53184_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53184_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53185_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Clear()
MethodInfo ICollection_1_Clear_m53185_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9573_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53185_GenericMethod/* genericMethod */

};
extern Il2CppType MethodFlags_t2110_0_0_0;
static ParameterInfo ICollection_1_t9573_ICollection_1_Contains_m53186_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodFlags_t2110_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53186_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Contains(T)
MethodInfo ICollection_1_Contains_m53186_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9573_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9573_ICollection_1_Contains_m53186_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53186_GenericMethod/* genericMethod */

};
extern Il2CppType MethodFlagsU5BU5D_t5578_0_0_0;
extern Il2CppType MethodFlagsU5BU5D_t5578_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9573_ICollection_1_CopyTo_m53187_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MethodFlagsU5BU5D_t5578_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53187_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53187_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9573_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9573_ICollection_1_CopyTo_m53187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53187_GenericMethod/* genericMethod */

};
extern Il2CppType MethodFlags_t2110_0_0_0;
static ParameterInfo ICollection_1_t9573_ICollection_1_Remove_m53188_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodFlags_t2110_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53188_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Remove(T)
MethodInfo ICollection_1_Remove_m53188_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9573_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9573_ICollection_1_Remove_m53188_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53188_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9573_MethodInfos[] =
{
	&ICollection_1_get_Count_m53182_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53183_MethodInfo,
	&ICollection_1_Add_m53184_MethodInfo,
	&ICollection_1_Clear_m53185_MethodInfo,
	&ICollection_1_Contains_m53186_MethodInfo,
	&ICollection_1_CopyTo_m53187_MethodInfo,
	&ICollection_1_Remove_m53188_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9575_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9573_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9575_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9573_0_0_0;
extern Il2CppType ICollection_1_t9573_1_0_0;
struct ICollection_1_t9573;
extern Il2CppGenericClass ICollection_1_t9573_GenericClass;
TypeInfo ICollection_1_t9573_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9573_MethodInfos/* methods */
	, ICollection_1_t9573_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9573_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9573_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9573_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9573_0_0_0/* byval_arg */
	, &ICollection_1_t9573_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9573_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
extern Il2CppType IEnumerator_1_t7442_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53189_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53189_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9575_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7442_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53189_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9575_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53189_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9575_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9575_0_0_0;
extern Il2CppType IEnumerable_1_t9575_1_0_0;
struct IEnumerable_1_t9575;
extern Il2CppGenericClass IEnumerable_1_t9575_GenericClass;
TypeInfo IEnumerable_1_t9575_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9575_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9575_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9575_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9575_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9575_0_0_0/* byval_arg */
	, &IEnumerable_1_t9575_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9575_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9574_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
extern MethodInfo IList_1_get_Item_m53190_MethodInfo;
extern MethodInfo IList_1_set_Item_m53191_MethodInfo;
static PropertyInfo IList_1_t9574____Item_PropertyInfo = 
{
	&IList_1_t9574_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53190_MethodInfo/* get */
	, &IList_1_set_Item_m53191_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9574_PropertyInfos[] =
{
	&IList_1_t9574____Item_PropertyInfo,
	NULL
};
extern Il2CppType MethodFlags_t2110_0_0_0;
static ParameterInfo IList_1_t9574_IList_1_IndexOf_m53192_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodFlags_t2110_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53192_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53192_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9574_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9574_IList_1_IndexOf_m53192_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53192_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodFlags_t2110_0_0_0;
static ParameterInfo IList_1_t9574_IList_1_Insert_m53193_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MethodFlags_t2110_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53193_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53193_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9574_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9574_IList_1_Insert_m53193_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53193_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9574_IList_1_RemoveAt_m53194_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53194_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53194_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9574_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9574_IList_1_RemoveAt_m53194_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53194_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9574_IList_1_get_Item_m53190_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MethodFlags_t2110_0_0_0;
extern void* RuntimeInvoker_MethodFlags_t2110_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53190_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53190_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9574_il2cpp_TypeInfo/* declaring_type */
	, &MethodFlags_t2110_0_0_0/* return_type */
	, RuntimeInvoker_MethodFlags_t2110_Int32_t123/* invoker_method */
	, IList_1_t9574_IList_1_get_Item_m53190_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53190_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MethodFlags_t2110_0_0_0;
static ParameterInfo IList_1_t9574_IList_1_set_Item_m53191_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MethodFlags_t2110_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53191_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53191_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9574_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9574_IList_1_set_Item_m53191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53191_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9574_MethodInfos[] =
{
	&IList_1_IndexOf_m53192_MethodInfo,
	&IList_1_Insert_m53193_MethodInfo,
	&IList_1_RemoveAt_m53194_MethodInfo,
	&IList_1_get_Item_m53190_MethodInfo,
	&IList_1_set_Item_m53191_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9574_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9573_il2cpp_TypeInfo,
	&IEnumerable_1_t9575_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9574_0_0_0;
extern Il2CppType IList_1_t9574_1_0_0;
struct IList_1_t9574;
extern Il2CppGenericClass IList_1_t9574_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9574_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9574_MethodInfos/* methods */
	, IList_1_t9574_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9574_il2cpp_TypeInfo/* element_class */
	, IList_1_t9574_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9574_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9574_0_0_0/* byval_arg */
	, &IList_1_t9574_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9574_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7444_il2cpp_TypeInfo;

// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Retu.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
extern MethodInfo IEnumerator_1_get_Current_m53195_MethodInfo;
static PropertyInfo IEnumerator_1_t7444____Current_PropertyInfo = 
{
	&IEnumerator_1_t7444_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53195_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7444_PropertyInfos[] =
{
	&IEnumerator_1_t7444____Current_PropertyInfo,
	NULL
};
extern Il2CppType ReturnTypeTag_t2111_0_0_0;
extern void* RuntimeInvoker_ReturnTypeTag_t2111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53195_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53195_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7444_il2cpp_TypeInfo/* declaring_type */
	, &ReturnTypeTag_t2111_0_0_0/* return_type */
	, RuntimeInvoker_ReturnTypeTag_t2111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53195_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7444_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53195_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7444_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7444_0_0_0;
extern Il2CppType IEnumerator_1_t7444_1_0_0;
struct IEnumerator_1_t7444;
extern Il2CppGenericClass IEnumerator_1_t7444_GenericClass;
TypeInfo IEnumerator_1_t7444_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7444_MethodInfos/* methods */
	, IEnumerator_1_t7444_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7444_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7444_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7444_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7444_0_0_0/* byval_arg */
	, &IEnumerator_1_t7444_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7444_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_738.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5284_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_738MethodDeclarations.h"

extern TypeInfo ReturnTypeTag_t2111_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31796_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReturnTypeTag_t2111_m41840_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisReturnTypeTag_t2111_m41840 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31792_MethodInfo;
 void InternalEnumerator_1__ctor_m31792 (InternalEnumerator_1_t5284 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31793_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31793 (InternalEnumerator_1_t5284 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m31796(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31796_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ReturnTypeTag_t2111_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31794_MethodInfo;
 void InternalEnumerator_1_Dispose_m31794 (InternalEnumerator_1_t5284 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31795_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31795 (InternalEnumerator_1_t5284 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m31796 (InternalEnumerator_1_t5284 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisReturnTypeTag_t2111_m41840(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisReturnTypeTag_t2111_m41840_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5284____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5284_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5284, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5284____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5284_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5284, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5284_FieldInfos[] =
{
	&InternalEnumerator_1_t5284____array_0_FieldInfo,
	&InternalEnumerator_1_t5284____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5284____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5284_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31793_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5284____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5284_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31796_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5284_PropertyInfos[] =
{
	&InternalEnumerator_1_t5284____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5284____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5284_InternalEnumerator_1__ctor_m31792_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31792_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31792_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31792/* method */
	, &InternalEnumerator_1_t5284_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5284_InternalEnumerator_1__ctor_m31792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31792_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31793_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31793_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31793/* method */
	, &InternalEnumerator_1_t5284_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31793_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31794_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31794_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31794/* method */
	, &InternalEnumerator_1_t5284_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31794_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31795_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31795_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31795/* method */
	, &InternalEnumerator_1_t5284_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31795_GenericMethod/* genericMethod */

};
extern Il2CppType ReturnTypeTag_t2111_0_0_0;
extern void* RuntimeInvoker_ReturnTypeTag_t2111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31796_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31796_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31796/* method */
	, &InternalEnumerator_1_t5284_il2cpp_TypeInfo/* declaring_type */
	, &ReturnTypeTag_t2111_0_0_0/* return_type */
	, RuntimeInvoker_ReturnTypeTag_t2111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31796_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5284_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31792_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31793_MethodInfo,
	&InternalEnumerator_1_Dispose_m31794_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31795_MethodInfo,
	&InternalEnumerator_1_get_Current_m31796_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5284_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31793_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31795_MethodInfo,
	&InternalEnumerator_1_Dispose_m31794_MethodInfo,
	&InternalEnumerator_1_get_Current_m31796_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5284_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7444_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5284_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7444_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5284_0_0_0;
extern Il2CppType InternalEnumerator_1_t5284_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5284_GenericClass;
TypeInfo InternalEnumerator_1_t5284_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5284_MethodInfos/* methods */
	, InternalEnumerator_1_t5284_PropertyInfos/* properties */
	, InternalEnumerator_1_t5284_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5284_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5284_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5284_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5284_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5284_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5284_1_0_0/* this_arg */
	, InternalEnumerator_1_t5284_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5284_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5284)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9576_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
extern MethodInfo ICollection_1_get_Count_m53196_MethodInfo;
static PropertyInfo ICollection_1_t9576____Count_PropertyInfo = 
{
	&ICollection_1_t9576_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53196_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53197_MethodInfo;
static PropertyInfo ICollection_1_t9576____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9576_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53197_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9576_PropertyInfos[] =
{
	&ICollection_1_t9576____Count_PropertyInfo,
	&ICollection_1_t9576____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53196_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::get_Count()
MethodInfo ICollection_1_get_Count_m53196_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9576_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53196_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53197_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53197_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9576_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53197_GenericMethod/* genericMethod */

};
extern Il2CppType ReturnTypeTag_t2111_0_0_0;
extern Il2CppType ReturnTypeTag_t2111_0_0_0;
static ParameterInfo ICollection_1_t9576_ICollection_1_Add_m53198_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReturnTypeTag_t2111_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53198_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Add(T)
MethodInfo ICollection_1_Add_m53198_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9576_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Byte_t838/* invoker_method */
	, ICollection_1_t9576_ICollection_1_Add_m53198_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53198_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53199_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Clear()
MethodInfo ICollection_1_Clear_m53199_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9576_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53199_GenericMethod/* genericMethod */

};
extern Il2CppType ReturnTypeTag_t2111_0_0_0;
static ParameterInfo ICollection_1_t9576_ICollection_1_Contains_m53200_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReturnTypeTag_t2111_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53200_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Contains(T)
MethodInfo ICollection_1_Contains_m53200_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9576_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9576_ICollection_1_Contains_m53200_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53200_GenericMethod/* genericMethod */

};
extern Il2CppType ReturnTypeTagU5BU5D_t5579_0_0_0;
extern Il2CppType ReturnTypeTagU5BU5D_t5579_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9576_ICollection_1_CopyTo_m53201_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ReturnTypeTagU5BU5D_t5579_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53201_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53201_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9576_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9576_ICollection_1_CopyTo_m53201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53201_GenericMethod/* genericMethod */

};
extern Il2CppType ReturnTypeTag_t2111_0_0_0;
static ParameterInfo ICollection_1_t9576_ICollection_1_Remove_m53202_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReturnTypeTag_t2111_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53202_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Remove(T)
MethodInfo ICollection_1_Remove_m53202_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9576_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9576_ICollection_1_Remove_m53202_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53202_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9576_MethodInfos[] =
{
	&ICollection_1_get_Count_m53196_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53197_MethodInfo,
	&ICollection_1_Add_m53198_MethodInfo,
	&ICollection_1_Clear_m53199_MethodInfo,
	&ICollection_1_Contains_m53200_MethodInfo,
	&ICollection_1_CopyTo_m53201_MethodInfo,
	&ICollection_1_Remove_m53202_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9578_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9576_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9578_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9576_0_0_0;
extern Il2CppType ICollection_1_t9576_1_0_0;
struct ICollection_1_t9576;
extern Il2CppGenericClass ICollection_1_t9576_GenericClass;
TypeInfo ICollection_1_t9576_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9576_MethodInfos/* methods */
	, ICollection_1_t9576_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9576_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9576_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9576_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9576_0_0_0/* byval_arg */
	, &ICollection_1_t9576_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9576_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
extern Il2CppType IEnumerator_1_t7444_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53203_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53203_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9578_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7444_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53203_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9578_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53203_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9578_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9578_0_0_0;
extern Il2CppType IEnumerable_1_t9578_1_0_0;
struct IEnumerable_1_t9578;
extern Il2CppGenericClass IEnumerable_1_t9578_GenericClass;
TypeInfo IEnumerable_1_t9578_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9578_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9578_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9578_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9578_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9578_0_0_0/* byval_arg */
	, &IEnumerable_1_t9578_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9578_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9577_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
extern MethodInfo IList_1_get_Item_m53204_MethodInfo;
extern MethodInfo IList_1_set_Item_m53205_MethodInfo;
static PropertyInfo IList_1_t9577____Item_PropertyInfo = 
{
	&IList_1_t9577_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53204_MethodInfo/* get */
	, &IList_1_set_Item_m53205_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9577_PropertyInfos[] =
{
	&IList_1_t9577____Item_PropertyInfo,
	NULL
};
extern Il2CppType ReturnTypeTag_t2111_0_0_0;
static ParameterInfo IList_1_t9577_IList_1_IndexOf_m53206_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReturnTypeTag_t2111_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53206_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53206_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9577_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9577_IList_1_IndexOf_m53206_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53206_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReturnTypeTag_t2111_0_0_0;
static ParameterInfo IList_1_t9577_IList_1_Insert_m53207_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ReturnTypeTag_t2111_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53207_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53207_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9577_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9577_IList_1_Insert_m53207_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53207_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9577_IList_1_RemoveAt_m53208_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53208_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53208_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9577_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9577_IList_1_RemoveAt_m53208_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53208_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9577_IList_1_get_Item_m53204_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ReturnTypeTag_t2111_0_0_0;
extern void* RuntimeInvoker_ReturnTypeTag_t2111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53204_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53204_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9577_il2cpp_TypeInfo/* declaring_type */
	, &ReturnTypeTag_t2111_0_0_0/* return_type */
	, RuntimeInvoker_ReturnTypeTag_t2111_Int32_t123/* invoker_method */
	, IList_1_t9577_IList_1_get_Item_m53204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53204_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ReturnTypeTag_t2111_0_0_0;
static ParameterInfo IList_1_t9577_IList_1_set_Item_m53205_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ReturnTypeTag_t2111_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53205_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53205_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9577_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9577_IList_1_set_Item_m53205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53205_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9577_MethodInfos[] =
{
	&IList_1_IndexOf_m53206_MethodInfo,
	&IList_1_Insert_m53207_MethodInfo,
	&IList_1_RemoveAt_m53208_MethodInfo,
	&IList_1_get_Item_m53204_MethodInfo,
	&IList_1_set_Item_m53205_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9577_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9576_il2cpp_TypeInfo,
	&IEnumerable_1_t9578_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9577_0_0_0;
extern Il2CppType IList_1_t9577_1_0_0;
struct IList_1_t9577;
extern Il2CppGenericClass IList_1_t9577_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9577_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9577_MethodInfos/* methods */
	, IList_1_t9577_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9577_il2cpp_TypeInfo/* element_class */
	, IList_1_t9577_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9577_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9577_0_0_0/* byval_arg */
	, &IList_1_t9577_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9577_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7445_il2cpp_TypeInfo;

// System.DateTime
#include "mscorlib_System_DateTime.h"


// T System.Collections.Generic.IEnumerator`1<System.DateTime>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.DateTime>
extern MethodInfo IEnumerator_1_get_Current_m53209_MethodInfo;
static PropertyInfo IEnumerator_1_t7445____Current_PropertyInfo = 
{
	&IEnumerator_1_t7445_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53209_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7445_PropertyInfos[] =
{
	&IEnumerator_1_t7445____Current_PropertyInfo,
	NULL
};
extern Il2CppType DateTime_t674_0_0_0;
extern void* RuntimeInvoker_DateTime_t674 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53209_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.DateTime>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53209_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7445_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t674_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t674/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53209_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7445_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53209_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7445_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7445_0_0_0;
extern Il2CppType IEnumerator_1_t7445_1_0_0;
struct IEnumerator_1_t7445;
extern Il2CppGenericClass IEnumerator_1_t7445_GenericClass;
TypeInfo IEnumerator_1_t7445_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7445_MethodInfos/* methods */
	, IEnumerator_1_t7445_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7445_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7445_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7445_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7445_0_0_0/* byval_arg */
	, &IEnumerator_1_t7445_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7445_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.DateTime>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_739.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5285_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.DateTime>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_739MethodDeclarations.h"

extern TypeInfo DateTime_t674_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31801_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDateTime_t674_m41851_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
 DateTime_t674  Array_InternalArray__get_Item_TisDateTime_t674_m41851 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31797_MethodInfo;
 void InternalEnumerator_1__ctor_m31797 (InternalEnumerator_1_t5285 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31798_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31798 (InternalEnumerator_1_t5285 * __this, MethodInfo* method){
	{
		DateTime_t674  L_0 = InternalEnumerator_1_get_Current_m31801(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31801_MethodInfo);
		DateTime_t674  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&DateTime_t674_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31799_MethodInfo;
 void InternalEnumerator_1_Dispose_m31799 (InternalEnumerator_1_t5285 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31800_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31800 (InternalEnumerator_1_t5285 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
 DateTime_t674  InternalEnumerator_1_get_Current_m31801 (InternalEnumerator_1_t5285 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		DateTime_t674  L_8 = Array_InternalArray__get_Item_TisDateTime_t674_m41851(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisDateTime_t674_m41851_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.DateTime>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5285____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5285_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5285, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5285____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5285_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5285, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5285_FieldInfos[] =
{
	&InternalEnumerator_1_t5285____array_0_FieldInfo,
	&InternalEnumerator_1_t5285____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5285____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5285_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31798_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5285____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5285_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31801_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5285_PropertyInfos[] =
{
	&InternalEnumerator_1_t5285____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5285____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5285_InternalEnumerator_1__ctor_m31797_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31797_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31797_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31797/* method */
	, &InternalEnumerator_1_t5285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5285_InternalEnumerator_1__ctor_m31797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31797_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31798_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31798_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31798/* method */
	, &InternalEnumerator_1_t5285_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31798_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31799_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31799_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31799/* method */
	, &InternalEnumerator_1_t5285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31799_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31800_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31800_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31800/* method */
	, &InternalEnumerator_1_t5285_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31800_GenericMethod/* genericMethod */

};
extern Il2CppType DateTime_t674_0_0_0;
extern void* RuntimeInvoker_DateTime_t674 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31801_GenericMethod;
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31801_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31801/* method */
	, &InternalEnumerator_1_t5285_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t674_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t674/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31801_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5285_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31797_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31798_MethodInfo,
	&InternalEnumerator_1_Dispose_m31799_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31800_MethodInfo,
	&InternalEnumerator_1_get_Current_m31801_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5285_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31798_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31800_MethodInfo,
	&InternalEnumerator_1_Dispose_m31799_MethodInfo,
	&InternalEnumerator_1_get_Current_m31801_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5285_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7445_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5285_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7445_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5285_0_0_0;
extern Il2CppType InternalEnumerator_1_t5285_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5285_GenericClass;
TypeInfo InternalEnumerator_1_t5285_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5285_MethodInfos/* methods */
	, InternalEnumerator_1_t5285_PropertyInfos/* properties */
	, InternalEnumerator_1_t5285_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5285_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5285_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5285_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5285_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5285_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5285_1_0_0/* this_arg */
	, InternalEnumerator_1_t5285_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5285_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5285)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9579_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.DateTime>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.DateTime>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.DateTime>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.DateTime>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.DateTime>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.DateTime>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.DateTime>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.DateTime>
extern MethodInfo ICollection_1_get_Count_m53210_MethodInfo;
static PropertyInfo ICollection_1_t9579____Count_PropertyInfo = 
{
	&ICollection_1_t9579_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53210_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53211_MethodInfo;
static PropertyInfo ICollection_1_t9579____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9579_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53211_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9579_PropertyInfos[] =
{
	&ICollection_1_t9579____Count_PropertyInfo,
	&ICollection_1_t9579____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53210_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.DateTime>::get_Count()
MethodInfo ICollection_1_get_Count_m53210_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9579_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53210_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53211_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.DateTime>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53211_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9579_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53211_GenericMethod/* genericMethod */

};
extern Il2CppType DateTime_t674_0_0_0;
extern Il2CppType DateTime_t674_0_0_0;
static ParameterInfo ICollection_1_t9579_ICollection_1_Add_m53212_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTime_t674_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_DateTime_t674 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53212_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.DateTime>::Add(T)
MethodInfo ICollection_1_Add_m53212_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9579_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_DateTime_t674/* invoker_method */
	, ICollection_1_t9579_ICollection_1_Add_m53212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53212_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53213_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.DateTime>::Clear()
MethodInfo ICollection_1_Clear_m53213_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9579_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53213_GenericMethod/* genericMethod */

};
extern Il2CppType DateTime_t674_0_0_0;
static ParameterInfo ICollection_1_t9579_ICollection_1_Contains_m53214_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTime_t674_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_DateTime_t674 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53214_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.DateTime>::Contains(T)
MethodInfo ICollection_1_Contains_m53214_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9579_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_DateTime_t674/* invoker_method */
	, ICollection_1_t9579_ICollection_1_Contains_m53214_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53214_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeU5BU5D_t2694_0_0_0;
extern Il2CppType DateTimeU5BU5D_t2694_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9579_ICollection_1_CopyTo_m53215_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeU5BU5D_t2694_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53215_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.DateTime>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53215_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9579_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9579_ICollection_1_CopyTo_m53215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53215_GenericMethod/* genericMethod */

};
extern Il2CppType DateTime_t674_0_0_0;
static ParameterInfo ICollection_1_t9579_ICollection_1_Remove_m53216_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTime_t674_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_DateTime_t674 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53216_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.DateTime>::Remove(T)
MethodInfo ICollection_1_Remove_m53216_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9579_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_DateTime_t674/* invoker_method */
	, ICollection_1_t9579_ICollection_1_Remove_m53216_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53216_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9579_MethodInfos[] =
{
	&ICollection_1_get_Count_m53210_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53211_MethodInfo,
	&ICollection_1_Add_m53212_MethodInfo,
	&ICollection_1_Clear_m53213_MethodInfo,
	&ICollection_1_Contains_m53214_MethodInfo,
	&ICollection_1_CopyTo_m53215_MethodInfo,
	&ICollection_1_Remove_m53216_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9581_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9579_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9581_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9579_0_0_0;
extern Il2CppType ICollection_1_t9579_1_0_0;
struct ICollection_1_t9579;
extern Il2CppGenericClass ICollection_1_t9579_GenericClass;
TypeInfo ICollection_1_t9579_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9579_MethodInfos/* methods */
	, ICollection_1_t9579_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9579_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9579_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9579_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9579_0_0_0/* byval_arg */
	, &ICollection_1_t9579_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9579_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.DateTime>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.DateTime>
extern Il2CppType IEnumerator_1_t7445_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53217_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.DateTime>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53217_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9581_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7445_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53217_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9581_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53217_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9581_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9581_0_0_0;
extern Il2CppType IEnumerable_1_t9581_1_0_0;
struct IEnumerable_1_t9581;
extern Il2CppGenericClass IEnumerable_1_t9581_GenericClass;
TypeInfo IEnumerable_1_t9581_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9581_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9581_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9581_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9581_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9581_0_0_0/* byval_arg */
	, &IEnumerable_1_t9581_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9581_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9580_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.DateTime>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.DateTime>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.DateTime>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.DateTime>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.DateTime>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.DateTime>
extern MethodInfo IList_1_get_Item_m53218_MethodInfo;
extern MethodInfo IList_1_set_Item_m53219_MethodInfo;
static PropertyInfo IList_1_t9580____Item_PropertyInfo = 
{
	&IList_1_t9580_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53218_MethodInfo/* get */
	, &IList_1_set_Item_m53219_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9580_PropertyInfos[] =
{
	&IList_1_t9580____Item_PropertyInfo,
	NULL
};
extern Il2CppType DateTime_t674_0_0_0;
static ParameterInfo IList_1_t9580_IList_1_IndexOf_m53220_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTime_t674_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_DateTime_t674 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53220_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.DateTime>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53220_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9580_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_DateTime_t674/* invoker_method */
	, IList_1_t9580_IList_1_IndexOf_m53220_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53220_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DateTime_t674_0_0_0;
static ParameterInfo IList_1_t9580_IList_1_Insert_m53221_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DateTime_t674_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_DateTime_t674 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53221_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.DateTime>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53221_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9580_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_DateTime_t674/* invoker_method */
	, IList_1_t9580_IList_1_Insert_m53221_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53221_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9580_IList_1_RemoveAt_m53222_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53222_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.DateTime>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53222_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9580_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9580_IList_1_RemoveAt_m53222_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53222_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9580_IList_1_get_Item_m53218_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DateTime_t674_0_0_0;
extern void* RuntimeInvoker_DateTime_t674_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53218_GenericMethod;
// T System.Collections.Generic.IList`1<System.DateTime>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53218_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9580_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t674_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t674_Int32_t123/* invoker_method */
	, IList_1_t9580_IList_1_get_Item_m53218_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53218_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DateTime_t674_0_0_0;
static ParameterInfo IList_1_t9580_IList_1_set_Item_m53219_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DateTime_t674_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_DateTime_t674 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53219_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.DateTime>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53219_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9580_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_DateTime_t674/* invoker_method */
	, IList_1_t9580_IList_1_set_Item_m53219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53219_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9580_MethodInfos[] =
{
	&IList_1_IndexOf_m53220_MethodInfo,
	&IList_1_Insert_m53221_MethodInfo,
	&IList_1_RemoveAt_m53222_MethodInfo,
	&IList_1_get_Item_m53218_MethodInfo,
	&IList_1_set_Item_m53219_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9580_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9579_il2cpp_TypeInfo,
	&IEnumerable_1_t9581_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9580_0_0_0;
extern Il2CppType IList_1_t9580_1_0_0;
struct IList_1_t9580;
extern Il2CppGenericClass IList_1_t9580_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9580_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9580_MethodInfos/* methods */
	, IList_1_t9580_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9580_il2cpp_TypeInfo/* element_class */
	, IList_1_t9580_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9580_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9580_0_0_0/* byval_arg */
	, &IList_1_t9580_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9580_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9582_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>
extern MethodInfo ICollection_1_get_Count_m53223_MethodInfo;
static PropertyInfo ICollection_1_t9582____Count_PropertyInfo = 
{
	&ICollection_1_t9582_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53223_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53224_MethodInfo;
static PropertyInfo ICollection_1_t9582____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9582_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53224_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9582_PropertyInfos[] =
{
	&ICollection_1_t9582____Count_PropertyInfo,
	&ICollection_1_t9582____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53223_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::get_Count()
MethodInfo ICollection_1_get_Count_m53223_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9582_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53223_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53224_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53224_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9582_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53224_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2715_0_0_0;
extern Il2CppType IComparable_1_t2715_0_0_0;
static ParameterInfo ICollection_1_t9582_ICollection_1_Add_m53225_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2715_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53225_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::Add(T)
MethodInfo ICollection_1_Add_m53225_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9582_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9582_ICollection_1_Add_m53225_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53225_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53226_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::Clear()
MethodInfo ICollection_1_Clear_m53226_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9582_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53226_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2715_0_0_0;
static ParameterInfo ICollection_1_t9582_ICollection_1_Contains_m53227_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2715_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53227_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::Contains(T)
MethodInfo ICollection_1_Contains_m53227_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9582_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9582_ICollection_1_Contains_m53227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53227_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1U5BU5D_t5580_0_0_0;
extern Il2CppType IComparable_1U5BU5D_t5580_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9582_ICollection_1_CopyTo_m53228_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1U5BU5D_t5580_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53228_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53228_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9582_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9582_ICollection_1_CopyTo_m53228_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53228_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2715_0_0_0;
static ParameterInfo ICollection_1_t9582_ICollection_1_Remove_m53229_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2715_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53229_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>::Remove(T)
MethodInfo ICollection_1_Remove_m53229_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9582_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9582_ICollection_1_Remove_m53229_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53229_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9582_MethodInfos[] =
{
	&ICollection_1_get_Count_m53223_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53224_MethodInfo,
	&ICollection_1_Add_m53225_MethodInfo,
	&ICollection_1_Clear_m53226_MethodInfo,
	&ICollection_1_Contains_m53227_MethodInfo,
	&ICollection_1_CopyTo_m53228_MethodInfo,
	&ICollection_1_Remove_m53229_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9584_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9582_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9584_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9582_0_0_0;
extern Il2CppType ICollection_1_t9582_1_0_0;
struct ICollection_1_t9582;
extern Il2CppGenericClass ICollection_1_t9582_GenericClass;
TypeInfo ICollection_1_t9582_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9582_MethodInfos/* methods */
	, ICollection_1_t9582_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9582_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9582_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9582_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9582_0_0_0/* byval_arg */
	, &ICollection_1_t9582_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9582_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.DateTime>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.DateTime>>
extern Il2CppType IEnumerator_1_t7447_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53230_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.DateTime>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53230_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9584_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7447_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53230_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9584_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53230_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9584_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9584_0_0_0;
extern Il2CppType IEnumerable_1_t9584_1_0_0;
struct IEnumerable_1_t9584;
extern Il2CppGenericClass IEnumerable_1_t9584_GenericClass;
TypeInfo IEnumerable_1_t9584_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9584_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9584_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9584_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9584_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9584_0_0_0/* byval_arg */
	, &IEnumerable_1_t9584_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9584_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7447_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.DateTime>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.DateTime>>
extern MethodInfo IEnumerator_1_get_Current_m53231_MethodInfo;
static PropertyInfo IEnumerator_1_t7447____Current_PropertyInfo = 
{
	&IEnumerator_1_t7447_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53231_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7447_PropertyInfos[] =
{
	&IEnumerator_1_t7447____Current_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2715_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53231_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.DateTime>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53231_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7447_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2715_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53231_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7447_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53231_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7447_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7447_0_0_0;
extern Il2CppType IEnumerator_1_t7447_1_0_0;
struct IEnumerator_1_t7447;
extern Il2CppGenericClass IEnumerator_1_t7447_GenericClass;
TypeInfo IEnumerator_1_t7447_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7447_MethodInfos/* methods */
	, IEnumerator_1_t7447_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7447_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7447_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7447_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7447_0_0_0/* byval_arg */
	, &IEnumerator_1_t7447_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7447_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t2715_il2cpp_TypeInfo;



// System.Int32 System.IComparable`1<System.DateTime>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.DateTime>
extern Il2CppType DateTime_t674_0_0_0;
static ParameterInfo IComparable_1_t2715_IComparable_1_CompareTo_m53232_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &DateTime_t674_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_DateTime_t674 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m53232_GenericMethod;
// System.Int32 System.IComparable`1<System.DateTime>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m53232_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t2715_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_DateTime_t674/* invoker_method */
	, IComparable_1_t2715_IComparable_1_CompareTo_m53232_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m53232_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t2715_MethodInfos[] =
{
	&IComparable_1_CompareTo_m53232_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t2715_1_0_0;
struct IComparable_1_t2715;
extern Il2CppGenericClass IComparable_1_t2715_GenericClass;
TypeInfo IComparable_1_t2715_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t2715_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2715_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t2715_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t2715_0_0_0/* byval_arg */
	, &IComparable_1_t2715_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t2715_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_740.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5286_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_740MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m31806_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_1_t2715_m41862_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.DateTime>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.DateTime>>(System.Int32)
#define Array_InternalArray__get_Item_TisIComparable_1_t2715_m41862(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5286____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5286_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5286, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5286____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5286_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5286, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5286_FieldInfos[] =
{
	&InternalEnumerator_1_t5286____array_0_FieldInfo,
	&InternalEnumerator_1_t5286____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31803_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5286____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5286_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31803_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5286____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5286_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31806_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5286_PropertyInfos[] =
{
	&InternalEnumerator_1_t5286____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5286____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5286_InternalEnumerator_1__ctor_m31802_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31802_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31802_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5286_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5286_InternalEnumerator_1__ctor_m31802_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31802_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31803_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31803_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5286_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31803_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31804_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31804_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5286_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31804_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31805_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31805_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5286_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31805_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2715_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31806_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31806_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5286_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2715_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31806_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5286_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31802_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31803_MethodInfo,
	&InternalEnumerator_1_Dispose_m31804_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31805_MethodInfo,
	&InternalEnumerator_1_get_Current_m31806_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31805_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31804_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5286_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31803_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31805_MethodInfo,
	&InternalEnumerator_1_Dispose_m31804_MethodInfo,
	&InternalEnumerator_1_get_Current_m31806_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5286_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7447_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5286_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7447_il2cpp_TypeInfo, 7},
};
extern TypeInfo IComparable_1_t2715_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5286_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31806_MethodInfo/* Method Usage */,
	&IComparable_1_t2715_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIComparable_1_t2715_m41862_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5286_0_0_0;
extern Il2CppType InternalEnumerator_1_t5286_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5286_GenericClass;
TypeInfo InternalEnumerator_1_t5286_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5286_MethodInfos/* methods */
	, InternalEnumerator_1_t5286_PropertyInfos/* properties */
	, InternalEnumerator_1_t5286_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5286_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5286_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5286_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5286_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5286_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5286_1_0_0/* this_arg */
	, InternalEnumerator_1_t5286_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5286_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5286_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5286)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9583_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>
extern MethodInfo IList_1_get_Item_m53233_MethodInfo;
extern MethodInfo IList_1_set_Item_m53234_MethodInfo;
static PropertyInfo IList_1_t9583____Item_PropertyInfo = 
{
	&IList_1_t9583_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53233_MethodInfo/* get */
	, &IList_1_set_Item_m53234_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9583_PropertyInfos[] =
{
	&IList_1_t9583____Item_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2715_0_0_0;
static ParameterInfo IList_1_t9583_IList_1_IndexOf_m53235_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2715_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53235_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53235_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9583_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9583_IList_1_IndexOf_m53235_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53235_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IComparable_1_t2715_0_0_0;
static ParameterInfo IList_1_t9583_IList_1_Insert_m53236_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2715_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53236_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53236_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9583_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9583_IList_1_Insert_m53236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53236_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9583_IList_1_RemoveAt_m53237_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53237_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53237_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9583_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9583_IList_1_RemoveAt_m53237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53237_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9583_IList_1_get_Item_m53233_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IComparable_1_t2715_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53233_GenericMethod;
// T System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53233_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9583_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2715_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9583_IList_1_get_Item_m53233_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53233_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IComparable_1_t2715_0_0_0;
static ParameterInfo IList_1_t9583_IList_1_set_Item_m53234_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2715_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53234_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53234_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9583_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9583_IList_1_set_Item_m53234_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53234_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9583_MethodInfos[] =
{
	&IList_1_IndexOf_m53235_MethodInfo,
	&IList_1_Insert_m53236_MethodInfo,
	&IList_1_RemoveAt_m53237_MethodInfo,
	&IList_1_get_Item_m53233_MethodInfo,
	&IList_1_set_Item_m53234_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9583_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9582_il2cpp_TypeInfo,
	&IEnumerable_1_t9584_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9583_0_0_0;
extern Il2CppType IList_1_t9583_1_0_0;
struct IList_1_t9583;
extern Il2CppGenericClass IList_1_t9583_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9583_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9583_MethodInfos/* methods */
	, IList_1_t9583_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9583_il2cpp_TypeInfo/* element_class */
	, IList_1_t9583_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9583_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9583_0_0_0/* byval_arg */
	, &IList_1_t9583_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9583_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9585_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>
extern MethodInfo ICollection_1_get_Count_m53238_MethodInfo;
static PropertyInfo ICollection_1_t9585____Count_PropertyInfo = 
{
	&ICollection_1_t9585_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53238_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53239_MethodInfo;
static PropertyInfo ICollection_1_t9585____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9585_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53239_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9585_PropertyInfos[] =
{
	&ICollection_1_t9585____Count_PropertyInfo,
	&ICollection_1_t9585____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53238_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::get_Count()
MethodInfo ICollection_1_get_Count_m53238_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9585_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53238_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53239_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53239_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9585_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53239_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2716_0_0_0;
extern Il2CppType IEquatable_1_t2716_0_0_0;
static ParameterInfo ICollection_1_t9585_ICollection_1_Add_m53240_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2716_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53240_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::Add(T)
MethodInfo ICollection_1_Add_m53240_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9585_ICollection_1_Add_m53240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53240_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53241_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::Clear()
MethodInfo ICollection_1_Clear_m53241_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53241_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2716_0_0_0;
static ParameterInfo ICollection_1_t9585_ICollection_1_Contains_m53242_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2716_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53242_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::Contains(T)
MethodInfo ICollection_1_Contains_m53242_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9585_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9585_ICollection_1_Contains_m53242_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53242_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1U5BU5D_t5581_0_0_0;
extern Il2CppType IEquatable_1U5BU5D_t5581_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9585_ICollection_1_CopyTo_m53243_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1U5BU5D_t5581_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53243_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53243_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9585_ICollection_1_CopyTo_m53243_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53243_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2716_0_0_0;
static ParameterInfo ICollection_1_t9585_ICollection_1_Remove_m53244_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2716_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53244_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>::Remove(T)
MethodInfo ICollection_1_Remove_m53244_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9585_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9585_ICollection_1_Remove_m53244_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53244_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9585_MethodInfos[] =
{
	&ICollection_1_get_Count_m53238_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53239_MethodInfo,
	&ICollection_1_Add_m53240_MethodInfo,
	&ICollection_1_Clear_m53241_MethodInfo,
	&ICollection_1_Contains_m53242_MethodInfo,
	&ICollection_1_CopyTo_m53243_MethodInfo,
	&ICollection_1_Remove_m53244_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9587_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9585_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9587_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9585_0_0_0;
extern Il2CppType ICollection_1_t9585_1_0_0;
struct ICollection_1_t9585;
extern Il2CppGenericClass ICollection_1_t9585_GenericClass;
TypeInfo ICollection_1_t9585_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9585_MethodInfos/* methods */
	, ICollection_1_t9585_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9585_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9585_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9585_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9585_0_0_0/* byval_arg */
	, &ICollection_1_t9585_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9585_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.DateTime>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.DateTime>>
extern Il2CppType IEnumerator_1_t7449_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53245_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.DateTime>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53245_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9587_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7449_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53245_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9587_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53245_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9587_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9587_0_0_0;
extern Il2CppType IEnumerable_1_t9587_1_0_0;
struct IEnumerable_1_t9587;
extern Il2CppGenericClass IEnumerable_1_t9587_GenericClass;
TypeInfo IEnumerable_1_t9587_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9587_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9587_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9587_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9587_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9587_0_0_0/* byval_arg */
	, &IEnumerable_1_t9587_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9587_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
