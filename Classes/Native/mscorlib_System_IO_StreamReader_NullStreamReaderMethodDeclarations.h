﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.StreamReader/NullStreamReader
struct NullStreamReader_t1940;
// System.Char[]
struct CharU5BU5D_t378;
// System.String
struct String_t;

// System.Void System.IO.StreamReader/NullStreamReader::.ctor()
 void NullStreamReader__ctor_m11008 (NullStreamReader_t1940 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.StreamReader/NullStreamReader::Peek()
 int32_t NullStreamReader_Peek_m11009 (NullStreamReader_t1940 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.StreamReader/NullStreamReader::Read()
 int32_t NullStreamReader_Read_m11010 (NullStreamReader_t1940 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.StreamReader/NullStreamReader::Read(System.Char[],System.Int32,System.Int32)
 int32_t NullStreamReader_Read_m11011 (NullStreamReader_t1940 * __this, CharU5BU5D_t378* ___buffer, int32_t ___index, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.StreamReader/NullStreamReader::ReadLine()
 String_t* NullStreamReader_ReadLine_m11012 (NullStreamReader_t1940 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.StreamReader/NullStreamReader::ReadToEnd()
 String_t* NullStreamReader_ReadToEnd_m11013 (NullStreamReader_t1940 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
