﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveAbstractBehaviour>
struct UnityAction_1_t4210;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveAbstractBehaviour>
struct InvokableCall_1_t4209  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveAbstractBehaviour>::Delegate
	UnityAction_1_t4210 * ___Delegate_0;
};
