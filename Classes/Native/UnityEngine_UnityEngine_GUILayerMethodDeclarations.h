﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUILayer
struct GUILayer_t991;
// UnityEngine.GUIElement
struct GUIElement_t990;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
 GUIElement_t990 * GUILayer_HitTest_m5723 (GUILayer_t991 * __this, Vector3_t73  ___screenPosition, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
 GUIElement_t990 * GUILayer_INTERNAL_CALL_HitTest_m5724 (Object_t * __this/* static, unused */, GUILayer_t991 * ___self, Vector3_t73 * ___screenPosition, MethodInfo* method) IL2CPP_METHOD_ATTR;
