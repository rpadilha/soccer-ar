﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>
struct ShimEnumerator_t4309;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct Dictionary_2_t762;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m25369 (ShimEnumerator_t4309 * __this, Dictionary_2_t762 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::MoveNext()
 bool ShimEnumerator_MoveNext_m25370 (ShimEnumerator_t4309 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m25371 (ShimEnumerator_t4309 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Key()
 Object_t * ShimEnumerator_get_Key_m25372 (ShimEnumerator_t4309 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Value()
 Object_t * ShimEnumerator_get_Value_m25373 (ShimEnumerator_t4309 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Current()
 Object_t * ShimEnumerator_get_Current_m25374 (ShimEnumerator_t4309 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
