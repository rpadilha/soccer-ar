﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.CaseInsensitiveComparer
struct CaseInsensitiveComparer_t1544;
// System.Object
struct Object_t;

// System.Void System.Collections.CaseInsensitiveComparer::.ctor()
 void CaseInsensitiveComparer__ctor_m10518 (CaseInsensitiveComparer_t1544 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.CaseInsensitiveComparer::.ctor(System.Boolean)
 void CaseInsensitiveComparer__ctor_m10519 (CaseInsensitiveComparer_t1544 * __this, bool ___invariant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.CaseInsensitiveComparer::.cctor()
 void CaseInsensitiveComparer__cctor_m10520 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::get_DefaultInvariant()
 CaseInsensitiveComparer_t1544 * CaseInsensitiveComparer_get_DefaultInvariant_m7820 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.CaseInsensitiveComparer::Compare(System.Object,System.Object)
 int32_t CaseInsensitiveComparer_Compare_m10521 (CaseInsensitiveComparer_t1544 * __this, Object_t * ___a, Object_t * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
