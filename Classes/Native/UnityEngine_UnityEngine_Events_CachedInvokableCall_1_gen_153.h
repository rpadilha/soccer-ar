﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_155.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>
struct CachedInvokableCall_1_t4769  : public InvokableCall_1_t4770
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
