﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct Enumerator_t3460;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3452;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m18373_gshared (Enumerator_t3460 * __this, Dictionary_2_t3452 * ___dictionary, MethodInfo* method);
#define Enumerator__ctor_m18373(__this, ___dictionary, method) (void)Enumerator__ctor_m18373_gshared((Enumerator_t3460 *)__this, (Dictionary_2_t3452 *)___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18374_gshared (Enumerator_t3460 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18374(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m18374_gshared((Enumerator_t3460 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
 DictionaryEntry_t1355  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18375_gshared (Enumerator_t3460 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18375(__this, method) (DictionaryEntry_t1355 )Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18375_gshared((Enumerator_t3460 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
 Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18376_gshared (Enumerator_t3460 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18376(__this, method) (Object_t *)Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18376_gshared((Enumerator_t3460 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
 Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18377_gshared (Enumerator_t3460 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18377(__this, method) (Object_t *)Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18377_gshared((Enumerator_t3460 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
 bool Enumerator_MoveNext_m18378_gshared (Enumerator_t3460 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m18378(__this, method) (bool)Enumerator_MoveNext_m18378_gshared((Enumerator_t3460 *)__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
 KeyValuePair_2_t3458  Enumerator_get_Current_m18379 (Enumerator_t3460 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
 Object_t * Enumerator_get_CurrentKey_m18380_gshared (Enumerator_t3460 * __this, MethodInfo* method);
#define Enumerator_get_CurrentKey_m18380(__this, method) (Object_t *)Enumerator_get_CurrentKey_m18380_gshared((Enumerator_t3460 *)__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
 Object_t * Enumerator_get_CurrentValue_m18381_gshared (Enumerator_t3460 * __this, MethodInfo* method);
#define Enumerator_get_CurrentValue_m18381(__this, method) (Object_t *)Enumerator_get_CurrentValue_m18381_gshared((Enumerator_t3460 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
 void Enumerator_VerifyState_m18382_gshared (Enumerator_t3460 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m18382(__this, method) (void)Enumerator_VerifyState_m18382_gshared((Enumerator_t3460 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
 void Enumerator_VerifyCurrent_m18383_gshared (Enumerator_t3460 * __this, MethodInfo* method);
#define Enumerator_VerifyCurrent_m18383(__this, method) (void)Enumerator_VerifyCurrent_m18383_gshared((Enumerator_t3460 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
 void Enumerator_Dispose_m18384_gshared (Enumerator_t3460 * __this, MethodInfo* method);
#define Enumerator_Dispose_m18384(__this, method) (void)Enumerator_Dispose_m18384_gshared((Enumerator_t3460 *)__this, method)
