﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ObliqueNear
struct ObliqueNear_t215;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void ObliqueNear::.ctor()
 void ObliqueNear__ctor_m759 (ObliqueNear_t215 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 ObliqueNear::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
 Matrix4x4_t176  ObliqueNear_CalculateObliqueMatrix_m760 (ObliqueNear_t215 * __this, Matrix4x4_t176  ___projection, Vector4_t216  ___clipPlane, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObliqueNear::OnPreCull()
 void ObliqueNear_OnPreCull_m761 (ObliqueNear_t215 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObliqueNear::Main()
 void ObliqueNear_Main_m762 (ObliqueNear_t215 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
