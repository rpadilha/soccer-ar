﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.NullWebCamTexAdaptor
struct NullWebCamTexAdaptor_t673;
// UnityEngine.Texture
struct Texture_t107;
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"

// System.Boolean Vuforia.NullWebCamTexAdaptor::get_DidUpdateThisFrame()
 bool NullWebCamTexAdaptor_get_DidUpdateThisFrame_m3127 (NullWebCamTexAdaptor_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.NullWebCamTexAdaptor::get_IsPlaying()
 bool NullWebCamTexAdaptor_get_IsPlaying_m3128 (NullWebCamTexAdaptor_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture Vuforia.NullWebCamTexAdaptor::get_Texture()
 Texture_t107 * NullWebCamTexAdaptor_get_Texture_m3129 (NullWebCamTexAdaptor_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::.ctor(System.Int32,Vuforia.QCARRenderer/Vec2I)
 void NullWebCamTexAdaptor__ctor_m3130 (NullWebCamTexAdaptor_t673 * __this, int32_t ___requestedFPS, Vec2I_t675  ___requestedTextureSize, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::Play()
 void NullWebCamTexAdaptor_Play_m3131 (NullWebCamTexAdaptor_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::Stop()
 void NullWebCamTexAdaptor_Stop_m3132 (NullWebCamTexAdaptor_t673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
