﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t3400;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t499;
// System.Object[]
struct ObjectU5BU5D_t130;
// System.Predicate`1<System.Object>
struct Predicate_1_t2832;
// System.Comparison`1<System.Object>
struct Comparison_1_t2833;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
 void IndexedSet_1__ctor_m17827_gshared (IndexedSet_1_t3400 * __this, MethodInfo* method);
#define IndexedSet_1__ctor_m17827(__this, method) (void)IndexedSet_1__ctor_m17827_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17829_gshared (IndexedSet_1_t3400 * __this, MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17829(__this, method) (Object_t *)IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17829_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
 void IndexedSet_1_Add_m17830_gshared (IndexedSet_1_t3400 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Add_m17830(__this, ___item, method) (void)IndexedSet_1_Add_m17830_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
 bool IndexedSet_1_Remove_m17831_gshared (IndexedSet_1_t3400 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Remove_m17831(__this, ___item, method) (bool)IndexedSet_1_Remove_m17831_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
 Object_t* IndexedSet_1_GetEnumerator_m17833_gshared (IndexedSet_1_t3400 * __this, MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m17833(__this, method) (Object_t*)IndexedSet_1_GetEnumerator_m17833_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
 void IndexedSet_1_Clear_m17834_gshared (IndexedSet_1_t3400 * __this, MethodInfo* method);
#define IndexedSet_1_Clear_m17834(__this, method) (void)IndexedSet_1_Clear_m17834_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
 bool IndexedSet_1_Contains_m17836_gshared (IndexedSet_1_t3400 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Contains_m17836(__this, ___item, method) (bool)IndexedSet_1_Contains_m17836_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
 void IndexedSet_1_CopyTo_m17838_gshared (IndexedSet_1_t3400 * __this, ObjectU5BU5D_t130* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define IndexedSet_1_CopyTo_m17838(__this, ___array, ___arrayIndex, method) (void)IndexedSet_1_CopyTo_m17838_gshared((IndexedSet_1_t3400 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
 int32_t IndexedSet_1_get_Count_m17839_gshared (IndexedSet_1_t3400 * __this, MethodInfo* method);
#define IndexedSet_1_get_Count_m17839(__this, method) (int32_t)IndexedSet_1_get_Count_m17839_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
 bool IndexedSet_1_get_IsReadOnly_m17841_gshared (IndexedSet_1_t3400 * __this, MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m17841(__this, method) (bool)IndexedSet_1_get_IsReadOnly_m17841_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
 int32_t IndexedSet_1_IndexOf_m17843_gshared (IndexedSet_1_t3400 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_IndexOf_m17843(__this, ___item, method) (int32_t)IndexedSet_1_IndexOf_m17843_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
 void IndexedSet_1_Insert_m17845_gshared (IndexedSet_1_t3400 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Insert_m17845(__this, ___index, ___item, method) (void)IndexedSet_1_Insert_m17845_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
 void IndexedSet_1_RemoveAt_m17847_gshared (IndexedSet_1_t3400 * __this, int32_t ___index, MethodInfo* method);
#define IndexedSet_1_RemoveAt_m17847(__this, ___index, method) (void)IndexedSet_1_RemoveAt_m17847_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
 Object_t * IndexedSet_1_get_Item_m17848_gshared (IndexedSet_1_t3400 * __this, int32_t ___index, MethodInfo* method);
#define IndexedSet_1_get_Item_m17848(__this, ___index, method) (Object_t *)IndexedSet_1_get_Item_m17848_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
 void IndexedSet_1_set_Item_m17850_gshared (IndexedSet_1_t3400 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define IndexedSet_1_set_Item_m17850(__this, ___index, ___value, method) (void)IndexedSet_1_set_Item_m17850_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
 void IndexedSet_1_RemoveAll_m17851_gshared (IndexedSet_1_t3400 * __this, Predicate_1_t2832 * ___match, MethodInfo* method);
#define IndexedSet_1_RemoveAll_m17851(__this, ___match, method) (void)IndexedSet_1_RemoveAll_m17851_gshared((IndexedSet_1_t3400 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
 void IndexedSet_1_Sort_m17852_gshared (IndexedSet_1_t3400 * __this, Comparison_1_t2833 * ___sortLayoutFunction, MethodInfo* method);
#define IndexedSet_1_Sort_m17852(__this, ___sortLayoutFunction, method) (void)IndexedSet_1_Sort_m17852_gshared((IndexedSet_1_t3400 *)__this, (Comparison_1_t2833 *)___sortLayoutFunction, method)
