﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t29;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Jump_Button
struct Jump_Button_t207  : public MonoBehaviour_t10
{
	// UnityEngine.GameObject Jump_Button::player
	GameObject_t29 * ___player_2;
};
