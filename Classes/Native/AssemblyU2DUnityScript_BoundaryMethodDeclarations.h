﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Boundary
struct Boundary_t213;

// System.Void Boundary::.ctor()
 void Boundary__ctor_m749 (Boundary_t213 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
