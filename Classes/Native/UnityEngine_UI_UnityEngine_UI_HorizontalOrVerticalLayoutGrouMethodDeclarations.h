﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct HorizontalOrVerticalLayoutGroup_t437;

// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
 void HorizontalOrVerticalLayoutGroup__ctor_m1896 (HorizontalOrVerticalLayoutGroup_t437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
 float HorizontalOrVerticalLayoutGroup_get_spacing_m1897 (HorizontalOrVerticalLayoutGroup_t437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
 void HorizontalOrVerticalLayoutGroup_set_spacing_m1898 (HorizontalOrVerticalLayoutGroup_t437 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
 bool HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1899 (HorizontalOrVerticalLayoutGroup_t437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
 void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1900 (HorizontalOrVerticalLayoutGroup_t437 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
 bool HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1901 (HorizontalOrVerticalLayoutGroup_t437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
 void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1902 (HorizontalOrVerticalLayoutGroup_t437 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
 void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1903 (HorizontalOrVerticalLayoutGroup_t437 * __this, int32_t ___axis, bool ___isVertical, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
 void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1904 (HorizontalOrVerticalLayoutGroup_t437 * __this, int32_t ___axis, bool ___isVertical, MethodInfo* method) IL2CPP_METHOD_ATTR;
