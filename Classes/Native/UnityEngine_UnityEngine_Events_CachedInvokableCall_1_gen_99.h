﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_101.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
struct CachedInvokableCall_1_t3772  : public InvokableCall_1_t3773
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
