﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Object>
struct EqualityComparer_1_t2840;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Object>
struct EqualityComparer_1_t2840  : public Object_t
{
};
struct EqualityComparer_1_t2840_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Object>::_default
	EqualityComparer_1_t2840 * ____default_0;
};
