﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>
struct ShimEnumerator_t4344;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>
struct Dictionary_2_t770;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m25770 (ShimEnumerator_t4344 * __this, Dictionary_2_t770 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::MoveNext()
 bool ShimEnumerator_MoveNext_m25771 (ShimEnumerator_t4344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m25772 (ShimEnumerator_t4344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::get_Key()
 Object_t * ShimEnumerator_get_Key_m25773 (ShimEnumerator_t4344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::get_Value()
 Object_t * ShimEnumerator_get_Value_m25774 (ShimEnumerator_t4344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::get_Current()
 Object_t * ShimEnumerator_get_Current_m25775 (ShimEnumerator_t4344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
