﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_1.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>
struct CachedInvokableCall_1_t2767  : public InvokableCall_1_t2768
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
