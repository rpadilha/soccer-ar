﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyValuePair_2_t4967;
// UnityEngine.Event
struct Event_t382;
struct Event_t382_marshaled;
// System.String
struct String_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m29999 (KeyValuePair_2_t4967 * __this, Event_t382 * ___key, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
 Event_t382 * KeyValuePair_2_get_Key_m30000 (KeyValuePair_2_t4967 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m30001 (KeyValuePair_2_t4967 * __this, Event_t382 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
 int32_t KeyValuePair_2_get_Value_m30002 (KeyValuePair_2_t4967 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m30003 (KeyValuePair_2_t4967 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
 String_t* KeyValuePair_2_ToString_m30004 (KeyValuePair_2_t4967 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
