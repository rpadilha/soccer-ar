﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.WordResult>
struct IList_1_t4155;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordResult>
struct ReadOnlyCollection_1_t4152  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordResult>::list
	Object_t* ___list_0;
};
