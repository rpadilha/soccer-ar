﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ISmartTerrainEventHandler>
struct Comparer_1_t4251;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ISmartTerrainEventHandler>
struct Comparer_1_t4251  : public Object_t
{
};
struct Comparer_1_t4251_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ISmartTerrainEventHandler>::_default
	Comparer_1_t4251 * ____default_0;
};
