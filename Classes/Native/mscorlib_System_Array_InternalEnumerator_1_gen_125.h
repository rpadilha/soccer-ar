﻿#pragma once
#include <stdint.h>
// System.Array
struct Array_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Array/InternalEnumerator`1<InGameState_Script>
struct InternalEnumerator_1_t3046 
{
	// System.Array System.Array/InternalEnumerator`1<InGameState_Script>::array
	Array_t * ___array_0;
	// System.Int32 System.Array/InternalEnumerator`1<InGameState_Script>::idx
	int32_t ___idx_1;
};
