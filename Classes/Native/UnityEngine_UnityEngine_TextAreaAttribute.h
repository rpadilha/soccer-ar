﻿#pragma once
#include <stdint.h>
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t550  : public PropertyAttribute_t1114
{
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;
};
