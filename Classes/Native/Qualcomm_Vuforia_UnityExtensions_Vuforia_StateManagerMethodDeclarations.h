﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.StateManager
struct StateManager_t768;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
struct IEnumerable_1_t769;
// Vuforia.Trackable
struct Trackable_t594;
// Vuforia.WordManager
struct WordManager_t733;

// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::GetActiveTrackableBehaviours()
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::GetTrackableBehaviours()
// System.Void Vuforia.StateManager::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean)
// Vuforia.WordManager Vuforia.StateManager::GetWordManager()
// System.Void Vuforia.StateManager::.ctor()
 void StateManager__ctor_m4149 (StateManager_t768 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
