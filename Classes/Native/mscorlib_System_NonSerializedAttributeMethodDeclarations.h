﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.NonSerializedAttribute
struct NonSerializedAttribute_t2290;

// System.Void System.NonSerializedAttribute::.ctor()
 void NonSerializedAttribute__ctor_m13228 (NonSerializedAttribute_t2290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
