﻿#pragma once
#include <stdint.h>
// System.OperatingSystem
struct OperatingSystem_t2268;
// System.Object
#include "mscorlib_System_Object.h"
// System.Environment
struct Environment_t2269  : public Object_t
{
};
struct Environment_t2269_StaticFields{
	// System.OperatingSystem System.Environment::os
	OperatingSystem_t2268 * ___os_0;
};
