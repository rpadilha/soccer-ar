﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData
#pragma pack(push, tp, 1)
struct SmartTerrainRevisionData_t692 
{
	// System.Int32 Vuforia.QCARManagerImpl/SmartTerrainRevisionData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.QCARManagerImpl/SmartTerrainRevisionData::revision
	int32_t ___revision_1;
};
#pragma pack(pop, tp)
