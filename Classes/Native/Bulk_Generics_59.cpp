﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IComparer_1_t2722_il2cpp_TypeInfo;

// System.Int32
#include "mscorlib_System_Int32.h"
// System.Int16
#include "mscorlib_System_Int16.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Int32 System.Collections.Generic.IComparer`1<System.Int16>::Compare(T,T)
// Metadata Definition System.Collections.Generic.IComparer`1<System.Int16>
extern Il2CppType Int16_t524_0_0_0;
extern Il2CppType Int16_t524_0_0_0;
extern Il2CppType Int16_t524_0_0_0;
static ParameterInfo IComparer_1_t2722_IComparer_1_Compare_m53723_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Int16_t524_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Int16_t524_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int16_t524_Int16_t524 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparer_1_Compare_m53723_GenericMethod;
// System.Int32 System.Collections.Generic.IComparer`1<System.Int16>::Compare(T,T)
MethodInfo IComparer_1_Compare_m53723_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &IComparer_1_t2722_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int16_t524_Int16_t524/* invoker_method */
	, IComparer_1_t2722_IComparer_1_Compare_m53723_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparer_1_Compare_m53723_GenericMethod/* genericMethod */

};
static MethodInfo* IComparer_1_t2722_MethodInfos[] =
{
	&IComparer_1_Compare_m53723_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparer_1_t2722_0_0_0;
extern Il2CppType IComparer_1_t2722_1_0_0;
struct IComparer_1_t2722;
extern Il2CppGenericClass IComparer_1_t2722_GenericClass;
TypeInfo IComparer_1_t2722_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IComparer_1_t2722_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparer_1_t2722_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparer_1_t2722_il2cpp_TypeInfo/* cast_class */
	, &IComparer_1_t2722_0_0_0/* byval_arg */
	, &IComparer_1_t2722_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparer_1_t2722_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparer_1_t2724_il2cpp_TypeInfo;

// System.Int64
#include "mscorlib_System_Int64.h"


// System.Int32 System.Collections.Generic.IComparer`1<System.Int64>::Compare(T,T)
// Metadata Definition System.Collections.Generic.IComparer`1<System.Int64>
extern Il2CppType Int64_t1163_0_0_0;
extern Il2CppType Int64_t1163_0_0_0;
extern Il2CppType Int64_t1163_0_0_0;
static ParameterInfo IComparer_1_t2724_IComparer_1_Compare_m53724_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Int64_t1163_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Int64_t1163_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int64_t1163_Int64_t1163 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparer_1_Compare_m53724_GenericMethod;
// System.Int32 System.Collections.Generic.IComparer`1<System.Int64>::Compare(T,T)
MethodInfo IComparer_1_Compare_m53724_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &IComparer_1_t2724_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int64_t1163_Int64_t1163/* invoker_method */
	, IComparer_1_t2724_IComparer_1_Compare_m53724_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparer_1_Compare_m53724_GenericMethod/* genericMethod */

};
static MethodInfo* IComparer_1_t2724_MethodInfos[] =
{
	&IComparer_1_Compare_m53724_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparer_1_t2724_0_0_0;
extern Il2CppType IComparer_1_t2724_1_0_0;
struct IComparer_1_t2724;
extern Il2CppGenericClass IComparer_1_t2724_GenericClass;
TypeInfo IComparer_1_t2724_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IComparer_1_t2724_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparer_1_t2724_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparer_1_t2724_il2cpp_TypeInfo/* cast_class */
	, &IComparer_1_t2724_0_0_0/* byval_arg */
	, &IComparer_1_t2724_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparer_1_t2724_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7516_il2cpp_TypeInfo;

// System.Environment/SpecialFolder
#include "mscorlib_System_Environment_SpecialFolder.h"


// T System.Collections.Generic.IEnumerator`1<System.Environment/SpecialFolder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Environment/SpecialFolder>
extern MethodInfo IEnumerator_1_get_Current_m53725_MethodInfo;
static PropertyInfo IEnumerator_1_t7516____Current_PropertyInfo = 
{
	&IEnumerator_1_t7516_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53725_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7516_PropertyInfos[] =
{
	&IEnumerator_1_t7516____Current_PropertyInfo,
	NULL
};
extern Il2CppType SpecialFolder_t2267_0_0_0;
extern void* RuntimeInvoker_SpecialFolder_t2267 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53725_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Environment/SpecialFolder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53725_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7516_il2cpp_TypeInfo/* declaring_type */
	, &SpecialFolder_t2267_0_0_0/* return_type */
	, RuntimeInvoker_SpecialFolder_t2267/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53725_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7516_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53725_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t7516_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7516_0_0_0;
extern Il2CppType IEnumerator_1_t7516_1_0_0;
struct IEnumerator_1_t7516;
extern Il2CppGenericClass IEnumerator_1_t7516_GenericClass;
TypeInfo IEnumerator_1_t7516_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7516_MethodInfos/* methods */
	, IEnumerator_1_t7516_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7516_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7516_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7516_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7516_0_0_0/* byval_arg */
	, &IEnumerator_1_t7516_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7516_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_774.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5341_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_774MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo SpecialFolder_t2267_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m32157_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Environment/SpecialFolder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Environment/SpecialFolder>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m32153_MethodInfo;
 void InternalEnumerator_1__ctor_m32153 (InternalEnumerator_1_t5341 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32154_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32154 (InternalEnumerator_1_t5341 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m32157(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m32157_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m32155_MethodInfo;
 void InternalEnumerator_1_Dispose_m32155 (InternalEnumerator_1_t5341 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m32156_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m32156 (InternalEnumerator_1_t5341 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m32157 (InternalEnumerator_1_t5341 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5341____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5341_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5341, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5341____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5341_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5341, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5341_FieldInfos[] =
{
	&InternalEnumerator_1_t5341____array_0_FieldInfo,
	&InternalEnumerator_1_t5341____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5341____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5341_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32154_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5341____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5341_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m32157_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5341_PropertyInfos[] =
{
	&InternalEnumerator_1_t5341____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5341____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5341_InternalEnumerator_1__ctor_m32153_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m32153_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m32153_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m32153/* method */
	, &InternalEnumerator_1_t5341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5341_InternalEnumerator_1__ctor_m32153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m32153_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32154_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32154_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32154/* method */
	, &InternalEnumerator_1_t5341_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32154_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m32155_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m32155_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m32155/* method */
	, &InternalEnumerator_1_t5341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m32155_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m32156_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m32156_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m32156/* method */
	, &InternalEnumerator_1_t5341_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m32156_GenericMethod/* genericMethod */

};
extern Il2CppType SpecialFolder_t2267_0_0_0;
extern void* RuntimeInvoker_SpecialFolder_t2267 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m32157_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m32157_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m32157/* method */
	, &InternalEnumerator_1_t5341_il2cpp_TypeInfo/* declaring_type */
	, &SpecialFolder_t2267_0_0_0/* return_type */
	, RuntimeInvoker_SpecialFolder_t2267/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m32157_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5341_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m32153_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32154_MethodInfo,
	&InternalEnumerator_1_Dispose_m32155_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32156_MethodInfo,
	&InternalEnumerator_1_get_Current_m32157_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5341_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32154_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32156_MethodInfo,
	&InternalEnumerator_1_Dispose_m32155_MethodInfo,
	&InternalEnumerator_1_get_Current_m32157_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5341_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7516_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5341_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7516_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5341_0_0_0;
extern Il2CppType InternalEnumerator_1_t5341_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t5341_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t5341_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5341_MethodInfos/* methods */
	, InternalEnumerator_1_t5341_PropertyInfos/* properties */
	, InternalEnumerator_1_t5341_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5341_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5341_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5341_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5341_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5341_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5341_1_0_0/* this_arg */
	, InternalEnumerator_1_t5341_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5341_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5341)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9681_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>
extern MethodInfo ICollection_1_get_Count_m53726_MethodInfo;
static PropertyInfo ICollection_1_t9681____Count_PropertyInfo = 
{
	&ICollection_1_t9681_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53726_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53727_MethodInfo;
static PropertyInfo ICollection_1_t9681____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9681_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53727_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9681_PropertyInfos[] =
{
	&ICollection_1_t9681____Count_PropertyInfo,
	&ICollection_1_t9681____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53726_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::get_Count()
MethodInfo ICollection_1_get_Count_m53726_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9681_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53726_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53727_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53727_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9681_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53727_GenericMethod/* genericMethod */

};
extern Il2CppType SpecialFolder_t2267_0_0_0;
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo ICollection_1_t9681_ICollection_1_Add_m53728_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53728_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::Add(T)
MethodInfo ICollection_1_Add_m53728_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9681_ICollection_1_Add_m53728_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53728_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53729_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::Clear()
MethodInfo ICollection_1_Clear_m53729_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53729_GenericMethod/* genericMethod */

};
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo ICollection_1_t9681_ICollection_1_Contains_m53730_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53730_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::Contains(T)
MethodInfo ICollection_1_Contains_m53730_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9681_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9681_ICollection_1_Contains_m53730_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53730_GenericMethod/* genericMethod */

};
extern Il2CppType SpecialFolderU5BU5D_t5611_0_0_0;
extern Il2CppType SpecialFolderU5BU5D_t5611_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9681_ICollection_1_CopyTo_m53731_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SpecialFolderU5BU5D_t5611_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53731_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53731_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9681_ICollection_1_CopyTo_m53731_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53731_GenericMethod/* genericMethod */

};
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo ICollection_1_t9681_ICollection_1_Remove_m53732_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53732_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Environment/SpecialFolder>::Remove(T)
MethodInfo ICollection_1_Remove_m53732_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9681_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9681_ICollection_1_Remove_m53732_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53732_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9681_MethodInfos[] =
{
	&ICollection_1_get_Count_m53726_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53727_MethodInfo,
	&ICollection_1_Add_m53728_MethodInfo,
	&ICollection_1_Clear_m53729_MethodInfo,
	&ICollection_1_Contains_m53730_MethodInfo,
	&ICollection_1_CopyTo_m53731_MethodInfo,
	&ICollection_1_Remove_m53732_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t9683_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9681_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9683_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9681_0_0_0;
extern Il2CppType ICollection_1_t9681_1_0_0;
struct ICollection_1_t9681;
extern Il2CppGenericClass ICollection_1_t9681_GenericClass;
TypeInfo ICollection_1_t9681_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9681_MethodInfos/* methods */
	, ICollection_1_t9681_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9681_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9681_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9681_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9681_0_0_0/* byval_arg */
	, &ICollection_1_t9681_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9681_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Environment/SpecialFolder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Environment/SpecialFolder>
extern Il2CppType IEnumerator_1_t7516_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53733_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Environment/SpecialFolder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53733_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9683_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7516_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53733_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9683_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53733_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9683_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9683_0_0_0;
extern Il2CppType IEnumerable_1_t9683_1_0_0;
struct IEnumerable_1_t9683;
extern Il2CppGenericClass IEnumerable_1_t9683_GenericClass;
TypeInfo IEnumerable_1_t9683_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9683_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9683_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9683_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9683_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9683_0_0_0/* byval_arg */
	, &IEnumerable_1_t9683_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9683_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9682_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Environment/SpecialFolder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Environment/SpecialFolder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Environment/SpecialFolder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Environment/SpecialFolder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Environment/SpecialFolder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Environment/SpecialFolder>
extern MethodInfo IList_1_get_Item_m53734_MethodInfo;
extern MethodInfo IList_1_set_Item_m53735_MethodInfo;
static PropertyInfo IList_1_t9682____Item_PropertyInfo = 
{
	&IList_1_t9682_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53734_MethodInfo/* get */
	, &IList_1_set_Item_m53735_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9682_PropertyInfos[] =
{
	&IList_1_t9682____Item_PropertyInfo,
	NULL
};
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo IList_1_t9682_IList_1_IndexOf_m53736_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53736_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Environment/SpecialFolder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53736_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9682_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9682_IList_1_IndexOf_m53736_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53736_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo IList_1_t9682_IList_1_Insert_m53737_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53737_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Environment/SpecialFolder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53737_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9682_IList_1_Insert_m53737_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53737_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9682_IList_1_RemoveAt_m53738_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53738_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Environment/SpecialFolder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53738_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9682_IList_1_RemoveAt_m53738_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53738_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9682_IList_1_get_Item_m53734_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SpecialFolder_t2267_0_0_0;
extern void* RuntimeInvoker_SpecialFolder_t2267_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53734_GenericMethod;
// T System.Collections.Generic.IList`1<System.Environment/SpecialFolder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53734_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9682_il2cpp_TypeInfo/* declaring_type */
	, &SpecialFolder_t2267_0_0_0/* return_type */
	, RuntimeInvoker_SpecialFolder_t2267_Int32_t123/* invoker_method */
	, IList_1_t9682_IList_1_get_Item_m53734_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53734_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo IList_1_t9682_IList_1_set_Item_m53735_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53735_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Environment/SpecialFolder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53735_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9682_IList_1_set_Item_m53735_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53735_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9682_MethodInfos[] =
{
	&IList_1_IndexOf_m53736_MethodInfo,
	&IList_1_Insert_m53737_MethodInfo,
	&IList_1_RemoveAt_m53738_MethodInfo,
	&IList_1_get_Item_m53734_MethodInfo,
	&IList_1_set_Item_m53735_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9682_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9681_il2cpp_TypeInfo,
	&IEnumerable_1_t9683_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9682_0_0_0;
extern Il2CppType IList_1_t9682_1_0_0;
struct IList_1_t9682;
extern Il2CppGenericClass IList_1_t9682_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9682_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9682_MethodInfos/* methods */
	, IList_1_t9682_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9682_il2cpp_TypeInfo/* element_class */
	, IList_1_t9682_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9682_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9682_0_0_0/* byval_arg */
	, &IList_1_t9682_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9682_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t2728_il2cpp_TypeInfo;

// System.Guid
#include "mscorlib_System_Guid.h"


// System.Int32 System.IComparable`1<System.Guid>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.Guid>
extern Il2CppType Guid_t2274_0_0_0;
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo IComparable_1_t2728_IComparable_1_CompareTo_m53739_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m53739_GenericMethod;
// System.Int32 System.IComparable`1<System.Guid>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m53739_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t2728_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Guid_t2274/* invoker_method */
	, IComparable_1_t2728_IComparable_1_CompareTo_m53739_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m53739_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t2728_MethodInfos[] =
{
	&IComparable_1_CompareTo_m53739_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t2728_0_0_0;
extern Il2CppType IComparable_1_t2728_1_0_0;
struct IComparable_1_t2728;
extern Il2CppGenericClass IComparable_1_t2728_GenericClass;
TypeInfo IComparable_1_t2728_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t2728_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2728_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t2728_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t2728_0_0_0/* byval_arg */
	, &IComparable_1_t2728_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t2728_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t2729_il2cpp_TypeInfo;



// System.Boolean System.IEquatable`1<System.Guid>::Equals(T)
// Metadata Definition System.IEquatable`1<System.Guid>
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo IEquatable_1_t2729_IEquatable_1_Equals_m53740_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m53740_GenericMethod;
// System.Boolean System.IEquatable`1<System.Guid>::Equals(T)
MethodInfo IEquatable_1_Equals_m53740_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t2729_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Guid_t2274/* invoker_method */
	, IEquatable_1_t2729_IEquatable_1_Equals_m53740_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m53740_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t2729_MethodInfos[] =
{
	&IEquatable_1_Equals_m53740_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t2729_0_0_0;
extern Il2CppType IEquatable_1_t2729_1_0_0;
struct IEquatable_1_t2729;
extern Il2CppGenericClass IEquatable_1_t2729_GenericClass;
TypeInfo IEquatable_1_t2729_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t2729_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t2729_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t2729_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t2729_0_0_0/* byval_arg */
	, &IEquatable_1_t2729_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t2729_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.GenericComparer`1<System.Guid>
#include "mscorlib_System_Collections_Generic_GenericComparer_1_gen_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GenericComparer_1_t2726_il2cpp_TypeInfo;
// System.Collections.Generic.GenericComparer`1<System.Guid>
#include "mscorlib_System_Collections_Generic_GenericComparer_1_gen_1MethodDeclarations.h"

extern TypeInfo Guid_t2274_il2cpp_TypeInfo;
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
// System.Collections.Generic.Comparer`1<System.Guid>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_59MethodDeclarations.h"
extern MethodInfo Comparer_1__ctor_m32159_MethodInfo;
extern MethodInfo IComparable_1_CompareTo_m53739_MethodInfo;


// System.Void System.Collections.Generic.GenericComparer`1<System.Guid>::.ctor()
extern MethodInfo GenericComparer_1__ctor_m14131_MethodInfo;
 void GenericComparer_1__ctor_m14131 (GenericComparer_1_t2726 * __this, MethodInfo* method){
	{
		Comparer_1__ctor_m32159(__this, /*hidden argument*/&Comparer_1__ctor_m32159_MethodInfo);
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Guid>::Compare(T,T)
extern MethodInfo GenericComparer_1_Compare_m32158_MethodInfo;
 int32_t GenericComparer_1_Compare_m32158 (GenericComparer_1_t2726 * __this, Guid_t2274  ___x, Guid_t2274  ___y, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		Guid_t2274  L_0 = ___x;
		Object_t * L_1 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		Guid_t2274  L_2 = ___y;
		Object_t * L_3 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		Guid_t2274  L_4 = ___y;
		Object_t * L_5 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		NullCheck(Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &(*(&___x))));
		int32_t L_6 = (int32_t)InterfaceFuncInvoker1< int32_t, Guid_t2274  >::Invoke(&IComparable_1_CompareTo_m53739_MethodInfo, Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &(*(&___x))), ___y);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.GenericComparer`1<System.Guid>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod GenericComparer_1__ctor_m14131_GenericMethod;
// System.Void System.Collections.Generic.GenericComparer`1<System.Guid>::.ctor()
MethodInfo GenericComparer_1__ctor_m14131_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericComparer_1__ctor_m14131/* method */
	, &GenericComparer_1_t2726_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &GenericComparer_1__ctor_m14131_GenericMethod/* genericMethod */

};
extern Il2CppType Guid_t2274_0_0_0;
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo GenericComparer_1_t2726_GenericComparer_1_Compare_m32158_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Guid_t2274_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod GenericComparer_1_Compare_m32158_GenericMethod;
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Guid>::Compare(T,T)
MethodInfo GenericComparer_1_Compare_m32158_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&GenericComparer_1_Compare_m32158/* method */
	, &GenericComparer_1_t2726_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Guid_t2274_Guid_t2274/* invoker_method */
	, GenericComparer_1_t2726_GenericComparer_1_Compare_m32158_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &GenericComparer_1_Compare_m32158_GenericMethod/* genericMethod */

};
static MethodInfo* GenericComparer_1_t2726_MethodInfos[] =
{
	&GenericComparer_1__ctor_m14131_MethodInfo,
	&GenericComparer_1_Compare_m32158_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo Comparer_1_System_Collections_IComparer_Compare_m32161_MethodInfo;
static MethodInfo* GenericComparer_1_t2726_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&GenericComparer_1_Compare_m32158_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m32161_MethodInfo,
	&GenericComparer_1_Compare_m32158_MethodInfo,
};
extern TypeInfo IComparer_1_t10026_il2cpp_TypeInfo;
extern TypeInfo IComparer_t1356_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair GenericComparer_1_t2726_InterfacesOffsets[] = 
{
	{ &IComparer_1_t10026_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType GenericComparer_1_t2726_0_0_0;
extern Il2CppType GenericComparer_1_t2726_1_0_0;
extern TypeInfo Comparer_1_t5342_il2cpp_TypeInfo;
struct GenericComparer_1_t2726;
extern Il2CppGenericClass GenericComparer_1_t2726_GenericClass;
TypeInfo GenericComparer_1_t2726_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, GenericComparer_1_t2726_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Comparer_1_t5342_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GenericComparer_1_t2726_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, GenericComparer_1_t2726_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GenericComparer_1_t2726_il2cpp_TypeInfo/* cast_class */
	, &GenericComparer_1_t2726_0_0_0/* byval_arg */
	, &GenericComparer_1_t2726_1_0_0/* this_arg */
	, GenericComparer_1_t2726_InterfacesOffsets/* interface_offsets */
	, &GenericComparer_1_t2726_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericComparer_1_t2726)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.Comparer`1<System.Guid>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_59.h"
#ifndef _MSC_VER
#else
#endif

// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Collections.Generic.GenericComparer`1
#include "mscorlib_System_Collections_Generic_GenericComparer_1.h"
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_60.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo Boolean_t122_il2cpp_TypeInfo;
extern TypeInfo GenericComparer_1_t1855_il2cpp_TypeInfo;
extern TypeInfo TypeU5BU5D_t922_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t5343_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_60MethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern Il2CppType GenericComparer_1_t1855_0_0_0;
extern MethodInfo Object__ctor_m312_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Type_IsAssignableFrom_m6740_MethodInfo;
extern MethodInfo Type_MakeGenericType_m6738_MethodInfo;
extern MethodInfo Activator_CreateInstance_m12682_MethodInfo;
extern MethodInfo DefaultComparer__ctor_m32163_MethodInfo;
extern MethodInfo Comparer_1_Compare_m53741_MethodInfo;
extern MethodInfo ArgumentException__ctor_m12706_MethodInfo;


// System.Void System.Collections.Generic.Comparer`1<System.Guid>::.ctor()
 void Comparer_1__ctor_m32159 (Comparer_1_t5342 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Guid>::.cctor()
extern MethodInfo Comparer_1__cctor_m32160_MethodInfo;
 void Comparer_1__cctor_m32160 (Object_t * __this/* static, unused */, MethodInfo* method){
	DefaultComparer_t5343 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (DefaultComparer_t5343 *)il2cpp_codegen_object_new(InitializedTypeInfo(&DefaultComparer_t5343_il2cpp_TypeInfo));
	DefaultComparer__ctor_m32163(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &DefaultComparer__ctor_m32163_MethodInfo);
	((Comparer_1_t5342_StaticFields*)InitializedTypeInfo(&Comparer_1_t5342_il2cpp_TypeInfo)->static_fields)->____default_0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Guid>::System.Collections.IComparer.Compare(System.Object,System.Object)
 int32_t Comparer_1_System_Collections_IComparer_Compare_m32161 (Comparer_1_t5342 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (___x)
		{
			goto IL_000b;
		}
	}
	{
		if (___y)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (___y)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((Object_t *)IsInst(___x, InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((Object_t *)IsInst(___y, InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, Guid_t2274 , Guid_t2274  >::Invoke(&Comparer_1_Compare_m53741_MethodInfo, __this, ((*(Guid_t2274 *)((Guid_t2274 *)UnBox (___x, InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo))))), ((*(Guid_t2274 *)((Guid_t2274 *)UnBox (___y, InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo))))));
		return L_0;
	}

IL_0033:
	{
		ArgumentException_t551 * L_1 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m12706(L_1, /*hidden argument*/&ArgumentException__ctor_m12706_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Guid>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Guid>::get_Default()
extern MethodInfo Comparer_1_get_Default_m32162_MethodInfo;
 Comparer_1_t5342 * Comparer_1_get_Default_m32162 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Comparer_1_t5342_il2cpp_TypeInfo));
		return (((Comparer_1_t5342_StaticFields*)InitializedTypeInfo(&Comparer_1_t5342_il2cpp_TypeInfo)->static_fields)->____default_0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<System.Guid>
extern Il2CppType Comparer_1_t5342_0_0_49;
FieldInfo Comparer_1_t5342_____default_0_FieldInfo = 
{
	"_default"/* name */
	, &Comparer_1_t5342_0_0_49/* type */
	, &Comparer_1_t5342_il2cpp_TypeInfo/* parent */
	, offsetof(Comparer_1_t5342_StaticFields, ____default_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Comparer_1_t5342_FieldInfos[] =
{
	&Comparer_1_t5342_____default_0_FieldInfo,
	NULL
};
static PropertyInfo Comparer_1_t5342____Default_PropertyInfo = 
{
	&Comparer_1_t5342_il2cpp_TypeInfo/* parent */
	, "Default"/* name */
	, &Comparer_1_get_Default_m32162_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Comparer_1_t5342_PropertyInfos[] =
{
	&Comparer_1_t5342____Default_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1__ctor_m32159_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1<System.Guid>::.ctor()
MethodInfo Comparer_1__ctor_m32159_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Comparer_1__ctor_m32159/* method */
	, &Comparer_1_t5342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1__ctor_m32159_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1__cctor_m32160_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1<System.Guid>::.cctor()
MethodInfo Comparer_1__cctor_m32160_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Comparer_1__cctor_m32160/* method */
	, &Comparer_1_t5342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1__cctor_m32160_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Comparer_1_t5342_Comparer_1_System_Collections_IComparer_Compare_m32161_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_System_Collections_IComparer_Compare_m32161_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1<System.Guid>::System.Collections.IComparer.Compare(System.Object,System.Object)
MethodInfo Comparer_1_System_Collections_IComparer_Compare_m32161_MethodInfo = 
{
	"System.Collections.IComparer.Compare"/* name */
	, (methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m32161/* method */
	, &Comparer_1_t5342_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, Comparer_1_t5342_Comparer_1_System_Collections_IComparer_Compare_m32161_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_System_Collections_IComparer_Compare_m32161_GenericMethod/* genericMethod */

};
extern Il2CppType Guid_t2274_0_0_0;
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo Comparer_1_t5342_Comparer_1_Compare_m53741_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Guid_t2274_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_Compare_m53741_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1<System.Guid>::Compare(T,T)
MethodInfo Comparer_1_Compare_m53741_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &Comparer_1_t5342_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Guid_t2274_Guid_t2274/* invoker_method */
	, Comparer_1_t5342_Comparer_1_Compare_m53741_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_Compare_m53741_GenericMethod/* genericMethod */

};
extern Il2CppType Comparer_1_t5342_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_get_Default_m32162_GenericMethod;
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Guid>::get_Default()
MethodInfo Comparer_1_get_Default_m32162_MethodInfo = 
{
	"get_Default"/* name */
	, (methodPointerType)&Comparer_1_get_Default_m32162/* method */
	, &Comparer_1_t5342_il2cpp_TypeInfo/* declaring_type */
	, &Comparer_1_t5342_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_get_Default_m32162_GenericMethod/* genericMethod */

};
static MethodInfo* Comparer_1_t5342_MethodInfos[] =
{
	&Comparer_1__ctor_m32159_MethodInfo,
	&Comparer_1__cctor_m32160_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m32161_MethodInfo,
	&Comparer_1_Compare_m53741_MethodInfo,
	&Comparer_1_get_Default_m32162_MethodInfo,
	NULL
};
static MethodInfo* Comparer_1_t5342_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&Comparer_1_Compare_m53741_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m32161_MethodInfo,
	NULL,
};
static TypeInfo* Comparer_1_t5342_InterfacesTypeInfos[] = 
{
	&IComparer_1_t10026_il2cpp_TypeInfo,
	&IComparer_t1356_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Comparer_1_t5342_InterfacesOffsets[] = 
{
	{ &IComparer_1_t10026_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Comparer_1_t5342_0_0_0;
extern Il2CppType Comparer_1_t5342_1_0_0;
extern TypeInfo Object_t_il2cpp_TypeInfo;
struct Comparer_1_t5342;
extern Il2CppGenericClass Comparer_1_t5342_GenericClass;
TypeInfo Comparer_1_t5342_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, Comparer_1_t5342_MethodInfos/* methods */
	, Comparer_1_t5342_PropertyInfos/* properties */
	, Comparer_1_t5342_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Comparer_1_t5342_il2cpp_TypeInfo/* element_class */
	, Comparer_1_t5342_InterfacesTypeInfos/* implemented_interfaces */
	, Comparer_1_t5342_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Comparer_1_t5342_il2cpp_TypeInfo/* cast_class */
	, &Comparer_1_t5342_0_0_0/* byval_arg */
	, &Comparer_1_t5342_1_0_0/* this_arg */
	, Comparer_1_t5342_InterfacesOffsets/* interface_offsets */
	, &Comparer_1_t5342_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Comparer_1_t5342)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Comparer_1_t5342_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IComparer`1<System.Guid>::Compare(T,T)
// Metadata Definition System.Collections.Generic.IComparer`1<System.Guid>
extern Il2CppType Guid_t2274_0_0_0;
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo IComparer_1_t10026_IComparer_1_Compare_m53742_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Guid_t2274_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparer_1_Compare_m53742_GenericMethod;
// System.Int32 System.Collections.Generic.IComparer`1<System.Guid>::Compare(T,T)
MethodInfo IComparer_1_Compare_m53742_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &IComparer_1_t10026_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Guid_t2274_Guid_t2274/* invoker_method */
	, IComparer_1_t10026_IComparer_1_Compare_m53742_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparer_1_Compare_m53742_GenericMethod/* genericMethod */

};
static MethodInfo* IComparer_1_t10026_MethodInfos[] =
{
	&IComparer_1_Compare_m53742_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparer_1_t10026_0_0_0;
extern Il2CppType IComparer_1_t10026_1_0_0;
struct IComparer_1_t10026;
extern Il2CppGenericClass IComparer_1_t10026_GenericClass;
TypeInfo IComparer_1_t10026_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IComparer_1_t10026_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparer_1_t10026_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparer_1_t10026_il2cpp_TypeInfo/* cast_class */
	, &IComparer_1_t10026_0_0_0/* byval_arg */
	, &IComparer_1_t10026_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparer_1_t10026_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo IComparable_t184_il2cpp_TypeInfo;
extern MethodInfo IComparable_CompareTo_m13522_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;


// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
 void DefaultComparer__ctor_m32163 (DefaultComparer_t5343 * __this, MethodInfo* method){
	{
		Comparer_1__ctor_m32159(__this, /*hidden argument*/&Comparer_1__ctor_m32159_MethodInfo);
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
extern MethodInfo DefaultComparer_Compare_m32164_MethodInfo;
 int32_t DefaultComparer_Compare_m32164 (DefaultComparer_t5343 * __this, Guid_t2274  ___x, Guid_t2274  ___y, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		Guid_t2274  L_0 = ___x;
		Object_t * L_1 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		Guid_t2274  L_2 = ___y;
		Object_t * L_3 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		Guid_t2274  L_4 = ___y;
		Object_t * L_5 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		Guid_t2274  L_6 = ___x;
		Object_t * L_7 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_6);
		if (!((Object_t*)IsInst(L_7, InitializedTypeInfo(&IComparable_1_t2728_il2cpp_TypeInfo))))
		{
			goto IL_003e;
		}
	}
	{
		Guid_t2274  L_8 = ___x;
		Object_t * L_9 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_8);
		NullCheck(((Object_t*)Castclass(L_9, InitializedTypeInfo(&IComparable_1_t2728_il2cpp_TypeInfo))));
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, Guid_t2274  >::Invoke(&IComparable_1_CompareTo_m53739_MethodInfo, ((Object_t*)Castclass(L_9, InitializedTypeInfo(&IComparable_1_t2728_il2cpp_TypeInfo))), ___y);
		return L_10;
	}

IL_003e:
	{
		Guid_t2274  L_11 = ___x;
		Object_t * L_12 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_11);
		if (!((Object_t *)IsInst(L_12, InitializedTypeInfo(&IComparable_t184_il2cpp_TypeInfo))))
		{
			goto IL_0062;
		}
	}
	{
		Guid_t2274  L_13 = ___x;
		Object_t * L_14 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_13);
		Guid_t2274  L_15 = ___y;
		Object_t * L_16 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_15);
		NullCheck(((Object_t *)Castclass(L_14, InitializedTypeInfo(&IComparable_t184_il2cpp_TypeInfo))));
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(&IComparable_CompareTo_m13522_MethodInfo, ((Object_t *)Castclass(L_14, InitializedTypeInfo(&IComparable_t184_il2cpp_TypeInfo))), L_16);
		return L_17;
	}

IL_0062:
	{
		ArgumentException_t551 * L_18 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_18, (String_t*) &_stringLiteral1447, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer__ctor_m32163_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
MethodInfo DefaultComparer__ctor_m32163_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultComparer__ctor_m32163/* method */
	, &DefaultComparer_t5343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer__ctor_m32163_GenericMethod/* genericMethod */

};
extern Il2CppType Guid_t2274_0_0_0;
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo DefaultComparer_t5343_DefaultComparer_Compare_m32164_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Guid_t2274_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_Compare_m32164_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
MethodInfo DefaultComparer_Compare_m32164_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&DefaultComparer_Compare_m32164/* method */
	, &DefaultComparer_t5343_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Guid_t2274_Guid_t2274/* invoker_method */
	, DefaultComparer_t5343_DefaultComparer_Compare_m32164_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_Compare_m32164_GenericMethod/* genericMethod */

};
static MethodInfo* DefaultComparer_t5343_MethodInfos[] =
{
	&DefaultComparer__ctor_m32163_MethodInfo,
	&DefaultComparer_Compare_m32164_MethodInfo,
	NULL
};
static MethodInfo* DefaultComparer_t5343_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&DefaultComparer_Compare_m32164_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m32161_MethodInfo,
	&DefaultComparer_Compare_m32164_MethodInfo,
};
static Il2CppInterfaceOffsetPair DefaultComparer_t5343_InterfacesOffsets[] = 
{
	{ &IComparer_1_t10026_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType DefaultComparer_t5343_0_0_0;
extern Il2CppType DefaultComparer_t5343_1_0_0;
struct DefaultComparer_t5343;
extern Il2CppGenericClass DefaultComparer_t5343_GenericClass;
extern TypeInfo Comparer_1_t1854_il2cpp_TypeInfo;
TypeInfo DefaultComparer_t5343_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, DefaultComparer_t5343_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Comparer_1_t5342_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Comparer_1_t1854_il2cpp_TypeInfo/* nested_in */
	, &DefaultComparer_t5343_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultComparer_t5343_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultComparer_t5343_il2cpp_TypeInfo/* cast_class */
	, &DefaultComparer_t5343_0_0_0/* byval_arg */
	, &DefaultComparer_t5343_1_0_0/* this_arg */
	, DefaultComparer_t5343_InterfacesOffsets/* interface_offsets */
	, &DefaultComparer_t5343_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultComparer_t5343)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer__2.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GenericEqualityComparer_1_t2727_il2cpp_TypeInfo;
// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer__2MethodDeclarations.h"

// System.Collections.Generic.EqualityComparer`1<System.Guid>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_77MethodDeclarations.h"
extern MethodInfo EqualityComparer_1__ctor_m32167_MethodInfo;
extern MethodInfo IEquatable_1_Equals_m53740_MethodInfo;


// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::.ctor()
extern MethodInfo GenericEqualityComparer_1__ctor_m14132_MethodInfo;
 void GenericEqualityComparer_1__ctor_m14132 (GenericEqualityComparer_1_t2727 * __this, MethodInfo* method){
	{
		EqualityComparer_1__ctor_m32167(__this, /*hidden argument*/&EqualityComparer_1__ctor_m32167_MethodInfo);
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::GetHashCode(T)
extern MethodInfo GenericEqualityComparer_1_GetHashCode_m32165_MethodInfo;
 int32_t GenericEqualityComparer_1_GetHashCode_m32165 (GenericEqualityComparer_1_t2727 * __this, Guid_t2274  ___obj, MethodInfo* method){
	{
		Guid_t2274  L_0 = ___obj;
		Object_t * L_1 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		NullCheck(Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &(*(&___obj))));
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&Object_GetHashCode_m321_MethodInfo, Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &(*(&___obj))));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::Equals(T,T)
extern MethodInfo GenericEqualityComparer_1_Equals_m32166_MethodInfo;
 bool GenericEqualityComparer_1_Equals_m32166 (GenericEqualityComparer_1_t2727 * __this, Guid_t2274  ___x, Guid_t2274  ___y, MethodInfo* method){
	{
		Guid_t2274  L_0 = ___x;
		Object_t * L_1 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		Guid_t2274  L_2 = ___y;
		Object_t * L_3 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_2);
		return ((((Object_t *)L_3) == ((Object_t *)NULL))? 1 : 0);
	}

IL_0012:
	{
		NullCheck(Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &(*(&___x))));
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, Guid_t2274  >::Invoke(&IEquatable_1_Equals_m53740_MethodInfo, Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &(*(&___x))), ___y);
		return L_4;
	}
}
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod GenericEqualityComparer_1__ctor_m14132_GenericMethod;
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::.ctor()
MethodInfo GenericEqualityComparer_1__ctor_m14132_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericEqualityComparer_1__ctor_m14132/* method */
	, &GenericEqualityComparer_1_t2727_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &GenericEqualityComparer_1__ctor_m14132_GenericMethod/* genericMethod */

};
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo GenericEqualityComparer_1_t2727_GenericEqualityComparer_1_GetHashCode_m32165_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod GenericEqualityComparer_1_GetHashCode_m32165_GenericMethod;
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::GetHashCode(T)
MethodInfo GenericEqualityComparer_1_GetHashCode_m32165_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&GenericEqualityComparer_1_GetHashCode_m32165/* method */
	, &GenericEqualityComparer_1_t2727_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Guid_t2274/* invoker_method */
	, GenericEqualityComparer_1_t2727_GenericEqualityComparer_1_GetHashCode_m32165_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &GenericEqualityComparer_1_GetHashCode_m32165_GenericMethod/* genericMethod */

};
extern Il2CppType Guid_t2274_0_0_0;
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo GenericEqualityComparer_1_t2727_GenericEqualityComparer_1_Equals_m32166_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Guid_t2274_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod GenericEqualityComparer_1_Equals_m32166_GenericMethod;
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::Equals(T,T)
MethodInfo GenericEqualityComparer_1_Equals_m32166_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&GenericEqualityComparer_1_Equals_m32166/* method */
	, &GenericEqualityComparer_1_t2727_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Guid_t2274_Guid_t2274/* invoker_method */
	, GenericEqualityComparer_1_t2727_GenericEqualityComparer_1_Equals_m32166_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &GenericEqualityComparer_1_Equals_m32166_GenericMethod/* genericMethod */

};
static MethodInfo* GenericEqualityComparer_1_t2727_MethodInfos[] =
{
	&GenericEqualityComparer_1__ctor_m14132_MethodInfo,
	&GenericEqualityComparer_1_GetHashCode_m32165_MethodInfo,
	&GenericEqualityComparer_1_Equals_m32166_MethodInfo,
	NULL
};
extern MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170_MethodInfo;
extern MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169_MethodInfo;
static MethodInfo* GenericEqualityComparer_1_t2727_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&GenericEqualityComparer_1_Equals_m32166_MethodInfo,
	&GenericEqualityComparer_1_GetHashCode_m32165_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169_MethodInfo,
	&GenericEqualityComparer_1_GetHashCode_m32165_MethodInfo,
	&GenericEqualityComparer_1_Equals_m32166_MethodInfo,
};
extern TypeInfo IEqualityComparer_1_t10027_il2cpp_TypeInfo;
extern TypeInfo IEqualityComparer_t1363_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair GenericEqualityComparer_1_t2727_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t10027_il2cpp_TypeInfo, 4},
	{ &IEqualityComparer_t1363_il2cpp_TypeInfo, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType GenericEqualityComparer_1_t2727_0_0_0;
extern Il2CppType GenericEqualityComparer_1_t2727_1_0_0;
extern TypeInfo EqualityComparer_1_t5344_il2cpp_TypeInfo;
struct GenericEqualityComparer_1_t2727;
extern Il2CppGenericClass GenericEqualityComparer_1_t2727_GenericClass;
TypeInfo GenericEqualityComparer_1_t2727_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericEqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, GenericEqualityComparer_1_t2727_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GenericEqualityComparer_1_t2727_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, GenericEqualityComparer_1_t2727_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GenericEqualityComparer_1_t2727_il2cpp_TypeInfo/* cast_class */
	, &GenericEqualityComparer_1_t2727_0_0_0/* byval_arg */
	, &GenericEqualityComparer_1_t2727_1_0_0/* this_arg */
	, GenericEqualityComparer_1_t2727_InterfacesOffsets/* interface_offsets */
	, &GenericEqualityComparer_1_t2727_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericEqualityComparer_1_t2727)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.EqualityComparer`1<System.Guid>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_77.h"
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.GenericEqualityComparer`1
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer_.h"
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_78.h"
extern TypeInfo GenericEqualityComparer_1_t1867_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t5345_il2cpp_TypeInfo;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_78MethodDeclarations.h"
extern Il2CppType GenericEqualityComparer_1_t1867_0_0_0;
extern MethodInfo DefaultComparer__ctor_m32172_MethodInfo;
extern MethodInfo EqualityComparer_1_GetHashCode_m53743_MethodInfo;
extern MethodInfo EqualityComparer_1_Equals_m53744_MethodInfo;


// System.Void System.Collections.Generic.EqualityComparer`1<System.Guid>::.ctor()
 void EqualityComparer_1__ctor_m32167 (EqualityComparer_1_t5344 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Guid>::.cctor()
extern MethodInfo EqualityComparer_1__cctor_m32168_MethodInfo;
 void EqualityComparer_1__cctor_m32168 (Object_t * __this/* static, unused */, MethodInfo* method){
	DefaultComparer_t5345 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (DefaultComparer_t5345 *)il2cpp_codegen_object_new(InitializedTypeInfo(&DefaultComparer_t5345_il2cpp_TypeInfo));
	DefaultComparer__ctor_m32172(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &DefaultComparer__ctor_m32172_MethodInfo);
	((EqualityComparer_1_t5344_StaticFields*)InitializedTypeInfo(&EqualityComparer_1_t5344_il2cpp_TypeInfo)->static_fields)->____default_0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
 int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169 (EqualityComparer_1_t5344 * __this, Object_t * ___obj, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, Guid_t2274  >::Invoke(&EqualityComparer_1_GetHashCode_m53743_MethodInfo, __this, ((*(Guid_t2274 *)((Guid_t2274 *)UnBox (___obj, InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo))))));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
 bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170 (EqualityComparer_1_t5344 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, Guid_t2274 , Guid_t2274  >::Invoke(&EqualityComparer_1_Equals_m53744_MethodInfo, __this, ((*(Guid_t2274 *)((Guid_t2274 *)UnBox (___x, InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo))))), ((*(Guid_t2274 *)((Guid_t2274 *)UnBox (___y, InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo))))));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Guid>::get_Default()
extern MethodInfo EqualityComparer_1_get_Default_m32171_MethodInfo;
 EqualityComparer_1_t5344 * EqualityComparer_1_get_Default_m32171 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&EqualityComparer_1_t5344_il2cpp_TypeInfo));
		return (((EqualityComparer_1_t5344_StaticFields*)InitializedTypeInfo(&EqualityComparer_1_t5344_il2cpp_TypeInfo)->static_fields)->____default_0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.Guid>
extern Il2CppType EqualityComparer_1_t5344_0_0_49;
FieldInfo EqualityComparer_1_t5344_____default_0_FieldInfo = 
{
	"_default"/* name */
	, &EqualityComparer_1_t5344_0_0_49/* type */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* parent */
	, offsetof(EqualityComparer_1_t5344_StaticFields, ____default_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* EqualityComparer_1_t5344_FieldInfos[] =
{
	&EqualityComparer_1_t5344_____default_0_FieldInfo,
	NULL
};
static PropertyInfo EqualityComparer_1_t5344____Default_PropertyInfo = 
{
	&EqualityComparer_1_t5344_il2cpp_TypeInfo/* parent */
	, "Default"/* name */
	, &EqualityComparer_1_get_Default_m32171_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* EqualityComparer_1_t5344_PropertyInfos[] =
{
	&EqualityComparer_1_t5344____Default_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1__ctor_m32167_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1<System.Guid>::.ctor()
MethodInfo EqualityComparer_1__ctor_m32167_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EqualityComparer_1__ctor_m32167/* method */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1__ctor_m32167_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1__cctor_m32168_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1<System.Guid>::.cctor()
MethodInfo EqualityComparer_1__cctor_m32168_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&EqualityComparer_1__cctor_m32168/* method */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1__cctor_m32168_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo EqualityComparer_1_t5344_EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169_MethodInfo = 
{
	"System.Collections.IEqualityComparer.GetHashCode"/* name */
	, (methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169/* method */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, EqualityComparer_1_t5344_EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo EqualityComparer_1_t5344_EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170_MethodInfo = 
{
	"System.Collections.IEqualityComparer.Equals"/* name */
	, (methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170/* method */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, EqualityComparer_1_t5344_EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170_GenericMethod/* genericMethod */

};
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo EqualityComparer_1_t5344_EqualityComparer_1_GetHashCode_m53743_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_GetHashCode_m53743_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Guid>::GetHashCode(T)
MethodInfo EqualityComparer_1_GetHashCode_m53743_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Guid_t2274/* invoker_method */
	, EqualityComparer_1_t5344_EqualityComparer_1_GetHashCode_m53743_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_GetHashCode_m53743_GenericMethod/* genericMethod */

};
extern Il2CppType Guid_t2274_0_0_0;
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo EqualityComparer_1_t5344_EqualityComparer_1_Equals_m53744_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Guid_t2274_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_Equals_m53744_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Guid>::Equals(T,T)
MethodInfo EqualityComparer_1_Equals_m53744_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Guid_t2274_Guid_t2274/* invoker_method */
	, EqualityComparer_1_t5344_EqualityComparer_1_Equals_m53744_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_Equals_m53744_GenericMethod/* genericMethod */

};
extern Il2CppType EqualityComparer_1_t5344_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_get_Default_m32171_GenericMethod;
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Guid>::get_Default()
MethodInfo EqualityComparer_1_get_Default_m32171_MethodInfo = 
{
	"get_Default"/* name */
	, (methodPointerType)&EqualityComparer_1_get_Default_m32171/* method */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* declaring_type */
	, &EqualityComparer_1_t5344_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_get_Default_m32171_GenericMethod/* genericMethod */

};
static MethodInfo* EqualityComparer_1_t5344_MethodInfos[] =
{
	&EqualityComparer_1__ctor_m32167_MethodInfo,
	&EqualityComparer_1__cctor_m32168_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170_MethodInfo,
	&EqualityComparer_1_GetHashCode_m53743_MethodInfo,
	&EqualityComparer_1_Equals_m53744_MethodInfo,
	&EqualityComparer_1_get_Default_m32171_MethodInfo,
	NULL
};
static MethodInfo* EqualityComparer_1_t5344_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&EqualityComparer_1_Equals_m53744_MethodInfo,
	&EqualityComparer_1_GetHashCode_m53743_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169_MethodInfo,
	NULL,
	NULL,
};
static TypeInfo* EqualityComparer_1_t5344_InterfacesTypeInfos[] = 
{
	&IEqualityComparer_1_t10027_il2cpp_TypeInfo,
	&IEqualityComparer_t1363_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair EqualityComparer_1_t5344_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t10027_il2cpp_TypeInfo, 4},
	{ &IEqualityComparer_t1363_il2cpp_TypeInfo, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType EqualityComparer_1_t5344_0_0_0;
extern Il2CppType EqualityComparer_1_t5344_1_0_0;
struct EqualityComparer_1_t5344;
extern Il2CppGenericClass EqualityComparer_1_t5344_GenericClass;
TypeInfo EqualityComparer_1_t5344_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, EqualityComparer_1_t5344_MethodInfos/* methods */
	, EqualityComparer_1_t5344_PropertyInfos/* properties */
	, EqualityComparer_1_t5344_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* element_class */
	, EqualityComparer_1_t5344_InterfacesTypeInfos/* implemented_interfaces */
	, EqualityComparer_1_t5344_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* cast_class */
	, &EqualityComparer_1_t5344_0_0_0/* byval_arg */
	, &EqualityComparer_1_t5344_1_0_0/* this_arg */
	, EqualityComparer_1_t5344_InterfacesOffsets/* interface_offsets */
	, &EqualityComparer_1_t5344_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EqualityComparer_1_t5344)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EqualityComparer_1_t5344_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Guid>::Equals(T,T)
// System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Guid>::GetHashCode(T)
// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.Guid>
extern Il2CppType Guid_t2274_0_0_0;
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo IEqualityComparer_1_t10027_IEqualityComparer_1_Equals_m53745_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Guid_t2274_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEqualityComparer_1_Equals_m53745_GenericMethod;
// System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Guid>::Equals(T,T)
MethodInfo IEqualityComparer_1_Equals_m53745_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEqualityComparer_1_t10027_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Guid_t2274_Guid_t2274/* invoker_method */
	, IEqualityComparer_1_t10027_IEqualityComparer_1_Equals_m53745_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEqualityComparer_1_Equals_m53745_GenericMethod/* genericMethod */

};
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo IEqualityComparer_1_t10027_IEqualityComparer_1_GetHashCode_m53746_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEqualityComparer_1_GetHashCode_m53746_GenericMethod;
// System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Guid>::GetHashCode(T)
MethodInfo IEqualityComparer_1_GetHashCode_m53746_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &IEqualityComparer_1_t10027_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Guid_t2274/* invoker_method */
	, IEqualityComparer_1_t10027_IEqualityComparer_1_GetHashCode_m53746_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEqualityComparer_1_GetHashCode_m53746_GenericMethod/* genericMethod */

};
static MethodInfo* IEqualityComparer_1_t10027_MethodInfos[] =
{
	&IEqualityComparer_1_Equals_m53745_MethodInfo,
	&IEqualityComparer_1_GetHashCode_m53746_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEqualityComparer_1_t10027_0_0_0;
extern Il2CppType IEqualityComparer_1_t10027_1_0_0;
struct IEqualityComparer_1_t10027;
extern Il2CppGenericClass IEqualityComparer_1_t10027_GenericClass;
TypeInfo IEqualityComparer_1_t10027_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEqualityComparer_1_t10027_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEqualityComparer_1_t10027_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEqualityComparer_1_t10027_il2cpp_TypeInfo/* cast_class */
	, &IEqualityComparer_1_t10027_0_0_0/* byval_arg */
	, &IEqualityComparer_1_t10027_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEqualityComparer_1_t10027_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::.ctor()
 void DefaultComparer__ctor_m32172 (DefaultComparer_t5345 * __this, MethodInfo* method){
	{
		EqualityComparer_1__ctor_m32167(__this, /*hidden argument*/&EqualityComparer_1__ctor_m32167_MethodInfo);
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::GetHashCode(T)
extern MethodInfo DefaultComparer_GetHashCode_m32173_MethodInfo;
 int32_t DefaultComparer_GetHashCode_m32173 (DefaultComparer_t5345 * __this, Guid_t2274  ___obj, MethodInfo* method){
	{
		Guid_t2274  L_0 = ___obj;
		Object_t * L_1 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		NullCheck(Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &(*(&___obj))));
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&Object_GetHashCode_m321_MethodInfo, Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &(*(&___obj))));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::Equals(T,T)
extern MethodInfo DefaultComparer_Equals_m32174_MethodInfo;
 bool DefaultComparer_Equals_m32174 (DefaultComparer_t5345 * __this, Guid_t2274  ___x, Guid_t2274  ___y, MethodInfo* method){
	{
		Guid_t2274  L_0 = ___x;
		Object_t * L_1 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		Guid_t2274  L_2 = ___y;
		Object_t * L_3 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_2);
		return ((((Object_t *)L_3) == ((Object_t *)NULL))? 1 : 0);
	}

IL_0012:
	{
		Guid_t2274  L_4 = ___y;
		Object_t * L_5 = Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &L_4);
		NullCheck(Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &(*(&___x))));
		bool L_6 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&Guid_t2274_il2cpp_TypeInfo), &(*(&___x))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer__ctor_m32172_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::.ctor()
MethodInfo DefaultComparer__ctor_m32172_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultComparer__ctor_m32172/* method */
	, &DefaultComparer_t5345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer__ctor_m32172_GenericMethod/* genericMethod */

};
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo DefaultComparer_t5345_DefaultComparer_GetHashCode_m32173_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_GetHashCode_m32173_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::GetHashCode(T)
MethodInfo DefaultComparer_GetHashCode_m32173_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultComparer_GetHashCode_m32173/* method */
	, &DefaultComparer_t5345_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Guid_t2274/* invoker_method */
	, DefaultComparer_t5345_DefaultComparer_GetHashCode_m32173_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_GetHashCode_m32173_GenericMethod/* genericMethod */

};
extern Il2CppType Guid_t2274_0_0_0;
extern Il2CppType Guid_t2274_0_0_0;
static ParameterInfo DefaultComparer_t5345_DefaultComparer_Equals_m32174_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Guid_t2274_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Guid_t2274_Guid_t2274 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_Equals_m32174_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::Equals(T,T)
MethodInfo DefaultComparer_Equals_m32174_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultComparer_Equals_m32174/* method */
	, &DefaultComparer_t5345_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Guid_t2274_Guid_t2274/* invoker_method */
	, DefaultComparer_t5345_DefaultComparer_Equals_m32174_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_Equals_m32174_GenericMethod/* genericMethod */

};
static MethodInfo* DefaultComparer_t5345_MethodInfos[] =
{
	&DefaultComparer__ctor_m32172_MethodInfo,
	&DefaultComparer_GetHashCode_m32173_MethodInfo,
	&DefaultComparer_Equals_m32174_MethodInfo,
	NULL
};
static MethodInfo* DefaultComparer_t5345_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&DefaultComparer_Equals_m32174_MethodInfo,
	&DefaultComparer_GetHashCode_m32173_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32170_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32169_MethodInfo,
	&DefaultComparer_GetHashCode_m32173_MethodInfo,
	&DefaultComparer_Equals_m32174_MethodInfo,
};
static Il2CppInterfaceOffsetPair DefaultComparer_t5345_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t10027_il2cpp_TypeInfo, 4},
	{ &IEqualityComparer_t1363_il2cpp_TypeInfo, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType DefaultComparer_t5345_0_0_0;
extern Il2CppType DefaultComparer_t5345_1_0_0;
struct DefaultComparer_t5345;
extern Il2CppGenericClass DefaultComparer_t5345_GenericClass;
extern TypeInfo EqualityComparer_1_t1866_il2cpp_TypeInfo;
TypeInfo DefaultComparer_t5345_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, DefaultComparer_t5345_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &EqualityComparer_1_t5344_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &EqualityComparer_1_t1866_il2cpp_TypeInfo/* nested_in */
	, &DefaultComparer_t5345_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultComparer_t5345_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultComparer_t5345_il2cpp_TypeInfo/* cast_class */
	, &DefaultComparer_t5345_0_0_0/* byval_arg */
	, &DefaultComparer_t5345_1_0_0/* this_arg */
	, DefaultComparer_t5345_InterfacesOffsets/* interface_offsets */
	, &DefaultComparer_t5345_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultComparer_t5345)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7518_il2cpp_TypeInfo;

// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimization.h"


// T System.Collections.Generic.IEnumerator`1<System.LoaderOptimization>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.LoaderOptimization>
extern MethodInfo IEnumerator_1_get_Current_m53747_MethodInfo;
static PropertyInfo IEnumerator_1_t7518____Current_PropertyInfo = 
{
	&IEnumerator_1_t7518_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53747_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7518_PropertyInfos[] =
{
	&IEnumerator_1_t7518____Current_PropertyInfo,
	NULL
};
extern Il2CppType LoaderOptimization_t2276_0_0_0;
extern void* RuntimeInvoker_LoaderOptimization_t2276 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53747_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.LoaderOptimization>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53747_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7518_il2cpp_TypeInfo/* declaring_type */
	, &LoaderOptimization_t2276_0_0_0/* return_type */
	, RuntimeInvoker_LoaderOptimization_t2276/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53747_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7518_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53747_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7518_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7518_0_0_0;
extern Il2CppType IEnumerator_1_t7518_1_0_0;
struct IEnumerator_1_t7518;
extern Il2CppGenericClass IEnumerator_1_t7518_GenericClass;
TypeInfo IEnumerator_1_t7518_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7518_MethodInfos/* methods */
	, IEnumerator_1_t7518_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7518_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7518_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7518_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7518_0_0_0/* byval_arg */
	, &IEnumerator_1_t7518_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7518_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.LoaderOptimization>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_775.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5346_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.LoaderOptimization>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_775MethodDeclarations.h"

extern TypeInfo LoaderOptimization_t2276_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m32179_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.LoaderOptimization>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.LoaderOptimization>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.LoaderOptimization>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m32175_MethodInfo;
 void InternalEnumerator_1__ctor_m32175 (InternalEnumerator_1_t5346 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.LoaderOptimization>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32176_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32176 (InternalEnumerator_1_t5346 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m32179(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m32179_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.LoaderOptimization>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m32177_MethodInfo;
 void InternalEnumerator_1_Dispose_m32177 (InternalEnumerator_1_t5346 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.LoaderOptimization>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m32178_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m32178 (InternalEnumerator_1_t5346 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.LoaderOptimization>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m32179 (InternalEnumerator_1_t5346 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.LoaderOptimization>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5346____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5346_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5346, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5346____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5346_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5346, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5346_FieldInfos[] =
{
	&InternalEnumerator_1_t5346____array_0_FieldInfo,
	&InternalEnumerator_1_t5346____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5346____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5346_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32176_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5346____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5346_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m32179_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5346_PropertyInfos[] =
{
	&InternalEnumerator_1_t5346____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5346____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5346_InternalEnumerator_1__ctor_m32175_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m32175_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.LoaderOptimization>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m32175_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m32175/* method */
	, &InternalEnumerator_1_t5346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5346_InternalEnumerator_1__ctor_m32175_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m32175_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32176_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.LoaderOptimization>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32176_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32176/* method */
	, &InternalEnumerator_1_t5346_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32176_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m32177_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.LoaderOptimization>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m32177_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m32177/* method */
	, &InternalEnumerator_1_t5346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m32177_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m32178_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.LoaderOptimization>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m32178_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m32178/* method */
	, &InternalEnumerator_1_t5346_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m32178_GenericMethod/* genericMethod */

};
extern Il2CppType LoaderOptimization_t2276_0_0_0;
extern void* RuntimeInvoker_LoaderOptimization_t2276 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m32179_GenericMethod;
// T System.Array/InternalEnumerator`1<System.LoaderOptimization>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m32179_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m32179/* method */
	, &InternalEnumerator_1_t5346_il2cpp_TypeInfo/* declaring_type */
	, &LoaderOptimization_t2276_0_0_0/* return_type */
	, RuntimeInvoker_LoaderOptimization_t2276/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m32179_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5346_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m32175_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32176_MethodInfo,
	&InternalEnumerator_1_Dispose_m32177_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32178_MethodInfo,
	&InternalEnumerator_1_get_Current_m32179_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5346_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32176_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32178_MethodInfo,
	&InternalEnumerator_1_Dispose_m32177_MethodInfo,
	&InternalEnumerator_1_get_Current_m32179_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5346_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7518_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5346_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7518_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5346_0_0_0;
extern Il2CppType InternalEnumerator_1_t5346_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5346_GenericClass;
TypeInfo InternalEnumerator_1_t5346_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5346_MethodInfos/* methods */
	, InternalEnumerator_1_t5346_PropertyInfos/* properties */
	, InternalEnumerator_1_t5346_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5346_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5346_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5346_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5346_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5346_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5346_1_0_0/* this_arg */
	, InternalEnumerator_1_t5346_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5346_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5346)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9684_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.LoaderOptimization>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.LoaderOptimization>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.LoaderOptimization>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.LoaderOptimization>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.LoaderOptimization>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.LoaderOptimization>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.LoaderOptimization>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.LoaderOptimization>
extern MethodInfo ICollection_1_get_Count_m53748_MethodInfo;
static PropertyInfo ICollection_1_t9684____Count_PropertyInfo = 
{
	&ICollection_1_t9684_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53748_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53749_MethodInfo;
static PropertyInfo ICollection_1_t9684____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9684_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53749_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9684_PropertyInfos[] =
{
	&ICollection_1_t9684____Count_PropertyInfo,
	&ICollection_1_t9684____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53748_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.LoaderOptimization>::get_Count()
MethodInfo ICollection_1_get_Count_m53748_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9684_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53748_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53749_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.LoaderOptimization>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53749_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9684_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53749_GenericMethod/* genericMethod */

};
extern Il2CppType LoaderOptimization_t2276_0_0_0;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo ICollection_1_t9684_ICollection_1_Add_m53750_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53750_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.LoaderOptimization>::Add(T)
MethodInfo ICollection_1_Add_m53750_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9684_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9684_ICollection_1_Add_m53750_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53750_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53751_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.LoaderOptimization>::Clear()
MethodInfo ICollection_1_Clear_m53751_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9684_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53751_GenericMethod/* genericMethod */

};
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo ICollection_1_t9684_ICollection_1_Contains_m53752_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53752_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.LoaderOptimization>::Contains(T)
MethodInfo ICollection_1_Contains_m53752_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9684_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9684_ICollection_1_Contains_m53752_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53752_GenericMethod/* genericMethod */

};
extern Il2CppType LoaderOptimizationU5BU5D_t5612_0_0_0;
extern Il2CppType LoaderOptimizationU5BU5D_t5612_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9684_ICollection_1_CopyTo_m53753_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &LoaderOptimizationU5BU5D_t5612_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53753_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.LoaderOptimization>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53753_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9684_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9684_ICollection_1_CopyTo_m53753_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53753_GenericMethod/* genericMethod */

};
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo ICollection_1_t9684_ICollection_1_Remove_m53754_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53754_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.LoaderOptimization>::Remove(T)
MethodInfo ICollection_1_Remove_m53754_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9684_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9684_ICollection_1_Remove_m53754_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53754_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9684_MethodInfos[] =
{
	&ICollection_1_get_Count_m53748_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53749_MethodInfo,
	&ICollection_1_Add_m53750_MethodInfo,
	&ICollection_1_Clear_m53751_MethodInfo,
	&ICollection_1_Contains_m53752_MethodInfo,
	&ICollection_1_CopyTo_m53753_MethodInfo,
	&ICollection_1_Remove_m53754_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9686_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9684_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9686_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9684_0_0_0;
extern Il2CppType ICollection_1_t9684_1_0_0;
struct ICollection_1_t9684;
extern Il2CppGenericClass ICollection_1_t9684_GenericClass;
TypeInfo ICollection_1_t9684_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9684_MethodInfos/* methods */
	, ICollection_1_t9684_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9684_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9684_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9684_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9684_0_0_0/* byval_arg */
	, &ICollection_1_t9684_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9684_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.LoaderOptimization>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.LoaderOptimization>
extern Il2CppType IEnumerator_1_t7518_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53755_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.LoaderOptimization>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53755_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9686_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7518_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53755_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9686_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53755_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9686_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9686_0_0_0;
extern Il2CppType IEnumerable_1_t9686_1_0_0;
struct IEnumerable_1_t9686;
extern Il2CppGenericClass IEnumerable_1_t9686_GenericClass;
TypeInfo IEnumerable_1_t9686_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9686_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9686_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9686_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9686_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9686_0_0_0/* byval_arg */
	, &IEnumerable_1_t9686_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9686_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9685_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.LoaderOptimization>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.LoaderOptimization>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.LoaderOptimization>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.LoaderOptimization>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.LoaderOptimization>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.LoaderOptimization>
extern MethodInfo IList_1_get_Item_m53756_MethodInfo;
extern MethodInfo IList_1_set_Item_m53757_MethodInfo;
static PropertyInfo IList_1_t9685____Item_PropertyInfo = 
{
	&IList_1_t9685_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53756_MethodInfo/* get */
	, &IList_1_set_Item_m53757_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9685_PropertyInfos[] =
{
	&IList_1_t9685____Item_PropertyInfo,
	NULL
};
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo IList_1_t9685_IList_1_IndexOf_m53758_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53758_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.LoaderOptimization>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53758_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9685_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9685_IList_1_IndexOf_m53758_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53758_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo IList_1_t9685_IList_1_Insert_m53759_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53759_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.LoaderOptimization>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53759_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9685_IList_1_Insert_m53759_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53759_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9685_IList_1_RemoveAt_m53760_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53760_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.LoaderOptimization>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53760_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9685_IList_1_RemoveAt_m53760_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53760_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9685_IList_1_get_Item_m53756_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType LoaderOptimization_t2276_0_0_0;
extern void* RuntimeInvoker_LoaderOptimization_t2276_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53756_GenericMethod;
// T System.Collections.Generic.IList`1<System.LoaderOptimization>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53756_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9685_il2cpp_TypeInfo/* declaring_type */
	, &LoaderOptimization_t2276_0_0_0/* return_type */
	, RuntimeInvoker_LoaderOptimization_t2276_Int32_t123/* invoker_method */
	, IList_1_t9685_IList_1_get_Item_m53756_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53756_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo IList_1_t9685_IList_1_set_Item_m53757_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53757_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.LoaderOptimization>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53757_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9685_IList_1_set_Item_m53757_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53757_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9685_MethodInfos[] =
{
	&IList_1_IndexOf_m53758_MethodInfo,
	&IList_1_Insert_m53759_MethodInfo,
	&IList_1_RemoveAt_m53760_MethodInfo,
	&IList_1_get_Item_m53756_MethodInfo,
	&IList_1_set_Item_m53757_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9685_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9684_il2cpp_TypeInfo,
	&IEnumerable_1_t9686_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9685_0_0_0;
extern Il2CppType IList_1_t9685_1_0_0;
struct IList_1_t9685;
extern Il2CppGenericClass IList_1_t9685_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9685_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9685_MethodInfos/* methods */
	, IList_1_t9685_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9685_il2cpp_TypeInfo/* element_class */
	, IList_1_t9685_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9685_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9685_0_0_0/* byval_arg */
	, &IList_1_t9685_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9685_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7520_il2cpp_TypeInfo;

// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.NonSerializedAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.NonSerializedAttribute>
extern MethodInfo IEnumerator_1_get_Current_m53761_MethodInfo;
static PropertyInfo IEnumerator_1_t7520____Current_PropertyInfo = 
{
	&IEnumerator_1_t7520_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53761_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7520_PropertyInfos[] =
{
	&IEnumerator_1_t7520____Current_PropertyInfo,
	NULL
};
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53761_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.NonSerializedAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53761_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7520_il2cpp_TypeInfo/* declaring_type */
	, &NonSerializedAttribute_t2290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53761_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7520_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53761_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7520_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7520_0_0_0;
extern Il2CppType IEnumerator_1_t7520_1_0_0;
struct IEnumerator_1_t7520;
extern Il2CppGenericClass IEnumerator_1_t7520_GenericClass;
TypeInfo IEnumerator_1_t7520_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7520_MethodInfos/* methods */
	, IEnumerator_1_t7520_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7520_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7520_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7520_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7520_0_0_0/* byval_arg */
	, &IEnumerator_1_t7520_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7520_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.NonSerializedAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_776.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5347_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.NonSerializedAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_776MethodDeclarations.h"

extern TypeInfo NonSerializedAttribute_t2290_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m32184_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisNonSerializedAttribute_t2290_m42273_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.NonSerializedAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.NonSerializedAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisNonSerializedAttribute_t2290_m42273(__this, p0, method) (NonSerializedAttribute_t2290 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.NonSerializedAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.NonSerializedAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.NonSerializedAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.NonSerializedAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.NonSerializedAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.NonSerializedAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5347____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5347_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5347, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5347____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5347_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5347, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5347_FieldInfos[] =
{
	&InternalEnumerator_1_t5347____array_0_FieldInfo,
	&InternalEnumerator_1_t5347____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32181_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5347____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5347_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32181_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5347____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5347_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m32184_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5347_PropertyInfos[] =
{
	&InternalEnumerator_1_t5347____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5347____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5347_InternalEnumerator_1__ctor_m32180_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m32180_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.NonSerializedAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m32180_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5347_InternalEnumerator_1__ctor_m32180_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m32180_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32181_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.NonSerializedAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32181_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5347_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32181_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m32182_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.NonSerializedAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m32182_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m32182_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m32183_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.NonSerializedAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m32183_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5347_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m32183_GenericMethod/* genericMethod */

};
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m32184_GenericMethod;
// T System.Array/InternalEnumerator`1<System.NonSerializedAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m32184_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5347_il2cpp_TypeInfo/* declaring_type */
	, &NonSerializedAttribute_t2290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m32184_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5347_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m32180_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32181_MethodInfo,
	&InternalEnumerator_1_Dispose_m32182_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32183_MethodInfo,
	&InternalEnumerator_1_get_Current_m32184_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m32183_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m32182_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5347_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32181_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32183_MethodInfo,
	&InternalEnumerator_1_Dispose_m32182_MethodInfo,
	&InternalEnumerator_1_get_Current_m32184_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5347_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7520_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5347_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7520_il2cpp_TypeInfo, 7},
};
extern TypeInfo NonSerializedAttribute_t2290_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5347_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m32184_MethodInfo/* Method Usage */,
	&NonSerializedAttribute_t2290_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisNonSerializedAttribute_t2290_m42273_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5347_0_0_0;
extern Il2CppType InternalEnumerator_1_t5347_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5347_GenericClass;
TypeInfo InternalEnumerator_1_t5347_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5347_MethodInfos/* methods */
	, InternalEnumerator_1_t5347_PropertyInfos/* properties */
	, InternalEnumerator_1_t5347_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5347_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5347_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5347_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5347_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5347_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5347_1_0_0/* this_arg */
	, InternalEnumerator_1_t5347_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5347_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5347_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5347)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9687_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>
extern MethodInfo ICollection_1_get_Count_m53762_MethodInfo;
static PropertyInfo ICollection_1_t9687____Count_PropertyInfo = 
{
	&ICollection_1_t9687_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53762_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53763_MethodInfo;
static PropertyInfo ICollection_1_t9687____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9687_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53763_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9687_PropertyInfos[] =
{
	&ICollection_1_t9687____Count_PropertyInfo,
	&ICollection_1_t9687____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53762_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m53762_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9687_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53762_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53763_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53763_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9687_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53763_GenericMethod/* genericMethod */

};
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo ICollection_1_t9687_ICollection_1_Add_m53764_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53764_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::Add(T)
MethodInfo ICollection_1_Add_m53764_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9687_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9687_ICollection_1_Add_m53764_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53764_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53765_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::Clear()
MethodInfo ICollection_1_Clear_m53765_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9687_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53765_GenericMethod/* genericMethod */

};
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo ICollection_1_t9687_ICollection_1_Contains_m53766_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53766_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m53766_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9687_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9687_ICollection_1_Contains_m53766_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53766_GenericMethod/* genericMethod */

};
extern Il2CppType NonSerializedAttributeU5BU5D_t5613_0_0_0;
extern Il2CppType NonSerializedAttributeU5BU5D_t5613_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9687_ICollection_1_CopyTo_m53767_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &NonSerializedAttributeU5BU5D_t5613_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53767_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53767_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9687_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9687_ICollection_1_CopyTo_m53767_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53767_GenericMethod/* genericMethod */

};
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo ICollection_1_t9687_ICollection_1_Remove_m53768_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53768_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.NonSerializedAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m53768_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9687_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9687_ICollection_1_Remove_m53768_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53768_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9687_MethodInfos[] =
{
	&ICollection_1_get_Count_m53762_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53763_MethodInfo,
	&ICollection_1_Add_m53764_MethodInfo,
	&ICollection_1_Clear_m53765_MethodInfo,
	&ICollection_1_Contains_m53766_MethodInfo,
	&ICollection_1_CopyTo_m53767_MethodInfo,
	&ICollection_1_Remove_m53768_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9689_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9687_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9689_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9687_0_0_0;
extern Il2CppType ICollection_1_t9687_1_0_0;
struct ICollection_1_t9687;
extern Il2CppGenericClass ICollection_1_t9687_GenericClass;
TypeInfo ICollection_1_t9687_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9687_MethodInfos/* methods */
	, ICollection_1_t9687_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9687_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9687_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9687_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9687_0_0_0/* byval_arg */
	, &ICollection_1_t9687_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9687_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.NonSerializedAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.NonSerializedAttribute>
extern Il2CppType IEnumerator_1_t7520_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53769_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.NonSerializedAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53769_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9689_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7520_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53769_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9689_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53769_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9689_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9689_0_0_0;
extern Il2CppType IEnumerable_1_t9689_1_0_0;
struct IEnumerable_1_t9689;
extern Il2CppGenericClass IEnumerable_1_t9689_GenericClass;
TypeInfo IEnumerable_1_t9689_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9689_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9689_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9689_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9689_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9689_0_0_0/* byval_arg */
	, &IEnumerable_1_t9689_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9689_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9688_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.NonSerializedAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.NonSerializedAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.NonSerializedAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.NonSerializedAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.NonSerializedAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.NonSerializedAttribute>
extern MethodInfo IList_1_get_Item_m53770_MethodInfo;
extern MethodInfo IList_1_set_Item_m53771_MethodInfo;
static PropertyInfo IList_1_t9688____Item_PropertyInfo = 
{
	&IList_1_t9688_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53770_MethodInfo/* get */
	, &IList_1_set_Item_m53771_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9688_PropertyInfos[] =
{
	&IList_1_t9688____Item_PropertyInfo,
	NULL
};
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo IList_1_t9688_IList_1_IndexOf_m53772_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53772_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.NonSerializedAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53772_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9688_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9688_IList_1_IndexOf_m53772_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53772_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo IList_1_t9688_IList_1_Insert_m53773_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53773_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.NonSerializedAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53773_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9688_IList_1_Insert_m53773_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53773_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9688_IList_1_RemoveAt_m53774_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53774_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.NonSerializedAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53774_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9688_IList_1_RemoveAt_m53774_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53774_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9688_IList_1_get_Item_m53770_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53770_GenericMethod;
// T System.Collections.Generic.IList`1<System.NonSerializedAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53770_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9688_il2cpp_TypeInfo/* declaring_type */
	, &NonSerializedAttribute_t2290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9688_IList_1_get_Item_m53770_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53770_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo IList_1_t9688_IList_1_set_Item_m53771_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53771_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.NonSerializedAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53771_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9688_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9688_IList_1_set_Item_m53771_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53771_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9688_MethodInfos[] =
{
	&IList_1_IndexOf_m53772_MethodInfo,
	&IList_1_Insert_m53773_MethodInfo,
	&IList_1_RemoveAt_m53774_MethodInfo,
	&IList_1_get_Item_m53770_MethodInfo,
	&IList_1_set_Item_m53771_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9688_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9687_il2cpp_TypeInfo,
	&IEnumerable_1_t9689_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9688_0_0_0;
extern Il2CppType IList_1_t9688_1_0_0;
struct IList_1_t9688;
extern Il2CppGenericClass IList_1_t9688_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9688_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9688_MethodInfos/* methods */
	, IList_1_t9688_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9688_il2cpp_TypeInfo/* element_class */
	, IList_1_t9688_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9688_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9688_0_0_0/* byval_arg */
	, &IList_1_t9688_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9688_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7522_il2cpp_TypeInfo;

// System.PlatformID
#include "mscorlib_System_PlatformID.h"


// T System.Collections.Generic.IEnumerator`1<System.PlatformID>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.PlatformID>
extern MethodInfo IEnumerator_1_get_Current_m53775_MethodInfo;
static PropertyInfo IEnumerator_1_t7522____Current_PropertyInfo = 
{
	&IEnumerator_1_t7522_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53775_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7522_PropertyInfos[] =
{
	&IEnumerator_1_t7522____Current_PropertyInfo,
	NULL
};
extern Il2CppType PlatformID_t2295_0_0_0;
extern void* RuntimeInvoker_PlatformID_t2295 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53775_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.PlatformID>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53775_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7522_il2cpp_TypeInfo/* declaring_type */
	, &PlatformID_t2295_0_0_0/* return_type */
	, RuntimeInvoker_PlatformID_t2295/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53775_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7522_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53775_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7522_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7522_0_0_0;
extern Il2CppType IEnumerator_1_t7522_1_0_0;
struct IEnumerator_1_t7522;
extern Il2CppGenericClass IEnumerator_1_t7522_GenericClass;
TypeInfo IEnumerator_1_t7522_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7522_MethodInfos/* methods */
	, IEnumerator_1_t7522_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7522_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7522_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7522_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7522_0_0_0/* byval_arg */
	, &IEnumerator_1_t7522_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7522_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.PlatformID>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_777.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5348_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.PlatformID>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_777MethodDeclarations.h"

extern TypeInfo PlatformID_t2295_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m32189_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPlatformID_t2295_m42284_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.PlatformID>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.PlatformID>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisPlatformID_t2295_m42284 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.PlatformID>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m32185_MethodInfo;
 void InternalEnumerator_1__ctor_m32185 (InternalEnumerator_1_t5348 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.PlatformID>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32186_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32186 (InternalEnumerator_1_t5348 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m32189(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m32189_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.PlatformID>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m32187_MethodInfo;
 void InternalEnumerator_1_Dispose_m32187 (InternalEnumerator_1_t5348 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.PlatformID>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m32188_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m32188 (InternalEnumerator_1_t5348 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.PlatformID>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m32189 (InternalEnumerator_1_t5348 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisPlatformID_t2295_m42284(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisPlatformID_t2295_m42284_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.PlatformID>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5348____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5348_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5348, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5348____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5348_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5348, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5348_FieldInfos[] =
{
	&InternalEnumerator_1_t5348____array_0_FieldInfo,
	&InternalEnumerator_1_t5348____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5348____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5348_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32186_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5348____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5348_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m32189_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5348_PropertyInfos[] =
{
	&InternalEnumerator_1_t5348____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5348____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5348_InternalEnumerator_1__ctor_m32185_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m32185_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.PlatformID>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m32185_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m32185/* method */
	, &InternalEnumerator_1_t5348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5348_InternalEnumerator_1__ctor_m32185_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m32185_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32186_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.PlatformID>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32186_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32186/* method */
	, &InternalEnumerator_1_t5348_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32186_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m32187_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.PlatformID>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m32187_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m32187/* method */
	, &InternalEnumerator_1_t5348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m32187_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m32188_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.PlatformID>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m32188_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m32188/* method */
	, &InternalEnumerator_1_t5348_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m32188_GenericMethod/* genericMethod */

};
extern Il2CppType PlatformID_t2295_0_0_0;
extern void* RuntimeInvoker_PlatformID_t2295 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m32189_GenericMethod;
// T System.Array/InternalEnumerator`1<System.PlatformID>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m32189_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m32189/* method */
	, &InternalEnumerator_1_t5348_il2cpp_TypeInfo/* declaring_type */
	, &PlatformID_t2295_0_0_0/* return_type */
	, RuntimeInvoker_PlatformID_t2295/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m32189_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5348_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m32185_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32186_MethodInfo,
	&InternalEnumerator_1_Dispose_m32187_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32188_MethodInfo,
	&InternalEnumerator_1_get_Current_m32189_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5348_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32186_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32188_MethodInfo,
	&InternalEnumerator_1_Dispose_m32187_MethodInfo,
	&InternalEnumerator_1_get_Current_m32189_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5348_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7522_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5348_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7522_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5348_0_0_0;
extern Il2CppType InternalEnumerator_1_t5348_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5348_GenericClass;
TypeInfo InternalEnumerator_1_t5348_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5348_MethodInfos/* methods */
	, InternalEnumerator_1_t5348_PropertyInfos/* properties */
	, InternalEnumerator_1_t5348_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5348_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5348_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5348_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5348_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5348_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5348_1_0_0/* this_arg */
	, InternalEnumerator_1_t5348_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5348_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5348)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9690_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.PlatformID>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.PlatformID>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.PlatformID>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.PlatformID>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.PlatformID>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.PlatformID>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.PlatformID>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.PlatformID>
extern MethodInfo ICollection_1_get_Count_m53776_MethodInfo;
static PropertyInfo ICollection_1_t9690____Count_PropertyInfo = 
{
	&ICollection_1_t9690_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53776_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53777_MethodInfo;
static PropertyInfo ICollection_1_t9690____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9690_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53777_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9690_PropertyInfos[] =
{
	&ICollection_1_t9690____Count_PropertyInfo,
	&ICollection_1_t9690____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53776_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.PlatformID>::get_Count()
MethodInfo ICollection_1_get_Count_m53776_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9690_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53776_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53777_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.PlatformID>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53777_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9690_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53777_GenericMethod/* genericMethod */

};
extern Il2CppType PlatformID_t2295_0_0_0;
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo ICollection_1_t9690_ICollection_1_Add_m53778_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53778_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.PlatformID>::Add(T)
MethodInfo ICollection_1_Add_m53778_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9690_ICollection_1_Add_m53778_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53778_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53779_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.PlatformID>::Clear()
MethodInfo ICollection_1_Clear_m53779_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53779_GenericMethod/* genericMethod */

};
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo ICollection_1_t9690_ICollection_1_Contains_m53780_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53780_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.PlatformID>::Contains(T)
MethodInfo ICollection_1_Contains_m53780_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9690_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9690_ICollection_1_Contains_m53780_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53780_GenericMethod/* genericMethod */

};
extern Il2CppType PlatformIDU5BU5D_t5614_0_0_0;
extern Il2CppType PlatformIDU5BU5D_t5614_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9690_ICollection_1_CopyTo_m53781_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PlatformIDU5BU5D_t5614_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53781_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.PlatformID>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53781_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9690_ICollection_1_CopyTo_m53781_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53781_GenericMethod/* genericMethod */

};
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo ICollection_1_t9690_ICollection_1_Remove_m53782_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53782_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.PlatformID>::Remove(T)
MethodInfo ICollection_1_Remove_m53782_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9690_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9690_ICollection_1_Remove_m53782_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53782_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9690_MethodInfos[] =
{
	&ICollection_1_get_Count_m53776_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53777_MethodInfo,
	&ICollection_1_Add_m53778_MethodInfo,
	&ICollection_1_Clear_m53779_MethodInfo,
	&ICollection_1_Contains_m53780_MethodInfo,
	&ICollection_1_CopyTo_m53781_MethodInfo,
	&ICollection_1_Remove_m53782_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9692_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9690_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9692_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9690_0_0_0;
extern Il2CppType ICollection_1_t9690_1_0_0;
struct ICollection_1_t9690;
extern Il2CppGenericClass ICollection_1_t9690_GenericClass;
TypeInfo ICollection_1_t9690_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9690_MethodInfos/* methods */
	, ICollection_1_t9690_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9690_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9690_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9690_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9690_0_0_0/* byval_arg */
	, &ICollection_1_t9690_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9690_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.PlatformID>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.PlatformID>
extern Il2CppType IEnumerator_1_t7522_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53783_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.PlatformID>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53783_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9692_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7522_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53783_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9692_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53783_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9692_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9692_0_0_0;
extern Il2CppType IEnumerable_1_t9692_1_0_0;
struct IEnumerable_1_t9692;
extern Il2CppGenericClass IEnumerable_1_t9692_GenericClass;
TypeInfo IEnumerable_1_t9692_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9692_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9692_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9692_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9692_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9692_0_0_0/* byval_arg */
	, &IEnumerable_1_t9692_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9692_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9691_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.PlatformID>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.PlatformID>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.PlatformID>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.PlatformID>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.PlatformID>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.PlatformID>
extern MethodInfo IList_1_get_Item_m53784_MethodInfo;
extern MethodInfo IList_1_set_Item_m53785_MethodInfo;
static PropertyInfo IList_1_t9691____Item_PropertyInfo = 
{
	&IList_1_t9691_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53784_MethodInfo/* get */
	, &IList_1_set_Item_m53785_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9691_PropertyInfos[] =
{
	&IList_1_t9691____Item_PropertyInfo,
	NULL
};
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo IList_1_t9691_IList_1_IndexOf_m53786_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53786_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.PlatformID>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53786_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9691_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9691_IList_1_IndexOf_m53786_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53786_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo IList_1_t9691_IList_1_Insert_m53787_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53787_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.PlatformID>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53787_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9691_IList_1_Insert_m53787_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53787_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9691_IList_1_RemoveAt_m53788_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53788_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.PlatformID>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53788_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9691_IList_1_RemoveAt_m53788_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53788_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9691_IList_1_get_Item_m53784_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType PlatformID_t2295_0_0_0;
extern void* RuntimeInvoker_PlatformID_t2295_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53784_GenericMethod;
// T System.Collections.Generic.IList`1<System.PlatformID>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53784_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9691_il2cpp_TypeInfo/* declaring_type */
	, &PlatformID_t2295_0_0_0/* return_type */
	, RuntimeInvoker_PlatformID_t2295_Int32_t123/* invoker_method */
	, IList_1_t9691_IList_1_get_Item_m53784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53784_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo IList_1_t9691_IList_1_set_Item_m53785_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53785_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.PlatformID>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53785_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9691_IList_1_set_Item_m53785_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53785_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9691_MethodInfos[] =
{
	&IList_1_IndexOf_m53786_MethodInfo,
	&IList_1_Insert_m53787_MethodInfo,
	&IList_1_RemoveAt_m53788_MethodInfo,
	&IList_1_get_Item_m53784_MethodInfo,
	&IList_1_set_Item_m53785_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9691_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9690_il2cpp_TypeInfo,
	&IEnumerable_1_t9692_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9691_0_0_0;
extern Il2CppType IList_1_t9691_1_0_0;
struct IList_1_t9691;
extern Il2CppGenericClass IList_1_t9691_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9691_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9691_MethodInfos/* methods */
	, IList_1_t9691_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9691_il2cpp_TypeInfo/* element_class */
	, IList_1_t9691_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9691_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9691_0_0_0/* byval_arg */
	, &IList_1_t9691_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9691_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7524_il2cpp_TypeInfo;

// System.StringComparison
#include "mscorlib_System_StringComparison.h"


// T System.Collections.Generic.IEnumerator`1<System.StringComparison>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.StringComparison>
extern MethodInfo IEnumerator_1_get_Current_m53789_MethodInfo;
static PropertyInfo IEnumerator_1_t7524____Current_PropertyInfo = 
{
	&IEnumerator_1_t7524_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53789_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7524_PropertyInfos[] =
{
	&IEnumerator_1_t7524____Current_PropertyInfo,
	NULL
};
extern Il2CppType StringComparison_t2300_0_0_0;
extern void* RuntimeInvoker_StringComparison_t2300 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53789_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.StringComparison>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53789_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7524_il2cpp_TypeInfo/* declaring_type */
	, &StringComparison_t2300_0_0_0/* return_type */
	, RuntimeInvoker_StringComparison_t2300/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53789_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7524_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53789_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7524_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7524_0_0_0;
extern Il2CppType IEnumerator_1_t7524_1_0_0;
struct IEnumerator_1_t7524;
extern Il2CppGenericClass IEnumerator_1_t7524_GenericClass;
TypeInfo IEnumerator_1_t7524_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7524_MethodInfos/* methods */
	, IEnumerator_1_t7524_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7524_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7524_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7524_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7524_0_0_0/* byval_arg */
	, &IEnumerator_1_t7524_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7524_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.StringComparison>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_778.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5349_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.StringComparison>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_778MethodDeclarations.h"

extern TypeInfo StringComparison_t2300_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m32194_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisStringComparison_t2300_m42295_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.StringComparison>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.StringComparison>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisStringComparison_t2300_m42295 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.StringComparison>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m32190_MethodInfo;
 void InternalEnumerator_1__ctor_m32190 (InternalEnumerator_1_t5349 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.StringComparison>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32191_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32191 (InternalEnumerator_1_t5349 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m32194(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m32194_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.StringComparison>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m32192_MethodInfo;
 void InternalEnumerator_1_Dispose_m32192 (InternalEnumerator_1_t5349 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.StringComparison>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m32193_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m32193 (InternalEnumerator_1_t5349 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.StringComparison>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m32194 (InternalEnumerator_1_t5349 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisStringComparison_t2300_m42295(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisStringComparison_t2300_m42295_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.StringComparison>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5349____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5349_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5349, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5349____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5349_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5349, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5349_FieldInfos[] =
{
	&InternalEnumerator_1_t5349____array_0_FieldInfo,
	&InternalEnumerator_1_t5349____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5349____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5349_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32191_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5349____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5349_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m32194_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5349_PropertyInfos[] =
{
	&InternalEnumerator_1_t5349____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5349____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5349_InternalEnumerator_1__ctor_m32190_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m32190_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.StringComparison>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m32190_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m32190/* method */
	, &InternalEnumerator_1_t5349_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5349_InternalEnumerator_1__ctor_m32190_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m32190_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32191_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.StringComparison>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32191_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32191/* method */
	, &InternalEnumerator_1_t5349_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32191_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m32192_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.StringComparison>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m32192_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m32192/* method */
	, &InternalEnumerator_1_t5349_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m32192_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m32193_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.StringComparison>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m32193_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m32193/* method */
	, &InternalEnumerator_1_t5349_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m32193_GenericMethod/* genericMethod */

};
extern Il2CppType StringComparison_t2300_0_0_0;
extern void* RuntimeInvoker_StringComparison_t2300 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m32194_GenericMethod;
// T System.Array/InternalEnumerator`1<System.StringComparison>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m32194_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m32194/* method */
	, &InternalEnumerator_1_t5349_il2cpp_TypeInfo/* declaring_type */
	, &StringComparison_t2300_0_0_0/* return_type */
	, RuntimeInvoker_StringComparison_t2300/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m32194_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5349_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m32190_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32191_MethodInfo,
	&InternalEnumerator_1_Dispose_m32192_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32193_MethodInfo,
	&InternalEnumerator_1_get_Current_m32194_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5349_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32191_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32193_MethodInfo,
	&InternalEnumerator_1_Dispose_m32192_MethodInfo,
	&InternalEnumerator_1_get_Current_m32194_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5349_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7524_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5349_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7524_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5349_0_0_0;
extern Il2CppType InternalEnumerator_1_t5349_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5349_GenericClass;
TypeInfo InternalEnumerator_1_t5349_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5349_MethodInfos/* methods */
	, InternalEnumerator_1_t5349_PropertyInfos/* properties */
	, InternalEnumerator_1_t5349_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5349_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5349_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5349_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5349_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5349_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5349_1_0_0/* this_arg */
	, InternalEnumerator_1_t5349_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5349_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5349)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9693_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.StringComparison>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.StringComparison>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.StringComparison>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.StringComparison>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.StringComparison>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.StringComparison>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.StringComparison>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.StringComparison>
extern MethodInfo ICollection_1_get_Count_m53790_MethodInfo;
static PropertyInfo ICollection_1_t9693____Count_PropertyInfo = 
{
	&ICollection_1_t9693_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53790_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53791_MethodInfo;
static PropertyInfo ICollection_1_t9693____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9693_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53791_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9693_PropertyInfos[] =
{
	&ICollection_1_t9693____Count_PropertyInfo,
	&ICollection_1_t9693____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53790_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.StringComparison>::get_Count()
MethodInfo ICollection_1_get_Count_m53790_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9693_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53790_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53791_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.StringComparison>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53791_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9693_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53791_GenericMethod/* genericMethod */

};
extern Il2CppType StringComparison_t2300_0_0_0;
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo ICollection_1_t9693_ICollection_1_Add_m53792_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53792_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.StringComparison>::Add(T)
MethodInfo ICollection_1_Add_m53792_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9693_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9693_ICollection_1_Add_m53792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53792_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53793_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.StringComparison>::Clear()
MethodInfo ICollection_1_Clear_m53793_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9693_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53793_GenericMethod/* genericMethod */

};
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo ICollection_1_t9693_ICollection_1_Contains_m53794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53794_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.StringComparison>::Contains(T)
MethodInfo ICollection_1_Contains_m53794_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9693_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9693_ICollection_1_Contains_m53794_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53794_GenericMethod/* genericMethod */

};
extern Il2CppType StringComparisonU5BU5D_t5615_0_0_0;
extern Il2CppType StringComparisonU5BU5D_t5615_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9693_ICollection_1_CopyTo_m53795_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &StringComparisonU5BU5D_t5615_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53795_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.StringComparison>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53795_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9693_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9693_ICollection_1_CopyTo_m53795_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53795_GenericMethod/* genericMethod */

};
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo ICollection_1_t9693_ICollection_1_Remove_m53796_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53796_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.StringComparison>::Remove(T)
MethodInfo ICollection_1_Remove_m53796_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9693_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9693_ICollection_1_Remove_m53796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53796_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9693_MethodInfos[] =
{
	&ICollection_1_get_Count_m53790_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53791_MethodInfo,
	&ICollection_1_Add_m53792_MethodInfo,
	&ICollection_1_Clear_m53793_MethodInfo,
	&ICollection_1_Contains_m53794_MethodInfo,
	&ICollection_1_CopyTo_m53795_MethodInfo,
	&ICollection_1_Remove_m53796_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9695_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9693_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9695_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9693_0_0_0;
extern Il2CppType ICollection_1_t9693_1_0_0;
struct ICollection_1_t9693;
extern Il2CppGenericClass ICollection_1_t9693_GenericClass;
TypeInfo ICollection_1_t9693_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9693_MethodInfos/* methods */
	, ICollection_1_t9693_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9693_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9693_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9693_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9693_0_0_0/* byval_arg */
	, &ICollection_1_t9693_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9693_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.StringComparison>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.StringComparison>
extern Il2CppType IEnumerator_1_t7524_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53797_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.StringComparison>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53797_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9695_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7524_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53797_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9695_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53797_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9695_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9695_0_0_0;
extern Il2CppType IEnumerable_1_t9695_1_0_0;
struct IEnumerable_1_t9695;
extern Il2CppGenericClass IEnumerable_1_t9695_GenericClass;
TypeInfo IEnumerable_1_t9695_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9695_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9695_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9695_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9695_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9695_0_0_0/* byval_arg */
	, &IEnumerable_1_t9695_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9695_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9694_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.StringComparison>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.StringComparison>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.StringComparison>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.StringComparison>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.StringComparison>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.StringComparison>
extern MethodInfo IList_1_get_Item_m53798_MethodInfo;
extern MethodInfo IList_1_set_Item_m53799_MethodInfo;
static PropertyInfo IList_1_t9694____Item_PropertyInfo = 
{
	&IList_1_t9694_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53798_MethodInfo/* get */
	, &IList_1_set_Item_m53799_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9694_PropertyInfos[] =
{
	&IList_1_t9694____Item_PropertyInfo,
	NULL
};
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo IList_1_t9694_IList_1_IndexOf_m53800_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53800_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.StringComparison>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53800_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9694_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9694_IList_1_IndexOf_m53800_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53800_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo IList_1_t9694_IList_1_Insert_m53801_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53801_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.StringComparison>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53801_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9694_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9694_IList_1_Insert_m53801_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53801_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9694_IList_1_RemoveAt_m53802_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53802_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.StringComparison>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53802_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9694_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9694_IList_1_RemoveAt_m53802_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53802_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9694_IList_1_get_Item_m53798_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType StringComparison_t2300_0_0_0;
extern void* RuntimeInvoker_StringComparison_t2300_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53798_GenericMethod;
// T System.Collections.Generic.IList`1<System.StringComparison>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53798_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9694_il2cpp_TypeInfo/* declaring_type */
	, &StringComparison_t2300_0_0_0/* return_type */
	, RuntimeInvoker_StringComparison_t2300_Int32_t123/* invoker_method */
	, IList_1_t9694_IList_1_get_Item_m53798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53798_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo IList_1_t9694_IList_1_set_Item_m53799_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53799_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.StringComparison>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53799_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9694_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9694_IList_1_set_Item_m53799_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53799_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9694_MethodInfos[] =
{
	&IList_1_IndexOf_m53800_MethodInfo,
	&IList_1_Insert_m53801_MethodInfo,
	&IList_1_RemoveAt_m53802_MethodInfo,
	&IList_1_get_Item_m53798_MethodInfo,
	&IList_1_set_Item_m53799_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9694_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9693_il2cpp_TypeInfo,
	&IEnumerable_1_t9695_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9694_0_0_0;
extern Il2CppType IList_1_t9694_1_0_0;
struct IList_1_t9694;
extern Il2CppGenericClass IList_1_t9694_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9694_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9694_MethodInfos/* methods */
	, IList_1_t9694_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9694_il2cpp_TypeInfo/* element_class */
	, IList_1_t9694_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9694_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9694_0_0_0/* byval_arg */
	, &IList_1_t9694_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9694_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7526_il2cpp_TypeInfo;

// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptions.h"


// T System.Collections.Generic.IEnumerator`1<System.StringSplitOptions>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.StringSplitOptions>
extern MethodInfo IEnumerator_1_get_Current_m53803_MethodInfo;
static PropertyInfo IEnumerator_1_t7526____Current_PropertyInfo = 
{
	&IEnumerator_1_t7526_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53803_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7526_PropertyInfos[] =
{
	&IEnumerator_1_t7526____Current_PropertyInfo,
	NULL
};
extern Il2CppType StringSplitOptions_t2301_0_0_0;
extern void* RuntimeInvoker_StringSplitOptions_t2301 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53803_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.StringSplitOptions>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53803_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7526_il2cpp_TypeInfo/* declaring_type */
	, &StringSplitOptions_t2301_0_0_0/* return_type */
	, RuntimeInvoker_StringSplitOptions_t2301/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53803_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7526_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53803_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7526_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7526_0_0_0;
extern Il2CppType IEnumerator_1_t7526_1_0_0;
struct IEnumerator_1_t7526;
extern Il2CppGenericClass IEnumerator_1_t7526_GenericClass;
TypeInfo IEnumerator_1_t7526_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7526_MethodInfos/* methods */
	, IEnumerator_1_t7526_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7526_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7526_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7526_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7526_0_0_0/* byval_arg */
	, &IEnumerator_1_t7526_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7526_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.StringSplitOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_779.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5350_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.StringSplitOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_779MethodDeclarations.h"

extern TypeInfo StringSplitOptions_t2301_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m32199_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.StringSplitOptions>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.StringSplitOptions>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.StringSplitOptions>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m32195_MethodInfo;
 void InternalEnumerator_1__ctor_m32195 (InternalEnumerator_1_t5350 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.StringSplitOptions>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32196_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32196 (InternalEnumerator_1_t5350 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m32199(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m32199_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.StringSplitOptions>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m32197_MethodInfo;
 void InternalEnumerator_1_Dispose_m32197 (InternalEnumerator_1_t5350 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.StringSplitOptions>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m32198_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m32198 (InternalEnumerator_1_t5350 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.StringSplitOptions>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m32199 (InternalEnumerator_1_t5350 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.StringSplitOptions>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5350____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5350_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5350, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5350____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5350_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5350, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5350_FieldInfos[] =
{
	&InternalEnumerator_1_t5350____array_0_FieldInfo,
	&InternalEnumerator_1_t5350____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5350____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5350_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32196_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5350____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5350_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m32199_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5350_PropertyInfos[] =
{
	&InternalEnumerator_1_t5350____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5350____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5350_InternalEnumerator_1__ctor_m32195_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m32195_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.StringSplitOptions>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m32195_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m32195/* method */
	, &InternalEnumerator_1_t5350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5350_InternalEnumerator_1__ctor_m32195_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m32195_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32196_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.StringSplitOptions>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32196_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32196/* method */
	, &InternalEnumerator_1_t5350_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32196_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m32197_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.StringSplitOptions>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m32197_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m32197/* method */
	, &InternalEnumerator_1_t5350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m32197_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m32198_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.StringSplitOptions>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m32198_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m32198/* method */
	, &InternalEnumerator_1_t5350_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m32198_GenericMethod/* genericMethod */

};
extern Il2CppType StringSplitOptions_t2301_0_0_0;
extern void* RuntimeInvoker_StringSplitOptions_t2301 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m32199_GenericMethod;
// T System.Array/InternalEnumerator`1<System.StringSplitOptions>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m32199_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m32199/* method */
	, &InternalEnumerator_1_t5350_il2cpp_TypeInfo/* declaring_type */
	, &StringSplitOptions_t2301_0_0_0/* return_type */
	, RuntimeInvoker_StringSplitOptions_t2301/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m32199_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5350_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m32195_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32196_MethodInfo,
	&InternalEnumerator_1_Dispose_m32197_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32198_MethodInfo,
	&InternalEnumerator_1_get_Current_m32199_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5350_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32196_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32198_MethodInfo,
	&InternalEnumerator_1_Dispose_m32197_MethodInfo,
	&InternalEnumerator_1_get_Current_m32199_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5350_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7526_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5350_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7526_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5350_0_0_0;
extern Il2CppType InternalEnumerator_1_t5350_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5350_GenericClass;
TypeInfo InternalEnumerator_1_t5350_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5350_MethodInfos/* methods */
	, InternalEnumerator_1_t5350_PropertyInfos/* properties */
	, InternalEnumerator_1_t5350_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5350_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5350_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5350_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5350_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5350_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5350_1_0_0/* this_arg */
	, InternalEnumerator_1_t5350_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5350_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5350)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9696_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.StringSplitOptions>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.StringSplitOptions>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.StringSplitOptions>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.StringSplitOptions>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.StringSplitOptions>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.StringSplitOptions>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.StringSplitOptions>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.StringSplitOptions>
extern MethodInfo ICollection_1_get_Count_m53804_MethodInfo;
static PropertyInfo ICollection_1_t9696____Count_PropertyInfo = 
{
	&ICollection_1_t9696_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53804_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53805_MethodInfo;
static PropertyInfo ICollection_1_t9696____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9696_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53805_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9696_PropertyInfos[] =
{
	&ICollection_1_t9696____Count_PropertyInfo,
	&ICollection_1_t9696____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53804_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.StringSplitOptions>::get_Count()
MethodInfo ICollection_1_get_Count_m53804_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9696_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53804_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53805_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.StringSplitOptions>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53805_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9696_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53805_GenericMethod/* genericMethod */

};
extern Il2CppType StringSplitOptions_t2301_0_0_0;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo ICollection_1_t9696_ICollection_1_Add_m53806_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53806_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.StringSplitOptions>::Add(T)
MethodInfo ICollection_1_Add_m53806_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9696_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9696_ICollection_1_Add_m53806_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53806_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53807_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.StringSplitOptions>::Clear()
MethodInfo ICollection_1_Clear_m53807_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9696_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53807_GenericMethod/* genericMethod */

};
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo ICollection_1_t9696_ICollection_1_Contains_m53808_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53808_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.StringSplitOptions>::Contains(T)
MethodInfo ICollection_1_Contains_m53808_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9696_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9696_ICollection_1_Contains_m53808_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53808_GenericMethod/* genericMethod */

};
extern Il2CppType StringSplitOptionsU5BU5D_t5616_0_0_0;
extern Il2CppType StringSplitOptionsU5BU5D_t5616_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9696_ICollection_1_CopyTo_m53809_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &StringSplitOptionsU5BU5D_t5616_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53809_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.StringSplitOptions>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53809_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9696_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9696_ICollection_1_CopyTo_m53809_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53809_GenericMethod/* genericMethod */

};
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo ICollection_1_t9696_ICollection_1_Remove_m53810_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53810_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.StringSplitOptions>::Remove(T)
MethodInfo ICollection_1_Remove_m53810_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9696_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9696_ICollection_1_Remove_m53810_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53810_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9696_MethodInfos[] =
{
	&ICollection_1_get_Count_m53804_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53805_MethodInfo,
	&ICollection_1_Add_m53806_MethodInfo,
	&ICollection_1_Clear_m53807_MethodInfo,
	&ICollection_1_Contains_m53808_MethodInfo,
	&ICollection_1_CopyTo_m53809_MethodInfo,
	&ICollection_1_Remove_m53810_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9698_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9696_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9698_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9696_0_0_0;
extern Il2CppType ICollection_1_t9696_1_0_0;
struct ICollection_1_t9696;
extern Il2CppGenericClass ICollection_1_t9696_GenericClass;
TypeInfo ICollection_1_t9696_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9696_MethodInfos/* methods */
	, ICollection_1_t9696_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9696_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9696_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9696_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9696_0_0_0/* byval_arg */
	, &ICollection_1_t9696_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9696_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.StringSplitOptions>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.StringSplitOptions>
extern Il2CppType IEnumerator_1_t7526_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53811_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.StringSplitOptions>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53811_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9698_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7526_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53811_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9698_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53811_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9698_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9698_0_0_0;
extern Il2CppType IEnumerable_1_t9698_1_0_0;
struct IEnumerable_1_t9698;
extern Il2CppGenericClass IEnumerable_1_t9698_GenericClass;
TypeInfo IEnumerable_1_t9698_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9698_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9698_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9698_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9698_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9698_0_0_0/* byval_arg */
	, &IEnumerable_1_t9698_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9698_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9697_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.StringSplitOptions>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.StringSplitOptions>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.StringSplitOptions>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.StringSplitOptions>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.StringSplitOptions>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.StringSplitOptions>
extern MethodInfo IList_1_get_Item_m53812_MethodInfo;
extern MethodInfo IList_1_set_Item_m53813_MethodInfo;
static PropertyInfo IList_1_t9697____Item_PropertyInfo = 
{
	&IList_1_t9697_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53812_MethodInfo/* get */
	, &IList_1_set_Item_m53813_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9697_PropertyInfos[] =
{
	&IList_1_t9697____Item_PropertyInfo,
	NULL
};
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo IList_1_t9697_IList_1_IndexOf_m53814_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53814_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.StringSplitOptions>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53814_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9697_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9697_IList_1_IndexOf_m53814_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53814_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo IList_1_t9697_IList_1_Insert_m53815_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53815_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.StringSplitOptions>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53815_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9697_IList_1_Insert_m53815_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53815_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9697_IList_1_RemoveAt_m53816_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53816_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.StringSplitOptions>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53816_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9697_IList_1_RemoveAt_m53816_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53816_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9697_IList_1_get_Item_m53812_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType StringSplitOptions_t2301_0_0_0;
extern void* RuntimeInvoker_StringSplitOptions_t2301_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53812_GenericMethod;
// T System.Collections.Generic.IList`1<System.StringSplitOptions>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53812_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9697_il2cpp_TypeInfo/* declaring_type */
	, &StringSplitOptions_t2301_0_0_0/* return_type */
	, RuntimeInvoker_StringSplitOptions_t2301_Int32_t123/* invoker_method */
	, IList_1_t9697_IList_1_get_Item_m53812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53812_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo IList_1_t9697_IList_1_set_Item_m53813_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53813_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.StringSplitOptions>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53813_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9697_IList_1_set_Item_m53813_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53813_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9697_MethodInfos[] =
{
	&IList_1_IndexOf_m53814_MethodInfo,
	&IList_1_Insert_m53815_MethodInfo,
	&IList_1_RemoveAt_m53816_MethodInfo,
	&IList_1_get_Item_m53812_MethodInfo,
	&IList_1_set_Item_m53813_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9697_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9696_il2cpp_TypeInfo,
	&IEnumerable_1_t9698_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9697_0_0_0;
extern Il2CppType IList_1_t9697_1_0_0;
struct IList_1_t9697;
extern Il2CppGenericClass IList_1_t9697_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9697_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9697_MethodInfos/* methods */
	, IList_1_t9697_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9697_il2cpp_TypeInfo/* element_class */
	, IList_1_t9697_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9697_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9697_0_0_0/* byval_arg */
	, &IList_1_t9697_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9697_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7528_il2cpp_TypeInfo;

// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.ThreadStaticAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ThreadStaticAttribute>
extern MethodInfo IEnumerator_1_get_Current_m53817_MethodInfo;
static PropertyInfo IEnumerator_1_t7528____Current_PropertyInfo = 
{
	&IEnumerator_1_t7528_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53817_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7528_PropertyInfos[] =
{
	&IEnumerator_1_t7528____Current_PropertyInfo,
	NULL
};
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53817_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.ThreadStaticAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53817_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7528_il2cpp_TypeInfo/* declaring_type */
	, &ThreadStaticAttribute_t2302_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53817_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7528_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53817_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7528_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7528_0_0_0;
extern Il2CppType IEnumerator_1_t7528_1_0_0;
struct IEnumerator_1_t7528;
extern Il2CppGenericClass IEnumerator_1_t7528_GenericClass;
TypeInfo IEnumerator_1_t7528_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7528_MethodInfos/* methods */
	, IEnumerator_1_t7528_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7528_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7528_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7528_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7528_0_0_0/* byval_arg */
	, &IEnumerator_1_t7528_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7528_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_780.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5351_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_780MethodDeclarations.h"

extern TypeInfo ThreadStaticAttribute_t2302_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m32204_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisThreadStaticAttribute_t2302_m42317_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.ThreadStaticAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.ThreadStaticAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisThreadStaticAttribute_t2302_m42317(__this, p0, method) (ThreadStaticAttribute_t2302 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5351____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5351_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5351, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5351____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5351_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5351, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5351_FieldInfos[] =
{
	&InternalEnumerator_1_t5351____array_0_FieldInfo,
	&InternalEnumerator_1_t5351____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32201_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5351____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5351_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32201_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5351____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5351_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m32204_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5351_PropertyInfos[] =
{
	&InternalEnumerator_1_t5351____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5351____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5351_InternalEnumerator_1__ctor_m32200_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m32200_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m32200_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5351_InternalEnumerator_1__ctor_m32200_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m32200_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32201_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32201_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5351_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32201_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m32202_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m32202_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m32202_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m32203_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m32203_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5351_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m32203_GenericMethod/* genericMethod */

};
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m32204_GenericMethod;
// T System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m32204_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5351_il2cpp_TypeInfo/* declaring_type */
	, &ThreadStaticAttribute_t2302_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m32204_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5351_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m32200_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32201_MethodInfo,
	&InternalEnumerator_1_Dispose_m32202_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32203_MethodInfo,
	&InternalEnumerator_1_get_Current_m32204_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m32203_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m32202_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5351_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32201_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32203_MethodInfo,
	&InternalEnumerator_1_Dispose_m32202_MethodInfo,
	&InternalEnumerator_1_get_Current_m32204_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5351_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7528_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5351_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7528_il2cpp_TypeInfo, 7},
};
extern TypeInfo ThreadStaticAttribute_t2302_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5351_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m32204_MethodInfo/* Method Usage */,
	&ThreadStaticAttribute_t2302_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisThreadStaticAttribute_t2302_m42317_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5351_0_0_0;
extern Il2CppType InternalEnumerator_1_t5351_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5351_GenericClass;
TypeInfo InternalEnumerator_1_t5351_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5351_MethodInfos/* methods */
	, InternalEnumerator_1_t5351_PropertyInfos/* properties */
	, InternalEnumerator_1_t5351_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5351_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5351_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5351_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5351_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5351_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5351_1_0_0/* this_arg */
	, InternalEnumerator_1_t5351_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5351_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5351_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5351)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9699_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>
extern MethodInfo ICollection_1_get_Count_m53818_MethodInfo;
static PropertyInfo ICollection_1_t9699____Count_PropertyInfo = 
{
	&ICollection_1_t9699_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53818_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53819_MethodInfo;
static PropertyInfo ICollection_1_t9699____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9699_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53819_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9699_PropertyInfos[] =
{
	&ICollection_1_t9699____Count_PropertyInfo,
	&ICollection_1_t9699____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53818_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m53818_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9699_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53818_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53819_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53819_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9699_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53819_GenericMethod/* genericMethod */

};
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo ICollection_1_t9699_ICollection_1_Add_m53820_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53820_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::Add(T)
MethodInfo ICollection_1_Add_m53820_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9699_ICollection_1_Add_m53820_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53820_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53821_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::Clear()
MethodInfo ICollection_1_Clear_m53821_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53821_GenericMethod/* genericMethod */

};
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo ICollection_1_t9699_ICollection_1_Contains_m53822_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53822_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m53822_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9699_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9699_ICollection_1_Contains_m53822_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53822_GenericMethod/* genericMethod */

};
extern Il2CppType ThreadStaticAttributeU5BU5D_t5617_0_0_0;
extern Il2CppType ThreadStaticAttributeU5BU5D_t5617_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9699_ICollection_1_CopyTo_m53823_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ThreadStaticAttributeU5BU5D_t5617_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53823_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53823_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9699_ICollection_1_CopyTo_m53823_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53823_GenericMethod/* genericMethod */

};
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo ICollection_1_t9699_ICollection_1_Remove_m53824_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53824_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ThreadStaticAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m53824_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9699_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9699_ICollection_1_Remove_m53824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53824_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9699_MethodInfos[] =
{
	&ICollection_1_get_Count_m53818_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53819_MethodInfo,
	&ICollection_1_Add_m53820_MethodInfo,
	&ICollection_1_Clear_m53821_MethodInfo,
	&ICollection_1_Contains_m53822_MethodInfo,
	&ICollection_1_CopyTo_m53823_MethodInfo,
	&ICollection_1_Remove_m53824_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9701_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9699_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9701_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9699_0_0_0;
extern Il2CppType ICollection_1_t9699_1_0_0;
struct ICollection_1_t9699;
extern Il2CppGenericClass ICollection_1_t9699_GenericClass;
TypeInfo ICollection_1_t9699_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9699_MethodInfos/* methods */
	, ICollection_1_t9699_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9699_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9699_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9699_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9699_0_0_0/* byval_arg */
	, &ICollection_1_t9699_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9699_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ThreadStaticAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ThreadStaticAttribute>
extern Il2CppType IEnumerator_1_t7528_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53825_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ThreadStaticAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53825_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9701_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7528_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53825_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9701_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53825_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9701_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9701_0_0_0;
extern Il2CppType IEnumerable_1_t9701_1_0_0;
struct IEnumerable_1_t9701;
extern Il2CppGenericClass IEnumerable_1_t9701_GenericClass;
TypeInfo IEnumerable_1_t9701_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9701_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9701_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9701_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9701_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9701_0_0_0/* byval_arg */
	, &IEnumerable_1_t9701_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9701_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9700_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.ThreadStaticAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.ThreadStaticAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.ThreadStaticAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.ThreadStaticAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.ThreadStaticAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.ThreadStaticAttribute>
extern MethodInfo IList_1_get_Item_m53826_MethodInfo;
extern MethodInfo IList_1_set_Item_m53827_MethodInfo;
static PropertyInfo IList_1_t9700____Item_PropertyInfo = 
{
	&IList_1_t9700_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53826_MethodInfo/* get */
	, &IList_1_set_Item_m53827_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9700_PropertyInfos[] =
{
	&IList_1_t9700____Item_PropertyInfo,
	NULL
};
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo IList_1_t9700_IList_1_IndexOf_m53828_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53828_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.ThreadStaticAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53828_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9700_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9700_IList_1_IndexOf_m53828_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53828_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo IList_1_t9700_IList_1_Insert_m53829_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53829_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ThreadStaticAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53829_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9700_IList_1_Insert_m53829_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53829_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9700_IList_1_RemoveAt_m53830_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53830_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ThreadStaticAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53830_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9700_IList_1_RemoveAt_m53830_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53830_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9700_IList_1_get_Item_m53826_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53826_GenericMethod;
// T System.Collections.Generic.IList`1<System.ThreadStaticAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53826_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9700_il2cpp_TypeInfo/* declaring_type */
	, &ThreadStaticAttribute_t2302_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9700_IList_1_get_Item_m53826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53826_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo IList_1_t9700_IList_1_set_Item_m53827_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53827_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ThreadStaticAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53827_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9700_IList_1_set_Item_m53827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53827_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9700_MethodInfos[] =
{
	&IList_1_IndexOf_m53828_MethodInfo,
	&IList_1_Insert_m53829_MethodInfo,
	&IList_1_RemoveAt_m53830_MethodInfo,
	&IList_1_get_Item_m53826_MethodInfo,
	&IList_1_set_Item_m53827_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9700_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9699_il2cpp_TypeInfo,
	&IEnumerable_1_t9701_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9700_0_0_0;
extern Il2CppType IList_1_t9700_1_0_0;
struct IList_1_t9700;
extern Il2CppGenericClass IList_1_t9700_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9700_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9700_MethodInfos/* methods */
	, IList_1_t9700_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9700_il2cpp_TypeInfo/* element_class */
	, IList_1_t9700_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9700_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9700_0_0_0/* byval_arg */
	, &IList_1_t9700_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9700_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_GenericComparer_1_gen_2.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GenericComparer_1_t2738_il2cpp_TypeInfo;
// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_GenericComparer_1_gen_2MethodDeclarations.h"

// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
extern TypeInfo TimeSpan_t852_il2cpp_TypeInfo;
extern TypeInfo IComparable_1_t2740_il2cpp_TypeInfo;
// System.Collections.Generic.Comparer`1<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_60MethodDeclarations.h"
extern MethodInfo Comparer_1__ctor_m32206_MethodInfo;
extern MethodInfo IComparable_1_CompareTo_m53318_MethodInfo;


// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern MethodInfo GenericComparer_1__ctor_m14136_MethodInfo;
 void GenericComparer_1__ctor_m14136 (GenericComparer_1_t2738 * __this, MethodInfo* method){
	{
		Comparer_1__ctor_m32206(__this, /*hidden argument*/&Comparer_1__ctor_m32206_MethodInfo);
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern MethodInfo GenericComparer_1_Compare_m32205_MethodInfo;
 int32_t GenericComparer_1_Compare_m32205 (GenericComparer_1_t2738 * __this, TimeSpan_t852  ___x, TimeSpan_t852  ___y, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		TimeSpan_t852  L_0 = ___x;
		Object_t * L_1 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		TimeSpan_t852  L_2 = ___y;
		Object_t * L_3 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		TimeSpan_t852  L_4 = ___y;
		Object_t * L_5 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		NullCheck(Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &(*(&___x))));
		int32_t L_6 = (int32_t)InterfaceFuncInvoker1< int32_t, TimeSpan_t852  >::Invoke(&IComparable_1_CompareTo_m53318_MethodInfo, Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &(*(&___x))), ___y);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.GenericComparer`1<System.TimeSpan>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod GenericComparer_1__ctor_m14136_GenericMethod;
// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
MethodInfo GenericComparer_1__ctor_m14136_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericComparer_1__ctor_m14136/* method */
	, &GenericComparer_1_t2738_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &GenericComparer_1__ctor_m14136_GenericMethod/* genericMethod */

};
extern Il2CppType TimeSpan_t852_0_0_0;
extern Il2CppType TimeSpan_t852_0_0_0;
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo GenericComparer_1_t2738_GenericComparer_1_Compare_m32205_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_TimeSpan_t852_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod GenericComparer_1_Compare_m32205_GenericMethod;
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
MethodInfo GenericComparer_1_Compare_m32205_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&GenericComparer_1_Compare_m32205/* method */
	, &GenericComparer_1_t2738_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_TimeSpan_t852_TimeSpan_t852/* invoker_method */
	, GenericComparer_1_t2738_GenericComparer_1_Compare_m32205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &GenericComparer_1_Compare_m32205_GenericMethod/* genericMethod */

};
static MethodInfo* GenericComparer_1_t2738_MethodInfos[] =
{
	&GenericComparer_1__ctor_m14136_MethodInfo,
	&GenericComparer_1_Compare_m32205_MethodInfo,
	NULL
};
extern MethodInfo Comparer_1_System_Collections_IComparer_Compare_m32208_MethodInfo;
static MethodInfo* GenericComparer_1_t2738_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&GenericComparer_1_Compare_m32205_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m32208_MethodInfo,
	&GenericComparer_1_Compare_m32205_MethodInfo,
};
extern TypeInfo IComparer_1_t10028_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair GenericComparer_1_t2738_InterfacesOffsets[] = 
{
	{ &IComparer_1_t10028_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType GenericComparer_1_t2738_0_0_0;
extern Il2CppType GenericComparer_1_t2738_1_0_0;
extern TypeInfo Comparer_1_t5352_il2cpp_TypeInfo;
struct GenericComparer_1_t2738;
extern Il2CppGenericClass GenericComparer_1_t2738_GenericClass;
TypeInfo GenericComparer_1_t2738_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, GenericComparer_1_t2738_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Comparer_1_t5352_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GenericComparer_1_t2738_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, GenericComparer_1_t2738_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GenericComparer_1_t2738_il2cpp_TypeInfo/* cast_class */
	, &GenericComparer_1_t2738_0_0_0/* byval_arg */
	, &GenericComparer_1_t2738_1_0_0/* this_arg */
	, GenericComparer_1_t2738_InterfacesOffsets/* interface_offsets */
	, &GenericComparer_1_t2738_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericComparer_1_t2738)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.Comparer`1<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_60.h"
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_61.h"
extern TypeInfo DefaultComparer_t5353_il2cpp_TypeInfo;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_61MethodDeclarations.h"
extern Il2CppType IComparable_1_t2740_0_0_0;
extern MethodInfo DefaultComparer__ctor_m32210_MethodInfo;
extern MethodInfo Comparer_1_Compare_m53831_MethodInfo;


// System.Void System.Collections.Generic.Comparer`1<System.TimeSpan>::.ctor()
 void Comparer_1__ctor_m32206 (Comparer_1_t5352 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.TimeSpan>::.cctor()
extern MethodInfo Comparer_1__cctor_m32207_MethodInfo;
 void Comparer_1__cctor_m32207 (Object_t * __this/* static, unused */, MethodInfo* method){
	DefaultComparer_t5353 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (DefaultComparer_t5353 *)il2cpp_codegen_object_new(InitializedTypeInfo(&DefaultComparer_t5353_il2cpp_TypeInfo));
	DefaultComparer__ctor_m32210(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &DefaultComparer__ctor_m32210_MethodInfo);
	((Comparer_1_t5352_StaticFields*)InitializedTypeInfo(&Comparer_1_t5352_il2cpp_TypeInfo)->static_fields)->____default_0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
// System.Int32 System.Collections.Generic.Comparer`1<System.TimeSpan>::System.Collections.IComparer.Compare(System.Object,System.Object)
 int32_t Comparer_1_System_Collections_IComparer_Compare_m32208 (Comparer_1_t5352 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (___x)
		{
			goto IL_000b;
		}
	}
	{
		if (___y)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (___y)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((Object_t *)IsInst(___x, InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((Object_t *)IsInst(___y, InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, TimeSpan_t852 , TimeSpan_t852  >::Invoke(&Comparer_1_Compare_m53831_MethodInfo, __this, ((*(TimeSpan_t852 *)((TimeSpan_t852 *)UnBox (___x, InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo))))), ((*(TimeSpan_t852 *)((TimeSpan_t852 *)UnBox (___y, InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo))))));
		return L_0;
	}

IL_0033:
	{
		ArgumentException_t551 * L_1 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m12706(L_1, /*hidden argument*/&ArgumentException__ctor_m12706_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.TimeSpan>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.TimeSpan>::get_Default()
extern MethodInfo Comparer_1_get_Default_m32209_MethodInfo;
 Comparer_1_t5352 * Comparer_1_get_Default_m32209 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Comparer_1_t5352_il2cpp_TypeInfo));
		return (((Comparer_1_t5352_StaticFields*)InitializedTypeInfo(&Comparer_1_t5352_il2cpp_TypeInfo)->static_fields)->____default_0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<System.TimeSpan>
extern Il2CppType Comparer_1_t5352_0_0_49;
FieldInfo Comparer_1_t5352_____default_0_FieldInfo = 
{
	"_default"/* name */
	, &Comparer_1_t5352_0_0_49/* type */
	, &Comparer_1_t5352_il2cpp_TypeInfo/* parent */
	, offsetof(Comparer_1_t5352_StaticFields, ____default_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Comparer_1_t5352_FieldInfos[] =
{
	&Comparer_1_t5352_____default_0_FieldInfo,
	NULL
};
static PropertyInfo Comparer_1_t5352____Default_PropertyInfo = 
{
	&Comparer_1_t5352_il2cpp_TypeInfo/* parent */
	, "Default"/* name */
	, &Comparer_1_get_Default_m32209_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Comparer_1_t5352_PropertyInfos[] =
{
	&Comparer_1_t5352____Default_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1__ctor_m32206_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1<System.TimeSpan>::.ctor()
MethodInfo Comparer_1__ctor_m32206_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Comparer_1__ctor_m32206/* method */
	, &Comparer_1_t5352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1__ctor_m32206_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1__cctor_m32207_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1<System.TimeSpan>::.cctor()
MethodInfo Comparer_1__cctor_m32207_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Comparer_1__cctor_m32207/* method */
	, &Comparer_1_t5352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1__cctor_m32207_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Comparer_1_t5352_Comparer_1_System_Collections_IComparer_Compare_m32208_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_System_Collections_IComparer_Compare_m32208_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1<System.TimeSpan>::System.Collections.IComparer.Compare(System.Object,System.Object)
MethodInfo Comparer_1_System_Collections_IComparer_Compare_m32208_MethodInfo = 
{
	"System.Collections.IComparer.Compare"/* name */
	, (methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m32208/* method */
	, &Comparer_1_t5352_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, Comparer_1_t5352_Comparer_1_System_Collections_IComparer_Compare_m32208_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_System_Collections_IComparer_Compare_m32208_GenericMethod/* genericMethod */

};
extern Il2CppType TimeSpan_t852_0_0_0;
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo Comparer_1_t5352_Comparer_1_Compare_m53831_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_TimeSpan_t852_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_Compare_m53831_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1<System.TimeSpan>::Compare(T,T)
MethodInfo Comparer_1_Compare_m53831_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &Comparer_1_t5352_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_TimeSpan_t852_TimeSpan_t852/* invoker_method */
	, Comparer_1_t5352_Comparer_1_Compare_m53831_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_Compare_m53831_GenericMethod/* genericMethod */

};
extern Il2CppType Comparer_1_t5352_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_get_Default_m32209_GenericMethod;
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.TimeSpan>::get_Default()
MethodInfo Comparer_1_get_Default_m32209_MethodInfo = 
{
	"get_Default"/* name */
	, (methodPointerType)&Comparer_1_get_Default_m32209/* method */
	, &Comparer_1_t5352_il2cpp_TypeInfo/* declaring_type */
	, &Comparer_1_t5352_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_get_Default_m32209_GenericMethod/* genericMethod */

};
static MethodInfo* Comparer_1_t5352_MethodInfos[] =
{
	&Comparer_1__ctor_m32206_MethodInfo,
	&Comparer_1__cctor_m32207_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m32208_MethodInfo,
	&Comparer_1_Compare_m53831_MethodInfo,
	&Comparer_1_get_Default_m32209_MethodInfo,
	NULL
};
static MethodInfo* Comparer_1_t5352_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&Comparer_1_Compare_m53831_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m32208_MethodInfo,
	NULL,
};
static TypeInfo* Comparer_1_t5352_InterfacesTypeInfos[] = 
{
	&IComparer_1_t10028_il2cpp_TypeInfo,
	&IComparer_t1356_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Comparer_1_t5352_InterfacesOffsets[] = 
{
	{ &IComparer_1_t10028_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Comparer_1_t5352_0_0_0;
extern Il2CppType Comparer_1_t5352_1_0_0;
struct Comparer_1_t5352;
extern Il2CppGenericClass Comparer_1_t5352_GenericClass;
TypeInfo Comparer_1_t5352_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, Comparer_1_t5352_MethodInfos/* methods */
	, Comparer_1_t5352_PropertyInfos/* properties */
	, Comparer_1_t5352_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Comparer_1_t5352_il2cpp_TypeInfo/* element_class */
	, Comparer_1_t5352_InterfacesTypeInfos/* implemented_interfaces */
	, Comparer_1_t5352_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Comparer_1_t5352_il2cpp_TypeInfo/* cast_class */
	, &Comparer_1_t5352_0_0_0/* byval_arg */
	, &Comparer_1_t5352_1_0_0/* this_arg */
	, Comparer_1_t5352_InterfacesOffsets/* interface_offsets */
	, &Comparer_1_t5352_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Comparer_1_t5352)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Comparer_1_t5352_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IComparer`1<System.TimeSpan>::Compare(T,T)
// Metadata Definition System.Collections.Generic.IComparer`1<System.TimeSpan>
extern Il2CppType TimeSpan_t852_0_0_0;
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo IComparer_1_t10028_IComparer_1_Compare_m53832_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_TimeSpan_t852_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparer_1_Compare_m53832_GenericMethod;
// System.Int32 System.Collections.Generic.IComparer`1<System.TimeSpan>::Compare(T,T)
MethodInfo IComparer_1_Compare_m53832_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &IComparer_1_t10028_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_TimeSpan_t852_TimeSpan_t852/* invoker_method */
	, IComparer_1_t10028_IComparer_1_Compare_m53832_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparer_1_Compare_m53832_GenericMethod/* genericMethod */

};
static MethodInfo* IComparer_1_t10028_MethodInfos[] =
{
	&IComparer_1_Compare_m53832_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparer_1_t10028_0_0_0;
extern Il2CppType IComparer_1_t10028_1_0_0;
struct IComparer_1_t10028;
extern Il2CppGenericClass IComparer_1_t10028_GenericClass;
TypeInfo IComparer_1_t10028_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IComparer_1_t10028_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparer_1_t10028_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparer_1_t10028_il2cpp_TypeInfo/* cast_class */
	, &IComparer_1_t10028_0_0_0/* byval_arg */
	, &IComparer_1_t10028_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparer_1_t10028_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::.ctor()
 void DefaultComparer__ctor_m32210 (DefaultComparer_t5353 * __this, MethodInfo* method){
	{
		Comparer_1__ctor_m32206(__this, /*hidden argument*/&Comparer_1__ctor_m32206_MethodInfo);
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::Compare(T,T)
extern MethodInfo DefaultComparer_Compare_m32211_MethodInfo;
 int32_t DefaultComparer_Compare_m32211 (DefaultComparer_t5353 * __this, TimeSpan_t852  ___x, TimeSpan_t852  ___y, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		TimeSpan_t852  L_0 = ___x;
		Object_t * L_1 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		TimeSpan_t852  L_2 = ___y;
		Object_t * L_3 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		TimeSpan_t852  L_4 = ___y;
		Object_t * L_5 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		TimeSpan_t852  L_6 = ___x;
		Object_t * L_7 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_6);
		if (!((Object_t*)IsInst(L_7, InitializedTypeInfo(&IComparable_1_t2740_il2cpp_TypeInfo))))
		{
			goto IL_003e;
		}
	}
	{
		TimeSpan_t852  L_8 = ___x;
		Object_t * L_9 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_8);
		NullCheck(((Object_t*)Castclass(L_9, InitializedTypeInfo(&IComparable_1_t2740_il2cpp_TypeInfo))));
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, TimeSpan_t852  >::Invoke(&IComparable_1_CompareTo_m53318_MethodInfo, ((Object_t*)Castclass(L_9, InitializedTypeInfo(&IComparable_1_t2740_il2cpp_TypeInfo))), ___y);
		return L_10;
	}

IL_003e:
	{
		TimeSpan_t852  L_11 = ___x;
		Object_t * L_12 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_11);
		if (!((Object_t *)IsInst(L_12, InitializedTypeInfo(&IComparable_t184_il2cpp_TypeInfo))))
		{
			goto IL_0062;
		}
	}
	{
		TimeSpan_t852  L_13 = ___x;
		Object_t * L_14 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_13);
		TimeSpan_t852  L_15 = ___y;
		Object_t * L_16 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_15);
		NullCheck(((Object_t *)Castclass(L_14, InitializedTypeInfo(&IComparable_t184_il2cpp_TypeInfo))));
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(&IComparable_CompareTo_m13522_MethodInfo, ((Object_t *)Castclass(L_14, InitializedTypeInfo(&IComparable_t184_il2cpp_TypeInfo))), L_16);
		return L_17;
	}

IL_0062:
	{
		ArgumentException_t551 * L_18 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_18, (String_t*) &_stringLiteral1447, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer__ctor_m32210_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::.ctor()
MethodInfo DefaultComparer__ctor_m32210_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultComparer__ctor_m32210/* method */
	, &DefaultComparer_t5353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer__ctor_m32210_GenericMethod/* genericMethod */

};
extern Il2CppType TimeSpan_t852_0_0_0;
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo DefaultComparer_t5353_DefaultComparer_Compare_m32211_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_TimeSpan_t852_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_Compare_m32211_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::Compare(T,T)
MethodInfo DefaultComparer_Compare_m32211_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&DefaultComparer_Compare_m32211/* method */
	, &DefaultComparer_t5353_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_TimeSpan_t852_TimeSpan_t852/* invoker_method */
	, DefaultComparer_t5353_DefaultComparer_Compare_m32211_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_Compare_m32211_GenericMethod/* genericMethod */

};
static MethodInfo* DefaultComparer_t5353_MethodInfos[] =
{
	&DefaultComparer__ctor_m32210_MethodInfo,
	&DefaultComparer_Compare_m32211_MethodInfo,
	NULL
};
static MethodInfo* DefaultComparer_t5353_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&DefaultComparer_Compare_m32211_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m32208_MethodInfo,
	&DefaultComparer_Compare_m32211_MethodInfo,
};
static Il2CppInterfaceOffsetPair DefaultComparer_t5353_InterfacesOffsets[] = 
{
	{ &IComparer_1_t10028_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType DefaultComparer_t5353_0_0_0;
extern Il2CppType DefaultComparer_t5353_1_0_0;
struct DefaultComparer_t5353;
extern Il2CppGenericClass DefaultComparer_t5353_GenericClass;
TypeInfo DefaultComparer_t5353_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, DefaultComparer_t5353_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Comparer_1_t5352_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Comparer_1_t1854_il2cpp_TypeInfo/* nested_in */
	, &DefaultComparer_t5353_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultComparer_t5353_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultComparer_t5353_il2cpp_TypeInfo/* cast_class */
	, &DefaultComparer_t5353_0_0_0/* byval_arg */
	, &DefaultComparer_t5353_1_0_0/* this_arg */
	, DefaultComparer_t5353_InterfacesOffsets/* interface_offsets */
	, &DefaultComparer_t5353_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultComparer_t5353)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer__3.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GenericEqualityComparer_1_t2739_il2cpp_TypeInfo;
// System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer__3MethodDeclarations.h"

extern TypeInfo IEquatable_1_t2741_il2cpp_TypeInfo;
// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_78MethodDeclarations.h"
extern MethodInfo EqualityComparer_1__ctor_m32214_MethodInfo;
extern MethodInfo IEquatable_1_Equals_m53333_MethodInfo;


// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::.ctor()
extern MethodInfo GenericEqualityComparer_1__ctor_m14137_MethodInfo;
 void GenericEqualityComparer_1__ctor_m14137 (GenericEqualityComparer_1_t2739 * __this, MethodInfo* method){
	{
		EqualityComparer_1__ctor_m32214(__this, /*hidden argument*/&EqualityComparer_1__ctor_m32214_MethodInfo);
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::GetHashCode(T)
extern MethodInfo GenericEqualityComparer_1_GetHashCode_m32212_MethodInfo;
 int32_t GenericEqualityComparer_1_GetHashCode_m32212 (GenericEqualityComparer_1_t2739 * __this, TimeSpan_t852  ___obj, MethodInfo* method){
	{
		TimeSpan_t852  L_0 = ___obj;
		Object_t * L_1 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		NullCheck(Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &(*(&___obj))));
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&Object_GetHashCode_m321_MethodInfo, Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &(*(&___obj))));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::Equals(T,T)
extern MethodInfo GenericEqualityComparer_1_Equals_m32213_MethodInfo;
 bool GenericEqualityComparer_1_Equals_m32213 (GenericEqualityComparer_1_t2739 * __this, TimeSpan_t852  ___x, TimeSpan_t852  ___y, MethodInfo* method){
	{
		TimeSpan_t852  L_0 = ___x;
		Object_t * L_1 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		TimeSpan_t852  L_2 = ___y;
		Object_t * L_3 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_2);
		return ((((Object_t *)L_3) == ((Object_t *)NULL))? 1 : 0);
	}

IL_0012:
	{
		NullCheck(Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &(*(&___x))));
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, TimeSpan_t852  >::Invoke(&IEquatable_1_Equals_m53333_MethodInfo, Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &(*(&___x))), ___y);
		return L_4;
	}
}
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod GenericEqualityComparer_1__ctor_m14137_GenericMethod;
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::.ctor()
MethodInfo GenericEqualityComparer_1__ctor_m14137_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericEqualityComparer_1__ctor_m14137/* method */
	, &GenericEqualityComparer_1_t2739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &GenericEqualityComparer_1__ctor_m14137_GenericMethod/* genericMethod */

};
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo GenericEqualityComparer_1_t2739_GenericEqualityComparer_1_GetHashCode_m32212_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod GenericEqualityComparer_1_GetHashCode_m32212_GenericMethod;
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::GetHashCode(T)
MethodInfo GenericEqualityComparer_1_GetHashCode_m32212_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&GenericEqualityComparer_1_GetHashCode_m32212/* method */
	, &GenericEqualityComparer_1_t2739_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_TimeSpan_t852/* invoker_method */
	, GenericEqualityComparer_1_t2739_GenericEqualityComparer_1_GetHashCode_m32212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &GenericEqualityComparer_1_GetHashCode_m32212_GenericMethod/* genericMethod */

};
extern Il2CppType TimeSpan_t852_0_0_0;
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo GenericEqualityComparer_1_t2739_GenericEqualityComparer_1_Equals_m32213_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_TimeSpan_t852_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod GenericEqualityComparer_1_Equals_m32213_GenericMethod;
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::Equals(T,T)
MethodInfo GenericEqualityComparer_1_Equals_m32213_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&GenericEqualityComparer_1_Equals_m32213/* method */
	, &GenericEqualityComparer_1_t2739_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_TimeSpan_t852_TimeSpan_t852/* invoker_method */
	, GenericEqualityComparer_1_t2739_GenericEqualityComparer_1_Equals_m32213_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &GenericEqualityComparer_1_Equals_m32213_GenericMethod/* genericMethod */

};
static MethodInfo* GenericEqualityComparer_1_t2739_MethodInfos[] =
{
	&GenericEqualityComparer_1__ctor_m14137_MethodInfo,
	&GenericEqualityComparer_1_GetHashCode_m32212_MethodInfo,
	&GenericEqualityComparer_1_Equals_m32213_MethodInfo,
	NULL
};
extern MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217_MethodInfo;
extern MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216_MethodInfo;
static MethodInfo* GenericEqualityComparer_1_t2739_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&GenericEqualityComparer_1_Equals_m32213_MethodInfo,
	&GenericEqualityComparer_1_GetHashCode_m32212_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216_MethodInfo,
	&GenericEqualityComparer_1_GetHashCode_m32212_MethodInfo,
	&GenericEqualityComparer_1_Equals_m32213_MethodInfo,
};
extern TypeInfo IEqualityComparer_1_t10029_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair GenericEqualityComparer_1_t2739_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t10029_il2cpp_TypeInfo, 4},
	{ &IEqualityComparer_t1363_il2cpp_TypeInfo, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType GenericEqualityComparer_1_t2739_0_0_0;
extern Il2CppType GenericEqualityComparer_1_t2739_1_0_0;
extern TypeInfo EqualityComparer_1_t5354_il2cpp_TypeInfo;
struct GenericEqualityComparer_1_t2739;
extern Il2CppGenericClass GenericEqualityComparer_1_t2739_GenericClass;
TypeInfo GenericEqualityComparer_1_t2739_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericEqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, GenericEqualityComparer_1_t2739_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GenericEqualityComparer_1_t2739_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, GenericEqualityComparer_1_t2739_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GenericEqualityComparer_1_t2739_il2cpp_TypeInfo/* cast_class */
	, &GenericEqualityComparer_1_t2739_0_0_0/* byval_arg */
	, &GenericEqualityComparer_1_t2739_1_0_0/* this_arg */
	, GenericEqualityComparer_1_t2739_InterfacesOffsets/* interface_offsets */
	, &GenericEqualityComparer_1_t2739_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericEqualityComparer_1_t2739)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_78.h"
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_79.h"
extern TypeInfo DefaultComparer_t5355_il2cpp_TypeInfo;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_79MethodDeclarations.h"
extern Il2CppType IEquatable_1_t2741_0_0_0;
extern MethodInfo DefaultComparer__ctor_m32219_MethodInfo;
extern MethodInfo EqualityComparer_1_GetHashCode_m53833_MethodInfo;
extern MethodInfo EqualityComparer_1_Equals_m53834_MethodInfo;


// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.ctor()
 void EqualityComparer_1__ctor_m32214 (EqualityComparer_1_t5354 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.cctor()
extern MethodInfo EqualityComparer_1__cctor_m32215_MethodInfo;
 void EqualityComparer_1__cctor_m32215 (Object_t * __this/* static, unused */, MethodInfo* method){
	DefaultComparer_t5355 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (DefaultComparer_t5355 *)il2cpp_codegen_object_new(InitializedTypeInfo(&DefaultComparer_t5355_il2cpp_TypeInfo));
	DefaultComparer__ctor_m32219(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &DefaultComparer__ctor_m32219_MethodInfo);
	((EqualityComparer_1_t5354_StaticFields*)InitializedTypeInfo(&EqualityComparer_1_t5354_il2cpp_TypeInfo)->static_fields)->____default_0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
 int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216 (EqualityComparer_1_t5354 * __this, Object_t * ___obj, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, TimeSpan_t852  >::Invoke(&EqualityComparer_1_GetHashCode_m53833_MethodInfo, __this, ((*(TimeSpan_t852 *)((TimeSpan_t852 *)UnBox (___obj, InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo))))));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
 bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217 (EqualityComparer_1_t5354 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, TimeSpan_t852 , TimeSpan_t852  >::Invoke(&EqualityComparer_1_Equals_m53834_MethodInfo, __this, ((*(TimeSpan_t852 *)((TimeSpan_t852 *)UnBox (___x, InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo))))), ((*(TimeSpan_t852 *)((TimeSpan_t852 *)UnBox (___y, InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo))))));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::get_Default()
extern MethodInfo EqualityComparer_1_get_Default_m32218_MethodInfo;
 EqualityComparer_1_t5354 * EqualityComparer_1_get_Default_m32218 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&EqualityComparer_1_t5354_il2cpp_TypeInfo));
		return (((EqualityComparer_1_t5354_StaticFields*)InitializedTypeInfo(&EqualityComparer_1_t5354_il2cpp_TypeInfo)->static_fields)->____default_0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
extern Il2CppType EqualityComparer_1_t5354_0_0_49;
FieldInfo EqualityComparer_1_t5354_____default_0_FieldInfo = 
{
	"_default"/* name */
	, &EqualityComparer_1_t5354_0_0_49/* type */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* parent */
	, offsetof(EqualityComparer_1_t5354_StaticFields, ____default_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* EqualityComparer_1_t5354_FieldInfos[] =
{
	&EqualityComparer_1_t5354_____default_0_FieldInfo,
	NULL
};
static PropertyInfo EqualityComparer_1_t5354____Default_PropertyInfo = 
{
	&EqualityComparer_1_t5354_il2cpp_TypeInfo/* parent */
	, "Default"/* name */
	, &EqualityComparer_1_get_Default_m32218_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* EqualityComparer_1_t5354_PropertyInfos[] =
{
	&EqualityComparer_1_t5354____Default_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1__ctor_m32214_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.ctor()
MethodInfo EqualityComparer_1__ctor_m32214_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EqualityComparer_1__ctor_m32214/* method */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1__ctor_m32214_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1__cctor_m32215_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::.cctor()
MethodInfo EqualityComparer_1__cctor_m32215_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&EqualityComparer_1__cctor_m32215/* method */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1__cctor_m32215_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo EqualityComparer_1_t5354_EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216_MethodInfo = 
{
	"System.Collections.IEqualityComparer.GetHashCode"/* name */
	, (methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216/* method */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, EqualityComparer_1_t5354_EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo EqualityComparer_1_t5354_EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217_MethodInfo = 
{
	"System.Collections.IEqualityComparer.Equals"/* name */
	, (methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217/* method */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, EqualityComparer_1_t5354_EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217_GenericMethod/* genericMethod */

};
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo EqualityComparer_1_t5354_EqualityComparer_1_GetHashCode_m53833_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_GetHashCode_m53833_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::GetHashCode(T)
MethodInfo EqualityComparer_1_GetHashCode_m53833_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_TimeSpan_t852/* invoker_method */
	, EqualityComparer_1_t5354_EqualityComparer_1_GetHashCode_m53833_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_GetHashCode_m53833_GenericMethod/* genericMethod */

};
extern Il2CppType TimeSpan_t852_0_0_0;
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo EqualityComparer_1_t5354_EqualityComparer_1_Equals_m53834_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_TimeSpan_t852_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_Equals_m53834_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::Equals(T,T)
MethodInfo EqualityComparer_1_Equals_m53834_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_TimeSpan_t852_TimeSpan_t852/* invoker_method */
	, EqualityComparer_1_t5354_EqualityComparer_1_Equals_m53834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_Equals_m53834_GenericMethod/* genericMethod */

};
extern Il2CppType EqualityComparer_1_t5354_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_get_Default_m32218_GenericMethod;
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::get_Default()
MethodInfo EqualityComparer_1_get_Default_m32218_MethodInfo = 
{
	"get_Default"/* name */
	, (methodPointerType)&EqualityComparer_1_get_Default_m32218/* method */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* declaring_type */
	, &EqualityComparer_1_t5354_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_get_Default_m32218_GenericMethod/* genericMethod */

};
static MethodInfo* EqualityComparer_1_t5354_MethodInfos[] =
{
	&EqualityComparer_1__ctor_m32214_MethodInfo,
	&EqualityComparer_1__cctor_m32215_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217_MethodInfo,
	&EqualityComparer_1_GetHashCode_m53833_MethodInfo,
	&EqualityComparer_1_Equals_m53834_MethodInfo,
	&EqualityComparer_1_get_Default_m32218_MethodInfo,
	NULL
};
static MethodInfo* EqualityComparer_1_t5354_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&EqualityComparer_1_Equals_m53834_MethodInfo,
	&EqualityComparer_1_GetHashCode_m53833_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216_MethodInfo,
	NULL,
	NULL,
};
static TypeInfo* EqualityComparer_1_t5354_InterfacesTypeInfos[] = 
{
	&IEqualityComparer_1_t10029_il2cpp_TypeInfo,
	&IEqualityComparer_t1363_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair EqualityComparer_1_t5354_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t10029_il2cpp_TypeInfo, 4},
	{ &IEqualityComparer_t1363_il2cpp_TypeInfo, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType EqualityComparer_1_t5354_0_0_0;
extern Il2CppType EqualityComparer_1_t5354_1_0_0;
struct EqualityComparer_1_t5354;
extern Il2CppGenericClass EqualityComparer_1_t5354_GenericClass;
TypeInfo EqualityComparer_1_t5354_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, EqualityComparer_1_t5354_MethodInfos/* methods */
	, EqualityComparer_1_t5354_PropertyInfos/* properties */
	, EqualityComparer_1_t5354_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* element_class */
	, EqualityComparer_1_t5354_InterfacesTypeInfos/* implemented_interfaces */
	, EqualityComparer_1_t5354_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* cast_class */
	, &EqualityComparer_1_t5354_0_0_0/* byval_arg */
	, &EqualityComparer_1_t5354_1_0_0/* this_arg */
	, EqualityComparer_1_t5354_InterfacesOffsets/* interface_offsets */
	, &EqualityComparer_1_t5354_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EqualityComparer_1_t5354)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EqualityComparer_1_t5354_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Boolean System.Collections.Generic.IEqualityComparer`1<System.TimeSpan>::Equals(T,T)
// System.Int32 System.Collections.Generic.IEqualityComparer`1<System.TimeSpan>::GetHashCode(T)
// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.TimeSpan>
extern Il2CppType TimeSpan_t852_0_0_0;
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo IEqualityComparer_1_t10029_IEqualityComparer_1_Equals_m53835_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_TimeSpan_t852_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEqualityComparer_1_Equals_m53835_GenericMethod;
// System.Boolean System.Collections.Generic.IEqualityComparer`1<System.TimeSpan>::Equals(T,T)
MethodInfo IEqualityComparer_1_Equals_m53835_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEqualityComparer_1_t10029_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_TimeSpan_t852_TimeSpan_t852/* invoker_method */
	, IEqualityComparer_1_t10029_IEqualityComparer_1_Equals_m53835_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEqualityComparer_1_Equals_m53835_GenericMethod/* genericMethod */

};
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo IEqualityComparer_1_t10029_IEqualityComparer_1_GetHashCode_m53836_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEqualityComparer_1_GetHashCode_m53836_GenericMethod;
// System.Int32 System.Collections.Generic.IEqualityComparer`1<System.TimeSpan>::GetHashCode(T)
MethodInfo IEqualityComparer_1_GetHashCode_m53836_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &IEqualityComparer_1_t10029_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_TimeSpan_t852/* invoker_method */
	, IEqualityComparer_1_t10029_IEqualityComparer_1_GetHashCode_m53836_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEqualityComparer_1_GetHashCode_m53836_GenericMethod/* genericMethod */

};
static MethodInfo* IEqualityComparer_1_t10029_MethodInfos[] =
{
	&IEqualityComparer_1_Equals_m53835_MethodInfo,
	&IEqualityComparer_1_GetHashCode_m53836_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEqualityComparer_1_t10029_0_0_0;
extern Il2CppType IEqualityComparer_1_t10029_1_0_0;
struct IEqualityComparer_1_t10029;
extern Il2CppGenericClass IEqualityComparer_1_t10029_GenericClass;
TypeInfo IEqualityComparer_1_t10029_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEqualityComparer_1_t10029_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEqualityComparer_1_t10029_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEqualityComparer_1_t10029_il2cpp_TypeInfo/* cast_class */
	, &IEqualityComparer_1_t10029_0_0_0/* byval_arg */
	, &IEqualityComparer_1_t10029_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEqualityComparer_1_t10029_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::.ctor()
 void DefaultComparer__ctor_m32219 (DefaultComparer_t5355 * __this, MethodInfo* method){
	{
		EqualityComparer_1__ctor_m32214(__this, /*hidden argument*/&EqualityComparer_1__ctor_m32214_MethodInfo);
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::GetHashCode(T)
extern MethodInfo DefaultComparer_GetHashCode_m32220_MethodInfo;
 int32_t DefaultComparer_GetHashCode_m32220 (DefaultComparer_t5355 * __this, TimeSpan_t852  ___obj, MethodInfo* method){
	{
		TimeSpan_t852  L_0 = ___obj;
		Object_t * L_1 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		NullCheck(Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &(*(&___obj))));
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&Object_GetHashCode_m321_MethodInfo, Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &(*(&___obj))));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::Equals(T,T)
extern MethodInfo DefaultComparer_Equals_m32221_MethodInfo;
 bool DefaultComparer_Equals_m32221 (DefaultComparer_t5355 * __this, TimeSpan_t852  ___x, TimeSpan_t852  ___y, MethodInfo* method){
	{
		TimeSpan_t852  L_0 = ___x;
		Object_t * L_1 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		TimeSpan_t852  L_2 = ___y;
		Object_t * L_3 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_2);
		return ((((Object_t *)L_3) == ((Object_t *)NULL))? 1 : 0);
	}

IL_0012:
	{
		TimeSpan_t852  L_4 = ___y;
		Object_t * L_5 = Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &L_4);
		NullCheck(Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &(*(&___x))));
		bool L_6 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&TimeSpan_t852_il2cpp_TypeInfo), &(*(&___x))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer__ctor_m32219_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::.ctor()
MethodInfo DefaultComparer__ctor_m32219_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultComparer__ctor_m32219/* method */
	, &DefaultComparer_t5355_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer__ctor_m32219_GenericMethod/* genericMethod */

};
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo DefaultComparer_t5355_DefaultComparer_GetHashCode_m32220_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_GetHashCode_m32220_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::GetHashCode(T)
MethodInfo DefaultComparer_GetHashCode_m32220_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultComparer_GetHashCode_m32220/* method */
	, &DefaultComparer_t5355_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_TimeSpan_t852/* invoker_method */
	, DefaultComparer_t5355_DefaultComparer_GetHashCode_m32220_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_GetHashCode_m32220_GenericMethod/* genericMethod */

};
extern Il2CppType TimeSpan_t852_0_0_0;
extern Il2CppType TimeSpan_t852_0_0_0;
static ParameterInfo DefaultComparer_t5355_DefaultComparer_Equals_m32221_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &TimeSpan_t852_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_TimeSpan_t852_TimeSpan_t852 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_Equals_m32221_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::Equals(T,T)
MethodInfo DefaultComparer_Equals_m32221_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultComparer_Equals_m32221/* method */
	, &DefaultComparer_t5355_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_TimeSpan_t852_TimeSpan_t852/* invoker_method */
	, DefaultComparer_t5355_DefaultComparer_Equals_m32221_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_Equals_m32221_GenericMethod/* genericMethod */

};
static MethodInfo* DefaultComparer_t5355_MethodInfos[] =
{
	&DefaultComparer__ctor_m32219_MethodInfo,
	&DefaultComparer_GetHashCode_m32220_MethodInfo,
	&DefaultComparer_Equals_m32221_MethodInfo,
	NULL
};
static MethodInfo* DefaultComparer_t5355_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&DefaultComparer_Equals_m32221_MethodInfo,
	&DefaultComparer_GetHashCode_m32220_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m32217_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m32216_MethodInfo,
	&DefaultComparer_GetHashCode_m32220_MethodInfo,
	&DefaultComparer_Equals_m32221_MethodInfo,
};
static Il2CppInterfaceOffsetPair DefaultComparer_t5355_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t10029_il2cpp_TypeInfo, 4},
	{ &IEqualityComparer_t1363_il2cpp_TypeInfo, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType DefaultComparer_t5355_0_0_0;
extern Il2CppType DefaultComparer_t5355_1_0_0;
struct DefaultComparer_t5355;
extern Il2CppGenericClass DefaultComparer_t5355_GenericClass;
TypeInfo DefaultComparer_t5355_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, DefaultComparer_t5355_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &EqualityComparer_1_t5354_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &EqualityComparer_1_t1866_il2cpp_TypeInfo/* nested_in */
	, &DefaultComparer_t5355_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultComparer_t5355_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultComparer_t5355_il2cpp_TypeInfo/* cast_class */
	, &DefaultComparer_t5355_0_0_0/* byval_arg */
	, &DefaultComparer_t5355_1_0_0/* this_arg */
	, DefaultComparer_t5355_InterfacesOffsets/* interface_offsets */
	, &DefaultComparer_t5355_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultComparer_t5355)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7530_il2cpp_TypeInfo;

// System.TypeCode
#include "mscorlib_System_TypeCode.h"


// T System.Collections.Generic.IEnumerator`1<System.TypeCode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.TypeCode>
extern MethodInfo IEnumerator_1_get_Current_m53837_MethodInfo;
static PropertyInfo IEnumerator_1_t7530____Current_PropertyInfo = 
{
	&IEnumerator_1_t7530_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53837_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7530_PropertyInfos[] =
{
	&IEnumerator_1_t7530____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeCode_t2305_0_0_0;
extern void* RuntimeInvoker_TypeCode_t2305 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53837_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.TypeCode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53837_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7530_il2cpp_TypeInfo/* declaring_type */
	, &TypeCode_t2305_0_0_0/* return_type */
	, RuntimeInvoker_TypeCode_t2305/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53837_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7530_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53837_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7530_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7530_0_0_0;
extern Il2CppType IEnumerator_1_t7530_1_0_0;
struct IEnumerator_1_t7530;
extern Il2CppGenericClass IEnumerator_1_t7530_GenericClass;
TypeInfo IEnumerator_1_t7530_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7530_MethodInfos/* methods */
	, IEnumerator_1_t7530_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7530_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7530_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7530_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7530_0_0_0/* byval_arg */
	, &IEnumerator_1_t7530_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7530_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.TypeCode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_781.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5356_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.TypeCode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_781MethodDeclarations.h"

extern TypeInfo TypeCode_t2305_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m32226_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeCode_t2305_m42328_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.TypeCode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.TypeCode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTypeCode_t2305_m42328 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m32222_MethodInfo;
 void InternalEnumerator_1__ctor_m32222 (InternalEnumerator_1_t5356 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.TypeCode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32223_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32223 (InternalEnumerator_1_t5356 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m32226(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m32226_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m32224_MethodInfo;
 void InternalEnumerator_1_Dispose_m32224 (InternalEnumerator_1_t5356 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.TypeCode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m32225_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m32225 (InternalEnumerator_1_t5356 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.TypeCode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m32226 (InternalEnumerator_1_t5356 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTypeCode_t2305_m42328(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTypeCode_t2305_m42328_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.TypeCode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5356____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5356_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5356, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5356____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5356_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5356, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5356_FieldInfos[] =
{
	&InternalEnumerator_1_t5356____array_0_FieldInfo,
	&InternalEnumerator_1_t5356____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5356____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5356_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32223_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5356____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5356_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m32226_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5356_PropertyInfos[] =
{
	&InternalEnumerator_1_t5356____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5356____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5356_InternalEnumerator_1__ctor_m32222_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m32222_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m32222_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m32222/* method */
	, &InternalEnumerator_1_t5356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5356_InternalEnumerator_1__ctor_m32222_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m32222_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32223_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.TypeCode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32223_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32223/* method */
	, &InternalEnumerator_1_t5356_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32223_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m32224_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m32224_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m32224/* method */
	, &InternalEnumerator_1_t5356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m32224_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m32225_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.TypeCode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m32225_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m32225/* method */
	, &InternalEnumerator_1_t5356_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m32225_GenericMethod/* genericMethod */

};
extern Il2CppType TypeCode_t2305_0_0_0;
extern void* RuntimeInvoker_TypeCode_t2305 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m32226_GenericMethod;
// T System.Array/InternalEnumerator`1<System.TypeCode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m32226_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m32226/* method */
	, &InternalEnumerator_1_t5356_il2cpp_TypeInfo/* declaring_type */
	, &TypeCode_t2305_0_0_0/* return_type */
	, RuntimeInvoker_TypeCode_t2305/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m32226_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5356_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m32222_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32223_MethodInfo,
	&InternalEnumerator_1_Dispose_m32224_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32225_MethodInfo,
	&InternalEnumerator_1_get_Current_m32226_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5356_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32223_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32225_MethodInfo,
	&InternalEnumerator_1_Dispose_m32224_MethodInfo,
	&InternalEnumerator_1_get_Current_m32226_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5356_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7530_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5356_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7530_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5356_0_0_0;
extern Il2CppType InternalEnumerator_1_t5356_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5356_GenericClass;
TypeInfo InternalEnumerator_1_t5356_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5356_MethodInfos/* methods */
	, InternalEnumerator_1_t5356_PropertyInfos/* properties */
	, InternalEnumerator_1_t5356_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5356_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5356_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5356_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5356_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5356_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5356_1_0_0/* this_arg */
	, InternalEnumerator_1_t5356_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5356_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5356)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9702_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.TypeCode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.TypeCode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.TypeCode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.TypeCode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.TypeCode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.TypeCode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.TypeCode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.TypeCode>
extern MethodInfo ICollection_1_get_Count_m53838_MethodInfo;
static PropertyInfo ICollection_1_t9702____Count_PropertyInfo = 
{
	&ICollection_1_t9702_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53838_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53839_MethodInfo;
static PropertyInfo ICollection_1_t9702____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9702_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53839_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9702_PropertyInfos[] =
{
	&ICollection_1_t9702____Count_PropertyInfo,
	&ICollection_1_t9702____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53838_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.TypeCode>::get_Count()
MethodInfo ICollection_1_get_Count_m53838_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9702_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53838_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53839_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.TypeCode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53839_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9702_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53839_GenericMethod/* genericMethod */

};
extern Il2CppType TypeCode_t2305_0_0_0;
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo ICollection_1_t9702_ICollection_1_Add_m53840_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53840_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.TypeCode>::Add(T)
MethodInfo ICollection_1_Add_m53840_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9702_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9702_ICollection_1_Add_m53840_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53840_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53841_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.TypeCode>::Clear()
MethodInfo ICollection_1_Clear_m53841_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9702_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53841_GenericMethod/* genericMethod */

};
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo ICollection_1_t9702_ICollection_1_Contains_m53842_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53842_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.TypeCode>::Contains(T)
MethodInfo ICollection_1_Contains_m53842_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9702_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9702_ICollection_1_Contains_m53842_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53842_GenericMethod/* genericMethod */

};
extern Il2CppType TypeCodeU5BU5D_t5618_0_0_0;
extern Il2CppType TypeCodeU5BU5D_t5618_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9702_ICollection_1_CopyTo_m53843_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeCodeU5BU5D_t5618_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53843_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.TypeCode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53843_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9702_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9702_ICollection_1_CopyTo_m53843_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53843_GenericMethod/* genericMethod */

};
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo ICollection_1_t9702_ICollection_1_Remove_m53844_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53844_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.TypeCode>::Remove(T)
MethodInfo ICollection_1_Remove_m53844_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9702_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9702_ICollection_1_Remove_m53844_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53844_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9702_MethodInfos[] =
{
	&ICollection_1_get_Count_m53838_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53839_MethodInfo,
	&ICollection_1_Add_m53840_MethodInfo,
	&ICollection_1_Clear_m53841_MethodInfo,
	&ICollection_1_Contains_m53842_MethodInfo,
	&ICollection_1_CopyTo_m53843_MethodInfo,
	&ICollection_1_Remove_m53844_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9704_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9702_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9704_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9702_0_0_0;
extern Il2CppType ICollection_1_t9702_1_0_0;
struct ICollection_1_t9702;
extern Il2CppGenericClass ICollection_1_t9702_GenericClass;
TypeInfo ICollection_1_t9702_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9702_MethodInfos/* methods */
	, ICollection_1_t9702_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9702_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9702_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9702_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9702_0_0_0/* byval_arg */
	, &ICollection_1_t9702_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9702_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.TypeCode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.TypeCode>
extern Il2CppType IEnumerator_1_t7530_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53845_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.TypeCode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53845_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9704_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7530_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53845_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9704_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53845_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9704_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9704_0_0_0;
extern Il2CppType IEnumerable_1_t9704_1_0_0;
struct IEnumerable_1_t9704;
extern Il2CppGenericClass IEnumerable_1_t9704_GenericClass;
TypeInfo IEnumerable_1_t9704_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9704_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9704_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9704_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9704_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9704_0_0_0/* byval_arg */
	, &IEnumerable_1_t9704_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9704_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9703_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.TypeCode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.TypeCode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.TypeCode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.TypeCode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.TypeCode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.TypeCode>
extern MethodInfo IList_1_get_Item_m53846_MethodInfo;
extern MethodInfo IList_1_set_Item_m53847_MethodInfo;
static PropertyInfo IList_1_t9703____Item_PropertyInfo = 
{
	&IList_1_t9703_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53846_MethodInfo/* get */
	, &IList_1_set_Item_m53847_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9703_PropertyInfos[] =
{
	&IList_1_t9703____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo IList_1_t9703_IList_1_IndexOf_m53848_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53848_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.TypeCode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53848_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9703_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9703_IList_1_IndexOf_m53848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53848_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo IList_1_t9703_IList_1_Insert_m53849_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53849_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.TypeCode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53849_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9703_IList_1_Insert_m53849_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53849_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9703_IList_1_RemoveAt_m53850_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53850_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.TypeCode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53850_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9703_IList_1_RemoveAt_m53850_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53850_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9703_IList_1_get_Item_m53846_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TypeCode_t2305_0_0_0;
extern void* RuntimeInvoker_TypeCode_t2305_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53846_GenericMethod;
// T System.Collections.Generic.IList`1<System.TypeCode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53846_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9703_il2cpp_TypeInfo/* declaring_type */
	, &TypeCode_t2305_0_0_0/* return_type */
	, RuntimeInvoker_TypeCode_t2305_Int32_t123/* invoker_method */
	, IList_1_t9703_IList_1_get_Item_m53846_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53846_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo IList_1_t9703_IList_1_set_Item_m53847_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53847_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.TypeCode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53847_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9703_IList_1_set_Item_m53847_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53847_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9703_MethodInfos[] =
{
	&IList_1_IndexOf_m53848_MethodInfo,
	&IList_1_Insert_m53849_MethodInfo,
	&IList_1_RemoveAt_m53850_MethodInfo,
	&IList_1_get_Item_m53846_MethodInfo,
	&IList_1_set_Item_m53847_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9703_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9702_il2cpp_TypeInfo,
	&IEnumerable_1_t9704_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9703_0_0_0;
extern Il2CppType IList_1_t9703_1_0_0;
struct IList_1_t9703;
extern Il2CppGenericClass IList_1_t9703_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9703_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9703_MethodInfos/* methods */
	, IList_1_t9703_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9703_il2cpp_TypeInfo/* element_class */
	, IList_1_t9703_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9703_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9703_0_0_0/* byval_arg */
	, &IList_1_t9703_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9703_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7532_il2cpp_TypeInfo;

// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityType.h"


// T System.Collections.Generic.IEnumerator`1<System.UnitySerializationHolder/UnityType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.UnitySerializationHolder/UnityType>
extern MethodInfo IEnumerator_1_get_Current_m53851_MethodInfo;
static PropertyInfo IEnumerator_1_t7532____Current_PropertyInfo = 
{
	&IEnumerator_1_t7532_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m53851_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7532_PropertyInfos[] =
{
	&IEnumerator_1_t7532____Current_PropertyInfo,
	NULL
};
extern Il2CppType UnityType_t2309_0_0_0;
extern void* RuntimeInvoker_UnityType_t2309 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m53851_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.UnitySerializationHolder/UnityType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m53851_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7532_il2cpp_TypeInfo/* declaring_type */
	, &UnityType_t2309_0_0_0/* return_type */
	, RuntimeInvoker_UnityType_t2309/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m53851_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7532_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m53851_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7532_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7532_0_0_0;
extern Il2CppType IEnumerator_1_t7532_1_0_0;
struct IEnumerator_1_t7532;
extern Il2CppGenericClass IEnumerator_1_t7532_GenericClass;
TypeInfo IEnumerator_1_t7532_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7532_MethodInfos/* methods */
	, IEnumerator_1_t7532_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7532_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7532_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7532_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7532_0_0_0/* byval_arg */
	, &IEnumerator_1_t7532_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7532_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_782.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5357_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_782MethodDeclarations.h"

extern TypeInfo UnityType_t2309_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m32231_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUnityType_t2309_m42339_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.UnitySerializationHolder/UnityType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.UnitySerializationHolder/UnityType>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisUnityType_t2309_m42339 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m32227_MethodInfo;
 void InternalEnumerator_1__ctor_m32227 (InternalEnumerator_1_t5357 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32228_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32228 (InternalEnumerator_1_t5357 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m32231(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m32231_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m32229_MethodInfo;
 void InternalEnumerator_1_Dispose_m32229 (InternalEnumerator_1_t5357 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m32230_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m32230 (InternalEnumerator_1_t5357 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m32231 (InternalEnumerator_1_t5357 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisUnityType_t2309_m42339(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisUnityType_t2309_m42339_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5357____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5357_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5357, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5357____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5357_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5357, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5357_FieldInfos[] =
{
	&InternalEnumerator_1_t5357____array_0_FieldInfo,
	&InternalEnumerator_1_t5357____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5357____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5357_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32228_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5357____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5357_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m32231_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5357_PropertyInfos[] =
{
	&InternalEnumerator_1_t5357____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5357____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5357_InternalEnumerator_1__ctor_m32227_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m32227_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m32227_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m32227/* method */
	, &InternalEnumerator_1_t5357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5357_InternalEnumerator_1__ctor_m32227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m32227_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32228_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32228_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32228/* method */
	, &InternalEnumerator_1_t5357_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32228_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m32229_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m32229_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m32229/* method */
	, &InternalEnumerator_1_t5357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m32229_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m32230_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m32230_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m32230/* method */
	, &InternalEnumerator_1_t5357_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m32230_GenericMethod/* genericMethod */

};
extern Il2CppType UnityType_t2309_0_0_0;
extern void* RuntimeInvoker_UnityType_t2309 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m32231_GenericMethod;
// T System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m32231_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m32231/* method */
	, &InternalEnumerator_1_t5357_il2cpp_TypeInfo/* declaring_type */
	, &UnityType_t2309_0_0_0/* return_type */
	, RuntimeInvoker_UnityType_t2309/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m32231_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5357_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m32227_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32228_MethodInfo,
	&InternalEnumerator_1_Dispose_m32229_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32230_MethodInfo,
	&InternalEnumerator_1_get_Current_m32231_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5357_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32228_MethodInfo,
	&InternalEnumerator_1_MoveNext_m32230_MethodInfo,
	&InternalEnumerator_1_Dispose_m32229_MethodInfo,
	&InternalEnumerator_1_get_Current_m32231_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5357_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7532_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5357_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7532_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5357_0_0_0;
extern Il2CppType InternalEnumerator_1_t5357_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5357_GenericClass;
TypeInfo InternalEnumerator_1_t5357_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5357_MethodInfos/* methods */
	, InternalEnumerator_1_t5357_PropertyInfos/* properties */
	, InternalEnumerator_1_t5357_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5357_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5357_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5357_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5357_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5357_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5357_1_0_0/* this_arg */
	, InternalEnumerator_1_t5357_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5357_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5357)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9705_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>
extern MethodInfo ICollection_1_get_Count_m53852_MethodInfo;
static PropertyInfo ICollection_1_t9705____Count_PropertyInfo = 
{
	&ICollection_1_t9705_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m53852_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m53853_MethodInfo;
static PropertyInfo ICollection_1_t9705____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9705_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m53853_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9705_PropertyInfos[] =
{
	&ICollection_1_t9705____Count_PropertyInfo,
	&ICollection_1_t9705____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m53852_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::get_Count()
MethodInfo ICollection_1_get_Count_m53852_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9705_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m53852_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m53853_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m53853_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9705_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m53853_GenericMethod/* genericMethod */

};
extern Il2CppType UnityType_t2309_0_0_0;
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo ICollection_1_t9705_ICollection_1_Add_m53854_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m53854_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::Add(T)
MethodInfo ICollection_1_Add_m53854_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9705_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Byte_t838/* invoker_method */
	, ICollection_1_t9705_ICollection_1_Add_m53854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m53854_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m53855_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::Clear()
MethodInfo ICollection_1_Clear_m53855_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9705_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m53855_GenericMethod/* genericMethod */

};
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo ICollection_1_t9705_ICollection_1_Contains_m53856_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m53856_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::Contains(T)
MethodInfo ICollection_1_Contains_m53856_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9705_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9705_ICollection_1_Contains_m53856_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m53856_GenericMethod/* genericMethod */

};
extern Il2CppType UnityTypeU5BU5D_t5619_0_0_0;
extern Il2CppType UnityTypeU5BU5D_t5619_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9705_ICollection_1_CopyTo_m53857_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UnityTypeU5BU5D_t5619_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m53857_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m53857_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9705_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9705_ICollection_1_CopyTo_m53857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m53857_GenericMethod/* genericMethod */

};
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo ICollection_1_t9705_ICollection_1_Remove_m53858_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m53858_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.UnitySerializationHolder/UnityType>::Remove(T)
MethodInfo ICollection_1_Remove_m53858_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9705_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, ICollection_1_t9705_ICollection_1_Remove_m53858_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m53858_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9705_MethodInfos[] =
{
	&ICollection_1_get_Count_m53852_MethodInfo,
	&ICollection_1_get_IsReadOnly_m53853_MethodInfo,
	&ICollection_1_Add_m53854_MethodInfo,
	&ICollection_1_Clear_m53855_MethodInfo,
	&ICollection_1_Contains_m53856_MethodInfo,
	&ICollection_1_CopyTo_m53857_MethodInfo,
	&ICollection_1_Remove_m53858_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9707_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9705_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9707_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9705_0_0_0;
extern Il2CppType ICollection_1_t9705_1_0_0;
struct ICollection_1_t9705;
extern Il2CppGenericClass ICollection_1_t9705_GenericClass;
TypeInfo ICollection_1_t9705_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9705_MethodInfos/* methods */
	, ICollection_1_t9705_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9705_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9705_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9705_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9705_0_0_0/* byval_arg */
	, &ICollection_1_t9705_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9705_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.UnitySerializationHolder/UnityType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.UnitySerializationHolder/UnityType>
extern Il2CppType IEnumerator_1_t7532_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m53859_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.UnitySerializationHolder/UnityType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m53859_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9707_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7532_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m53859_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9707_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m53859_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9707_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9707_0_0_0;
extern Il2CppType IEnumerable_1_t9707_1_0_0;
struct IEnumerable_1_t9707;
extern Il2CppGenericClass IEnumerable_1_t9707_GenericClass;
TypeInfo IEnumerable_1_t9707_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9707_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9707_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9707_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9707_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9707_0_0_0/* byval_arg */
	, &IEnumerable_1_t9707_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9707_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9706_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>
extern MethodInfo IList_1_get_Item_m53860_MethodInfo;
extern MethodInfo IList_1_set_Item_m53861_MethodInfo;
static PropertyInfo IList_1_t9706____Item_PropertyInfo = 
{
	&IList_1_t9706_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m53860_MethodInfo/* get */
	, &IList_1_set_Item_m53861_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9706_PropertyInfos[] =
{
	&IList_1_t9706____Item_PropertyInfo,
	NULL
};
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo IList_1_t9706_IList_1_IndexOf_m53862_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m53862_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m53862_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9706_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9706_IList_1_IndexOf_m53862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m53862_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo IList_1_t9706_IList_1_Insert_m53863_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m53863_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m53863_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9706_IList_1_Insert_m53863_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m53863_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9706_IList_1_RemoveAt_m53864_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m53864_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m53864_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9706_IList_1_RemoveAt_m53864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m53864_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9706_IList_1_get_Item_m53860_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType UnityType_t2309_0_0_0;
extern void* RuntimeInvoker_UnityType_t2309_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m53860_GenericMethod;
// T System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m53860_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9706_il2cpp_TypeInfo/* declaring_type */
	, &UnityType_t2309_0_0_0/* return_type */
	, RuntimeInvoker_UnityType_t2309_Int32_t123/* invoker_method */
	, IList_1_t9706_IList_1_get_Item_m53860_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m53860_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo IList_1_t9706_IList_1_set_Item_m53861_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m53861_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.UnitySerializationHolder/UnityType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m53861_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, IList_1_t9706_IList_1_set_Item_m53861_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m53861_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9706_MethodInfos[] =
{
	&IList_1_IndexOf_m53862_MethodInfo,
	&IList_1_Insert_m53863_MethodInfo,
	&IList_1_RemoveAt_m53864_MethodInfo,
	&IList_1_get_Item_m53860_MethodInfo,
	&IList_1_set_Item_m53861_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9706_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9705_il2cpp_TypeInfo,
	&IEnumerable_1_t9707_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9706_0_0_0;
extern Il2CppType IList_1_t9706_1_0_0;
struct IList_1_t9706;
extern Il2CppGenericClass IList_1_t9706_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9706_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9706_MethodInfos/* methods */
	, IList_1_t9706_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9706_il2cpp_TypeInfo/* element_class */
	, IList_1_t9706_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9706_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9706_0_0_0/* byval_arg */
	, &IList_1_t9706_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9706_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t2744_il2cpp_TypeInfo;

// System.Version
#include "mscorlib_System_Version.h"


// System.Int32 System.IComparable`1<System.Version>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.Version>
extern Il2CppType Version_t1386_0_0_0;
extern Il2CppType Version_t1386_0_0_0;
static ParameterInfo IComparable_1_t2744_IComparable_1_CompareTo_m53865_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Version_t1386_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m53865_GenericMethod;
// System.Int32 System.IComparable`1<System.Version>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m53865_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t2744_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IComparable_1_t2744_IComparable_1_CompareTo_m53865_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m53865_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t2744_MethodInfos[] =
{
	&IComparable_1_CompareTo_m53865_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t2744_0_0_0;
extern Il2CppType IComparable_1_t2744_1_0_0;
struct IComparable_1_t2744;
extern Il2CppGenericClass IComparable_1_t2744_GenericClass;
TypeInfo IComparable_1_t2744_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t2744_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2744_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t2744_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t2744_0_0_0/* byval_arg */
	, &IComparable_1_t2744_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t2744_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t2745_il2cpp_TypeInfo;



// System.Boolean System.IEquatable`1<System.Version>::Equals(T)
// Metadata Definition System.IEquatable`1<System.Version>
extern Il2CppType Version_t1386_0_0_0;
static ParameterInfo IEquatable_1_t2745_IEquatable_1_Equals_m53866_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Version_t1386_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m53866_GenericMethod;
// System.Boolean System.IEquatable`1<System.Version>::Equals(T)
MethodInfo IEquatable_1_Equals_m53866_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t2745_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, IEquatable_1_t2745_IEquatable_1_Equals_m53866_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m53866_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t2745_MethodInfos[] =
{
	&IEquatable_1_Equals_m53866_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t2745_0_0_0;
extern Il2CppType IEquatable_1_t2745_1_0_0;
struct IEquatable_1_t2745;
extern Il2CppGenericClass IEquatable_1_t2745_GenericClass;
TypeInfo IEquatable_1_t2745_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t2745_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t2745_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t2745_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t2745_0_0_0/* byval_arg */
	, &IEquatable_1_t2745_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t2745_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
