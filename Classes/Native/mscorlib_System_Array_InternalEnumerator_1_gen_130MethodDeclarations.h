﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<InGameState_Script/InGameState>
struct InternalEnumerator_1_t3064;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// InGameState_Script/InGameState
#include "AssemblyU2DCSharp_InGameState_Script_InGameState.h"

// System.Void System.Array/InternalEnumerator`1<InGameState_Script/InGameState>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m15584 (InternalEnumerator_1_t3064 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<InGameState_Script/InGameState>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15585 (InternalEnumerator_1_t3064 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<InGameState_Script/InGameState>::Dispose()
 void InternalEnumerator_1_Dispose_m15586 (InternalEnumerator_1_t3064 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<InGameState_Script/InGameState>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m15587 (InternalEnumerator_1_t3064 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<InGameState_Script/InGameState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m15588 (InternalEnumerator_1_t3064 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
