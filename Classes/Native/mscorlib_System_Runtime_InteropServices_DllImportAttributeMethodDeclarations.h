﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.DllImportAttribute
struct DllImportAttribute_t1762;
// System.String
struct String_t;

// System.Void System.Runtime.InteropServices.DllImportAttribute::.ctor(System.String)
 void DllImportAttribute__ctor_m9911 (DllImportAttribute_t1762 * __this, String_t* ___dllName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.DllImportAttribute::get_Value()
 String_t* DllImportAttribute_get_Value_m9912 (DllImportAttribute_t1762 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
