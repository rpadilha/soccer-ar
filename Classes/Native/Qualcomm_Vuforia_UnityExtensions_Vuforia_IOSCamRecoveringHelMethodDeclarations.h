﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.IOSCamRecoveringHelper
struct IOSCamRecoveringHelper_t592;

// System.Void Vuforia.IOSCamRecoveringHelper::SetHasJustResumed()
 void IOSCamRecoveringHelper_SetHasJustResumed_m2791 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.IOSCamRecoveringHelper::TryToRecover()
 bool IOSCamRecoveringHelper_TryToRecover_m2792 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSCamRecoveringHelper::SetSuccessfullyRecovered()
 void IOSCamRecoveringHelper_SetSuccessfullyRecovered_m2793 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSCamRecoveringHelper::.cctor()
 void IOSCamRecoveringHelper__cctor_m2794 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
