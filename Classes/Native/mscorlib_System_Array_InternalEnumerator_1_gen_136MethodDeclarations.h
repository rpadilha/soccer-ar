﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Player_Script/Player_State>
struct InternalEnumerator_1_t3097;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Player_Script/Player_State
#include "AssemblyU2DCSharp_Player_Script_Player_State.h"

// System.Void System.Array/InternalEnumerator`1<Player_Script/Player_State>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m15783 (InternalEnumerator_1_t3097 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Player_Script/Player_State>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15784 (InternalEnumerator_1_t3097 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Player_Script/Player_State>::Dispose()
 void InternalEnumerator_1_Dispose_m15785 (InternalEnumerator_1_t3097 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Player_Script/Player_State>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m15786 (InternalEnumerator_1_t3097 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Player_Script/Player_State>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m15787 (InternalEnumerator_1_t3097 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
