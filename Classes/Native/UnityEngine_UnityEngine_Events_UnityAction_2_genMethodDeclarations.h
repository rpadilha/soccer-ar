﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t4987;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
 void UnityAction_2__ctor_m30121_gshared (UnityAction_2_t4987 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method);
#define UnityAction_2__ctor_m30121(__this, ___object, ___method, method) (void)UnityAction_2__ctor_m30121_gshared((UnityAction_2_t4987 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
 void UnityAction_2_Invoke_m30122_gshared (UnityAction_2_t4987 * __this, Object_t * ___arg0, Object_t * ___arg1, MethodInfo* method);
#define UnityAction_2_Invoke_m30122(__this, ___arg0, ___arg1, method) (void)UnityAction_2_Invoke_m30122_gshared((UnityAction_2_t4987 *)__this, (Object_t *)___arg0, (Object_t *)___arg1, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,System.Object>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
 Object_t * UnityAction_2_BeginInvoke_m30123_gshared (UnityAction_2_t4987 * __this, Object_t * ___arg0, Object_t * ___arg1, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method);
#define UnityAction_2_BeginInvoke_m30123(__this, ___arg0, ___arg1, ___callback, ___object, method) (Object_t *)UnityAction_2_BeginInvoke_m30123_gshared((UnityAction_2_t4987 *)__this, (Object_t *)___arg0, (Object_t *)___arg1, (AsyncCallback_t251 *)___callback, (Object_t *)___object, method)
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
 void UnityAction_2_EndInvoke_m30124_gshared (UnityAction_2_t4987 * __this, Object_t * ___result, MethodInfo* method);
#define UnityAction_2_EndInvoke_m30124(__this, ___result, method) (void)UnityAction_2_EndInvoke_m30124_gshared((UnityAction_2_t4987 *)__this, (Object_t *)___result, method)
