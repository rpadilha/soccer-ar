﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t4043;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t499;

// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::.ctor()
 void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m22741_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m22741(__this, method) (void)U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m22741_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
 Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m22742_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m22742(__this, method) (Object_t *)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m22742_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m22743_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m22743(__this, method) (Object_t *)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m22743_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m22744_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m22744(__this, method) (Object_t *)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m22744_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
 Object_t* U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m22745_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m22745(__this, method) (Object_t*)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m22745_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::MoveNext()
 bool U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m22746_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m22746(__this, method) (bool)U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m22746_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Dispose()
 void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m22747_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m22747(__this, method) (void)U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m22747_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t4043 *)__this, method)
