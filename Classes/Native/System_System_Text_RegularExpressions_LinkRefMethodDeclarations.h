﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.LinkRef
struct LinkRef_t1473;

// System.Void System.Text.RegularExpressions.LinkRef::.ctor()
 void LinkRef__ctor_m7421 (LinkRef_t1473 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
