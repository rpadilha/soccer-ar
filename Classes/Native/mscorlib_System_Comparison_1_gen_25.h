﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButton
struct VirtualButton_t639;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.VirtualButton>
struct Comparison_1_t3929  : public MulticastDelegate_t373
{
};
