﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_t814;
// Vuforia.IVirtualButtonEventHandler
struct IVirtualButtonEventHandler_t815;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>
struct Enumerator_t947 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::l
	List_1_t814 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::current
	Object_t * ___current_3;
};
