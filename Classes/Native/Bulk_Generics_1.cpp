﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_2.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo InvokableCall_1_t2779_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_2MethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_9.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
#include "mscorlib_ArrayTypes.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
extern TypeInfo UnityAction_1_t2780_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
extern TypeInfo CylinderTargetBehaviour_t5_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_9MethodDeclarations.h"
extern Il2CppType UnityAction_1_t2780_0_0_0;
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCylinderTargetBehaviour_t5_m32433_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo UnityAction_1_Invoke_m14282_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.CylinderTargetBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.CylinderTargetBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisCylinderTargetBehaviour_t5_m32433(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)

// System.Array
#include "mscorlib_System_Array.h"

// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>
extern Il2CppType UnityAction_1_t2780_0_0_1;
FieldInfo InvokableCall_1_t2779____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2780_0_0_1/* type */
	, &InvokableCall_1_t2779_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2779, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2779_FieldInfos[] =
{
	&InvokableCall_1_t2779____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2779_InvokableCall_1__ctor_m14277_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14277_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14277_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2779_InvokableCall_1__ctor_m14277_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14277_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2780_0_0_0;
static ParameterInfo InvokableCall_1_t2779_InvokableCall_1__ctor_m14278_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2780_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14278_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14278_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2779_InvokableCall_1__ctor_m14278_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14278_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2779_InvokableCall_1_Invoke_m14279_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14279_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14279_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2779_InvokableCall_1_Invoke_m14279_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14279_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2779_InvokableCall_1_Find_m14280_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14280_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14280_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2779_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2779_InvokableCall_1_Find_m14280_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14280_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2779_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14277_MethodInfo,
	&InvokableCall_1__ctor_m14278_MethodInfo,
	&InvokableCall_1_Invoke_m14279_MethodInfo,
	&InvokableCall_1_Find_m14280_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14279_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14280_MethodInfo;
static MethodInfo* InvokableCall_1_t2779_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14279_MethodInfo,
	&InvokableCall_1_Find_m14280_MethodInfo,
};
extern TypeInfo UnityAction_1_t2780_il2cpp_TypeInfo;
extern TypeInfo CylinderTargetBehaviour_t5_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2779_RGCTXData[5] = 
{
	&UnityAction_1_t2780_0_0_0/* Type Usage */,
	&UnityAction_1_t2780_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCylinderTargetBehaviour_t5_m32433_MethodInfo/* Method Usage */,
	&CylinderTargetBehaviour_t5_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14282_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2779_0_0_0;
extern Il2CppType InvokableCall_1_t2779_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t2779;
extern Il2CppGenericClass InvokableCall_1_t2779_GenericClass;
TypeInfo InvokableCall_1_t2779_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2779_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2779_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2779_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2779_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2779_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2779_0_0_0/* byval_arg */
	, &InvokableCall_1_t2779_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2779_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2779_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2779)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2780_UnityAction_1__ctor_m14281_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14281_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14281_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2780_UnityAction_1__ctor_m14281_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14281_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
static ParameterInfo UnityAction_1_t2780_UnityAction_1_Invoke_m14282_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t5_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14282_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14282_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2780_UnityAction_1_Invoke_m14282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14282_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t5_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2780_UnityAction_1_BeginInvoke_m14283_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t5_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14283_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14283_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2780_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2780_UnityAction_1_BeginInvoke_m14283_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14283_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2780_UnityAction_1_EndInvoke_m14284_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14284_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14284_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2780_UnityAction_1_EndInvoke_m14284_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14284_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2780_MethodInfos[] =
{
	&UnityAction_1__ctor_m14281_MethodInfo,
	&UnityAction_1_Invoke_m14282_MethodInfo,
	&UnityAction_1_BeginInvoke_m14283_MethodInfo,
	&UnityAction_1_EndInvoke_m14284_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m14283_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14284_MethodInfo;
static MethodInfo* UnityAction_1_t2780_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14282_MethodInfo,
	&UnityAction_1_BeginInvoke_m14283_MethodInfo,
	&UnityAction_1_EndInvoke_m14284_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2780_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2780_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_1_t2780;
extern Il2CppGenericClass UnityAction_1_t2780_GenericClass;
TypeInfo UnityAction_1_t2780_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2780_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2780_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2780_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2780_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2780_0_0_0/* byval_arg */
	, &UnityAction_1_t2780_1_0_0/* this_arg */
	, UnityAction_1_t2780_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2780_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2780)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5961_il2cpp_TypeInfo;

// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42604_MethodInfo;
static PropertyInfo IEnumerator_1_t5961____Current_PropertyInfo = 
{
	&IEnumerator_1_t5961_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42604_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5961_PropertyInfos[] =
{
	&IEnumerator_1_t5961____Current_PropertyInfo,
	NULL
};
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42604_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42604_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5961_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadBehaviour_t7_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42604_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5961_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42604_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t5961_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5961_0_0_0;
extern Il2CppType IEnumerator_1_t5961_1_0_0;
struct IEnumerator_1_t5961;
extern Il2CppGenericClass IEnumerator_1_t5961_GenericClass;
TypeInfo IEnumerator_1_t5961_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5961_MethodInfos/* methods */
	, IEnumerator_1_t5961_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5961_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5961_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5961_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5961_0_0_0/* byval_arg */
	, &IEnumerator_1_t5961_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5961_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_17.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2781_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_17MethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo DataSetLoadBehaviour_t7_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m14289_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDataSetLoadBehaviour_t7_m32435_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetLoadBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetLoadBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisDataSetLoadBehaviour_t7_m32435(__this, p0, method) (DataSetLoadBehaviour_t7 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2781____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2781, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2781____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2781, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2781_FieldInfos[] =
{
	&InternalEnumerator_1_t2781____array_0_FieldInfo,
	&InternalEnumerator_1_t2781____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14286_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2781____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2781_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14286_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2781____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2781_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14289_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2781_PropertyInfos[] =
{
	&InternalEnumerator_1_t2781____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2781____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2781_InternalEnumerator_1__ctor_m14285_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14285_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14285_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2781_InternalEnumerator_1__ctor_m14285_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14285_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14286_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14286_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14286_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14287_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14287_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14287_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14288_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14288_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14288_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14289_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14289_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadBehaviour_t7_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14289_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2781_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14285_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14286_MethodInfo,
	&InternalEnumerator_1_Dispose_m14287_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14288_MethodInfo,
	&InternalEnumerator_1_get_Current_m14289_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m14288_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14287_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2781_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14286_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14288_MethodInfo,
	&InternalEnumerator_1_Dispose_m14287_MethodInfo,
	&InternalEnumerator_1_get_Current_m14289_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2781_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5961_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2781_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5961_il2cpp_TypeInfo, 7},
};
extern TypeInfo DataSetLoadBehaviour_t7_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2781_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14289_MethodInfo/* Method Usage */,
	&DataSetLoadBehaviour_t7_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDataSetLoadBehaviour_t7_m32435_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2781_0_0_0;
extern Il2CppType InternalEnumerator_1_t2781_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2781_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2781_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2781_MethodInfos/* methods */
	, InternalEnumerator_1_t2781_PropertyInfos/* properties */
	, InternalEnumerator_1_t2781_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2781_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2781_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2781_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2781_1_0_0/* this_arg */
	, InternalEnumerator_1_t2781_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2781_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2781_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2781)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7575_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>
extern MethodInfo ICollection_1_get_Count_m42605_MethodInfo;
static PropertyInfo ICollection_1_t7575____Count_PropertyInfo = 
{
	&ICollection_1_t7575_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42605_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42606_MethodInfo;
static PropertyInfo ICollection_1_t7575____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7575_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42606_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7575_PropertyInfos[] =
{
	&ICollection_1_t7575____Count_PropertyInfo,
	&ICollection_1_t7575____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42605_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42605_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7575_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42605_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42606_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42606_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7575_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42606_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
static ParameterInfo ICollection_1_t7575_ICollection_1_Add_m42607_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t7_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42607_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42607_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7575_ICollection_1_Add_m42607_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42607_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42608_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42608_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42608_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
static ParameterInfo ICollection_1_t7575_ICollection_1_Contains_m42609_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t7_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42609_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42609_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7575_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7575_ICollection_1_Contains_m42609_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42609_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviourU5BU5D_t5361_0_0_0;
extern Il2CppType DataSetLoadBehaviourU5BU5D_t5361_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7575_ICollection_1_CopyTo_m42610_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviourU5BU5D_t5361_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42610_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42610_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7575_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7575_ICollection_1_CopyTo_m42610_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42610_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
static ParameterInfo ICollection_1_t7575_ICollection_1_Remove_m42611_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t7_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42611_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42611_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7575_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7575_ICollection_1_Remove_m42611_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42611_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7575_MethodInfos[] =
{
	&ICollection_1_get_Count_m42605_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42606_MethodInfo,
	&ICollection_1_Add_m42607_MethodInfo,
	&ICollection_1_Clear_m42608_MethodInfo,
	&ICollection_1_Contains_m42609_MethodInfo,
	&ICollection_1_CopyTo_m42610_MethodInfo,
	&ICollection_1_Remove_m42611_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7577_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7575_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7577_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7575_0_0_0;
extern Il2CppType ICollection_1_t7575_1_0_0;
struct ICollection_1_t7575;
extern Il2CppGenericClass ICollection_1_t7575_GenericClass;
TypeInfo ICollection_1_t7575_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7575_MethodInfos/* methods */
	, ICollection_1_t7575_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7575_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7575_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7575_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7575_0_0_0/* byval_arg */
	, &ICollection_1_t7575_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7575_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadBehaviour>
extern Il2CppType IEnumerator_1_t5961_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42612_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42612_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7577_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5961_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42612_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7577_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42612_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7577_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7577_0_0_0;
extern Il2CppType IEnumerable_1_t7577_1_0_0;
struct IEnumerable_1_t7577;
extern Il2CppGenericClass IEnumerable_1_t7577_GenericClass;
TypeInfo IEnumerable_1_t7577_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7577_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7577_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7577_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7577_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7577_0_0_0/* byval_arg */
	, &IEnumerable_1_t7577_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7577_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7576_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>
extern MethodInfo IList_1_get_Item_m42613_MethodInfo;
extern MethodInfo IList_1_set_Item_m42614_MethodInfo;
static PropertyInfo IList_1_t7576____Item_PropertyInfo = 
{
	&IList_1_t7576_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42613_MethodInfo/* get */
	, &IList_1_set_Item_m42614_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7576_PropertyInfos[] =
{
	&IList_1_t7576____Item_PropertyInfo,
	NULL
};
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
static ParameterInfo IList_1_t7576_IList_1_IndexOf_m42615_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t7_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42615_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42615_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7576_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7576_IList_1_IndexOf_m42615_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42615_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
static ParameterInfo IList_1_t7576_IList_1_Insert_m42616_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t7_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42616_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42616_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7576_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7576_IList_1_Insert_m42616_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42616_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7576_IList_1_RemoveAt_m42617_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42617_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42617_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7576_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7576_IList_1_RemoveAt_m42617_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42617_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7576_IList_1_get_Item_m42613_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42613_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42613_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7576_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadBehaviour_t7_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7576_IList_1_get_Item_m42613_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42613_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
static ParameterInfo IList_1_t7576_IList_1_set_Item_m42614_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t7_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42614_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42614_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7576_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7576_IList_1_set_Item_m42614_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42614_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7576_MethodInfos[] =
{
	&IList_1_IndexOf_m42615_MethodInfo,
	&IList_1_Insert_m42616_MethodInfo,
	&IList_1_RemoveAt_m42617_MethodInfo,
	&IList_1_get_Item_m42613_MethodInfo,
	&IList_1_set_Item_m42614_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7576_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7575_il2cpp_TypeInfo,
	&IEnumerable_1_t7577_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7576_0_0_0;
extern Il2CppType IList_1_t7576_1_0_0;
struct IList_1_t7576;
extern Il2CppGenericClass IList_1_t7576_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7576_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7576_MethodInfos/* methods */
	, IList_1_t7576_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7576_il2cpp_TypeInfo/* element_class */
	, IList_1_t7576_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7576_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7576_0_0_0/* byval_arg */
	, &IList_1_t7576_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7576_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7578_il2cpp_TypeInfo;

// Vuforia.DataSetLoadAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetLoadAbstract.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42618_MethodInfo;
static PropertyInfo ICollection_1_t7578____Count_PropertyInfo = 
{
	&ICollection_1_t7578_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42618_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42619_MethodInfo;
static PropertyInfo ICollection_1_t7578____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7578_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42619_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7578_PropertyInfos[] =
{
	&ICollection_1_t7578____Count_PropertyInfo,
	&ICollection_1_t7578____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42618_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42618_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7578_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42618_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42619_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42619_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7578_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42619_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadAbstractBehaviour_t8_0_0_0;
extern Il2CppType DataSetLoadAbstractBehaviour_t8_0_0_0;
static ParameterInfo ICollection_1_t7578_ICollection_1_Add_m42620_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t8_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42620_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42620_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7578_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7578_ICollection_1_Add_m42620_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42620_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42621_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42621_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7578_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42621_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadAbstractBehaviour_t8_0_0_0;
static ParameterInfo ICollection_1_t7578_ICollection_1_Contains_m42622_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t8_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42622_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42622_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7578_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7578_ICollection_1_Contains_m42622_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42622_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadAbstractBehaviourU5BU5D_t5626_0_0_0;
extern Il2CppType DataSetLoadAbstractBehaviourU5BU5D_t5626_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7578_ICollection_1_CopyTo_m42623_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviourU5BU5D_t5626_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42623_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42623_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7578_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7578_ICollection_1_CopyTo_m42623_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42623_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadAbstractBehaviour_t8_0_0_0;
static ParameterInfo ICollection_1_t7578_ICollection_1_Remove_m42624_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t8_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42624_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42624_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7578_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7578_ICollection_1_Remove_m42624_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42624_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7578_MethodInfos[] =
{
	&ICollection_1_get_Count_m42618_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42619_MethodInfo,
	&ICollection_1_Add_m42620_MethodInfo,
	&ICollection_1_Clear_m42621_MethodInfo,
	&ICollection_1_Contains_m42622_MethodInfo,
	&ICollection_1_CopyTo_m42623_MethodInfo,
	&ICollection_1_Remove_m42624_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7580_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7578_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7580_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7578_0_0_0;
extern Il2CppType ICollection_1_t7578_1_0_0;
struct ICollection_1_t7578;
extern Il2CppGenericClass ICollection_1_t7578_GenericClass;
TypeInfo ICollection_1_t7578_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7578_MethodInfos/* methods */
	, ICollection_1_t7578_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7578_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7578_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7578_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7578_0_0_0/* byval_arg */
	, &ICollection_1_t7578_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7578_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5963_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42625_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42625_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7580_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5963_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42625_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7580_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42625_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7580_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7580_0_0_0;
extern Il2CppType IEnumerable_1_t7580_1_0_0;
struct IEnumerable_1_t7580;
extern Il2CppGenericClass IEnumerable_1_t7580_GenericClass;
TypeInfo IEnumerable_1_t7580_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7580_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7580_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7580_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7580_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7580_0_0_0/* byval_arg */
	, &IEnumerable_1_t7580_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7580_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5963_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42626_MethodInfo;
static PropertyInfo IEnumerator_1_t5963____Current_PropertyInfo = 
{
	&IEnumerator_1_t5963_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42626_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5963_PropertyInfos[] =
{
	&IEnumerator_1_t5963____Current_PropertyInfo,
	NULL
};
extern Il2CppType DataSetLoadAbstractBehaviour_t8_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42626_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42626_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5963_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadAbstractBehaviour_t8_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42626_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5963_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42626_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5963_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5963_0_0_0;
extern Il2CppType IEnumerator_1_t5963_1_0_0;
struct IEnumerator_1_t5963;
extern Il2CppGenericClass IEnumerator_1_t5963_GenericClass;
TypeInfo IEnumerator_1_t5963_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5963_MethodInfos/* methods */
	, IEnumerator_1_t5963_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5963_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5963_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5963_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5963_0_0_0/* byval_arg */
	, &IEnumerator_1_t5963_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5963_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_18.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2782_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_18MethodDeclarations.h"

extern TypeInfo DataSetLoadAbstractBehaviour_t8_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14294_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDataSetLoadAbstractBehaviour_t8_m32446_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetLoadAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetLoadAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisDataSetLoadAbstractBehaviour_t8_m32446(__this, p0, method) (DataSetLoadAbstractBehaviour_t8 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2782____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2782_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2782, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2782____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2782_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2782, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2782_FieldInfos[] =
{
	&InternalEnumerator_1_t2782____array_0_FieldInfo,
	&InternalEnumerator_1_t2782____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14291_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2782____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2782_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14291_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2782____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2782_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14294_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2782_PropertyInfos[] =
{
	&InternalEnumerator_1_t2782____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2782____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2782_InternalEnumerator_1__ctor_m14290_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14290_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14290_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2782_InternalEnumerator_1__ctor_m14290_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14290_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14291_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14291_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2782_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14291_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14292_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14292_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14292_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14293_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14293_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2782_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14293_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadAbstractBehaviour_t8_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14294_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14294_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2782_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadAbstractBehaviour_t8_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14294_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2782_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14290_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14291_MethodInfo,
	&InternalEnumerator_1_Dispose_m14292_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14293_MethodInfo,
	&InternalEnumerator_1_get_Current_m14294_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14293_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14292_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2782_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14291_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14293_MethodInfo,
	&InternalEnumerator_1_Dispose_m14292_MethodInfo,
	&InternalEnumerator_1_get_Current_m14294_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2782_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5963_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2782_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5963_il2cpp_TypeInfo, 7},
};
extern TypeInfo DataSetLoadAbstractBehaviour_t8_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2782_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14294_MethodInfo/* Method Usage */,
	&DataSetLoadAbstractBehaviour_t8_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDataSetLoadAbstractBehaviour_t8_m32446_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2782_0_0_0;
extern Il2CppType InternalEnumerator_1_t2782_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2782_GenericClass;
TypeInfo InternalEnumerator_1_t2782_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2782_MethodInfos/* methods */
	, InternalEnumerator_1_t2782_PropertyInfos/* properties */
	, InternalEnumerator_1_t2782_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2782_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2782_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2782_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2782_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2782_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2782_1_0_0/* this_arg */
	, InternalEnumerator_1_t2782_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2782_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2782_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2782)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7579_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42627_MethodInfo;
extern MethodInfo IList_1_set_Item_m42628_MethodInfo;
static PropertyInfo IList_1_t7579____Item_PropertyInfo = 
{
	&IList_1_t7579_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42627_MethodInfo/* get */
	, &IList_1_set_Item_m42628_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7579_PropertyInfos[] =
{
	&IList_1_t7579____Item_PropertyInfo,
	NULL
};
extern Il2CppType DataSetLoadAbstractBehaviour_t8_0_0_0;
static ParameterInfo IList_1_t7579_IList_1_IndexOf_m42629_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t8_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42629_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42629_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7579_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7579_IList_1_IndexOf_m42629_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42629_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DataSetLoadAbstractBehaviour_t8_0_0_0;
static ParameterInfo IList_1_t7579_IList_1_Insert_m42630_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t8_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42630_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42630_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7579_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7579_IList_1_Insert_m42630_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42630_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7579_IList_1_RemoveAt_m42631_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42631_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42631_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7579_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7579_IList_1_RemoveAt_m42631_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42631_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7579_IList_1_get_Item_m42627_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DataSetLoadAbstractBehaviour_t8_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42627_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42627_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7579_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadAbstractBehaviour_t8_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7579_IList_1_get_Item_m42627_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42627_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DataSetLoadAbstractBehaviour_t8_0_0_0;
static ParameterInfo IList_1_t7579_IList_1_set_Item_m42628_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t8_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42628_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42628_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7579_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7579_IList_1_set_Item_m42628_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42628_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7579_MethodInfos[] =
{
	&IList_1_IndexOf_m42629_MethodInfo,
	&IList_1_Insert_m42630_MethodInfo,
	&IList_1_RemoveAt_m42631_MethodInfo,
	&IList_1_get_Item_m42627_MethodInfo,
	&IList_1_set_Item_m42628_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7579_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7578_il2cpp_TypeInfo,
	&IEnumerable_1_t7580_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7579_0_0_0;
extern Il2CppType IList_1_t7579_1_0_0;
struct IList_1_t7579;
extern Il2CppGenericClass IList_1_t7579_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7579_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7579_MethodInfos/* methods */
	, IList_1_t7579_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7579_il2cpp_TypeInfo/* element_class */
	, IList_1_t7579_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7579_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7579_0_0_0/* byval_arg */
	, &IList_1_t7579_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7579_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_7.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2783_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_7MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_3.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2784_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_3MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14297_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14299_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2783____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2783_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2783, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2783_FieldInfos[] =
{
	&CachedInvokableCall_1_t2783____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2783_CachedInvokableCall_1__ctor_m14295_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t7_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14295_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14295_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2783_CachedInvokableCall_1__ctor_m14295_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14295_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2783_CachedInvokableCall_1_Invoke_m14296_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14296_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14296_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2783_CachedInvokableCall_1_Invoke_m14296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14296_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2783_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14295_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14296_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14296_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14300_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2783_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14296_MethodInfo,
	&InvokableCall_1_Find_m14300_MethodInfo,
};
extern Il2CppType UnityAction_1_t2785_0_0_0;
extern TypeInfo UnityAction_1_t2785_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisDataSetLoadBehaviour_t7_m32456_MethodInfo;
extern TypeInfo DataSetLoadBehaviour_t7_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14302_MethodInfo;
extern TypeInfo DataSetLoadBehaviour_t7_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2783_RGCTXData[8] = 
{
	&UnityAction_1_t2785_0_0_0/* Type Usage */,
	&UnityAction_1_t2785_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDataSetLoadBehaviour_t7_m32456_MethodInfo/* Method Usage */,
	&DataSetLoadBehaviour_t7_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14302_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14297_MethodInfo/* Method Usage */,
	&DataSetLoadBehaviour_t7_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14299_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2783_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2783_1_0_0;
struct CachedInvokableCall_1_t2783;
extern Il2CppGenericClass CachedInvokableCall_1_t2783_GenericClass;
TypeInfo CachedInvokableCall_1_t2783_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2783_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2783_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2784_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2783_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2783_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2783_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2783_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2783_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2783_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2783_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2783)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_10.h"
extern TypeInfo UnityAction_1_t2785_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_10MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DataSetLoadBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DataSetLoadBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisDataSetLoadBehaviour_t7_m32456(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>
extern Il2CppType UnityAction_1_t2785_0_0_1;
FieldInfo InvokableCall_1_t2784____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2785_0_0_1/* type */
	, &InvokableCall_1_t2784_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2784, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2784_FieldInfos[] =
{
	&InvokableCall_1_t2784____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2784_InvokableCall_1__ctor_m14297_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14297_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14297_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2784_InvokableCall_1__ctor_m14297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14297_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2785_0_0_0;
static ParameterInfo InvokableCall_1_t2784_InvokableCall_1__ctor_m14298_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2785_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14298_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14298_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2784_InvokableCall_1__ctor_m14298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14298_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2784_InvokableCall_1_Invoke_m14299_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14299_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14299_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2784_InvokableCall_1_Invoke_m14299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14299_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2784_InvokableCall_1_Find_m14300_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14300_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14300_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2784_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2784_InvokableCall_1_Find_m14300_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14300_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2784_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14297_MethodInfo,
	&InvokableCall_1__ctor_m14298_MethodInfo,
	&InvokableCall_1_Invoke_m14299_MethodInfo,
	&InvokableCall_1_Find_m14300_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2784_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14299_MethodInfo,
	&InvokableCall_1_Find_m14300_MethodInfo,
};
extern TypeInfo UnityAction_1_t2785_il2cpp_TypeInfo;
extern TypeInfo DataSetLoadBehaviour_t7_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2784_RGCTXData[5] = 
{
	&UnityAction_1_t2785_0_0_0/* Type Usage */,
	&UnityAction_1_t2785_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDataSetLoadBehaviour_t7_m32456_MethodInfo/* Method Usage */,
	&DataSetLoadBehaviour_t7_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14302_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2784_0_0_0;
extern Il2CppType InvokableCall_1_t2784_1_0_0;
struct InvokableCall_1_t2784;
extern Il2CppGenericClass InvokableCall_1_t2784_GenericClass;
TypeInfo InvokableCall_1_t2784_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2784_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2784_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2784_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2784_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2784_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2784_0_0_0/* byval_arg */
	, &InvokableCall_1_t2784_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2784_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2784_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2784)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2785_UnityAction_1__ctor_m14301_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14301_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14301_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2785_UnityAction_1__ctor_m14301_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14301_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
static ParameterInfo UnityAction_1_t2785_UnityAction_1_Invoke_m14302_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t7_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14302_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14302_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2785_UnityAction_1_Invoke_m14302_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14302_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t7_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2785_UnityAction_1_BeginInvoke_m14303_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t7_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14303_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14303_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2785_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2785_UnityAction_1_BeginInvoke_m14303_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14303_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2785_UnityAction_1_EndInvoke_m14304_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14304_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14304_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2785_UnityAction_1_EndInvoke_m14304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14304_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2785_MethodInfos[] =
{
	&UnityAction_1__ctor_m14301_MethodInfo,
	&UnityAction_1_Invoke_m14302_MethodInfo,
	&UnityAction_1_BeginInvoke_m14303_MethodInfo,
	&UnityAction_1_EndInvoke_m14304_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14303_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14304_MethodInfo;
static MethodInfo* UnityAction_1_t2785_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14302_MethodInfo,
	&UnityAction_1_BeginInvoke_m14303_MethodInfo,
	&UnityAction_1_EndInvoke_m14304_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2785_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2785_1_0_0;
struct UnityAction_1_t2785;
extern Il2CppGenericClass UnityAction_1_t2785_GenericClass;
TypeInfo UnityAction_1_t2785_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2785_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2785_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2785_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2785_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2785_0_0_0/* byval_arg */
	, &UnityAction_1_t2785_1_0_0/* this_arg */
	, UnityAction_1_t2785_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2785_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2785)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5965_il2cpp_TypeInfo;

// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DefaultInitializationErrorHandler>
extern MethodInfo IEnumerator_1_get_Current_m42632_MethodInfo;
static PropertyInfo IEnumerator_1_t5965____Current_PropertyInfo = 
{
	&IEnumerator_1_t5965_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42632_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5965_PropertyInfos[] =
{
	&IEnumerator_1_t5965____Current_PropertyInfo,
	NULL
};
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42632_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42632_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5965_il2cpp_TypeInfo/* declaring_type */
	, &DefaultInitializationErrorHandler_t9_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42632_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5965_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42632_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5965_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5965_0_0_0;
extern Il2CppType IEnumerator_1_t5965_1_0_0;
struct IEnumerator_1_t5965;
extern Il2CppGenericClass IEnumerator_1_t5965_GenericClass;
TypeInfo IEnumerator_1_t5965_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5965_MethodInfos/* methods */
	, IEnumerator_1_t5965_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5965_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5965_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5965_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5965_0_0_0/* byval_arg */
	, &IEnumerator_1_t5965_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5965_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_19.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2786_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_19MethodDeclarations.h"

extern TypeInfo DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14309_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDefaultInitializationErrorHandler_t9_m32458_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultInitializationErrorHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultInitializationErrorHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisDefaultInitializationErrorHandler_t9_m32458(__this, p0, method) (DefaultInitializationErrorHandler_t9 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2786____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2786, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2786____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2786, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2786_FieldInfos[] =
{
	&InternalEnumerator_1_t2786____array_0_FieldInfo,
	&InternalEnumerator_1_t2786____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14306_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2786____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2786_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14306_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2786____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2786_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14309_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2786_PropertyInfos[] =
{
	&InternalEnumerator_1_t2786____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2786____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2786_InternalEnumerator_1__ctor_m14305_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14305_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14305_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2786_InternalEnumerator_1__ctor_m14305_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14305_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14306_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14306_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14306_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14307_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14307_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14307_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14308_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14308_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14308_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14309_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14309_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* declaring_type */
	, &DefaultInitializationErrorHandler_t9_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14309_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2786_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14305_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14306_MethodInfo,
	&InternalEnumerator_1_Dispose_m14307_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14308_MethodInfo,
	&InternalEnumerator_1_get_Current_m14309_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14308_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14307_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2786_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14306_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14308_MethodInfo,
	&InternalEnumerator_1_Dispose_m14307_MethodInfo,
	&InternalEnumerator_1_get_Current_m14309_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2786_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5965_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2786_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5965_il2cpp_TypeInfo, 7},
};
extern TypeInfo DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2786_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14309_MethodInfo/* Method Usage */,
	&DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDefaultInitializationErrorHandler_t9_m32458_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2786_0_0_0;
extern Il2CppType InternalEnumerator_1_t2786_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2786_GenericClass;
TypeInfo InternalEnumerator_1_t2786_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2786_MethodInfos/* methods */
	, InternalEnumerator_1_t2786_PropertyInfos/* properties */
	, InternalEnumerator_1_t2786_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2786_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2786_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2786_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2786_1_0_0/* this_arg */
	, InternalEnumerator_1_t2786_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2786_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2786_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2786)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7581_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>
extern MethodInfo ICollection_1_get_Count_m42633_MethodInfo;
static PropertyInfo ICollection_1_t7581____Count_PropertyInfo = 
{
	&ICollection_1_t7581_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42633_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42634_MethodInfo;
static PropertyInfo ICollection_1_t7581____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7581_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42634_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7581_PropertyInfos[] =
{
	&ICollection_1_t7581____Count_PropertyInfo,
	&ICollection_1_t7581____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42633_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m42633_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7581_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42633_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42634_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42634_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7581_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42634_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
static ParameterInfo ICollection_1_t7581_ICollection_1_Add_m42635_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t9_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42635_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Add(T)
MethodInfo ICollection_1_Add_m42635_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7581_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7581_ICollection_1_Add_m42635_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42635_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42636_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Clear()
MethodInfo ICollection_1_Clear_m42636_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7581_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42636_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
static ParameterInfo ICollection_1_t7581_ICollection_1_Contains_m42637_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t9_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42637_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m42637_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7581_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7581_ICollection_1_Contains_m42637_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42637_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandlerU5BU5D_t5362_0_0_0;
extern Il2CppType DefaultInitializationErrorHandlerU5BU5D_t5362_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7581_ICollection_1_CopyTo_m42638_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandlerU5BU5D_t5362_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42638_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42638_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7581_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7581_ICollection_1_CopyTo_m42638_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42638_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
static ParameterInfo ICollection_1_t7581_ICollection_1_Remove_m42639_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t9_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42639_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m42639_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7581_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7581_ICollection_1_Remove_m42639_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42639_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7581_MethodInfos[] =
{
	&ICollection_1_get_Count_m42633_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42634_MethodInfo,
	&ICollection_1_Add_m42635_MethodInfo,
	&ICollection_1_Clear_m42636_MethodInfo,
	&ICollection_1_Contains_m42637_MethodInfo,
	&ICollection_1_CopyTo_m42638_MethodInfo,
	&ICollection_1_Remove_m42639_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7583_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7581_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7583_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7581_0_0_0;
extern Il2CppType ICollection_1_t7581_1_0_0;
struct ICollection_1_t7581;
extern Il2CppGenericClass ICollection_1_t7581_GenericClass;
TypeInfo ICollection_1_t7581_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7581_MethodInfos/* methods */
	, ICollection_1_t7581_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7581_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7581_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7581_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7581_0_0_0/* byval_arg */
	, &ICollection_1_t7581_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7581_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultInitializationErrorHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DefaultInitializationErrorHandler>
extern Il2CppType IEnumerator_1_t5965_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42640_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultInitializationErrorHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42640_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7583_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5965_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42640_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7583_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42640_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7583_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7583_0_0_0;
extern Il2CppType IEnumerable_1_t7583_1_0_0;
struct IEnumerable_1_t7583;
extern Il2CppGenericClass IEnumerable_1_t7583_GenericClass;
TypeInfo IEnumerable_1_t7583_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7583_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7583_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7583_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7583_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7583_0_0_0/* byval_arg */
	, &IEnumerable_1_t7583_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7583_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7582_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>
extern MethodInfo IList_1_get_Item_m42641_MethodInfo;
extern MethodInfo IList_1_set_Item_m42642_MethodInfo;
static PropertyInfo IList_1_t7582____Item_PropertyInfo = 
{
	&IList_1_t7582_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42641_MethodInfo/* get */
	, &IList_1_set_Item_m42642_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7582_PropertyInfos[] =
{
	&IList_1_t7582____Item_PropertyInfo,
	NULL
};
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
static ParameterInfo IList_1_t7582_IList_1_IndexOf_m42643_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t9_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42643_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42643_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7582_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7582_IList_1_IndexOf_m42643_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42643_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
static ParameterInfo IList_1_t7582_IList_1_Insert_m42644_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t9_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42644_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42644_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7582_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7582_IList_1_Insert_m42644_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42644_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7582_IList_1_RemoveAt_m42645_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42645_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42645_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7582_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7582_IList_1_RemoveAt_m42645_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42645_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7582_IList_1_get_Item_m42641_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42641_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42641_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7582_il2cpp_TypeInfo/* declaring_type */
	, &DefaultInitializationErrorHandler_t9_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7582_IList_1_get_Item_m42641_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42641_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
static ParameterInfo IList_1_t7582_IList_1_set_Item_m42642_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t9_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42642_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42642_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7582_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7582_IList_1_set_Item_m42642_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42642_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7582_MethodInfos[] =
{
	&IList_1_IndexOf_m42643_MethodInfo,
	&IList_1_Insert_m42644_MethodInfo,
	&IList_1_RemoveAt_m42645_MethodInfo,
	&IList_1_get_Item_m42641_MethodInfo,
	&IList_1_set_Item_m42642_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7582_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7581_il2cpp_TypeInfo,
	&IEnumerable_1_t7583_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7582_0_0_0;
extern Il2CppType IList_1_t7582_1_0_0;
struct IList_1_t7582;
extern Il2CppGenericClass IList_1_t7582_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7582_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7582_MethodInfos/* methods */
	, IList_1_t7582_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7582_il2cpp_TypeInfo/* element_class */
	, IList_1_t7582_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7582_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7582_0_0_0/* byval_arg */
	, &IList_1_t7582_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7582_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_8.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2787_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_8MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_4.h"
extern TypeInfo InvokableCall_1_t2788_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_4MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14312_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14314_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2787____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2787_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2787, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2787_FieldInfos[] =
{
	&CachedInvokableCall_1_t2787____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2787_CachedInvokableCall_1__ctor_m14310_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t9_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14310_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14310_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2787_CachedInvokableCall_1__ctor_m14310_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14310_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2787_CachedInvokableCall_1_Invoke_m14311_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14311_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14311_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2787_CachedInvokableCall_1_Invoke_m14311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14311_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2787_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14310_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14311_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14311_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14315_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2787_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14311_MethodInfo,
	&InvokableCall_1_Find_m14315_MethodInfo,
};
extern Il2CppType UnityAction_1_t2789_0_0_0;
extern TypeInfo UnityAction_1_t2789_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisDefaultInitializationErrorHandler_t9_m32468_MethodInfo;
extern TypeInfo DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14317_MethodInfo;
extern TypeInfo DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2787_RGCTXData[8] = 
{
	&UnityAction_1_t2789_0_0_0/* Type Usage */,
	&UnityAction_1_t2789_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultInitializationErrorHandler_t9_m32468_MethodInfo/* Method Usage */,
	&DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14317_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14312_MethodInfo/* Method Usage */,
	&DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14314_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2787_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2787_1_0_0;
struct CachedInvokableCall_1_t2787;
extern Il2CppGenericClass CachedInvokableCall_1_t2787_GenericClass;
TypeInfo CachedInvokableCall_1_t2787_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2787_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2787_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2788_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2787_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2787_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2787_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2787_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2787_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2787_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2787_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2787)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_11.h"
extern TypeInfo UnityAction_1_t2789_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_11MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultInitializationErrorHandler>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultInitializationErrorHandler>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisDefaultInitializationErrorHandler_t9_m32468(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
extern Il2CppType UnityAction_1_t2789_0_0_1;
FieldInfo InvokableCall_1_t2788____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2789_0_0_1/* type */
	, &InvokableCall_1_t2788_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2788, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2788_FieldInfos[] =
{
	&InvokableCall_1_t2788____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2788_InvokableCall_1__ctor_m14312_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14312_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14312_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2788_InvokableCall_1__ctor_m14312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14312_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2789_0_0_0;
static ParameterInfo InvokableCall_1_t2788_InvokableCall_1__ctor_m14313_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2789_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14313_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14313_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2788_InvokableCall_1__ctor_m14313_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14313_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2788_InvokableCall_1_Invoke_m14314_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14314_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14314_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2788_InvokableCall_1_Invoke_m14314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14314_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2788_InvokableCall_1_Find_m14315_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14315_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14315_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2788_InvokableCall_1_Find_m14315_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14315_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2788_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14312_MethodInfo,
	&InvokableCall_1__ctor_m14313_MethodInfo,
	&InvokableCall_1_Invoke_m14314_MethodInfo,
	&InvokableCall_1_Find_m14315_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2788_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14314_MethodInfo,
	&InvokableCall_1_Find_m14315_MethodInfo,
};
extern TypeInfo UnityAction_1_t2789_il2cpp_TypeInfo;
extern TypeInfo DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2788_RGCTXData[5] = 
{
	&UnityAction_1_t2789_0_0_0/* Type Usage */,
	&UnityAction_1_t2789_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultInitializationErrorHandler_t9_m32468_MethodInfo/* Method Usage */,
	&DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14317_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2788_0_0_0;
extern Il2CppType InvokableCall_1_t2788_1_0_0;
struct InvokableCall_1_t2788;
extern Il2CppGenericClass InvokableCall_1_t2788_GenericClass;
TypeInfo InvokableCall_1_t2788_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2788_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2788_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2788_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2788_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2788_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2788_0_0_0/* byval_arg */
	, &InvokableCall_1_t2788_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2788_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2788_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2788)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2789_UnityAction_1__ctor_m14316_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14316_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14316_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2789_UnityAction_1__ctor_m14316_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14316_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
static ParameterInfo UnityAction_1_t2789_UnityAction_1_Invoke_m14317_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t9_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14317_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14317_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2789_UnityAction_1_Invoke_m14317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14317_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t9_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2789_UnityAction_1_BeginInvoke_m14318_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t9_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14318_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14318_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2789_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2789_UnityAction_1_BeginInvoke_m14318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14318_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2789_UnityAction_1_EndInvoke_m14319_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14319_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14319_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2789_UnityAction_1_EndInvoke_m14319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14319_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2789_MethodInfos[] =
{
	&UnityAction_1__ctor_m14316_MethodInfo,
	&UnityAction_1_Invoke_m14317_MethodInfo,
	&UnityAction_1_BeginInvoke_m14318_MethodInfo,
	&UnityAction_1_EndInvoke_m14319_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14318_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14319_MethodInfo;
static MethodInfo* UnityAction_1_t2789_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14317_MethodInfo,
	&UnityAction_1_BeginInvoke_m14318_MethodInfo,
	&UnityAction_1_EndInvoke_m14319_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2789_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2789_1_0_0;
struct UnityAction_1_t2789;
extern Il2CppGenericClass UnityAction_1_t2789_GenericClass;
TypeInfo UnityAction_1_t2789_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2789_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2789_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2789_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2789_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2789_0_0_0/* byval_arg */
	, &UnityAction_1_t2789_1_0_0/* this_arg */
	, UnityAction_1_t2789_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2789_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2789)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5967_il2cpp_TypeInfo;

// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.QCARAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.QCARAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42646_MethodInfo;
static PropertyInfo IEnumerator_1_t5967____Current_PropertyInfo = 
{
	&IEnumerator_1_t5967_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42646_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5967_PropertyInfos[] =
{
	&IEnumerator_1_t5967____Current_PropertyInfo,
	NULL
};
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42646_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.QCARAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42646_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5967_il2cpp_TypeInfo/* declaring_type */
	, &QCARAbstractBehaviour_t45_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42646_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5967_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42646_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5967_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5967_0_0_0;
extern Il2CppType IEnumerator_1_t5967_1_0_0;
struct IEnumerator_1_t5967;
extern Il2CppGenericClass IEnumerator_1_t5967_GenericClass;
TypeInfo IEnumerator_1_t5967_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5967_MethodInfos/* methods */
	, IEnumerator_1_t5967_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5967_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5967_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5967_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5967_0_0_0/* byval_arg */
	, &IEnumerator_1_t5967_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5967_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_20.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2790_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_20MethodDeclarations.h"

extern TypeInfo QCARAbstractBehaviour_t45_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14324_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisQCARAbstractBehaviour_t45_m32470_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.QCARAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.QCARAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisQCARAbstractBehaviour_t45_m32470(__this, p0, method) (QCARAbstractBehaviour_t45 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2790____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2790, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2790____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2790, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2790_FieldInfos[] =
{
	&InternalEnumerator_1_t2790____array_0_FieldInfo,
	&InternalEnumerator_1_t2790____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14321_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2790____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2790_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14321_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2790____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2790_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14324_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2790_PropertyInfos[] =
{
	&InternalEnumerator_1_t2790____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2790____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2790_InternalEnumerator_1__ctor_m14320_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14320_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14320_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2790_InternalEnumerator_1__ctor_m14320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14320_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14321_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14321_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14321_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14322_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14322_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14322_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14323_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14323_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14323_GenericMethod/* genericMethod */

};
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14324_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14324_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* declaring_type */
	, &QCARAbstractBehaviour_t45_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14324_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2790_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14320_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14321_MethodInfo,
	&InternalEnumerator_1_Dispose_m14322_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14323_MethodInfo,
	&InternalEnumerator_1_get_Current_m14324_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14323_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14322_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2790_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14321_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14323_MethodInfo,
	&InternalEnumerator_1_Dispose_m14322_MethodInfo,
	&InternalEnumerator_1_get_Current_m14324_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2790_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5967_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2790_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5967_il2cpp_TypeInfo, 7},
};
extern TypeInfo QCARAbstractBehaviour_t45_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2790_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14324_MethodInfo/* Method Usage */,
	&QCARAbstractBehaviour_t45_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisQCARAbstractBehaviour_t45_m32470_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2790_0_0_0;
extern Il2CppType InternalEnumerator_1_t2790_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2790_GenericClass;
TypeInfo InternalEnumerator_1_t2790_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2790_MethodInfos/* methods */
	, InternalEnumerator_1_t2790_PropertyInfos/* properties */
	, InternalEnumerator_1_t2790_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2790_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2790_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2790_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2790_1_0_0/* this_arg */
	, InternalEnumerator_1_t2790_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2790_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2790_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2790)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7584_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42647_MethodInfo;
static PropertyInfo ICollection_1_t7584____Count_PropertyInfo = 
{
	&ICollection_1_t7584_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42647_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42648_MethodInfo;
static PropertyInfo ICollection_1_t7584____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7584_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42648_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7584_PropertyInfos[] =
{
	&ICollection_1_t7584____Count_PropertyInfo,
	&ICollection_1_t7584____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42647_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42647_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7584_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42647_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42648_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42648_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7584_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42648_GenericMethod/* genericMethod */

};
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
static ParameterInfo ICollection_1_t7584_ICollection_1_Add_m42649_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t45_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42649_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42649_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7584_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7584_ICollection_1_Add_m42649_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42649_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42650_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42650_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7584_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42650_GenericMethod/* genericMethod */

};
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
static ParameterInfo ICollection_1_t7584_ICollection_1_Contains_m42651_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t45_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42651_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42651_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7584_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7584_ICollection_1_Contains_m42651_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42651_GenericMethod/* genericMethod */

};
extern Il2CppType QCARAbstractBehaviourU5BU5D_t5627_0_0_0;
extern Il2CppType QCARAbstractBehaviourU5BU5D_t5627_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7584_ICollection_1_CopyTo_m42652_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviourU5BU5D_t5627_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42652_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42652_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7584_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7584_ICollection_1_CopyTo_m42652_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42652_GenericMethod/* genericMethod */

};
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
static ParameterInfo ICollection_1_t7584_ICollection_1_Remove_m42653_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t45_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42653_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42653_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7584_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7584_ICollection_1_Remove_m42653_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42653_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7584_MethodInfos[] =
{
	&ICollection_1_get_Count_m42647_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42648_MethodInfo,
	&ICollection_1_Add_m42649_MethodInfo,
	&ICollection_1_Clear_m42650_MethodInfo,
	&ICollection_1_Contains_m42651_MethodInfo,
	&ICollection_1_CopyTo_m42652_MethodInfo,
	&ICollection_1_Remove_m42653_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7586_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7584_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7586_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7584_0_0_0;
extern Il2CppType ICollection_1_t7584_1_0_0;
struct ICollection_1_t7584;
extern Il2CppGenericClass ICollection_1_t7584_GenericClass;
TypeInfo ICollection_1_t7584_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7584_MethodInfos/* methods */
	, ICollection_1_t7584_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7584_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7584_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7584_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7584_0_0_0/* byval_arg */
	, &ICollection_1_t7584_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7584_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.QCARAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.QCARAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5967_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42654_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.QCARAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42654_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7586_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5967_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42654_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7586_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42654_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7586_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7586_0_0_0;
extern Il2CppType IEnumerable_1_t7586_1_0_0;
struct IEnumerable_1_t7586;
extern Il2CppGenericClass IEnumerable_1_t7586_GenericClass;
TypeInfo IEnumerable_1_t7586_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7586_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7586_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7586_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7586_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7586_0_0_0/* byval_arg */
	, &IEnumerable_1_t7586_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7586_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7585_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42655_MethodInfo;
extern MethodInfo IList_1_set_Item_m42656_MethodInfo;
static PropertyInfo IList_1_t7585____Item_PropertyInfo = 
{
	&IList_1_t7585_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42655_MethodInfo/* get */
	, &IList_1_set_Item_m42656_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7585_PropertyInfos[] =
{
	&IList_1_t7585____Item_PropertyInfo,
	NULL
};
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
static ParameterInfo IList_1_t7585_IList_1_IndexOf_m42657_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t45_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42657_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42657_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7585_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7585_IList_1_IndexOf_m42657_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42657_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
static ParameterInfo IList_1_t7585_IList_1_Insert_m42658_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t45_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42658_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42658_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7585_IList_1_Insert_m42658_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42658_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7585_IList_1_RemoveAt_m42659_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42659_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42659_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7585_IList_1_RemoveAt_m42659_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42659_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7585_IList_1_get_Item_m42655_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42655_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42655_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7585_il2cpp_TypeInfo/* declaring_type */
	, &QCARAbstractBehaviour_t45_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7585_IList_1_get_Item_m42655_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42655_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
static ParameterInfo IList_1_t7585_IList_1_set_Item_m42656_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t45_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42656_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42656_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7585_IList_1_set_Item_m42656_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42656_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7585_MethodInfos[] =
{
	&IList_1_IndexOf_m42657_MethodInfo,
	&IList_1_Insert_m42658_MethodInfo,
	&IList_1_RemoveAt_m42659_MethodInfo,
	&IList_1_get_Item_m42655_MethodInfo,
	&IList_1_set_Item_m42656_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7585_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7584_il2cpp_TypeInfo,
	&IEnumerable_1_t7586_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7585_0_0_0;
extern Il2CppType IList_1_t7585_1_0_0;
struct IList_1_t7585;
extern Il2CppGenericClass IList_1_t7585_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7585_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7585_MethodInfos/* methods */
	, IList_1_t7585_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7585_il2cpp_TypeInfo/* element_class */
	, IList_1_t7585_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7585_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7585_0_0_0/* byval_arg */
	, &IList_1_t7585_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7585_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7587_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>
extern MethodInfo ICollection_1_get_Count_m42660_MethodInfo;
static PropertyInfo ICollection_1_t7587____Count_PropertyInfo = 
{
	&ICollection_1_t7587_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42660_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42661_MethodInfo;
static PropertyInfo ICollection_1_t7587____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7587_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42661_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7587_PropertyInfos[] =
{
	&ICollection_1_t7587____Count_PropertyInfo,
	&ICollection_1_t7587____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42660_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42660_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7587_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42660_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42661_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42661_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7587_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42661_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorQCARBehaviour_t162_0_0_0;
extern Il2CppType IEditorQCARBehaviour_t162_0_0_0;
static ParameterInfo ICollection_1_t7587_ICollection_1_Add_m42662_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t162_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42662_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42662_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7587_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7587_ICollection_1_Add_m42662_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42662_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42663_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42663_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7587_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42663_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorQCARBehaviour_t162_0_0_0;
static ParameterInfo ICollection_1_t7587_ICollection_1_Contains_m42664_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t162_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42664_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42664_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7587_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7587_ICollection_1_Contains_m42664_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42664_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorQCARBehaviourU5BU5D_t5628_0_0_0;
extern Il2CppType IEditorQCARBehaviourU5BU5D_t5628_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7587_ICollection_1_CopyTo_m42665_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviourU5BU5D_t5628_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42665_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42665_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7587_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7587_ICollection_1_CopyTo_m42665_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42665_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorQCARBehaviour_t162_0_0_0;
static ParameterInfo ICollection_1_t7587_ICollection_1_Remove_m42666_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t162_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42666_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42666_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7587_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7587_ICollection_1_Remove_m42666_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42666_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7587_MethodInfos[] =
{
	&ICollection_1_get_Count_m42660_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42661_MethodInfo,
	&ICollection_1_Add_m42662_MethodInfo,
	&ICollection_1_Clear_m42663_MethodInfo,
	&ICollection_1_Contains_m42664_MethodInfo,
	&ICollection_1_CopyTo_m42665_MethodInfo,
	&ICollection_1_Remove_m42666_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7589_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7587_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7589_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7587_0_0_0;
extern Il2CppType ICollection_1_t7587_1_0_0;
struct ICollection_1_t7587;
extern Il2CppGenericClass ICollection_1_t7587_GenericClass;
TypeInfo ICollection_1_t7587_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7587_MethodInfos/* methods */
	, ICollection_1_t7587_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7587_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7587_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7587_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7587_0_0_0/* byval_arg */
	, &ICollection_1_t7587_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7587_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorQCARBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorQCARBehaviour>
extern Il2CppType IEnumerator_1_t5969_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42667_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorQCARBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42667_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7589_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5969_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42667_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7589_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42667_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7589_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7589_0_0_0;
extern Il2CppType IEnumerable_1_t7589_1_0_0;
struct IEnumerable_1_t7589;
extern Il2CppGenericClass IEnumerable_1_t7589_GenericClass;
TypeInfo IEnumerable_1_t7589_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7589_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7589_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7589_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7589_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7589_0_0_0/* byval_arg */
	, &IEnumerable_1_t7589_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7589_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5969_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorQCARBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorQCARBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42668_MethodInfo;
static PropertyInfo IEnumerator_1_t5969____Current_PropertyInfo = 
{
	&IEnumerator_1_t5969_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42668_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5969_PropertyInfos[] =
{
	&IEnumerator_1_t5969____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorQCARBehaviour_t162_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42668_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorQCARBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42668_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5969_il2cpp_TypeInfo/* declaring_type */
	, &IEditorQCARBehaviour_t162_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42668_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5969_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42668_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5969_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5969_0_0_0;
extern Il2CppType IEnumerator_1_t5969_1_0_0;
struct IEnumerator_1_t5969;
extern Il2CppGenericClass IEnumerator_1_t5969_GenericClass;
TypeInfo IEnumerator_1_t5969_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5969_MethodInfos/* methods */
	, IEnumerator_1_t5969_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5969_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5969_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5969_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5969_0_0_0/* byval_arg */
	, &IEnumerator_1_t5969_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5969_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_21.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2791_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_21MethodDeclarations.h"

extern TypeInfo IEditorQCARBehaviour_t162_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14329_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorQCARBehaviour_t162_m32481_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorQCARBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorQCARBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorQCARBehaviour_t162_m32481(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2791____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2791, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2791____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2791, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2791_FieldInfos[] =
{
	&InternalEnumerator_1_t2791____array_0_FieldInfo,
	&InternalEnumerator_1_t2791____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14326_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2791____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2791_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14326_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2791____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2791_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14329_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2791_PropertyInfos[] =
{
	&InternalEnumerator_1_t2791____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2791____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2791_InternalEnumerator_1__ctor_m14325_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14325_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14325_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2791_InternalEnumerator_1__ctor_m14325_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14325_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14326_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14326_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14326_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14327_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14327_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14327_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14328_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14328_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14328_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorQCARBehaviour_t162_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14329_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14329_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* declaring_type */
	, &IEditorQCARBehaviour_t162_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14329_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2791_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14325_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14326_MethodInfo,
	&InternalEnumerator_1_Dispose_m14327_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14328_MethodInfo,
	&InternalEnumerator_1_get_Current_m14329_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14328_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14327_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2791_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14326_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14328_MethodInfo,
	&InternalEnumerator_1_Dispose_m14327_MethodInfo,
	&InternalEnumerator_1_get_Current_m14329_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2791_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5969_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2791_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5969_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorQCARBehaviour_t162_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2791_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14329_MethodInfo/* Method Usage */,
	&IEditorQCARBehaviour_t162_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorQCARBehaviour_t162_m32481_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2791_0_0_0;
extern Il2CppType InternalEnumerator_1_t2791_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2791_GenericClass;
TypeInfo InternalEnumerator_1_t2791_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2791_MethodInfos/* methods */
	, InternalEnumerator_1_t2791_PropertyInfos/* properties */
	, InternalEnumerator_1_t2791_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2791_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2791_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2791_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2791_1_0_0/* this_arg */
	, InternalEnumerator_1_t2791_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2791_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2791_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2791)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7588_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>
extern MethodInfo IList_1_get_Item_m42669_MethodInfo;
extern MethodInfo IList_1_set_Item_m42670_MethodInfo;
static PropertyInfo IList_1_t7588____Item_PropertyInfo = 
{
	&IList_1_t7588_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42669_MethodInfo/* get */
	, &IList_1_set_Item_m42670_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7588_PropertyInfos[] =
{
	&IList_1_t7588____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorQCARBehaviour_t162_0_0_0;
static ParameterInfo IList_1_t7588_IList_1_IndexOf_m42671_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t162_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42671_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42671_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7588_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7588_IList_1_IndexOf_m42671_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42671_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorQCARBehaviour_t162_0_0_0;
static ParameterInfo IList_1_t7588_IList_1_Insert_m42672_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t162_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42672_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42672_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7588_IList_1_Insert_m42672_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42672_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7588_IList_1_RemoveAt_m42673_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42673_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42673_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7588_IList_1_RemoveAt_m42673_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42673_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7588_IList_1_get_Item_m42669_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorQCARBehaviour_t162_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42669_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42669_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7588_il2cpp_TypeInfo/* declaring_type */
	, &IEditorQCARBehaviour_t162_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7588_IList_1_get_Item_m42669_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42669_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorQCARBehaviour_t162_0_0_0;
static ParameterInfo IList_1_t7588_IList_1_set_Item_m42670_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t162_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42670_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42670_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7588_IList_1_set_Item_m42670_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42670_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7588_MethodInfos[] =
{
	&IList_1_IndexOf_m42671_MethodInfo,
	&IList_1_Insert_m42672_MethodInfo,
	&IList_1_RemoveAt_m42673_MethodInfo,
	&IList_1_get_Item_m42669_MethodInfo,
	&IList_1_set_Item_m42670_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7588_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7587_il2cpp_TypeInfo,
	&IEnumerable_1_t7589_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7588_0_0_0;
extern Il2CppType IList_1_t7588_1_0_0;
struct IList_1_t7588;
extern Il2CppGenericClass IList_1_t7588_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7588_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7588_MethodInfos/* methods */
	, IList_1_t7588_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7588_il2cpp_TypeInfo/* element_class */
	, IList_1_t7588_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7588_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7588_0_0_0/* byval_arg */
	, &IList_1_t7588_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7588_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Action`1<Vuforia.QCARUnity/InitError>
#include "mscorlib_System_Action_1_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Action_1_t117_il2cpp_TypeInfo;
// System.Action`1<Vuforia.QCARUnity/InitError>
#include "mscorlib_System_Action_1_genMethodDeclarations.h"

// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"


// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::.ctor(System.Object,System.IntPtr)
extern MethodInfo Action_1__ctor_m258_MethodInfo;
 void Action_1__ctor_m258 (Action_1_t117 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method){
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::Invoke(T)
extern MethodInfo Action_1_Invoke_m5486_MethodInfo;
 void Action_1_Invoke_m5486 (Action_1_t117 * __this, int32_t ___obj, MethodInfo* method){
	if(__this->___prev_9 != NULL)
	{
		Action_1_Invoke_m5486((Action_1_t117 *)__this->___prev_9,___obj, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___obj, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___obj, MethodInfo* method);
	((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
}
// System.IAsyncResult System.Action`1<Vuforia.QCARUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern MethodInfo Action_1_BeginInvoke_m14330_MethodInfo;
 Object_t * Action_1_BeginInvoke_m14330 (Action_1_t117 * __this, int32_t ___obj, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&InitError_t124_il2cpp_TypeInfo), &___obj);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::EndInvoke(System.IAsyncResult)
extern MethodInfo Action_1_EndInvoke_m14331_MethodInfo;
 void Action_1_EndInvoke_m14331 (Action_1_t117 * __this, Object_t * ___result, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Metadata Definition System.Action`1<Vuforia.QCARUnity/InitError>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Action_1_t117_Action_1__ctor_m258_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1__ctor_m258_GenericMethod;
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::.ctor(System.Object,System.IntPtr)
MethodInfo Action_1__ctor_m258_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action_1__ctor_m258/* method */
	, &Action_1_t117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Action_1_t117_Action_1__ctor_m258_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1__ctor_m258_GenericMethod/* genericMethod */

};
extern Il2CppType InitError_t124_0_0_0;
extern Il2CppType InitError_t124_0_0_0;
static ParameterInfo Action_1_t117_Action_1_Invoke_m5486_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &InitError_t124_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_Invoke_m5486_GenericMethod;
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::Invoke(T)
MethodInfo Action_1_Invoke_m5486_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_1_Invoke_m5486/* method */
	, &Action_1_t117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Action_1_t117_Action_1_Invoke_m5486_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_Invoke_m5486_GenericMethod/* genericMethod */

};
extern Il2CppType InitError_t124_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_1_t117_Action_1_BeginInvoke_m14330_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &InitError_t124_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_BeginInvoke_m14330_GenericMethod;
// System.IAsyncResult System.Action`1<Vuforia.QCARUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Action_1_BeginInvoke_m14330_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_1_BeginInvoke_m14330/* method */
	, &Action_1_t117_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123_Object_t_Object_t/* invoker_method */
	, Action_1_t117_Action_1_BeginInvoke_m14330_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_BeginInvoke_m14330_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Action_1_t117_Action_1_EndInvoke_m14331_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_EndInvoke_m14331_GenericMethod;
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::EndInvoke(System.IAsyncResult)
MethodInfo Action_1_EndInvoke_m14331_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_1_EndInvoke_m14331/* method */
	, &Action_1_t117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Action_1_t117_Action_1_EndInvoke_m14331_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_EndInvoke_m14331_GenericMethod/* genericMethod */

};
static MethodInfo* Action_1_t117_MethodInfos[] =
{
	&Action_1__ctor_m258_MethodInfo,
	&Action_1_Invoke_m5486_MethodInfo,
	&Action_1_BeginInvoke_m14330_MethodInfo,
	&Action_1_EndInvoke_m14331_MethodInfo,
	NULL
};
static MethodInfo* Action_1_t117_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Action_1_Invoke_m5486_MethodInfo,
	&Action_1_BeginInvoke_m14330_MethodInfo,
	&Action_1_EndInvoke_m14331_MethodInfo,
};
static Il2CppInterfaceOffsetPair Action_1_t117_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Action_1_t117_0_0_0;
extern Il2CppType Action_1_t117_1_0_0;
struct Action_1_t117;
extern Il2CppGenericClass Action_1_t117_GenericClass;
TypeInfo Action_1_t117_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t117_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Action_1_t117_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Action_1_t117_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Action_1_t117_il2cpp_TypeInfo/* cast_class */
	, &Action_1_t117_0_0_0/* byval_arg */
	, &Action_1_t117_1_0_0/* this_arg */
	, Action_1_t117_InterfacesOffsets/* interface_offsets */
	, &Action_1_t117_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_1_t117)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5971_il2cpp_TypeInfo;

// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>
extern MethodInfo IEnumerator_1_get_Current_m42674_MethodInfo;
static PropertyInfo IEnumerator_1_t5971____Current_PropertyInfo = 
{
	&IEnumerator_1_t5971_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42674_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5971_PropertyInfos[] =
{
	&IEnumerator_1_t5971____Current_PropertyInfo,
	NULL
};
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42674_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42674_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5971_il2cpp_TypeInfo/* declaring_type */
	, &DefaultSmartTerrainEventHandler_t14_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42674_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5971_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42674_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5971_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5971_0_0_0;
extern Il2CppType IEnumerator_1_t5971_1_0_0;
struct IEnumerator_1_t5971;
extern Il2CppGenericClass IEnumerator_1_t5971_GenericClass;
TypeInfo IEnumerator_1_t5971_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5971_MethodInfos/* methods */
	, IEnumerator_1_t5971_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5971_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5971_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5971_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5971_0_0_0/* byval_arg */
	, &IEnumerator_1_t5971_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5971_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_22.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2792_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_22MethodDeclarations.h"

extern TypeInfo DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14336_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDefaultSmartTerrainEventHandler_t14_m32492_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultSmartTerrainEventHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultSmartTerrainEventHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisDefaultSmartTerrainEventHandler_t14_m32492(__this, p0, method) (DefaultSmartTerrainEventHandler_t14 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2792____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2792, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2792____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2792, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2792_FieldInfos[] =
{
	&InternalEnumerator_1_t2792____array_0_FieldInfo,
	&InternalEnumerator_1_t2792____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2792____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2792_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2792____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2792_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14336_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2792_PropertyInfos[] =
{
	&InternalEnumerator_1_t2792____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2792____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2792_InternalEnumerator_1__ctor_m14332_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14332_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14332_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2792_InternalEnumerator_1__ctor_m14332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14332_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14334_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14334_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14334_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14335_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14335_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14335_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14336_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14336_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* declaring_type */
	, &DefaultSmartTerrainEventHandler_t14_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14336_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2792_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14332_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_MethodInfo,
	&InternalEnumerator_1_Dispose_m14334_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14335_MethodInfo,
	&InternalEnumerator_1_get_Current_m14336_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14335_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14334_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2792_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14335_MethodInfo,
	&InternalEnumerator_1_Dispose_m14334_MethodInfo,
	&InternalEnumerator_1_get_Current_m14336_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2792_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5971_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2792_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5971_il2cpp_TypeInfo, 7},
};
extern TypeInfo DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2792_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14336_MethodInfo/* Method Usage */,
	&DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDefaultSmartTerrainEventHandler_t14_m32492_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2792_0_0_0;
extern Il2CppType InternalEnumerator_1_t2792_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2792_GenericClass;
TypeInfo InternalEnumerator_1_t2792_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2792_MethodInfos/* methods */
	, InternalEnumerator_1_t2792_PropertyInfos/* properties */
	, InternalEnumerator_1_t2792_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2792_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2792_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2792_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2792_1_0_0/* this_arg */
	, InternalEnumerator_1_t2792_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2792_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2792_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2792)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7590_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>
extern MethodInfo ICollection_1_get_Count_m42675_MethodInfo;
static PropertyInfo ICollection_1_t7590____Count_PropertyInfo = 
{
	&ICollection_1_t7590_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42675_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42676_MethodInfo;
static PropertyInfo ICollection_1_t7590____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7590_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42676_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7590_PropertyInfos[] =
{
	&ICollection_1_t7590____Count_PropertyInfo,
	&ICollection_1_t7590____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42675_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m42675_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7590_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42675_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42676_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42676_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7590_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42676_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
static ParameterInfo ICollection_1_t7590_ICollection_1_Add_m42677_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t14_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42677_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Add(T)
MethodInfo ICollection_1_Add_m42677_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7590_ICollection_1_Add_m42677_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42677_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42678_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Clear()
MethodInfo ICollection_1_Clear_m42678_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42678_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
static ParameterInfo ICollection_1_t7590_ICollection_1_Contains_m42679_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t14_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42679_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m42679_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7590_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7590_ICollection_1_Contains_m42679_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42679_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandlerU5BU5D_t5363_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandlerU5BU5D_t5363_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7590_ICollection_1_CopyTo_m42680_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandlerU5BU5D_t5363_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42680_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42680_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7590_ICollection_1_CopyTo_m42680_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42680_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
static ParameterInfo ICollection_1_t7590_ICollection_1_Remove_m42681_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t14_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42681_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m42681_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7590_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7590_ICollection_1_Remove_m42681_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42681_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7590_MethodInfos[] =
{
	&ICollection_1_get_Count_m42675_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42676_MethodInfo,
	&ICollection_1_Add_m42677_MethodInfo,
	&ICollection_1_Clear_m42678_MethodInfo,
	&ICollection_1_Contains_m42679_MethodInfo,
	&ICollection_1_CopyTo_m42680_MethodInfo,
	&ICollection_1_Remove_m42681_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7592_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7590_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7592_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7590_0_0_0;
extern Il2CppType ICollection_1_t7590_1_0_0;
struct ICollection_1_t7590;
extern Il2CppGenericClass ICollection_1_t7590_GenericClass;
TypeInfo ICollection_1_t7590_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7590_MethodInfos/* methods */
	, ICollection_1_t7590_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7590_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7590_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7590_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7590_0_0_0/* byval_arg */
	, &ICollection_1_t7590_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7590_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultSmartTerrainEventHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DefaultSmartTerrainEventHandler>
extern Il2CppType IEnumerator_1_t5971_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42682_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultSmartTerrainEventHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42682_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7592_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5971_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42682_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7592_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42682_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7592_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7592_0_0_0;
extern Il2CppType IEnumerable_1_t7592_1_0_0;
struct IEnumerable_1_t7592;
extern Il2CppGenericClass IEnumerable_1_t7592_GenericClass;
TypeInfo IEnumerable_1_t7592_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7592_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7592_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7592_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7592_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7592_0_0_0/* byval_arg */
	, &IEnumerable_1_t7592_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7592_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7591_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>
extern MethodInfo IList_1_get_Item_m42683_MethodInfo;
extern MethodInfo IList_1_set_Item_m42684_MethodInfo;
static PropertyInfo IList_1_t7591____Item_PropertyInfo = 
{
	&IList_1_t7591_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42683_MethodInfo/* get */
	, &IList_1_set_Item_m42684_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7591_PropertyInfos[] =
{
	&IList_1_t7591____Item_PropertyInfo,
	NULL
};
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
static ParameterInfo IList_1_t7591_IList_1_IndexOf_m42685_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t14_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42685_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42685_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7591_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7591_IList_1_IndexOf_m42685_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42685_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
static ParameterInfo IList_1_t7591_IList_1_Insert_m42686_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t14_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42686_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42686_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7591_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7591_IList_1_Insert_m42686_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42686_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7591_IList_1_RemoveAt_m42687_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42687_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42687_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7591_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7591_IList_1_RemoveAt_m42687_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42687_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7591_IList_1_get_Item_m42683_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42683_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42683_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7591_il2cpp_TypeInfo/* declaring_type */
	, &DefaultSmartTerrainEventHandler_t14_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7591_IList_1_get_Item_m42683_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42683_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
static ParameterInfo IList_1_t7591_IList_1_set_Item_m42684_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t14_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42684_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42684_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7591_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7591_IList_1_set_Item_m42684_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42684_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7591_MethodInfos[] =
{
	&IList_1_IndexOf_m42685_MethodInfo,
	&IList_1_Insert_m42686_MethodInfo,
	&IList_1_RemoveAt_m42687_MethodInfo,
	&IList_1_get_Item_m42683_MethodInfo,
	&IList_1_set_Item_m42684_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7591_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7590_il2cpp_TypeInfo,
	&IEnumerable_1_t7592_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7591_0_0_0;
extern Il2CppType IList_1_t7591_1_0_0;
struct IList_1_t7591;
extern Il2CppGenericClass IList_1_t7591_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7591_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7591_MethodInfos/* methods */
	, IList_1_t7591_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7591_il2cpp_TypeInfo/* element_class */
	, IList_1_t7591_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7591_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7591_0_0_0/* byval_arg */
	, &IList_1_t7591_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7591_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_9.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2793_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_9MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_5.h"
extern TypeInfo InvokableCall_1_t2794_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_5MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14339_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14341_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2793____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2793_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2793, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2793_FieldInfos[] =
{
	&CachedInvokableCall_1_t2793____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2793_CachedInvokableCall_1__ctor_m14337_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t14_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14337_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14337_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2793_CachedInvokableCall_1__ctor_m14337_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14337_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2793_CachedInvokableCall_1_Invoke_m14338_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14338_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14338_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2793_CachedInvokableCall_1_Invoke_m14338_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14338_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2793_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14337_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14338_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14338_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14342_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2793_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14338_MethodInfo,
	&InvokableCall_1_Find_m14342_MethodInfo,
};
extern Il2CppType UnityAction_1_t2795_0_0_0;
extern TypeInfo UnityAction_1_t2795_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisDefaultSmartTerrainEventHandler_t14_m32502_MethodInfo;
extern TypeInfo DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14344_MethodInfo;
extern TypeInfo DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2793_RGCTXData[8] = 
{
	&UnityAction_1_t2795_0_0_0/* Type Usage */,
	&UnityAction_1_t2795_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultSmartTerrainEventHandler_t14_m32502_MethodInfo/* Method Usage */,
	&DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14344_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14339_MethodInfo/* Method Usage */,
	&DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14341_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2793_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2793_1_0_0;
struct CachedInvokableCall_1_t2793;
extern Il2CppGenericClass CachedInvokableCall_1_t2793_GenericClass;
TypeInfo CachedInvokableCall_1_t2793_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2793_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2793_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2793_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2793_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2793_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2793_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2793_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2793_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2793_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2793)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_12.h"
extern TypeInfo UnityAction_1_t2795_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_12MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultSmartTerrainEventHandler>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultSmartTerrainEventHandler>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisDefaultSmartTerrainEventHandler_t14_m32502(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
extern Il2CppType UnityAction_1_t2795_0_0_1;
FieldInfo InvokableCall_1_t2794____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2795_0_0_1/* type */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2794, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2794_FieldInfos[] =
{
	&InvokableCall_1_t2794____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2794_InvokableCall_1__ctor_m14339_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14339_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14339_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2794_InvokableCall_1__ctor_m14339_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14339_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2795_0_0_0;
static ParameterInfo InvokableCall_1_t2794_InvokableCall_1__ctor_m14340_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2795_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14340_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14340_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2794_InvokableCall_1__ctor_m14340_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14340_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2794_InvokableCall_1_Invoke_m14341_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14341_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14341_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2794_InvokableCall_1_Invoke_m14341_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14341_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2794_InvokableCall_1_Find_m14342_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14342_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14342_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2794_InvokableCall_1_Find_m14342_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14342_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2794_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14339_MethodInfo,
	&InvokableCall_1__ctor_m14340_MethodInfo,
	&InvokableCall_1_Invoke_m14341_MethodInfo,
	&InvokableCall_1_Find_m14342_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2794_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14341_MethodInfo,
	&InvokableCall_1_Find_m14342_MethodInfo,
};
extern TypeInfo UnityAction_1_t2795_il2cpp_TypeInfo;
extern TypeInfo DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2794_RGCTXData[5] = 
{
	&UnityAction_1_t2795_0_0_0/* Type Usage */,
	&UnityAction_1_t2795_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultSmartTerrainEventHandler_t14_m32502_MethodInfo/* Method Usage */,
	&DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14344_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2794_0_0_0;
extern Il2CppType InvokableCall_1_t2794_1_0_0;
struct InvokableCall_1_t2794;
extern Il2CppGenericClass InvokableCall_1_t2794_GenericClass;
TypeInfo InvokableCall_1_t2794_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2794_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2794_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2794_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2794_0_0_0/* byval_arg */
	, &InvokableCall_1_t2794_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2794_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2794_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2794)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2795_UnityAction_1__ctor_m14343_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14343_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14343_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2795_UnityAction_1__ctor_m14343_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14343_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
static ParameterInfo UnityAction_1_t2795_UnityAction_1_Invoke_m14344_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t14_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14344_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14344_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2795_UnityAction_1_Invoke_m14344_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14344_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t14_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2795_UnityAction_1_BeginInvoke_m14345_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t14_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14345_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14345_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2795_UnityAction_1_BeginInvoke_m14345_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14345_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2795_UnityAction_1_EndInvoke_m14346_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14346_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14346_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2795_UnityAction_1_EndInvoke_m14346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14346_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2795_MethodInfos[] =
{
	&UnityAction_1__ctor_m14343_MethodInfo,
	&UnityAction_1_Invoke_m14344_MethodInfo,
	&UnityAction_1_BeginInvoke_m14345_MethodInfo,
	&UnityAction_1_EndInvoke_m14346_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14345_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14346_MethodInfo;
static MethodInfo* UnityAction_1_t2795_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14344_MethodInfo,
	&UnityAction_1_BeginInvoke_m14345_MethodInfo,
	&UnityAction_1_EndInvoke_m14346_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2795_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2795_1_0_0;
struct UnityAction_1_t2795;
extern Il2CppGenericClass UnityAction_1_t2795_GenericClass;
TypeInfo UnityAction_1_t2795_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2795_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2795_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2795_0_0_0/* byval_arg */
	, &UnityAction_1_t2795_1_0_0/* this_arg */
	, UnityAction_1_t2795_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2795_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2795)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<System.Object>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2796_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<System.Object>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_0MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<System.Object>
extern Il2CppType Object_t_0_0_6;
FieldInfo CastHelper_1_t2796____t_0_FieldInfo = 
{
	"t"/* name */
	, &Object_t_0_0_6/* type */
	, &CastHelper_1_t2796_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2796, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2796____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2796_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2796, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2796_FieldInfos[] =
{
	&CastHelper_1_t2796____t_0_FieldInfo,
	&CastHelper_1_t2796____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2796_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2796_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2796_0_0_0;
extern Il2CppType CastHelper_1_t2796_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2796_GenericClass;
TypeInfo CastHelper_1_t2796_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2796_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2796_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2796_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2796_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2796_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2796_0_0_0/* byval_arg */
	, &CastHelper_1_t2796_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2796_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2796)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2797_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_genMethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType ReconstructionBehaviour_t11_0_0_6;
FieldInfo CastHelper_1_t2797____t_0_FieldInfo = 
{
	"t"/* name */
	, &ReconstructionBehaviour_t11_0_0_6/* type */
	, &CastHelper_1_t2797_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2797, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2797____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2797_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2797, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2797_FieldInfos[] =
{
	&CastHelper_1_t2797____t_0_FieldInfo,
	&CastHelper_1_t2797____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2797_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2797_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2797_0_0_0;
extern Il2CppType CastHelper_1_t2797_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2797_GenericClass;
TypeInfo CastHelper_1_t2797_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2797_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2797_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2797_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2797_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2797_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2797_0_0_0/* byval_arg */
	, &CastHelper_1_t2797_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2797_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2797)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Action`1<Vuforia.Prop>
#include "mscorlib_System_Action_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Action_1_t126_il2cpp_TypeInfo;
// System.Action`1<Vuforia.Prop>
#include "mscorlib_System_Action_1_gen_0MethodDeclarations.h"



// System.Void System.Action`1<Vuforia.Prop>::.ctor(System.Object,System.IntPtr)
// System.Void System.Action`1<Vuforia.Prop>::Invoke(T)
// System.IAsyncResult System.Action`1<Vuforia.Prop>::BeginInvoke(T,System.AsyncCallback,System.Object)
// System.Void System.Action`1<Vuforia.Prop>::EndInvoke(System.IAsyncResult)
// Metadata Definition System.Action`1<Vuforia.Prop>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Action_1_t126_Action_1__ctor_m272_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1__ctor_m272_GenericMethod;
// System.Void System.Action`1<Vuforia.Prop>::.ctor(System.Object,System.IntPtr)
MethodInfo Action_1__ctor_m272_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action_1__ctor_m14347_gshared/* method */
	, &Action_1_t126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Action_1_t126_Action_1__ctor_m272_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1__ctor_m272_GenericMethod/* genericMethod */

};
extern Il2CppType Prop_t15_0_0_0;
extern Il2CppType Prop_t15_0_0_0;
static ParameterInfo Action_1_t126_Action_1_Invoke_m5335_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Prop_t15_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_Invoke_m5335_GenericMethod;
// System.Void System.Action`1<Vuforia.Prop>::Invoke(T)
MethodInfo Action_1_Invoke_m5335_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_1_Invoke_m14348_gshared/* method */
	, &Action_1_t126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Action_1_t126_Action_1_Invoke_m5335_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_Invoke_m5335_GenericMethod/* genericMethod */

};
extern Il2CppType Prop_t15_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_1_t126_Action_1_BeginInvoke_m14349_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Prop_t15_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_BeginInvoke_m14349_GenericMethod;
// System.IAsyncResult System.Action`1<Vuforia.Prop>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Action_1_BeginInvoke_m14349_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_1_BeginInvoke_m14350_gshared/* method */
	, &Action_1_t126_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Action_1_t126_Action_1_BeginInvoke_m14349_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_BeginInvoke_m14349_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Action_1_t126_Action_1_EndInvoke_m14351_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_EndInvoke_m14351_GenericMethod;
// System.Void System.Action`1<Vuforia.Prop>::EndInvoke(System.IAsyncResult)
MethodInfo Action_1_EndInvoke_m14351_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_1_EndInvoke_m14352_gshared/* method */
	, &Action_1_t126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Action_1_t126_Action_1_EndInvoke_m14351_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_EndInvoke_m14351_GenericMethod/* genericMethod */

};
static MethodInfo* Action_1_t126_MethodInfos[] =
{
	&Action_1__ctor_m272_MethodInfo,
	&Action_1_Invoke_m5335_MethodInfo,
	&Action_1_BeginInvoke_m14349_MethodInfo,
	&Action_1_EndInvoke_m14351_MethodInfo,
	NULL
};
extern MethodInfo Action_1_Invoke_m5335_MethodInfo;
extern MethodInfo Action_1_BeginInvoke_m14349_MethodInfo;
extern MethodInfo Action_1_EndInvoke_m14351_MethodInfo;
static MethodInfo* Action_1_t126_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Action_1_Invoke_m5335_MethodInfo,
	&Action_1_BeginInvoke_m14349_MethodInfo,
	&Action_1_EndInvoke_m14351_MethodInfo,
};
static Il2CppInterfaceOffsetPair Action_1_t126_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Action_1_t126_0_0_0;
extern Il2CppType Action_1_t126_1_0_0;
struct Action_1_t126;
extern Il2CppGenericClass Action_1_t126_GenericClass;
TypeInfo Action_1_t126_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t126_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Action_1_t126_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Action_1_t126_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Action_1_t126_il2cpp_TypeInfo/* cast_class */
	, &Action_1_t126_0_0_0/* byval_arg */
	, &Action_1_t126_1_0_0/* this_arg */
	, Action_1_t126_InterfacesOffsets/* interface_offsets */
	, &Action_1_t126_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_1_t126)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_9.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Action_1_t2798_il2cpp_TypeInfo;
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_9MethodDeclarations.h"



// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern MethodInfo Action_1__ctor_m14347_MethodInfo;
 void Action_1__ctor_m14347_gshared (Action_1_t2798 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern MethodInfo Action_1_Invoke_m14348_MethodInfo;
 void Action_1_Invoke_m14348_gshared (Action_1_t2798 * __this, Object_t * ___obj, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Action_1_Invoke_m14348((Action_1_t2798 *)__this->___prev_9,___obj, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___obj, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___obj, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern MethodInfo Action_1_BeginInvoke_m14350_MethodInfo;
 Object_t * Action_1_BeginInvoke_m14350_gshared (Action_1_t2798 * __this, Object_t * ___obj, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern MethodInfo Action_1_EndInvoke_m14352_MethodInfo;
 void Action_1_EndInvoke_m14352_gshared (Action_1_t2798 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Metadata Definition System.Action`1<System.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Action_1_t2798_Action_1__ctor_m14347_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1__ctor_m14347_GenericMethod;
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
MethodInfo Action_1__ctor_m14347_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action_1__ctor_m14347_gshared/* method */
	, &Action_1_t2798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Action_1_t2798_Action_1__ctor_m14347_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1__ctor_m14347_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_1_t2798_Action_1_Invoke_m14348_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_Invoke_m14348_GenericMethod;
// System.Void System.Action`1<System.Object>::Invoke(T)
MethodInfo Action_1_Invoke_m14348_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_1_Invoke_m14348_gshared/* method */
	, &Action_1_t2798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Action_1_t2798_Action_1_Invoke_m14348_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_Invoke_m14348_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_1_t2798_Action_1_BeginInvoke_m14350_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_BeginInvoke_m14350_GenericMethod;
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Action_1_BeginInvoke_m14350_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_1_BeginInvoke_m14350_gshared/* method */
	, &Action_1_t2798_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Action_1_t2798_Action_1_BeginInvoke_m14350_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_BeginInvoke_m14350_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Action_1_t2798_Action_1_EndInvoke_m14352_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_EndInvoke_m14352_GenericMethod;
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
MethodInfo Action_1_EndInvoke_m14352_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_1_EndInvoke_m14352_gshared/* method */
	, &Action_1_t2798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Action_1_t2798_Action_1_EndInvoke_m14352_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_EndInvoke_m14352_GenericMethod/* genericMethod */

};
static MethodInfo* Action_1_t2798_MethodInfos[] =
{
	&Action_1__ctor_m14347_MethodInfo,
	&Action_1_Invoke_m14348_MethodInfo,
	&Action_1_BeginInvoke_m14350_MethodInfo,
	&Action_1_EndInvoke_m14352_MethodInfo,
	NULL
};
static MethodInfo* Action_1_t2798_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Action_1_Invoke_m14348_MethodInfo,
	&Action_1_BeginInvoke_m14350_MethodInfo,
	&Action_1_EndInvoke_m14352_MethodInfo,
};
static Il2CppInterfaceOffsetPair Action_1_t2798_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Action_1_t2798_0_0_0;
extern Il2CppType Action_1_t2798_1_0_0;
struct Action_1_t2798;
extern Il2CppGenericClass Action_1_t2798_GenericClass;
TypeInfo Action_1_t2798_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t2798_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Action_1_t2798_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Action_1_t2798_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Action_1_t2798_il2cpp_TypeInfo/* cast_class */
	, &Action_1_t2798_0_0_0/* byval_arg */
	, &Action_1_t2798_1_0_0/* this_arg */
	, Action_1_t2798_InterfacesOffsets/* interface_offsets */
	, &Action_1_t2798_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_1_t2798)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Action`1<Vuforia.Surface>
#include "mscorlib_System_Action_1_gen_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Action_1_t127_il2cpp_TypeInfo;
// System.Action`1<Vuforia.Surface>
#include "mscorlib_System_Action_1_gen_1MethodDeclarations.h"



// System.Void System.Action`1<Vuforia.Surface>::.ctor(System.Object,System.IntPtr)
// System.Void System.Action`1<Vuforia.Surface>::Invoke(T)
// System.IAsyncResult System.Action`1<Vuforia.Surface>::BeginInvoke(T,System.AsyncCallback,System.Object)
// System.Void System.Action`1<Vuforia.Surface>::EndInvoke(System.IAsyncResult)
// Metadata Definition System.Action`1<Vuforia.Surface>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Action_1_t127_Action_1__ctor_m274_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1__ctor_m274_GenericMethod;
// System.Void System.Action`1<Vuforia.Surface>::.ctor(System.Object,System.IntPtr)
MethodInfo Action_1__ctor_m274_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action_1__ctor_m14347_gshared/* method */
	, &Action_1_t127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Action_1_t127_Action_1__ctor_m274_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1__ctor_m274_GenericMethod/* genericMethod */

};
extern Il2CppType Surface_t16_0_0_0;
extern Il2CppType Surface_t16_0_0_0;
static ParameterInfo Action_1_t127_Action_1_Invoke_m5332_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Surface_t16_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_Invoke_m5332_GenericMethod;
// System.Void System.Action`1<Vuforia.Surface>::Invoke(T)
MethodInfo Action_1_Invoke_m5332_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_1_Invoke_m14348_gshared/* method */
	, &Action_1_t127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Action_1_t127_Action_1_Invoke_m5332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_Invoke_m5332_GenericMethod/* genericMethod */

};
extern Il2CppType Surface_t16_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_1_t127_Action_1_BeginInvoke_m14353_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Surface_t16_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_BeginInvoke_m14353_GenericMethod;
// System.IAsyncResult System.Action`1<Vuforia.Surface>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Action_1_BeginInvoke_m14353_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_1_BeginInvoke_m14350_gshared/* method */
	, &Action_1_t127_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Action_1_t127_Action_1_BeginInvoke_m14353_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_BeginInvoke_m14353_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Action_1_t127_Action_1_EndInvoke_m14354_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_EndInvoke_m14354_GenericMethod;
// System.Void System.Action`1<Vuforia.Surface>::EndInvoke(System.IAsyncResult)
MethodInfo Action_1_EndInvoke_m14354_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_1_EndInvoke_m14352_gshared/* method */
	, &Action_1_t127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Action_1_t127_Action_1_EndInvoke_m14354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_EndInvoke_m14354_GenericMethod/* genericMethod */

};
static MethodInfo* Action_1_t127_MethodInfos[] =
{
	&Action_1__ctor_m274_MethodInfo,
	&Action_1_Invoke_m5332_MethodInfo,
	&Action_1_BeginInvoke_m14353_MethodInfo,
	&Action_1_EndInvoke_m14354_MethodInfo,
	NULL
};
extern MethodInfo Action_1_Invoke_m5332_MethodInfo;
extern MethodInfo Action_1_BeginInvoke_m14353_MethodInfo;
extern MethodInfo Action_1_EndInvoke_m14354_MethodInfo;
static MethodInfo* Action_1_t127_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Action_1_Invoke_m5332_MethodInfo,
	&Action_1_BeginInvoke_m14353_MethodInfo,
	&Action_1_EndInvoke_m14354_MethodInfo,
};
static Il2CppInterfaceOffsetPair Action_1_t127_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Action_1_t127_0_0_0;
extern Il2CppType Action_1_t127_1_0_0;
struct Action_1_t127;
extern Il2CppGenericClass Action_1_t127_GenericClass;
TypeInfo Action_1_t127_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t127_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Action_1_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Action_1_t127_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Action_1_t127_il2cpp_TypeInfo/* cast_class */
	, &Action_1_t127_0_0_0/* byval_arg */
	, &Action_1_t127_1_0_0/* this_arg */
	, Action_1_t127_InterfacesOffsets/* interface_offsets */
	, &Action_1_t127_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_1_t127)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5973_il2cpp_TypeInfo;

// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultTrackableEventHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DefaultTrackableEventHandler>
extern MethodInfo IEnumerator_1_get_Current_m42688_MethodInfo;
static PropertyInfo IEnumerator_1_t5973____Current_PropertyInfo = 
{
	&IEnumerator_1_t5973_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42688_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5973_PropertyInfos[] =
{
	&IEnumerator_1_t5973____Current_PropertyInfo,
	NULL
};
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42688_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultTrackableEventHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42688_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5973_il2cpp_TypeInfo/* declaring_type */
	, &DefaultTrackableEventHandler_t18_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42688_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5973_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42688_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5973_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5973_0_0_0;
extern Il2CppType IEnumerator_1_t5973_1_0_0;
struct IEnumerator_1_t5973;
extern Il2CppGenericClass IEnumerator_1_t5973_GenericClass;
TypeInfo IEnumerator_1_t5973_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5973_MethodInfos/* methods */
	, IEnumerator_1_t5973_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5973_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5973_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5973_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5973_0_0_0/* byval_arg */
	, &IEnumerator_1_t5973_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5973_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_23.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2799_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_23MethodDeclarations.h"

extern TypeInfo DefaultTrackableEventHandler_t18_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14359_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDefaultTrackableEventHandler_t18_m32504_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultTrackableEventHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultTrackableEventHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisDefaultTrackableEventHandler_t18_m32504(__this, p0, method) (DefaultTrackableEventHandler_t18 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2799____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2799_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2799, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2799____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2799_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2799, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2799_FieldInfos[] =
{
	&InternalEnumerator_1_t2799____array_0_FieldInfo,
	&InternalEnumerator_1_t2799____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14356_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2799____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2799_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14356_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2799____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2799_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14359_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2799_PropertyInfos[] =
{
	&InternalEnumerator_1_t2799____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2799____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2799_InternalEnumerator_1__ctor_m14355_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14355_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14355_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2799_InternalEnumerator_1__ctor_m14355_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14355_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14356_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14356_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2799_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14356_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14357_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14357_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14357_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14358_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14358_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2799_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14358_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14359_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14359_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2799_il2cpp_TypeInfo/* declaring_type */
	, &DefaultTrackableEventHandler_t18_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14359_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2799_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14355_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14356_MethodInfo,
	&InternalEnumerator_1_Dispose_m14357_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14358_MethodInfo,
	&InternalEnumerator_1_get_Current_m14359_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14358_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14357_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2799_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14356_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14358_MethodInfo,
	&InternalEnumerator_1_Dispose_m14357_MethodInfo,
	&InternalEnumerator_1_get_Current_m14359_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2799_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5973_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2799_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5973_il2cpp_TypeInfo, 7},
};
extern TypeInfo DefaultTrackableEventHandler_t18_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2799_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14359_MethodInfo/* Method Usage */,
	&DefaultTrackableEventHandler_t18_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDefaultTrackableEventHandler_t18_m32504_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2799_0_0_0;
extern Il2CppType InternalEnumerator_1_t2799_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2799_GenericClass;
TypeInfo InternalEnumerator_1_t2799_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2799_MethodInfos/* methods */
	, InternalEnumerator_1_t2799_PropertyInfos/* properties */
	, InternalEnumerator_1_t2799_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2799_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2799_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2799_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2799_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2799_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2799_1_0_0/* this_arg */
	, InternalEnumerator_1_t2799_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2799_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2799_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2799)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7593_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>
extern MethodInfo ICollection_1_get_Count_m42689_MethodInfo;
static PropertyInfo ICollection_1_t7593____Count_PropertyInfo = 
{
	&ICollection_1_t7593_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42689_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42690_MethodInfo;
static PropertyInfo ICollection_1_t7593____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7593_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42690_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7593_PropertyInfos[] =
{
	&ICollection_1_t7593____Count_PropertyInfo,
	&ICollection_1_t7593____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42689_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m42689_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7593_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42689_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42690_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42690_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7593_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42690_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
static ParameterInfo ICollection_1_t7593_ICollection_1_Add_m42691_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t18_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42691_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Add(T)
MethodInfo ICollection_1_Add_m42691_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7593_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7593_ICollection_1_Add_m42691_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42691_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42692_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Clear()
MethodInfo ICollection_1_Clear_m42692_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7593_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42692_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
static ParameterInfo ICollection_1_t7593_ICollection_1_Contains_m42693_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t18_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42693_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m42693_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7593_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7593_ICollection_1_Contains_m42693_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42693_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandlerU5BU5D_t5364_0_0_0;
extern Il2CppType DefaultTrackableEventHandlerU5BU5D_t5364_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7593_ICollection_1_CopyTo_m42694_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandlerU5BU5D_t5364_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42694_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42694_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7593_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7593_ICollection_1_CopyTo_m42694_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42694_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
static ParameterInfo ICollection_1_t7593_ICollection_1_Remove_m42695_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t18_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42695_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m42695_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7593_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7593_ICollection_1_Remove_m42695_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42695_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7593_MethodInfos[] =
{
	&ICollection_1_get_Count_m42689_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42690_MethodInfo,
	&ICollection_1_Add_m42691_MethodInfo,
	&ICollection_1_Clear_m42692_MethodInfo,
	&ICollection_1_Contains_m42693_MethodInfo,
	&ICollection_1_CopyTo_m42694_MethodInfo,
	&ICollection_1_Remove_m42695_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7595_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7593_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7595_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7593_0_0_0;
extern Il2CppType ICollection_1_t7593_1_0_0;
struct ICollection_1_t7593;
extern Il2CppGenericClass ICollection_1_t7593_GenericClass;
TypeInfo ICollection_1_t7593_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7593_MethodInfos/* methods */
	, ICollection_1_t7593_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7593_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7593_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7593_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7593_0_0_0/* byval_arg */
	, &ICollection_1_t7593_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7593_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultTrackableEventHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DefaultTrackableEventHandler>
extern Il2CppType IEnumerator_1_t5973_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42696_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultTrackableEventHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42696_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7595_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5973_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42696_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7595_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42696_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7595_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7595_0_0_0;
extern Il2CppType IEnumerable_1_t7595_1_0_0;
struct IEnumerable_1_t7595;
extern Il2CppGenericClass IEnumerable_1_t7595_GenericClass;
TypeInfo IEnumerable_1_t7595_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7595_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7595_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7595_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7595_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7595_0_0_0/* byval_arg */
	, &IEnumerable_1_t7595_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7595_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7594_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>
extern MethodInfo IList_1_get_Item_m42697_MethodInfo;
extern MethodInfo IList_1_set_Item_m42698_MethodInfo;
static PropertyInfo IList_1_t7594____Item_PropertyInfo = 
{
	&IList_1_t7594_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42697_MethodInfo/* get */
	, &IList_1_set_Item_m42698_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7594_PropertyInfos[] =
{
	&IList_1_t7594____Item_PropertyInfo,
	NULL
};
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
static ParameterInfo IList_1_t7594_IList_1_IndexOf_m42699_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t18_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42699_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42699_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7594_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7594_IList_1_IndexOf_m42699_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42699_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
static ParameterInfo IList_1_t7594_IList_1_Insert_m42700_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t18_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42700_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42700_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7594_IList_1_Insert_m42700_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42700_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7594_IList_1_RemoveAt_m42701_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42701_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42701_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7594_IList_1_RemoveAt_m42701_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42701_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7594_IList_1_get_Item_m42697_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42697_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42697_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7594_il2cpp_TypeInfo/* declaring_type */
	, &DefaultTrackableEventHandler_t18_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7594_IList_1_get_Item_m42697_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42697_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
static ParameterInfo IList_1_t7594_IList_1_set_Item_m42698_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t18_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42698_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42698_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7594_IList_1_set_Item_m42698_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42698_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7594_MethodInfos[] =
{
	&IList_1_IndexOf_m42699_MethodInfo,
	&IList_1_Insert_m42700_MethodInfo,
	&IList_1_RemoveAt_m42701_MethodInfo,
	&IList_1_get_Item_m42697_MethodInfo,
	&IList_1_set_Item_m42698_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7594_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7593_il2cpp_TypeInfo,
	&IEnumerable_1_t7595_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7594_0_0_0;
extern Il2CppType IList_1_t7594_1_0_0;
struct IList_1_t7594;
extern Il2CppGenericClass IList_1_t7594_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7594_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7594_MethodInfos/* methods */
	, IList_1_t7594_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7594_il2cpp_TypeInfo/* element_class */
	, IList_1_t7594_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7594_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7594_0_0_0/* byval_arg */
	, &IList_1_t7594_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7594_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t3837_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>
extern MethodInfo ICollection_1_get_Count_m42702_MethodInfo;
static PropertyInfo ICollection_1_t3837____Count_PropertyInfo = 
{
	&ICollection_1_t3837_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42702_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42703_MethodInfo;
static PropertyInfo ICollection_1_t3837____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t3837_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42703_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t3837_PropertyInfos[] =
{
	&ICollection_1_t3837____Count_PropertyInfo,
	&ICollection_1_t3837____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42702_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m42702_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t3837_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42702_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42703_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42703_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t3837_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42703_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackableEventHandler_t134_0_0_0;
extern Il2CppType ITrackableEventHandler_t134_0_0_0;
static ParameterInfo ICollection_1_t3837_ICollection_1_Add_m42704_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t134_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42704_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Add(T)
MethodInfo ICollection_1_Add_m42704_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t3837_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t3837_ICollection_1_Add_m42704_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42704_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42705_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Clear()
MethodInfo ICollection_1_Clear_m42705_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t3837_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42705_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackableEventHandler_t134_0_0_0;
static ParameterInfo ICollection_1_t3837_ICollection_1_Contains_m42706_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t134_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42706_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m42706_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t3837_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t3837_ICollection_1_Contains_m42706_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42706_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackableEventHandlerU5BU5D_t3834_0_0_0;
extern Il2CppType ITrackableEventHandlerU5BU5D_t3834_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t3837_ICollection_1_CopyTo_m42707_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandlerU5BU5D_t3834_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42707_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42707_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t3837_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t3837_ICollection_1_CopyTo_m42707_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42707_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackableEventHandler_t134_0_0_0;
static ParameterInfo ICollection_1_t3837_ICollection_1_Remove_m42708_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t134_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42708_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m42708_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t3837_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t3837_ICollection_1_Remove_m42708_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42708_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t3837_MethodInfos[] =
{
	&ICollection_1_get_Count_m42702_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42703_MethodInfo,
	&ICollection_1_Add_m42704_MethodInfo,
	&ICollection_1_Clear_m42705_MethodInfo,
	&ICollection_1_Contains_m42706_MethodInfo,
	&ICollection_1_CopyTo_m42707_MethodInfo,
	&ICollection_1_Remove_m42708_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t3835_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t3837_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t3835_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t3837_0_0_0;
extern Il2CppType ICollection_1_t3837_1_0_0;
struct ICollection_1_t3837;
extern Il2CppGenericClass ICollection_1_t3837_GenericClass;
TypeInfo ICollection_1_t3837_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t3837_MethodInfos/* methods */
	, ICollection_1_t3837_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t3837_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t3837_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t3837_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t3837_0_0_0/* byval_arg */
	, &ICollection_1_t3837_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t3837_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ITrackableEventHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ITrackableEventHandler>
extern Il2CppType IEnumerator_1_t3836_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42709_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ITrackableEventHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42709_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t3835_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3836_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42709_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t3835_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42709_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t3835_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t3835_0_0_0;
extern Il2CppType IEnumerable_1_t3835_1_0_0;
struct IEnumerable_1_t3835;
extern Il2CppGenericClass IEnumerable_1_t3835_GenericClass;
TypeInfo IEnumerable_1_t3835_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t3835_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t3835_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t3835_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t3835_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t3835_0_0_0/* byval_arg */
	, &IEnumerable_1_t3835_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t3835_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t3836_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.ITrackableEventHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ITrackableEventHandler>
extern MethodInfo IEnumerator_1_get_Current_m42710_MethodInfo;
static PropertyInfo IEnumerator_1_t3836____Current_PropertyInfo = 
{
	&IEnumerator_1_t3836_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42710_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t3836_PropertyInfos[] =
{
	&IEnumerator_1_t3836____Current_PropertyInfo,
	NULL
};
extern Il2CppType ITrackableEventHandler_t134_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42710_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ITrackableEventHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42710_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t3836_il2cpp_TypeInfo/* declaring_type */
	, &ITrackableEventHandler_t134_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42710_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t3836_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42710_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t3836_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t3836_0_0_0;
extern Il2CppType IEnumerator_1_t3836_1_0_0;
struct IEnumerator_1_t3836;
extern Il2CppGenericClass IEnumerator_1_t3836_GenericClass;
TypeInfo IEnumerator_1_t3836_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t3836_MethodInfos/* methods */
	, IEnumerator_1_t3836_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t3836_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t3836_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t3836_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t3836_0_0_0/* byval_arg */
	, &IEnumerator_1_t3836_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t3836_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_24.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2800_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_24MethodDeclarations.h"

extern TypeInfo ITrackableEventHandler_t134_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14364_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisITrackableEventHandler_t134_m32515_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ITrackableEventHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ITrackableEventHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisITrackableEventHandler_t134_m32515(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2800____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2800_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2800, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2800____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2800_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2800, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2800_FieldInfos[] =
{
	&InternalEnumerator_1_t2800____array_0_FieldInfo,
	&InternalEnumerator_1_t2800____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14361_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2800____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2800_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14361_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2800____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2800_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14364_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2800_PropertyInfos[] =
{
	&InternalEnumerator_1_t2800____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2800____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2800_InternalEnumerator_1__ctor_m14360_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14360_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14360_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2800_InternalEnumerator_1__ctor_m14360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14360_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14361_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14361_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2800_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14361_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14362_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14362_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14362_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14363_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14363_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2800_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14363_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackableEventHandler_t134_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14364_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14364_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2800_il2cpp_TypeInfo/* declaring_type */
	, &ITrackableEventHandler_t134_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14364_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2800_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14360_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14361_MethodInfo,
	&InternalEnumerator_1_Dispose_m14362_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14363_MethodInfo,
	&InternalEnumerator_1_get_Current_m14364_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14363_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14362_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2800_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14361_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14363_MethodInfo,
	&InternalEnumerator_1_Dispose_m14362_MethodInfo,
	&InternalEnumerator_1_get_Current_m14364_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2800_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t3836_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2800_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3836_il2cpp_TypeInfo, 7},
};
extern TypeInfo ITrackableEventHandler_t134_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2800_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14364_MethodInfo/* Method Usage */,
	&ITrackableEventHandler_t134_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisITrackableEventHandler_t134_m32515_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2800_0_0_0;
extern Il2CppType InternalEnumerator_1_t2800_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2800_GenericClass;
TypeInfo InternalEnumerator_1_t2800_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2800_MethodInfos/* methods */
	, InternalEnumerator_1_t2800_PropertyInfos/* properties */
	, InternalEnumerator_1_t2800_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2800_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2800_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2800_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2800_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2800_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2800_1_0_0/* this_arg */
	, InternalEnumerator_1_t2800_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2800_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2800_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2800)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t3841_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>
extern MethodInfo IList_1_get_Item_m42711_MethodInfo;
extern MethodInfo IList_1_set_Item_m42712_MethodInfo;
static PropertyInfo IList_1_t3841____Item_PropertyInfo = 
{
	&IList_1_t3841_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42711_MethodInfo/* get */
	, &IList_1_set_Item_m42712_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t3841_PropertyInfos[] =
{
	&IList_1_t3841____Item_PropertyInfo,
	NULL
};
extern Il2CppType ITrackableEventHandler_t134_0_0_0;
static ParameterInfo IList_1_t3841_IList_1_IndexOf_m42713_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t134_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42713_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42713_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t3841_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t3841_IList_1_IndexOf_m42713_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42713_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ITrackableEventHandler_t134_0_0_0;
static ParameterInfo IList_1_t3841_IList_1_Insert_m42714_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t134_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42714_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42714_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t3841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t3841_IList_1_Insert_m42714_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42714_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t3841_IList_1_RemoveAt_m42715_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42715_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42715_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t3841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t3841_IList_1_RemoveAt_m42715_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42715_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t3841_IList_1_get_Item_m42711_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ITrackableEventHandler_t134_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42711_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42711_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t3841_il2cpp_TypeInfo/* declaring_type */
	, &ITrackableEventHandler_t134_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t3841_IList_1_get_Item_m42711_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42711_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ITrackableEventHandler_t134_0_0_0;
static ParameterInfo IList_1_t3841_IList_1_set_Item_m42712_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t134_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42712_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42712_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t3841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t3841_IList_1_set_Item_m42712_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42712_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t3841_MethodInfos[] =
{
	&IList_1_IndexOf_m42713_MethodInfo,
	&IList_1_Insert_m42714_MethodInfo,
	&IList_1_RemoveAt_m42715_MethodInfo,
	&IList_1_get_Item_m42711_MethodInfo,
	&IList_1_set_Item_m42712_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t3841_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t3837_il2cpp_TypeInfo,
	&IEnumerable_1_t3835_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t3841_0_0_0;
extern Il2CppType IList_1_t3841_1_0_0;
struct IList_1_t3841;
extern Il2CppGenericClass IList_1_t3841_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t3841_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t3841_MethodInfos/* methods */
	, IList_1_t3841_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t3841_il2cpp_TypeInfo/* element_class */
	, IList_1_t3841_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t3841_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t3841_0_0_0/* byval_arg */
	, &IList_1_t3841_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t3841_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_10.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2801_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_10MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_6.h"
extern TypeInfo InvokableCall_1_t2802_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_6MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14367_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14369_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2801____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2801_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2801, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2801_FieldInfos[] =
{
	&CachedInvokableCall_1_t2801____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2801_CachedInvokableCall_1__ctor_m14365_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t18_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14365_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14365_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2801_CachedInvokableCall_1__ctor_m14365_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14365_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2801_CachedInvokableCall_1_Invoke_m14366_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14366_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14366_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2801_CachedInvokableCall_1_Invoke_m14366_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14366_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2801_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14365_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14366_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14366_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14370_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2801_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14366_MethodInfo,
	&InvokableCall_1_Find_m14370_MethodInfo,
};
extern Il2CppType UnityAction_1_t2803_0_0_0;
extern TypeInfo UnityAction_1_t2803_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisDefaultTrackableEventHandler_t18_m32525_MethodInfo;
extern TypeInfo DefaultTrackableEventHandler_t18_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14372_MethodInfo;
extern TypeInfo DefaultTrackableEventHandler_t18_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2801_RGCTXData[8] = 
{
	&UnityAction_1_t2803_0_0_0/* Type Usage */,
	&UnityAction_1_t2803_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultTrackableEventHandler_t18_m32525_MethodInfo/* Method Usage */,
	&DefaultTrackableEventHandler_t18_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14372_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14367_MethodInfo/* Method Usage */,
	&DefaultTrackableEventHandler_t18_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14369_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2801_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2801_1_0_0;
struct CachedInvokableCall_1_t2801;
extern Il2CppGenericClass CachedInvokableCall_1_t2801_GenericClass;
TypeInfo CachedInvokableCall_1_t2801_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2801_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2801_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2802_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2801_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2801_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2801_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2801_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2801_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2801_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2801_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2801)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_13.h"
extern TypeInfo UnityAction_1_t2803_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_13MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultTrackableEventHandler>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultTrackableEventHandler>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisDefaultTrackableEventHandler_t18_m32525(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>
extern Il2CppType UnityAction_1_t2803_0_0_1;
FieldInfo InvokableCall_1_t2802____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2803_0_0_1/* type */
	, &InvokableCall_1_t2802_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2802, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2802_FieldInfos[] =
{
	&InvokableCall_1_t2802____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2802_InvokableCall_1__ctor_m14367_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14367_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14367_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2802_InvokableCall_1__ctor_m14367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14367_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2803_0_0_0;
static ParameterInfo InvokableCall_1_t2802_InvokableCall_1__ctor_m14368_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2803_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14368_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14368_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2802_InvokableCall_1__ctor_m14368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14368_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2802_InvokableCall_1_Invoke_m14369_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14369_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14369_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2802_InvokableCall_1_Invoke_m14369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14369_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2802_InvokableCall_1_Find_m14370_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14370_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14370_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2802_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2802_InvokableCall_1_Find_m14370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14370_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2802_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14367_MethodInfo,
	&InvokableCall_1__ctor_m14368_MethodInfo,
	&InvokableCall_1_Invoke_m14369_MethodInfo,
	&InvokableCall_1_Find_m14370_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2802_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14369_MethodInfo,
	&InvokableCall_1_Find_m14370_MethodInfo,
};
extern TypeInfo UnityAction_1_t2803_il2cpp_TypeInfo;
extern TypeInfo DefaultTrackableEventHandler_t18_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2802_RGCTXData[5] = 
{
	&UnityAction_1_t2803_0_0_0/* Type Usage */,
	&UnityAction_1_t2803_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultTrackableEventHandler_t18_m32525_MethodInfo/* Method Usage */,
	&DefaultTrackableEventHandler_t18_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14372_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2802_0_0_0;
extern Il2CppType InvokableCall_1_t2802_1_0_0;
struct InvokableCall_1_t2802;
extern Il2CppGenericClass InvokableCall_1_t2802_GenericClass;
TypeInfo InvokableCall_1_t2802_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2802_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2802_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2802_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2802_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2802_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2802_0_0_0/* byval_arg */
	, &InvokableCall_1_t2802_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2802_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2802_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2802)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2803_UnityAction_1__ctor_m14371_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14371_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14371_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2803_UnityAction_1__ctor_m14371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14371_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
static ParameterInfo UnityAction_1_t2803_UnityAction_1_Invoke_m14372_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t18_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14372_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14372_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2803_UnityAction_1_Invoke_m14372_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14372_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t18_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2803_UnityAction_1_BeginInvoke_m14373_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t18_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14373_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14373_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2803_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2803_UnityAction_1_BeginInvoke_m14373_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14373_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2803_UnityAction_1_EndInvoke_m14374_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14374_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14374_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2803_UnityAction_1_EndInvoke_m14374_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14374_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2803_MethodInfos[] =
{
	&UnityAction_1__ctor_m14371_MethodInfo,
	&UnityAction_1_Invoke_m14372_MethodInfo,
	&UnityAction_1_BeginInvoke_m14373_MethodInfo,
	&UnityAction_1_EndInvoke_m14374_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14373_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14374_MethodInfo;
static MethodInfo* UnityAction_1_t2803_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14372_MethodInfo,
	&UnityAction_1_BeginInvoke_m14373_MethodInfo,
	&UnityAction_1_EndInvoke_m14374_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2803_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2803_1_0_0;
struct UnityAction_1_t2803;
extern Il2CppGenericClass UnityAction_1_t2803_GenericClass;
TypeInfo UnityAction_1_t2803_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2803_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2803_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2803_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2803_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2803_0_0_0/* byval_arg */
	, &UnityAction_1_t2803_1_0_0/* this_arg */
	, UnityAction_1_t2803_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2803_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2803)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<Vuforia.TrackableBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2804_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<Vuforia.TrackableBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_1MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<Vuforia.TrackableBehaviour>
extern Il2CppType TrackableBehaviour_t17_0_0_6;
FieldInfo CastHelper_1_t2804____t_0_FieldInfo = 
{
	"t"/* name */
	, &TrackableBehaviour_t17_0_0_6/* type */
	, &CastHelper_1_t2804_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2804, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2804____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2804_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2804, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2804_FieldInfos[] =
{
	&CastHelper_1_t2804____t_0_FieldInfo,
	&CastHelper_1_t2804____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2804_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2804_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2804_0_0_0;
extern Il2CppType CastHelper_1_t2804_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2804_GenericClass;
TypeInfo CastHelper_1_t2804_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2804_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2804_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2804_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2804_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2804_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2804_0_0_0/* byval_arg */
	, &CastHelper_1_t2804_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2804_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2804)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5976_il2cpp_TypeInfo;

// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Renderer>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Renderer>
extern MethodInfo IEnumerator_1_get_Current_m42716_MethodInfo;
static PropertyInfo IEnumerator_1_t5976____Current_PropertyInfo = 
{
	&IEnumerator_1_t5976_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42716_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5976_PropertyInfos[] =
{
	&IEnumerator_1_t5976____Current_PropertyInfo,
	NULL
};
extern Il2CppType Renderer_t129_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42716_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Renderer>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42716_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5976_il2cpp_TypeInfo/* declaring_type */
	, &Renderer_t129_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42716_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5976_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42716_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5976_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5976_0_0_0;
extern Il2CppType IEnumerator_1_t5976_1_0_0;
struct IEnumerator_1_t5976;
extern Il2CppGenericClass IEnumerator_1_t5976_GenericClass;
TypeInfo IEnumerator_1_t5976_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5976_MethodInfos/* methods */
	, IEnumerator_1_t5976_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5976_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5976_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5976_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5976_0_0_0/* byval_arg */
	, &IEnumerator_1_t5976_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5976_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Renderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_25.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2805_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Renderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_25MethodDeclarations.h"

extern TypeInfo Renderer_t129_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14379_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRenderer_t129_m32527_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Renderer>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Renderer>(System.Int32)
#define Array_InternalArray__get_Item_TisRenderer_t129_m32527(__this, p0, method) (Renderer_t129 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Renderer>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Renderer>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Renderer>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Renderer>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Renderer>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Renderer>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2805____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2805, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2805____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2805, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2805_FieldInfos[] =
{
	&InternalEnumerator_1_t2805____array_0_FieldInfo,
	&InternalEnumerator_1_t2805____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14376_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2805____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2805_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14376_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2805____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2805_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14379_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2805_PropertyInfos[] =
{
	&InternalEnumerator_1_t2805____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2805____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2805_InternalEnumerator_1__ctor_m14375_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14375_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Renderer>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14375_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2805_InternalEnumerator_1__ctor_m14375_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14375_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14376_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Renderer>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14376_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14376_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14377_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Renderer>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14377_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14377_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14378_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Renderer>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14378_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14378_GenericMethod/* genericMethod */

};
extern Il2CppType Renderer_t129_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14379_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Renderer>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14379_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* declaring_type */
	, &Renderer_t129_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14379_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2805_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14375_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14376_MethodInfo,
	&InternalEnumerator_1_Dispose_m14377_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14378_MethodInfo,
	&InternalEnumerator_1_get_Current_m14379_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14378_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14377_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2805_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14376_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14378_MethodInfo,
	&InternalEnumerator_1_Dispose_m14377_MethodInfo,
	&InternalEnumerator_1_get_Current_m14379_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2805_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5976_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2805_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5976_il2cpp_TypeInfo, 7},
};
extern TypeInfo Renderer_t129_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2805_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14379_MethodInfo/* Method Usage */,
	&Renderer_t129_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisRenderer_t129_m32527_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2805_0_0_0;
extern Il2CppType InternalEnumerator_1_t2805_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2805_GenericClass;
TypeInfo InternalEnumerator_1_t2805_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2805_MethodInfos/* methods */
	, InternalEnumerator_1_t2805_PropertyInfos/* properties */
	, InternalEnumerator_1_t2805_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2805_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2805_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2805_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2805_1_0_0/* this_arg */
	, InternalEnumerator_1_t2805_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2805_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2805_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2805)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7596_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Renderer>
extern MethodInfo ICollection_1_get_Count_m42717_MethodInfo;
static PropertyInfo ICollection_1_t7596____Count_PropertyInfo = 
{
	&ICollection_1_t7596_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42717_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42718_MethodInfo;
static PropertyInfo ICollection_1_t7596____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7596_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42718_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7596_PropertyInfos[] =
{
	&ICollection_1_t7596____Count_PropertyInfo,
	&ICollection_1_t7596____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42717_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::get_Count()
MethodInfo ICollection_1_get_Count_m42717_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7596_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42717_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42718_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42718_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7596_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42718_GenericMethod/* genericMethod */

};
extern Il2CppType Renderer_t129_0_0_0;
extern Il2CppType Renderer_t129_0_0_0;
static ParameterInfo ICollection_1_t7596_ICollection_1_Add_m42719_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Renderer_t129_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42719_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Add(T)
MethodInfo ICollection_1_Add_m42719_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7596_ICollection_1_Add_m42719_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42719_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42720_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Clear()
MethodInfo ICollection_1_Clear_m42720_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42720_GenericMethod/* genericMethod */

};
extern Il2CppType Renderer_t129_0_0_0;
static ParameterInfo ICollection_1_t7596_ICollection_1_Contains_m42721_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Renderer_t129_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42721_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Contains(T)
MethodInfo ICollection_1_Contains_m42721_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7596_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7596_ICollection_1_Contains_m42721_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42721_GenericMethod/* genericMethod */

};
extern Il2CppType RendererU5BU5D_t131_0_0_0;
extern Il2CppType RendererU5BU5D_t131_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7596_ICollection_1_CopyTo_m42722_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RendererU5BU5D_t131_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42722_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42722_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7596_ICollection_1_CopyTo_m42722_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42722_GenericMethod/* genericMethod */

};
extern Il2CppType Renderer_t129_0_0_0;
static ParameterInfo ICollection_1_t7596_ICollection_1_Remove_m42723_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Renderer_t129_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42723_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Remove(T)
MethodInfo ICollection_1_Remove_m42723_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7596_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7596_ICollection_1_Remove_m42723_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42723_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7596_MethodInfos[] =
{
	&ICollection_1_get_Count_m42717_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42718_MethodInfo,
	&ICollection_1_Add_m42719_MethodInfo,
	&ICollection_1_Clear_m42720_MethodInfo,
	&ICollection_1_Contains_m42721_MethodInfo,
	&ICollection_1_CopyTo_m42722_MethodInfo,
	&ICollection_1_Remove_m42723_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7598_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7596_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7598_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7596_0_0_0;
extern Il2CppType ICollection_1_t7596_1_0_0;
struct ICollection_1_t7596;
extern Il2CppGenericClass ICollection_1_t7596_GenericClass;
TypeInfo ICollection_1_t7596_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7596_MethodInfos/* methods */
	, ICollection_1_t7596_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7596_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7596_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7596_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7596_0_0_0/* byval_arg */
	, &ICollection_1_t7596_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7596_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Renderer>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Renderer>
extern Il2CppType IEnumerator_1_t5976_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42724_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Renderer>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42724_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7598_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5976_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42724_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7598_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42724_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7598_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7598_0_0_0;
extern Il2CppType IEnumerable_1_t7598_1_0_0;
struct IEnumerable_1_t7598;
extern Il2CppGenericClass IEnumerable_1_t7598_GenericClass;
TypeInfo IEnumerable_1_t7598_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7598_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7598_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7598_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7598_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7598_0_0_0/* byval_arg */
	, &IEnumerable_1_t7598_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7598_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7597_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Renderer>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Renderer>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Renderer>
extern MethodInfo IList_1_get_Item_m42725_MethodInfo;
extern MethodInfo IList_1_set_Item_m42726_MethodInfo;
static PropertyInfo IList_1_t7597____Item_PropertyInfo = 
{
	&IList_1_t7597_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42725_MethodInfo/* get */
	, &IList_1_set_Item_m42726_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7597_PropertyInfos[] =
{
	&IList_1_t7597____Item_PropertyInfo,
	NULL
};
extern Il2CppType Renderer_t129_0_0_0;
static ParameterInfo IList_1_t7597_IList_1_IndexOf_m42727_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Renderer_t129_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42727_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Renderer>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42727_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7597_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7597_IList_1_IndexOf_m42727_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42727_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Renderer_t129_0_0_0;
static ParameterInfo IList_1_t7597_IList_1_Insert_m42728_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Renderer_t129_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42728_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42728_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7597_IList_1_Insert_m42728_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42728_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7597_IList_1_RemoveAt_m42729_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42729_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42729_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7597_IList_1_RemoveAt_m42729_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42729_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7597_IList_1_get_Item_m42725_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Renderer_t129_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42725_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Renderer>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42725_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7597_il2cpp_TypeInfo/* declaring_type */
	, &Renderer_t129_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7597_IList_1_get_Item_m42725_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42725_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Renderer_t129_0_0_0;
static ParameterInfo IList_1_t7597_IList_1_set_Item_m42726_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Renderer_t129_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42726_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42726_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7597_IList_1_set_Item_m42726_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42726_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7597_MethodInfos[] =
{
	&IList_1_IndexOf_m42727_MethodInfo,
	&IList_1_Insert_m42728_MethodInfo,
	&IList_1_RemoveAt_m42729_MethodInfo,
	&IList_1_get_Item_m42725_MethodInfo,
	&IList_1_set_Item_m42726_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7597_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7596_il2cpp_TypeInfo,
	&IEnumerable_1_t7598_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7597_0_0_0;
extern Il2CppType IList_1_t7597_1_0_0;
struct IList_1_t7597;
extern Il2CppGenericClass IList_1_t7597_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7597_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7597_MethodInfos/* methods */
	, IList_1_t7597_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7597_il2cpp_TypeInfo/* element_class */
	, IList_1_t7597_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7597_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7597_0_0_0/* byval_arg */
	, &IList_1_t7597_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7597_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5978_il2cpp_TypeInfo;

// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Collider>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Collider>
extern MethodInfo IEnumerator_1_get_Current_m42730_MethodInfo;
static PropertyInfo IEnumerator_1_t5978____Current_PropertyInfo = 
{
	&IEnumerator_1_t5978_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42730_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5978_PropertyInfos[] =
{
	&IEnumerator_1_t5978____Current_PropertyInfo,
	NULL
};
extern Il2CppType Collider_t70_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42730_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Collider>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42730_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5978_il2cpp_TypeInfo/* declaring_type */
	, &Collider_t70_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42730_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5978_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42730_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5978_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5978_0_0_0;
extern Il2CppType IEnumerator_1_t5978_1_0_0;
struct IEnumerator_1_t5978;
extern Il2CppGenericClass IEnumerator_1_t5978_GenericClass;
TypeInfo IEnumerator_1_t5978_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5978_MethodInfos/* methods */
	, IEnumerator_1_t5978_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5978_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5978_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5978_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5978_0_0_0/* byval_arg */
	, &IEnumerator_1_t5978_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5978_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Collider>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_26.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2806_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Collider>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_26MethodDeclarations.h"

extern TypeInfo Collider_t70_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14384_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCollider_t70_m32538_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Collider>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Collider>(System.Int32)
#define Array_InternalArray__get_Item_TisCollider_t70_m32538(__this, p0, method) (Collider_t70 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Collider>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Collider>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Collider>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Collider>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2806____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2806, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2806____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2806, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2806_FieldInfos[] =
{
	&InternalEnumerator_1_t2806____array_0_FieldInfo,
	&InternalEnumerator_1_t2806____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14381_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2806____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2806_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14381_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2806____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2806_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14384_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2806_PropertyInfos[] =
{
	&InternalEnumerator_1_t2806____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2806____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2806_InternalEnumerator_1__ctor_m14380_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14380_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14380_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2806_InternalEnumerator_1__ctor_m14380_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14380_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14381_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Collider>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14381_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14381_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14382_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14382_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14382_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14383_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Collider>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14383_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14383_GenericMethod/* genericMethod */

};
extern Il2CppType Collider_t70_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14384_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Collider>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14384_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* declaring_type */
	, &Collider_t70_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14384_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2806_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14380_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14381_MethodInfo,
	&InternalEnumerator_1_Dispose_m14382_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14383_MethodInfo,
	&InternalEnumerator_1_get_Current_m14384_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14383_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14382_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2806_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14381_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14383_MethodInfo,
	&InternalEnumerator_1_Dispose_m14382_MethodInfo,
	&InternalEnumerator_1_get_Current_m14384_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2806_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5978_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2806_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5978_il2cpp_TypeInfo, 7},
};
extern TypeInfo Collider_t70_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2806_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14384_MethodInfo/* Method Usage */,
	&Collider_t70_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCollider_t70_m32538_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2806_0_0_0;
extern Il2CppType InternalEnumerator_1_t2806_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2806_GenericClass;
TypeInfo InternalEnumerator_1_t2806_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2806_MethodInfos/* methods */
	, InternalEnumerator_1_t2806_PropertyInfos/* properties */
	, InternalEnumerator_1_t2806_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2806_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2806_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2806_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2806_1_0_0/* this_arg */
	, InternalEnumerator_1_t2806_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2806_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2806_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2806)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7599_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Collider>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Collider>
extern MethodInfo ICollection_1_get_Count_m42731_MethodInfo;
static PropertyInfo ICollection_1_t7599____Count_PropertyInfo = 
{
	&ICollection_1_t7599_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42731_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42732_MethodInfo;
static PropertyInfo ICollection_1_t7599____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7599_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42732_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7599_PropertyInfos[] =
{
	&ICollection_1_t7599____Count_PropertyInfo,
	&ICollection_1_t7599____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42731_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Collider>::get_Count()
MethodInfo ICollection_1_get_Count_m42731_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7599_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42731_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42732_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42732_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7599_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42732_GenericMethod/* genericMethod */

};
extern Il2CppType Collider_t70_0_0_0;
extern Il2CppType Collider_t70_0_0_0;
static ParameterInfo ICollection_1_t7599_ICollection_1_Add_m42733_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Collider_t70_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42733_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Add(T)
MethodInfo ICollection_1_Add_m42733_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7599_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7599_ICollection_1_Add_m42733_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42733_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42734_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Clear()
MethodInfo ICollection_1_Clear_m42734_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7599_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42734_GenericMethod/* genericMethod */

};
extern Il2CppType Collider_t70_0_0_0;
static ParameterInfo ICollection_1_t7599_ICollection_1_Contains_m42735_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Collider_t70_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42735_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Contains(T)
MethodInfo ICollection_1_Contains_m42735_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7599_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7599_ICollection_1_Contains_m42735_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42735_GenericMethod/* genericMethod */

};
extern Il2CppType ColliderU5BU5D_t132_0_0_0;
extern Il2CppType ColliderU5BU5D_t132_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7599_ICollection_1_CopyTo_m42736_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ColliderU5BU5D_t132_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42736_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42736_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7599_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7599_ICollection_1_CopyTo_m42736_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42736_GenericMethod/* genericMethod */

};
extern Il2CppType Collider_t70_0_0_0;
static ParameterInfo ICollection_1_t7599_ICollection_1_Remove_m42737_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Collider_t70_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42737_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Remove(T)
MethodInfo ICollection_1_Remove_m42737_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7599_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7599_ICollection_1_Remove_m42737_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42737_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7599_MethodInfos[] =
{
	&ICollection_1_get_Count_m42731_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42732_MethodInfo,
	&ICollection_1_Add_m42733_MethodInfo,
	&ICollection_1_Clear_m42734_MethodInfo,
	&ICollection_1_Contains_m42735_MethodInfo,
	&ICollection_1_CopyTo_m42736_MethodInfo,
	&ICollection_1_Remove_m42737_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7601_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7599_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7601_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7599_0_0_0;
extern Il2CppType ICollection_1_t7599_1_0_0;
struct ICollection_1_t7599;
extern Il2CppGenericClass ICollection_1_t7599_GenericClass;
TypeInfo ICollection_1_t7599_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7599_MethodInfos/* methods */
	, ICollection_1_t7599_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7599_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7599_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7599_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7599_0_0_0/* byval_arg */
	, &ICollection_1_t7599_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7599_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Collider>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Collider>
extern Il2CppType IEnumerator_1_t5978_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42738_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Collider>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42738_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7601_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5978_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42738_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7601_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42738_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7601_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7601_0_0_0;
extern Il2CppType IEnumerable_1_t7601_1_0_0;
struct IEnumerable_1_t7601;
extern Il2CppGenericClass IEnumerable_1_t7601_GenericClass;
TypeInfo IEnumerable_1_t7601_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7601_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7601_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7601_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7601_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7601_0_0_0/* byval_arg */
	, &IEnumerable_1_t7601_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7601_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7600_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Collider>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Collider>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Collider>
extern MethodInfo IList_1_get_Item_m42739_MethodInfo;
extern MethodInfo IList_1_set_Item_m42740_MethodInfo;
static PropertyInfo IList_1_t7600____Item_PropertyInfo = 
{
	&IList_1_t7600_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42739_MethodInfo/* get */
	, &IList_1_set_Item_m42740_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7600_PropertyInfos[] =
{
	&IList_1_t7600____Item_PropertyInfo,
	NULL
};
extern Il2CppType Collider_t70_0_0_0;
static ParameterInfo IList_1_t7600_IList_1_IndexOf_m42741_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Collider_t70_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42741_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Collider>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42741_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7600_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7600_IList_1_IndexOf_m42741_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42741_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Collider_t70_0_0_0;
static ParameterInfo IList_1_t7600_IList_1_Insert_m42742_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Collider_t70_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42742_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42742_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7600_IList_1_Insert_m42742_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42742_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7600_IList_1_RemoveAt_m42743_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42743_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42743_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7600_IList_1_RemoveAt_m42743_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42743_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7600_IList_1_get_Item_m42739_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Collider_t70_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42739_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Collider>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42739_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7600_il2cpp_TypeInfo/* declaring_type */
	, &Collider_t70_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7600_IList_1_get_Item_m42739_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42739_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Collider_t70_0_0_0;
static ParameterInfo IList_1_t7600_IList_1_set_Item_m42740_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Collider_t70_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42740_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42740_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7600_IList_1_set_Item_m42740_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42740_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7600_MethodInfos[] =
{
	&IList_1_IndexOf_m42741_MethodInfo,
	&IList_1_Insert_m42742_MethodInfo,
	&IList_1_RemoveAt_m42743_MethodInfo,
	&IList_1_get_Item_m42739_MethodInfo,
	&IList_1_set_Item_m42740_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7600_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7599_il2cpp_TypeInfo,
	&IEnumerable_1_t7601_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7600_0_0_0;
extern Il2CppType IList_1_t7600_1_0_0;
struct IList_1_t7600;
extern Il2CppGenericClass IList_1_t7600_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7600_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7600_MethodInfos/* methods */
	, IList_1_t7600_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7600_il2cpp_TypeInfo/* element_class */
	, IList_1_t7600_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7600_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7600_0_0_0/* byval_arg */
	, &IList_1_t7600_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7600_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5980_il2cpp_TypeInfo;

// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.GLErrorHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.GLErrorHandler>
extern MethodInfo IEnumerator_1_get_Current_m42744_MethodInfo;
static PropertyInfo IEnumerator_1_t5980____Current_PropertyInfo = 
{
	&IEnumerator_1_t5980_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42744_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5980_PropertyInfos[] =
{
	&IEnumerator_1_t5980____Current_PropertyInfo,
	NULL
};
extern Il2CppType GLErrorHandler_t19_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42744_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.GLErrorHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42744_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5980_il2cpp_TypeInfo/* declaring_type */
	, &GLErrorHandler_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42744_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5980_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42744_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5980_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5980_0_0_0;
extern Il2CppType IEnumerator_1_t5980_1_0_0;
struct IEnumerator_1_t5980;
extern Il2CppGenericClass IEnumerator_1_t5980_GenericClass;
TypeInfo IEnumerator_1_t5980_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5980_MethodInfos/* methods */
	, IEnumerator_1_t5980_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5980_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5980_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5980_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5980_0_0_0/* byval_arg */
	, &IEnumerator_1_t5980_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5980_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_27.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2807_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_27MethodDeclarations.h"

extern TypeInfo GLErrorHandler_t19_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14389_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGLErrorHandler_t19_m32552_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.GLErrorHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.GLErrorHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisGLErrorHandler_t19_m32552(__this, p0, method) (GLErrorHandler_t19 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2807____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2807, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2807____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2807, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2807_FieldInfos[] =
{
	&InternalEnumerator_1_t2807____array_0_FieldInfo,
	&InternalEnumerator_1_t2807____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14386_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2807____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2807_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14386_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2807____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2807_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14389_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2807_PropertyInfos[] =
{
	&InternalEnumerator_1_t2807____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2807____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2807_InternalEnumerator_1__ctor_m14385_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14385_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14385_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2807_InternalEnumerator_1__ctor_m14385_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14385_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14386_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14386_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14386_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14387_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14387_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14387_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14388_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14388_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14388_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t19_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14389_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14389_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* declaring_type */
	, &GLErrorHandler_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14389_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2807_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14385_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14386_MethodInfo,
	&InternalEnumerator_1_Dispose_m14387_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14388_MethodInfo,
	&InternalEnumerator_1_get_Current_m14389_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14388_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14387_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2807_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14386_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14388_MethodInfo,
	&InternalEnumerator_1_Dispose_m14387_MethodInfo,
	&InternalEnumerator_1_get_Current_m14389_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2807_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5980_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2807_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5980_il2cpp_TypeInfo, 7},
};
extern TypeInfo GLErrorHandler_t19_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2807_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14389_MethodInfo/* Method Usage */,
	&GLErrorHandler_t19_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGLErrorHandler_t19_m32552_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2807_0_0_0;
extern Il2CppType InternalEnumerator_1_t2807_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2807_GenericClass;
TypeInfo InternalEnumerator_1_t2807_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2807_MethodInfos/* methods */
	, InternalEnumerator_1_t2807_PropertyInfos/* properties */
	, InternalEnumerator_1_t2807_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2807_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2807_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2807_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2807_1_0_0/* this_arg */
	, InternalEnumerator_1_t2807_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2807_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2807_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2807)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7602_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>
extern MethodInfo ICollection_1_get_Count_m42745_MethodInfo;
static PropertyInfo ICollection_1_t7602____Count_PropertyInfo = 
{
	&ICollection_1_t7602_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42745_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42746_MethodInfo;
static PropertyInfo ICollection_1_t7602____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7602_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42746_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7602_PropertyInfos[] =
{
	&ICollection_1_t7602____Count_PropertyInfo,
	&ICollection_1_t7602____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42745_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m42745_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7602_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42745_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42746_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42746_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7602_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42746_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t19_0_0_0;
extern Il2CppType GLErrorHandler_t19_0_0_0;
static ParameterInfo ICollection_1_t7602_ICollection_1_Add_m42747_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t19_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42747_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Add(T)
MethodInfo ICollection_1_Add_m42747_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7602_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7602_ICollection_1_Add_m42747_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42747_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42748_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Clear()
MethodInfo ICollection_1_Clear_m42748_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7602_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42748_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t19_0_0_0;
static ParameterInfo ICollection_1_t7602_ICollection_1_Contains_m42749_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t19_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42749_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m42749_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7602_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7602_ICollection_1_Contains_m42749_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42749_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandlerU5BU5D_t5365_0_0_0;
extern Il2CppType GLErrorHandlerU5BU5D_t5365_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7602_ICollection_1_CopyTo_m42750_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandlerU5BU5D_t5365_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42750_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42750_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7602_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7602_ICollection_1_CopyTo_m42750_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42750_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t19_0_0_0;
static ParameterInfo ICollection_1_t7602_ICollection_1_Remove_m42751_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t19_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42751_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m42751_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7602_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7602_ICollection_1_Remove_m42751_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42751_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7602_MethodInfos[] =
{
	&ICollection_1_get_Count_m42745_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42746_MethodInfo,
	&ICollection_1_Add_m42747_MethodInfo,
	&ICollection_1_Clear_m42748_MethodInfo,
	&ICollection_1_Contains_m42749_MethodInfo,
	&ICollection_1_CopyTo_m42750_MethodInfo,
	&ICollection_1_Remove_m42751_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7604_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7602_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7604_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7602_0_0_0;
extern Il2CppType ICollection_1_t7602_1_0_0;
struct ICollection_1_t7602;
extern Il2CppGenericClass ICollection_1_t7602_GenericClass;
TypeInfo ICollection_1_t7602_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7602_MethodInfos/* methods */
	, ICollection_1_t7602_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7602_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7602_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7602_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7602_0_0_0/* byval_arg */
	, &ICollection_1_t7602_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7602_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.GLErrorHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.GLErrorHandler>
extern Il2CppType IEnumerator_1_t5980_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42752_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.GLErrorHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42752_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7604_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5980_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42752_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7604_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42752_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7604_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7604_0_0_0;
extern Il2CppType IEnumerable_1_t7604_1_0_0;
struct IEnumerable_1_t7604;
extern Il2CppGenericClass IEnumerable_1_t7604_GenericClass;
TypeInfo IEnumerable_1_t7604_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7604_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7604_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7604_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7604_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7604_0_0_0/* byval_arg */
	, &IEnumerable_1_t7604_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7604_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7603_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>
extern MethodInfo IList_1_get_Item_m42753_MethodInfo;
extern MethodInfo IList_1_set_Item_m42754_MethodInfo;
static PropertyInfo IList_1_t7603____Item_PropertyInfo = 
{
	&IList_1_t7603_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42753_MethodInfo/* get */
	, &IList_1_set_Item_m42754_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7603_PropertyInfos[] =
{
	&IList_1_t7603____Item_PropertyInfo,
	NULL
};
extern Il2CppType GLErrorHandler_t19_0_0_0;
static ParameterInfo IList_1_t7603_IList_1_IndexOf_m42755_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t19_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42755_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42755_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7603_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7603_IList_1_IndexOf_m42755_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42755_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GLErrorHandler_t19_0_0_0;
static ParameterInfo IList_1_t7603_IList_1_Insert_m42756_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t19_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42756_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42756_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7603_IList_1_Insert_m42756_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42756_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7603_IList_1_RemoveAt_m42757_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42757_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42757_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7603_IList_1_RemoveAt_m42757_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42757_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7603_IList_1_get_Item_m42753_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType GLErrorHandler_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42753_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42753_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7603_il2cpp_TypeInfo/* declaring_type */
	, &GLErrorHandler_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7603_IList_1_get_Item_m42753_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42753_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GLErrorHandler_t19_0_0_0;
static ParameterInfo IList_1_t7603_IList_1_set_Item_m42754_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t19_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42754_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42754_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7603_IList_1_set_Item_m42754_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42754_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7603_MethodInfos[] =
{
	&IList_1_IndexOf_m42755_MethodInfo,
	&IList_1_Insert_m42756_MethodInfo,
	&IList_1_RemoveAt_m42757_MethodInfo,
	&IList_1_get_Item_m42753_MethodInfo,
	&IList_1_set_Item_m42754_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7603_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7602_il2cpp_TypeInfo,
	&IEnumerable_1_t7604_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7603_0_0_0;
extern Il2CppType IList_1_t7603_1_0_0;
struct IList_1_t7603;
extern Il2CppGenericClass IList_1_t7603_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7603_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7603_MethodInfos/* methods */
	, IList_1_t7603_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7603_il2cpp_TypeInfo/* element_class */
	, IList_1_t7603_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7603_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7603_0_0_0/* byval_arg */
	, &IList_1_t7603_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7603_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_11.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2808_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_11MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_7.h"
extern TypeInfo InvokableCall_1_t2809_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_7MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14392_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14394_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2808____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2808_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2808, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2808_FieldInfos[] =
{
	&CachedInvokableCall_1_t2808____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType GLErrorHandler_t19_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2808_CachedInvokableCall_1__ctor_m14390_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t19_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14390_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14390_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2808_CachedInvokableCall_1__ctor_m14390_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14390_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2808_CachedInvokableCall_1_Invoke_m14391_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14391_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14391_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2808_CachedInvokableCall_1_Invoke_m14391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14391_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2808_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14390_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14391_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14391_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14395_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2808_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14391_MethodInfo,
	&InvokableCall_1_Find_m14395_MethodInfo,
};
extern Il2CppType UnityAction_1_t2810_0_0_0;
extern TypeInfo UnityAction_1_t2810_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisGLErrorHandler_t19_m32562_MethodInfo;
extern TypeInfo GLErrorHandler_t19_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14397_MethodInfo;
extern TypeInfo GLErrorHandler_t19_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2808_RGCTXData[8] = 
{
	&UnityAction_1_t2810_0_0_0/* Type Usage */,
	&UnityAction_1_t2810_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGLErrorHandler_t19_m32562_MethodInfo/* Method Usage */,
	&GLErrorHandler_t19_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14397_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14392_MethodInfo/* Method Usage */,
	&GLErrorHandler_t19_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14394_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2808_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2808_1_0_0;
struct CachedInvokableCall_1_t2808;
extern Il2CppGenericClass CachedInvokableCall_1_t2808_GenericClass;
TypeInfo CachedInvokableCall_1_t2808_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2808_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2808_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2808_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2808_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2808_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2808_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2808_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2808_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2808_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2808)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_14.h"
extern TypeInfo UnityAction_1_t2810_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_14MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.GLErrorHandler>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.GLErrorHandler>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisGLErrorHandler_t19_m32562(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>
extern Il2CppType UnityAction_1_t2810_0_0_1;
FieldInfo InvokableCall_1_t2809____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2810_0_0_1/* type */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2809, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2809_FieldInfos[] =
{
	&InvokableCall_1_t2809____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2809_InvokableCall_1__ctor_m14392_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14392_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14392_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2809_InvokableCall_1__ctor_m14392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14392_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2810_0_0_0;
static ParameterInfo InvokableCall_1_t2809_InvokableCall_1__ctor_m14393_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2810_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14393_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14393_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2809_InvokableCall_1__ctor_m14393_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14393_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2809_InvokableCall_1_Invoke_m14394_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14394_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14394_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2809_InvokableCall_1_Invoke_m14394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14394_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2809_InvokableCall_1_Find_m14395_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14395_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14395_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2809_InvokableCall_1_Find_m14395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14395_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2809_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14392_MethodInfo,
	&InvokableCall_1__ctor_m14393_MethodInfo,
	&InvokableCall_1_Invoke_m14394_MethodInfo,
	&InvokableCall_1_Find_m14395_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2809_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14394_MethodInfo,
	&InvokableCall_1_Find_m14395_MethodInfo,
};
extern TypeInfo UnityAction_1_t2810_il2cpp_TypeInfo;
extern TypeInfo GLErrorHandler_t19_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2809_RGCTXData[5] = 
{
	&UnityAction_1_t2810_0_0_0/* Type Usage */,
	&UnityAction_1_t2810_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGLErrorHandler_t19_m32562_MethodInfo/* Method Usage */,
	&GLErrorHandler_t19_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14397_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2809_0_0_0;
extern Il2CppType InvokableCall_1_t2809_1_0_0;
struct InvokableCall_1_t2809;
extern Il2CppGenericClass InvokableCall_1_t2809_GenericClass;
TypeInfo InvokableCall_1_t2809_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2809_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2809_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2809_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2809_0_0_0/* byval_arg */
	, &InvokableCall_1_t2809_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2809_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2809_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2809)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2810_UnityAction_1__ctor_m14396_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14396_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14396_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2810_UnityAction_1__ctor_m14396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14396_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t19_0_0_0;
static ParameterInfo UnityAction_1_t2810_UnityAction_1_Invoke_m14397_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t19_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14397_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14397_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2810_UnityAction_1_Invoke_m14397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14397_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t19_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2810_UnityAction_1_BeginInvoke_m14398_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t19_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14398_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14398_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2810_UnityAction_1_BeginInvoke_m14398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14398_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2810_UnityAction_1_EndInvoke_m14399_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14399_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14399_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2810_UnityAction_1_EndInvoke_m14399_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14399_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2810_MethodInfos[] =
{
	&UnityAction_1__ctor_m14396_MethodInfo,
	&UnityAction_1_Invoke_m14397_MethodInfo,
	&UnityAction_1_BeginInvoke_m14398_MethodInfo,
	&UnityAction_1_EndInvoke_m14399_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14398_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14399_MethodInfo;
static MethodInfo* UnityAction_1_t2810_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14397_MethodInfo,
	&UnityAction_1_BeginInvoke_m14398_MethodInfo,
	&UnityAction_1_EndInvoke_m14399_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2810_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2810_1_0_0;
struct UnityAction_1_t2810;
extern Il2CppGenericClass UnityAction_1_t2810_GenericClass;
TypeInfo UnityAction_1_t2810_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2810_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2810_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2810_0_0_0/* byval_arg */
	, &UnityAction_1_t2810_1_0_0/* this_arg */
	, UnityAction_1_t2810_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2810_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2810)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5982_il2cpp_TypeInfo;

// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42758_MethodInfo;
static PropertyInfo IEnumerator_1_t5982____Current_PropertyInfo = 
{
	&IEnumerator_1_t5982_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42758_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5982_PropertyInfos[] =
{
	&IEnumerator_1_t5982____Current_PropertyInfo,
	NULL
};
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42758_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42758_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5982_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaBehaviour_t20_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42758_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5982_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42758_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5982_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5982_0_0_0;
extern Il2CppType IEnumerator_1_t5982_1_0_0;
struct IEnumerator_1_t5982;
extern Il2CppGenericClass IEnumerator_1_t5982_GenericClass;
TypeInfo IEnumerator_1_t5982_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5982_MethodInfos/* methods */
	, IEnumerator_1_t5982_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5982_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5982_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5982_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5982_0_0_0/* byval_arg */
	, &IEnumerator_1_t5982_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5982_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_28.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2811_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_28MethodDeclarations.h"

extern TypeInfo HideExcessAreaBehaviour_t20_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14404_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHideExcessAreaBehaviour_t20_m32564_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.HideExcessAreaBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.HideExcessAreaBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisHideExcessAreaBehaviour_t20_m32564(__this, p0, method) (HideExcessAreaBehaviour_t20 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2811____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2811_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2811, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2811____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2811_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2811, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2811_FieldInfos[] =
{
	&InternalEnumerator_1_t2811____array_0_FieldInfo,
	&InternalEnumerator_1_t2811____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14401_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2811____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2811_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14401_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2811____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2811_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14404_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2811_PropertyInfos[] =
{
	&InternalEnumerator_1_t2811____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2811____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2811_InternalEnumerator_1__ctor_m14400_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14400_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14400_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2811_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2811_InternalEnumerator_1__ctor_m14400_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14400_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14401_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14401_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2811_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14401_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14402_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14402_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2811_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14402_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14403_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14403_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2811_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14403_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14404_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14404_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2811_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaBehaviour_t20_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14404_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2811_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14400_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14401_MethodInfo,
	&InternalEnumerator_1_Dispose_m14402_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14403_MethodInfo,
	&InternalEnumerator_1_get_Current_m14404_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14403_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14402_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2811_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14401_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14403_MethodInfo,
	&InternalEnumerator_1_Dispose_m14402_MethodInfo,
	&InternalEnumerator_1_get_Current_m14404_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2811_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5982_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2811_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5982_il2cpp_TypeInfo, 7},
};
extern TypeInfo HideExcessAreaBehaviour_t20_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2811_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14404_MethodInfo/* Method Usage */,
	&HideExcessAreaBehaviour_t20_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisHideExcessAreaBehaviour_t20_m32564_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2811_0_0_0;
extern Il2CppType InternalEnumerator_1_t2811_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2811_GenericClass;
TypeInfo InternalEnumerator_1_t2811_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2811_MethodInfos/* methods */
	, InternalEnumerator_1_t2811_PropertyInfos/* properties */
	, InternalEnumerator_1_t2811_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2811_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2811_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2811_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2811_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2811_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2811_1_0_0/* this_arg */
	, InternalEnumerator_1_t2811_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2811_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2811_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2811)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7605_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>
extern MethodInfo ICollection_1_get_Count_m42759_MethodInfo;
static PropertyInfo ICollection_1_t7605____Count_PropertyInfo = 
{
	&ICollection_1_t7605_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42759_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42760_MethodInfo;
static PropertyInfo ICollection_1_t7605____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7605_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42760_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7605_PropertyInfos[] =
{
	&ICollection_1_t7605____Count_PropertyInfo,
	&ICollection_1_t7605____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42759_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42759_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7605_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42759_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42760_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42760_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7605_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42760_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
static ParameterInfo ICollection_1_t7605_ICollection_1_Add_m42761_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t20_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42761_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42761_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7605_ICollection_1_Add_m42761_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42761_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42762_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42762_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42762_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
static ParameterInfo ICollection_1_t7605_ICollection_1_Contains_m42763_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t20_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42763_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42763_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7605_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7605_ICollection_1_Contains_m42763_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42763_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviourU5BU5D_t5366_0_0_0;
extern Il2CppType HideExcessAreaBehaviourU5BU5D_t5366_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7605_ICollection_1_CopyTo_m42764_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviourU5BU5D_t5366_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42764_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42764_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7605_ICollection_1_CopyTo_m42764_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42764_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
static ParameterInfo ICollection_1_t7605_ICollection_1_Remove_m42765_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t20_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42765_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42765_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7605_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7605_ICollection_1_Remove_m42765_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42765_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7605_MethodInfos[] =
{
	&ICollection_1_get_Count_m42759_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42760_MethodInfo,
	&ICollection_1_Add_m42761_MethodInfo,
	&ICollection_1_Clear_m42762_MethodInfo,
	&ICollection_1_Contains_m42763_MethodInfo,
	&ICollection_1_CopyTo_m42764_MethodInfo,
	&ICollection_1_Remove_m42765_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7607_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7605_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7607_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7605_0_0_0;
extern Il2CppType ICollection_1_t7605_1_0_0;
struct ICollection_1_t7605;
extern Il2CppGenericClass ICollection_1_t7605_GenericClass;
TypeInfo ICollection_1_t7605_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7605_MethodInfos/* methods */
	, ICollection_1_t7605_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7605_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7605_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7605_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7605_0_0_0/* byval_arg */
	, &ICollection_1_t7605_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7605_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaBehaviour>
extern Il2CppType IEnumerator_1_t5982_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42766_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42766_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7607_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5982_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42766_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7607_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42766_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7607_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7607_0_0_0;
extern Il2CppType IEnumerable_1_t7607_1_0_0;
struct IEnumerable_1_t7607;
extern Il2CppGenericClass IEnumerable_1_t7607_GenericClass;
TypeInfo IEnumerable_1_t7607_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7607_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7607_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7607_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7607_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7607_0_0_0/* byval_arg */
	, &IEnumerable_1_t7607_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7607_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7606_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>
extern MethodInfo IList_1_get_Item_m42767_MethodInfo;
extern MethodInfo IList_1_set_Item_m42768_MethodInfo;
static PropertyInfo IList_1_t7606____Item_PropertyInfo = 
{
	&IList_1_t7606_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42767_MethodInfo/* get */
	, &IList_1_set_Item_m42768_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7606_PropertyInfos[] =
{
	&IList_1_t7606____Item_PropertyInfo,
	NULL
};
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
static ParameterInfo IList_1_t7606_IList_1_IndexOf_m42769_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t20_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42769_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42769_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7606_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7606_IList_1_IndexOf_m42769_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42769_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
static ParameterInfo IList_1_t7606_IList_1_Insert_m42770_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t20_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42770_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42770_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7606_IList_1_Insert_m42770_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42770_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7606_IList_1_RemoveAt_m42771_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42771_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42771_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7606_IList_1_RemoveAt_m42771_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42771_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7606_IList_1_get_Item_m42767_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42767_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42767_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7606_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaBehaviour_t20_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7606_IList_1_get_Item_m42767_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42767_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
static ParameterInfo IList_1_t7606_IList_1_set_Item_m42768_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t20_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42768_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42768_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7606_IList_1_set_Item_m42768_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42768_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7606_MethodInfos[] =
{
	&IList_1_IndexOf_m42769_MethodInfo,
	&IList_1_Insert_m42770_MethodInfo,
	&IList_1_RemoveAt_m42771_MethodInfo,
	&IList_1_get_Item_m42767_MethodInfo,
	&IList_1_set_Item_m42768_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7606_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7605_il2cpp_TypeInfo,
	&IEnumerable_1_t7607_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7606_0_0_0;
extern Il2CppType IList_1_t7606_1_0_0;
struct IList_1_t7606;
extern Il2CppGenericClass IList_1_t7606_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7606_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7606_MethodInfos/* methods */
	, IList_1_t7606_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7606_il2cpp_TypeInfo/* element_class */
	, IList_1_t7606_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7606_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7606_0_0_0/* byval_arg */
	, &IList_1_t7606_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7606_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7608_il2cpp_TypeInfo;

// Vuforia.HideExcessAreaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstr.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42772_MethodInfo;
static PropertyInfo ICollection_1_t7608____Count_PropertyInfo = 
{
	&ICollection_1_t7608_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42772_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42773_MethodInfo;
static PropertyInfo ICollection_1_t7608____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7608_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42773_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7608_PropertyInfos[] =
{
	&ICollection_1_t7608____Count_PropertyInfo,
	&ICollection_1_t7608____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42772_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42772_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7608_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42772_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42773_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42773_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7608_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42773_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaAbstractBehaviour_t21_0_0_0;
extern Il2CppType HideExcessAreaAbstractBehaviour_t21_0_0_0;
static ParameterInfo ICollection_1_t7608_ICollection_1_Add_m42774_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t21_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42774_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42774_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7608_ICollection_1_Add_m42774_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42774_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42775_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42775_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42775_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaAbstractBehaviour_t21_0_0_0;
static ParameterInfo ICollection_1_t7608_ICollection_1_Contains_m42776_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t21_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42776_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42776_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7608_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7608_ICollection_1_Contains_m42776_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42776_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaAbstractBehaviourU5BU5D_t5629_0_0_0;
extern Il2CppType HideExcessAreaAbstractBehaviourU5BU5D_t5629_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7608_ICollection_1_CopyTo_m42777_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviourU5BU5D_t5629_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42777_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42777_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7608_ICollection_1_CopyTo_m42777_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42777_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaAbstractBehaviour_t21_0_0_0;
static ParameterInfo ICollection_1_t7608_ICollection_1_Remove_m42778_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t21_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42778_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42778_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7608_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7608_ICollection_1_Remove_m42778_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42778_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7608_MethodInfos[] =
{
	&ICollection_1_get_Count_m42772_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42773_MethodInfo,
	&ICollection_1_Add_m42774_MethodInfo,
	&ICollection_1_Clear_m42775_MethodInfo,
	&ICollection_1_Contains_m42776_MethodInfo,
	&ICollection_1_CopyTo_m42777_MethodInfo,
	&ICollection_1_Remove_m42778_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7610_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7608_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7610_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7608_0_0_0;
extern Il2CppType ICollection_1_t7608_1_0_0;
struct ICollection_1_t7608;
extern Il2CppGenericClass ICollection_1_t7608_GenericClass;
TypeInfo ICollection_1_t7608_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7608_MethodInfos/* methods */
	, ICollection_1_t7608_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7608_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7608_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7608_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7608_0_0_0/* byval_arg */
	, &ICollection_1_t7608_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7608_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5984_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42779_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42779_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7610_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5984_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42779_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7610_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42779_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7610_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7610_0_0_0;
extern Il2CppType IEnumerable_1_t7610_1_0_0;
struct IEnumerable_1_t7610;
extern Il2CppGenericClass IEnumerable_1_t7610_GenericClass;
TypeInfo IEnumerable_1_t7610_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7610_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7610_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7610_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7610_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7610_0_0_0/* byval_arg */
	, &IEnumerable_1_t7610_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7610_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5984_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42780_MethodInfo;
static PropertyInfo IEnumerator_1_t5984____Current_PropertyInfo = 
{
	&IEnumerator_1_t5984_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42780_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5984_PropertyInfos[] =
{
	&IEnumerator_1_t5984____Current_PropertyInfo,
	NULL
};
extern Il2CppType HideExcessAreaAbstractBehaviour_t21_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42780_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42780_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5984_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaAbstractBehaviour_t21_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42780_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5984_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42780_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5984_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5984_0_0_0;
extern Il2CppType IEnumerator_1_t5984_1_0_0;
struct IEnumerator_1_t5984;
extern Il2CppGenericClass IEnumerator_1_t5984_GenericClass;
TypeInfo IEnumerator_1_t5984_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5984_MethodInfos/* methods */
	, IEnumerator_1_t5984_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5984_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5984_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5984_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5984_0_0_0/* byval_arg */
	, &IEnumerator_1_t5984_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5984_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_29.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2812_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_29MethodDeclarations.h"

extern TypeInfo HideExcessAreaAbstractBehaviour_t21_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14409_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHideExcessAreaAbstractBehaviour_t21_m32575_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.HideExcessAreaAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.HideExcessAreaAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisHideExcessAreaAbstractBehaviour_t21_m32575(__this, p0, method) (HideExcessAreaAbstractBehaviour_t21 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2812____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2812_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2812, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2812____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2812_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2812, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2812_FieldInfos[] =
{
	&InternalEnumerator_1_t2812____array_0_FieldInfo,
	&InternalEnumerator_1_t2812____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14406_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2812____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2812_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14406_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2812____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2812_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14409_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2812_PropertyInfos[] =
{
	&InternalEnumerator_1_t2812____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2812____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2812_InternalEnumerator_1__ctor_m14405_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14405_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14405_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2812_InternalEnumerator_1__ctor_m14405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14405_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14406_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14406_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2812_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14406_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14407_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14407_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14407_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14408_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14408_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2812_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14408_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaAbstractBehaviour_t21_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14409_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14409_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2812_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaAbstractBehaviour_t21_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14409_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2812_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14405_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14406_MethodInfo,
	&InternalEnumerator_1_Dispose_m14407_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14408_MethodInfo,
	&InternalEnumerator_1_get_Current_m14409_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14408_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14407_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2812_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14406_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14408_MethodInfo,
	&InternalEnumerator_1_Dispose_m14407_MethodInfo,
	&InternalEnumerator_1_get_Current_m14409_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2812_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5984_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2812_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5984_il2cpp_TypeInfo, 7},
};
extern TypeInfo HideExcessAreaAbstractBehaviour_t21_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2812_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14409_MethodInfo/* Method Usage */,
	&HideExcessAreaAbstractBehaviour_t21_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisHideExcessAreaAbstractBehaviour_t21_m32575_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2812_0_0_0;
extern Il2CppType InternalEnumerator_1_t2812_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2812_GenericClass;
TypeInfo InternalEnumerator_1_t2812_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2812_MethodInfos/* methods */
	, InternalEnumerator_1_t2812_PropertyInfos/* properties */
	, InternalEnumerator_1_t2812_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2812_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2812_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2812_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2812_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2812_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2812_1_0_0/* this_arg */
	, InternalEnumerator_1_t2812_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2812_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2812_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2812)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7609_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42781_MethodInfo;
extern MethodInfo IList_1_set_Item_m42782_MethodInfo;
static PropertyInfo IList_1_t7609____Item_PropertyInfo = 
{
	&IList_1_t7609_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42781_MethodInfo/* get */
	, &IList_1_set_Item_m42782_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7609_PropertyInfos[] =
{
	&IList_1_t7609____Item_PropertyInfo,
	NULL
};
extern Il2CppType HideExcessAreaAbstractBehaviour_t21_0_0_0;
static ParameterInfo IList_1_t7609_IList_1_IndexOf_m42783_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t21_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42783_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42783_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7609_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7609_IList_1_IndexOf_m42783_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42783_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HideExcessAreaAbstractBehaviour_t21_0_0_0;
static ParameterInfo IList_1_t7609_IList_1_Insert_m42784_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t21_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42784_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42784_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7609_IList_1_Insert_m42784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42784_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7609_IList_1_RemoveAt_m42785_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42785_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42785_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7609_IList_1_RemoveAt_m42785_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42785_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7609_IList_1_get_Item_m42781_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType HideExcessAreaAbstractBehaviour_t21_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42781_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42781_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7609_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaAbstractBehaviour_t21_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7609_IList_1_get_Item_m42781_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42781_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HideExcessAreaAbstractBehaviour_t21_0_0_0;
static ParameterInfo IList_1_t7609_IList_1_set_Item_m42782_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t21_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42782_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42782_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7609_IList_1_set_Item_m42782_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42782_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7609_MethodInfos[] =
{
	&IList_1_IndexOf_m42783_MethodInfo,
	&IList_1_Insert_m42784_MethodInfo,
	&IList_1_RemoveAt_m42785_MethodInfo,
	&IList_1_get_Item_m42781_MethodInfo,
	&IList_1_set_Item_m42782_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7609_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7608_il2cpp_TypeInfo,
	&IEnumerable_1_t7610_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7609_0_0_0;
extern Il2CppType IList_1_t7609_1_0_0;
struct IList_1_t7609;
extern Il2CppGenericClass IList_1_t7609_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7609_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7609_MethodInfos/* methods */
	, IList_1_t7609_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7609_il2cpp_TypeInfo/* element_class */
	, IList_1_t7609_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7609_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7609_0_0_0/* byval_arg */
	, &IList_1_t7609_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7609_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_12.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2813_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_12MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_8.h"
extern TypeInfo InvokableCall_1_t2814_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_8MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14412_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14414_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2813____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2813_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2813, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2813_FieldInfos[] =
{
	&CachedInvokableCall_1_t2813____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2813_CachedInvokableCall_1__ctor_m14410_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t20_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14410_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14410_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2813_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2813_CachedInvokableCall_1__ctor_m14410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14410_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2813_CachedInvokableCall_1_Invoke_m14411_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14411_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14411_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2813_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2813_CachedInvokableCall_1_Invoke_m14411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14411_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2813_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14410_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14411_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14411_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14415_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2813_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14411_MethodInfo,
	&InvokableCall_1_Find_m14415_MethodInfo,
};
extern Il2CppType UnityAction_1_t2815_0_0_0;
extern TypeInfo UnityAction_1_t2815_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisHideExcessAreaBehaviour_t20_m32585_MethodInfo;
extern TypeInfo HideExcessAreaBehaviour_t20_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14417_MethodInfo;
extern TypeInfo HideExcessAreaBehaviour_t20_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2813_RGCTXData[8] = 
{
	&UnityAction_1_t2815_0_0_0/* Type Usage */,
	&UnityAction_1_t2815_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisHideExcessAreaBehaviour_t20_m32585_MethodInfo/* Method Usage */,
	&HideExcessAreaBehaviour_t20_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14417_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14412_MethodInfo/* Method Usage */,
	&HideExcessAreaBehaviour_t20_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14414_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2813_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2813_1_0_0;
struct CachedInvokableCall_1_t2813;
extern Il2CppGenericClass CachedInvokableCall_1_t2813_GenericClass;
TypeInfo CachedInvokableCall_1_t2813_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2813_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2813_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2814_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2813_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2813_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2813_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2813_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2813_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2813_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2813_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2813)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_15.h"
extern TypeInfo UnityAction_1_t2815_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_15MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.HideExcessAreaBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.HideExcessAreaBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisHideExcessAreaBehaviour_t20_m32585(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>
extern Il2CppType UnityAction_1_t2815_0_0_1;
FieldInfo InvokableCall_1_t2814____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2815_0_0_1/* type */
	, &InvokableCall_1_t2814_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2814, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2814_FieldInfos[] =
{
	&InvokableCall_1_t2814____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2814_InvokableCall_1__ctor_m14412_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14412_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14412_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2814_InvokableCall_1__ctor_m14412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14412_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2815_0_0_0;
static ParameterInfo InvokableCall_1_t2814_InvokableCall_1__ctor_m14413_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2815_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14413_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14413_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2814_InvokableCall_1__ctor_m14413_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14413_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2814_InvokableCall_1_Invoke_m14414_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14414_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14414_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2814_InvokableCall_1_Invoke_m14414_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14414_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2814_InvokableCall_1_Find_m14415_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14415_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14415_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2814_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2814_InvokableCall_1_Find_m14415_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14415_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2814_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14412_MethodInfo,
	&InvokableCall_1__ctor_m14413_MethodInfo,
	&InvokableCall_1_Invoke_m14414_MethodInfo,
	&InvokableCall_1_Find_m14415_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2814_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m14414_MethodInfo,
	&InvokableCall_1_Find_m14415_MethodInfo,
};
extern TypeInfo UnityAction_1_t2815_il2cpp_TypeInfo;
extern TypeInfo HideExcessAreaBehaviour_t20_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2814_RGCTXData[5] = 
{
	&UnityAction_1_t2815_0_0_0/* Type Usage */,
	&UnityAction_1_t2815_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisHideExcessAreaBehaviour_t20_m32585_MethodInfo/* Method Usage */,
	&HideExcessAreaBehaviour_t20_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14417_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2814_0_0_0;
extern Il2CppType InvokableCall_1_t2814_1_0_0;
struct InvokableCall_1_t2814;
extern Il2CppGenericClass InvokableCall_1_t2814_GenericClass;
TypeInfo InvokableCall_1_t2814_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2814_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2814_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2814_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2814_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2814_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2814_0_0_0/* byval_arg */
	, &InvokableCall_1_t2814_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2814_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2814_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2814)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2815_UnityAction_1__ctor_m14416_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14416_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14416_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2815_UnityAction_1__ctor_m14416_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14416_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
static ParameterInfo UnityAction_1_t2815_UnityAction_1_Invoke_m14417_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t20_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14417_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14417_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2815_UnityAction_1_Invoke_m14417_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14417_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t20_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2815_UnityAction_1_BeginInvoke_m14418_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t20_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14418_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14418_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2815_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2815_UnityAction_1_BeginInvoke_m14418_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14418_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2815_UnityAction_1_EndInvoke_m14419_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14419_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14419_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2815_UnityAction_1_EndInvoke_m14419_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14419_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2815_MethodInfos[] =
{
	&UnityAction_1__ctor_m14416_MethodInfo,
	&UnityAction_1_Invoke_m14417_MethodInfo,
	&UnityAction_1_BeginInvoke_m14418_MethodInfo,
	&UnityAction_1_EndInvoke_m14419_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14418_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14419_MethodInfo;
static MethodInfo* UnityAction_1_t2815_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m14417_MethodInfo,
	&UnityAction_1_BeginInvoke_m14418_MethodInfo,
	&UnityAction_1_EndInvoke_m14419_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2815_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2815_1_0_0;
struct UnityAction_1_t2815;
extern Il2CppGenericClass UnityAction_1_t2815_GenericClass;
TypeInfo UnityAction_1_t2815_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2815_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2815_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2815_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2815_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2815_0_0_0/* byval_arg */
	, &UnityAction_1_t2815_1_0_0/* this_arg */
	, UnityAction_1_t2815_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2815_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2815)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5986_il2cpp_TypeInfo;

// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42786_MethodInfo;
static PropertyInfo IEnumerator_1_t5986____Current_PropertyInfo = 
{
	&IEnumerator_1_t5986_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42786_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5986_PropertyInfos[] =
{
	&IEnumerator_1_t5986____Current_PropertyInfo,
	NULL
};
extern Il2CppType ImageTargetBehaviour_t22_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42786_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42786_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5986_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetBehaviour_t22_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42786_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5986_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42786_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5986_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5986_0_0_0;
extern Il2CppType IEnumerator_1_t5986_1_0_0;
struct IEnumerator_1_t5986;
extern Il2CppGenericClass IEnumerator_1_t5986_GenericClass;
TypeInfo IEnumerator_1_t5986_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5986_MethodInfos/* methods */
	, IEnumerator_1_t5986_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5986_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5986_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5986_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5986_0_0_0/* byval_arg */
	, &IEnumerator_1_t5986_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5986_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_30.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2816_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_30MethodDeclarations.h"

extern TypeInfo ImageTargetBehaviour_t22_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14424_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisImageTargetBehaviour_t22_m32587_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ImageTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ImageTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisImageTargetBehaviour_t22_m32587(__this, p0, method) (ImageTargetBehaviour_t22 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2816____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2816_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2816, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2816____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2816_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2816, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2816_FieldInfos[] =
{
	&InternalEnumerator_1_t2816____array_0_FieldInfo,
	&InternalEnumerator_1_t2816____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14421_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2816____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2816_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14421_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2816____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2816_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14424_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2816_PropertyInfos[] =
{
	&InternalEnumerator_1_t2816____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2816____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2816_InternalEnumerator_1__ctor_m14420_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14420_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14420_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2816_InternalEnumerator_1__ctor_m14420_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14420_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14421_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14421_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2816_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14421_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14422_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14422_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14422_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14423_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14423_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2816_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14423_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetBehaviour_t22_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14424_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14424_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2816_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetBehaviour_t22_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14424_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2816_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14420_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14421_MethodInfo,
	&InternalEnumerator_1_Dispose_m14422_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14423_MethodInfo,
	&InternalEnumerator_1_get_Current_m14424_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14423_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14422_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2816_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14421_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14423_MethodInfo,
	&InternalEnumerator_1_Dispose_m14422_MethodInfo,
	&InternalEnumerator_1_get_Current_m14424_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2816_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t5986_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2816_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5986_il2cpp_TypeInfo, 7},
};
extern TypeInfo ImageTargetBehaviour_t22_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2816_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14424_MethodInfo/* Method Usage */,
	&ImageTargetBehaviour_t22_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisImageTargetBehaviour_t22_m32587_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2816_0_0_0;
extern Il2CppType InternalEnumerator_1_t2816_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2816_GenericClass;
TypeInfo InternalEnumerator_1_t2816_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2816_MethodInfos/* methods */
	, InternalEnumerator_1_t2816_PropertyInfos/* properties */
	, InternalEnumerator_1_t2816_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2816_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2816_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2816_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2816_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2816_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2816_1_0_0/* this_arg */
	, InternalEnumerator_1_t2816_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2816_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2816_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2816)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7611_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m42787_MethodInfo;
static PropertyInfo ICollection_1_t7611____Count_PropertyInfo = 
{
	&ICollection_1_t7611_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42787_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42788_MethodInfo;
static PropertyInfo ICollection_1_t7611____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7611_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42788_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7611_PropertyInfos[] =
{
	&ICollection_1_t7611____Count_PropertyInfo,
	&ICollection_1_t7611____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42787_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42787_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7611_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42787_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42788_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42788_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7611_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42788_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetBehaviour_t22_0_0_0;
extern Il2CppType ImageTargetBehaviour_t22_0_0_0;
static ParameterInfo ICollection_1_t7611_ICollection_1_Add_m42789_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t22_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42789_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42789_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7611_ICollection_1_Add_m42789_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42789_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42790_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42790_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42790_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetBehaviour_t22_0_0_0;
static ParameterInfo ICollection_1_t7611_ICollection_1_Contains_m42791_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t22_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42791_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42791_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7611_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7611_ICollection_1_Contains_m42791_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42791_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetBehaviourU5BU5D_t5367_0_0_0;
extern Il2CppType ImageTargetBehaviourU5BU5D_t5367_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7611_ICollection_1_CopyTo_m42792_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviourU5BU5D_t5367_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42792_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42792_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7611_ICollection_1_CopyTo_m42792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42792_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetBehaviour_t22_0_0_0;
static ParameterInfo ICollection_1_t7611_ICollection_1_Remove_m42793_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t22_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42793_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42793_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7611_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7611_ICollection_1_Remove_m42793_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42793_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7611_MethodInfos[] =
{
	&ICollection_1_get_Count_m42787_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42788_MethodInfo,
	&ICollection_1_Add_m42789_MethodInfo,
	&ICollection_1_Clear_m42790_MethodInfo,
	&ICollection_1_Contains_m42791_MethodInfo,
	&ICollection_1_CopyTo_m42792_MethodInfo,
	&ICollection_1_Remove_m42793_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7613_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7611_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7613_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7611_0_0_0;
extern Il2CppType ICollection_1_t7611_1_0_0;
struct ICollection_1_t7611;
extern Il2CppGenericClass ICollection_1_t7611_GenericClass;
TypeInfo ICollection_1_t7611_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7611_MethodInfos/* methods */
	, ICollection_1_t7611_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7611_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7611_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7611_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7611_0_0_0/* byval_arg */
	, &ICollection_1_t7611_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7611_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetBehaviour>
extern Il2CppType IEnumerator_1_t5986_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42794_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42794_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7613_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5986_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42794_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7613_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42794_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7613_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7613_0_0_0;
extern Il2CppType IEnumerable_1_t7613_1_0_0;
struct IEnumerable_1_t7613;
extern Il2CppGenericClass IEnumerable_1_t7613_GenericClass;
TypeInfo IEnumerable_1_t7613_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7613_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7613_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7613_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7613_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7613_0_0_0/* byval_arg */
	, &IEnumerable_1_t7613_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7613_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7612_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>
extern MethodInfo IList_1_get_Item_m42795_MethodInfo;
extern MethodInfo IList_1_set_Item_m42796_MethodInfo;
static PropertyInfo IList_1_t7612____Item_PropertyInfo = 
{
	&IList_1_t7612_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42795_MethodInfo/* get */
	, &IList_1_set_Item_m42796_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7612_PropertyInfos[] =
{
	&IList_1_t7612____Item_PropertyInfo,
	NULL
};
extern Il2CppType ImageTargetBehaviour_t22_0_0_0;
static ParameterInfo IList_1_t7612_IList_1_IndexOf_m42797_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t22_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42797_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42797_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7612_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7612_IList_1_IndexOf_m42797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42797_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ImageTargetBehaviour_t22_0_0_0;
static ParameterInfo IList_1_t7612_IList_1_Insert_m42798_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t22_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42798_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42798_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7612_IList_1_Insert_m42798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42798_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7612_IList_1_RemoveAt_m42799_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42799_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42799_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7612_IList_1_RemoveAt_m42799_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42799_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7612_IList_1_get_Item_m42795_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ImageTargetBehaviour_t22_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42795_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42795_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7612_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetBehaviour_t22_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7612_IList_1_get_Item_m42795_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42795_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ImageTargetBehaviour_t22_0_0_0;
static ParameterInfo IList_1_t7612_IList_1_set_Item_m42796_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t22_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42796_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42796_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7612_IList_1_set_Item_m42796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42796_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7612_MethodInfos[] =
{
	&IList_1_IndexOf_m42797_MethodInfo,
	&IList_1_Insert_m42798_MethodInfo,
	&IList_1_RemoveAt_m42799_MethodInfo,
	&IList_1_get_Item_m42795_MethodInfo,
	&IList_1_set_Item_m42796_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7612_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7611_il2cpp_TypeInfo,
	&IEnumerable_1_t7613_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7612_0_0_0;
extern Il2CppType IList_1_t7612_1_0_0;
struct IList_1_t7612;
extern Il2CppGenericClass IList_1_t7612_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7612_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7612_MethodInfos/* methods */
	, IList_1_t7612_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7612_il2cpp_TypeInfo/* element_class */
	, IList_1_t7612_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7612_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7612_0_0_0/* byval_arg */
	, &IList_1_t7612_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7612_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7614_il2cpp_TypeInfo;

// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42800_MethodInfo;
static PropertyInfo ICollection_1_t7614____Count_PropertyInfo = 
{
	&ICollection_1_t7614_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42800_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42801_MethodInfo;
static PropertyInfo ICollection_1_t7614____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7614_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42801_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7614_PropertyInfos[] =
{
	&ICollection_1_t7614____Count_PropertyInfo,
	&ICollection_1_t7614____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42800_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42800_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7614_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42800_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42801_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42801_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7614_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42801_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetAbstractBehaviour_t23_0_0_0;
extern Il2CppType ImageTargetAbstractBehaviour_t23_0_0_0;
static ParameterInfo ICollection_1_t7614_ICollection_1_Add_m42802_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviour_t23_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42802_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42802_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7614_ICollection_1_Add_m42802_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42802_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42803_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42803_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42803_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetAbstractBehaviour_t23_0_0_0;
static ParameterInfo ICollection_1_t7614_ICollection_1_Contains_m42804_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviour_t23_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42804_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42804_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7614_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7614_ICollection_1_Contains_m42804_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42804_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetAbstractBehaviourU5BU5D_t5630_0_0_0;
extern Il2CppType ImageTargetAbstractBehaviourU5BU5D_t5630_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7614_ICollection_1_CopyTo_m42805_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviourU5BU5D_t5630_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42805_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42805_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7614_ICollection_1_CopyTo_m42805_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42805_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetAbstractBehaviour_t23_0_0_0;
static ParameterInfo ICollection_1_t7614_ICollection_1_Remove_m42806_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviour_t23_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42806_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42806_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7614_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7614_ICollection_1_Remove_m42806_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42806_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7614_MethodInfos[] =
{
	&ICollection_1_get_Count_m42800_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42801_MethodInfo,
	&ICollection_1_Add_m42802_MethodInfo,
	&ICollection_1_Clear_m42803_MethodInfo,
	&ICollection_1_Contains_m42804_MethodInfo,
	&ICollection_1_CopyTo_m42805_MethodInfo,
	&ICollection_1_Remove_m42806_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7616_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7614_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7616_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7614_0_0_0;
extern Il2CppType ICollection_1_t7614_1_0_0;
struct ICollection_1_t7614;
extern Il2CppGenericClass ICollection_1_t7614_GenericClass;
TypeInfo ICollection_1_t7614_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7614_MethodInfos/* methods */
	, ICollection_1_t7614_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7614_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7614_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7614_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7614_0_0_0/* byval_arg */
	, &ICollection_1_t7614_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7614_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5988_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42807_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42807_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7616_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5988_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42807_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7616_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42807_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7616_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7616_0_0_0;
extern Il2CppType IEnumerable_1_t7616_1_0_0;
struct IEnumerable_1_t7616;
extern Il2CppGenericClass IEnumerable_1_t7616_GenericClass;
TypeInfo IEnumerable_1_t7616_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7616_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7616_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7616_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7616_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7616_0_0_0/* byval_arg */
	, &IEnumerable_1_t7616_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7616_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5988_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42808_MethodInfo;
static PropertyInfo IEnumerator_1_t5988____Current_PropertyInfo = 
{
	&IEnumerator_1_t5988_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42808_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5988_PropertyInfos[] =
{
	&IEnumerator_1_t5988____Current_PropertyInfo,
	NULL
};
extern Il2CppType ImageTargetAbstractBehaviour_t23_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42808_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42808_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5988_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetAbstractBehaviour_t23_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42808_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5988_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42808_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5988_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5988_0_0_0;
extern Il2CppType IEnumerator_1_t5988_1_0_0;
struct IEnumerator_1_t5988;
extern Il2CppGenericClass IEnumerator_1_t5988_GenericClass;
TypeInfo IEnumerator_1_t5988_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5988_MethodInfos/* methods */
	, IEnumerator_1_t5988_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5988_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5988_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5988_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5988_0_0_0/* byval_arg */
	, &IEnumerator_1_t5988_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5988_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
