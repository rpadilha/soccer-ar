﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.IUserDefinedTargetEventHandler>
struct Comparer_1_t4554;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.IUserDefinedTargetEventHandler>
struct Comparer_1_t4554  : public Object_t
{
};
struct Comparer_1_t4554_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.IUserDefinedTargetEventHandler>::_default
	Comparer_1_t4554 * ____default_0;
};
