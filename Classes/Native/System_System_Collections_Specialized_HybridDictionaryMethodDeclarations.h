﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t1350;
// System.Collections.IDictionary
struct IDictionary_t1351;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Array
struct Array_t;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1352;

// System.Void System.Collections.Specialized.HybridDictionary::.ctor()
 void HybridDictionary__ctor_m6899 (HybridDictionary_t1350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.HybridDictionary::.ctor(System.Int32,System.Boolean)
 void HybridDictionary__ctor_m6900 (HybridDictionary_t1350 * __this, int32_t ___initialSize, bool ___caseInsensitive, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.HybridDictionary::System.Collections.IEnumerable.GetEnumerator()
 Object_t * HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m6901 (HybridDictionary_t1350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Collections.Specialized.HybridDictionary::get_inner()
 Object_t * HybridDictionary_get_inner_m6902 (HybridDictionary_t1350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.HybridDictionary::get_Count()
 int32_t HybridDictionary_get_Count_m6903 (HybridDictionary_t1350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.HybridDictionary::get_IsSynchronized()
 bool HybridDictionary_get_IsSynchronized_m6904 (HybridDictionary_t1350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.HybridDictionary::get_Item(System.Object)
 Object_t * HybridDictionary_get_Item_m6905 (HybridDictionary_t1350 * __this, Object_t * ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.HybridDictionary::set_Item(System.Object,System.Object)
 void HybridDictionary_set_Item_m6906 (HybridDictionary_t1350 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.HybridDictionary::get_SyncRoot()
 Object_t * HybridDictionary_get_SyncRoot_m6907 (HybridDictionary_t1350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.HybridDictionary::Add(System.Object,System.Object)
 void HybridDictionary_Add_m6908 (HybridDictionary_t1350 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.HybridDictionary::CopyTo(System.Array,System.Int32)
 void HybridDictionary_CopyTo_m6909 (HybridDictionary_t1350 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator System.Collections.Specialized.HybridDictionary::GetEnumerator()
 Object_t * HybridDictionary_GetEnumerator_m6910 (HybridDictionary_t1350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.HybridDictionary::Remove(System.Object)
 void HybridDictionary_Remove_m6911 (HybridDictionary_t1350 * __this, Object_t * ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.HybridDictionary::Switch()
 void HybridDictionary_Switch_m6912 (HybridDictionary_t1350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
