﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.Oid
struct Oid_t1405;
// System.String
struct String_t;

// System.Void System.Security.Cryptography.Oid::.ctor()
 void Oid__ctor_m7310 (Oid_t1405 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Oid::.ctor(System.String)
 void Oid__ctor_m7311 (Oid_t1405 * __this, String_t* ___oid, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Oid::.ctor(System.String,System.String)
 void Oid__ctor_m7312 (Oid_t1405 * __this, String_t* ___value, String_t* ___friendlyName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Oid::.ctor(System.Security.Cryptography.Oid)
 void Oid__ctor_m7313 (Oid_t1405 * __this, Oid_t1405 * ___oid, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Oid::get_FriendlyName()
 String_t* Oid_get_FriendlyName_m7314 (Oid_t1405 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Oid::get_Value()
 String_t* Oid_get_Value_m7315 (Oid_t1405 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Oid::GetName(System.String)
 String_t* Oid_GetName_m7316 (Oid_t1405 * __this, String_t* ___oid, MethodInfo* method) IL2CPP_METHOD_ATTR;
