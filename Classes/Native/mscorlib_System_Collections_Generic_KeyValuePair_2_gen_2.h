﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t16;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>
struct KeyValuePair_2_t892 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::value
	Object_t * ___value_1;
};
