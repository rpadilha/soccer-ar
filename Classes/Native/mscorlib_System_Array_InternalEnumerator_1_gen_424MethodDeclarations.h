﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption/Type>
struct InternalEnumerator_1_t4713;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption/Type>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m28531 (InternalEnumerator_1_t4713 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption/Type>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28532 (InternalEnumerator_1_t4713 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption/Type>::Dispose()
 void InternalEnumerator_1_Dispose_m28533 (InternalEnumerator_1_t4713 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption/Type>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m28534 (InternalEnumerator_1_t4713 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption/Type>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28535 (InternalEnumerator_1_t4713 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
