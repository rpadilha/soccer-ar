﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$128
struct $ArrayType$128_t2333;
struct $ArrayType$128_t2333_marshaled;

void $ArrayType$128_t2333_marshal(const $ArrayType$128_t2333& unmarshaled, $ArrayType$128_t2333_marshaled& marshaled);
void $ArrayType$128_t2333_marshal_back(const $ArrayType$128_t2333_marshaled& marshaled, $ArrayType$128_t2333& unmarshaled);
void $ArrayType$128_t2333_marshal_cleanup($ArrayType$128_t2333_marshaled& marshaled);
