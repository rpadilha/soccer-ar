﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t342;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t10;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
 void TweenRunner_1__ctor_m2296 (TweenRunner_1_t342 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
 Object_t * TweenRunner_1_Start_m18874 (Object_t * __this/* static, unused */, ColorTween_t314  ___tweenInfo, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
 void TweenRunner_1_Init_m2297 (TweenRunner_1_t342 * __this, MonoBehaviour_t10 * ___coroutineContainer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
 void TweenRunner_1_StartTween_m2325 (TweenRunner_1_t342 * __this, ColorTween_t314  ___info, MethodInfo* method) IL2CPP_METHOD_ATTR;
