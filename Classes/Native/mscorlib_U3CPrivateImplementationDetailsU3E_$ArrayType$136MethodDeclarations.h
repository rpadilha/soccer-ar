﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$136
struct $ArrayType$136_t2325;
struct $ArrayType$136_t2325_marshaled;

void $ArrayType$136_t2325_marshal(const $ArrayType$136_t2325& unmarshaled, $ArrayType$136_t2325_marshaled& marshaled);
void $ArrayType$136_t2325_marshal_back(const $ArrayType$136_t2325_marshaled& marshaled, $ArrayType$136_t2325& unmarshaled);
void $ArrayType$136_t2325_marshal_cleanup($ArrayType$136_t2325_marshaled& marshaled);
