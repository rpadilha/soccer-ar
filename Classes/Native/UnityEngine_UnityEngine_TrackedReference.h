﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.TrackedReference
struct TrackedReference_t1072  : public Object_t
{
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	IntPtr_t121 ___m_Ptr_0;
};
// Native definition for marshalling of: UnityEngine.TrackedReference
struct TrackedReference_t1072_marshaled
{
	IntPtr_t121 ___m_Ptr_0;
};
