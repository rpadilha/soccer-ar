﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.FormatterConverter
struct FormatterConverter_t2123;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.String
struct String_t;

// System.Void System.Runtime.Serialization.FormatterConverter::.ctor()
 void FormatterConverter__ctor_m11930 (FormatterConverter_t2123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.FormatterConverter::Convert(System.Object,System.Type)
 Object_t * FormatterConverter_Convert_m11931 (FormatterConverter_t2123 * __this, Object_t * ___value, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.FormatterConverter::ToBoolean(System.Object)
 bool FormatterConverter_ToBoolean_m11932 (FormatterConverter_t2123 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Runtime.Serialization.FormatterConverter::ToInt16(System.Object)
 int16_t FormatterConverter_ToInt16_m11933 (FormatterConverter_t2123 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.FormatterConverter::ToInt32(System.Object)
 int32_t FormatterConverter_ToInt32_m11934 (FormatterConverter_t2123 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Runtime.Serialization.FormatterConverter::ToInt64(System.Object)
 int64_t FormatterConverter_ToInt64_m11935 (FormatterConverter_t2123 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.FormatterConverter::ToString(System.Object)
 String_t* FormatterConverter_ToString_m11936 (FormatterConverter_t2123 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Runtime.Serialization.FormatterConverter::ToUInt32(System.Object)
 uint32_t FormatterConverter_ToUInt32_m11937 (FormatterConverter_t2123 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
