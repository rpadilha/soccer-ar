﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Animator>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_176.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animator>
struct CachedInvokableCall_1_t4870  : public InvokableCall_1_t4871
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Animator>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
