﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t64;
// Player_Script
struct Player_Script_t94;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t95;
// UnityEngine.GameObject
struct GameObject_t29;
// Sphere
struct Sphere_t71;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t96;
// UnityEngine.Material[]
struct MaterialU5BU5D_t97;
// ScorerTimeHUD
struct ScorerTimeHUD_t90;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// InGameState_Script/InGameState
#include "AssemblyU2DCSharp_InGameState_Script_InGameState.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// InGameState_Script
struct InGameState_Script_t83  : public MonoBehaviour_t10
{
	// UnityEngine.Material InGameState_Script::localTeam
	Material_t64 * ___localTeam_2;
	// UnityEngine.Material InGameState_Script::visitTeam
	Material_t64 * ___visitTeam_3;
	// Player_Script InGameState_Script::passer
	Player_Script_t94 * ___passer_4;
	// Player_Script InGameState_Script::passed
	Player_Script_t94 * ___passed_5;
	// Player_Script InGameState_Script::passer_oponent
	Player_Script_t94 * ___passer_oponent_6;
	// Player_Script InGameState_Script::passed_oponent
	Player_Script_t94 * ___passed_oponent_7;
	// System.Boolean InGameState_Script::scoredbylocal
	bool ___scoredbylocal_8;
	// System.Boolean InGameState_Script::scoredbyvisiting
	bool ___scoredbyvisiting_9;
	// InGameState_Script/InGameState InGameState_Script::state
	int32_t ___state_10;
	// UnityEngine.GameObject[] InGameState_Script::players
	GameObjectU5BU5D_t95* ___players_11;
	// UnityEngine.GameObject[] InGameState_Script::oponents
	GameObjectU5BU5D_t95* ___oponents_12;
	// UnityEngine.GameObject InGameState_Script::keeper
	GameObject_t29 * ___keeper_13;
	// UnityEngine.GameObject InGameState_Script::keeper_oponent
	GameObject_t29 * ___keeper_oponent_14;
	// UnityEngine.GameObject InGameState_Script::lastTouched
	GameObject_t29 * ___lastTouched_15;
	// System.Single InGameState_Script::timeToChangeState
	float ___timeToChangeState_16;
	// UnityEngine.Vector3 InGameState_Script::positionSide
	Vector3_t73  ___positionSide_17;
	// Sphere InGameState_Script::sphere
	Sphere_t71 * ___sphere_18;
	// UnityEngine.Transform InGameState_Script::center
	Transform_t74 * ___center_19;
	// UnityEngine.Vector3 InGameState_Script::target_throw_in
	Vector3_t73  ___target_throw_in_20;
	// UnityEngine.GameObject InGameState_Script::whoLastTouched
	GameObject_t29 * ___whoLastTouched_21;
	// UnityEngine.GameObject InGameState_Script::candidateToThrowIn
	GameObject_t29 * ___candidateToThrowIn_22;
	// System.Single InGameState_Script::timeToSaqueOponent
	float ___timeToSaqueOponent_23;
	// UnityEngine.Transform InGameState_Script::cornerSource
	Transform_t74 * ___cornerSource_24;
	// UnityEngine.GameObject InGameState_Script::areaCorner
	GameObject_t29 * ___areaCorner_25;
	// UnityEngine.Transform InGameState_Script::goal_kick
	Transform_t74 * ___goal_kick_26;
	// UnityEngine.GameObject InGameState_Script::goalKeeper
	GameObject_t29 * ___goalKeeper_27;
	// UnityEngine.GameObject InGameState_Script::cornerTrigger
	GameObject_t29 * ___cornerTrigger_28;
	// UnityEngine.Mesh[] InGameState_Script::Meshes
	MeshU5BU5D_t96* ___Meshes_29;
	// UnityEngine.Material[] InGameState_Script::Mat
	MaterialU5BU5D_t97* ___Mat_30;
	// System.Single InGameState_Script::timeToKickOff
	float ___timeToKickOff_31;
	// UnityEngine.GameObject InGameState_Script::lastCandidate
	GameObject_t29 * ___lastCandidate_32;
	// System.Int32 InGameState_Script::score_local
	int32_t ___score_local_33;
	// System.Int32 InGameState_Script::score_visiting
	int32_t ___score_visiting_34;
	// UnityEngine.GameObject[] InGameState_Script::playerPrefab
	GameObjectU5BU5D_t95* ___playerPrefab_35;
	// UnityEngine.GameObject InGameState_Script::goalKeeperPrefab
	GameObject_t29 * ___goalKeeperPrefab_36;
	// UnityEngine.GameObject InGameState_Script::ballPrefab
	GameObject_t29 * ___ballPrefab_37;
	// UnityEngine.Transform InGameState_Script::target_oponent_goal
	Transform_t74 * ___target_oponent_goal_38;
	// ScorerTimeHUD InGameState_Script::scorerTime
	ScorerTimeHUD_t90 * ___scorerTime_39;
	// System.Int32 InGameState_Script::bFirstHalf
	int32_t ___bFirstHalf_40;
	// UnityEngine.Material InGameState_Script::localMaterial
	Material_t64 * ___localMaterial_41;
	// UnityEngine.Material InGameState_Script::visitMaterial
	Material_t64 * ___visitMaterial_42;
};
