﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t48;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t46;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t597;

// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionBehaviour()
 ReconstructionAbstractBehaviour_t46 * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2900 (ReconstructionFromTargetAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionFromTarget()
 Object_t * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m2901 (ReconstructionFromTargetAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Awake()
 void ReconstructionFromTargetAbstractBehaviour_Awake_m2902 (ReconstructionFromTargetAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnDestroy()
 void ReconstructionFromTargetAbstractBehaviour_OnDestroy_m2903 (ReconstructionFromTargetAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Initialize()
 void ReconstructionFromTargetAbstractBehaviour_Initialize_m2904 (ReconstructionFromTargetAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnTrackerStarted()
 void ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2905 (ReconstructionFromTargetAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
 void ReconstructionFromTargetAbstractBehaviour__ctor_m447 (ReconstructionFromTargetAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
