﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.Surface>
struct Comparer_1_t4326;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.Surface>
struct Comparer_1_t4326  : public Object_t
{
};
struct Comparer_1_t4326_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.Surface>::_default
	Comparer_1_t4326 * ____default_0;
};
