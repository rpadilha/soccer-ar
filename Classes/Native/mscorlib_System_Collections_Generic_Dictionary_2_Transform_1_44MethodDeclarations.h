﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.UInt16>
struct Transform_1_t4127;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.UInt16>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m23611 (Transform_1_t4127 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.UInt16>::Invoke(TKey,TValue)
 uint16_t Transform_1_Invoke_m23612 (Transform_1_t4127 * __this, Type_t * ___key, uint16_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.UInt16>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m23613 (Transform_1_t4127 * __this, Type_t * ___key, uint16_t ___value, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.UInt16>::EndInvoke(System.IAsyncResult)
 uint16_t Transform_1_EndInvoke_m23614 (Transform_1_t4127 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
