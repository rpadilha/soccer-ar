﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARUnity
struct QCARUnity_t798;
// Vuforia.QCARUnity/QCARHint
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_QCARHint.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"

// System.Void Vuforia.QCARUnity::Deinit()
 void QCARUnity_Deinit_m317 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARUnity::IsRendererDirty()
 bool QCARUnity_IsRendererDirty_m4255 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARUnity::SetHint(Vuforia.QCARUnity/QCARHint,System.Int32)
 bool QCARUnity_SetHint_m4256 (Object_t * __this/* static, unused */, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARUnity::SetHint(System.Int32,System.Int32)
 bool QCARUnity_SetHint_m4257 (Object_t * __this/* static, unused */, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Vuforia.QCARUnity::GetProjectionGL(System.Single,System.Single,UnityEngine.ScreenOrientation)
 Matrix4x4_t176  QCARUnity_GetProjectionGL_m4258 (Object_t * __this/* static, unused */, float ___nearPlane, float ___farPlane, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARUnity::OnPause()
 void QCARUnity_OnPause_m315 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARUnity::OnResume()
 void QCARUnity_OnResume_m316 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARUnity::SetRendererDirty()
 void QCARUnity_SetRendererDirty_m4259 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
