﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t52;

// System.Void Vuforia.TextRecoBehaviour::.ctor()
 void TextRecoBehaviour__ctor_m86 (TextRecoBehaviour_t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
