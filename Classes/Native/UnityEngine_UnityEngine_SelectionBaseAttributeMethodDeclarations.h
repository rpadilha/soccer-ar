﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t542;

// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
 void SelectionBaseAttribute__ctor_m2572 (SelectionBaseAttribute_t542 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
