﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MarshalByRefObject
struct MarshalByRefObject_t1401;

// System.Void System.MarshalByRefObject::.ctor()
 void MarshalByRefObject__ctor_m7890 (MarshalByRefObject_t1401 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
