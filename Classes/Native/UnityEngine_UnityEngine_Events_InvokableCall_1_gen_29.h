﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>
struct UnityAction_1_t2979;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>
struct InvokableCall_1_t2978  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::Delegate
	UnityAction_1_t2979 * ___Delegate_0;
};
