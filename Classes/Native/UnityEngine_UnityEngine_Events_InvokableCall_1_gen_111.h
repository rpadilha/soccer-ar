﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.TrackableBehaviour>
struct UnityAction_1_t3833;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.TrackableBehaviour>
struct InvokableCall_1_t3832  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.TrackableBehaviour>::Delegate
	UnityAction_1_t3833 * ___Delegate_0;
};
