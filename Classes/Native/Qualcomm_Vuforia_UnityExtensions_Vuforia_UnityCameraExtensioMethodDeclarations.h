﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.UnityCameraExtensions
struct UnityCameraExtensions_t593;
// UnityEngine.Camera
struct Camera_t168;

// System.Int32 Vuforia.UnityCameraExtensions::GetPixelHeightInt(UnityEngine.Camera)
 int32_t UnityCameraExtensions_GetPixelHeightInt_m2795 (Object_t * __this/* static, unused */, Camera_t168 * ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.UnityCameraExtensions::GetPixelWidthInt(UnityEngine.Camera)
 int32_t UnityCameraExtensions_GetPixelWidthInt_m2796 (Object_t * __this/* static, unused */, Camera_t168 * ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.UnityCameraExtensions::GetMaxDepthForVideoBackground(UnityEngine.Camera)
 float UnityCameraExtensions_GetMaxDepthForVideoBackground_m2797 (Object_t * __this/* static, unused */, Camera_t168 * ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.UnityCameraExtensions::GetMinDepthForVideoBackground(UnityEngine.Camera)
 float UnityCameraExtensions_GetMinDepthForVideoBackground_m2798 (Object_t * __this/* static, unused */, Camera_t168 * ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
