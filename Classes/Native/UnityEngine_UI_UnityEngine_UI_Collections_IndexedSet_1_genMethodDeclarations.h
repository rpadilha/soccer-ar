﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>
struct IndexedSet_1_t327;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t330;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ICanvasElement>
struct IEnumerator_1_t3398;
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t3399;
// System.Predicate`1<UnityEngine.UI.ICanvasElement>
struct Predicate_1_t329;
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct Comparison_1_t328;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m2259(__this, method) (void)IndexedSet_1__ctor_m17827_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17828(__this, method) (Object_t *)IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17829_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m2270(__this, ___item, method) (void)IndexedSet_1_Add_m17830_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m2272(__this, ___item, method) (bool)IndexedSet_1_Remove_m17831_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m17832(__this, method) (Object_t*)IndexedSet_1_GetEnumerator_m17833_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m2269(__this, method) (void)IndexedSet_1_Clear_m17834_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m17835(__this, ___item, method) (bool)IndexedSet_1_Contains_m17836_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m17837(__this, ___array, ___arrayIndex, method) (void)IndexedSet_1_CopyTo_m17838_gshared((IndexedSet_1_t3400 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m2268(__this, method) (int32_t)IndexedSet_1_get_Count_m17839_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m17840(__this, method) (bool)IndexedSet_1_get_IsReadOnly_m17841_gshared((IndexedSet_1_t3400 *)__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m17842(__this, ___item, method) (int32_t)IndexedSet_1_IndexOf_m17843_gshared((IndexedSet_1_t3400 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m17844(__this, ___index, ___item, method) (void)IndexedSet_1_Insert_m17845_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m17846(__this, ___index, method) (void)IndexedSet_1_RemoveAt_m17847_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m2266(__this, ___index, method) (Object_t *)IndexedSet_1_get_Item_m17848_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m17849(__this, ___index, ___value, method) (void)IndexedSet_1_set_Item_m17850_gshared((IndexedSet_1_t3400 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m2264(__this, ___match, method) (void)IndexedSet_1_RemoveAll_m17851_gshared((IndexedSet_1_t3400 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m2265(__this, ___sortLayoutFunction, method) (void)IndexedSet_1_Sort_m17852_gshared((IndexedSet_1_t3400 *)__this, (Comparison_1_t2833 *)___sortLayoutFunction, method)
