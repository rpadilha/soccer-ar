﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARRenderer/VideoBGCfgData
struct VideoBGCfgData_t705;
struct VideoBGCfgData_t705_marshaled;

void VideoBGCfgData_t705_marshal(const VideoBGCfgData_t705& unmarshaled, VideoBGCfgData_t705_marshaled& marshaled);
void VideoBGCfgData_t705_marshal_back(const VideoBGCfgData_t705_marshaled& marshaled, VideoBGCfgData_t705& unmarshaled);
void VideoBGCfgData_t705_marshal_cleanup(VideoBGCfgData_t705_marshaled& marshaled);
