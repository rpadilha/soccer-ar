﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_164.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody>
struct CachedInvokableCall_1_t4804  : public InvokableCall_1_t4805
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
