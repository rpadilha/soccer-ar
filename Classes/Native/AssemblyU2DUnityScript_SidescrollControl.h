﻿#pragma once
#include <stdint.h>
// Joystick
struct Joystick_t208;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.CharacterController
struct CharacterController_t209;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// SidescrollControl
struct SidescrollControl_t221  : public MonoBehaviour_t10
{
	// Joystick SidescrollControl::moveTouchPad
	Joystick_t208 * ___moveTouchPad_2;
	// Joystick SidescrollControl::jumpTouchPad
	Joystick_t208 * ___jumpTouchPad_3;
	// System.Single SidescrollControl::forwardSpeed
	float ___forwardSpeed_4;
	// System.Single SidescrollControl::backwardSpeed
	float ___backwardSpeed_5;
	// System.Single SidescrollControl::jumpSpeed
	float ___jumpSpeed_6;
	// System.Single SidescrollControl::inAirMultiplier
	float ___inAirMultiplier_7;
	// UnityEngine.Transform SidescrollControl::thisTransform
	Transform_t74 * ___thisTransform_8;
	// UnityEngine.CharacterController SidescrollControl::character
	CharacterController_t209 * ___character_9;
	// UnityEngine.Vector3 SidescrollControl::velocity
	Vector3_t73  ___velocity_10;
	// System.Boolean SidescrollControl::canJump
	bool ___canJump_11;
};
