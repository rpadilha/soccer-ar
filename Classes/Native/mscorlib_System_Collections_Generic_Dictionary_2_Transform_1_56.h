﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t742;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Transform_1_t4202  : public MulticastDelegate_t373
{
};
