﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>
struct InternalEnumerator_1_t5303;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509K.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31887 (InternalEnumerator_1_t5303 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31888 (InternalEnumerator_1_t5303 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>::Dispose()
 void InternalEnumerator_1_Dispose_m31889 (InternalEnumerator_1_t5303 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31890 (InternalEnumerator_1_t5303 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31891 (InternalEnumerator_1_t5303 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
