﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t743;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct KeyCollection_t881  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::dictionary
	Dictionary_2_t743 * ___dictionary_0;
};
