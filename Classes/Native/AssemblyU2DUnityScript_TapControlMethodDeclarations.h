﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// TapControl
struct TapControl_t226;
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"

// System.Void TapControl::.ctor()
 void TapControl__ctor_m786 (TapControl_t226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapControl::Start()
 void TapControl_Start_m787 (TapControl_t226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapControl::OnEndGame()
 void TapControl_OnEndGame_m788 (TapControl_t226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapControl::FaceMovementDirection()
 void TapControl_FaceMovementDirection_m789 (TapControl_t226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapControl::CameraControl(UnityEngine.Touch,UnityEngine.Touch)
 void TapControl_CameraControl_m790 (TapControl_t226 * __this, Touch_t201  ___touch0, Touch_t201  ___touch1, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapControl::CharacterControl()
 void TapControl_CharacterControl_m791 (TapControl_t226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapControl::ResetControlState()
 void TapControl_ResetControlState_m792 (TapControl_t226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapControl::Update()
 void TapControl_Update_m793 (TapControl_t226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapControl::LateUpdate()
 void TapControl_LateUpdate_m794 (TapControl_t226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapControl::Main()
 void TapControl_Main_m795 (TapControl_t226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
