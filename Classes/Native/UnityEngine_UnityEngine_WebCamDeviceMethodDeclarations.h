﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WebCamDevice
struct WebCamDevice_t929;
struct WebCamDevice_t929_marshaled;
// System.String
struct String_t;

// System.String UnityEngine.WebCamDevice::get_name()
 String_t* WebCamDevice_get_name_m5457 (WebCamDevice_t929 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
 bool WebCamDevice_get_isFrontFacing_m6283 (WebCamDevice_t929 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void WebCamDevice_t929_marshal(const WebCamDevice_t929& unmarshaled, WebCamDevice_t929_marshaled& marshaled);
void WebCamDevice_t929_marshal_back(const WebCamDevice_t929_marshaled& marshaled, WebCamDevice_t929& unmarshaled);
void WebCamDevice_t929_marshal_cleanup(WebCamDevice_t929_marshaled& marshaled);
