﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderFallback
struct DecoderFallback_t2196;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2195;

// System.Void System.Text.DecoderFallback::.ctor()
 void DecoderFallback__ctor_m12412 (DecoderFallback_t2196 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallback::.cctor()
 void DecoderFallback__cctor_m12413 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_ExceptionFallback()
 DecoderFallback_t2196 * DecoderFallback_get_ExceptionFallback_m12414 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_ReplacementFallback()
 DecoderFallback_t2196 * DecoderFallback_get_ReplacementFallback_m12415 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_StandardSafeFallback()
 DecoderFallback_t2196 * DecoderFallback_get_StandardSafeFallback_m12416 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.DecoderFallback::CreateFallbackBuffer()
