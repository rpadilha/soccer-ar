﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<CameraRelativeControl>
struct UnityAction_1_t3116;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<CameraRelativeControl>
struct InvokableCall_1_t3115  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<CameraRelativeControl>::Delegate
	UnityAction_1_t3116 * ___Delegate_0;
};
