﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Canvas>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_181.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Canvas>
struct CachedInvokableCall_1_t4915  : public InvokableCall_1_t4916
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Canvas>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
