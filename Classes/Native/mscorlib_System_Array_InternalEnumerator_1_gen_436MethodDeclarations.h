﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>
struct InternalEnumerator_1_t4758;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RectTransform/Axis
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m28857 (InternalEnumerator_1_t4758 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28858 (InternalEnumerator_1_t4758 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::Dispose()
 void InternalEnumerator_1_Dispose_m28859 (InternalEnumerator_1_t4758 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m28860 (InternalEnumerator_1_t4758 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.RectTransform/Axis>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28861 (InternalEnumerator_1_t4758 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
