﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Random
struct Random_t1048;

// System.Single UnityEngine.Random::Range(System.Single,System.Single)
 float Random_Range_m620 (Object_t * __this/* static, unused */, float ___min, float ___max, MethodInfo* method) IL2CPP_METHOD_ATTR;
