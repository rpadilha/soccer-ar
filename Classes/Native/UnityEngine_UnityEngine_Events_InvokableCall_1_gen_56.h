﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<FollowTransform>
struct UnityAction_1_t3127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<FollowTransform>
struct InvokableCall_1_t3126  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<FollowTransform>::Delegate
	UnityAction_1_t3127 * ___Delegate_0;
};
