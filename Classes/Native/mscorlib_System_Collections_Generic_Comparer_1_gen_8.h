﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Transform>
struct Comparer_1_t3308;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Transform>
struct Comparer_1_t3308  : public Object_t
{
};
struct Comparer_1_t3308_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Transform>::_default
	Comparer_1_t3308 * ____default_0;
};
