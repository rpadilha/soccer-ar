﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerBehaviour
struct MarkerBehaviour_t39;

// System.Void Vuforia.MarkerBehaviour::.ctor()
 void MarkerBehaviour__ctor_m74 (MarkerBehaviour_t39 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
