﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.GraphicRaycaster>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_80.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.GraphicRaycaster>
struct CachedInvokableCall_1_t3540  : public InvokableCall_1_t3541
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.GraphicRaycaster>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
