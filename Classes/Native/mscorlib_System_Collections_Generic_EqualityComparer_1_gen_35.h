﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.DataSet>
struct EqualityComparer_1_t4039;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.DataSet>
struct EqualityComparer_1_t4039  : public Object_t
{
};
struct EqualityComparer_1_t4039_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.DataSet>::_default
	EqualityComparer_1_t4039 * ____default_0;
};
