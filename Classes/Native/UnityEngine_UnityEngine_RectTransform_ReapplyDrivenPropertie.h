﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// UnityEngine.RectTransform
struct RectTransform_t338;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t558  : public MulticastDelegate_t373
{
};
