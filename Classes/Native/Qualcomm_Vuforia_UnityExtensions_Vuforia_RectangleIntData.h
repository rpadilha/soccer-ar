﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.RectangleIntData
#pragma pack(push, tp, 1)
struct RectangleIntData_t633 
{
	// System.Int32 Vuforia.RectangleIntData::leftTopX
	int32_t ___leftTopX_0;
	// System.Int32 Vuforia.RectangleIntData::leftTopY
	int32_t ___leftTopY_1;
	// System.Int32 Vuforia.RectangleIntData::rightBottomX
	int32_t ___rightBottomX_2;
	// System.Int32 Vuforia.RectangleIntData::rightBottomY
	int32_t ___rightBottomY_3;
};
#pragma pack(pop, tp)
