﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTracker
struct SmartTerrainTracker_t720;
// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t618;

// System.Single Vuforia.SmartTerrainTracker::get_ScaleToMillimeter()
// System.Boolean Vuforia.SmartTerrainTracker::SetScaleToMillimeter(System.Single)
// Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder()
// System.Void Vuforia.SmartTerrainTracker::.ctor()
 void SmartTerrainTracker__ctor_m3230 (SmartTerrainTracker_t720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
