﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Color32>
struct InternalEnumerator_1_t4002;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m22328 (InternalEnumerator_1_t4002 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22329 (InternalEnumerator_1_t4002 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
 void InternalEnumerator_1_Dispose_m22330 (InternalEnumerator_1_t4002 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m22331 (InternalEnumerator_1_t4002 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
 Color32_t463  InternalEnumerator_1_get_Current_m22332 (InternalEnumerator_1_t4002 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
