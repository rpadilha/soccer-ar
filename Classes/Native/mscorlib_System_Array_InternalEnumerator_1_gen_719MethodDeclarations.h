﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>
struct InternalEnumerator_1_t5265;
// System.Object
struct Object_t;
// System.Runtime.InteropServices.PreserveSigAttribute
struct PreserveSigAttribute_t2029;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m31697(__this, ___array, method) (void)InternalEnumerator_1__ctor_m14156_gshared((InternalEnumerator_1_t2752 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31698(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m31699(__this, method) (void)InternalEnumerator_1_Dispose_m14160_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m31700(__this, method) (bool)InternalEnumerator_1_MoveNext_m14162_gshared((InternalEnumerator_1_t2752 *)__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m31701(__this, method) (PreserveSigAttribute_t2029 *)InternalEnumerator_1_get_Current_m14164_gshared((InternalEnumerator_1_t2752 *)__this, method)
