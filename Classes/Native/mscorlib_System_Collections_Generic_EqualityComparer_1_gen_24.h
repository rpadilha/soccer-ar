﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>
struct EqualityComparer_1_t3760;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>
struct EqualityComparer_1_t3760  : public Object_t
{
};
struct EqualityComparer_1_t3760_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>::_default
	EqualityComparer_1_t3760 * ____default_0;
};
