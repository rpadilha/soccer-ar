﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct KeyValuePair_2_t4685;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t1003;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m28292 (KeyValuePair_2_t4685 * __this, int32_t ___key, LayoutCache_t1003 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Key()
 int32_t KeyValuePair_2_get_Key_m28293 (KeyValuePair_2_t4685 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m28294 (KeyValuePair_2_t4685 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Value()
 LayoutCache_t1003 * KeyValuePair_2_get_Value_m28295 (KeyValuePair_2_t4685 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m28296 (KeyValuePair_2_t4685 * __this, LayoutCache_t1003 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::ToString()
 String_t* KeyValuePair_2_ToString_m28297 (KeyValuePair_2_t4685 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
