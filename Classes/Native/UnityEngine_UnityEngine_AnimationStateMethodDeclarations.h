﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationState
struct AnimationState_t186;

// System.Void UnityEngine.AnimationState::set_time(System.Single)
 void AnimationState_set_time_m657 (AnimationState_t186 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationState::get_normalizedTime()
 float AnimationState_get_normalizedTime_m613 (AnimationState_t186 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_speed(System.Single)
 void AnimationState_set_speed_m611 (AnimationState_t186 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
