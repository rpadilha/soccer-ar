﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.TripleDESCryptoServiceProvider
struct TripleDESCryptoServiceProvider_t2179;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t1608;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void System.Security.Cryptography.TripleDESCryptoServiceProvider::.ctor()
 void TripleDESCryptoServiceProvider__ctor_m12314 (TripleDESCryptoServiceProvider_t2179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.TripleDESCryptoServiceProvider::GenerateIV()
 void TripleDESCryptoServiceProvider_GenerateIV_m12315 (TripleDESCryptoServiceProvider_t2179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.TripleDESCryptoServiceProvider::GenerateKey()
 void TripleDESCryptoServiceProvider_GenerateKey_m12316 (TripleDESCryptoServiceProvider_t2179 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.TripleDESCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
 Object_t * TripleDESCryptoServiceProvider_CreateDecryptor_m12317 (TripleDESCryptoServiceProvider_t2179 * __this, ByteU5BU5D_t653* ___rgbKey, ByteU5BU5D_t653* ___rgbIV, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.TripleDESCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
 Object_t * TripleDESCryptoServiceProvider_CreateEncryptor_m12318 (TripleDESCryptoServiceProvider_t2179 * __this, ByteU5BU5D_t653* ___rgbKey, ByteU5BU5D_t653* ___rgbIV, MethodInfo* method) IL2CPP_METHOD_ATTR;
