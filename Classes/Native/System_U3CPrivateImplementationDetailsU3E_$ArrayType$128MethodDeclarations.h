﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$128
struct $ArrayType$128_t1524;
struct $ArrayType$128_t1524_marshaled;

void $ArrayType$128_t1524_marshal(const $ArrayType$128_t1524& unmarshaled, $ArrayType$128_t1524_marshaled& marshaled);
void $ArrayType$128_t1524_marshal_back(const $ArrayType$128_t1524_marshaled& marshaled, $ArrayType$128_t1524& unmarshaled);
void $ArrayType$128_t1524_marshal_cleanup($ArrayType$128_t1524_marshaled& marshaled);
