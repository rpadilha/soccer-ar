﻿#pragma once
#include <stdint.h>
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t1086;
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t1087;
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t1088;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.AttributeHelperEngine
struct AttributeHelperEngine_t1089  : public Object_t
{
};
struct AttributeHelperEngine_t1089_StaticFields{
	// UnityEngine.DisallowMultipleComponent[] UnityEngine.AttributeHelperEngine::_disallowMultipleComponentArray
	DisallowMultipleComponentU5BU5D_t1086* ____disallowMultipleComponentArray_0;
	// UnityEngine.ExecuteInEditMode[] UnityEngine.AttributeHelperEngine::_executeInEditModeArray
	ExecuteInEditModeU5BU5D_t1087* ____executeInEditModeArray_1;
	// UnityEngine.RequireComponent[] UnityEngine.AttributeHelperEngine::_requireComponentArray
	RequireComponentU5BU5D_t1088* ____requireComponentArray_2;
};
