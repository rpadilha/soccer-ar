﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUI
struct GUI_t119;
// UnityEngine.GUISkin
struct GUISkin_t997;
// UnityEngine.Material
struct Material_t64;
// System.String
struct String_t;
// UnityEngine.GUIContent
struct GUIContent_t529;
// UnityEngine.GUIStyle
struct GUIStyle_t999;
// UnityEngine.Texture
struct Texture_t107;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t118;
// System.DateTime
#include "mscorlib_System_DateTime.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.ScaleMode
#include "UnityEngine_UnityEngine_ScaleMode.h"

// System.Void UnityEngine.GUI::.cctor()
 void GUI__cctor_m5735 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_nextScrollStepTime(System.DateTime)
 void GUI_set_nextScrollStepTime_m5736 (Object_t * __this/* static, unused */, DateTime_t674  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
 void GUI_set_skin_m5737 (Object_t * __this/* static, unused */, GUISkin_t997 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
 GUISkin_t997 * GUI_get_skin_m5738 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::INTERNAL_get_color(UnityEngine.Color&)
 void GUI_INTERNAL_get_color_m5739 (Object_t * __this/* static, unused */, Color_t66 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUI::get_color()
 Color_t66  GUI_get_color_m5740 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_changed(System.Boolean)
 void GUI_set_changed_m5741 (Object_t * __this/* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String)
 void GUI_Label_m266 (Object_t * __this/* static, unused */, Rect_t103  ___position, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
 void GUI_Label_m5742 (Object_t * __this/* static, unused */, Rect_t103  ___position, GUIContent_t529 * ___content, GUIStyle_t999 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoLabel(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
 void GUI_DoLabel_m5743 (Object_t * __this/* static, unused */, Rect_t103  ___position, GUIContent_t529 * ___content, IntPtr_t121 ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
 void GUI_INTERNAL_CALL_DoLabel_m5744 (Object_t * __this/* static, unused */, Rect_t103 * ___position, GUIContent_t529 * ___content, IntPtr_t121 ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture)
 void GUI_DrawTexture_m720 (Object_t * __this/* static, unused */, Rect_t103  ___position, Texture_t107 * ___image, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.ScaleMode,System.Boolean,System.Single)
 void GUI_DrawTexture_m5745 (Object_t * __this/* static, unused */, Rect_t103  ___position, Texture_t107 * ___image, int32_t ___scaleMode, bool ___alphaBlend, float ___imageAspect, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.GUI::get_blendMaterial()
 Material_t64 * GUI_get_blendMaterial_m5746 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.GUI::get_blitMaterial()
 Material_t64 * GUI_get_blitMaterial_m5747 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
 bool GUI_Button_m267 (Object_t * __this/* static, unused */, Rect_t103  ___position, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::DoButton(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
 bool GUI_DoButton_m5748 (Object_t * __this/* static, unused */, Rect_t103  ___position, GUIContent_t529 * ___content, IntPtr_t121 ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
 bool GUI_INTERNAL_CALL_DoButton_m5749 (Object_t * __this/* static, unused */, Rect_t103 * ___position, GUIContent_t529 * ___content, IntPtr_t121 ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String)
 Rect_t103  GUI_Window_m264 (Object_t * __this/* static, unused */, int32_t ___id, Rect_t103  ___clientRect, WindowFunction_t118 * ___func, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::CallWindowDelegate(UnityEngine.GUI/WindowFunction,System.Int32,UnityEngine.GUISkin,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle)
 void GUI_CallWindowDelegate_m5750 (Object_t * __this/* static, unused */, WindowFunction_t118 * ___func, int32_t ___id, GUISkin_t997 * ____skin, int32_t ___forceRect, float ___width, float ___height, GUIStyle_t999 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::DoWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
 Rect_t103  GUI_DoWindow_m5751 (Object_t * __this/* static, unused */, int32_t ___id, Rect_t103  ___clientRect, WindowFunction_t118 * ___func, GUIContent_t529 * ___title, GUIStyle_t999 * ___style, GUISkin_t997 * ___skin, bool ___forceRectOnLayout, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
 Rect_t103  GUI_INTERNAL_CALL_DoWindow_m5752 (Object_t * __this/* static, unused */, int32_t ___id, Rect_t103 * ___clientRect, WindowFunction_t118 * ___func, GUIContent_t529 * ___title, GUIStyle_t999 * ___style, GUISkin_t997 * ___skin, bool ___forceRectOnLayout, MethodInfo* method) IL2CPP_METHOD_ATTR;
