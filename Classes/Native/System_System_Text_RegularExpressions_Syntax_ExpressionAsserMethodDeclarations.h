﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Syntax.ExpressionAssertion
struct ExpressionAssertion_t1497;
// System.Text.RegularExpressions.Syntax.Expression
struct Expression_t1496;
// System.Text.RegularExpressions.ICompiler
struct ICompiler_t1499;

// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::.ctor()
 void ExpressionAssertion__ctor_m7642 (ExpressionAssertion_t1497 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Reverse(System.Boolean)
 void ExpressionAssertion_set_Reverse_m7643 (ExpressionAssertion_t1497 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Negate(System.Boolean)
 void ExpressionAssertion_set_Negate_m7644 (ExpressionAssertion_t1497 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionAssertion::get_TestExpression()
 Expression_t1496 * ExpressionAssertion_get_TestExpression_m7645 (ExpressionAssertion_t1497 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_TestExpression(System.Text.RegularExpressions.Syntax.Expression)
 void ExpressionAssertion_set_TestExpression_m7646 (ExpressionAssertion_t1497 * __this, Expression_t1496 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
 void ExpressionAssertion_Compile_m7647 (ExpressionAssertion_t1497 * __this, Object_t * ___cmp, bool ___reverse, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.Syntax.ExpressionAssertion::IsComplex()
 bool ExpressionAssertion_IsComplex_m7648 (ExpressionAssertion_t1497 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
