﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.X509Crl
struct X509Crl_t1430;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t1572;
// System.Byte[]
struct ByteU5BU5D_t653;
// System.String
struct String_t;
// Mono.Security.X509.X509Crl/X509CrlEntry
struct X509CrlEntry_t1432;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1407;
// System.Security.Cryptography.DSA
struct DSA_t1408;
// System.Security.Cryptography.RSA
struct RSA_t1409;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1403;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void Mono.Security.X509.X509Crl::.ctor(System.Byte[])
 void X509Crl__ctor_m8388 (X509Crl_t1430 * __this, ByteU5BU5D_t653* ___crl, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Crl::Parse(System.Byte[])
 void X509Crl_Parse_m8389 (X509Crl_t1430 * __this, ByteU5BU5D_t653* ___crl, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl::get_Extensions()
 X509ExtensionCollection_t1572 * X509Crl_get_Extensions_m8020 (X509Crl_t1430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl::get_Hash()
 ByteU5BU5D_t653* X509Crl_get_Hash_m8390 (X509Crl_t1430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::get_IssuerName()
 String_t* X509Crl_get_IssuerName_m8028 (X509Crl_t1430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509Crl::get_NextUpdate()
 DateTime_t674  X509Crl_get_NextUpdate_m8026 (X509Crl_t1430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::Compare(System.Byte[],System.Byte[])
 bool X509Crl_Compare_m8391 (X509Crl_t1430 * __this, ByteU5BU5D_t653* ___array1, ByteU5BU5D_t653* ___array2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(Mono.Security.X509.X509Certificate)
 X509CrlEntry_t1432 * X509Crl_GetCrlEntry_m8024 (X509Crl_t1430 * __this, X509Certificate_t1407 * ___x509, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(System.Byte[])
 X509CrlEntry_t1432 * X509Crl_GetCrlEntry_m8392 (X509Crl_t1430 * __this, ByteU5BU5D_t653* ___serialNumber, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::GetHashName()
 String_t* X509Crl_GetHashName_m8393 (X509Crl_t1430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.DSA)
 bool X509Crl_VerifySignature_m8394 (X509Crl_t1430 * __this, DSA_t1408 * ___dsa, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.RSA)
 bool X509Crl_VerifySignature_m8395 (X509Crl_t1430 * __this, RSA_t1409 * ___rsa, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.AsymmetricAlgorithm)
 bool X509Crl_VerifySignature_m8023 (X509Crl_t1430 * __this, AsymmetricAlgorithm_t1403 * ___aa, MethodInfo* method) IL2CPP_METHOD_ATTR;
