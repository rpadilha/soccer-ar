﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t1;

// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
 void BackgroundPlaneBehaviour__ctor_m0 (BackgroundPlaneBehaviour_t1 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
