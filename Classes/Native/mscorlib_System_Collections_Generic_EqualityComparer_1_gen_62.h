﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct EqualityComparer_1_t4632;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct EqualityComparer_1_t4632  : public Object_t
{
};
struct EqualityComparer_1_t4632_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::_default
	EqualityComparer_1_t4632 * ____default_0;
};
