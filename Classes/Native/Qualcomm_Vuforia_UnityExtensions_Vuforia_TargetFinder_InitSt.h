﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.TargetFinder/InitState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
// Vuforia.TargetFinder/InitState
struct InitState_t774 
{
	// System.Int32 Vuforia.TargetFinder/InitState::value__
	int32_t ___value___1;
};
