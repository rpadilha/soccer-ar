﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Vuforia.DataSetLoadBehaviour
struct DataSetLoadBehaviour_t7;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>
struct UnityAction_1_t2785  : public MulticastDelegate_t373
{
};
