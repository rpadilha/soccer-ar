﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamImpl
struct WebCamImpl_t648;
// UnityEngine.Camera[]
struct CameraU5BU5D_t172;
// System.String
struct String_t;
// UnityEngine.Color32[]
struct Color32U5BU5D_t654;
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
// Vuforia.QCARRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoT.h"

// System.Boolean Vuforia.WebCamImpl::get_DidUpdateThisFrame()
 bool WebCamImpl_get_DidUpdateThisFrame_m4221 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamImpl::get_IsPlaying()
 bool WebCamImpl_get_IsPlaying_m4222 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WebCamImpl::get_ActualWidth()
 int32_t WebCamImpl_get_ActualWidth_m4223 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WebCamImpl::get_ActualHeight()
 int32_t WebCamImpl_get_ActualHeight_m4224 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamImpl::get_IsTextureSizeAvailable()
 bool WebCamImpl_get_IsTextureSizeAvailable_m4225 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::set_IsTextureSizeAvailable(System.Boolean)
 void WebCamImpl_set_IsTextureSizeAvailable_m4226 (WebCamImpl_t648 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamImpl::get_FlipHorizontally()
 bool WebCamImpl_get_FlipHorizontally_m4227 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/Vec2I Vuforia.WebCamImpl::get_ResampledTextureSize()
 Vec2I_t675  WebCamImpl_get_ResampledTextureSize_m4228 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::ComputeResampledTextureSize()
 void WebCamImpl_ComputeResampledTextureSize_m4229 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::.ctor(UnityEngine.Camera[],System.Int32,System.String,System.Boolean)
 void WebCamImpl__ctor_m4230 (WebCamImpl_t648 * __this, CameraU5BU5D_t172* ___arCameras, int32_t ___renderTextureLayer, String_t* ___webcamDeviceName, bool ___flipHorizontally, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::StartCamera()
 void WebCamImpl_StartCamera_m4231 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::StopCamera()
 void WebCamImpl_StopCamera_m4232 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::ResetPlaying()
 void WebCamImpl_ResetPlaying_m4233 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] Vuforia.WebCamImpl::GetPixels32AndBufferFrame()
 Color32U5BU5D_t654* WebCamImpl_GetPixels32AndBufferFrame_m4234 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::RenderFrame(System.Int32)
 void WebCamImpl_RenderFrame_m4235 (WebCamImpl_t648 * __this, int32_t ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/VideoModeData Vuforia.WebCamImpl::GetVideoMode()
 VideoModeData_t602  WebCamImpl_GetVideoMode_m4236 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoTextureInfo Vuforia.WebCamImpl::GetVideoTextureInfo()
 VideoTextureInfo_t588  WebCamImpl_GetVideoTextureInfo_m4237 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamImpl::IsRendererDirty()
 bool WebCamImpl_IsRendererDirty_m4238 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::OnDestroy()
 void WebCamImpl_OnDestroy_m4239 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::Update()
 void WebCamImpl_Update_m4240 (WebCamImpl_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
