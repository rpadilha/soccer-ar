﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
struct UnityAction_1_t4541;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
struct InvokableCall_1_t4540  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Delegate
	UnityAction_1_t4541 * ___Delegate_0;
};
