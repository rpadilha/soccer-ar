﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t514;

// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
 void DisallowMultipleComponent__ctor_m2333 (DisallowMultipleComponent_t514 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
