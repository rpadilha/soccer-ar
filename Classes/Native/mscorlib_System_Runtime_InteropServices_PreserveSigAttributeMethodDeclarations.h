﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.PreserveSigAttribute
struct PreserveSigAttribute_t2029;

// System.Void System.Runtime.InteropServices.PreserveSigAttribute::.ctor()
 void PreserveSigAttribute__ctor_m11603 (PreserveSigAttribute_t2029 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
