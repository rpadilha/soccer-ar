﻿#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"
#include "codegen/il2cpp-codegen.h"

// System.Array
#include "mscorlib_System_Array.h"
// System.Void
#include "mscorlib_System_Void.h"
#include "mscorlib_ArrayTypes.h"
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.RankException
#include "mscorlib_System_RankException.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
extern TypeInfo ArgumentNullException_t1224_il2cpp_TypeInfo;
extern TypeInfo RankException_t2296_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
extern TypeInfo ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo;
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// Locale
#include "mscorlib_LocaleMethodDeclarations.h"
// System.RankException
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
extern MethodInfo ArgumentNullException__ctor_m6710_MethodInfo;
extern MethodInfo Array_get_Rank_m7838_MethodInfo;
extern MethodInfo Locale_GetText_m9928_MethodInfo;
extern MethodInfo RankException__ctor_m13346_MethodInfo;
extern MethodInfo Array_GetLength_m9760_MethodInfo;
extern MethodInfo Array_GetLowerBound_m9762_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo ArgumentOutOfRangeException__ctor_m7832_MethodInfo;
extern MethodInfo Array_Copy_m9801_MethodInfo;
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTimeKind>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisDateTimeKind_t2252_m42232_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTimeKind>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTimeKind>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisDateTimeKind_t2252_m42232 (Array_t * __this, DateTimeKindU5BU5D_t5609* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTimeKind>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisDateTimeKind_t2252_m42232 (Array_t * __this, DateTimeKindU5BU5D_t5609* ___array, int32_t ___index, MethodInfo* method){
	{
		if (___array)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_3 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_3, L_2, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		NullCheck(___array);
		int32_t L_6 = Array_GetLength_m9760(___array, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		if ((((int32_t)((int32_t)(___index+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t551 * L_7 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_7, (String_t*) &_stringLiteral1279, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		NullCheck(___array);
		int32_t L_8 = Array_get_Rank_m7838(___array, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_10 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_10, L_9, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)___index) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1280, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		ArgumentOutOfRangeException_t1547 * L_12 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7832(L_12, (String_t*) &_stringLiteral474, L_11, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7832_MethodInfo);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		int32_t L_14 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		Array_Copy_m9801(NULL /*static, unused*/, __this, L_13, (Array_t *)(Array_t *)___array, ___index, L_14, /*hidden argument*/&Array_Copy_m9801_MethodInfo);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTimeKind>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
extern Il2CppType DateTimeKindU5BU5D_t5609_0_0_0;
extern Il2CppType DateTimeKindU5BU5D_t5609_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisDateTimeKind_t2252_m42232_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &DateTimeKindU5BU5D_t5609_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_DateTimeKind_t2252_0_0_0;
extern Il2CppType Void_t111_0_0_0;
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisDateTimeKind_t2252_m42232_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisDateTimeKind_t2252_m42232_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDateTimeKind_t2252_m42232/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisDateTimeKind_t2252_m42232_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisDateTimeKind_t2252_m42232_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
extern TypeInfo NotSupportedException_t498_il2cpp_TypeInfo;
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern MethodInfo NotSupportedException__ctor_m7849_MethodInfo;
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTimeKind>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisDateTimeKind_t2252_m42233_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTimeKind>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTimeKind>(T)
 bool Array_InternalArray__ICollection_Remove_TisDateTimeKind_t2252_m42233 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTimeKind>(T)
 bool Array_InternalArray__ICollection_Remove_TisDateTimeKind_t2252_m42233 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTimeKind>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo Boolean_t122_il2cpp_TypeInfo;
extern Il2CppType DateTimeKind_t2252_0_0_0;
extern Il2CppType DateTimeKind_t2252_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisDateTimeKind_t2252_m42233_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &DateTimeKind_t2252_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisDateTimeKind_t2252_m42233_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisDateTimeKind_t2252_m42233_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisDateTimeKind_t2252_m42233/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisDateTimeKind_t2252_m42233_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisDateTimeKind_t2252_m42233_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Object
#include "mscorlib_System_Object.h"
extern TypeInfo DateTimeKind_t2252_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_GetGenericValueImpl_TisDateTimeKind_t2252_m42228_MethodInfo;
extern MethodInfo Object_Equals_m320_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.DateTimeKind>(System.Int32,!!0&)
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTimeKind>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisDateTimeKind_t2252_m42234_MethodInfo;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.DateTimeKind>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTimeKind>(T)
 int32_t Array_InternalArray__IndexOf_TisDateTimeKind_t2252_m42234 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTimeKind>(T)
 int32_t Array_InternalArray__IndexOf_TisDateTimeKind_t2252_m42234 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&DateTimeKind_t2252_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&DateTimeKind_t2252_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = ___item;
		Object_t * L_11 = Box(InitializedTypeInfo(&DateTimeKind_t2252_il2cpp_TypeInfo), &L_10);
		NullCheck(Box(InitializedTypeInfo(&DateTimeKind_t2252_il2cpp_TypeInfo), &(*(&V_2))));
		bool L_12 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&DateTimeKind_t2252_il2cpp_TypeInfo), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.DateTimeKind>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
extern Il2CppType DateTimeKind_t2252_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisDateTimeKind_t2252_m42234_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &DateTimeKind_t2252_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisDateTimeKind_t2252_m42234_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisDateTimeKind_t2252_m42234_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisDateTimeKind_t2252_m42234/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisDateTimeKind_t2252_m42234_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisDateTimeKind_t2252_m42234_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.DateTimeKind>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisDateTimeKind_t2252_m42235_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.DateTimeKind>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.DateTimeKind>(System.Int32,T)
 void Array_InternalArray__Insert_TisDateTimeKind_t2252_m42235 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__Insert<System.DateTimeKind>(System.Int32,T)
 void Array_InternalArray__Insert_TisDateTimeKind_t2252_m42235 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.DateTimeKind>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DateTimeKind_t2252_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisDateTimeKind_t2252_m42235_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &DateTimeKind_t2252_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisDateTimeKind_t2252_m42235_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisDateTimeKind_t2252_m42235_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisDateTimeKind_t2252_m42235/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisDateTimeKind_t2252_m42235_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisDateTimeKind_t2252_m42235_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern MethodInfo ArgumentOutOfRangeException__ctor_m7836_MethodInfo;
extern MethodInfo Array_SetGenericValueImpl_TisDateTimeKind_t2252_m42236_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.DateTimeKind>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.DateTimeKind>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisDateTimeKind_t2252_m42237_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.DateTimeKind>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.DateTimeKind>(System.Int32,T)
 void Array_InternalArray__set_Item_TisDateTimeKind_t2252_m42237 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__set_Item<System.DateTimeKind>(System.Int32,T)
 void Array_InternalArray__set_Item_TisDateTimeKind_t2252_m42237 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	ObjectU5BU5D_t130* V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((ObjectU5BU5D_t130*)IsInst(__this, InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = ___item;
		Object_t * L_3 = Box(InitializedTypeInfo(&DateTimeKind_t2252_il2cpp_TypeInfo), &L_2);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ___index);
		ArrayElementTypeCheck (V_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(V_0, ___index)) = (Object_t *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, ___index, (&___item));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.DateTimeKind>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DateTimeKind_t2252_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisDateTimeKind_t2252_m42237_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &DateTimeKind_t2252_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisDateTimeKind_t2252_m42237_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisDateTimeKind_t2252_m42237_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisDateTimeKind_t2252_m42237/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisDateTimeKind_t2252_m42237_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisDateTimeKind_t2252_m42237_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.DateTimeKind>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.DateTimeKind>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DateTimeKind_t2252_1_0_0;
extern Il2CppType DateTimeKind_t2252_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisDateTimeKind_t2252_m42236_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DateTimeKind_t2252_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisDateTimeKind_t2252_m42236_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_DateTimeKindU26_t7511 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisDateTimeKind_t2252_m42236_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_DateTimeKindU26_t7511/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisDateTimeKind_t2252_m42236_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisDateTimeKind_t2252_m42236_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.DateTimeKind>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_772.h"
extern TypeInfo InternalEnumerator_1_t5335_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.DateTimeKind>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_772MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32122_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTimeKind>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisDateTimeKind_t2252_m42238_MethodInfo;
struct Array_t;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTimeKind>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTimeKind>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDateTimeKind_t2252_m42238 (Array_t * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTimeKind>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDateTimeKind_t2252_m42238 (Array_t * __this, MethodInfo* method){
	{
		InternalEnumerator_1_t5335  L_0 = {0};
		InternalEnumerator_1__ctor_m32122(&L_0, __this, /*hidden argument*/&InternalEnumerator_1__ctor_m32122_MethodInfo);
		InternalEnumerator_1_t5335  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&InternalEnumerator_1_t5335_il2cpp_TypeInfo), &L_1);
		return (Object_t*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTimeKind>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7512_il2cpp_TypeInfo;
extern Il2CppType IEnumerator_1_t7512_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisDateTimeKind_t2252_m42238_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisDateTimeKind_t2252_m42238_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDateTimeKind_t2252_m42238/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7512_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisDateTimeKind_t2252_m42238_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"
extern MethodInfo Array_GetGenericValueImpl_TisDayOfWeek_t2255_m42239_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.DayOfWeek>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.DayOfWeek>(System.Int32)
extern MethodInfo Array_InternalArray__get_Item_TisDayOfWeek_t2255_m42240_MethodInfo;
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.DayOfWeek>(System.Int32)
// T System.Array::InternalArray__get_Item<System.DayOfWeek>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisDayOfWeek_t2255_m42240 (Array_t * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array::InternalArray__get_Item<System.DayOfWeek>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisDayOfWeek_t2255_m42240 (Array_t * __this, int32_t ___index, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, ___index, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.DayOfWeek>(System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo DayOfWeek_t2255_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__get_Item_TisDayOfWeek_t2255_m42240_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_DayOfWeek_t2255_0_0_0;
extern Il2CppType DayOfWeek_t2255_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__get_Item_TisDayOfWeek_t2255_m42240_GenericMethod;
extern void* RuntimeInvoker_DayOfWeek_t2255_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__get_Item_TisDayOfWeek_t2255_m42240_MethodInfo = 
{
	"InternalArray__get_Item"/* name */
	, (methodPointerType)&Array_InternalArray__get_Item_TisDayOfWeek_t2255_m42240/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &DayOfWeek_t2255_0_0_0/* return_type */
	, RuntimeInvoker_DayOfWeek_t2255_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__get_Item_TisDayOfWeek_t2255_m42240_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__get_Item_TisDayOfWeek_t2255_m42240_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.DayOfWeek>(System.Int32,T&)
// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.DayOfWeek>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DayOfWeek_t2255_1_0_2;
extern Il2CppType DayOfWeek_t2255_1_0_0;
static ParameterInfo Array_t_Array_GetGenericValueImpl_TisDayOfWeek_t2255_m42239_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DayOfWeek_t2255_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_GetGenericValueImpl_TisDayOfWeek_t2255_m42239_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_DayOfWeekU26_t7513 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_GetGenericValueImpl_TisDayOfWeek_t2255_m42239_MethodInfo = 
{
	"GetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_DayOfWeekU26_t7513/* invoker_method */
	, Array_t_Array_GetGenericValueImpl_TisDayOfWeek_t2255_m42239_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_GetGenericValueImpl_TisDayOfWeek_t2255_m42239_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.DayOfWeek>(T)
extern MethodInfo Array_InternalArray__ICollection_Add_TisDayOfWeek_t2255_m42241_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.DayOfWeek>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.DayOfWeek>(T)
 void Array_InternalArray__ICollection_Add_TisDayOfWeek_t2255_m42241 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_Add<System.DayOfWeek>(T)
 void Array_InternalArray__ICollection_Add_TisDayOfWeek_t2255_m42241 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.DayOfWeek>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType DayOfWeek_t2255_0_0_0;
extern Il2CppType DayOfWeek_t2255_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Add_TisDayOfWeek_t2255_m42241_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &DayOfWeek_t2255_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Add_TisDayOfWeek_t2255_m42241_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Add_TisDayOfWeek_t2255_m42241_MethodInfo = 
{
	"InternalArray__ICollection_Add"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Add_TisDayOfWeek_t2255_m42241/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Add_TisDayOfWeek_t2255_m42241_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Add_TisDayOfWeek_t2255_m42241_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DayOfWeek>(T)
extern MethodInfo Array_InternalArray__ICollection_Contains_TisDayOfWeek_t2255_m42242_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.DayOfWeek>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DayOfWeek>(T)
 bool Array_InternalArray__ICollection_Contains_TisDayOfWeek_t2255_m42242 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DayOfWeek>(T)
 bool Array_InternalArray__ICollection_Contains_TisDayOfWeek_t2255_m42242 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		Object_t * L_9 = Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &L_8);
		NullCheck(Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &(*(&___item))));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &(*(&___item))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.DayOfWeek>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType DayOfWeek_t2255_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Contains_TisDayOfWeek_t2255_m42242_ParameterInfos[] = 
{
	{"item", 0, 134218874, &EmptyCustomAttributesCache, &DayOfWeek_t2255_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Contains_TisDayOfWeek_t2255_m42242_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Contains_TisDayOfWeek_t2255_m42242_MethodInfo = 
{
	"InternalArray__ICollection_Contains"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Contains_TisDayOfWeek_t2255_m42242/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Contains_TisDayOfWeek_t2255_m42242_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Contains_TisDayOfWeek_t2255_m42242_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DayOfWeek>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisDayOfWeek_t2255_m42243_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.DayOfWeek>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DayOfWeek>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisDayOfWeek_t2255_m42243 (Array_t * __this, DayOfWeekU5BU5D_t5610* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DayOfWeek>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisDayOfWeek_t2255_m42243 (Array_t * __this, DayOfWeekU5BU5D_t5610* ___array, int32_t ___index, MethodInfo* method){
	{
		if (___array)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_3 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_3, L_2, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		NullCheck(___array);
		int32_t L_6 = Array_GetLength_m9760(___array, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		if ((((int32_t)((int32_t)(___index+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t551 * L_7 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_7, (String_t*) &_stringLiteral1279, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		NullCheck(___array);
		int32_t L_8 = Array_get_Rank_m7838(___array, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_10 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_10, L_9, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)___index) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1280, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		ArgumentOutOfRangeException_t1547 * L_12 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7832(L_12, (String_t*) &_stringLiteral474, L_11, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7832_MethodInfo);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		int32_t L_14 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		Array_Copy_m9801(NULL /*static, unused*/, __this, L_13, (Array_t *)(Array_t *)___array, ___index, L_14, /*hidden argument*/&Array_Copy_m9801_MethodInfo);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.DayOfWeek>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType DayOfWeekU5BU5D_t5610_0_0_0;
extern Il2CppType DayOfWeekU5BU5D_t5610_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisDayOfWeek_t2255_m42243_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &DayOfWeekU5BU5D_t5610_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisDayOfWeek_t2255_m42243_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisDayOfWeek_t2255_m42243_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDayOfWeek_t2255_m42243/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisDayOfWeek_t2255_m42243_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisDayOfWeek_t2255_m42243_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DayOfWeek>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisDayOfWeek_t2255_m42244_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.DayOfWeek>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DayOfWeek>(T)
 bool Array_InternalArray__ICollection_Remove_TisDayOfWeek_t2255_m42244 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DayOfWeek>(T)
 bool Array_InternalArray__ICollection_Remove_TisDayOfWeek_t2255_m42244 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.DayOfWeek>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType DayOfWeek_t2255_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisDayOfWeek_t2255_m42244_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &DayOfWeek_t2255_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisDayOfWeek_t2255_m42244_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisDayOfWeek_t2255_m42244_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisDayOfWeek_t2255_m42244/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisDayOfWeek_t2255_m42244_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisDayOfWeek_t2255_m42244_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.DayOfWeek>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisDayOfWeek_t2255_m42245_MethodInfo;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.DayOfWeek>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.DayOfWeek>(T)
 int32_t Array_InternalArray__IndexOf_TisDayOfWeek_t2255_m42245 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::InternalArray__IndexOf<System.DayOfWeek>(T)
 int32_t Array_InternalArray__IndexOf_TisDayOfWeek_t2255_m42245 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = ___item;
		Object_t * L_11 = Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &L_10);
		NullCheck(Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &(*(&V_2))));
		bool L_12 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.DayOfWeek>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType DayOfWeek_t2255_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisDayOfWeek_t2255_m42245_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &DayOfWeek_t2255_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisDayOfWeek_t2255_m42245_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisDayOfWeek_t2255_m42245_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisDayOfWeek_t2255_m42245/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisDayOfWeek_t2255_m42245_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisDayOfWeek_t2255_m42245_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.DayOfWeek>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisDayOfWeek_t2255_m42246_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.DayOfWeek>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.DayOfWeek>(System.Int32,T)
 void Array_InternalArray__Insert_TisDayOfWeek_t2255_m42246 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__Insert<System.DayOfWeek>(System.Int32,T)
 void Array_InternalArray__Insert_TisDayOfWeek_t2255_m42246 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.DayOfWeek>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DayOfWeek_t2255_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisDayOfWeek_t2255_m42246_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &DayOfWeek_t2255_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisDayOfWeek_t2255_m42246_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisDayOfWeek_t2255_m42246_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisDayOfWeek_t2255_m42246/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisDayOfWeek_t2255_m42246_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisDayOfWeek_t2255_m42246_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Array_SetGenericValueImpl_TisDayOfWeek_t2255_m42247_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.DayOfWeek>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.DayOfWeek>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisDayOfWeek_t2255_m42248_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.DayOfWeek>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.DayOfWeek>(System.Int32,T)
 void Array_InternalArray__set_Item_TisDayOfWeek_t2255_m42248 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__set_Item<System.DayOfWeek>(System.Int32,T)
 void Array_InternalArray__set_Item_TisDayOfWeek_t2255_m42248 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	ObjectU5BU5D_t130* V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((ObjectU5BU5D_t130*)IsInst(__this, InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = ___item;
		Object_t * L_3 = Box(InitializedTypeInfo(&DayOfWeek_t2255_il2cpp_TypeInfo), &L_2);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ___index);
		ArrayElementTypeCheck (V_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(V_0, ___index)) = (Object_t *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, ___index, (&___item));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.DayOfWeek>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DayOfWeek_t2255_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisDayOfWeek_t2255_m42248_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &DayOfWeek_t2255_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisDayOfWeek_t2255_m42248_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisDayOfWeek_t2255_m42248_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisDayOfWeek_t2255_m42248/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisDayOfWeek_t2255_m42248_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisDayOfWeek_t2255_m42248_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.DayOfWeek>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.DayOfWeek>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DayOfWeek_t2255_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisDayOfWeek_t2255_m42247_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DayOfWeek_t2255_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisDayOfWeek_t2255_m42247_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_DayOfWeekU26_t7513 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisDayOfWeek_t2255_m42247_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_DayOfWeekU26_t7513/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisDayOfWeek_t2255_m42247_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisDayOfWeek_t2255_m42247_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.DayOfWeek>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_773.h"
extern TypeInfo InternalEnumerator_1_t5340_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.DayOfWeek>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_773MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32148_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DayOfWeek>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisDayOfWeek_t2255_m42249_MethodInfo;
struct Array_t;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DayOfWeek>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DayOfWeek>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDayOfWeek_t2255_m42249 (Array_t * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DayOfWeek>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDayOfWeek_t2255_m42249 (Array_t * __this, MethodInfo* method){
	{
		InternalEnumerator_1_t5340  L_0 = {0};
		InternalEnumerator_1__ctor_m32148(&L_0, __this, /*hidden argument*/&InternalEnumerator_1__ctor_m32148_MethodInfo);
		InternalEnumerator_1_t5340  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&InternalEnumerator_1_t5340_il2cpp_TypeInfo), &L_1);
		return (Object_t*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DayOfWeek>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7514_il2cpp_TypeInfo;
extern Il2CppType IEnumerator_1_t7514_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisDayOfWeek_t2255_m42249_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisDayOfWeek_t2255_m42249_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDayOfWeek_t2255_m42249/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7514_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisDayOfWeek_t2255_m42249_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Environment/SpecialFolder
#include "mscorlib_System_Environment_SpecialFolder.h"
extern MethodInfo Array_GetGenericValueImpl_TisSpecialFolder_t2267_m42250_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.Environment/SpecialFolder>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.Environment/SpecialFolder>(System.Int32)
extern MethodInfo Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251_MethodInfo;
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.Environment/SpecialFolder>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Environment/SpecialFolder>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251 (Array_t * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array::InternalArray__get_Item<System.Environment/SpecialFolder>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251 (Array_t * __this, int32_t ___index, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, ___index, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Environment/SpecialFolder>(System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo SpecialFolder_t2267_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_SpecialFolder_t2267_0_0_0;
extern Il2CppType SpecialFolder_t2267_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251_GenericMethod;
extern void* RuntimeInvoker_SpecialFolder_t2267_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251_MethodInfo = 
{
	"InternalArray__get_Item"/* name */
	, (methodPointerType)&Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &SpecialFolder_t2267_0_0_0/* return_type */
	, RuntimeInvoker_SpecialFolder_t2267_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__get_Item_TisSpecialFolder_t2267_m42251_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.Environment/SpecialFolder>(System.Int32,T&)
// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Environment/SpecialFolder>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SpecialFolder_t2267_1_0_2;
extern Il2CppType SpecialFolder_t2267_1_0_0;
static ParameterInfo Array_t_Array_GetGenericValueImpl_TisSpecialFolder_t2267_m42250_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SpecialFolder_t2267_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_GetGenericValueImpl_TisSpecialFolder_t2267_m42250_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_SpecialFolderU26_t7515 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_GetGenericValueImpl_TisSpecialFolder_t2267_m42250_MethodInfo = 
{
	"GetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_SpecialFolderU26_t7515/* invoker_method */
	, Array_t_Array_GetGenericValueImpl_TisSpecialFolder_t2267_m42250_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_GetGenericValueImpl_TisSpecialFolder_t2267_m42250_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.Environment/SpecialFolder>(T)
extern MethodInfo Array_InternalArray__ICollection_Add_TisSpecialFolder_t2267_m42252_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.Environment/SpecialFolder>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Environment/SpecialFolder>(T)
 void Array_InternalArray__ICollection_Add_TisSpecialFolder_t2267_m42252 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_Add<System.Environment/SpecialFolder>(T)
 void Array_InternalArray__ICollection_Add_TisSpecialFolder_t2267_m42252 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Environment/SpecialFolder>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType SpecialFolder_t2267_0_0_0;
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Add_TisSpecialFolder_t2267_m42252_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Add_TisSpecialFolder_t2267_m42252_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Add_TisSpecialFolder_t2267_m42252_MethodInfo = 
{
	"InternalArray__ICollection_Add"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Add_TisSpecialFolder_t2267_m42252/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Add_TisSpecialFolder_t2267_m42252_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Add_TisSpecialFolder_t2267_m42252_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Environment/SpecialFolder>(T)
extern MethodInfo Array_InternalArray__ICollection_Contains_TisSpecialFolder_t2267_m42253_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.Environment/SpecialFolder>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Environment/SpecialFolder>(T)
 bool Array_InternalArray__ICollection_Contains_TisSpecialFolder_t2267_m42253 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Environment/SpecialFolder>(T)
 bool Array_InternalArray__ICollection_Contains_TisSpecialFolder_t2267_m42253 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		Object_t * L_9 = Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &L_8);
		NullCheck(Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &(*(&___item))));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &(*(&___item))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Environment/SpecialFolder>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Contains_TisSpecialFolder_t2267_m42253_ParameterInfos[] = 
{
	{"item", 0, 134218874, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Contains_TisSpecialFolder_t2267_m42253_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Contains_TisSpecialFolder_t2267_m42253_MethodInfo = 
{
	"InternalArray__ICollection_Contains"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Contains_TisSpecialFolder_t2267_m42253/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Contains_TisSpecialFolder_t2267_m42253_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Contains_TisSpecialFolder_t2267_m42253_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Environment/SpecialFolder>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisSpecialFolder_t2267_m42254_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.Environment/SpecialFolder>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Environment/SpecialFolder>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisSpecialFolder_t2267_m42254 (Array_t * __this, SpecialFolderU5BU5D_t5611* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Environment/SpecialFolder>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisSpecialFolder_t2267_m42254 (Array_t * __this, SpecialFolderU5BU5D_t5611* ___array, int32_t ___index, MethodInfo* method){
	{
		if (___array)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_3 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_3, L_2, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		NullCheck(___array);
		int32_t L_6 = Array_GetLength_m9760(___array, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		if ((((int32_t)((int32_t)(___index+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t551 * L_7 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_7, (String_t*) &_stringLiteral1279, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		NullCheck(___array);
		int32_t L_8 = Array_get_Rank_m7838(___array, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_10 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_10, L_9, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)___index) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1280, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		ArgumentOutOfRangeException_t1547 * L_12 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7832(L_12, (String_t*) &_stringLiteral474, L_11, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7832_MethodInfo);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		int32_t L_14 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		Array_Copy_m9801(NULL /*static, unused*/, __this, L_13, (Array_t *)(Array_t *)___array, ___index, L_14, /*hidden argument*/&Array_Copy_m9801_MethodInfo);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Environment/SpecialFolder>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType SpecialFolderU5BU5D_t5611_0_0_0;
extern Il2CppType SpecialFolderU5BU5D_t5611_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisSpecialFolder_t2267_m42254_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &SpecialFolderU5BU5D_t5611_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisSpecialFolder_t2267_m42254_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisSpecialFolder_t2267_m42254_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSpecialFolder_t2267_m42254/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisSpecialFolder_t2267_m42254_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisSpecialFolder_t2267_m42254_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Environment/SpecialFolder>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisSpecialFolder_t2267_m42255_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.Environment/SpecialFolder>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Environment/SpecialFolder>(T)
 bool Array_InternalArray__ICollection_Remove_TisSpecialFolder_t2267_m42255 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Environment/SpecialFolder>(T)
 bool Array_InternalArray__ICollection_Remove_TisSpecialFolder_t2267_m42255 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Environment/SpecialFolder>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisSpecialFolder_t2267_m42255_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisSpecialFolder_t2267_m42255_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisSpecialFolder_t2267_m42255_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisSpecialFolder_t2267_m42255/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisSpecialFolder_t2267_m42255_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisSpecialFolder_t2267_m42255_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.Environment/SpecialFolder>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisSpecialFolder_t2267_m42256_MethodInfo;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.Environment/SpecialFolder>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Environment/SpecialFolder>(T)
 int32_t Array_InternalArray__IndexOf_TisSpecialFolder_t2267_m42256 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::InternalArray__IndexOf<System.Environment/SpecialFolder>(T)
 int32_t Array_InternalArray__IndexOf_TisSpecialFolder_t2267_m42256 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = ___item;
		Object_t * L_11 = Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &L_10);
		NullCheck(Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &(*(&V_2))));
		bool L_12 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Environment/SpecialFolder>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisSpecialFolder_t2267_m42256_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisSpecialFolder_t2267_m42256_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisSpecialFolder_t2267_m42256_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisSpecialFolder_t2267_m42256/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisSpecialFolder_t2267_m42256_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisSpecialFolder_t2267_m42256_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.Environment/SpecialFolder>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisSpecialFolder_t2267_m42257_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.Environment/SpecialFolder>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Environment/SpecialFolder>(System.Int32,T)
 void Array_InternalArray__Insert_TisSpecialFolder_t2267_m42257 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__Insert<System.Environment/SpecialFolder>(System.Int32,T)
 void Array_InternalArray__Insert_TisSpecialFolder_t2267_m42257 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Environment/SpecialFolder>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisSpecialFolder_t2267_m42257_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisSpecialFolder_t2267_m42257_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisSpecialFolder_t2267_m42257_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisSpecialFolder_t2267_m42257/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisSpecialFolder_t2267_m42257_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisSpecialFolder_t2267_m42257_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Array_SetGenericValueImpl_TisSpecialFolder_t2267_m42258_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.Environment/SpecialFolder>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.Environment/SpecialFolder>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisSpecialFolder_t2267_m42259_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.Environment/SpecialFolder>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Environment/SpecialFolder>(System.Int32,T)
 void Array_InternalArray__set_Item_TisSpecialFolder_t2267_m42259 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__set_Item<System.Environment/SpecialFolder>(System.Int32,T)
 void Array_InternalArray__set_Item_TisSpecialFolder_t2267_m42259 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	ObjectU5BU5D_t130* V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((ObjectU5BU5D_t130*)IsInst(__this, InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = ___item;
		Object_t * L_3 = Box(InitializedTypeInfo(&SpecialFolder_t2267_il2cpp_TypeInfo), &L_2);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ___index);
		ArrayElementTypeCheck (V_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(V_0, ___index)) = (Object_t *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, ___index, (&___item));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Environment/SpecialFolder>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SpecialFolder_t2267_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisSpecialFolder_t2267_m42259_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &SpecialFolder_t2267_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisSpecialFolder_t2267_m42259_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisSpecialFolder_t2267_m42259_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisSpecialFolder_t2267_m42259/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisSpecialFolder_t2267_m42259_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisSpecialFolder_t2267_m42259_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.Environment/SpecialFolder>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Environment/SpecialFolder>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SpecialFolder_t2267_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisSpecialFolder_t2267_m42258_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SpecialFolder_t2267_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisSpecialFolder_t2267_m42258_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_SpecialFolderU26_t7515 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisSpecialFolder_t2267_m42258_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_SpecialFolderU26_t7515/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisSpecialFolder_t2267_m42258_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisSpecialFolder_t2267_m42258_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_774.h"
extern TypeInfo InternalEnumerator_1_t5341_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Environment/SpecialFolder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_774MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32153_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Environment/SpecialFolder>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisSpecialFolder_t2267_m42260_MethodInfo;
struct Array_t;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Environment/SpecialFolder>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Environment/SpecialFolder>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSpecialFolder_t2267_m42260 (Array_t * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Environment/SpecialFolder>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisSpecialFolder_t2267_m42260 (Array_t * __this, MethodInfo* method){
	{
		InternalEnumerator_1_t5341  L_0 = {0};
		InternalEnumerator_1__ctor_m32153(&L_0, __this, /*hidden argument*/&InternalEnumerator_1__ctor_m32153_MethodInfo);
		InternalEnumerator_1_t5341  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&InternalEnumerator_1_t5341_il2cpp_TypeInfo), &L_1);
		return (Object_t*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Environment/SpecialFolder>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7516_il2cpp_TypeInfo;
extern Il2CppType IEnumerator_1_t7516_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisSpecialFolder_t2267_m42260_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisSpecialFolder_t2267_m42260_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSpecialFolder_t2267_m42260/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7516_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisSpecialFolder_t2267_m42260_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimization.h"
extern MethodInfo Array_GetGenericValueImpl_TisLoaderOptimization_t2276_m42261_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.LoaderOptimization>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.LoaderOptimization>(System.Int32)
extern MethodInfo Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262_MethodInfo;
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.LoaderOptimization>(System.Int32)
// T System.Array::InternalArray__get_Item<System.LoaderOptimization>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262 (Array_t * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array::InternalArray__get_Item<System.LoaderOptimization>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262 (Array_t * __this, int32_t ___index, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, ___index, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.LoaderOptimization>(System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo LoaderOptimization_t2276_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_LoaderOptimization_t2276_0_0_0;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262_GenericMethod;
extern void* RuntimeInvoker_LoaderOptimization_t2276_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262_MethodInfo = 
{
	"InternalArray__get_Item"/* name */
	, (methodPointerType)&Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &LoaderOptimization_t2276_0_0_0/* return_type */
	, RuntimeInvoker_LoaderOptimization_t2276_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__get_Item_TisLoaderOptimization_t2276_m42262_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.LoaderOptimization>(System.Int32,T&)
// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.LoaderOptimization>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType LoaderOptimization_t2276_1_0_2;
extern Il2CppType LoaderOptimization_t2276_1_0_0;
static ParameterInfo Array_t_Array_GetGenericValueImpl_TisLoaderOptimization_t2276_m42261_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_GetGenericValueImpl_TisLoaderOptimization_t2276_m42261_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_LoaderOptimizationU26_t7517 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_GetGenericValueImpl_TisLoaderOptimization_t2276_m42261_MethodInfo = 
{
	"GetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_LoaderOptimizationU26_t7517/* invoker_method */
	, Array_t_Array_GetGenericValueImpl_TisLoaderOptimization_t2276_m42261_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_GetGenericValueImpl_TisLoaderOptimization_t2276_m42261_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.LoaderOptimization>(T)
extern MethodInfo Array_InternalArray__ICollection_Add_TisLoaderOptimization_t2276_m42263_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.LoaderOptimization>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.LoaderOptimization>(T)
 void Array_InternalArray__ICollection_Add_TisLoaderOptimization_t2276_m42263 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_Add<System.LoaderOptimization>(T)
 void Array_InternalArray__ICollection_Add_TisLoaderOptimization_t2276_m42263 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.LoaderOptimization>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Add_TisLoaderOptimization_t2276_m42263_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Add_TisLoaderOptimization_t2276_m42263_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Add_TisLoaderOptimization_t2276_m42263_MethodInfo = 
{
	"InternalArray__ICollection_Add"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Add_TisLoaderOptimization_t2276_m42263/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Add_TisLoaderOptimization_t2276_m42263_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Add_TisLoaderOptimization_t2276_m42263_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.LoaderOptimization>(T)
extern MethodInfo Array_InternalArray__ICollection_Contains_TisLoaderOptimization_t2276_m42264_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.LoaderOptimization>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.LoaderOptimization>(T)
 bool Array_InternalArray__ICollection_Contains_TisLoaderOptimization_t2276_m42264 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.LoaderOptimization>(T)
 bool Array_InternalArray__ICollection_Contains_TisLoaderOptimization_t2276_m42264 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		Object_t * L_9 = Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &L_8);
		NullCheck(Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &(*(&___item))));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &(*(&___item))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.LoaderOptimization>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Contains_TisLoaderOptimization_t2276_m42264_ParameterInfos[] = 
{
	{"item", 0, 134218874, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Contains_TisLoaderOptimization_t2276_m42264_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Contains_TisLoaderOptimization_t2276_m42264_MethodInfo = 
{
	"InternalArray__ICollection_Contains"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Contains_TisLoaderOptimization_t2276_m42264/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Contains_TisLoaderOptimization_t2276_m42264_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Contains_TisLoaderOptimization_t2276_m42264_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.LoaderOptimization>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisLoaderOptimization_t2276_m42265_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.LoaderOptimization>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.LoaderOptimization>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisLoaderOptimization_t2276_m42265 (Array_t * __this, LoaderOptimizationU5BU5D_t5612* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.LoaderOptimization>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisLoaderOptimization_t2276_m42265 (Array_t * __this, LoaderOptimizationU5BU5D_t5612* ___array, int32_t ___index, MethodInfo* method){
	{
		if (___array)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_3 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_3, L_2, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		NullCheck(___array);
		int32_t L_6 = Array_GetLength_m9760(___array, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		if ((((int32_t)((int32_t)(___index+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t551 * L_7 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_7, (String_t*) &_stringLiteral1279, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		NullCheck(___array);
		int32_t L_8 = Array_get_Rank_m7838(___array, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_10 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_10, L_9, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)___index) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1280, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		ArgumentOutOfRangeException_t1547 * L_12 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7832(L_12, (String_t*) &_stringLiteral474, L_11, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7832_MethodInfo);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		int32_t L_14 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		Array_Copy_m9801(NULL /*static, unused*/, __this, L_13, (Array_t *)(Array_t *)___array, ___index, L_14, /*hidden argument*/&Array_Copy_m9801_MethodInfo);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.LoaderOptimization>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType LoaderOptimizationU5BU5D_t5612_0_0_0;
extern Il2CppType LoaderOptimizationU5BU5D_t5612_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisLoaderOptimization_t2276_m42265_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &LoaderOptimizationU5BU5D_t5612_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisLoaderOptimization_t2276_m42265_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisLoaderOptimization_t2276_m42265_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLoaderOptimization_t2276_m42265/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisLoaderOptimization_t2276_m42265_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisLoaderOptimization_t2276_m42265_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.LoaderOptimization>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisLoaderOptimization_t2276_m42266_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.LoaderOptimization>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.LoaderOptimization>(T)
 bool Array_InternalArray__ICollection_Remove_TisLoaderOptimization_t2276_m42266 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.LoaderOptimization>(T)
 bool Array_InternalArray__ICollection_Remove_TisLoaderOptimization_t2276_m42266 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.LoaderOptimization>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisLoaderOptimization_t2276_m42266_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisLoaderOptimization_t2276_m42266_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisLoaderOptimization_t2276_m42266_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisLoaderOptimization_t2276_m42266/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisLoaderOptimization_t2276_m42266_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisLoaderOptimization_t2276_m42266_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.LoaderOptimization>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisLoaderOptimization_t2276_m42267_MethodInfo;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.LoaderOptimization>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.LoaderOptimization>(T)
 int32_t Array_InternalArray__IndexOf_TisLoaderOptimization_t2276_m42267 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::InternalArray__IndexOf<System.LoaderOptimization>(T)
 int32_t Array_InternalArray__IndexOf_TisLoaderOptimization_t2276_m42267 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = ___item;
		Object_t * L_11 = Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &L_10);
		NullCheck(Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &(*(&V_2))));
		bool L_12 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.LoaderOptimization>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisLoaderOptimization_t2276_m42267_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisLoaderOptimization_t2276_m42267_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisLoaderOptimization_t2276_m42267_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisLoaderOptimization_t2276_m42267/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisLoaderOptimization_t2276_m42267_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisLoaderOptimization_t2276_m42267_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.LoaderOptimization>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisLoaderOptimization_t2276_m42268_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.LoaderOptimization>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.LoaderOptimization>(System.Int32,T)
 void Array_InternalArray__Insert_TisLoaderOptimization_t2276_m42268 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__Insert<System.LoaderOptimization>(System.Int32,T)
 void Array_InternalArray__Insert_TisLoaderOptimization_t2276_m42268 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.LoaderOptimization>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisLoaderOptimization_t2276_m42268_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisLoaderOptimization_t2276_m42268_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisLoaderOptimization_t2276_m42268_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisLoaderOptimization_t2276_m42268/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisLoaderOptimization_t2276_m42268_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisLoaderOptimization_t2276_m42268_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Array_SetGenericValueImpl_TisLoaderOptimization_t2276_m42269_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.LoaderOptimization>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.LoaderOptimization>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisLoaderOptimization_t2276_m42270_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.LoaderOptimization>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.LoaderOptimization>(System.Int32,T)
 void Array_InternalArray__set_Item_TisLoaderOptimization_t2276_m42270 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__set_Item<System.LoaderOptimization>(System.Int32,T)
 void Array_InternalArray__set_Item_TisLoaderOptimization_t2276_m42270 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	ObjectU5BU5D_t130* V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((ObjectU5BU5D_t130*)IsInst(__this, InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = ___item;
		Object_t * L_3 = Box(InitializedTypeInfo(&LoaderOptimization_t2276_il2cpp_TypeInfo), &L_2);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ___index);
		ArrayElementTypeCheck (V_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(V_0, ___index)) = (Object_t *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, ___index, (&___item));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.LoaderOptimization>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType LoaderOptimization_t2276_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisLoaderOptimization_t2276_m42270_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisLoaderOptimization_t2276_m42270_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisLoaderOptimization_t2276_m42270_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisLoaderOptimization_t2276_m42270/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisLoaderOptimization_t2276_m42270_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisLoaderOptimization_t2276_m42270_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.LoaderOptimization>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.LoaderOptimization>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType LoaderOptimization_t2276_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisLoaderOptimization_t2276_m42269_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &LoaderOptimization_t2276_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisLoaderOptimization_t2276_m42269_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_LoaderOptimizationU26_t7517 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisLoaderOptimization_t2276_m42269_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_LoaderOptimizationU26_t7517/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisLoaderOptimization_t2276_m42269_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisLoaderOptimization_t2276_m42269_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.LoaderOptimization>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_775.h"
extern TypeInfo InternalEnumerator_1_t5346_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.LoaderOptimization>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_775MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32175_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.LoaderOptimization>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisLoaderOptimization_t2276_m42271_MethodInfo;
struct Array_t;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.LoaderOptimization>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.LoaderOptimization>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLoaderOptimization_t2276_m42271 (Array_t * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.LoaderOptimization>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLoaderOptimization_t2276_m42271 (Array_t * __this, MethodInfo* method){
	{
		InternalEnumerator_1_t5346  L_0 = {0};
		InternalEnumerator_1__ctor_m32175(&L_0, __this, /*hidden argument*/&InternalEnumerator_1__ctor_m32175_MethodInfo);
		InternalEnumerator_1_t5346  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&InternalEnumerator_1_t5346_il2cpp_TypeInfo), &L_1);
		return (Object_t*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.LoaderOptimization>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7518_il2cpp_TypeInfo;
extern Il2CppType IEnumerator_1_t7518_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisLoaderOptimization_t2276_m42271_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisLoaderOptimization_t2276_m42271_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLoaderOptimization_t2276_m42271/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7518_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisLoaderOptimization_t2276_m42271_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttribute.h"
extern MethodInfo Array_GetGenericValueImpl_TisNonSerializedAttribute_t2290_m42272_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.Object>(System.Int32,!!0&)
// Declaration System.Void System.Array::GetGenericValueImpl<System.NonSerializedAttribute>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.NonSerializedAttribute>(System.Int32)
extern MethodInfo Array_InternalArray__get_Item_TisNonSerializedAttribute_t2290_m42273_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t ___index, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, ___index, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)___index, method)
// Declaration T System.Array::InternalArray__get_Item<System.NonSerializedAttribute>(System.Int32)
// T System.Array::InternalArray__get_Item<System.NonSerializedAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisNonSerializedAttribute_t2290_m42273(__this, ___index, method) (NonSerializedAttribute_t2290 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)___index, method)
// T System.Array::InternalArray__get_Item<System.NonSerializedAttribute>(System.Int32)
// Metadata Definition T System.Array::InternalArray__get_Item<System.NonSerializedAttribute>(System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo NonSerializedAttribute_t2290_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__get_Item_TisNonSerializedAttribute_t2290_m42273_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_NonSerializedAttribute_t2290_0_0_0;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__get_Item_TisNonSerializedAttribute_t2290_m42273_GenericMethod;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__get_Item_TisNonSerializedAttribute_t2290_m42273_MethodInfo = 
{
	"InternalArray__get_Item"/* name */
	, (methodPointerType)&Array_InternalArray__get_Item_TisObject_t_m32233_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &NonSerializedAttribute_t2290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__get_Item_TisNonSerializedAttribute_t2290_m42273_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__get_Item_TisNonSerializedAttribute_t2290_m42273_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.NonSerializedAttribute>(System.Int32,T&)
// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.NonSerializedAttribute>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType NonSerializedAttribute_t2290_1_0_2;
extern Il2CppType NonSerializedAttribute_t2290_1_0_0;
static ParameterInfo Array_t_Array_GetGenericValueImpl_TisNonSerializedAttribute_t2290_m42272_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_GetGenericValueImpl_TisNonSerializedAttribute_t2290_m42272_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_NonSerializedAttributeU26_t7519 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_GetGenericValueImpl_TisNonSerializedAttribute_t2290_m42272_MethodInfo = 
{
	"GetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_NonSerializedAttributeU26_t7519/* invoker_method */
	, Array_t_Array_GetGenericValueImpl_TisNonSerializedAttribute_t2290_m42272_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_GetGenericValueImpl_TisNonSerializedAttribute_t2290_m42272_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.NonSerializedAttribute>(T)
extern MethodInfo Array_InternalArray__ICollection_Add_TisNonSerializedAttribute_t2290_m42274_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.Object>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Object>(T)
 void Array_InternalArray__ICollection_Add_TisObject_t_m32237_gshared (Array_t * __this, Object_t * ___item, MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisObject_t_m32237(__this, ___item, method) (void)Array_InternalArray__ICollection_Add_TisObject_t_m32237_gshared((Array_t *)__this, (Object_t *)___item, method)
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.NonSerializedAttribute>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.NonSerializedAttribute>(T)
#define Array_InternalArray__ICollection_Add_TisNonSerializedAttribute_t2290_m42274(__this, ___item, method) (void)Array_InternalArray__ICollection_Add_TisObject_t_m32237_gshared((Array_t *)__this, (Object_t *)___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.NonSerializedAttribute>(T)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.NonSerializedAttribute>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Add_TisNonSerializedAttribute_t2290_m42274_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Add_TisNonSerializedAttribute_t2290_m42274_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Add_TisNonSerializedAttribute_t2290_m42274_MethodInfo = 
{
	"InternalArray__ICollection_Add"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Add_TisObject_t_m32237_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Add_TisNonSerializedAttribute_t2290_m42274_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Add_TisNonSerializedAttribute_t2290_m42274_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.NonSerializedAttribute>(T)
extern MethodInfo Array_InternalArray__ICollection_Contains_TisNonSerializedAttribute_t2290_m42275_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.Object>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Object>(T)
 bool Array_InternalArray__ICollection_Contains_TisObject_t_m32239_gshared (Array_t * __this, Object_t * ___item, MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisObject_t_m32239(__this, ___item, method) (bool)Array_InternalArray__ICollection_Contains_TisObject_t_m32239_gshared((Array_t *)__this, (Object_t *)___item, method)
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.NonSerializedAttribute>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.NonSerializedAttribute>(T)
#define Array_InternalArray__ICollection_Contains_TisNonSerializedAttribute_t2290_m42275(__this, ___item, method) (bool)Array_InternalArray__ICollection_Contains_TisObject_t_m32239_gshared((Array_t *)__this, (Object_t *)___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.NonSerializedAttribute>(T)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.NonSerializedAttribute>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Contains_TisNonSerializedAttribute_t2290_m42275_ParameterInfos[] = 
{
	{"item", 0, 134218874, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern TypeInfo NonSerializedAttribute_t2290_il2cpp_TypeInfo;
static Il2CppRGCTXData Array_InternalArray__ICollection_Contains_TisNonSerializedAttribute_t2290_m42275_RGCTXData[1] = 
{
	&NonSerializedAttribute_t2290_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Contains_TisNonSerializedAttribute_t2290_m42275_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Contains_TisNonSerializedAttribute_t2290_m42275_MethodInfo = 
{
	"InternalArray__ICollection_Contains"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Contains_TisObject_t_m32239_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Contains_TisNonSerializedAttribute_t2290_m42275_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, Array_InternalArray__ICollection_Contains_TisNonSerializedAttribute_t2290_m42275_RGCTXData/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Contains_TisNonSerializedAttribute_t2290_m42275_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.NonSerializedAttribute>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisNonSerializedAttribute_t2290_m42276_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.Object>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Object>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisObject_t_m32241_gshared (Array_t * __this, ObjectU5BU5D_t130* ___array, int32_t ___index, MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisObject_t_m32241(__this, ___array, ___index, method) (void)Array_InternalArray__ICollection_CopyTo_TisObject_t_m32241_gshared((Array_t *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.NonSerializedAttribute>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.NonSerializedAttribute>(T[],System.Int32)
#define Array_InternalArray__ICollection_CopyTo_TisNonSerializedAttribute_t2290_m42276(__this, ___array, ___index, method) (void)Array_InternalArray__ICollection_CopyTo_TisObject_t_m32241_gshared((Array_t *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.NonSerializedAttribute>(T[],System.Int32)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.NonSerializedAttribute>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType NonSerializedAttributeU5BU5D_t5613_0_0_0;
extern Il2CppType NonSerializedAttributeU5BU5D_t5613_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisNonSerializedAttribute_t2290_m42276_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &NonSerializedAttributeU5BU5D_t5613_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisNonSerializedAttribute_t2290_m42276_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisNonSerializedAttribute_t2290_m42276_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisObject_t_m32241_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisNonSerializedAttribute_t2290_m42276_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisNonSerializedAttribute_t2290_m42276_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.NonSerializedAttribute>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisNonSerializedAttribute_t2290_m42277_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.Object>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Object>(T)
 bool Array_InternalArray__ICollection_Remove_TisObject_t_m32242_gshared (Array_t * __this, Object_t * ___item, MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisObject_t_m32242(__this, ___item, method) (bool)Array_InternalArray__ICollection_Remove_TisObject_t_m32242_gshared((Array_t *)__this, (Object_t *)___item, method)
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.NonSerializedAttribute>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.NonSerializedAttribute>(T)
#define Array_InternalArray__ICollection_Remove_TisNonSerializedAttribute_t2290_m42277(__this, ___item, method) (bool)Array_InternalArray__ICollection_Remove_TisObject_t_m32242_gshared((Array_t *)__this, (Object_t *)___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.NonSerializedAttribute>(T)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.NonSerializedAttribute>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisNonSerializedAttribute_t2290_m42277_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisNonSerializedAttribute_t2290_m42277_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisNonSerializedAttribute_t2290_m42277_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisObject_t_m32242_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisNonSerializedAttribute_t2290_m42277_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisNonSerializedAttribute_t2290_m42277_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.NonSerializedAttribute>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisNonSerializedAttribute_t2290_m42278_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.Object>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Object>(T)
 int32_t Array_InternalArray__IndexOf_TisObject_t_m32243_gshared (Array_t * __this, Object_t * ___item, MethodInfo* method);
#define Array_InternalArray__IndexOf_TisObject_t_m32243(__this, ___item, method) (int32_t)Array_InternalArray__IndexOf_TisObject_t_m32243_gshared((Array_t *)__this, (Object_t *)___item, method)
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.NonSerializedAttribute>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.NonSerializedAttribute>(T)
#define Array_InternalArray__IndexOf_TisNonSerializedAttribute_t2290_m42278(__this, ___item, method) (int32_t)Array_InternalArray__IndexOf_TisObject_t_m32243_gshared((Array_t *)__this, (Object_t *)___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.NonSerializedAttribute>(T)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.NonSerializedAttribute>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisNonSerializedAttribute_t2290_m42278_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern TypeInfo NonSerializedAttribute_t2290_il2cpp_TypeInfo;
static Il2CppRGCTXData Array_InternalArray__IndexOf_TisNonSerializedAttribute_t2290_m42278_RGCTXData[1] = 
{
	&NonSerializedAttribute_t2290_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisNonSerializedAttribute_t2290_m42278_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisNonSerializedAttribute_t2290_m42278_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisObject_t_m32243_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisNonSerializedAttribute_t2290_m42278_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, Array_InternalArray__IndexOf_TisNonSerializedAttribute_t2290_m42278_RGCTXData/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisNonSerializedAttribute_t2290_m42278_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.NonSerializedAttribute>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisNonSerializedAttribute_t2290_m42279_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.Object>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Object>(System.Int32,T)
 void Array_InternalArray__Insert_TisObject_t_m32244_gshared (Array_t * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define Array_InternalArray__Insert_TisObject_t_m32244(__this, ___index, ___item, method) (void)Array_InternalArray__Insert_TisObject_t_m32244_gshared((Array_t *)__this, (int32_t)___index, (Object_t *)___item, method)
// Declaration System.Void System.Array::InternalArray__Insert<System.NonSerializedAttribute>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.NonSerializedAttribute>(System.Int32,T)
#define Array_InternalArray__Insert_TisNonSerializedAttribute_t2290_m42279(__this, ___index, ___item, method) (void)Array_InternalArray__Insert_TisObject_t_m32244_gshared((Array_t *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Array::InternalArray__Insert<System.NonSerializedAttribute>(System.Int32,T)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.NonSerializedAttribute>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisNonSerializedAttribute_t2290_m42279_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisNonSerializedAttribute_t2290_m42279_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisNonSerializedAttribute_t2290_m42279_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisObject_t_m32244_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisNonSerializedAttribute_t2290_m42279_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisNonSerializedAttribute_t2290_m42279_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Array_SetGenericValueImpl_TisNonSerializedAttribute_t2290_m42280_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.Object>(System.Int32,!!0&)
// Declaration System.Void System.Array::SetGenericValueImpl<System.NonSerializedAttribute>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.NonSerializedAttribute>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisNonSerializedAttribute_t2290_m42281_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.Object>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Object>(System.Int32,T)
 void Array_InternalArray__set_Item_TisObject_t_m32246_gshared (Array_t * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define Array_InternalArray__set_Item_TisObject_t_m32246(__this, ___index, ___item, method) (void)Array_InternalArray__set_Item_TisObject_t_m32246_gshared((Array_t *)__this, (int32_t)___index, (Object_t *)___item, method)
// Declaration System.Void System.Array::InternalArray__set_Item<System.NonSerializedAttribute>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.NonSerializedAttribute>(System.Int32,T)
#define Array_InternalArray__set_Item_TisNonSerializedAttribute_t2290_m42281(__this, ___index, ___item, method) (void)Array_InternalArray__set_Item_TisObject_t_m32246_gshared((Array_t *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Array::InternalArray__set_Item<System.NonSerializedAttribute>(System.Int32,T)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.NonSerializedAttribute>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType NonSerializedAttribute_t2290_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisNonSerializedAttribute_t2290_m42281_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_0_0_0},
};
extern TypeInfo NonSerializedAttribute_t2290_il2cpp_TypeInfo;
static Il2CppRGCTXData Array_InternalArray__set_Item_TisNonSerializedAttribute_t2290_m42281_RGCTXData[1] = 
{
	&NonSerializedAttribute_t2290_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisNonSerializedAttribute_t2290_m42281_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisNonSerializedAttribute_t2290_m42281_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisObject_t_m32246_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisNonSerializedAttribute_t2290_m42281_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, Array_InternalArray__set_Item_TisNonSerializedAttribute_t2290_m42281_RGCTXData/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisNonSerializedAttribute_t2290_m42281_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.NonSerializedAttribute>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.NonSerializedAttribute>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType NonSerializedAttribute_t2290_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisNonSerializedAttribute_t2290_m42280_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &NonSerializedAttribute_t2290_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisNonSerializedAttribute_t2290_m42280_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_NonSerializedAttributeU26_t7519 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisNonSerializedAttribute_t2290_m42280_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_NonSerializedAttributeU26_t7519/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisNonSerializedAttribute_t2290_m42280_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisNonSerializedAttribute_t2290_m42280_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.NonSerializedAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_776.h"
extern TypeInfo InternalEnumerator_1_t5347_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.NonSerializedAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_776MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32180_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.NonSerializedAttribute>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisNonSerializedAttribute_t2290_m42282_MethodInfo;
struct Array_t;
struct Array_t;
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0.h"
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Object>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Object>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m32247_gshared (Array_t * __this, MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m32247(__this, method) (Object_t*)Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m32247_gshared((Array_t *)__this, method)
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.NonSerializedAttribute>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.NonSerializedAttribute>()
#define Array_InternalArray__IEnumerable_GetEnumerator_TisNonSerializedAttribute_t2290_m42282(__this, method) (Object_t*)Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m32247_gshared((Array_t *)__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.NonSerializedAttribute>()
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.NonSerializedAttribute>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7520_il2cpp_TypeInfo;
extern TypeInfo InternalEnumerator_1_t5347_il2cpp_TypeInfo;
static Il2CppRGCTXData Array_InternalArray__IEnumerable_GetEnumerator_TisNonSerializedAttribute_t2290_m42282_RGCTXData[2] = 
{
	&InternalEnumerator_1_t5347_il2cpp_TypeInfo/* Class Usage */,
	&InternalEnumerator_1__ctor_m32180_MethodInfo/* Method Usage */,
};
extern Il2CppType IEnumerator_1_t7520_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisNonSerializedAttribute_t2290_m42282_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisNonSerializedAttribute_t2290_m42282_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m32247_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7520_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, Array_InternalArray__IEnumerable_GetEnumerator_TisNonSerializedAttribute_t2290_m42282_RGCTXData/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisNonSerializedAttribute_t2290_m42282_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.PlatformID
#include "mscorlib_System_PlatformID.h"
extern MethodInfo Array_GetGenericValueImpl_TisPlatformID_t2295_m42283_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.PlatformID>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.PlatformID>(System.Int32)
extern MethodInfo Array_InternalArray__get_Item_TisPlatformID_t2295_m42284_MethodInfo;
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.PlatformID>(System.Int32)
// T System.Array::InternalArray__get_Item<System.PlatformID>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisPlatformID_t2295_m42284 (Array_t * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array::InternalArray__get_Item<System.PlatformID>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisPlatformID_t2295_m42284 (Array_t * __this, int32_t ___index, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, ___index, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.PlatformID>(System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo PlatformID_t2295_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__get_Item_TisPlatformID_t2295_m42284_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_PlatformID_t2295_0_0_0;
extern Il2CppType PlatformID_t2295_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__get_Item_TisPlatformID_t2295_m42284_GenericMethod;
extern void* RuntimeInvoker_PlatformID_t2295_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__get_Item_TisPlatformID_t2295_m42284_MethodInfo = 
{
	"InternalArray__get_Item"/* name */
	, (methodPointerType)&Array_InternalArray__get_Item_TisPlatformID_t2295_m42284/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &PlatformID_t2295_0_0_0/* return_type */
	, RuntimeInvoker_PlatformID_t2295_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__get_Item_TisPlatformID_t2295_m42284_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__get_Item_TisPlatformID_t2295_m42284_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.PlatformID>(System.Int32,T&)
// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.PlatformID>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PlatformID_t2295_1_0_2;
extern Il2CppType PlatformID_t2295_1_0_0;
static ParameterInfo Array_t_Array_GetGenericValueImpl_TisPlatformID_t2295_m42283_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PlatformID_t2295_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_GetGenericValueImpl_TisPlatformID_t2295_m42283_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_PlatformIDU26_t7521 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_GetGenericValueImpl_TisPlatformID_t2295_m42283_MethodInfo = 
{
	"GetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_PlatformIDU26_t7521/* invoker_method */
	, Array_t_Array_GetGenericValueImpl_TisPlatformID_t2295_m42283_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_GetGenericValueImpl_TisPlatformID_t2295_m42283_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.PlatformID>(T)
extern MethodInfo Array_InternalArray__ICollection_Add_TisPlatformID_t2295_m42285_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.PlatformID>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.PlatformID>(T)
 void Array_InternalArray__ICollection_Add_TisPlatformID_t2295_m42285 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_Add<System.PlatformID>(T)
 void Array_InternalArray__ICollection_Add_TisPlatformID_t2295_m42285 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.PlatformID>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType PlatformID_t2295_0_0_0;
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Add_TisPlatformID_t2295_m42285_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Add_TisPlatformID_t2295_m42285_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Add_TisPlatformID_t2295_m42285_MethodInfo = 
{
	"InternalArray__ICollection_Add"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Add_TisPlatformID_t2295_m42285/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Add_TisPlatformID_t2295_m42285_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Add_TisPlatformID_t2295_m42285_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.PlatformID>(T)
extern MethodInfo Array_InternalArray__ICollection_Contains_TisPlatformID_t2295_m42286_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.PlatformID>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.PlatformID>(T)
 bool Array_InternalArray__ICollection_Contains_TisPlatformID_t2295_m42286 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.PlatformID>(T)
 bool Array_InternalArray__ICollection_Contains_TisPlatformID_t2295_m42286 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		Object_t * L_9 = Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &L_8);
		NullCheck(Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &(*(&___item))));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &(*(&___item))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.PlatformID>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Contains_TisPlatformID_t2295_m42286_ParameterInfos[] = 
{
	{"item", 0, 134218874, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Contains_TisPlatformID_t2295_m42286_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Contains_TisPlatformID_t2295_m42286_MethodInfo = 
{
	"InternalArray__ICollection_Contains"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Contains_TisPlatformID_t2295_m42286/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Contains_TisPlatformID_t2295_m42286_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Contains_TisPlatformID_t2295_m42286_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.PlatformID>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisPlatformID_t2295_m42287_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.PlatformID>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.PlatformID>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisPlatformID_t2295_m42287 (Array_t * __this, PlatformIDU5BU5D_t5614* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.PlatformID>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisPlatformID_t2295_m42287 (Array_t * __this, PlatformIDU5BU5D_t5614* ___array, int32_t ___index, MethodInfo* method){
	{
		if (___array)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_3 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_3, L_2, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		NullCheck(___array);
		int32_t L_6 = Array_GetLength_m9760(___array, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		if ((((int32_t)((int32_t)(___index+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t551 * L_7 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_7, (String_t*) &_stringLiteral1279, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		NullCheck(___array);
		int32_t L_8 = Array_get_Rank_m7838(___array, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_10 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_10, L_9, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)___index) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1280, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		ArgumentOutOfRangeException_t1547 * L_12 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7832(L_12, (String_t*) &_stringLiteral474, L_11, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7832_MethodInfo);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		int32_t L_14 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		Array_Copy_m9801(NULL /*static, unused*/, __this, L_13, (Array_t *)(Array_t *)___array, ___index, L_14, /*hidden argument*/&Array_Copy_m9801_MethodInfo);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.PlatformID>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType PlatformIDU5BU5D_t5614_0_0_0;
extern Il2CppType PlatformIDU5BU5D_t5614_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisPlatformID_t2295_m42287_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &PlatformIDU5BU5D_t5614_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisPlatformID_t2295_m42287_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisPlatformID_t2295_m42287_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisPlatformID_t2295_m42287/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisPlatformID_t2295_m42287_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisPlatformID_t2295_m42287_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.PlatformID>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisPlatformID_t2295_m42288_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.PlatformID>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.PlatformID>(T)
 bool Array_InternalArray__ICollection_Remove_TisPlatformID_t2295_m42288 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.PlatformID>(T)
 bool Array_InternalArray__ICollection_Remove_TisPlatformID_t2295_m42288 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.PlatformID>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisPlatformID_t2295_m42288_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisPlatformID_t2295_m42288_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisPlatformID_t2295_m42288_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisPlatformID_t2295_m42288/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisPlatformID_t2295_m42288_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisPlatformID_t2295_m42288_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.PlatformID>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisPlatformID_t2295_m42289_MethodInfo;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.PlatformID>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.PlatformID>(T)
 int32_t Array_InternalArray__IndexOf_TisPlatformID_t2295_m42289 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::InternalArray__IndexOf<System.PlatformID>(T)
 int32_t Array_InternalArray__IndexOf_TisPlatformID_t2295_m42289 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = ___item;
		Object_t * L_11 = Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &L_10);
		NullCheck(Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &(*(&V_2))));
		bool L_12 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.PlatformID>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisPlatformID_t2295_m42289_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisPlatformID_t2295_m42289_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisPlatformID_t2295_m42289_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisPlatformID_t2295_m42289/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisPlatformID_t2295_m42289_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisPlatformID_t2295_m42289_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.PlatformID>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisPlatformID_t2295_m42290_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.PlatformID>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.PlatformID>(System.Int32,T)
 void Array_InternalArray__Insert_TisPlatformID_t2295_m42290 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__Insert<System.PlatformID>(System.Int32,T)
 void Array_InternalArray__Insert_TisPlatformID_t2295_m42290 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.PlatformID>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisPlatformID_t2295_m42290_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisPlatformID_t2295_m42290_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisPlatformID_t2295_m42290_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisPlatformID_t2295_m42290/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisPlatformID_t2295_m42290_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisPlatformID_t2295_m42290_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Array_SetGenericValueImpl_TisPlatformID_t2295_m42291_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.PlatformID>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.PlatformID>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisPlatformID_t2295_m42292_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.PlatformID>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.PlatformID>(System.Int32,T)
 void Array_InternalArray__set_Item_TisPlatformID_t2295_m42292 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__set_Item<System.PlatformID>(System.Int32,T)
 void Array_InternalArray__set_Item_TisPlatformID_t2295_m42292 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	ObjectU5BU5D_t130* V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((ObjectU5BU5D_t130*)IsInst(__this, InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = ___item;
		Object_t * L_3 = Box(InitializedTypeInfo(&PlatformID_t2295_il2cpp_TypeInfo), &L_2);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ___index);
		ArrayElementTypeCheck (V_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(V_0, ___index)) = (Object_t *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, ___index, (&___item));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.PlatformID>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PlatformID_t2295_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisPlatformID_t2295_m42292_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &PlatformID_t2295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisPlatformID_t2295_m42292_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisPlatformID_t2295_m42292_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisPlatformID_t2295_m42292/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisPlatformID_t2295_m42292_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisPlatformID_t2295_m42292_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.PlatformID>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.PlatformID>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType PlatformID_t2295_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisPlatformID_t2295_m42291_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PlatformID_t2295_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisPlatformID_t2295_m42291_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_PlatformIDU26_t7521 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisPlatformID_t2295_m42291_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_PlatformIDU26_t7521/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisPlatformID_t2295_m42291_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisPlatformID_t2295_m42291_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.PlatformID>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_777.h"
extern TypeInfo InternalEnumerator_1_t5348_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.PlatformID>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_777MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32185_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.PlatformID>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisPlatformID_t2295_m42293_MethodInfo;
struct Array_t;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.PlatformID>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.PlatformID>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisPlatformID_t2295_m42293 (Array_t * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.PlatformID>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisPlatformID_t2295_m42293 (Array_t * __this, MethodInfo* method){
	{
		InternalEnumerator_1_t5348  L_0 = {0};
		InternalEnumerator_1__ctor_m32185(&L_0, __this, /*hidden argument*/&InternalEnumerator_1__ctor_m32185_MethodInfo);
		InternalEnumerator_1_t5348  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&InternalEnumerator_1_t5348_il2cpp_TypeInfo), &L_1);
		return (Object_t*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.PlatformID>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7522_il2cpp_TypeInfo;
extern Il2CppType IEnumerator_1_t7522_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisPlatformID_t2295_m42293_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisPlatformID_t2295_m42293_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisPlatformID_t2295_m42293/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7522_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisPlatformID_t2295_m42293_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.StringComparison
#include "mscorlib_System_StringComparison.h"
extern MethodInfo Array_GetGenericValueImpl_TisStringComparison_t2300_m42294_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.StringComparison>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.StringComparison>(System.Int32)
extern MethodInfo Array_InternalArray__get_Item_TisStringComparison_t2300_m42295_MethodInfo;
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.StringComparison>(System.Int32)
// T System.Array::InternalArray__get_Item<System.StringComparison>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisStringComparison_t2300_m42295 (Array_t * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array::InternalArray__get_Item<System.StringComparison>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisStringComparison_t2300_m42295 (Array_t * __this, int32_t ___index, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, ___index, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.StringComparison>(System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo StringComparison_t2300_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__get_Item_TisStringComparison_t2300_m42295_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_StringComparison_t2300_0_0_0;
extern Il2CppType StringComparison_t2300_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__get_Item_TisStringComparison_t2300_m42295_GenericMethod;
extern void* RuntimeInvoker_StringComparison_t2300_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__get_Item_TisStringComparison_t2300_m42295_MethodInfo = 
{
	"InternalArray__get_Item"/* name */
	, (methodPointerType)&Array_InternalArray__get_Item_TisStringComparison_t2300_m42295/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &StringComparison_t2300_0_0_0/* return_type */
	, RuntimeInvoker_StringComparison_t2300_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__get_Item_TisStringComparison_t2300_m42295_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__get_Item_TisStringComparison_t2300_m42295_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.StringComparison>(System.Int32,T&)
// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.StringComparison>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringComparison_t2300_1_0_2;
extern Il2CppType StringComparison_t2300_1_0_0;
static ParameterInfo Array_t_Array_GetGenericValueImpl_TisStringComparison_t2300_m42294_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &StringComparison_t2300_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_GetGenericValueImpl_TisStringComparison_t2300_m42294_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_StringComparisonU26_t7523 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_GetGenericValueImpl_TisStringComparison_t2300_m42294_MethodInfo = 
{
	"GetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_StringComparisonU26_t7523/* invoker_method */
	, Array_t_Array_GetGenericValueImpl_TisStringComparison_t2300_m42294_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_GetGenericValueImpl_TisStringComparison_t2300_m42294_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.StringComparison>(T)
extern MethodInfo Array_InternalArray__ICollection_Add_TisStringComparison_t2300_m42296_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.StringComparison>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.StringComparison>(T)
 void Array_InternalArray__ICollection_Add_TisStringComparison_t2300_m42296 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_Add<System.StringComparison>(T)
 void Array_InternalArray__ICollection_Add_TisStringComparison_t2300_m42296 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.StringComparison>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType StringComparison_t2300_0_0_0;
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Add_TisStringComparison_t2300_m42296_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Add_TisStringComparison_t2300_m42296_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Add_TisStringComparison_t2300_m42296_MethodInfo = 
{
	"InternalArray__ICollection_Add"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Add_TisStringComparison_t2300_m42296/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Add_TisStringComparison_t2300_m42296_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Add_TisStringComparison_t2300_m42296_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringComparison>(T)
extern MethodInfo Array_InternalArray__ICollection_Contains_TisStringComparison_t2300_m42297_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringComparison>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringComparison>(T)
 bool Array_InternalArray__ICollection_Contains_TisStringComparison_t2300_m42297 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringComparison>(T)
 bool Array_InternalArray__ICollection_Contains_TisStringComparison_t2300_m42297 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		Object_t * L_9 = Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &L_8);
		NullCheck(Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &(*(&___item))));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &(*(&___item))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringComparison>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Contains_TisStringComparison_t2300_m42297_ParameterInfos[] = 
{
	{"item", 0, 134218874, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Contains_TisStringComparison_t2300_m42297_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Contains_TisStringComparison_t2300_m42297_MethodInfo = 
{
	"InternalArray__ICollection_Contains"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Contains_TisStringComparison_t2300_m42297/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Contains_TisStringComparison_t2300_m42297_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Contains_TisStringComparison_t2300_m42297_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringComparison>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisStringComparison_t2300_m42298_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringComparison>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringComparison>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisStringComparison_t2300_m42298 (Array_t * __this, StringComparisonU5BU5D_t5615* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringComparison>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisStringComparison_t2300_m42298 (Array_t * __this, StringComparisonU5BU5D_t5615* ___array, int32_t ___index, MethodInfo* method){
	{
		if (___array)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_3 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_3, L_2, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		NullCheck(___array);
		int32_t L_6 = Array_GetLength_m9760(___array, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		if ((((int32_t)((int32_t)(___index+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t551 * L_7 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_7, (String_t*) &_stringLiteral1279, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		NullCheck(___array);
		int32_t L_8 = Array_get_Rank_m7838(___array, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_10 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_10, L_9, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)___index) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1280, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		ArgumentOutOfRangeException_t1547 * L_12 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7832(L_12, (String_t*) &_stringLiteral474, L_11, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7832_MethodInfo);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		int32_t L_14 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		Array_Copy_m9801(NULL /*static, unused*/, __this, L_13, (Array_t *)(Array_t *)___array, ___index, L_14, /*hidden argument*/&Array_Copy_m9801_MethodInfo);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringComparison>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType StringComparisonU5BU5D_t5615_0_0_0;
extern Il2CppType StringComparisonU5BU5D_t5615_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisStringComparison_t2300_m42298_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &StringComparisonU5BU5D_t5615_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisStringComparison_t2300_m42298_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisStringComparison_t2300_m42298_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisStringComparison_t2300_m42298/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisStringComparison_t2300_m42298_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisStringComparison_t2300_m42298_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringComparison>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisStringComparison_t2300_m42299_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringComparison>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringComparison>(T)
 bool Array_InternalArray__ICollection_Remove_TisStringComparison_t2300_m42299 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringComparison>(T)
 bool Array_InternalArray__ICollection_Remove_TisStringComparison_t2300_m42299 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringComparison>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisStringComparison_t2300_m42299_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisStringComparison_t2300_m42299_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisStringComparison_t2300_m42299_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisStringComparison_t2300_m42299/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisStringComparison_t2300_m42299_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisStringComparison_t2300_m42299_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.StringComparison>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisStringComparison_t2300_m42300_MethodInfo;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.StringComparison>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.StringComparison>(T)
 int32_t Array_InternalArray__IndexOf_TisStringComparison_t2300_m42300 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::InternalArray__IndexOf<System.StringComparison>(T)
 int32_t Array_InternalArray__IndexOf_TisStringComparison_t2300_m42300 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = ___item;
		Object_t * L_11 = Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &L_10);
		NullCheck(Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &(*(&V_2))));
		bool L_12 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.StringComparison>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisStringComparison_t2300_m42300_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisStringComparison_t2300_m42300_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisStringComparison_t2300_m42300_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisStringComparison_t2300_m42300/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisStringComparison_t2300_m42300_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisStringComparison_t2300_m42300_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.StringComparison>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisStringComparison_t2300_m42301_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.StringComparison>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.StringComparison>(System.Int32,T)
 void Array_InternalArray__Insert_TisStringComparison_t2300_m42301 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__Insert<System.StringComparison>(System.Int32,T)
 void Array_InternalArray__Insert_TisStringComparison_t2300_m42301 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.StringComparison>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisStringComparison_t2300_m42301_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisStringComparison_t2300_m42301_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisStringComparison_t2300_m42301_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisStringComparison_t2300_m42301/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisStringComparison_t2300_m42301_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisStringComparison_t2300_m42301_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Array_SetGenericValueImpl_TisStringComparison_t2300_m42302_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.StringComparison>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.StringComparison>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisStringComparison_t2300_m42303_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.StringComparison>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.StringComparison>(System.Int32,T)
 void Array_InternalArray__set_Item_TisStringComparison_t2300_m42303 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__set_Item<System.StringComparison>(System.Int32,T)
 void Array_InternalArray__set_Item_TisStringComparison_t2300_m42303 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	ObjectU5BU5D_t130* V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((ObjectU5BU5D_t130*)IsInst(__this, InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = ___item;
		Object_t * L_3 = Box(InitializedTypeInfo(&StringComparison_t2300_il2cpp_TypeInfo), &L_2);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ___index);
		ArrayElementTypeCheck (V_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(V_0, ___index)) = (Object_t *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, ___index, (&___item));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.StringComparison>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringComparison_t2300_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisStringComparison_t2300_m42303_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &StringComparison_t2300_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisStringComparison_t2300_m42303_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisStringComparison_t2300_m42303_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisStringComparison_t2300_m42303/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisStringComparison_t2300_m42303_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisStringComparison_t2300_m42303_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.StringComparison>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.StringComparison>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringComparison_t2300_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisStringComparison_t2300_m42302_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &StringComparison_t2300_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisStringComparison_t2300_m42302_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_StringComparisonU26_t7523 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisStringComparison_t2300_m42302_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_StringComparisonU26_t7523/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisStringComparison_t2300_m42302_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisStringComparison_t2300_m42302_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.StringComparison>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_778.h"
extern TypeInfo InternalEnumerator_1_t5349_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.StringComparison>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_778MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32190_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringComparison>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisStringComparison_t2300_m42304_MethodInfo;
struct Array_t;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringComparison>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringComparison>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisStringComparison_t2300_m42304 (Array_t * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringComparison>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisStringComparison_t2300_m42304 (Array_t * __this, MethodInfo* method){
	{
		InternalEnumerator_1_t5349  L_0 = {0};
		InternalEnumerator_1__ctor_m32190(&L_0, __this, /*hidden argument*/&InternalEnumerator_1__ctor_m32190_MethodInfo);
		InternalEnumerator_1_t5349  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&InternalEnumerator_1_t5349_il2cpp_TypeInfo), &L_1);
		return (Object_t*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringComparison>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7524_il2cpp_TypeInfo;
extern Il2CppType IEnumerator_1_t7524_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisStringComparison_t2300_m42304_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisStringComparison_t2300_m42304_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisStringComparison_t2300_m42304/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7524_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisStringComparison_t2300_m42304_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptions.h"
extern MethodInfo Array_GetGenericValueImpl_TisStringSplitOptions_t2301_m42305_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.StringSplitOptions>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.StringSplitOptions>(System.Int32)
extern MethodInfo Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306_MethodInfo;
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.StringSplitOptions>(System.Int32)
// T System.Array::InternalArray__get_Item<System.StringSplitOptions>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306 (Array_t * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array::InternalArray__get_Item<System.StringSplitOptions>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306 (Array_t * __this, int32_t ___index, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, ___index, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.StringSplitOptions>(System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo StringSplitOptions_t2301_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_StringSplitOptions_t2301_0_0_0;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306_GenericMethod;
extern void* RuntimeInvoker_StringSplitOptions_t2301_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306_MethodInfo = 
{
	"InternalArray__get_Item"/* name */
	, (methodPointerType)&Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &StringSplitOptions_t2301_0_0_0/* return_type */
	, RuntimeInvoker_StringSplitOptions_t2301_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__get_Item_TisStringSplitOptions_t2301_m42306_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.StringSplitOptions>(System.Int32,T&)
// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.StringSplitOptions>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringSplitOptions_t2301_1_0_2;
extern Il2CppType StringSplitOptions_t2301_1_0_0;
static ParameterInfo Array_t_Array_GetGenericValueImpl_TisStringSplitOptions_t2301_m42305_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_GetGenericValueImpl_TisStringSplitOptions_t2301_m42305_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_StringSplitOptionsU26_t7525 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_GetGenericValueImpl_TisStringSplitOptions_t2301_m42305_MethodInfo = 
{
	"GetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_StringSplitOptionsU26_t7525/* invoker_method */
	, Array_t_Array_GetGenericValueImpl_TisStringSplitOptions_t2301_m42305_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_GetGenericValueImpl_TisStringSplitOptions_t2301_m42305_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.StringSplitOptions>(T)
extern MethodInfo Array_InternalArray__ICollection_Add_TisStringSplitOptions_t2301_m42307_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.StringSplitOptions>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.StringSplitOptions>(T)
 void Array_InternalArray__ICollection_Add_TisStringSplitOptions_t2301_m42307 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_Add<System.StringSplitOptions>(T)
 void Array_InternalArray__ICollection_Add_TisStringSplitOptions_t2301_m42307 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.StringSplitOptions>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Add_TisStringSplitOptions_t2301_m42307_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Add_TisStringSplitOptions_t2301_m42307_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Add_TisStringSplitOptions_t2301_m42307_MethodInfo = 
{
	"InternalArray__ICollection_Add"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Add_TisStringSplitOptions_t2301_m42307/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Add_TisStringSplitOptions_t2301_m42307_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Add_TisStringSplitOptions_t2301_m42307_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringSplitOptions>(T)
extern MethodInfo Array_InternalArray__ICollection_Contains_TisStringSplitOptions_t2301_m42308_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringSplitOptions>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringSplitOptions>(T)
 bool Array_InternalArray__ICollection_Contains_TisStringSplitOptions_t2301_m42308 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringSplitOptions>(T)
 bool Array_InternalArray__ICollection_Contains_TisStringSplitOptions_t2301_m42308 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		Object_t * L_9 = Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &L_8);
		NullCheck(Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &(*(&___item))));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &(*(&___item))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringSplitOptions>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Contains_TisStringSplitOptions_t2301_m42308_ParameterInfos[] = 
{
	{"item", 0, 134218874, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Contains_TisStringSplitOptions_t2301_m42308_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Contains_TisStringSplitOptions_t2301_m42308_MethodInfo = 
{
	"InternalArray__ICollection_Contains"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Contains_TisStringSplitOptions_t2301_m42308/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Contains_TisStringSplitOptions_t2301_m42308_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Contains_TisStringSplitOptions_t2301_m42308_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringSplitOptions>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisStringSplitOptions_t2301_m42309_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringSplitOptions>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringSplitOptions>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisStringSplitOptions_t2301_m42309 (Array_t * __this, StringSplitOptionsU5BU5D_t5616* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringSplitOptions>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisStringSplitOptions_t2301_m42309 (Array_t * __this, StringSplitOptionsU5BU5D_t5616* ___array, int32_t ___index, MethodInfo* method){
	{
		if (___array)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_3 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_3, L_2, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		NullCheck(___array);
		int32_t L_6 = Array_GetLength_m9760(___array, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		if ((((int32_t)((int32_t)(___index+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t551 * L_7 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_7, (String_t*) &_stringLiteral1279, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		NullCheck(___array);
		int32_t L_8 = Array_get_Rank_m7838(___array, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_10 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_10, L_9, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)___index) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1280, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		ArgumentOutOfRangeException_t1547 * L_12 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7832(L_12, (String_t*) &_stringLiteral474, L_11, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7832_MethodInfo);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		int32_t L_14 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		Array_Copy_m9801(NULL /*static, unused*/, __this, L_13, (Array_t *)(Array_t *)___array, ___index, L_14, /*hidden argument*/&Array_Copy_m9801_MethodInfo);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringSplitOptions>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType StringSplitOptionsU5BU5D_t5616_0_0_0;
extern Il2CppType StringSplitOptionsU5BU5D_t5616_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisStringSplitOptions_t2301_m42309_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &StringSplitOptionsU5BU5D_t5616_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisStringSplitOptions_t2301_m42309_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisStringSplitOptions_t2301_m42309_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisStringSplitOptions_t2301_m42309/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisStringSplitOptions_t2301_m42309_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisStringSplitOptions_t2301_m42309_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringSplitOptions>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisStringSplitOptions_t2301_m42310_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringSplitOptions>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringSplitOptions>(T)
 bool Array_InternalArray__ICollection_Remove_TisStringSplitOptions_t2301_m42310 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringSplitOptions>(T)
 bool Array_InternalArray__ICollection_Remove_TisStringSplitOptions_t2301_m42310 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringSplitOptions>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisStringSplitOptions_t2301_m42310_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisStringSplitOptions_t2301_m42310_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisStringSplitOptions_t2301_m42310_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisStringSplitOptions_t2301_m42310/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisStringSplitOptions_t2301_m42310_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisStringSplitOptions_t2301_m42310_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.StringSplitOptions>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisStringSplitOptions_t2301_m42311_MethodInfo;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.StringSplitOptions>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.StringSplitOptions>(T)
 int32_t Array_InternalArray__IndexOf_TisStringSplitOptions_t2301_m42311 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::InternalArray__IndexOf<System.StringSplitOptions>(T)
 int32_t Array_InternalArray__IndexOf_TisStringSplitOptions_t2301_m42311 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = ___item;
		Object_t * L_11 = Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &L_10);
		NullCheck(Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &(*(&V_2))));
		bool L_12 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.StringSplitOptions>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisStringSplitOptions_t2301_m42311_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisStringSplitOptions_t2301_m42311_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisStringSplitOptions_t2301_m42311_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisStringSplitOptions_t2301_m42311/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisStringSplitOptions_t2301_m42311_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisStringSplitOptions_t2301_m42311_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.StringSplitOptions>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisStringSplitOptions_t2301_m42312_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.StringSplitOptions>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.StringSplitOptions>(System.Int32,T)
 void Array_InternalArray__Insert_TisStringSplitOptions_t2301_m42312 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__Insert<System.StringSplitOptions>(System.Int32,T)
 void Array_InternalArray__Insert_TisStringSplitOptions_t2301_m42312 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.StringSplitOptions>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisStringSplitOptions_t2301_m42312_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisStringSplitOptions_t2301_m42312_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisStringSplitOptions_t2301_m42312_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisStringSplitOptions_t2301_m42312/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisStringSplitOptions_t2301_m42312_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisStringSplitOptions_t2301_m42312_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Array_SetGenericValueImpl_TisStringSplitOptions_t2301_m42313_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.StringSplitOptions>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.StringSplitOptions>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisStringSplitOptions_t2301_m42314_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.StringSplitOptions>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.StringSplitOptions>(System.Int32,T)
 void Array_InternalArray__set_Item_TisStringSplitOptions_t2301_m42314 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__set_Item<System.StringSplitOptions>(System.Int32,T)
 void Array_InternalArray__set_Item_TisStringSplitOptions_t2301_m42314 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	ObjectU5BU5D_t130* V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((ObjectU5BU5D_t130*)IsInst(__this, InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = ___item;
		Object_t * L_3 = Box(InitializedTypeInfo(&StringSplitOptions_t2301_il2cpp_TypeInfo), &L_2);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ___index);
		ArrayElementTypeCheck (V_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(V_0, ___index)) = (Object_t *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, ___index, (&___item));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.StringSplitOptions>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringSplitOptions_t2301_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisStringSplitOptions_t2301_m42314_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisStringSplitOptions_t2301_m42314_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisStringSplitOptions_t2301_m42314_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisStringSplitOptions_t2301_m42314/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisStringSplitOptions_t2301_m42314_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisStringSplitOptions_t2301_m42314_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.StringSplitOptions>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.StringSplitOptions>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StringSplitOptions_t2301_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisStringSplitOptions_t2301_m42313_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &StringSplitOptions_t2301_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisStringSplitOptions_t2301_m42313_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_StringSplitOptionsU26_t7525 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisStringSplitOptions_t2301_m42313_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_StringSplitOptionsU26_t7525/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisStringSplitOptions_t2301_m42313_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisStringSplitOptions_t2301_m42313_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.StringSplitOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_779.h"
extern TypeInfo InternalEnumerator_1_t5350_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.StringSplitOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_779MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32195_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringSplitOptions>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisStringSplitOptions_t2301_m42315_MethodInfo;
struct Array_t;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringSplitOptions>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringSplitOptions>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisStringSplitOptions_t2301_m42315 (Array_t * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringSplitOptions>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisStringSplitOptions_t2301_m42315 (Array_t * __this, MethodInfo* method){
	{
		InternalEnumerator_1_t5350  L_0 = {0};
		InternalEnumerator_1__ctor_m32195(&L_0, __this, /*hidden argument*/&InternalEnumerator_1__ctor_m32195_MethodInfo);
		InternalEnumerator_1_t5350  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&InternalEnumerator_1_t5350_il2cpp_TypeInfo), &L_1);
		return (Object_t*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringSplitOptions>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7526_il2cpp_TypeInfo;
extern Il2CppType IEnumerator_1_t7526_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisStringSplitOptions_t2301_m42315_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisStringSplitOptions_t2301_m42315_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisStringSplitOptions_t2301_m42315/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7526_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisStringSplitOptions_t2301_m42315_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
extern MethodInfo Array_GetGenericValueImpl_TisThreadStaticAttribute_t2302_m42316_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.ThreadStaticAttribute>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.ThreadStaticAttribute>(System.Int32)
extern MethodInfo Array_InternalArray__get_Item_TisThreadStaticAttribute_t2302_m42317_MethodInfo;
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.ThreadStaticAttribute>(System.Int32)
// T System.Array::InternalArray__get_Item<System.ThreadStaticAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisThreadStaticAttribute_t2302_m42317(__this, ___index, method) (ThreadStaticAttribute_t2302 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)___index, method)
// T System.Array::InternalArray__get_Item<System.ThreadStaticAttribute>(System.Int32)
// Metadata Definition T System.Array::InternalArray__get_Item<System.ThreadStaticAttribute>(System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo ThreadStaticAttribute_t2302_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__get_Item_TisThreadStaticAttribute_t2302_m42317_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_ThreadStaticAttribute_t2302_0_0_0;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__get_Item_TisThreadStaticAttribute_t2302_m42317_GenericMethod;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__get_Item_TisThreadStaticAttribute_t2302_m42317_MethodInfo = 
{
	"InternalArray__get_Item"/* name */
	, (methodPointerType)&Array_InternalArray__get_Item_TisObject_t_m32233_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &ThreadStaticAttribute_t2302_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__get_Item_TisThreadStaticAttribute_t2302_m42317_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__get_Item_TisThreadStaticAttribute_t2302_m42317_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.ThreadStaticAttribute>(System.Int32,T&)
// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.ThreadStaticAttribute>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ThreadStaticAttribute_t2302_1_0_2;
extern Il2CppType ThreadStaticAttribute_t2302_1_0_0;
static ParameterInfo Array_t_Array_GetGenericValueImpl_TisThreadStaticAttribute_t2302_m42316_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_GetGenericValueImpl_TisThreadStaticAttribute_t2302_m42316_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_ThreadStaticAttributeU26_t7527 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_GetGenericValueImpl_TisThreadStaticAttribute_t2302_m42316_MethodInfo = 
{
	"GetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_ThreadStaticAttributeU26_t7527/* invoker_method */
	, Array_t_Array_GetGenericValueImpl_TisThreadStaticAttribute_t2302_m42316_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_GetGenericValueImpl_TisThreadStaticAttribute_t2302_m42316_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.ThreadStaticAttribute>(T)
extern MethodInfo Array_InternalArray__ICollection_Add_TisThreadStaticAttribute_t2302_m42318_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.ThreadStaticAttribute>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.ThreadStaticAttribute>(T)
#define Array_InternalArray__ICollection_Add_TisThreadStaticAttribute_t2302_m42318(__this, ___item, method) (void)Array_InternalArray__ICollection_Add_TisObject_t_m32237_gshared((Array_t *)__this, (Object_t *)___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.ThreadStaticAttribute>(T)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.ThreadStaticAttribute>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Add_TisThreadStaticAttribute_t2302_m42318_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Add_TisThreadStaticAttribute_t2302_m42318_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Add_TisThreadStaticAttribute_t2302_m42318_MethodInfo = 
{
	"InternalArray__ICollection_Add"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Add_TisObject_t_m32237_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Add_TisThreadStaticAttribute_t2302_m42318_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Add_TisThreadStaticAttribute_t2302_m42318_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.ThreadStaticAttribute>(T)
extern MethodInfo Array_InternalArray__ICollection_Contains_TisThreadStaticAttribute_t2302_m42319_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.ThreadStaticAttribute>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.ThreadStaticAttribute>(T)
#define Array_InternalArray__ICollection_Contains_TisThreadStaticAttribute_t2302_m42319(__this, ___item, method) (bool)Array_InternalArray__ICollection_Contains_TisObject_t_m32239_gshared((Array_t *)__this, (Object_t *)___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.ThreadStaticAttribute>(T)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.ThreadStaticAttribute>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Contains_TisThreadStaticAttribute_t2302_m42319_ParameterInfos[] = 
{
	{"item", 0, 134218874, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern TypeInfo ThreadStaticAttribute_t2302_il2cpp_TypeInfo;
static Il2CppRGCTXData Array_InternalArray__ICollection_Contains_TisThreadStaticAttribute_t2302_m42319_RGCTXData[1] = 
{
	&ThreadStaticAttribute_t2302_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Contains_TisThreadStaticAttribute_t2302_m42319_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Contains_TisThreadStaticAttribute_t2302_m42319_MethodInfo = 
{
	"InternalArray__ICollection_Contains"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Contains_TisObject_t_m32239_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Contains_TisThreadStaticAttribute_t2302_m42319_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, Array_InternalArray__ICollection_Contains_TisThreadStaticAttribute_t2302_m42319_RGCTXData/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Contains_TisThreadStaticAttribute_t2302_m42319_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.ThreadStaticAttribute>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisThreadStaticAttribute_t2302_m42320_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.ThreadStaticAttribute>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.ThreadStaticAttribute>(T[],System.Int32)
#define Array_InternalArray__ICollection_CopyTo_TisThreadStaticAttribute_t2302_m42320(__this, ___array, ___index, method) (void)Array_InternalArray__ICollection_CopyTo_TisObject_t_m32241_gshared((Array_t *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.ThreadStaticAttribute>(T[],System.Int32)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.ThreadStaticAttribute>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType ThreadStaticAttributeU5BU5D_t5617_0_0_0;
extern Il2CppType ThreadStaticAttributeU5BU5D_t5617_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisThreadStaticAttribute_t2302_m42320_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &ThreadStaticAttributeU5BU5D_t5617_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisThreadStaticAttribute_t2302_m42320_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisThreadStaticAttribute_t2302_m42320_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisObject_t_m32241_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisThreadStaticAttribute_t2302_m42320_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisThreadStaticAttribute_t2302_m42320_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.ThreadStaticAttribute>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisThreadStaticAttribute_t2302_m42321_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.ThreadStaticAttribute>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.ThreadStaticAttribute>(T)
#define Array_InternalArray__ICollection_Remove_TisThreadStaticAttribute_t2302_m42321(__this, ___item, method) (bool)Array_InternalArray__ICollection_Remove_TisObject_t_m32242_gshared((Array_t *)__this, (Object_t *)___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.ThreadStaticAttribute>(T)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.ThreadStaticAttribute>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisThreadStaticAttribute_t2302_m42321_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisThreadStaticAttribute_t2302_m42321_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisThreadStaticAttribute_t2302_m42321_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisObject_t_m32242_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisThreadStaticAttribute_t2302_m42321_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisThreadStaticAttribute_t2302_m42321_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.ThreadStaticAttribute>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisThreadStaticAttribute_t2302_m42322_MethodInfo;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.ThreadStaticAttribute>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.ThreadStaticAttribute>(T)
#define Array_InternalArray__IndexOf_TisThreadStaticAttribute_t2302_m42322(__this, ___item, method) (int32_t)Array_InternalArray__IndexOf_TisObject_t_m32243_gshared((Array_t *)__this, (Object_t *)___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.ThreadStaticAttribute>(T)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.ThreadStaticAttribute>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisThreadStaticAttribute_t2302_m42322_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern TypeInfo ThreadStaticAttribute_t2302_il2cpp_TypeInfo;
static Il2CppRGCTXData Array_InternalArray__IndexOf_TisThreadStaticAttribute_t2302_m42322_RGCTXData[1] = 
{
	&ThreadStaticAttribute_t2302_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisThreadStaticAttribute_t2302_m42322_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisThreadStaticAttribute_t2302_m42322_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisObject_t_m32243_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisThreadStaticAttribute_t2302_m42322_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, Array_InternalArray__IndexOf_TisThreadStaticAttribute_t2302_m42322_RGCTXData/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisThreadStaticAttribute_t2302_m42322_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.ThreadStaticAttribute>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisThreadStaticAttribute_t2302_m42323_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.ThreadStaticAttribute>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.ThreadStaticAttribute>(System.Int32,T)
#define Array_InternalArray__Insert_TisThreadStaticAttribute_t2302_m42323(__this, ___index, ___item, method) (void)Array_InternalArray__Insert_TisObject_t_m32244_gshared((Array_t *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Array::InternalArray__Insert<System.ThreadStaticAttribute>(System.Int32,T)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.ThreadStaticAttribute>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisThreadStaticAttribute_t2302_m42323_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisThreadStaticAttribute_t2302_m42323_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisThreadStaticAttribute_t2302_m42323_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisObject_t_m32244_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisThreadStaticAttribute_t2302_m42323_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisThreadStaticAttribute_t2302_m42323_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Array_SetGenericValueImpl_TisThreadStaticAttribute_t2302_m42324_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.ThreadStaticAttribute>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.ThreadStaticAttribute>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisThreadStaticAttribute_t2302_m42325_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.ThreadStaticAttribute>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.ThreadStaticAttribute>(System.Int32,T)
#define Array_InternalArray__set_Item_TisThreadStaticAttribute_t2302_m42325(__this, ___index, ___item, method) (void)Array_InternalArray__set_Item_TisObject_t_m32246_gshared((Array_t *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Array::InternalArray__set_Item<System.ThreadStaticAttribute>(System.Int32,T)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.ThreadStaticAttribute>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ThreadStaticAttribute_t2302_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisThreadStaticAttribute_t2302_m42325_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_0_0_0},
};
extern TypeInfo ThreadStaticAttribute_t2302_il2cpp_TypeInfo;
static Il2CppRGCTXData Array_InternalArray__set_Item_TisThreadStaticAttribute_t2302_m42325_RGCTXData[1] = 
{
	&ThreadStaticAttribute_t2302_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisThreadStaticAttribute_t2302_m42325_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisThreadStaticAttribute_t2302_m42325_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisObject_t_m32246_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisThreadStaticAttribute_t2302_m42325_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, Array_InternalArray__set_Item_TisThreadStaticAttribute_t2302_m42325_RGCTXData/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisThreadStaticAttribute_t2302_m42325_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.ThreadStaticAttribute>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.ThreadStaticAttribute>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ThreadStaticAttribute_t2302_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisThreadStaticAttribute_t2302_m42324_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ThreadStaticAttribute_t2302_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisThreadStaticAttribute_t2302_m42324_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_ThreadStaticAttributeU26_t7527 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisThreadStaticAttribute_t2302_m42324_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_ThreadStaticAttributeU26_t7527/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisThreadStaticAttribute_t2302_m42324_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisThreadStaticAttribute_t2302_m42324_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_780.h"
extern TypeInfo InternalEnumerator_1_t5351_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.ThreadStaticAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_780MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32200_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.ThreadStaticAttribute>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisThreadStaticAttribute_t2302_m42326_MethodInfo;
struct Array_t;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.ThreadStaticAttribute>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.ThreadStaticAttribute>()
#define Array_InternalArray__IEnumerable_GetEnumerator_TisThreadStaticAttribute_t2302_m42326(__this, method) (Object_t*)Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m32247_gshared((Array_t *)__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.ThreadStaticAttribute>()
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.ThreadStaticAttribute>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7528_il2cpp_TypeInfo;
extern TypeInfo InternalEnumerator_1_t5351_il2cpp_TypeInfo;
static Il2CppRGCTXData Array_InternalArray__IEnumerable_GetEnumerator_TisThreadStaticAttribute_t2302_m42326_RGCTXData[2] = 
{
	&InternalEnumerator_1_t5351_il2cpp_TypeInfo/* Class Usage */,
	&InternalEnumerator_1__ctor_m32200_MethodInfo/* Method Usage */,
};
extern Il2CppType IEnumerator_1_t7528_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisThreadStaticAttribute_t2302_m42326_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisThreadStaticAttribute_t2302_m42326_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m32247_gshared/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7528_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, Array_InternalArray__IEnumerable_GetEnumerator_TisThreadStaticAttribute_t2302_m42326_RGCTXData/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisThreadStaticAttribute_t2302_m42326_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.TypeCode
#include "mscorlib_System_TypeCode.h"
extern MethodInfo Array_GetGenericValueImpl_TisTypeCode_t2305_m42327_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.TypeCode>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.TypeCode>(System.Int32)
extern MethodInfo Array_InternalArray__get_Item_TisTypeCode_t2305_m42328_MethodInfo;
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.TypeCode>(System.Int32)
// T System.Array::InternalArray__get_Item<System.TypeCode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTypeCode_t2305_m42328 (Array_t * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array::InternalArray__get_Item<System.TypeCode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTypeCode_t2305_m42328 (Array_t * __this, int32_t ___index, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, ___index, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.TypeCode>(System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo TypeCode_t2305_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__get_Item_TisTypeCode_t2305_m42328_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_TypeCode_t2305_0_0_0;
extern Il2CppType TypeCode_t2305_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__get_Item_TisTypeCode_t2305_m42328_GenericMethod;
extern void* RuntimeInvoker_TypeCode_t2305_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__get_Item_TisTypeCode_t2305_m42328_MethodInfo = 
{
	"InternalArray__get_Item"/* name */
	, (methodPointerType)&Array_InternalArray__get_Item_TisTypeCode_t2305_m42328/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeCode_t2305_0_0_0/* return_type */
	, RuntimeInvoker_TypeCode_t2305_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__get_Item_TisTypeCode_t2305_m42328_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__get_Item_TisTypeCode_t2305_m42328_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.TypeCode>(System.Int32,T&)
// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.TypeCode>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeCode_t2305_1_0_2;
extern Il2CppType TypeCode_t2305_1_0_0;
static ParameterInfo Array_t_Array_GetGenericValueImpl_TisTypeCode_t2305_m42327_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeCode_t2305_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_GetGenericValueImpl_TisTypeCode_t2305_m42327_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_TypeCodeU26_t7529 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_GetGenericValueImpl_TisTypeCode_t2305_m42327_MethodInfo = 
{
	"GetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_TypeCodeU26_t7529/* invoker_method */
	, Array_t_Array_GetGenericValueImpl_TisTypeCode_t2305_m42327_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_GetGenericValueImpl_TisTypeCode_t2305_m42327_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.TypeCode>(T)
extern MethodInfo Array_InternalArray__ICollection_Add_TisTypeCode_t2305_m42329_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.TypeCode>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.TypeCode>(T)
 void Array_InternalArray__ICollection_Add_TisTypeCode_t2305_m42329 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_Add<System.TypeCode>(T)
 void Array_InternalArray__ICollection_Add_TisTypeCode_t2305_m42329 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.TypeCode>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType TypeCode_t2305_0_0_0;
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Add_TisTypeCode_t2305_m42329_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Add_TisTypeCode_t2305_m42329_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Add_TisTypeCode_t2305_m42329_MethodInfo = 
{
	"InternalArray__ICollection_Add"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Add_TisTypeCode_t2305_m42329/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Add_TisTypeCode_t2305_m42329_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Add_TisTypeCode_t2305_m42329_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TypeCode>(T)
extern MethodInfo Array_InternalArray__ICollection_Contains_TisTypeCode_t2305_m42330_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.TypeCode>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TypeCode>(T)
 bool Array_InternalArray__ICollection_Contains_TisTypeCode_t2305_m42330 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TypeCode>(T)
 bool Array_InternalArray__ICollection_Contains_TisTypeCode_t2305_m42330 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		Object_t * L_9 = Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &L_8);
		NullCheck(Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &(*(&___item))));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &(*(&___item))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.TypeCode>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Contains_TisTypeCode_t2305_m42330_ParameterInfos[] = 
{
	{"item", 0, 134218874, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Contains_TisTypeCode_t2305_m42330_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Contains_TisTypeCode_t2305_m42330_MethodInfo = 
{
	"InternalArray__ICollection_Contains"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Contains_TisTypeCode_t2305_m42330/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Contains_TisTypeCode_t2305_m42330_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Contains_TisTypeCode_t2305_m42330_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TypeCode>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisTypeCode_t2305_m42331_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.TypeCode>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TypeCode>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisTypeCode_t2305_m42331 (Array_t * __this, TypeCodeU5BU5D_t5618* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TypeCode>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisTypeCode_t2305_m42331 (Array_t * __this, TypeCodeU5BU5D_t5618* ___array, int32_t ___index, MethodInfo* method){
	{
		if (___array)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_3 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_3, L_2, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		NullCheck(___array);
		int32_t L_6 = Array_GetLength_m9760(___array, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		if ((((int32_t)((int32_t)(___index+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t551 * L_7 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_7, (String_t*) &_stringLiteral1279, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		NullCheck(___array);
		int32_t L_8 = Array_get_Rank_m7838(___array, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_10 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_10, L_9, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)___index) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1280, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		ArgumentOutOfRangeException_t1547 * L_12 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7832(L_12, (String_t*) &_stringLiteral474, L_11, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7832_MethodInfo);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		int32_t L_14 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		Array_Copy_m9801(NULL /*static, unused*/, __this, L_13, (Array_t *)(Array_t *)___array, ___index, L_14, /*hidden argument*/&Array_Copy_m9801_MethodInfo);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.TypeCode>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType TypeCodeU5BU5D_t5618_0_0_0;
extern Il2CppType TypeCodeU5BU5D_t5618_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisTypeCode_t2305_m42331_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &TypeCodeU5BU5D_t5618_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisTypeCode_t2305_m42331_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisTypeCode_t2305_m42331_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTypeCode_t2305_m42331/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisTypeCode_t2305_m42331_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisTypeCode_t2305_m42331_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TypeCode>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisTypeCode_t2305_m42332_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.TypeCode>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TypeCode>(T)
 bool Array_InternalArray__ICollection_Remove_TisTypeCode_t2305_m42332 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TypeCode>(T)
 bool Array_InternalArray__ICollection_Remove_TisTypeCode_t2305_m42332 (Array_t * __this, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.TypeCode>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisTypeCode_t2305_m42332_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisTypeCode_t2305_m42332_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisTypeCode_t2305_m42332_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisTypeCode_t2305_m42332/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisTypeCode_t2305_m42332_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisTypeCode_t2305_m42332_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.TypeCode>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisTypeCode_t2305_m42333_MethodInfo;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.TypeCode>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.TypeCode>(T)
 int32_t Array_InternalArray__IndexOf_TisTypeCode_t2305_m42333 (Array_t * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::InternalArray__IndexOf<System.TypeCode>(T)
 int32_t Array_InternalArray__IndexOf_TisTypeCode_t2305_m42333 (Array_t * __this, int32_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = ___item;
		Object_t * L_11 = Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &L_10);
		NullCheck(Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &(*(&V_2))));
		bool L_12 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.TypeCode>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisTypeCode_t2305_m42333_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisTypeCode_t2305_m42333_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisTypeCode_t2305_m42333_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisTypeCode_t2305_m42333/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisTypeCode_t2305_m42333_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisTypeCode_t2305_m42333_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.TypeCode>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisTypeCode_t2305_m42334_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.TypeCode>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.TypeCode>(System.Int32,T)
 void Array_InternalArray__Insert_TisTypeCode_t2305_m42334 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__Insert<System.TypeCode>(System.Int32,T)
 void Array_InternalArray__Insert_TisTypeCode_t2305_m42334 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.TypeCode>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisTypeCode_t2305_m42334_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisTypeCode_t2305_m42334_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisTypeCode_t2305_m42334_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisTypeCode_t2305_m42334/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisTypeCode_t2305_m42334_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisTypeCode_t2305_m42334_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Array_SetGenericValueImpl_TisTypeCode_t2305_m42335_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.TypeCode>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.TypeCode>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisTypeCode_t2305_m42336_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.TypeCode>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.TypeCode>(System.Int32,T)
 void Array_InternalArray__set_Item_TisTypeCode_t2305_m42336 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__set_Item<System.TypeCode>(System.Int32,T)
 void Array_InternalArray__set_Item_TisTypeCode_t2305_m42336 (Array_t * __this, int32_t ___index, int32_t ___item, MethodInfo* method){
	ObjectU5BU5D_t130* V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((ObjectU5BU5D_t130*)IsInst(__this, InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = ___item;
		Object_t * L_3 = Box(InitializedTypeInfo(&TypeCode_t2305_il2cpp_TypeInfo), &L_2);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ___index);
		ArrayElementTypeCheck (V_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(V_0, ___index)) = (Object_t *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, ___index, (&___item));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.TypeCode>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeCode_t2305_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisTypeCode_t2305_m42336_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &TypeCode_t2305_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisTypeCode_t2305_m42336_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisTypeCode_t2305_m42336_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisTypeCode_t2305_m42336/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisTypeCode_t2305_m42336_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisTypeCode_t2305_m42336_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.TypeCode>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.TypeCode>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypeCode_t2305_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisTypeCode_t2305_m42335_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeCode_t2305_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisTypeCode_t2305_m42335_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_TypeCodeU26_t7529 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisTypeCode_t2305_m42335_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_TypeCodeU26_t7529/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisTypeCode_t2305_m42335_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisTypeCode_t2305_m42335_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.TypeCode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_781.h"
extern TypeInfo InternalEnumerator_1_t5356_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.TypeCode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_781MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32222_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TypeCode>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisTypeCode_t2305_m42337_MethodInfo;
struct Array_t;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TypeCode>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TypeCode>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTypeCode_t2305_m42337 (Array_t * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TypeCode>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTypeCode_t2305_m42337 (Array_t * __this, MethodInfo* method){
	{
		InternalEnumerator_1_t5356  L_0 = {0};
		InternalEnumerator_1__ctor_m32222(&L_0, __this, /*hidden argument*/&InternalEnumerator_1__ctor_m32222_MethodInfo);
		InternalEnumerator_1_t5356  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&InternalEnumerator_1_t5356_il2cpp_TypeInfo), &L_1);
		return (Object_t*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TypeCode>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7530_il2cpp_TypeInfo;
extern Il2CppType IEnumerator_1_t7530_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisTypeCode_t2305_m42337_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisTypeCode_t2305_m42337_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTypeCode_t2305_m42337/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7530_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisTypeCode_t2305_m42337_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityType.h"
extern MethodInfo Array_GetGenericValueImpl_TisUnityType_t2309_m42338_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.UnitySerializationHolder/UnityType>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.UnitySerializationHolder/UnityType>(System.Int32)
extern MethodInfo Array_InternalArray__get_Item_TisUnityType_t2309_m42339_MethodInfo;
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.UnitySerializationHolder/UnityType>(System.Int32)
// T System.Array::InternalArray__get_Item<System.UnitySerializationHolder/UnityType>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisUnityType_t2309_m42339 (Array_t * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array::InternalArray__get_Item<System.UnitySerializationHolder/UnityType>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisUnityType_t2309_m42339 (Array_t * __this, int32_t ___index, MethodInfo* method){
	uint8_t V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, ___index, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.UnitySerializationHolder/UnityType>(System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo UnityType_t2309_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__get_Item_TisUnityType_t2309_m42339_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppGenericInst GenInst_UnityType_t2309_0_0_0;
extern Il2CppType UnityType_t2309_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__get_Item_TisUnityType_t2309_m42339_GenericMethod;
extern void* RuntimeInvoker_UnityType_t2309_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__get_Item_TisUnityType_t2309_m42339_MethodInfo = 
{
	"InternalArray__get_Item"/* name */
	, (methodPointerType)&Array_InternalArray__get_Item_TisUnityType_t2309_m42339/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &UnityType_t2309_0_0_0/* return_type */
	, RuntimeInvoker_UnityType_t2309_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__get_Item_TisUnityType_t2309_m42339_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__get_Item_TisUnityType_t2309_m42339_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.UnitySerializationHolder/UnityType>(System.Int32,T&)
// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.UnitySerializationHolder/UnityType>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UnityType_t2309_1_0_2;
extern Il2CppType UnityType_t2309_1_0_0;
static ParameterInfo Array_t_Array_GetGenericValueImpl_TisUnityType_t2309_m42338_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UnityType_t2309_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_GetGenericValueImpl_TisUnityType_t2309_m42338_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_UnityTypeU26_t7531 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_GetGenericValueImpl_TisUnityType_t2309_m42338_MethodInfo = 
{
	"GetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_UnityTypeU26_t7531/* invoker_method */
	, Array_t_Array_GetGenericValueImpl_TisUnityType_t2309_m42338_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_GetGenericValueImpl_TisUnityType_t2309_m42338_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.UnitySerializationHolder/UnityType>(T)
extern MethodInfo Array_InternalArray__ICollection_Add_TisUnityType_t2309_m42340_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.UnitySerializationHolder/UnityType>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.UnitySerializationHolder/UnityType>(T)
 void Array_InternalArray__ICollection_Add_TisUnityType_t2309_m42340 (Array_t * __this, uint8_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_Add<System.UnitySerializationHolder/UnityType>(T)
 void Array_InternalArray__ICollection_Add_TisUnityType_t2309_m42340 (Array_t * __this, uint8_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.UnitySerializationHolder/UnityType>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType UnityType_t2309_0_0_0;
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Add_TisUnityType_t2309_m42340_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Add_TisUnityType_t2309_m42340_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Byte_t838 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Add_TisUnityType_t2309_m42340_MethodInfo = 
{
	"InternalArray__ICollection_Add"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Add_TisUnityType_t2309_m42340/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Byte_t838/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Add_TisUnityType_t2309_m42340_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Add_TisUnityType_t2309_m42340_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.UnitySerializationHolder/UnityType>(T)
extern MethodInfo Array_InternalArray__ICollection_Contains_TisUnityType_t2309_m42341_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.UnitySerializationHolder/UnityType>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.UnitySerializationHolder/UnityType>(T)
 bool Array_InternalArray__ICollection_Contains_TisUnityType_t2309_m42341 (Array_t * __this, uint8_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.UnitySerializationHolder/UnityType>(T)
 bool Array_InternalArray__ICollection_Contains_TisUnityType_t2309_m42341 (Array_t * __this, uint8_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		uint8_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		uint8_t L_8 = V_2;
		Object_t * L_9 = Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &L_8);
		NullCheck(Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &(*(&___item))));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &(*(&___item))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.UnitySerializationHolder/UnityType>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Contains_TisUnityType_t2309_m42341_ParameterInfos[] = 
{
	{"item", 0, 134218874, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Contains_TisUnityType_t2309_m42341_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Contains_TisUnityType_t2309_m42341_MethodInfo = 
{
	"InternalArray__ICollection_Contains"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Contains_TisUnityType_t2309_m42341/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Contains_TisUnityType_t2309_m42341_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Contains_TisUnityType_t2309_m42341_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.UnitySerializationHolder/UnityType>(T[],System.Int32)
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisUnityType_t2309_m42342_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.UnitySerializationHolder/UnityType>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.UnitySerializationHolder/UnityType>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisUnityType_t2309_m42342 (Array_t * __this, UnityTypeU5BU5D_t5619* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.UnitySerializationHolder/UnityType>(T[],System.Int32)
 void Array_InternalArray__ICollection_CopyTo_TisUnityType_t2309_m42342 (Array_t * __this, UnityTypeU5BU5D_t5619* ___array, int32_t ___index, MethodInfo* method){
	{
		if (___array)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral473, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_3 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_3, L_2, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9762(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		NullCheck(___array);
		int32_t L_6 = Array_GetLength_m9760(___array, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		if ((((int32_t)((int32_t)(___index+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t551 * L_7 = (ArgumentException_t551 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t551_il2cpp_TypeInfo));
		ArgumentException__ctor_m2636(L_7, (String_t*) &_stringLiteral1279, /*hidden argument*/&ArgumentException__ctor_m2636_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		NullCheck(___array);
		int32_t L_8 = Array_get_Rank_m7838(___array, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_10 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_10, L_9, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)___index) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1280, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		ArgumentOutOfRangeException_t1547 * L_12 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7832(L_12, (String_t*) &_stringLiteral474, L_11, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7832_MethodInfo);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		int32_t L_14 = Array_GetLength_m9760(__this, 0, /*hidden argument*/&Array_GetLength_m9760_MethodInfo);
		Array_Copy_m9801(NULL /*static, unused*/, __this, L_13, (Array_t *)(Array_t *)___array, ___index, L_14, /*hidden argument*/&Array_Copy_m9801_MethodInfo);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.UnitySerializationHolder/UnityType>(T[],System.Int32)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType UnityTypeU5BU5D_t5619_0_0_0;
extern Il2CppType UnityTypeU5BU5D_t5619_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_CopyTo_TisUnityType_t2309_m42342_ParameterInfos[] = 
{
	{"array", 0, 134218875, &EmptyCustomAttributesCache, &UnityTypeU5BU5D_t5619_0_0_0},
	{"index", 1, 134218876, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_CopyTo_TisUnityType_t2309_m42342_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_CopyTo_TisUnityType_t2309_m42342_MethodInfo = 
{
	"InternalArray__ICollection_CopyTo"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUnityType_t2309_m42342/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_CopyTo_TisUnityType_t2309_m42342_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_CopyTo_TisUnityType_t2309_m42342_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.UnitySerializationHolder/UnityType>(T)
extern MethodInfo Array_InternalArray__ICollection_Remove_TisUnityType_t2309_m42343_MethodInfo;
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.UnitySerializationHolder/UnityType>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.UnitySerializationHolder/UnityType>(T)
 bool Array_InternalArray__ICollection_Remove_TisUnityType_t2309_m42343 (Array_t * __this, uint8_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.UnitySerializationHolder/UnityType>(T)
 bool Array_InternalArray__ICollection_Remove_TisUnityType_t2309_m42343 (Array_t * __this, uint8_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.UnitySerializationHolder/UnityType>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__ICollection_Remove_TisUnityType_t2309_m42343_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__ICollection_Remove_TisUnityType_t2309_m42343_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122_Byte_t838 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__ICollection_Remove_TisUnityType_t2309_m42343_MethodInfo = 
{
	"InternalArray__ICollection_Remove"/* name */
	, (methodPointerType)&Array_InternalArray__ICollection_Remove_TisUnityType_t2309_m42343/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Byte_t838/* invoker_method */
	, Array_t_Array_InternalArray__ICollection_Remove_TisUnityType_t2309_m42343_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__ICollection_Remove_TisUnityType_t2309_m42343_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.UnitySerializationHolder/UnityType>(T)
extern MethodInfo Array_InternalArray__IndexOf_TisUnityType_t2309_m42344_MethodInfo;
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.UnitySerializationHolder/UnityType>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.UnitySerializationHolder/UnityType>(T)
 int32_t Array_InternalArray__IndexOf_TisUnityType_t2309_m42344 (Array_t * __this, uint8_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::InternalArray__IndexOf<System.UnitySerializationHolder/UnityType>(T)
 int32_t Array_InternalArray__IndexOf_TisUnityType_t2309_m42344 (Array_t * __this, uint8_t ___item, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = Array_get_Rank_m7838(__this, /*hidden argument*/&Array_get_Rank_m7838_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9928(NULL /*static, unused*/, (String_t*) &_stringLiteral1278, /*hidden argument*/&Locale_GetText_m9928_MethodInfo);
		RankException_t2296 * L_2 = (RankException_t2296 *)il2cpp_codegen_object_new (InitializedTypeInfo(&RankException_t2296_il2cpp_TypeInfo));
		RankException__ctor_m13346(L_2, L_1, /*hidden argument*/&RankException__ctor_m13346_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = ___item;
		Object_t * L_5 = Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		uint8_t L_6 = V_2;
		Object_t * L_7 = Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		uint8_t L_10 = ___item;
		Object_t * L_11 = Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &L_10);
		NullCheck(Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &(*(&V_2))));
		bool L_12 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m320_MethodInfo, Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = Array_GetLowerBound_m9762(__this, 0, /*hidden argument*/&Array_GetLowerBound_m9762_MethodInfo);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.UnitySerializationHolder/UnityType>(T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__IndexOf_TisUnityType_t2309_m42344_ParameterInfos[] = 
{
	{"item", 0, 134218880, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IndexOf_TisUnityType_t2309_m42344_GenericMethod;
extern void* RuntimeInvoker_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IndexOf_TisUnityType_t2309_m42344_MethodInfo = 
{
	"InternalArray__IndexOf"/* name */
	, (methodPointerType)&Array_InternalArray__IndexOf_TisUnityType_t2309_m42344/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Byte_t838/* invoker_method */
	, Array_t_Array_InternalArray__IndexOf_TisUnityType_t2309_m42344_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IndexOf_TisUnityType_t2309_m42344_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.UnitySerializationHolder/UnityType>(System.Int32,T)
extern MethodInfo Array_InternalArray__Insert_TisUnityType_t2309_m42345_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.UnitySerializationHolder/UnityType>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.UnitySerializationHolder/UnityType>(System.Int32,T)
 void Array_InternalArray__Insert_TisUnityType_t2309_m42345 (Array_t * __this, int32_t ___index, uint8_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__Insert<System.UnitySerializationHolder/UnityType>(System.Int32,T)
 void Array_InternalArray__Insert_TisUnityType_t2309_m42345 (Array_t * __this, int32_t ___index, uint8_t ___item, MethodInfo* method){
	{
		NotSupportedException_t498 * L_0 = (NotSupportedException_t498 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NotSupportedException_t498_il2cpp_TypeInfo));
		NotSupportedException__ctor_m7849(L_0, (String_t*) &_stringLiteral508, /*hidden argument*/&NotSupportedException__ctor_m7849_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.UnitySerializationHolder/UnityType>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__Insert_TisUnityType_t2309_m42345_ParameterInfos[] = 
{
	{"index", 0, 134218877, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218878, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__Insert_TisUnityType_t2309_m42345_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__Insert_TisUnityType_t2309_m42345_MethodInfo = 
{
	"InternalArray__Insert"/* name */
	, (methodPointerType)&Array_InternalArray__Insert_TisUnityType_t2309_m42345/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, Array_t_Array_InternalArray__Insert_TisUnityType_t2309_m42345_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__Insert_TisUnityType_t2309_m42345_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Array_SetGenericValueImpl_TisUnityType_t2309_m42346_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.UnitySerializationHolder/UnityType>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.UnitySerializationHolder/UnityType>(System.Int32,T)
extern MethodInfo Array_InternalArray__set_Item_TisUnityType_t2309_m42347_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.UnitySerializationHolder/UnityType>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.UnitySerializationHolder/UnityType>(System.Int32,T)
 void Array_InternalArray__set_Item_TisUnityType_t2309_m42347 (Array_t * __this, int32_t ___index, uint8_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::InternalArray__set_Item<System.UnitySerializationHolder/UnityType>(System.Int32,T)
 void Array_InternalArray__set_Item_TisUnityType_t2309_m42347 (Array_t * __this, int32_t ___index, uint8_t ___item, MethodInfo* method){
	ObjectU5BU5D_t130* V_0 = {0};
	{
		int32_t L_0 = Array_get_Length_m814(__this, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		if ((((uint32_t)___index) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1547 * L_1 = (ArgumentOutOfRangeException_t1547 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1547_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7836(L_1, (String_t*) &_stringLiteral474, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7836_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((ObjectU5BU5D_t130*)IsInst(__this, InitializedTypeInfo(&ObjectU5BU5D_t130_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		uint8_t L_2 = ___item;
		Object_t * L_3 = Box(InitializedTypeInfo(&UnityType_t2309_il2cpp_TypeInfo), &L_2);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ___index);
		ArrayElementTypeCheck (V_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(V_0, ___index)) = (Object_t *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, ___index, (&___item));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.UnitySerializationHolder/UnityType>(System.Int32,T)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UnityType_t2309_0_0_0;
static ParameterInfo Array_t_Array_InternalArray__set_Item_TisUnityType_t2309_m42347_ParameterInfos[] = 
{
	{"index", 0, 134218882, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134218883, &EmptyCustomAttributesCache, &UnityType_t2309_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__set_Item_TisUnityType_t2309_m42347_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Byte_t838 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__set_Item_TisUnityType_t2309_m42347_MethodInfo = 
{
	"InternalArray__set_Item"/* name */
	, (methodPointerType)&Array_InternalArray__set_Item_TisUnityType_t2309_m42347/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Byte_t838/* invoker_method */
	, Array_t_Array_InternalArray__set_Item_TisUnityType_t2309_m42347_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__set_Item_TisUnityType_t2309_m42347_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.UnitySerializationHolder/UnityType>(System.Int32,T&)
// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.UnitySerializationHolder/UnityType>(System.Int32,T&)
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UnityType_t2309_1_0_0;
static ParameterInfo Array_t_Array_SetGenericValueImpl_TisUnityType_t2309_m42346_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UnityType_t2309_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern Il2CppGenericMethod Array_SetGenericValueImpl_TisUnityType_t2309_m42346_GenericMethod;
extern void* RuntimeInvoker_Void_t111_Int32_t123_UnityTypeU26_t7531 (MethodInfo* method, void* obj, void** args);
MethodInfo Array_SetGenericValueImpl_TisUnityType_t2309_m42346_MethodInfo = 
{
	"SetGenericValueImpl"/* name */
	, NULL/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_UnityTypeU26_t7531/* invoker_method */
	, Array_t_Array_SetGenericValueImpl_TisUnityType_t2309_m42346_ParameterInfos/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_SetGenericValueImpl_TisUnityType_t2309_m42346_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_782.h"
extern TypeInfo InternalEnumerator_1_t5357_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.UnitySerializationHolder/UnityType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_782MethodDeclarations.h"
extern MethodInfo InternalEnumerator_1__ctor_m32227_MethodInfo;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.UnitySerializationHolder/UnityType>()
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisUnityType_t2309_m42348_MethodInfo;
struct Array_t;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.UnitySerializationHolder/UnityType>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.UnitySerializationHolder/UnityType>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisUnityType_t2309_m42348 (Array_t * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.UnitySerializationHolder/UnityType>()
 Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisUnityType_t2309_m42348 (Array_t * __this, MethodInfo* method){
	{
		InternalEnumerator_1_t5357  L_0 = {0};
		InternalEnumerator_1__ctor_m32227(&L_0, __this, /*hidden argument*/&InternalEnumerator_1__ctor_m32227_MethodInfo);
		InternalEnumerator_1_t5357  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&InternalEnumerator_1_t5357_il2cpp_TypeInfo), &L_1);
		return (Object_t*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.UnitySerializationHolder/UnityType>()
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t7532_il2cpp_TypeInfo;
extern Il2CppType IEnumerator_1_t7532_0_0_0;
extern Il2CppGenericMethod Array_InternalArray__IEnumerable_GetEnumerator_TisUnityType_t2309_m42348_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisUnityType_t2309_m42348_MethodInfo = 
{
	"InternalArray__IEnumerable_GetEnumerator"/* name */
	, (methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUnityType_t2309_m42348/* method */
	, &Array_t_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7532_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &Array_InternalArray__IEnumerable_GetEnumerator_TisUnityType_t2309_m42348_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// Vuforia.SmartTerrainBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder_0.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.ReconstructionFromTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT_0.h"
extern TypeInfo ReconstructionFromTarget_t597_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo QCARWrapper_t754_il2cpp_TypeInfo;
extern TypeInfo IQCARWrapper_t753_il2cpp_TypeInfo;
extern TypeInfo IntPtr_t121_il2cpp_TypeInfo;
extern TypeInfo ReconstructionFromTargetImpl_t614_il2cpp_TypeInfo;
extern TypeInfo IPremiumObjectFactory_t681_il2cpp_TypeInfo;
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// Vuforia.QCARWrapper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARWrapperMethodDeclarations.h"
// Vuforia.ReconstructionFromTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT_0MethodDeclarations.h"
// Vuforia.PremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor_0MethodDeclarations.h"
extern Il2CppType ReconstructionFromTarget_t597_0_0_0;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo QCARWrapper_get_Instance_m4090_MethodInfo;
extern MethodInfo IQCARWrapper_SmartTerrainBuilderCreateReconstructionFromTarget_m5238_MethodInfo;
extern MethodInfo ReconstructionFromTargetImpl__ctor_m2886_MethodInfo;
extern MethodInfo PremiumObjectFactory_get_Instance_m3141_MethodInfo;
extern MethodInfo IPremiumObjectFactory_CreateReconstruction_TisReconstructionFromTarget_t597_m42349_MethodInfo;
struct IPremiumObjectFactory_t681;
// Declaration T Vuforia.IPremiumObjectFactory::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
// T Vuforia.IPremiumObjectFactory::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
// T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
extern MethodInfo SmartTerrainBuilderImpl_CreateReconstruction_TisReconstructionFromTarget_t597_m42350_MethodInfo;
struct SmartTerrainBuilderImpl_t715;
// Declaration T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
// T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
 Object_t * SmartTerrainBuilderImpl_CreateReconstruction_TisReconstructionFromTarget_t597_m42350 (SmartTerrainBuilderImpl_t715 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
 Object_t * SmartTerrainBuilderImpl_CreateReconstruction_TisReconstructionFromTarget_t597_m42350 (SmartTerrainBuilderImpl_t715 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ReconstructionFromTarget_t597_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ReconstructionFromTarget_t597_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_0) != ((Type_t *)L_1)))
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_2 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		NullCheck(L_2);
		IntPtr_t121 L_3 = (IntPtr_t121)InterfaceFuncInvoker0< IntPtr_t121 >::Invoke(&IQCARWrapper_SmartTerrainBuilderCreateReconstructionFromTarget_m5238_MethodInfo, L_2);
		ReconstructionFromTargetImpl_t614 * L_4 = (ReconstructionFromTargetImpl_t614 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ReconstructionFromTargetImpl_t614_il2cpp_TypeInfo));
		ReconstructionFromTargetImpl__ctor_m2886(L_4, L_3, /*hidden argument*/&ReconstructionFromTargetImpl__ctor_m2886_MethodInfo);
		return ((Object_t *)Castclass(((Object_t *)IsInst(L_4, InitializedTypeInfo(&ReconstructionFromTarget_t597_il2cpp_TypeInfo))), InitializedTypeInfo(&ReconstructionFromTarget_t597_il2cpp_TypeInfo)));
	}

IL_0030:
	{
		Object_t * L_5 = PremiumObjectFactory_get_Instance_m3141(NULL /*static, unused*/, /*hidden argument*/&PremiumObjectFactory_get_Instance_m3141_MethodInfo);
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)GenericInterfaceFuncInvoker0< Object_t * >::Invoke(&IPremiumObjectFactory_CreateReconstruction_TisReconstructionFromTarget_t597_m42349_MethodInfo, L_5);
		return L_6;
	}
}
// Metadata Definition T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
extern TypeInfo SmartTerrainBuilderImpl_t715_il2cpp_TypeInfo;
extern Il2CppGenericInst GenInst_ReconstructionFromTarget_t597_0_0_0;
extern Il2CppType ReconstructionFromTarget_t597_0_0_0;
extern TypeInfo SmartTerrainBuilderImpl_t715_il2cpp_TypeInfo;
extern Il2CppGenericMethod SmartTerrainBuilderImpl_CreateReconstruction_TisReconstructionFromTarget_t597_m42350_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo SmartTerrainBuilderImpl_CreateReconstruction_TisReconstructionFromTarget_t597_m42350_MethodInfo = 
{
	"CreateReconstruction"/* name */
	, (methodPointerType)&SmartTerrainBuilderImpl_CreateReconstruction_TisReconstructionFromTarget_t597_m42350/* method */
	, &SmartTerrainBuilderImpl_t715_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTarget_t597_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &SmartTerrainBuilderImpl_CreateReconstruction_TisReconstructionFromTarget_t597_m42350_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// T Vuforia.IPremiumObjectFactory::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
// T Vuforia.IPremiumObjectFactory::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
// Metadata Definition T Vuforia.IPremiumObjectFactory::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
extern TypeInfo IPremiumObjectFactory_t681_il2cpp_TypeInfo;
extern Il2CppType ReconstructionFromTarget_t597_0_0_0;
extern Il2CppGenericMethod IPremiumObjectFactory_CreateReconstruction_TisReconstructionFromTarget_t597_m42349_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo IPremiumObjectFactory_CreateReconstruction_TisReconstructionFromTarget_t597_m42349_MethodInfo = 
{
	"CreateReconstruction"/* name */
	, NULL/* method */
	, &IPremiumObjectFactory_t681_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTarget_t597_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &IPremiumObjectFactory_CreateReconstruction_TisReconstructionFromTarget_t597_m42349_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// Vuforia.TrackerManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManagerImpl.h"
// Vuforia.SmartTerrainTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_0.h"
// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTracker.h"
// Vuforia.MarkerTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTracker.h"
// Vuforia.TextTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTracker.h"
extern TypeInfo SmartTerrainTracker_t720_il2cpp_TypeInfo;
extern TypeInfo ObjectTracker_t605_il2cpp_TypeInfo;
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern TypeInfo MarkerTracker_t666_il2cpp_TypeInfo;
extern TypeInfo TextTracker_t722_il2cpp_TypeInfo;
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern Il2CppType SmartTerrainTracker_t720_0_0_0;
extern Il2CppType ObjectTracker_t605_0_0_0;
extern Il2CppType MarkerTracker_t666_0_0_0;
extern Il2CppType TextTracker_t722_0_0_0;
extern MethodInfo Debug_LogError_m270_MethodInfo;
// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.SmartTerrainTracker>()
extern MethodInfo TrackerManagerImpl_GetTracker_TisSmartTerrainTracker_t720_m42351_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.SmartTerrainTracker>()
// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.SmartTerrainTracker>()
 SmartTerrainTracker_t720 * TrackerManagerImpl_GetTracker_TisSmartTerrainTracker_t720_m42351 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.SmartTerrainTracker>()
 SmartTerrainTracker_t720 * TrackerManagerImpl_GetTracker_TisSmartTerrainTracker_t720_m42351 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	SmartTerrainTracker_t720 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_0) != ((Type_t *)L_1)))
		{
			goto IL_0027;
		}
	}
	{
		ObjectTracker_t605 * L_2 = (__this->___mObjectTracker_1);
		return ((SmartTerrainTracker_t720 *)Castclass(((SmartTerrainTracker_t720 *)IsInst(L_2, InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo))), InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo)));
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_3 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_4 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_3) != ((Type_t *)L_4)))
		{
			goto IL_004e;
		}
	}
	{
		MarkerTracker_t666 * L_5 = (__this->___mMarkerTracker_2);
		return ((SmartTerrainTracker_t720 *)Castclass(((SmartTerrainTracker_t720 *)IsInst(L_5, InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo))), InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo)));
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_7 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_6) != ((Type_t *)L_7)))
		{
			goto IL_0075;
		}
	}
	{
		TextTracker_t722 * L_8 = (__this->___mTextTracker_3);
		return ((SmartTerrainTracker_t720 *)Castclass(((SmartTerrainTracker_t720 *)IsInst(L_8, InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo))), InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo)));
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_9 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_9) != ((Type_t *)L_10)))
		{
			goto IL_009c;
		}
	}
	{
		SmartTerrainTracker_t720 * L_11 = (__this->___mSmartTerrainTracker_4);
		return ((SmartTerrainTracker_t720 *)Castclass(((SmartTerrainTracker_t720 *)IsInst(L_11, InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo))), InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo)));
	}

IL_009c:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral282, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&SmartTerrainTracker_t720_il2cpp_TypeInfo, (&V_0));
		return V_0;
	}
}
// Metadata Definition T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.SmartTerrainTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppGenericInst GenInst_SmartTerrainTracker_t720_0_0_0;
extern Il2CppType SmartTerrainTracker_t720_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_GetTracker_TisSmartTerrainTracker_t720_m42351_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_GetTracker_TisSmartTerrainTracker_t720_m42351_MethodInfo = 
{
	"GetTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_GetTracker_TisSmartTerrainTracker_t720_m42351/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTracker_t720_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_GetTracker_TisSmartTerrainTracker_t720_m42351_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.ObjectTracker>()
extern MethodInfo TrackerManagerImpl_GetTracker_TisObjectTracker_t605_m42352_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.ObjectTracker>()
// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.ObjectTracker>()
 ObjectTracker_t605 * TrackerManagerImpl_GetTracker_TisObjectTracker_t605_m42352 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.ObjectTracker>()
 ObjectTracker_t605 * TrackerManagerImpl_GetTracker_TisObjectTracker_t605_m42352 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	ObjectTracker_t605 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_0) != ((Type_t *)L_1)))
		{
			goto IL_0027;
		}
	}
	{
		ObjectTracker_t605 * L_2 = (__this->___mObjectTracker_1);
		return ((ObjectTracker_t605 *)Castclass(((ObjectTracker_t605 *)IsInst(L_2, InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo))), InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo)));
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_3 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_4 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_3) != ((Type_t *)L_4)))
		{
			goto IL_004e;
		}
	}
	{
		MarkerTracker_t666 * L_5 = (__this->___mMarkerTracker_2);
		return ((ObjectTracker_t605 *)Castclass(((ObjectTracker_t605 *)IsInst(L_5, InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo))), InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo)));
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_7 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_6) != ((Type_t *)L_7)))
		{
			goto IL_0075;
		}
	}
	{
		TextTracker_t722 * L_8 = (__this->___mTextTracker_3);
		return ((ObjectTracker_t605 *)Castclass(((ObjectTracker_t605 *)IsInst(L_8, InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo))), InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo)));
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_9 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_9) != ((Type_t *)L_10)))
		{
			goto IL_009c;
		}
	}
	{
		SmartTerrainTracker_t720 * L_11 = (__this->___mSmartTerrainTracker_4);
		return ((ObjectTracker_t605 *)Castclass(((ObjectTracker_t605 *)IsInst(L_11, InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo))), InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo)));
	}

IL_009c:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral282, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&ObjectTracker_t605_il2cpp_TypeInfo, (&V_0));
		return V_0;
	}
}
// Metadata Definition T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.ObjectTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppGenericInst GenInst_ObjectTracker_t605_0_0_0;
extern Il2CppType ObjectTracker_t605_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_GetTracker_TisObjectTracker_t605_m42352_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_GetTracker_TisObjectTracker_t605_m42352_MethodInfo = 
{
	"GetTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_GetTracker_TisObjectTracker_t605_m42352/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTracker_t605_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_GetTracker_TisObjectTracker_t605_m42352_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.UInt16
#include "mscorlib_System_UInt16.h"
// Vuforia.ObjectTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerImpl.h"
// Vuforia.MarkerTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTrackerImpl.h"
// Vuforia.TextTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImpl.h"
// Vuforia.SmartTerrainTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_1.h"
extern TypeInfo QCARRuntimeUtilities_t156_il2cpp_TypeInfo;
extern TypeInfo TypeMapping_t727_il2cpp_TypeInfo;
extern TypeInfo ObjectTrackerImpl_t664_il2cpp_TypeInfo;
extern TypeInfo MarkerTrackerImpl_t670_il2cpp_TypeInfo;
extern TypeInfo TextTrackerImpl_t725_il2cpp_TypeInfo;
extern TypeInfo SmartTerrainTrackerImpl_t721_il2cpp_TypeInfo;
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitieMethodDeclarations.h"
// Vuforia.TypeMapping
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TypeMappingMethodDeclarations.h"
// Vuforia.ObjectTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerImplMethodDeclarations.h"
// Vuforia.MarkerTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTrackerImplMethodDeclarations.h"
// Vuforia.TextTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImplMethodDeclarations.h"
// Vuforia.SmartTerrainTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_1MethodDeclarations.h"
extern MethodInfo QCARRuntimeUtilities_IsQCAREnabled_m361_MethodInfo;
extern MethodInfo TypeMapping_GetTypeID_m3246_MethodInfo;
extern MethodInfo IQCARWrapper_TrackerManagerInitTracker_m5254_MethodInfo;
extern MethodInfo ObjectTrackerImpl__ctor_m3093_MethodInfo;
extern MethodInfo MarkerTrackerImpl__ctor_m3122_MethodInfo;
extern MethodInfo TextTrackerImpl__ctor_m3245_MethodInfo;
extern MethodInfo SmartTerrainTrackerImpl__ctor_m3236_MethodInfo;
// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.SmartTerrainTracker>()
extern MethodInfo TrackerManagerImpl_InitTracker_TisSmartTerrainTracker_t720_m42353_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.SmartTerrainTracker>()
// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.SmartTerrainTracker>()
 SmartTerrainTracker_t720 * TrackerManagerImpl_InitTracker_TisSmartTerrainTracker_t720_m42353 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.SmartTerrainTracker>()
 SmartTerrainTracker_t720 * TrackerManagerImpl_InitTracker_TisSmartTerrainTracker_t720_m42353 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	SmartTerrainTracker_t720 * V_0 = {0};
	SmartTerrainTracker_t720 * V_1 = {0};
	SmartTerrainTracker_t720 * V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m361(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m361_MethodInfo);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Initobj (&SmartTerrainTracker_t720_il2cpp_TypeInfo, (&V_0));
		return V_0;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_1 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TypeMapping_t727_il2cpp_TypeInfo));
		uint16_t L_3 = TypeMapping_GetTypeID_m3246(NULL /*static, unused*/, L_2, /*hidden argument*/&TypeMapping_GetTypeID_m3246_MethodInfo);
		NullCheck(L_1);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&IQCARWrapper_TrackerManagerInitTracker_m5254_MethodInfo, L_1, L_3);
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral283, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&SmartTerrainTracker_t720_il2cpp_TypeInfo, (&V_1));
		return V_1;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_5) != ((Type_t *)L_6)))
		{
			goto IL_007a;
		}
	}
	{
		ObjectTracker_t605 * L_7 = (__this->___mObjectTracker_1);
		if (L_7)
		{
			goto IL_0069;
		}
	}
	{
		ObjectTrackerImpl_t664 * L_8 = (ObjectTrackerImpl_t664 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ObjectTrackerImpl_t664_il2cpp_TypeInfo));
		ObjectTrackerImpl__ctor_m3093(L_8, /*hidden argument*/&ObjectTrackerImpl__ctor_m3093_MethodInfo);
		__this->___mObjectTracker_1 = L_8;
	}

IL_0069:
	{
		ObjectTracker_t605 * L_9 = (__this->___mObjectTracker_1);
		return ((SmartTerrainTracker_t720 *)Castclass(((SmartTerrainTracker_t720 *)IsInst(L_9, InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo))), InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo)));
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_11 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_10) != ((Type_t *)L_11)))
		{
			goto IL_00b4;
		}
	}
	{
		MarkerTracker_t666 * L_12 = (__this->___mMarkerTracker_2);
		if (L_12)
		{
			goto IL_00a3;
		}
	}
	{
		MarkerTrackerImpl_t670 * L_13 = (MarkerTrackerImpl_t670 *)il2cpp_codegen_object_new (InitializedTypeInfo(&MarkerTrackerImpl_t670_il2cpp_TypeInfo));
		MarkerTrackerImpl__ctor_m3122(L_13, /*hidden argument*/&MarkerTrackerImpl__ctor_m3122_MethodInfo);
		__this->___mMarkerTracker_2 = L_13;
	}

IL_00a3:
	{
		MarkerTracker_t666 * L_14 = (__this->___mMarkerTracker_2);
		return ((SmartTerrainTracker_t720 *)Castclass(((SmartTerrainTracker_t720 *)IsInst(L_14, InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo))), InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo)));
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_15 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_16 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_15) != ((Type_t *)L_16)))
		{
			goto IL_00ee;
		}
	}
	{
		TextTracker_t722 * L_17 = (__this->___mTextTracker_3);
		if (L_17)
		{
			goto IL_00dd;
		}
	}
	{
		TextTrackerImpl_t725 * L_18 = (TextTrackerImpl_t725 *)il2cpp_codegen_object_new (InitializedTypeInfo(&TextTrackerImpl_t725_il2cpp_TypeInfo));
		TextTrackerImpl__ctor_m3245(L_18, /*hidden argument*/&TextTrackerImpl__ctor_m3245_MethodInfo);
		__this->___mTextTracker_3 = L_18;
	}

IL_00dd:
	{
		TextTracker_t722 * L_19 = (__this->___mTextTracker_3);
		return ((SmartTerrainTracker_t720 *)Castclass(((SmartTerrainTracker_t720 *)IsInst(L_19, InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo))), InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo)));
	}

IL_00ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_20 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_21 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_20) != ((Type_t *)L_21)))
		{
			goto IL_0128;
		}
	}
	{
		SmartTerrainTracker_t720 * L_22 = (__this->___mSmartTerrainTracker_4);
		if (L_22)
		{
			goto IL_0117;
		}
	}
	{
		SmartTerrainTrackerImpl_t721 * L_23 = (SmartTerrainTrackerImpl_t721 *)il2cpp_codegen_object_new (InitializedTypeInfo(&SmartTerrainTrackerImpl_t721_il2cpp_TypeInfo));
		SmartTerrainTrackerImpl__ctor_m3236(L_23, /*hidden argument*/&SmartTerrainTrackerImpl__ctor_m3236_MethodInfo);
		__this->___mSmartTerrainTracker_4 = L_23;
	}

IL_0117:
	{
		SmartTerrainTracker_t720 * L_24 = (__this->___mSmartTerrainTracker_4);
		return ((SmartTerrainTracker_t720 *)Castclass(((SmartTerrainTracker_t720 *)IsInst(L_24, InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo))), InitializedTypeInfo(&SmartTerrainTracker_t720_il2cpp_TypeInfo)));
	}

IL_0128:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral284, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&SmartTerrainTracker_t720_il2cpp_TypeInfo, (&V_2));
		return V_2;
	}
}
// Metadata Definition T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.SmartTerrainTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppType SmartTerrainTracker_t720_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_InitTracker_TisSmartTerrainTracker_t720_m42353_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_InitTracker_TisSmartTerrainTracker_t720_m42353_MethodInfo = 
{
	"InitTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_InitTracker_TisSmartTerrainTracker_t720_m42353/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTracker_t720_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_InitTracker_TisSmartTerrainTracker_t720_m42353_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.MarkerTracker>()
extern MethodInfo TrackerManagerImpl_GetTracker_TisMarkerTracker_t666_m42354_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.MarkerTracker>()
// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.MarkerTracker>()
 MarkerTracker_t666 * TrackerManagerImpl_GetTracker_TisMarkerTracker_t666_m42354 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.MarkerTracker>()
 MarkerTracker_t666 * TrackerManagerImpl_GetTracker_TisMarkerTracker_t666_m42354 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	MarkerTracker_t666 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_0) != ((Type_t *)L_1)))
		{
			goto IL_0027;
		}
	}
	{
		ObjectTracker_t605 * L_2 = (__this->___mObjectTracker_1);
		return ((MarkerTracker_t666 *)Castclass(((MarkerTracker_t666 *)IsInst(L_2, InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo))), InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo)));
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_3 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_4 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_3) != ((Type_t *)L_4)))
		{
			goto IL_004e;
		}
	}
	{
		MarkerTracker_t666 * L_5 = (__this->___mMarkerTracker_2);
		return ((MarkerTracker_t666 *)Castclass(((MarkerTracker_t666 *)IsInst(L_5, InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo))), InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo)));
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_7 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_6) != ((Type_t *)L_7)))
		{
			goto IL_0075;
		}
	}
	{
		TextTracker_t722 * L_8 = (__this->___mTextTracker_3);
		return ((MarkerTracker_t666 *)Castclass(((MarkerTracker_t666 *)IsInst(L_8, InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo))), InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo)));
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_9 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_9) != ((Type_t *)L_10)))
		{
			goto IL_009c;
		}
	}
	{
		SmartTerrainTracker_t720 * L_11 = (__this->___mSmartTerrainTracker_4);
		return ((MarkerTracker_t666 *)Castclass(((MarkerTracker_t666 *)IsInst(L_11, InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo))), InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo)));
	}

IL_009c:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral282, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&MarkerTracker_t666_il2cpp_TypeInfo, (&V_0));
		return V_0;
	}
}
// Metadata Definition T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.MarkerTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppGenericInst GenInst_MarkerTracker_t666_0_0_0;
extern Il2CppType MarkerTracker_t666_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_GetTracker_TisMarkerTracker_t666_m42354_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_GetTracker_TisMarkerTracker_t666_m42354_MethodInfo = 
{
	"GetTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_GetTracker_TisMarkerTracker_t666_m42354/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &MarkerTracker_t666_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_GetTracker_TisMarkerTracker_t666_m42354_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.MarkerTracker>()
extern MethodInfo TrackerManagerImpl_InitTracker_TisMarkerTracker_t666_m42355_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.MarkerTracker>()
// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.MarkerTracker>()
 MarkerTracker_t666 * TrackerManagerImpl_InitTracker_TisMarkerTracker_t666_m42355 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.MarkerTracker>()
 MarkerTracker_t666 * TrackerManagerImpl_InitTracker_TisMarkerTracker_t666_m42355 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	MarkerTracker_t666 * V_0 = {0};
	MarkerTracker_t666 * V_1 = {0};
	MarkerTracker_t666 * V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m361(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m361_MethodInfo);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Initobj (&MarkerTracker_t666_il2cpp_TypeInfo, (&V_0));
		return V_0;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_1 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TypeMapping_t727_il2cpp_TypeInfo));
		uint16_t L_3 = TypeMapping_GetTypeID_m3246(NULL /*static, unused*/, L_2, /*hidden argument*/&TypeMapping_GetTypeID_m3246_MethodInfo);
		NullCheck(L_1);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&IQCARWrapper_TrackerManagerInitTracker_m5254_MethodInfo, L_1, L_3);
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral283, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&MarkerTracker_t666_il2cpp_TypeInfo, (&V_1));
		return V_1;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_5) != ((Type_t *)L_6)))
		{
			goto IL_007a;
		}
	}
	{
		ObjectTracker_t605 * L_7 = (__this->___mObjectTracker_1);
		if (L_7)
		{
			goto IL_0069;
		}
	}
	{
		ObjectTrackerImpl_t664 * L_8 = (ObjectTrackerImpl_t664 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ObjectTrackerImpl_t664_il2cpp_TypeInfo));
		ObjectTrackerImpl__ctor_m3093(L_8, /*hidden argument*/&ObjectTrackerImpl__ctor_m3093_MethodInfo);
		__this->___mObjectTracker_1 = L_8;
	}

IL_0069:
	{
		ObjectTracker_t605 * L_9 = (__this->___mObjectTracker_1);
		return ((MarkerTracker_t666 *)Castclass(((MarkerTracker_t666 *)IsInst(L_9, InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo))), InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo)));
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_11 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_10) != ((Type_t *)L_11)))
		{
			goto IL_00b4;
		}
	}
	{
		MarkerTracker_t666 * L_12 = (__this->___mMarkerTracker_2);
		if (L_12)
		{
			goto IL_00a3;
		}
	}
	{
		MarkerTrackerImpl_t670 * L_13 = (MarkerTrackerImpl_t670 *)il2cpp_codegen_object_new (InitializedTypeInfo(&MarkerTrackerImpl_t670_il2cpp_TypeInfo));
		MarkerTrackerImpl__ctor_m3122(L_13, /*hidden argument*/&MarkerTrackerImpl__ctor_m3122_MethodInfo);
		__this->___mMarkerTracker_2 = L_13;
	}

IL_00a3:
	{
		MarkerTracker_t666 * L_14 = (__this->___mMarkerTracker_2);
		return ((MarkerTracker_t666 *)Castclass(((MarkerTracker_t666 *)IsInst(L_14, InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo))), InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo)));
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_15 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_16 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_15) != ((Type_t *)L_16)))
		{
			goto IL_00ee;
		}
	}
	{
		TextTracker_t722 * L_17 = (__this->___mTextTracker_3);
		if (L_17)
		{
			goto IL_00dd;
		}
	}
	{
		TextTrackerImpl_t725 * L_18 = (TextTrackerImpl_t725 *)il2cpp_codegen_object_new (InitializedTypeInfo(&TextTrackerImpl_t725_il2cpp_TypeInfo));
		TextTrackerImpl__ctor_m3245(L_18, /*hidden argument*/&TextTrackerImpl__ctor_m3245_MethodInfo);
		__this->___mTextTracker_3 = L_18;
	}

IL_00dd:
	{
		TextTracker_t722 * L_19 = (__this->___mTextTracker_3);
		return ((MarkerTracker_t666 *)Castclass(((MarkerTracker_t666 *)IsInst(L_19, InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo))), InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo)));
	}

IL_00ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_20 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_21 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_20) != ((Type_t *)L_21)))
		{
			goto IL_0128;
		}
	}
	{
		SmartTerrainTracker_t720 * L_22 = (__this->___mSmartTerrainTracker_4);
		if (L_22)
		{
			goto IL_0117;
		}
	}
	{
		SmartTerrainTrackerImpl_t721 * L_23 = (SmartTerrainTrackerImpl_t721 *)il2cpp_codegen_object_new (InitializedTypeInfo(&SmartTerrainTrackerImpl_t721_il2cpp_TypeInfo));
		SmartTerrainTrackerImpl__ctor_m3236(L_23, /*hidden argument*/&SmartTerrainTrackerImpl__ctor_m3236_MethodInfo);
		__this->___mSmartTerrainTracker_4 = L_23;
	}

IL_0117:
	{
		SmartTerrainTracker_t720 * L_24 = (__this->___mSmartTerrainTracker_4);
		return ((MarkerTracker_t666 *)Castclass(((MarkerTracker_t666 *)IsInst(L_24, InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo))), InitializedTypeInfo(&MarkerTracker_t666_il2cpp_TypeInfo)));
	}

IL_0128:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral284, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&MarkerTracker_t666_il2cpp_TypeInfo, (&V_2));
		return V_2;
	}
}
// Metadata Definition T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.MarkerTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppType MarkerTracker_t666_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_InitTracker_TisMarkerTracker_t666_m42355_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_InitTracker_TisMarkerTracker_t666_m42355_MethodInfo = 
{
	"InitTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_InitTracker_TisMarkerTracker_t666_m42355/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &MarkerTracker_t666_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_InitTracker_TisMarkerTracker_t666_m42355_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.ObjectTracker>()
extern MethodInfo TrackerManagerImpl_InitTracker_TisObjectTracker_t605_m42356_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.ObjectTracker>()
// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.ObjectTracker>()
 ObjectTracker_t605 * TrackerManagerImpl_InitTracker_TisObjectTracker_t605_m42356 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.ObjectTracker>()
 ObjectTracker_t605 * TrackerManagerImpl_InitTracker_TisObjectTracker_t605_m42356 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	ObjectTracker_t605 * V_0 = {0};
	ObjectTracker_t605 * V_1 = {0};
	ObjectTracker_t605 * V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m361(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m361_MethodInfo);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Initobj (&ObjectTracker_t605_il2cpp_TypeInfo, (&V_0));
		return V_0;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_1 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TypeMapping_t727_il2cpp_TypeInfo));
		uint16_t L_3 = TypeMapping_GetTypeID_m3246(NULL /*static, unused*/, L_2, /*hidden argument*/&TypeMapping_GetTypeID_m3246_MethodInfo);
		NullCheck(L_1);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&IQCARWrapper_TrackerManagerInitTracker_m5254_MethodInfo, L_1, L_3);
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral283, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&ObjectTracker_t605_il2cpp_TypeInfo, (&V_1));
		return V_1;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_5) != ((Type_t *)L_6)))
		{
			goto IL_007a;
		}
	}
	{
		ObjectTracker_t605 * L_7 = (__this->___mObjectTracker_1);
		if (L_7)
		{
			goto IL_0069;
		}
	}
	{
		ObjectTrackerImpl_t664 * L_8 = (ObjectTrackerImpl_t664 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ObjectTrackerImpl_t664_il2cpp_TypeInfo));
		ObjectTrackerImpl__ctor_m3093(L_8, /*hidden argument*/&ObjectTrackerImpl__ctor_m3093_MethodInfo);
		__this->___mObjectTracker_1 = L_8;
	}

IL_0069:
	{
		ObjectTracker_t605 * L_9 = (__this->___mObjectTracker_1);
		return ((ObjectTracker_t605 *)Castclass(((ObjectTracker_t605 *)IsInst(L_9, InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo))), InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo)));
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_11 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_10) != ((Type_t *)L_11)))
		{
			goto IL_00b4;
		}
	}
	{
		MarkerTracker_t666 * L_12 = (__this->___mMarkerTracker_2);
		if (L_12)
		{
			goto IL_00a3;
		}
	}
	{
		MarkerTrackerImpl_t670 * L_13 = (MarkerTrackerImpl_t670 *)il2cpp_codegen_object_new (InitializedTypeInfo(&MarkerTrackerImpl_t670_il2cpp_TypeInfo));
		MarkerTrackerImpl__ctor_m3122(L_13, /*hidden argument*/&MarkerTrackerImpl__ctor_m3122_MethodInfo);
		__this->___mMarkerTracker_2 = L_13;
	}

IL_00a3:
	{
		MarkerTracker_t666 * L_14 = (__this->___mMarkerTracker_2);
		return ((ObjectTracker_t605 *)Castclass(((ObjectTracker_t605 *)IsInst(L_14, InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo))), InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo)));
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_15 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_16 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_15) != ((Type_t *)L_16)))
		{
			goto IL_00ee;
		}
	}
	{
		TextTracker_t722 * L_17 = (__this->___mTextTracker_3);
		if (L_17)
		{
			goto IL_00dd;
		}
	}
	{
		TextTrackerImpl_t725 * L_18 = (TextTrackerImpl_t725 *)il2cpp_codegen_object_new (InitializedTypeInfo(&TextTrackerImpl_t725_il2cpp_TypeInfo));
		TextTrackerImpl__ctor_m3245(L_18, /*hidden argument*/&TextTrackerImpl__ctor_m3245_MethodInfo);
		__this->___mTextTracker_3 = L_18;
	}

IL_00dd:
	{
		TextTracker_t722 * L_19 = (__this->___mTextTracker_3);
		return ((ObjectTracker_t605 *)Castclass(((ObjectTracker_t605 *)IsInst(L_19, InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo))), InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo)));
	}

IL_00ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_20 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_21 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_20) != ((Type_t *)L_21)))
		{
			goto IL_0128;
		}
	}
	{
		SmartTerrainTracker_t720 * L_22 = (__this->___mSmartTerrainTracker_4);
		if (L_22)
		{
			goto IL_0117;
		}
	}
	{
		SmartTerrainTrackerImpl_t721 * L_23 = (SmartTerrainTrackerImpl_t721 *)il2cpp_codegen_object_new (InitializedTypeInfo(&SmartTerrainTrackerImpl_t721_il2cpp_TypeInfo));
		SmartTerrainTrackerImpl__ctor_m3236(L_23, /*hidden argument*/&SmartTerrainTrackerImpl__ctor_m3236_MethodInfo);
		__this->___mSmartTerrainTracker_4 = L_23;
	}

IL_0117:
	{
		SmartTerrainTracker_t720 * L_24 = (__this->___mSmartTerrainTracker_4);
		return ((ObjectTracker_t605 *)Castclass(((ObjectTracker_t605 *)IsInst(L_24, InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo))), InitializedTypeInfo(&ObjectTracker_t605_il2cpp_TypeInfo)));
	}

IL_0128:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral284, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&ObjectTracker_t605_il2cpp_TypeInfo, (&V_2));
		return V_2;
	}
}
// Metadata Definition T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.ObjectTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppType ObjectTracker_t605_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_InitTracker_TisObjectTracker_t605_m42356_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_InitTracker_TisObjectTracker_t605_m42356_MethodInfo = 
{
	"InitTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_InitTracker_TisObjectTracker_t605_m42356/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTracker_t605_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_InitTracker_TisObjectTracker_t605_m42356_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.TextTracker>()
extern MethodInfo TrackerManagerImpl_GetTracker_TisTextTracker_t722_m42357_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.TextTracker>()
// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.TextTracker>()
 TextTracker_t722 * TrackerManagerImpl_GetTracker_TisTextTracker_t722_m42357 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.TextTracker>()
 TextTracker_t722 * TrackerManagerImpl_GetTracker_TisTextTracker_t722_m42357 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	TextTracker_t722 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_0) != ((Type_t *)L_1)))
		{
			goto IL_0027;
		}
	}
	{
		ObjectTracker_t605 * L_2 = (__this->___mObjectTracker_1);
		return ((TextTracker_t722 *)Castclass(((TextTracker_t722 *)IsInst(L_2, InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo))), InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo)));
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_3 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_4 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_3) != ((Type_t *)L_4)))
		{
			goto IL_004e;
		}
	}
	{
		MarkerTracker_t666 * L_5 = (__this->___mMarkerTracker_2);
		return ((TextTracker_t722 *)Castclass(((TextTracker_t722 *)IsInst(L_5, InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo))), InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo)));
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_7 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_6) != ((Type_t *)L_7)))
		{
			goto IL_0075;
		}
	}
	{
		TextTracker_t722 * L_8 = (__this->___mTextTracker_3);
		return ((TextTracker_t722 *)Castclass(((TextTracker_t722 *)IsInst(L_8, InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo))), InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo)));
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_9 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_9) != ((Type_t *)L_10)))
		{
			goto IL_009c;
		}
	}
	{
		SmartTerrainTracker_t720 * L_11 = (__this->___mSmartTerrainTracker_4);
		return ((TextTracker_t722 *)Castclass(((TextTracker_t722 *)IsInst(L_11, InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo))), InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo)));
	}

IL_009c:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral282, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&TextTracker_t722_il2cpp_TypeInfo, (&V_0));
		return V_0;
	}
}
// Metadata Definition T Vuforia.TrackerManagerImpl::GetTracker<Vuforia.TextTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppGenericInst GenInst_TextTracker_t722_0_0_0;
extern Il2CppType TextTracker_t722_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_GetTracker_TisTextTracker_t722_m42357_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_GetTracker_TisTextTracker_t722_m42357_MethodInfo = 
{
	"GetTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_GetTracker_TisTextTracker_t722_m42357/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &TextTracker_t722_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_GetTracker_TisTextTracker_t722_m42357_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo IQCARWrapper_TrackerManagerDeinitTracker_m5255_MethodInfo;
// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.MarkerTracker>()
extern MethodInfo TrackerManagerImpl_DeinitTracker_TisMarkerTracker_t666_m42358_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.MarkerTracker>()
// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.MarkerTracker>()
 bool TrackerManagerImpl_DeinitTracker_TisMarkerTracker_t666_m42358 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.MarkerTracker>()
 bool TrackerManagerImpl_DeinitTracker_TisMarkerTracker_t666_m42358 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_0 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TypeMapping_t727_il2cpp_TypeInfo));
		uint16_t L_2 = TypeMapping_GetTypeID_m3246(NULL /*static, unused*/, L_1, /*hidden argument*/&TypeMapping_GetTypeID_m3246_MethodInfo);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&IQCARWrapper_TrackerManagerDeinitTracker_m5255_MethodInfo, L_0, L_2);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral285, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		return 0;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_4 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_4) != ((Type_t *)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		__this->___mObjectTracker_1 = (ObjectTracker_t605 *)NULL;
		goto IL_00af;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_7 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_6) != ((Type_t *)L_7)))
		{
			goto IL_0065;
		}
	}
	{
		__this->___mMarkerTracker_2 = (MarkerTracker_t666 *)NULL;
		goto IL_00af;
	}

IL_0065:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_8 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_9 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_8) != ((Type_t *)L_9)))
		{
			goto IL_0084;
		}
	}
	{
		__this->___mTextTracker_3 = (TextTracker_t722 *)NULL;
		goto IL_00af;
	}

IL_0084:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_11 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_10) != ((Type_t *)L_11)))
		{
			goto IL_00a3;
		}
	}
	{
		__this->___mSmartTerrainTracker_4 = (SmartTerrainTracker_t720 *)NULL;
		goto IL_00af;
	}

IL_00a3:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral286, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		return 0;
	}

IL_00af:
	{
		return 1;
	}
}
// Metadata Definition System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.MarkerTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_DeinitTracker_TisMarkerTracker_t666_m42358_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_DeinitTracker_TisMarkerTracker_t666_m42358_MethodInfo = 
{
	"DeinitTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_DeinitTracker_TisMarkerTracker_t666_m42358/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_DeinitTracker_TisMarkerTracker_t666_m42358_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.ObjectTracker>()
extern MethodInfo TrackerManagerImpl_DeinitTracker_TisObjectTracker_t605_m42359_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.ObjectTracker>()
// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.ObjectTracker>()
 bool TrackerManagerImpl_DeinitTracker_TisObjectTracker_t605_m42359 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.ObjectTracker>()
 bool TrackerManagerImpl_DeinitTracker_TisObjectTracker_t605_m42359 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_0 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TypeMapping_t727_il2cpp_TypeInfo));
		uint16_t L_2 = TypeMapping_GetTypeID_m3246(NULL /*static, unused*/, L_1, /*hidden argument*/&TypeMapping_GetTypeID_m3246_MethodInfo);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&IQCARWrapper_TrackerManagerDeinitTracker_m5255_MethodInfo, L_0, L_2);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral285, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		return 0;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_4 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_4) != ((Type_t *)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		__this->___mObjectTracker_1 = (ObjectTracker_t605 *)NULL;
		goto IL_00af;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_7 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_6) != ((Type_t *)L_7)))
		{
			goto IL_0065;
		}
	}
	{
		__this->___mMarkerTracker_2 = (MarkerTracker_t666 *)NULL;
		goto IL_00af;
	}

IL_0065:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_8 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_9 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_8) != ((Type_t *)L_9)))
		{
			goto IL_0084;
		}
	}
	{
		__this->___mTextTracker_3 = (TextTracker_t722 *)NULL;
		goto IL_00af;
	}

IL_0084:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_11 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_10) != ((Type_t *)L_11)))
		{
			goto IL_00a3;
		}
	}
	{
		__this->___mSmartTerrainTracker_4 = (SmartTerrainTracker_t720 *)NULL;
		goto IL_00af;
	}

IL_00a3:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral286, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		return 0;
	}

IL_00af:
	{
		return 1;
	}
}
// Metadata Definition System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.ObjectTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_DeinitTracker_TisObjectTracker_t605_m42359_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_DeinitTracker_TisObjectTracker_t605_m42359_MethodInfo = 
{
	"DeinitTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_DeinitTracker_TisObjectTracker_t605_m42359/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_DeinitTracker_TisObjectTracker_t605_m42359_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.TextTracker>()
extern MethodInfo TrackerManagerImpl_DeinitTracker_TisTextTracker_t722_m42360_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.TextTracker>()
// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.TextTracker>()
 bool TrackerManagerImpl_DeinitTracker_TisTextTracker_t722_m42360 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.TextTracker>()
 bool TrackerManagerImpl_DeinitTracker_TisTextTracker_t722_m42360 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_0 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TypeMapping_t727_il2cpp_TypeInfo));
		uint16_t L_2 = TypeMapping_GetTypeID_m3246(NULL /*static, unused*/, L_1, /*hidden argument*/&TypeMapping_GetTypeID_m3246_MethodInfo);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&IQCARWrapper_TrackerManagerDeinitTracker_m5255_MethodInfo, L_0, L_2);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral285, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		return 0;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_4 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_4) != ((Type_t *)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		__this->___mObjectTracker_1 = (ObjectTracker_t605 *)NULL;
		goto IL_00af;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_7 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_6) != ((Type_t *)L_7)))
		{
			goto IL_0065;
		}
	}
	{
		__this->___mMarkerTracker_2 = (MarkerTracker_t666 *)NULL;
		goto IL_00af;
	}

IL_0065:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_8 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_9 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_8) != ((Type_t *)L_9)))
		{
			goto IL_0084;
		}
	}
	{
		__this->___mTextTracker_3 = (TextTracker_t722 *)NULL;
		goto IL_00af;
	}

IL_0084:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_11 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_10) != ((Type_t *)L_11)))
		{
			goto IL_00a3;
		}
	}
	{
		__this->___mSmartTerrainTracker_4 = (SmartTerrainTracker_t720 *)NULL;
		goto IL_00af;
	}

IL_00a3:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral286, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		return 0;
	}

IL_00af:
	{
		return 1;
	}
}
// Metadata Definition System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.TextTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_DeinitTracker_TisTextTracker_t722_m42360_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_DeinitTracker_TisTextTracker_t722_m42360_MethodInfo = 
{
	"DeinitTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_DeinitTracker_TisTextTracker_t722_m42360/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_DeinitTracker_TisTextTracker_t722_m42360_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.SmartTerrainTracker>()
extern MethodInfo TrackerManagerImpl_DeinitTracker_TisSmartTerrainTracker_t720_m42361_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.SmartTerrainTracker>()
// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.SmartTerrainTracker>()
 bool TrackerManagerImpl_DeinitTracker_TisSmartTerrainTracker_t720_m42361 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.SmartTerrainTracker>()
 bool TrackerManagerImpl_DeinitTracker_TisSmartTerrainTracker_t720_m42361 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_0 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TypeMapping_t727_il2cpp_TypeInfo));
		uint16_t L_2 = TypeMapping_GetTypeID_m3246(NULL /*static, unused*/, L_1, /*hidden argument*/&TypeMapping_GetTypeID_m3246_MethodInfo);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&IQCARWrapper_TrackerManagerDeinitTracker_m5255_MethodInfo, L_0, L_2);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral285, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		return 0;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_4 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_4) != ((Type_t *)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		__this->___mObjectTracker_1 = (ObjectTracker_t605 *)NULL;
		goto IL_00af;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_7 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_6) != ((Type_t *)L_7)))
		{
			goto IL_0065;
		}
	}
	{
		__this->___mMarkerTracker_2 = (MarkerTracker_t666 *)NULL;
		goto IL_00af;
	}

IL_0065:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_8 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_9 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_8) != ((Type_t *)L_9)))
		{
			goto IL_0084;
		}
	}
	{
		__this->___mTextTracker_3 = (TextTracker_t722 *)NULL;
		goto IL_00af;
	}

IL_0084:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_11 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_10) != ((Type_t *)L_11)))
		{
			goto IL_00a3;
		}
	}
	{
		__this->___mSmartTerrainTracker_4 = (SmartTerrainTracker_t720 *)NULL;
		goto IL_00af;
	}

IL_00a3:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral286, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		return 0;
	}

IL_00af:
	{
		return 1;
	}
}
// Metadata Definition System.Boolean Vuforia.TrackerManagerImpl::DeinitTracker<Vuforia.SmartTerrainTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_DeinitTracker_TisSmartTerrainTracker_t720_m42361_GenericMethod;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_DeinitTracker_TisSmartTerrainTracker_t720_m42361_MethodInfo = 
{
	"DeinitTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_DeinitTracker_TisSmartTerrainTracker_t720_m42361/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_DeinitTracker_TisSmartTerrainTracker_t720_m42361_GenericMethod/* genericMethod */

};
#ifndef _MSC_VER
#else
#endif

// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.TextTracker>()
extern MethodInfo TrackerManagerImpl_InitTracker_TisTextTracker_t722_m42362_MethodInfo;
struct TrackerManagerImpl_t788;
// Declaration T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.TextTracker>()
// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.TextTracker>()
 TextTracker_t722 * TrackerManagerImpl_InitTracker_TisTextTracker_t722_m42362 (TrackerManagerImpl_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.TextTracker>()
 TextTracker_t722 * TrackerManagerImpl_InitTracker_TisTextTracker_t722_m42362 (TrackerManagerImpl_t788 * __this, MethodInfo* method){
	TextTracker_t722 * V_0 = {0};
	TextTracker_t722 * V_1 = {0};
	TextTracker_t722 * V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m361(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m361_MethodInfo);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Initobj (&TextTracker_t722_il2cpp_TypeInfo, (&V_0));
		return V_0;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_1 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TypeMapping_t727_il2cpp_TypeInfo));
		uint16_t L_3 = TypeMapping_GetTypeID_m3246(NULL /*static, unused*/, L_2, /*hidden argument*/&TypeMapping_GetTypeID_m3246_MethodInfo);
		NullCheck(L_1);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(&IQCARWrapper_TrackerManagerInitTracker_m5254_MethodInfo, L_1, L_3);
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral283, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&TextTracker_t722_il2cpp_TypeInfo, (&V_1));
		return V_1;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_6 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&ObjectTracker_t605_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_5) != ((Type_t *)L_6)))
		{
			goto IL_007a;
		}
	}
	{
		ObjectTracker_t605 * L_7 = (__this->___mObjectTracker_1);
		if (L_7)
		{
			goto IL_0069;
		}
	}
	{
		ObjectTrackerImpl_t664 * L_8 = (ObjectTrackerImpl_t664 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ObjectTrackerImpl_t664_il2cpp_TypeInfo));
		ObjectTrackerImpl__ctor_m3093(L_8, /*hidden argument*/&ObjectTrackerImpl__ctor_m3093_MethodInfo);
		__this->___mObjectTracker_1 = L_8;
	}

IL_0069:
	{
		ObjectTracker_t605 * L_9 = (__this->___mObjectTracker_1);
		return ((TextTracker_t722 *)Castclass(((TextTracker_t722 *)IsInst(L_9, InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo))), InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo)));
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_10 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_11 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&MarkerTracker_t666_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_10) != ((Type_t *)L_11)))
		{
			goto IL_00b4;
		}
	}
	{
		MarkerTracker_t666 * L_12 = (__this->___mMarkerTracker_2);
		if (L_12)
		{
			goto IL_00a3;
		}
	}
	{
		MarkerTrackerImpl_t670 * L_13 = (MarkerTrackerImpl_t670 *)il2cpp_codegen_object_new (InitializedTypeInfo(&MarkerTrackerImpl_t670_il2cpp_TypeInfo));
		MarkerTrackerImpl__ctor_m3122(L_13, /*hidden argument*/&MarkerTrackerImpl__ctor_m3122_MethodInfo);
		__this->___mMarkerTracker_2 = L_13;
	}

IL_00a3:
	{
		MarkerTracker_t666 * L_14 = (__this->___mMarkerTracker_2);
		return ((TextTracker_t722 *)Castclass(((TextTracker_t722 *)IsInst(L_14, InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo))), InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo)));
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_15 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_16 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_15) != ((Type_t *)L_16)))
		{
			goto IL_00ee;
		}
	}
	{
		TextTracker_t722 * L_17 = (__this->___mTextTracker_3);
		if (L_17)
		{
			goto IL_00dd;
		}
	}
	{
		TextTrackerImpl_t725 * L_18 = (TextTrackerImpl_t725 *)il2cpp_codegen_object_new (InitializedTypeInfo(&TextTrackerImpl_t725_il2cpp_TypeInfo));
		TextTrackerImpl__ctor_m3245(L_18, /*hidden argument*/&TextTrackerImpl__ctor_m3245_MethodInfo);
		__this->___mTextTracker_3 = L_18;
	}

IL_00dd:
	{
		TextTracker_t722 * L_19 = (__this->___mTextTracker_3);
		return ((TextTracker_t722 *)Castclass(((TextTracker_t722 *)IsInst(L_19, InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo))), InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo)));
	}

IL_00ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_20 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TextTracker_t722_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Type_t * L_21 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&SmartTerrainTracker_t720_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		if ((((Type_t *)L_20) != ((Type_t *)L_21)))
		{
			goto IL_0128;
		}
	}
	{
		SmartTerrainTracker_t720 * L_22 = (__this->___mSmartTerrainTracker_4);
		if (L_22)
		{
			goto IL_0117;
		}
	}
	{
		SmartTerrainTrackerImpl_t721 * L_23 = (SmartTerrainTrackerImpl_t721 *)il2cpp_codegen_object_new (InitializedTypeInfo(&SmartTerrainTrackerImpl_t721_il2cpp_TypeInfo));
		SmartTerrainTrackerImpl__ctor_m3236(L_23, /*hidden argument*/&SmartTerrainTrackerImpl__ctor_m3236_MethodInfo);
		__this->___mSmartTerrainTracker_4 = L_23;
	}

IL_0117:
	{
		SmartTerrainTracker_t720 * L_24 = (__this->___mSmartTerrainTracker_4);
		return ((TextTracker_t722 *)Castclass(((TextTracker_t722 *)IsInst(L_24, InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo))), InitializedTypeInfo(&TextTracker_t722_il2cpp_TypeInfo)));
	}

IL_0128:
	{
		Debug_LogError_m270(NULL /*static, unused*/, (String_t*) &_stringLiteral284, /*hidden argument*/&Debug_LogError_m270_MethodInfo);
		Initobj (&TextTracker_t722_il2cpp_TypeInfo, (&V_2));
		return V_2;
	}
}
// Metadata Definition T Vuforia.TrackerManagerImpl::InitTracker<Vuforia.TextTracker>()
extern TypeInfo TrackerManagerImpl_t788_il2cpp_TypeInfo;
extern Il2CppType TextTracker_t722_0_0_0;
extern Il2CppGenericMethod TrackerManagerImpl_InitTracker_TisTextTracker_t722_m42362_GenericMethod;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
MethodInfo TrackerManagerImpl_InitTracker_TisTextTracker_t722_m42362_MethodInfo = 
{
	"InitTracker"/* name */
	, (methodPointerType)&TrackerManagerImpl_InitTracker_TisTextTracker_t722_m42362/* method */
	, &TrackerManagerImpl_t788_il2cpp_TypeInfo/* declaring_type */
	, &TextTracker_t722_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, NULL/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, NULL/* native_delegate_wrapper */
	, &TrackerManagerImpl_InitTracker_TisTextTracker_t722_m42362_GenericMethod/* genericMethod */

};
