﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.QCARManagerImpl/AutoRotationState
struct AutoRotationState_t696 
{
	// System.Boolean Vuforia.QCARManagerImpl/AutoRotationState::setOnPause
	bool ___setOnPause_0;
	// System.Boolean Vuforia.QCARManagerImpl/AutoRotationState::autorotateToPortrait
	bool ___autorotateToPortrait_1;
	// System.Boolean Vuforia.QCARManagerImpl/AutoRotationState::autorotateToPortraitUpsideDown
	bool ___autorotateToPortraitUpsideDown_2;
	// System.Boolean Vuforia.QCARManagerImpl/AutoRotationState::autorotateToLandscapeLeft
	bool ___autorotateToLandscapeLeft_3;
	// System.Boolean Vuforia.QCARManagerImpl/AutoRotationState::autorotateToLandscapeRight
	bool ___autorotateToLandscapeRight_4;
};
// Native definition for marshalling of: Vuforia.QCARManagerImpl/AutoRotationState
struct AutoRotationState_t696_marshaled
{
	int32_t ___setOnPause_0;
	int32_t ___autorotateToPortrait_1;
	int32_t ___autorotateToPortraitUpsideDown_2;
	int32_t ___autorotateToLandscapeLeft_3;
	int32_t ___autorotateToLandscapeRight_4;
};
