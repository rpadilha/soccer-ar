﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.IntervalCollection/Enumerator
struct Enumerator_t1489;
// System.Object
struct Object_t;
// System.Collections.IList
struct IList_t1488;

// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::.ctor(System.Collections.IList)
 void Enumerator__ctor_m7532 (Enumerator_t1489 * __this, Object_t * ___list, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.RegularExpressions.IntervalCollection/Enumerator::get_Current()
 Object_t * Enumerator_get_Current_m7533 (Enumerator_t1489 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.IntervalCollection/Enumerator::MoveNext()
 bool Enumerator_MoveNext_m7534 (Enumerator_t1489 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::Reset()
 void Enumerator_Reset_m7535 (Enumerator_t1489 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
