﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.PointerInputModule/ButtonState
struct ButtonState_t299;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct Predicate_1_t3356  : public MulticastDelegate_t373
{
};
