﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/TrackableResultData
struct TrackableResultData_t684;
struct TrackableResultData_t684_marshaled;

void TrackableResultData_t684_marshal(const TrackableResultData_t684& unmarshaled, TrackableResultData_t684_marshaled& marshaled);
void TrackableResultData_t684_marshal_back(const TrackableResultData_t684_marshaled& marshaled, TrackableResultData_t684& unmarshaled);
void TrackableResultData_t684_marshal_cleanup(TrackableResultData_t684_marshaled& marshaled);
