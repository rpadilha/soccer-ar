﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_121.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadAbstractBehaviour>
struct CachedInvokableCall_1_t3909  : public InvokableCall_1_t3910
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
