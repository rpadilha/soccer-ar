﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t27;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t28;
// UnityEngine.GameObject
struct GameObject_t29;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t30;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t31;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t23;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t32;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t33;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t6;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t34;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t35;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t36;

// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
 void VuforiaBehaviourComponentFactory__ctor_m62 (VuforiaBehaviourComponentFactory_t27 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
 MaskOutAbstractBehaviour_t28 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m63 (VuforiaBehaviourComponentFactory_t27 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
 VirtualButtonAbstractBehaviour_t30 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m64 (VuforiaBehaviourComponentFactory_t27 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
 TurnOffAbstractBehaviour_t31 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m65 (VuforiaBehaviourComponentFactory_t27 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
 ImageTargetAbstractBehaviour_t23 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m66 (VuforiaBehaviourComponentFactory_t27 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
 MarkerAbstractBehaviour_t32 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m67 (VuforiaBehaviourComponentFactory_t27 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
 MultiTargetAbstractBehaviour_t33 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m68 (VuforiaBehaviourComponentFactory_t27 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
 CylinderTargetAbstractBehaviour_t6 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m69 (VuforiaBehaviourComponentFactory_t27 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
 WordAbstractBehaviour_t34 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m70 (VuforiaBehaviourComponentFactory_t27 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
 TextRecoAbstractBehaviour_t35 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m71 (VuforiaBehaviourComponentFactory_t27 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
 ObjectTargetAbstractBehaviour_t36 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m72 (VuforiaBehaviourComponentFactory_t27 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
