﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Color32
struct Color32_t463;
// System.String
struct String_t;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
 void Color32__ctor_m2273 (Color32_t463 * __this, uint8_t ___r, uint8_t ___g, uint8_t ___b, uint8_t ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Color32::ToString()
 String_t* Color32_ToString_m5964 (Color32_t463 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
 Color32_t463  Color32_op_Implicit_m2313 (Object_t * __this/* static, unused */, Color_t66  ___c, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
 Color_t66  Color32_op_Implicit_m2274 (Object_t * __this/* static, unused */, Color32_t463  ___c, MethodInfo* method) IL2CPP_METHOD_ATTR;
