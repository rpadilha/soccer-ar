﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t1406;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t1404;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1403;
// System.Security.Cryptography.Oid
struct Oid_t1405;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1407;
// System.Byte[]
struct ByteU5BU5D_t653;
// System.Security.Cryptography.DSA
struct DSA_t1408;
// System.Security.Cryptography.RSA
struct RSA_t1409;

// System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(Mono.Security.X509.X509Certificate)
 void PublicKey__ctor_m7106 (PublicKey_t1406 * __this, X509Certificate_t1407 * ___certificate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
 AsnEncodedData_t1404 * PublicKey_get_EncodedKeyValue_m7107 (PublicKey_t1406 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
 AsnEncodedData_t1404 * PublicKey_get_EncodedParameters_m7108 (PublicKey_t1406 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
 AsymmetricAlgorithm_t1403 * PublicKey_get_Key_m7109 (PublicKey_t1406 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
 Oid_t1405 * PublicKey_get_Oid_m7110 (PublicKey_t1406 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
 ByteU5BU5D_t653* PublicKey_GetUnsignedBigInteger_m7111 (Object_t * __this/* static, unused */, ByteU5BU5D_t653* ___integer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
 DSA_t1408 * PublicKey_DecodeDSA_m7112 (Object_t * __this/* static, unused */, ByteU5BU5D_t653* ___rawPublicKey, ByteU5BU5D_t653* ___rawParameters, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
 RSA_t1409 * PublicKey_DecodeRSA_m7113 (Object_t * __this/* static, unused */, ByteU5BU5D_t653* ___rawPublicKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
