﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Jump_Button
struct Jump_Button_t207;

// System.Void Jump_Button::.ctor()
 void Jump_Button__ctor_m731 (Jump_Button_t207 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jump_Button::Update()
 void Jump_Button_Update_m732 (Jump_Button_t207 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jump_Button::Main()
 void Jump_Button_Main_m733 (Jump_Button_t207 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
