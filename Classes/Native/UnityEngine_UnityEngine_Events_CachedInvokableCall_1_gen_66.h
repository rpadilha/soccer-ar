﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<SidescrollControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_62.h"
// UnityEngine.Events.CachedInvokableCall`1<SidescrollControl>
struct CachedInvokableCall_1_t3154  : public InvokableCall_1_t3155
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<SidescrollControl>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
