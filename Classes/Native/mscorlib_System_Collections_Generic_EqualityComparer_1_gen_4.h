﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Component>
struct EqualityComparer_1_t3226;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Component>
struct EqualityComparer_1_t3226  : public Object_t
{
};
struct EqualityComparer_1_t3226_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Component>::_default
	EqualityComparer_1_t3226 * ____default_0;
};
