﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct KeyCollection_t881;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t743;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3913;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.String[]
struct StringU5BU5D_t862;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_33.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_12MethodDeclarations.h"
#define KeyCollection__ctor_m24352(__this, ___dictionary, method) (void)KeyCollection__ctor_m18354_gshared((KeyCollection_t3456 *)__this, (Dictionary_2_t3452 *)___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m24353(__this, ___item, method) (void)KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18355_gshared((KeyCollection_t3456 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m24354(__this, method) (void)KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18356_gshared((KeyCollection_t3456 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m24355(__this, ___item, method) (bool)KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18357_gshared((KeyCollection_t3456 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m24356(__this, ___item, method) (bool)KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18358_gshared((KeyCollection_t3456 *)__this, (Object_t *)___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m24357(__this, method) (Object_t*)KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18359_gshared((KeyCollection_t3456 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m24358(__this, ___array, ___index, method) (void)KeyCollection_System_Collections_ICollection_CopyTo_m18360_gshared((KeyCollection_t3456 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m24359(__this, method) (Object_t *)KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18361_gshared((KeyCollection_t3456 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m24360(__this, method) (bool)KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18362_gshared((KeyCollection_t3456 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m24361(__this, method) (bool)KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18363_gshared((KeyCollection_t3456 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m24362(__this, method) (Object_t *)KeyCollection_System_Collections_ICollection_get_SyncRoot_m18364_gshared((KeyCollection_t3456 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m24363(__this, ___array, ___index, method) (void)KeyCollection_CopyTo_m18365_gshared((KeyCollection_t3456 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::GetEnumerator()
 Enumerator_t4199  KeyCollection_GetEnumerator_m24364 (KeyCollection_t881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Count()
#define KeyCollection_get_Count_m24365(__this, method) (int32_t)KeyCollection_get_Count_m18367_gshared((KeyCollection_t3456 *)__this, method)
