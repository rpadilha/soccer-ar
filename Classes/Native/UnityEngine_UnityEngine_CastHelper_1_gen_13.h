﻿#pragma once
#include <stdint.h>
// Joystick_Script
struct Joystick_Script_t102;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Joystick_Script>
struct CastHelper_1_t3083 
{
	// T UnityEngine.CastHelper`1<Joystick_Script>::t
	Joystick_Script_t102 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Joystick_Script>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
