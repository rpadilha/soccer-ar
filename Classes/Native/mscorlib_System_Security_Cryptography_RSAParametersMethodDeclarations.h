﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAParameters
struct RSAParameters_t1560;
struct RSAParameters_t1560_marshaled;

void RSAParameters_t1560_marshal(const RSAParameters_t1560& unmarshaled, RSAParameters_t1560_marshaled& marshaled);
void RSAParameters_t1560_marshal_back(const RSAParameters_t1560_marshaled& marshaled, RSAParameters_t1560& unmarshaled);
void RSAParameters_t1560_marshal_cleanup(RSAParameters_t1560_marshaled& marshaled);
