﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.TouchInputModule>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_73.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.TouchInputModule>
struct CachedInvokableCall_1_t3372  : public InvokableCall_1_t3373
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.TouchInputModule>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
