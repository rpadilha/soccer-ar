﻿#pragma once
#include <stdint.h>
// System.DBNull
struct DBNull_t2249;
// System.Object
#include "mscorlib_System_Object.h"
// System.DBNull
struct DBNull_t2249  : public Object_t
{
};
struct DBNull_t2249_StaticFields{
	// System.DBNull System.DBNull::Value
	DBNull_t2249 * ___Value_0;
};
