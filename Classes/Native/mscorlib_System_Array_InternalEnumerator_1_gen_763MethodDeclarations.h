﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Policy.IIdentityPermissionFactory>
struct InternalEnumerator_1_t5309;
// System.Object
struct Object_t;
// System.Security.Policy.IIdentityPermissionFactory
struct IIdentityPermissionFactory_t2705;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Security.Policy.IIdentityPermissionFactory>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m31917(__this, ___array, method) (void)InternalEnumerator_1__ctor_m14156_gshared((InternalEnumerator_1_t2752 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Security.Policy.IIdentityPermissionFactory>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31918(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Policy.IIdentityPermissionFactory>::Dispose()
#define InternalEnumerator_1_Dispose_m31919(__this, method) (void)InternalEnumerator_1_Dispose_m14160_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Policy.IIdentityPermissionFactory>::MoveNext()
#define InternalEnumerator_1_MoveNext_m31920(__this, method) (bool)InternalEnumerator_1_MoveNext_m14162_gshared((InternalEnumerator_1_t2752 *)__this, method)
// T System.Array/InternalEnumerator`1<System.Security.Policy.IIdentityPermissionFactory>::get_Current()
#define InternalEnumerator_1_get_Current_m31921(__this, method) (Object_t *)InternalEnumerator_1_get_Current_m14164_gshared((InternalEnumerator_1_t2752 *)__this, method)
