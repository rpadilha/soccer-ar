﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Interval_t1487_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
struct CostDelegate_t1490  : public MulticastDelegate_t373
{
};
