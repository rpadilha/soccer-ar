﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.WebCamTexture>
struct UnityAction_1_t4860;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>
struct InvokableCall_1_t4859  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>::Delegate
	UnityAction_1_t4860 * ___Delegate_0;
};
