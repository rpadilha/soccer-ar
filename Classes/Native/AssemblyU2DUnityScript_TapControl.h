﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t29;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.GUITexture
struct GUITexture_t100;
// ZoomCamera
struct ZoomCamera_t224;
// UnityEngine.Camera
struct Camera_t168;
// UnityEngine.CharacterController
struct CharacterController_t209;
// System.Int32[]
struct Int32U5BU5D_t175;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t225;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// ControlState
#include "AssemblyU2DUnityScript_ControlState.h"
// TapControl
struct TapControl_t226  : public MonoBehaviour_t10
{
	// UnityEngine.GameObject TapControl::cameraObject
	GameObject_t29 * ___cameraObject_2;
	// UnityEngine.Transform TapControl::cameraPivot
	Transform_t74 * ___cameraPivot_3;
	// UnityEngine.GUITexture TapControl::jumpButton
	GUITexture_t100 * ___jumpButton_4;
	// System.Single TapControl::speed
	float ___speed_5;
	// System.Single TapControl::jumpSpeed
	float ___jumpSpeed_6;
	// System.Single TapControl::inAirMultiplier
	float ___inAirMultiplier_7;
	// System.Single TapControl::minimumDistanceToMove
	float ___minimumDistanceToMove_8;
	// System.Single TapControl::minimumTimeUntilMove
	float ___minimumTimeUntilMove_9;
	// System.Boolean TapControl::zoomEnabled
	bool ___zoomEnabled_10;
	// System.Single TapControl::zoomEpsilon
	float ___zoomEpsilon_11;
	// System.Single TapControl::zoomRate
	float ___zoomRate_12;
	// System.Boolean TapControl::rotateEnabled
	bool ___rotateEnabled_13;
	// System.Single TapControl::rotateEpsilon
	float ___rotateEpsilon_14;
	// ZoomCamera TapControl::zoomCamera
	ZoomCamera_t224 * ___zoomCamera_15;
	// UnityEngine.Camera TapControl::cam
	Camera_t168 * ___cam_16;
	// UnityEngine.Transform TapControl::thisTransform
	Transform_t74 * ___thisTransform_17;
	// UnityEngine.CharacterController TapControl::character
	CharacterController_t209 * ___character_18;
	// UnityEngine.Vector3 TapControl::targetLocation
	Vector3_t73  ___targetLocation_19;
	// System.Boolean TapControl::moving
	bool ___moving_20;
	// System.Single TapControl::rotationTarget
	float ___rotationTarget_21;
	// System.Single TapControl::rotationVelocity
	float ___rotationVelocity_22;
	// UnityEngine.Vector3 TapControl::velocity
	Vector3_t73  ___velocity_23;
	// ControlState TapControl::state
	int32_t ___state_24;
	// System.Int32[] TapControl::fingerDown
	Int32U5BU5D_t175* ___fingerDown_25;
	// UnityEngine.Vector2[] TapControl::fingerDownPosition
	Vector2U5BU5D_t225* ___fingerDownPosition_26;
	// System.Int32[] TapControl::fingerDownFrame
	Int32U5BU5D_t175* ___fingerDownFrame_27;
	// System.Single TapControl::firstTouchTime
	float ___firstTouchTime_28;
};
