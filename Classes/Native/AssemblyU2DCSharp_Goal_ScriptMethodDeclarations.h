﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Goal_Script
struct Goal_Script_t86;
// UnityEngine.Collider
struct Collider_t70;

// System.Void Goal_Script::.ctor()
 void Goal_Script__ctor_m133 (Goal_Script_t86 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goal_Script::Start()
 void Goal_Script_Start_m134 (Goal_Script_t86 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goal_Script::Update()
 void Goal_Script_Update_m135 (Goal_Script_t86 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goal_Script::OnTriggerEnter(UnityEngine.Collider)
 void Goal_Script_OnTriggerEnter_m136 (Goal_Script_t86 * __this, Collider_t70 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Goal_Script::OnTriggerStay(UnityEngine.Collider)
 void Goal_Script_OnTriggerStay_m137 (Goal_Script_t86 * __this, Collider_t70 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
