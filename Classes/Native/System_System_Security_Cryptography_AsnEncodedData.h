﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.Oid
struct Oid_t1405;
// System.Byte[]
struct ByteU5BU5D_t653;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1019;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t1404  : public Object_t
{
	// System.Security.Cryptography.Oid System.Security.Cryptography.AsnEncodedData::_oid
	Oid_t1405 * ____oid_0;
	// System.Byte[] System.Security.Cryptography.AsnEncodedData::_raw
	ByteU5BU5D_t653* ____raw_1;
};
struct AsnEncodedData_t1404_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.AsnEncodedData::<>f__switch$mapA
	Dictionary_2_t1019 * ___U3CU3Ef__switch$mapA_2;
};
