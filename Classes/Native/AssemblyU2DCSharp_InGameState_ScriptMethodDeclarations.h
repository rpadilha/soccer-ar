﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InGameState_Script
struct InGameState_Script_t83;
// UnityEngine.GameObject
struct GameObject_t29;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t95;
// Player_Script/TypePlayer
#include "AssemblyU2DCSharp_Player_Script_TypePlayer.h"

// System.Void InGameState_Script::.ctor()
 void InGameState_Script__ctor_m153 (InGameState_Script_t83 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InGameState_Script::Awake()
 void InGameState_Script_Awake_m154 (InGameState_Script_t83 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InGameState_Script::Start()
 void InGameState_Script_Start_m155 (InGameState_Script_t83 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InGameState_Script::LoadTeams()
 void InGameState_Script_LoadTeams_m156 (InGameState_Script_t83 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InGameState_Script::Update()
 void InGameState_Script_Update_m157 (InGameState_Script_t83 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject InGameState_Script::SearchPlayerNearBall(UnityEngine.GameObject[])
 GameObject_t29 * InGameState_Script_SearchPlayerNearBall_m158 (InGameState_Script_t83 * __this, GameObjectU5BU5D_t95* ___arrayPlayers, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InGameState_Script::PutPlayersInCornerArea(UnityEngine.GameObject[],Player_Script/TypePlayer)
 void InGameState_Script_PutPlayersInCornerArea_m159 (InGameState_Script_t83 * __this, GameObjectU5BU5D_t95* ___arrayPlayers, int32_t ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
