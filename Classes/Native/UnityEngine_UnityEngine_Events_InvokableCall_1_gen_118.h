﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
struct UnityAction_1_t3889;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
struct InvokableCall_1_t3888  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Delegate
	UnityAction_1_t3889 * ___Delegate_0;
};
