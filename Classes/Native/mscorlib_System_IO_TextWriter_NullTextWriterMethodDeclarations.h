﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.TextWriter/NullTextWriter
struct NullTextWriter_t1945;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t378;

// System.Void System.IO.TextWriter/NullTextWriter::.ctor()
 void NullTextWriter__ctor_m11073 (NullTextWriter_t1945 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter/NullTextWriter::Write(System.String)
 void NullTextWriter_Write_m11074 (NullTextWriter_t1945 * __this, String_t* ___s, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter/NullTextWriter::Write(System.Char)
 void NullTextWriter_Write_m11075 (NullTextWriter_t1945 * __this, uint16_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter/NullTextWriter::Write(System.Char[],System.Int32,System.Int32)
 void NullTextWriter_Write_m11076 (NullTextWriter_t1945 * __this, CharU5BU5D_t378* ___value, int32_t ___index, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
