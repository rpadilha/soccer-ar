﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Syntax.ExpressionCollection
struct ExpressionCollection_t1498;
// System.Text.RegularExpressions.Syntax.Expression
struct Expression_t1496;
// System.Object
struct Object_t;

// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::.ctor()
 void ExpressionCollection__ctor_m7589 (ExpressionCollection_t1498 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::Add(System.Text.RegularExpressions.Syntax.Expression)
 void ExpressionCollection_Add_m7590 (ExpressionCollection_t1498 * __this, Expression_t1496 * ___e, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionCollection::get_Item(System.Int32)
 Expression_t1496 * ExpressionCollection_get_Item_m7591 (ExpressionCollection_t1498 * __this, int32_t ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::set_Item(System.Int32,System.Text.RegularExpressions.Syntax.Expression)
 void ExpressionCollection_set_Item_m7592 (ExpressionCollection_t1498 * __this, int32_t ___i, Expression_t1496 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::OnValidate(System.Object)
 void ExpressionCollection_OnValidate_m7593 (ExpressionCollection_t1498 * __this, Object_t * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
