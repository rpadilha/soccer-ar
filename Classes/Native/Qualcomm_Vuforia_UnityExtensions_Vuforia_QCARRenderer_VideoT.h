﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"
// Vuforia.QCARRenderer/VideoTextureInfo
#pragma pack(push, tp, 1)
struct VideoTextureInfo_t588 
{
	// Vuforia.QCARRenderer/Vec2I Vuforia.QCARRenderer/VideoTextureInfo::textureSize
	Vec2I_t675  ___textureSize_0;
	// Vuforia.QCARRenderer/Vec2I Vuforia.QCARRenderer/VideoTextureInfo::imageSize
	Vec2I_t675  ___imageSize_1;
};
#pragma pack(pop, tp)
