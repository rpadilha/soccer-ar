﻿#pragma once
#include <stdint.h>
// Vuforia.Surface[]
struct SurfaceU5BU5D_t4253;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.Surface>
struct List_1_t767  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Surface>::_items
	SurfaceU5BU5D_t4253* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::_version
	int32_t ____version_3;
};
struct List_1_t767_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.Surface>::EmptyArray
	SurfaceU5BU5D_t4253* ___EmptyArray_4;
};
