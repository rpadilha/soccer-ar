﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Sphere>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_52.h"
// UnityEngine.Events.CachedInvokableCall`1<Sphere>
struct CachedInvokableCall_1_t3106  : public InvokableCall_1_t3107
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Sphere>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
