﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_7.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>
struct CachedInvokableCall_1_t2808  : public InvokableCall_1_t2809
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
