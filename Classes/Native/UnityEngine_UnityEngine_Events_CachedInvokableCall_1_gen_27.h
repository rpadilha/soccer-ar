﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_23.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>
struct CachedInvokableCall_1_t2945  : public InvokableCall_1_t2946
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
