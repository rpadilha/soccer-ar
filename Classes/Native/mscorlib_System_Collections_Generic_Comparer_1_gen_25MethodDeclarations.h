﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparer_1_t3971;
// System.Object
struct Object_t;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
 void Comparer_1__ctor_m22031 (Comparer_1_t3971 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
 void Comparer_1__cctor_m22032 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IComparer.Compare(System.Object,System.Object)
 int32_t Comparer_1_System_Collections_IComparer_Compare_m22033 (Comparer_1_t3971 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::get_Default()
 Comparer_1_t3971 * Comparer_1_get_Default_m22034 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
