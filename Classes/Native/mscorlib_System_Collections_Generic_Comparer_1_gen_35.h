﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.WordAbstractBehaviour>
struct Comparer_1_t4189;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.WordAbstractBehaviour>
struct Comparer_1_t4189  : public Object_t
{
};
struct Comparer_1_t4189_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.WordAbstractBehaviour>::_default
	Comparer_1_t4189 * ____default_0;
};
