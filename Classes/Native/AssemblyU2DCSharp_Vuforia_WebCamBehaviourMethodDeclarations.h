﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamBehaviour
struct WebCamBehaviour_t62;

// System.Void Vuforia.WebCamBehaviour::.ctor()
 void WebCamBehaviour__ctor_m95 (WebCamBehaviour_t62 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
