﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t64;
// ShieldMenu[]
struct ShieldMenuU5BU5D_t87;
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ChooseTeam
struct ChooseTeam_t88  : public MonoBehaviour_t10
{
	// UnityEngine.Material ChooseTeam::normalMaterial
	Material_t64 * ___normalMaterial_2;
	// UnityEngine.Material ChooseTeam::selectMaterial
	Material_t64 * ___selectMaterial_3;
	// ShieldMenu[] ChooseTeam::shields
	ShieldMenuU5BU5D_t87* ___shields_4;
	// System.String ChooseTeam::Selected
	String_t* ___Selected_5;
	// System.String ChooseTeam::localOrVisit
	String_t* ___localOrVisit_6;
};
