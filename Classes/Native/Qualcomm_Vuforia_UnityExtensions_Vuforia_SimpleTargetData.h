﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.SimpleTargetData
#pragma pack(push, tp, 1)
struct SimpleTargetData_t810 
{
	// System.Int32 Vuforia.SimpleTargetData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.SimpleTargetData::unused
	int32_t ___unused_1;
};
#pragma pack(pop, tp)
