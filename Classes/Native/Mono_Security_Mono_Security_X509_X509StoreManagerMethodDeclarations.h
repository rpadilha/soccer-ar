﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.X509StoreManager
struct X509StoreManager_t1626;
// Mono.Security.X509.X509Stores
struct X509Stores_t1445;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1569;

// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_CurrentUser()
 X509Stores_t1445 * X509StoreManager_get_CurrentUser_m8041 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_LocalMachine()
 X509Stores_t1445 * X509StoreManager_get_LocalMachine_m8042 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_TrustedRootCertificates()
 X509CertificateCollection_t1569 * X509StoreManager_get_TrustedRootCertificates_m8415 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
