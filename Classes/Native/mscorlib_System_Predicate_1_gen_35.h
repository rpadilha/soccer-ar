﻿#pragma once
#include <stdint.h>
// Vuforia.WordResult
struct WordResult_t747;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.WordResult>
struct Predicate_1_t4153  : public MulticastDelegate_t373
{
};
