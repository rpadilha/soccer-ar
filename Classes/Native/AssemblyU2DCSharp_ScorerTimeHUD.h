﻿#pragma once
#include <stdint.h>
// InGameState_Script
struct InGameState_Script_t83;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ScorerTimeHUD
struct ScorerTimeHUD_t90  : public MonoBehaviour_t10
{
	// System.Single ScorerTimeHUD::timeMatch
	float ___timeMatch_2;
	// System.Int32 ScorerTimeHUD::minutes
	int32_t ___minutes_3;
	// System.Int32 ScorerTimeHUD::seconds
	int32_t ___seconds_4;
	// System.Single ScorerTimeHUD::TRANSFORM_TIME
	float ___TRANSFORM_TIME_5;
	// InGameState_Script ScorerTimeHUD::inGame
	InGameState_Script_t83 * ___inGame_6;
};
