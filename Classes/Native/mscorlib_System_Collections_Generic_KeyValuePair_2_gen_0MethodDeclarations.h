﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>
struct KeyValuePair_2_t878;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t34;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m24091 (KeyValuePair_2_t878 * __this, int32_t ___key, WordAbstractBehaviour_t34 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::get_Key()
 int32_t KeyValuePair_2_get_Key_m5178 (KeyValuePair_2_t878 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m24092 (KeyValuePair_2_t878 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::get_Value()
 WordAbstractBehaviour_t34 * KeyValuePair_2_get_Value_m5181 (KeyValuePair_2_t878 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m24093 (KeyValuePair_2_t878 * __this, WordAbstractBehaviour_t34 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::ToString()
 String_t* KeyValuePair_2_ToString_m24094 (KeyValuePair_2_t878 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
