﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.ChannelInfo
struct ChannelInfo_t2044  : public Object_t
{
	// System.Object[] System.Runtime.Remoting.ChannelInfo::channelData
	ObjectU5BU5D_t130* ___channelData_0;
};
