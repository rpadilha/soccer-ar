﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MonoGenericMethod
struct MonoGenericMethod_t1990;
// System.Type
struct Type_t;

// System.Void System.Reflection.MonoGenericMethod::.ctor()
 void MonoGenericMethod__ctor_m11457 (MonoGenericMethod_t1990 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericMethod::get_ReflectedType()
 Type_t * MonoGenericMethod_get_ReflectedType_m11458 (MonoGenericMethod_t1990 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
