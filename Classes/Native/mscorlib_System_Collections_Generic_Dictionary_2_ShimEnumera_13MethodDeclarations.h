﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordAbstractBehaviour>
struct ShimEnumerator_t4180;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.WordAbstractBehaviour>
struct Dictionary_2_t741;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m24161 (ShimEnumerator_t4180 * __this, Dictionary_2_t741 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordAbstractBehaviour>::MoveNext()
 bool ShimEnumerator_MoveNext_m24162 (ShimEnumerator_t4180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordAbstractBehaviour>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m24163 (ShimEnumerator_t4180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordAbstractBehaviour>::get_Key()
 Object_t * ShimEnumerator_get_Key_m24164 (ShimEnumerator_t4180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordAbstractBehaviour>::get_Value()
 Object_t * ShimEnumerator_get_Value_m24165 (ShimEnumerator_t4180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordAbstractBehaviour>::get_Current()
 Object_t * ShimEnumerator_get_Current_m24166 (ShimEnumerator_t4180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
