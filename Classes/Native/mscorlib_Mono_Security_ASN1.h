﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t653;
// System.Collections.ArrayList
struct ArrayList_t1361;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Security.ASN1
struct ASN1_t1826  : public Object_t
{
	// System.Byte Mono.Security.ASN1::m_nTag
	uint8_t ___m_nTag_0;
	// System.Byte[] Mono.Security.ASN1::m_aValue
	ByteU5BU5D_t653* ___m_aValue_1;
	// System.Collections.ArrayList Mono.Security.ASN1::elist
	ArrayList_t1361 * ___elist_2;
};
