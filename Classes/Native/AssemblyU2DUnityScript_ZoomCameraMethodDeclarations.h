﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ZoomCamera
struct ZoomCamera_t224;

// System.Void ZoomCamera::.ctor()
 void ZoomCamera__ctor_m796 (ZoomCamera_t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomCamera::Start()
 void ZoomCamera_Start_m797 (ZoomCamera_t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomCamera::Update()
 void ZoomCamera_Update_m798 (ZoomCamera_t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomCamera::Main()
 void ZoomCamera_Main_m799 (ZoomCamera_t224 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
