﻿#pragma once
#include <stdint.h>
// Vuforia.Trackable[]
struct TrackableU5BU5D_t3973;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.Trackable>
struct List_1_t834  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Trackable>::_items
	TrackableU5BU5D_t3973* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Trackable>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Trackable>::_version
	int32_t ____version_3;
};
struct List_1_t834_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Trackable>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.Trackable>::EmptyArray
	TrackableU5BU5D_t3973* ___EmptyArray_4;
};
