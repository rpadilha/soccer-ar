﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
struct CachedInvokableCall_1_t2759  : public InvokableCall_1_t2760
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
