﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Goal_Script
struct Goal_Script_t86;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Goal_Script>
struct UnityAction_1_t3035  : public MulticastDelegate_t373
{
};
