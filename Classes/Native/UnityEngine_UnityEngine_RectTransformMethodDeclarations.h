﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RectTransform
struct RectTransform_t338;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t558;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t85;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"
// UnityEngine.RectTransform/Axis
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"

// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
 void RectTransform_add_reapplyDrivenProperties_m2698 (Object_t * __this/* static, unused */, ReapplyDrivenProperties_t558 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
 void RectTransform_remove_reapplyDrivenProperties_m6051 (Object_t * __this/* static, unused */, ReapplyDrivenProperties_t558 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
 void RectTransform_INTERNAL_get_rect_m6052 (RectTransform_t338 * __this, Rect_t103 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
 Rect_t103  RectTransform_get_rect_m2308 (RectTransform_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_get_anchorMin_m6053 (RectTransform_t338 * __this, Vector2_t99 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_set_anchorMin_m6054 (RectTransform_t338 * __this, Vector2_t99 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
 Vector2_t99  RectTransform_get_anchorMin_m2383 (RectTransform_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
 void RectTransform_set_anchorMin_m2508 (RectTransform_t338 * __this, Vector2_t99  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_get_anchorMax_m6055 (RectTransform_t338 * __this, Vector2_t99 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_set_anchorMax_m6056 (RectTransform_t338 * __this, Vector2_t99 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
 Vector2_t99  RectTransform_get_anchorMax_m2503 (RectTransform_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
 void RectTransform_set_anchorMax_m2384 (RectTransform_t338 * __this, Vector2_t99  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_get_anchoredPosition_m6057 (RectTransform_t338 * __this, Vector2_t99 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_set_anchoredPosition_m6058 (RectTransform_t338 * __this, Vector2_t99 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
 Vector2_t99  RectTransform_get_anchoredPosition_m2504 (RectTransform_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
 void RectTransform_set_anchoredPosition_m2509 (RectTransform_t338 * __this, Vector2_t99  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_get_sizeDelta_m6059 (RectTransform_t338 * __this, Vector2_t99 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_set_sizeDelta_m6060 (RectTransform_t338 * __this, Vector2_t99 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
 Vector2_t99  RectTransform_get_sizeDelta_m2505 (RectTransform_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
 void RectTransform_set_sizeDelta_m2385 (RectTransform_t338 * __this, Vector2_t99  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_get_pivot_m6061 (RectTransform_t338 * __this, Vector2_t99 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_set_pivot_m6062 (RectTransform_t338 * __this, Vector2_t99 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
 Vector2_t99  RectTransform_get_pivot_m2381 (RectTransform_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
 void RectTransform_set_pivot_m2510 (RectTransform_t338 * __this, Vector2_t99  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
 void RectTransform_SendReapplyDrivenProperties_m6063 (Object_t * __this/* static, unused */, RectTransform_t338 * ___driven, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
 void RectTransform_GetLocalCorners_m6064 (RectTransform_t338 * __this, Vector3U5BU5D_t85* ___fourCornersArray, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
 void RectTransform_GetWorldCorners_m2567 (RectTransform_t338 * __this, Vector3U5BU5D_t85* ___fourCornersArray, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
 void RectTransform_SetInsetAndSizeFromParentEdge_m2694 (RectTransform_t338 * __this, int32_t ___edge, float ___inset, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
 void RectTransform_SetSizeWithCurrentAnchors_m2647 (RectTransform_t338 * __this, int32_t ___axis, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
 Vector2_t99  RectTransform_GetParentSize_m6065 (RectTransform_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
