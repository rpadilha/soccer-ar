﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Boundary
struct Boundary_t213  : public Object_t
{
	// UnityEngine.Vector2 Boundary::min
	Vector2_t99  ___min_0;
	// UnityEngine.Vector2 Boundary::max
	Vector2_t99  ___max_1;
};
