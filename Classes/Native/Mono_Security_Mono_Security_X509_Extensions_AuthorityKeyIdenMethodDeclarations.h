﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
struct AuthorityKeyIdentifierExtension_t1571;
// System.Byte[]
struct ByteU5BU5D_t653;
// Mono.Security.X509.X509Extension
struct X509Extension_t1431;
// System.String
struct String_t;

// System.Void Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::.ctor(Mono.Security.X509.X509Extension)
 void AuthorityKeyIdentifierExtension__ctor_m8021 (AuthorityKeyIdentifierExtension_t1571 * __this, X509Extension_t1431 * ___extension, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::Decode()
 void AuthorityKeyIdentifierExtension_Decode_m8418 (AuthorityKeyIdentifierExtension_t1571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::get_Identifier()
 ByteU5BU5D_t653* AuthorityKeyIdentifierExtension_get_Identifier_m8022 (AuthorityKeyIdentifierExtension_t1571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::ToString()
 String_t* AuthorityKeyIdentifierExtension_ToString_m8419 (AuthorityKeyIdentifierExtension_t1571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
