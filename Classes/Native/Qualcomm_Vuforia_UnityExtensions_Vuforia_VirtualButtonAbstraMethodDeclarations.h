﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t30;
// System.String
struct String_t;
// Vuforia.VirtualButton
struct VirtualButton_t639;
// UnityEngine.GameObject
struct GameObject_t29;
// Vuforia.IVirtualButtonEventHandler
struct IVirtualButtonEventHandler_t815;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t23;
// UnityEngine.Renderer
struct Renderer_t129;
// UnityEngine.Transform
struct Transform_t74;
// Vuforia.VirtualButton/Sensitivity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.String Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButtonName()
 String_t* VirtualButtonAbstractBehaviour_get_VirtualButtonName_m495 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_Pressed()
 bool VirtualButtonAbstractBehaviour_get_Pressed_m4382 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_HasUpdatedPose()
 bool VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m507 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_UnregisterOnDestroy()
 bool VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m505 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonAbstractBehaviour::set_UnregisterOnDestroy(System.Boolean)
 void VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m506 (VirtualButtonAbstractBehaviour_t30 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButton Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButton()
 VirtualButton_t639 * VirtualButtonAbstractBehaviour_get_VirtualButton_m4383 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonAbstractBehaviour::.ctor()
 void VirtualButtonAbstractBehaviour__ctor_m494 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonAbstractBehaviour::RegisterEventHandler(Vuforia.IVirtualButtonEventHandler)
 void VirtualButtonAbstractBehaviour_RegisterEventHandler_m4384 (VirtualButtonAbstractBehaviour_t30 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UnregisterEventHandler(Vuforia.IVirtualButtonEventHandler)
 bool VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4385 (VirtualButtonAbstractBehaviour_t30 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::CalculateButtonArea(UnityEngine.Vector2&,UnityEngine.Vector2&)
 bool VirtualButtonAbstractBehaviour_CalculateButtonArea_m4386 (VirtualButtonAbstractBehaviour_t30 * __this, Vector2_t99 * ___topLeft, Vector2_t99 * ___bottomRight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateAreaRectangle()
 bool VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4387 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateSensitivity()
 bool VirtualButtonAbstractBehaviour_UpdateSensitivity_m4388 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateEnabled()
 bool VirtualButtonAbstractBehaviour_UpdateEnabled_m4389 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdatePose()
 bool VirtualButtonAbstractBehaviour_UpdatePose_m508 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnTrackerUpdated(System.Boolean)
 void VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4390 (VirtualButtonAbstractBehaviour_t30 * __this, bool ___pressed, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VirtualButtonAbstractBehaviour::GetImageTargetBehaviour()
 ImageTargetAbstractBehaviour_t23 * VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetVirtualButtonName(System.String)
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m496 (VirtualButtonAbstractBehaviour_t30 * __this, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_SensitivitySetting()
 int32_t VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m497 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetSensitivitySetting(Vuforia.VirtualButton/Sensitivity)
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m498 (VirtualButtonAbstractBehaviour_t30 * __this, int32_t ___sensibility, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousTransform()
 Matrix4x4_t176  VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m499 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousTransform(UnityEngine.Matrix4x4)
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m500 (VirtualButtonAbstractBehaviour_t30 * __this, Matrix4x4_t176  ___transformMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousParent()
 GameObject_t29 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m501 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousParent(UnityEngine.GameObject)
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m502 (VirtualButtonAbstractBehaviour_t30 * __this, GameObject_t29 * ___parent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.InitializeVirtualButton(Vuforia.VirtualButton)
 void VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m503 (VirtualButtonAbstractBehaviour_t30 * __this, VirtualButton_t639 * ___virtualButton, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPosAndScaleFromButtonArea(UnityEngine.Vector2,UnityEngine.Vector2)
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m504 (VirtualButtonAbstractBehaviour_t30 * __this, Vector2_t99  ___topLeft, Vector2_t99  ___bottomRight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Renderer Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.GetRenderer()
 Renderer_t129 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m513 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonAbstractBehaviour::LateUpdate()
 void VirtualButtonAbstractBehaviour_LateUpdate_m4392 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDisable()
 void VirtualButtonAbstractBehaviour_OnDisable_m4393 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDestroy()
 void VirtualButtonAbstractBehaviour_OnDestroy_m4394 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Equals(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
 bool VirtualButtonAbstractBehaviour_Equals_m4395 (Object_t * __this/* static, unused */, Vector2_t99  ___vec1, Vector2_t99  ___vec2, float ___threshold, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_enabled()
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m509 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.set_enabled(System.Boolean)
 void VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m510 (VirtualButtonAbstractBehaviour_t30 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_transform()
 Transform_t74 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m511 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_gameObject()
 GameObject_t29 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m512 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
