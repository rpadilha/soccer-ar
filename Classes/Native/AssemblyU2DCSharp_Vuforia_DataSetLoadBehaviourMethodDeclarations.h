﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DataSetLoadBehaviour
struct DataSetLoadBehaviour_t7;

// System.Void Vuforia.DataSetLoadBehaviour::.ctor()
 void DataSetLoadBehaviour__ctor_m3 (DataSetLoadBehaviour_t7 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
 void DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m4 (DataSetLoadBehaviour_t7 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
