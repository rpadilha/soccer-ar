﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>
struct UnityAction_1_t2942;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>
struct InvokableCall_1_t2941  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::Delegate
	UnityAction_1_t2942 * ___Delegate_0;
};
