﻿#pragma once
#include <stdint.h>
// Vuforia.ISmartTerrainEventHandler[]
struct ISmartTerrainEventHandlerU5BU5D_t4239;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>
struct List_1_t757  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::_items
	ISmartTerrainEventHandlerU5BU5D_t4239* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t757_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::EmptyArray
	ISmartTerrainEventHandlerU5BU5D_t4239* ___EmptyArray_4;
};
