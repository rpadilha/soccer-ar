﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>
struct InternalEnumerator_1_t4141;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m23694 (InternalEnumerator_1_t4141 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23695 (InternalEnumerator_1_t4141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>::Dispose()
 void InternalEnumerator_1_Dispose_m23696 (InternalEnumerator_1_t4141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m23697 (InternalEnumerator_1_t4141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>::get_Current()
 KeyValuePair_2_t4138  InternalEnumerator_1_get_Current_m23698 (InternalEnumerator_1_t4141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
