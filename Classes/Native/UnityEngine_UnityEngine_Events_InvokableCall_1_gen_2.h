﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>
struct UnityAction_1_t2780;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>
struct InvokableCall_1_t2779  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::Delegate
	UnityAction_1_t2780 * ___Delegate_0;
};
