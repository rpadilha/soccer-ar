﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_94.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo InternalEnumerator_1_t2955_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_94MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBu.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Array
#include "mscorlib_System_Array.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m15188_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUserDefinedTargetBuildingAbstractBehaviour_t56_m33336_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisUserDefinedTargetBuildingAbstractBehaviour_t56_m33336(__this, p0, method) (UserDefinedTargetBuildingAbstractBehaviour_t56 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2955____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2955, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2955____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2955, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2955_FieldInfos[] =
{
	&InternalEnumerator_1_t2955____array_0_FieldInfo,
	&InternalEnumerator_1_t2955____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15185_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2955____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2955_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15185_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2955____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2955_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15188_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2955_PropertyInfos[] =
{
	&InternalEnumerator_1_t2955____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2955____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2955_InternalEnumerator_1__ctor_m15184_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15184_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15184_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2955_InternalEnumerator_1__ctor_m15184_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15184_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15185_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15185_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15185_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15186_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15186_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15186_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15187_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15187_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15187_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15188_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15188_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15188_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2955_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15184_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15185_MethodInfo,
	&InternalEnumerator_1_Dispose_m15186_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15187_MethodInfo,
	&InternalEnumerator_1_get_Current_m15188_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m15187_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15186_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2955_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15185_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15187_MethodInfo,
	&InternalEnumerator_1_Dispose_m15186_MethodInfo,
	&InternalEnumerator_1_get_Current_m15188_MethodInfo,
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t6106_il2cpp_TypeInfo;
static TypeInfo* InternalEnumerator_1_t2955_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6106_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2955_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6106_il2cpp_TypeInfo, 7},
};
extern TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2955_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15188_MethodInfo/* Method Usage */,
	&UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisUserDefinedTargetBuildingAbstractBehaviour_t56_m33336_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2955_0_0_0;
extern Il2CppType InternalEnumerator_1_t2955_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2955_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2955_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2955_MethodInfos/* methods */
	, InternalEnumerator_1_t2955_PropertyInfos/* properties */
	, InternalEnumerator_1_t2955_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2955_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2955_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2955_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2955_1_0_0/* this_arg */
	, InternalEnumerator_1_t2955_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2955_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2955_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2955)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7791_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43699_MethodInfo;
extern MethodInfo IList_1_set_Item_m43700_MethodInfo;
static PropertyInfo IList_1_t7791____Item_PropertyInfo = 
{
	&IList_1_t7791_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43699_MethodInfo/* get */
	, &IList_1_set_Item_m43700_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7791_PropertyInfos[] =
{
	&IList_1_t7791____Item_PropertyInfo,
	NULL
};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
static ParameterInfo IList_1_t7791_IList_1_IndexOf_m43701_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43701_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43701_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7791_IList_1_IndexOf_m43701_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43701_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
static ParameterInfo IList_1_t7791_IList_1_Insert_m43702_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43702_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43702_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7791_IList_1_Insert_m43702_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43702_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7791_IList_1_RemoveAt_m43703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43703_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43703_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7791_IList_1_RemoveAt_m43703_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43703_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7791_IList_1_get_Item_m43699_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43699_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43699_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7791_IList_1_get_Item_m43699_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43699_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
static ParameterInfo IList_1_t7791_IList_1_set_Item_m43700_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43700_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43700_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7791_IList_1_set_Item_m43700_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43700_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7791_MethodInfos[] =
{
	&IList_1_IndexOf_m43701_MethodInfo,
	&IList_1_Insert_m43702_MethodInfo,
	&IList_1_RemoveAt_m43703_MethodInfo,
	&IList_1_get_Item_m43699_MethodInfo,
	&IList_1_set_Item_m43700_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7790_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7792_il2cpp_TypeInfo;
static TypeInfo* IList_1_t7791_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7790_il2cpp_TypeInfo,
	&IEnumerable_1_t7792_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7791_0_0_0;
extern Il2CppType IList_1_t7791_1_0_0;
struct IList_1_t7791;
extern Il2CppGenericClass IList_1_t7791_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7791_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7791_MethodInfos/* methods */
	, IList_1_t7791_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7791_il2cpp_TypeInfo/* element_class */
	, IList_1_t7791_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7791_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7791_0_0_0/* byval_arg */
	, &IList_1_t7791_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7791_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_29.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2956_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_29MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_25.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2957_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_25MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15191_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15193_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2956____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2956_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2956, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2956_FieldInfos[] =
{
	&CachedInvokableCall_1_t2956____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2956_CachedInvokableCall_1__ctor_m15189_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t55_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15189_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15189_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2956_CachedInvokableCall_1__ctor_m15189_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15189_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2956_CachedInvokableCall_1_Invoke_m15190_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15190_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15190_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2956_CachedInvokableCall_1_Invoke_m15190_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15190_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2956_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15189_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15190_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m15190_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15194_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2956_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15190_MethodInfo,
	&InvokableCall_1_Find_m15194_MethodInfo,
};
extern Il2CppType UnityAction_1_t2958_0_0_0;
extern TypeInfo UnityAction_1_t2958_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisUserDefinedTargetBuildingBehaviour_t55_m33346_MethodInfo;
extern TypeInfo UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15196_MethodInfo;
extern TypeInfo UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2956_RGCTXData[8] = 
{
	&UnityAction_1_t2958_0_0_0/* Type Usage */,
	&UnityAction_1_t2958_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisUserDefinedTargetBuildingBehaviour_t55_m33346_MethodInfo/* Method Usage */,
	&UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15196_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15191_MethodInfo/* Method Usage */,
	&UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15193_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2956_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2956_1_0_0;
struct CachedInvokableCall_1_t2956;
extern Il2CppGenericClass CachedInvokableCall_1_t2956_GenericClass;
TypeInfo CachedInvokableCall_1_t2956_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2956_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2956_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2957_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2956_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2956_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2956_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2956_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2956_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2956_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2956_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2956)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_32.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t2958_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_32MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.UserDefinedTargetBuildingBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.UserDefinedTargetBuildingBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisUserDefinedTargetBuildingBehaviour_t55_m33346(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern Il2CppType UnityAction_1_t2958_0_0_1;
FieldInfo InvokableCall_1_t2957____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2958_0_0_1/* type */
	, &InvokableCall_1_t2957_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2957, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2957_FieldInfos[] =
{
	&InvokableCall_1_t2957____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2957_InvokableCall_1__ctor_m15191_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15191_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15191_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2957_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2957_InvokableCall_1__ctor_m15191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15191_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2958_0_0_0;
static ParameterInfo InvokableCall_1_t2957_InvokableCall_1__ctor_m15192_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2958_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15192_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15192_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2957_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2957_InvokableCall_1__ctor_m15192_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15192_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2957_InvokableCall_1_Invoke_m15193_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15193_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15193_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2957_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2957_InvokableCall_1_Invoke_m15193_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15193_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2957_InvokableCall_1_Find_m15194_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15194_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15194_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2957_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2957_InvokableCall_1_Find_m15194_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15194_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2957_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15191_MethodInfo,
	&InvokableCall_1__ctor_m15192_MethodInfo,
	&InvokableCall_1_Invoke_m15193_MethodInfo,
	&InvokableCall_1_Find_m15194_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2957_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15193_MethodInfo,
	&InvokableCall_1_Find_m15194_MethodInfo,
};
extern TypeInfo UnityAction_1_t2958_il2cpp_TypeInfo;
extern TypeInfo UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2957_RGCTXData[5] = 
{
	&UnityAction_1_t2958_0_0_0/* Type Usage */,
	&UnityAction_1_t2958_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisUserDefinedTargetBuildingBehaviour_t55_m33346_MethodInfo/* Method Usage */,
	&UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15196_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2957_0_0_0;
extern Il2CppType InvokableCall_1_t2957_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t2957;
extern Il2CppGenericClass InvokableCall_1_t2957_GenericClass;
TypeInfo InvokableCall_1_t2957_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2957_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2957_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2957_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2957_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2957_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2957_0_0_0/* byval_arg */
	, &InvokableCall_1_t2957_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2957_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2957_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2957)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2958_UnityAction_1__ctor_m15195_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15195_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15195_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2958_UnityAction_1__ctor_m15195_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15195_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
static ParameterInfo UnityAction_1_t2958_UnityAction_1_Invoke_m15196_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t55_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15196_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15196_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2958_UnityAction_1_Invoke_m15196_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15196_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t55_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2958_UnityAction_1_BeginInvoke_m15197_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t55_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15197_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15197_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2958_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2958_UnityAction_1_BeginInvoke_m15197_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15197_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2958_UnityAction_1_EndInvoke_m15198_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15198_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15198_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2958_UnityAction_1_EndInvoke_m15198_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15198_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2958_MethodInfos[] =
{
	&UnityAction_1__ctor_m15195_MethodInfo,
	&UnityAction_1_Invoke_m15196_MethodInfo,
	&UnityAction_1_BeginInvoke_m15197_MethodInfo,
	&UnityAction_1_EndInvoke_m15198_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m15197_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15198_MethodInfo;
static MethodInfo* UnityAction_1_t2958_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15196_MethodInfo,
	&UnityAction_1_BeginInvoke_m15197_MethodInfo,
	&UnityAction_1_EndInvoke_m15198_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2958_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2958_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_1_t2958;
extern Il2CppGenericClass UnityAction_1_t2958_GenericClass;
TypeInfo UnityAction_1_t2958_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2958_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2958_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2958_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2958_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2958_0_0_0/* byval_arg */
	, &UnityAction_1_t2958_1_0_0/* this_arg */
	, UnityAction_1_t2958_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2958_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2958)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6108_il2cpp_TypeInfo;

// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43704_MethodInfo;
static PropertyInfo IEnumerator_1_t6108____Current_PropertyInfo = 
{
	&IEnumerator_1_t6108_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43704_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6108_PropertyInfos[] =
{
	&IEnumerator_1_t6108____Current_PropertyInfo,
	NULL
};
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43704_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43704_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6108_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundBehaviour_t57_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43704_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6108_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43704_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6108_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6108_0_0_0;
extern Il2CppType IEnumerator_1_t6108_1_0_0;
struct IEnumerator_1_t6108;
extern Il2CppGenericClass IEnumerator_1_t6108_GenericClass;
TypeInfo IEnumerator_1_t6108_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6108_MethodInfos/* methods */
	, IEnumerator_1_t6108_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6108_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6108_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6108_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6108_0_0_0/* byval_arg */
	, &IEnumerator_1_t6108_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6108_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_95.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2959_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_95MethodDeclarations.h"

extern TypeInfo VideoBackgroundBehaviour_t57_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15203_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVideoBackgroundBehaviour_t57_m33348_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VideoBackgroundBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VideoBackgroundBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisVideoBackgroundBehaviour_t57_m33348(__this, p0, method) (VideoBackgroundBehaviour_t57 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2959____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2959_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2959, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2959____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2959_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2959, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2959_FieldInfos[] =
{
	&InternalEnumerator_1_t2959____array_0_FieldInfo,
	&InternalEnumerator_1_t2959____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15200_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2959____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2959_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15200_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2959____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2959_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15203_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2959_PropertyInfos[] =
{
	&InternalEnumerator_1_t2959____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2959____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2959_InternalEnumerator_1__ctor_m15199_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15199_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15199_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2959_InternalEnumerator_1__ctor_m15199_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15199_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15200_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15200_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2959_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15200_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15201_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15201_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15201_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15202_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15202_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15202_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15203_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15203_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2959_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundBehaviour_t57_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15203_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2959_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15199_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15200_MethodInfo,
	&InternalEnumerator_1_Dispose_m15201_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15202_MethodInfo,
	&InternalEnumerator_1_get_Current_m15203_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15202_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15201_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2959_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15200_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15202_MethodInfo,
	&InternalEnumerator_1_Dispose_m15201_MethodInfo,
	&InternalEnumerator_1_get_Current_m15203_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2959_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6108_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2959_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6108_il2cpp_TypeInfo, 7},
};
extern TypeInfo VideoBackgroundBehaviour_t57_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2959_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15203_MethodInfo/* Method Usage */,
	&VideoBackgroundBehaviour_t57_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVideoBackgroundBehaviour_t57_m33348_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2959_0_0_0;
extern Il2CppType InternalEnumerator_1_t2959_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2959_GenericClass;
TypeInfo InternalEnumerator_1_t2959_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2959_MethodInfos/* methods */
	, InternalEnumerator_1_t2959_PropertyInfos/* properties */
	, InternalEnumerator_1_t2959_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2959_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2959_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2959_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2959_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2959_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2959_1_0_0/* this_arg */
	, InternalEnumerator_1_t2959_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2959_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2959_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2959)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7793_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>
extern MethodInfo ICollection_1_get_Count_m43705_MethodInfo;
static PropertyInfo ICollection_1_t7793____Count_PropertyInfo = 
{
	&ICollection_1_t7793_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43705_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43706_MethodInfo;
static PropertyInfo ICollection_1_t7793____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7793_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43706_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7793_PropertyInfos[] =
{
	&ICollection_1_t7793____Count_PropertyInfo,
	&ICollection_1_t7793____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43705_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43705_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7793_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43705_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43706_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43706_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7793_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43706_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
static ParameterInfo ICollection_1_t7793_ICollection_1_Add_m43707_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t57_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43707_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43707_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7793_ICollection_1_Add_m43707_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43707_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43708_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43708_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43708_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
static ParameterInfo ICollection_1_t7793_ICollection_1_Contains_m43709_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t57_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43709_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43709_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7793_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7793_ICollection_1_Contains_m43709_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43709_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviourU5BU5D_t5384_0_0_0;
extern Il2CppType VideoBackgroundBehaviourU5BU5D_t5384_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7793_ICollection_1_CopyTo_m43710_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviourU5BU5D_t5384_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43710_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43710_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7793_ICollection_1_CopyTo_m43710_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43710_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
static ParameterInfo ICollection_1_t7793_ICollection_1_Remove_m43711_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t57_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43711_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43711_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7793_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7793_ICollection_1_Remove_m43711_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43711_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7793_MethodInfos[] =
{
	&ICollection_1_get_Count_m43705_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43706_MethodInfo,
	&ICollection_1_Add_m43707_MethodInfo,
	&ICollection_1_Clear_m43708_MethodInfo,
	&ICollection_1_Contains_m43709_MethodInfo,
	&ICollection_1_CopyTo_m43710_MethodInfo,
	&ICollection_1_Remove_m43711_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7795_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7793_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7795_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7793_0_0_0;
extern Il2CppType ICollection_1_t7793_1_0_0;
struct ICollection_1_t7793;
extern Il2CppGenericClass ICollection_1_t7793_GenericClass;
TypeInfo ICollection_1_t7793_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7793_MethodInfos/* methods */
	, ICollection_1_t7793_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7793_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7793_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7793_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7793_0_0_0/* byval_arg */
	, &ICollection_1_t7793_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7793_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundBehaviour>
extern Il2CppType IEnumerator_1_t6108_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43712_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43712_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7795_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6108_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43712_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7795_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43712_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7795_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7795_0_0_0;
extern Il2CppType IEnumerable_1_t7795_1_0_0;
struct IEnumerable_1_t7795;
extern Il2CppGenericClass IEnumerable_1_t7795_GenericClass;
TypeInfo IEnumerable_1_t7795_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7795_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7795_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7795_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7795_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7795_0_0_0/* byval_arg */
	, &IEnumerable_1_t7795_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7795_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7794_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>
extern MethodInfo IList_1_get_Item_m43713_MethodInfo;
extern MethodInfo IList_1_set_Item_m43714_MethodInfo;
static PropertyInfo IList_1_t7794____Item_PropertyInfo = 
{
	&IList_1_t7794_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43713_MethodInfo/* get */
	, &IList_1_set_Item_m43714_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7794_PropertyInfos[] =
{
	&IList_1_t7794____Item_PropertyInfo,
	NULL
};
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
static ParameterInfo IList_1_t7794_IList_1_IndexOf_m43715_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t57_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43715_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43715_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7794_IList_1_IndexOf_m43715_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43715_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
static ParameterInfo IList_1_t7794_IList_1_Insert_m43716_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t57_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43716_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43716_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7794_IList_1_Insert_m43716_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43716_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7794_IList_1_RemoveAt_m43717_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43717_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43717_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7794_IList_1_RemoveAt_m43717_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43717_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7794_IList_1_get_Item_m43713_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43713_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43713_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundBehaviour_t57_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7794_IList_1_get_Item_m43713_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43713_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
static ParameterInfo IList_1_t7794_IList_1_set_Item_m43714_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t57_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43714_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43714_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7794_IList_1_set_Item_m43714_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43714_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7794_MethodInfos[] =
{
	&IList_1_IndexOf_m43715_MethodInfo,
	&IList_1_Insert_m43716_MethodInfo,
	&IList_1_RemoveAt_m43717_MethodInfo,
	&IList_1_get_Item_m43713_MethodInfo,
	&IList_1_set_Item_m43714_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7794_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7793_il2cpp_TypeInfo,
	&IEnumerable_1_t7795_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7794_0_0_0;
extern Il2CppType IList_1_t7794_1_0_0;
struct IList_1_t7794;
extern Il2CppGenericClass IList_1_t7794_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7794_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7794_MethodInfos/* methods */
	, IList_1_t7794_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7794_il2cpp_TypeInfo/* element_class */
	, IList_1_t7794_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7794_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7794_0_0_0/* byval_arg */
	, &IList_1_t7794_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7794_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7796_il2cpp_TypeInfo;

// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43718_MethodInfo;
static PropertyInfo ICollection_1_t7796____Count_PropertyInfo = 
{
	&ICollection_1_t7796_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43718_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43719_MethodInfo;
static PropertyInfo ICollection_1_t7796____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7796_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43719_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7796_PropertyInfos[] =
{
	&ICollection_1_t7796____Count_PropertyInfo,
	&ICollection_1_t7796____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43718_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43718_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7796_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43718_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43719_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43719_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7796_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43719_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
static ParameterInfo ICollection_1_t7796_ICollection_1_Add_m43720_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t58_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43720_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43720_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7796_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7796_ICollection_1_Add_m43720_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43720_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43721_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43721_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7796_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43721_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
static ParameterInfo ICollection_1_t7796_ICollection_1_Contains_m43722_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t58_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43722_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43722_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7796_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7796_ICollection_1_Contains_m43722_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43722_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundAbstractBehaviourU5BU5D_t803_0_0_0;
extern Il2CppType VideoBackgroundAbstractBehaviourU5BU5D_t803_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7796_ICollection_1_CopyTo_m43723_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviourU5BU5D_t803_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43723_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43723_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7796_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7796_ICollection_1_CopyTo_m43723_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43723_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
static ParameterInfo ICollection_1_t7796_ICollection_1_Remove_m43724_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t58_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43724_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43724_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7796_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7796_ICollection_1_Remove_m43724_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43724_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7796_MethodInfos[] =
{
	&ICollection_1_get_Count_m43718_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43719_MethodInfo,
	&ICollection_1_Add_m43720_MethodInfo,
	&ICollection_1_Clear_m43721_MethodInfo,
	&ICollection_1_Contains_m43722_MethodInfo,
	&ICollection_1_CopyTo_m43723_MethodInfo,
	&ICollection_1_Remove_m43724_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7798_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7796_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7798_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7796_0_0_0;
extern Il2CppType ICollection_1_t7796_1_0_0;
struct ICollection_1_t7796;
extern Il2CppGenericClass ICollection_1_t7796_GenericClass;
TypeInfo ICollection_1_t7796_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7796_MethodInfos/* methods */
	, ICollection_1_t7796_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7796_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7796_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7796_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7796_0_0_0/* byval_arg */
	, &ICollection_1_t7796_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7796_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6110_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43725_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43725_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7798_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6110_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43725_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7798_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43725_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7798_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7798_0_0_0;
extern Il2CppType IEnumerable_1_t7798_1_0_0;
struct IEnumerable_1_t7798;
extern Il2CppGenericClass IEnumerable_1_t7798_GenericClass;
TypeInfo IEnumerable_1_t7798_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7798_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7798_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7798_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7798_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7798_0_0_0/* byval_arg */
	, &IEnumerable_1_t7798_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7798_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6110_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43726_MethodInfo;
static PropertyInfo IEnumerator_1_t6110____Current_PropertyInfo = 
{
	&IEnumerator_1_t6110_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43726_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6110_PropertyInfos[] =
{
	&IEnumerator_1_t6110____Current_PropertyInfo,
	NULL
};
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43726_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43726_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6110_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundAbstractBehaviour_t58_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43726_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6110_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43726_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6110_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6110_0_0_0;
extern Il2CppType IEnumerator_1_t6110_1_0_0;
struct IEnumerator_1_t6110;
extern Il2CppGenericClass IEnumerator_1_t6110_GenericClass;
TypeInfo IEnumerator_1_t6110_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6110_MethodInfos/* methods */
	, IEnumerator_1_t6110_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6110_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6110_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6110_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6110_0_0_0/* byval_arg */
	, &IEnumerator_1_t6110_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6110_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_96.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2960_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_96MethodDeclarations.h"

extern TypeInfo VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15208_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVideoBackgroundAbstractBehaviour_t58_m33359_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VideoBackgroundAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VideoBackgroundAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisVideoBackgroundAbstractBehaviour_t58_m33359(__this, p0, method) (VideoBackgroundAbstractBehaviour_t58 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2960____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2960, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2960____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2960, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2960_FieldInfos[] =
{
	&InternalEnumerator_1_t2960____array_0_FieldInfo,
	&InternalEnumerator_1_t2960____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15205_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2960____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2960_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15205_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2960____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2960_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15208_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2960_PropertyInfos[] =
{
	&InternalEnumerator_1_t2960____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2960____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2960_InternalEnumerator_1__ctor_m15204_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15204_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15204_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2960_InternalEnumerator_1__ctor_m15204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15204_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15205_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15205_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15205_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15206_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15206_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15206_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15207_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15207_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15207_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15208_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15208_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundAbstractBehaviour_t58_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15208_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2960_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15204_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15205_MethodInfo,
	&InternalEnumerator_1_Dispose_m15206_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15207_MethodInfo,
	&InternalEnumerator_1_get_Current_m15208_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15207_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15206_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2960_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15205_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15207_MethodInfo,
	&InternalEnumerator_1_Dispose_m15206_MethodInfo,
	&InternalEnumerator_1_get_Current_m15208_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2960_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6110_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2960_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6110_il2cpp_TypeInfo, 7},
};
extern TypeInfo VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2960_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15208_MethodInfo/* Method Usage */,
	&VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVideoBackgroundAbstractBehaviour_t58_m33359_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2960_0_0_0;
extern Il2CppType InternalEnumerator_1_t2960_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2960_GenericClass;
TypeInfo InternalEnumerator_1_t2960_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2960_MethodInfos/* methods */
	, InternalEnumerator_1_t2960_PropertyInfos/* properties */
	, InternalEnumerator_1_t2960_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2960_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2960_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2960_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2960_1_0_0/* this_arg */
	, InternalEnumerator_1_t2960_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2960_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2960_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2960)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7797_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43727_MethodInfo;
extern MethodInfo IList_1_set_Item_m43728_MethodInfo;
static PropertyInfo IList_1_t7797____Item_PropertyInfo = 
{
	&IList_1_t7797_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43727_MethodInfo/* get */
	, &IList_1_set_Item_m43728_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7797_PropertyInfos[] =
{
	&IList_1_t7797____Item_PropertyInfo,
	NULL
};
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
static ParameterInfo IList_1_t7797_IList_1_IndexOf_m43729_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t58_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43729_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43729_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7797_IList_1_IndexOf_m43729_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43729_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
static ParameterInfo IList_1_t7797_IList_1_Insert_m43730_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t58_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43730_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43730_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7797_IList_1_Insert_m43730_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43730_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7797_IList_1_RemoveAt_m43731_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43731_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43731_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7797_IList_1_RemoveAt_m43731_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43731_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7797_IList_1_get_Item_m43727_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43727_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43727_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundAbstractBehaviour_t58_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7797_IList_1_get_Item_m43727_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43727_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
static ParameterInfo IList_1_t7797_IList_1_set_Item_m43728_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t58_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43728_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43728_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7797_IList_1_set_Item_m43728_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43728_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7797_MethodInfos[] =
{
	&IList_1_IndexOf_m43729_MethodInfo,
	&IList_1_Insert_m43730_MethodInfo,
	&IList_1_RemoveAt_m43731_MethodInfo,
	&IList_1_get_Item_m43727_MethodInfo,
	&IList_1_set_Item_m43728_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7797_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7796_il2cpp_TypeInfo,
	&IEnumerable_1_t7798_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7797_0_0_0;
extern Il2CppType IList_1_t7797_1_0_0;
struct IList_1_t7797;
extern Il2CppGenericClass IList_1_t7797_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7797_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7797_MethodInfos/* methods */
	, IList_1_t7797_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7797_il2cpp_TypeInfo/* element_class */
	, IList_1_t7797_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7797_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7797_0_0_0/* byval_arg */
	, &IList_1_t7797_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7797_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_30.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2961_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_30MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_26.h"
extern TypeInfo InvokableCall_1_t2962_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_26MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15211_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15213_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2961____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2961_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2961, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2961_FieldInfos[] =
{
	&CachedInvokableCall_1_t2961____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2961_CachedInvokableCall_1__ctor_m15209_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t57_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15209_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15209_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2961_CachedInvokableCall_1__ctor_m15209_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15209_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2961_CachedInvokableCall_1_Invoke_m15210_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15210_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15210_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2961_CachedInvokableCall_1_Invoke_m15210_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15210_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2961_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15209_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15210_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15210_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15214_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2961_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15210_MethodInfo,
	&InvokableCall_1_Find_m15214_MethodInfo,
};
extern Il2CppType UnityAction_1_t2963_0_0_0;
extern TypeInfo UnityAction_1_t2963_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisVideoBackgroundBehaviour_t57_m33369_MethodInfo;
extern TypeInfo VideoBackgroundBehaviour_t57_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15216_MethodInfo;
extern TypeInfo VideoBackgroundBehaviour_t57_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2961_RGCTXData[8] = 
{
	&UnityAction_1_t2963_0_0_0/* Type Usage */,
	&UnityAction_1_t2963_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVideoBackgroundBehaviour_t57_m33369_MethodInfo/* Method Usage */,
	&VideoBackgroundBehaviour_t57_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15216_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15211_MethodInfo/* Method Usage */,
	&VideoBackgroundBehaviour_t57_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15213_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2961_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2961_1_0_0;
struct CachedInvokableCall_1_t2961;
extern Il2CppGenericClass CachedInvokableCall_1_t2961_GenericClass;
TypeInfo CachedInvokableCall_1_t2961_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2961_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2961_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2962_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2961_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2961_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2961_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2961_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2961_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2961_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2961_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2961)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_33.h"
extern TypeInfo UnityAction_1_t2963_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_33MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VideoBackgroundBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VideoBackgroundBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisVideoBackgroundBehaviour_t57_m33369(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>
extern Il2CppType UnityAction_1_t2963_0_0_1;
FieldInfo InvokableCall_1_t2962____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2963_0_0_1/* type */
	, &InvokableCall_1_t2962_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2962, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2962_FieldInfos[] =
{
	&InvokableCall_1_t2962____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2962_InvokableCall_1__ctor_m15211_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15211_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15211_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2962_InvokableCall_1__ctor_m15211_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15211_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2963_0_0_0;
static ParameterInfo InvokableCall_1_t2962_InvokableCall_1__ctor_m15212_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2963_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15212_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15212_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2962_InvokableCall_1__ctor_m15212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15212_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2962_InvokableCall_1_Invoke_m15213_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15213_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15213_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2962_InvokableCall_1_Invoke_m15213_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15213_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2962_InvokableCall_1_Find_m15214_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15214_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15214_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2962_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2962_InvokableCall_1_Find_m15214_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15214_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2962_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15211_MethodInfo,
	&InvokableCall_1__ctor_m15212_MethodInfo,
	&InvokableCall_1_Invoke_m15213_MethodInfo,
	&InvokableCall_1_Find_m15214_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2962_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15213_MethodInfo,
	&InvokableCall_1_Find_m15214_MethodInfo,
};
extern TypeInfo UnityAction_1_t2963_il2cpp_TypeInfo;
extern TypeInfo VideoBackgroundBehaviour_t57_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2962_RGCTXData[5] = 
{
	&UnityAction_1_t2963_0_0_0/* Type Usage */,
	&UnityAction_1_t2963_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVideoBackgroundBehaviour_t57_m33369_MethodInfo/* Method Usage */,
	&VideoBackgroundBehaviour_t57_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15216_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2962_0_0_0;
extern Il2CppType InvokableCall_1_t2962_1_0_0;
struct InvokableCall_1_t2962;
extern Il2CppGenericClass InvokableCall_1_t2962_GenericClass;
TypeInfo InvokableCall_1_t2962_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2962_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2962_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2962_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2962_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2962_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2962_0_0_0/* byval_arg */
	, &InvokableCall_1_t2962_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2962_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2962_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2962)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2963_UnityAction_1__ctor_m15215_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15215_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15215_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2963_UnityAction_1__ctor_m15215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15215_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
static ParameterInfo UnityAction_1_t2963_UnityAction_1_Invoke_m15216_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t57_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15216_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15216_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2963_UnityAction_1_Invoke_m15216_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15216_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t57_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2963_UnityAction_1_BeginInvoke_m15217_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t57_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15217_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15217_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2963_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2963_UnityAction_1_BeginInvoke_m15217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15217_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2963_UnityAction_1_EndInvoke_m15218_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15218_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15218_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2963_UnityAction_1_EndInvoke_m15218_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15218_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2963_MethodInfos[] =
{
	&UnityAction_1__ctor_m15215_MethodInfo,
	&UnityAction_1_Invoke_m15216_MethodInfo,
	&UnityAction_1_BeginInvoke_m15217_MethodInfo,
	&UnityAction_1_EndInvoke_m15218_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15217_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15218_MethodInfo;
static MethodInfo* UnityAction_1_t2963_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15216_MethodInfo,
	&UnityAction_1_BeginInvoke_m15217_MethodInfo,
	&UnityAction_1_EndInvoke_m15218_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2963_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2963_1_0_0;
struct UnityAction_1_t2963;
extern Il2CppGenericClass UnityAction_1_t2963_GenericClass;
TypeInfo UnityAction_1_t2963_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2963_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2963_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2963_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2963_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2963_0_0_0/* byval_arg */
	, &UnityAction_1_t2963_1_0_0/* this_arg */
	, UnityAction_1_t2963_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2963_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2963)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6112_il2cpp_TypeInfo;

// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRenderer>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRenderer>
extern MethodInfo IEnumerator_1_get_Current_m43732_MethodInfo;
static PropertyInfo IEnumerator_1_t6112____Current_PropertyInfo = 
{
	&IEnumerator_1_t6112_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43732_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6112_PropertyInfos[] =
{
	&IEnumerator_1_t6112____Current_PropertyInfo,
	NULL
};
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43732_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRenderer>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43732_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6112_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRenderer_t59_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43732_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6112_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43732_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6112_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6112_0_0_0;
extern Il2CppType IEnumerator_1_t6112_1_0_0;
struct IEnumerator_1_t6112;
extern Il2CppGenericClass IEnumerator_1_t6112_GenericClass;
TypeInfo IEnumerator_1_t6112_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6112_MethodInfos/* methods */
	, IEnumerator_1_t6112_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6112_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6112_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6112_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6112_0_0_0/* byval_arg */
	, &IEnumerator_1_t6112_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6112_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_97.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2964_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_97MethodDeclarations.h"

extern TypeInfo VideoTextureRenderer_t59_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15223_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVideoTextureRenderer_t59_m33371_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VideoTextureRenderer>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VideoTextureRenderer>(System.Int32)
#define Array_InternalArray__get_Item_TisVideoTextureRenderer_t59_m33371(__this, p0, method) (VideoTextureRenderer_t59 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2964____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2964_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2964, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2964____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2964_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2964, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2964_FieldInfos[] =
{
	&InternalEnumerator_1_t2964____array_0_FieldInfo,
	&InternalEnumerator_1_t2964____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15220_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2964____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2964_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15220_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2964____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2964_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15223_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2964_PropertyInfos[] =
{
	&InternalEnumerator_1_t2964____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2964____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2964_InternalEnumerator_1__ctor_m15219_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15219_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15219_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2964_InternalEnumerator_1__ctor_m15219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15219_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15220_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15220_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2964_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15220_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15221_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15221_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15221_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15222_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15222_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2964_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15222_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15223_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15223_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2964_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRenderer_t59_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15223_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2964_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15219_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15220_MethodInfo,
	&InternalEnumerator_1_Dispose_m15221_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15222_MethodInfo,
	&InternalEnumerator_1_get_Current_m15223_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15222_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15221_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2964_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15220_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15222_MethodInfo,
	&InternalEnumerator_1_Dispose_m15221_MethodInfo,
	&InternalEnumerator_1_get_Current_m15223_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2964_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6112_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2964_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6112_il2cpp_TypeInfo, 7},
};
extern TypeInfo VideoTextureRenderer_t59_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2964_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15223_MethodInfo/* Method Usage */,
	&VideoTextureRenderer_t59_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVideoTextureRenderer_t59_m33371_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2964_0_0_0;
extern Il2CppType InternalEnumerator_1_t2964_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2964_GenericClass;
TypeInfo InternalEnumerator_1_t2964_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2964_MethodInfos/* methods */
	, InternalEnumerator_1_t2964_PropertyInfos/* properties */
	, InternalEnumerator_1_t2964_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2964_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2964_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2964_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2964_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2964_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2964_1_0_0/* this_arg */
	, InternalEnumerator_1_t2964_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2964_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2964_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2964)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7799_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>
extern MethodInfo ICollection_1_get_Count_m43733_MethodInfo;
static PropertyInfo ICollection_1_t7799____Count_PropertyInfo = 
{
	&ICollection_1_t7799_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43733_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43734_MethodInfo;
static PropertyInfo ICollection_1_t7799____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7799_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43734_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7799_PropertyInfos[] =
{
	&ICollection_1_t7799____Count_PropertyInfo,
	&ICollection_1_t7799____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43733_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::get_Count()
MethodInfo ICollection_1_get_Count_m43733_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7799_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43733_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43734_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43734_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7799_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43734_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
static ParameterInfo ICollection_1_t7799_ICollection_1_Add_m43735_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t59_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43735_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Add(T)
MethodInfo ICollection_1_Add_m43735_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7799_ICollection_1_Add_m43735_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43735_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43736_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Clear()
MethodInfo ICollection_1_Clear_m43736_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43736_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
static ParameterInfo ICollection_1_t7799_ICollection_1_Contains_m43737_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t59_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43737_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Contains(T)
MethodInfo ICollection_1_Contains_m43737_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7799_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7799_ICollection_1_Contains_m43737_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43737_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererU5BU5D_t5385_0_0_0;
extern Il2CppType VideoTextureRendererU5BU5D_t5385_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7799_ICollection_1_CopyTo_m43738_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererU5BU5D_t5385_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43738_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43738_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7799_ICollection_1_CopyTo_m43738_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43738_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
static ParameterInfo ICollection_1_t7799_ICollection_1_Remove_m43739_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t59_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43739_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Remove(T)
MethodInfo ICollection_1_Remove_m43739_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7799_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7799_ICollection_1_Remove_m43739_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43739_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7799_MethodInfos[] =
{
	&ICollection_1_get_Count_m43733_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43734_MethodInfo,
	&ICollection_1_Add_m43735_MethodInfo,
	&ICollection_1_Clear_m43736_MethodInfo,
	&ICollection_1_Contains_m43737_MethodInfo,
	&ICollection_1_CopyTo_m43738_MethodInfo,
	&ICollection_1_Remove_m43739_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7801_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7799_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7801_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7799_0_0_0;
extern Il2CppType ICollection_1_t7799_1_0_0;
struct ICollection_1_t7799;
extern Il2CppGenericClass ICollection_1_t7799_GenericClass;
TypeInfo ICollection_1_t7799_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7799_MethodInfos/* methods */
	, ICollection_1_t7799_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7799_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7799_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7799_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7799_0_0_0/* byval_arg */
	, &ICollection_1_t7799_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7799_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRenderer>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRenderer>
extern Il2CppType IEnumerator_1_t6112_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43740_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRenderer>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43740_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7801_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6112_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43740_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7801_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43740_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7801_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7801_0_0_0;
extern Il2CppType IEnumerable_1_t7801_1_0_0;
struct IEnumerable_1_t7801;
extern Il2CppGenericClass IEnumerable_1_t7801_GenericClass;
TypeInfo IEnumerable_1_t7801_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7801_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7801_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7801_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7801_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7801_0_0_0/* byval_arg */
	, &IEnumerable_1_t7801_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7801_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7800_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>
extern MethodInfo IList_1_get_Item_m43741_MethodInfo;
extern MethodInfo IList_1_set_Item_m43742_MethodInfo;
static PropertyInfo IList_1_t7800____Item_PropertyInfo = 
{
	&IList_1_t7800_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43741_MethodInfo/* get */
	, &IList_1_set_Item_m43742_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7800_PropertyInfos[] =
{
	&IList_1_t7800____Item_PropertyInfo,
	NULL
};
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
static ParameterInfo IList_1_t7800_IList_1_IndexOf_m43743_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t59_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43743_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43743_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7800_IList_1_IndexOf_m43743_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43743_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
static ParameterInfo IList_1_t7800_IList_1_Insert_m43744_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t59_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43744_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43744_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7800_IList_1_Insert_m43744_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43744_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7800_IList_1_RemoveAt_m43745_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43745_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43745_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7800_IList_1_RemoveAt_m43745_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43745_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7800_IList_1_get_Item_m43741_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43741_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43741_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRenderer_t59_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7800_IList_1_get_Item_m43741_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43741_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
static ParameterInfo IList_1_t7800_IList_1_set_Item_m43742_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t59_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43742_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43742_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7800_IList_1_set_Item_m43742_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43742_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7800_MethodInfos[] =
{
	&IList_1_IndexOf_m43743_MethodInfo,
	&IList_1_Insert_m43744_MethodInfo,
	&IList_1_RemoveAt_m43745_MethodInfo,
	&IList_1_get_Item_m43741_MethodInfo,
	&IList_1_set_Item_m43742_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7800_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7799_il2cpp_TypeInfo,
	&IEnumerable_1_t7801_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7800_0_0_0;
extern Il2CppType IList_1_t7800_1_0_0;
struct IList_1_t7800;
extern Il2CppGenericClass IList_1_t7800_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7800_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7800_MethodInfos/* methods */
	, IList_1_t7800_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7800_il2cpp_TypeInfo/* element_class */
	, IList_1_t7800_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7800_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7800_0_0_0/* byval_arg */
	, &IList_1_t7800_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7800_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7802_il2cpp_TypeInfo;

// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendere.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43746_MethodInfo;
static PropertyInfo ICollection_1_t7802____Count_PropertyInfo = 
{
	&ICollection_1_t7802_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43746_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43747_MethodInfo;
static PropertyInfo ICollection_1_t7802____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7802_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43747_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7802_PropertyInfos[] =
{
	&ICollection_1_t7802____Count_PropertyInfo,
	&ICollection_1_t7802____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43746_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43746_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7802_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43746_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43747_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43747_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7802_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43747_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
static ParameterInfo ICollection_1_t7802_ICollection_1_Add_m43748_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t60_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43748_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43748_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7802_ICollection_1_Add_m43748_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43748_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43749_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43749_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43749_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
static ParameterInfo ICollection_1_t7802_ICollection_1_Contains_m43750_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t60_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43750_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43750_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7802_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7802_ICollection_1_Contains_m43750_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43750_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererAbstractBehaviourU5BU5D_t5649_0_0_0;
extern Il2CppType VideoTextureRendererAbstractBehaviourU5BU5D_t5649_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7802_ICollection_1_CopyTo_m43751_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviourU5BU5D_t5649_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43751_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43751_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7802_ICollection_1_CopyTo_m43751_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43751_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
static ParameterInfo ICollection_1_t7802_ICollection_1_Remove_m43752_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t60_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43752_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43752_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7802_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7802_ICollection_1_Remove_m43752_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43752_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7802_MethodInfos[] =
{
	&ICollection_1_get_Count_m43746_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43747_MethodInfo,
	&ICollection_1_Add_m43748_MethodInfo,
	&ICollection_1_Clear_m43749_MethodInfo,
	&ICollection_1_Contains_m43750_MethodInfo,
	&ICollection_1_CopyTo_m43751_MethodInfo,
	&ICollection_1_Remove_m43752_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7804_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7802_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7804_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7802_0_0_0;
extern Il2CppType ICollection_1_t7802_1_0_0;
struct ICollection_1_t7802;
extern Il2CppGenericClass ICollection_1_t7802_GenericClass;
TypeInfo ICollection_1_t7802_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7802_MethodInfos/* methods */
	, ICollection_1_t7802_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7802_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7802_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7802_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7802_0_0_0/* byval_arg */
	, &ICollection_1_t7802_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7802_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRendererAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRendererAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6114_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43753_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRendererAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43753_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7804_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6114_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43753_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7804_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43753_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7804_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7804_0_0_0;
extern Il2CppType IEnumerable_1_t7804_1_0_0;
struct IEnumerable_1_t7804;
extern Il2CppGenericClass IEnumerable_1_t7804_GenericClass;
TypeInfo IEnumerable_1_t7804_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7804_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7804_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7804_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7804_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7804_0_0_0/* byval_arg */
	, &IEnumerable_1_t7804_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7804_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6114_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43754_MethodInfo;
static PropertyInfo IEnumerator_1_t6114____Current_PropertyInfo = 
{
	&IEnumerator_1_t6114_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43754_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6114_PropertyInfos[] =
{
	&IEnumerator_1_t6114____Current_PropertyInfo,
	NULL
};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43754_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43754_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6114_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRendererAbstractBehaviour_t60_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43754_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6114_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43754_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6114_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6114_0_0_0;
extern Il2CppType IEnumerator_1_t6114_1_0_0;
struct IEnumerator_1_t6114;
extern Il2CppGenericClass IEnumerator_1_t6114_GenericClass;
TypeInfo IEnumerator_1_t6114_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6114_MethodInfos/* methods */
	, IEnumerator_1_t6114_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6114_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6114_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6114_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6114_0_0_0/* byval_arg */
	, &IEnumerator_1_t6114_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6114_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_98.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2965_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_98MethodDeclarations.h"

extern TypeInfo VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15228_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVideoTextureRendererAbstractBehaviour_t60_m33382_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VideoTextureRendererAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VideoTextureRendererAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisVideoTextureRendererAbstractBehaviour_t60_m33382(__this, p0, method) (VideoTextureRendererAbstractBehaviour_t60 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2965____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2965, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2965____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2965, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2965_FieldInfos[] =
{
	&InternalEnumerator_1_t2965____array_0_FieldInfo,
	&InternalEnumerator_1_t2965____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15225_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2965____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2965_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15225_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2965____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2965_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15228_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2965_PropertyInfos[] =
{
	&InternalEnumerator_1_t2965____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2965____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2965_InternalEnumerator_1__ctor_m15224_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15224_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15224_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2965_InternalEnumerator_1__ctor_m15224_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15224_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15225_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15225_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15225_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15226_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15226_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15226_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15227_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15227_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15227_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15228_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15228_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRendererAbstractBehaviour_t60_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15228_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2965_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15224_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15225_MethodInfo,
	&InternalEnumerator_1_Dispose_m15226_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15227_MethodInfo,
	&InternalEnumerator_1_get_Current_m15228_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15227_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15226_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2965_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15225_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15227_MethodInfo,
	&InternalEnumerator_1_Dispose_m15226_MethodInfo,
	&InternalEnumerator_1_get_Current_m15228_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2965_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6114_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2965_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6114_il2cpp_TypeInfo, 7},
};
extern TypeInfo VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2965_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15228_MethodInfo/* Method Usage */,
	&VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVideoTextureRendererAbstractBehaviour_t60_m33382_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2965_0_0_0;
extern Il2CppType InternalEnumerator_1_t2965_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2965_GenericClass;
TypeInfo InternalEnumerator_1_t2965_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2965_MethodInfos/* methods */
	, InternalEnumerator_1_t2965_PropertyInfos/* properties */
	, InternalEnumerator_1_t2965_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2965_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2965_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2965_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2965_1_0_0/* this_arg */
	, InternalEnumerator_1_t2965_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2965_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2965_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2965)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7803_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43755_MethodInfo;
extern MethodInfo IList_1_set_Item_m43756_MethodInfo;
static PropertyInfo IList_1_t7803____Item_PropertyInfo = 
{
	&IList_1_t7803_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43755_MethodInfo/* get */
	, &IList_1_set_Item_m43756_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7803_PropertyInfos[] =
{
	&IList_1_t7803____Item_PropertyInfo,
	NULL
};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
static ParameterInfo IList_1_t7803_IList_1_IndexOf_m43757_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t60_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43757_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43757_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7803_IList_1_IndexOf_m43757_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43757_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
static ParameterInfo IList_1_t7803_IList_1_Insert_m43758_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t60_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43758_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43758_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7803_IList_1_Insert_m43758_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43758_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7803_IList_1_RemoveAt_m43759_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43759_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43759_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7803_IList_1_RemoveAt_m43759_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43759_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7803_IList_1_get_Item_m43755_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43755_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43755_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRendererAbstractBehaviour_t60_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7803_IList_1_get_Item_m43755_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43755_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
static ParameterInfo IList_1_t7803_IList_1_set_Item_m43756_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t60_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43756_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43756_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7803_IList_1_set_Item_m43756_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43756_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7803_MethodInfos[] =
{
	&IList_1_IndexOf_m43757_MethodInfo,
	&IList_1_Insert_m43758_MethodInfo,
	&IList_1_RemoveAt_m43759_MethodInfo,
	&IList_1_get_Item_m43755_MethodInfo,
	&IList_1_set_Item_m43756_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7803_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7802_il2cpp_TypeInfo,
	&IEnumerable_1_t7804_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7803_0_0_0;
extern Il2CppType IList_1_t7803_1_0_0;
struct IList_1_t7803;
extern Il2CppGenericClass IList_1_t7803_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7803_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7803_MethodInfos/* methods */
	, IList_1_t7803_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7803_il2cpp_TypeInfo/* element_class */
	, IList_1_t7803_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7803_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7803_0_0_0/* byval_arg */
	, &IList_1_t7803_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7803_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_31.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2966_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_31MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_27.h"
extern TypeInfo InvokableCall_1_t2967_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_27MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15231_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15233_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2966____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2966_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2966, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2966_FieldInfos[] =
{
	&CachedInvokableCall_1_t2966____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2966_CachedInvokableCall_1__ctor_m15229_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t59_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15229_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15229_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2966_CachedInvokableCall_1__ctor_m15229_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15229_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2966_CachedInvokableCall_1_Invoke_m15230_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15230_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15230_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2966_CachedInvokableCall_1_Invoke_m15230_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15230_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2966_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15229_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15230_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15230_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15234_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2966_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15230_MethodInfo,
	&InvokableCall_1_Find_m15234_MethodInfo,
};
extern Il2CppType UnityAction_1_t2968_0_0_0;
extern TypeInfo UnityAction_1_t2968_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisVideoTextureRenderer_t59_m33392_MethodInfo;
extern TypeInfo VideoTextureRenderer_t59_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15236_MethodInfo;
extern TypeInfo VideoTextureRenderer_t59_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2966_RGCTXData[8] = 
{
	&UnityAction_1_t2968_0_0_0/* Type Usage */,
	&UnityAction_1_t2968_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVideoTextureRenderer_t59_m33392_MethodInfo/* Method Usage */,
	&VideoTextureRenderer_t59_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15236_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15231_MethodInfo/* Method Usage */,
	&VideoTextureRenderer_t59_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15233_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2966_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2966_1_0_0;
struct CachedInvokableCall_1_t2966;
extern Il2CppGenericClass CachedInvokableCall_1_t2966_GenericClass;
TypeInfo CachedInvokableCall_1_t2966_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2966_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2966_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2967_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2966_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2966_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2966_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2966_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2966_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2966_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2966_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2966)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_34.h"
extern TypeInfo UnityAction_1_t2968_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_34MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VideoTextureRenderer>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VideoTextureRenderer>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisVideoTextureRenderer_t59_m33392(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>
extern Il2CppType UnityAction_1_t2968_0_0_1;
FieldInfo InvokableCall_1_t2967____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2968_0_0_1/* type */
	, &InvokableCall_1_t2967_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2967, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2967_FieldInfos[] =
{
	&InvokableCall_1_t2967____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2967_InvokableCall_1__ctor_m15231_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15231_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15231_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2967_InvokableCall_1__ctor_m15231_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15231_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2968_0_0_0;
static ParameterInfo InvokableCall_1_t2967_InvokableCall_1__ctor_m15232_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2968_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15232_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15232_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2967_InvokableCall_1__ctor_m15232_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15232_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2967_InvokableCall_1_Invoke_m15233_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15233_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15233_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2967_InvokableCall_1_Invoke_m15233_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15233_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2967_InvokableCall_1_Find_m15234_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15234_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15234_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2967_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2967_InvokableCall_1_Find_m15234_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15234_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2967_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15231_MethodInfo,
	&InvokableCall_1__ctor_m15232_MethodInfo,
	&InvokableCall_1_Invoke_m15233_MethodInfo,
	&InvokableCall_1_Find_m15234_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2967_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15233_MethodInfo,
	&InvokableCall_1_Find_m15234_MethodInfo,
};
extern TypeInfo UnityAction_1_t2968_il2cpp_TypeInfo;
extern TypeInfo VideoTextureRenderer_t59_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2967_RGCTXData[5] = 
{
	&UnityAction_1_t2968_0_0_0/* Type Usage */,
	&UnityAction_1_t2968_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVideoTextureRenderer_t59_m33392_MethodInfo/* Method Usage */,
	&VideoTextureRenderer_t59_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15236_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2967_0_0_0;
extern Il2CppType InvokableCall_1_t2967_1_0_0;
struct InvokableCall_1_t2967;
extern Il2CppGenericClass InvokableCall_1_t2967_GenericClass;
TypeInfo InvokableCall_1_t2967_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2967_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2967_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2967_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2967_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2967_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2967_0_0_0/* byval_arg */
	, &InvokableCall_1_t2967_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2967_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2967_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2967)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2968_UnityAction_1__ctor_m15235_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15235_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15235_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2968_UnityAction_1__ctor_m15235_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15235_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
static ParameterInfo UnityAction_1_t2968_UnityAction_1_Invoke_m15236_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t59_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15236_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15236_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2968_UnityAction_1_Invoke_m15236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15236_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t59_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2968_UnityAction_1_BeginInvoke_m15237_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t59_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15237_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15237_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2968_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2968_UnityAction_1_BeginInvoke_m15237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15237_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2968_UnityAction_1_EndInvoke_m15238_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15238_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15238_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2968_UnityAction_1_EndInvoke_m15238_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15238_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2968_MethodInfos[] =
{
	&UnityAction_1__ctor_m15235_MethodInfo,
	&UnityAction_1_Invoke_m15236_MethodInfo,
	&UnityAction_1_BeginInvoke_m15237_MethodInfo,
	&UnityAction_1_EndInvoke_m15238_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15237_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15238_MethodInfo;
static MethodInfo* UnityAction_1_t2968_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15236_MethodInfo,
	&UnityAction_1_BeginInvoke_m15237_MethodInfo,
	&UnityAction_1_EndInvoke_m15238_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2968_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2968_1_0_0;
struct UnityAction_1_t2968;
extern Il2CppGenericClass UnityAction_1_t2968_GenericClass;
TypeInfo UnityAction_1_t2968_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2968_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2968_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2968_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2968_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2968_0_0_0/* byval_arg */
	, &UnityAction_1_t2968_1_0_0/* this_arg */
	, UnityAction_1_t2968_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2968_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2968)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6116_il2cpp_TypeInfo;

// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43760_MethodInfo;
static PropertyInfo IEnumerator_1_t6116____Current_PropertyInfo = 
{
	&IEnumerator_1_t6116_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43760_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6116_PropertyInfos[] =
{
	&IEnumerator_1_t6116____Current_PropertyInfo,
	NULL
};
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43760_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43760_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6116_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonBehaviour_t61_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43760_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6116_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43760_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6116_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6116_0_0_0;
extern Il2CppType IEnumerator_1_t6116_1_0_0;
struct IEnumerator_1_t6116;
extern Il2CppGenericClass IEnumerator_1_t6116_GenericClass;
TypeInfo IEnumerator_1_t6116_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6116_MethodInfos/* methods */
	, IEnumerator_1_t6116_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6116_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6116_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6116_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6116_0_0_0/* byval_arg */
	, &IEnumerator_1_t6116_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6116_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_99.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2969_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_99MethodDeclarations.h"

extern TypeInfo VirtualButtonBehaviour_t61_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15243_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVirtualButtonBehaviour_t61_m33394_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VirtualButtonBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VirtualButtonBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisVirtualButtonBehaviour_t61_m33394(__this, p0, method) (VirtualButtonBehaviour_t61 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2969____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2969_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2969, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2969____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2969_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2969, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2969_FieldInfos[] =
{
	&InternalEnumerator_1_t2969____array_0_FieldInfo,
	&InternalEnumerator_1_t2969____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15240_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2969____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2969_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15240_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2969____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2969_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15243_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2969_PropertyInfos[] =
{
	&InternalEnumerator_1_t2969____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2969____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2969_InternalEnumerator_1__ctor_m15239_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15239_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15239_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2969_InternalEnumerator_1__ctor_m15239_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15239_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15240_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15240_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2969_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15240_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15241_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15241_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15241_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15242_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15242_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2969_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15242_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15243_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15243_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2969_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonBehaviour_t61_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15243_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2969_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15239_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15240_MethodInfo,
	&InternalEnumerator_1_Dispose_m15241_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15242_MethodInfo,
	&InternalEnumerator_1_get_Current_m15243_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15242_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15241_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2969_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15240_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15242_MethodInfo,
	&InternalEnumerator_1_Dispose_m15241_MethodInfo,
	&InternalEnumerator_1_get_Current_m15243_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2969_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6116_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2969_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6116_il2cpp_TypeInfo, 7},
};
extern TypeInfo VirtualButtonBehaviour_t61_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2969_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15243_MethodInfo/* Method Usage */,
	&VirtualButtonBehaviour_t61_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVirtualButtonBehaviour_t61_m33394_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2969_0_0_0;
extern Il2CppType InternalEnumerator_1_t2969_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2969_GenericClass;
TypeInfo InternalEnumerator_1_t2969_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2969_MethodInfos/* methods */
	, InternalEnumerator_1_t2969_PropertyInfos/* properties */
	, InternalEnumerator_1_t2969_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2969_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2969_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2969_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2969_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2969_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2969_1_0_0/* this_arg */
	, InternalEnumerator_1_t2969_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2969_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2969_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2969)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7805_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>
extern MethodInfo ICollection_1_get_Count_m43761_MethodInfo;
static PropertyInfo ICollection_1_t7805____Count_PropertyInfo = 
{
	&ICollection_1_t7805_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43761_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43762_MethodInfo;
static PropertyInfo ICollection_1_t7805____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7805_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43762_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7805_PropertyInfos[] =
{
	&ICollection_1_t7805____Count_PropertyInfo,
	&ICollection_1_t7805____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43761_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43761_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7805_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43761_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43762_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43762_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7805_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43762_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
static ParameterInfo ICollection_1_t7805_ICollection_1_Add_m43763_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t61_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43763_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43763_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7805_ICollection_1_Add_m43763_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43763_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43764_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43764_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43764_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
static ParameterInfo ICollection_1_t7805_ICollection_1_Contains_m43765_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t61_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43765_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43765_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7805_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7805_ICollection_1_Contains_m43765_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43765_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviourU5BU5D_t5386_0_0_0;
extern Il2CppType VirtualButtonBehaviourU5BU5D_t5386_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7805_ICollection_1_CopyTo_m43766_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviourU5BU5D_t5386_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43766_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43766_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7805_ICollection_1_CopyTo_m43766_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43766_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
static ParameterInfo ICollection_1_t7805_ICollection_1_Remove_m43767_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t61_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43767_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43767_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7805_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7805_ICollection_1_Remove_m43767_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43767_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7805_MethodInfos[] =
{
	&ICollection_1_get_Count_m43761_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43762_MethodInfo,
	&ICollection_1_Add_m43763_MethodInfo,
	&ICollection_1_Clear_m43764_MethodInfo,
	&ICollection_1_Contains_m43765_MethodInfo,
	&ICollection_1_CopyTo_m43766_MethodInfo,
	&ICollection_1_Remove_m43767_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7807_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7805_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7807_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7805_0_0_0;
extern Il2CppType ICollection_1_t7805_1_0_0;
struct ICollection_1_t7805;
extern Il2CppGenericClass ICollection_1_t7805_GenericClass;
TypeInfo ICollection_1_t7805_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7805_MethodInfos/* methods */
	, ICollection_1_t7805_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7805_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7805_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7805_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7805_0_0_0/* byval_arg */
	, &ICollection_1_t7805_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7805_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonBehaviour>
extern Il2CppType IEnumerator_1_t6116_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43768_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43768_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7807_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6116_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43768_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7807_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43768_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7807_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7807_0_0_0;
extern Il2CppType IEnumerable_1_t7807_1_0_0;
struct IEnumerable_1_t7807;
extern Il2CppGenericClass IEnumerable_1_t7807_GenericClass;
TypeInfo IEnumerable_1_t7807_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7807_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7807_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7807_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7807_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7807_0_0_0/* byval_arg */
	, &IEnumerable_1_t7807_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7807_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7806_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>
extern MethodInfo IList_1_get_Item_m43769_MethodInfo;
extern MethodInfo IList_1_set_Item_m43770_MethodInfo;
static PropertyInfo IList_1_t7806____Item_PropertyInfo = 
{
	&IList_1_t7806_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43769_MethodInfo/* get */
	, &IList_1_set_Item_m43770_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7806_PropertyInfos[] =
{
	&IList_1_t7806____Item_PropertyInfo,
	NULL
};
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
static ParameterInfo IList_1_t7806_IList_1_IndexOf_m43771_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t61_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43771_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43771_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7806_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7806_IList_1_IndexOf_m43771_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43771_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
static ParameterInfo IList_1_t7806_IList_1_Insert_m43772_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t61_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43772_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43772_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7806_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7806_IList_1_Insert_m43772_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43772_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7806_IList_1_RemoveAt_m43773_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43773_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43773_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7806_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7806_IList_1_RemoveAt_m43773_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43773_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7806_IList_1_get_Item_m43769_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43769_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43769_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7806_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonBehaviour_t61_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7806_IList_1_get_Item_m43769_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43769_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
static ParameterInfo IList_1_t7806_IList_1_set_Item_m43770_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t61_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43770_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43770_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7806_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7806_IList_1_set_Item_m43770_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43770_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7806_MethodInfos[] =
{
	&IList_1_IndexOf_m43771_MethodInfo,
	&IList_1_Insert_m43772_MethodInfo,
	&IList_1_RemoveAt_m43773_MethodInfo,
	&IList_1_get_Item_m43769_MethodInfo,
	&IList_1_set_Item_m43770_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7806_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7805_il2cpp_TypeInfo,
	&IEnumerable_1_t7807_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7806_0_0_0;
extern Il2CppType IList_1_t7806_1_0_0;
struct IList_1_t7806;
extern Il2CppGenericClass IList_1_t7806_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7806_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7806_MethodInfos/* methods */
	, IList_1_t7806_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7806_il2cpp_TypeInfo/* element_class */
	, IList_1_t7806_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7806_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7806_0_0_0/* byval_arg */
	, &IList_1_t7806_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7806_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t4382_il2cpp_TypeInfo;

// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43774_MethodInfo;
static PropertyInfo ICollection_1_t4382____Count_PropertyInfo = 
{
	&ICollection_1_t4382_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43774_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43775_MethodInfo;
static PropertyInfo ICollection_1_t4382____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t4382_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43775_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t4382_PropertyInfos[] =
{
	&ICollection_1_t4382____Count_PropertyInfo,
	&ICollection_1_t4382____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43774_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43774_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t4382_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43774_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43775_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43775_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t4382_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43775_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
static ParameterInfo ICollection_1_t4382_ICollection_1_Add_m43776_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43776_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43776_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t4382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t4382_ICollection_1_Add_m43776_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43776_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43777_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43777_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t4382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43777_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
static ParameterInfo ICollection_1_t4382_ICollection_1_Contains_m43778_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43778_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43778_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t4382_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t4382_ICollection_1_Contains_m43778_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43778_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonAbstractBehaviourU5BU5D_t773_0_0_0;
extern Il2CppType VirtualButtonAbstractBehaviourU5BU5D_t773_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t4382_ICollection_1_CopyTo_m43779_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviourU5BU5D_t773_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43779_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43779_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t4382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t4382_ICollection_1_CopyTo_m43779_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43779_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
static ParameterInfo ICollection_1_t4382_ICollection_1_Remove_m43780_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43780_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43780_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t4382_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t4382_ICollection_1_Remove_m43780_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43780_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t4382_MethodInfos[] =
{
	&ICollection_1_get_Count_m43774_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43775_MethodInfo,
	&ICollection_1_Add_m43776_MethodInfo,
	&ICollection_1_Clear_m43777_MethodInfo,
	&ICollection_1_Contains_m43778_MethodInfo,
	&ICollection_1_CopyTo_m43779_MethodInfo,
	&ICollection_1_Remove_m43780_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t795_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t4382_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t795_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t4382_0_0_0;
extern Il2CppType ICollection_1_t4382_1_0_0;
struct ICollection_1_t4382;
extern Il2CppGenericClass ICollection_1_t4382_GenericClass;
TypeInfo ICollection_1_t4382_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t4382_MethodInfos/* methods */
	, ICollection_1_t4382_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t4382_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t4382_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t4382_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t4382_0_0_0/* byval_arg */
	, &ICollection_1_t4382_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t4382_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour>
extern Il2CppType IEnumerator_1_t914_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m5397_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m5397_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t795_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t914_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m5397_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t795_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m5397_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t795_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t795_0_0_0;
extern Il2CppType IEnumerable_1_t795_1_0_0;
struct IEnumerable_1_t795;
extern Il2CppGenericClass IEnumerable_1_t795_GenericClass;
TypeInfo IEnumerable_1_t795_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t795_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t795_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t795_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t795_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t795_0_0_0/* byval_arg */
	, &IEnumerable_1_t795_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t795_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t914_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m5398_MethodInfo;
static PropertyInfo IEnumerator_1_t914____Current_PropertyInfo = 
{
	&IEnumerator_1_t914_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m5398_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t914_PropertyInfos[] =
{
	&IEnumerator_1_t914____Current_PropertyInfo,
	NULL
};
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m5398_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m5398_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t914_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonAbstractBehaviour_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m5398_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t914_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m5398_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t914_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t914_0_0_0;
extern Il2CppType IEnumerator_1_t914_1_0_0;
struct IEnumerator_1_t914;
extern Il2CppGenericClass IEnumerator_1_t914_GenericClass;
TypeInfo IEnumerator_1_t914_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t914_MethodInfos/* methods */
	, IEnumerator_1_t914_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t914_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t914_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t914_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t914_0_0_0/* byval_arg */
	, &IEnumerator_1_t914_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t914_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_100.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2970_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_100MethodDeclarations.h"

extern TypeInfo VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15248_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVirtualButtonAbstractBehaviour_t30_m33405_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VirtualButtonAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VirtualButtonAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisVirtualButtonAbstractBehaviour_t30_m33405(__this, p0, method) (VirtualButtonAbstractBehaviour_t30 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2970____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2970, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2970____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2970, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2970_FieldInfos[] =
{
	&InternalEnumerator_1_t2970____array_0_FieldInfo,
	&InternalEnumerator_1_t2970____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15245_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2970____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2970_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15245_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2970____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2970_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15248_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2970_PropertyInfos[] =
{
	&InternalEnumerator_1_t2970____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2970____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2970_InternalEnumerator_1__ctor_m15244_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15244_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15244_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2970_InternalEnumerator_1__ctor_m15244_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15244_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15245_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15245_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15245_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15246_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15246_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15246_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15247_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15247_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15247_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15248_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15248_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonAbstractBehaviour_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15248_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2970_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15244_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15245_MethodInfo,
	&InternalEnumerator_1_Dispose_m15246_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15247_MethodInfo,
	&InternalEnumerator_1_get_Current_m15248_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15247_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15246_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2970_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15245_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15247_MethodInfo,
	&InternalEnumerator_1_Dispose_m15246_MethodInfo,
	&InternalEnumerator_1_get_Current_m15248_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2970_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t914_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2970_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t914_il2cpp_TypeInfo, 7},
};
extern TypeInfo VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2970_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15248_MethodInfo/* Method Usage */,
	&VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVirtualButtonAbstractBehaviour_t30_m33405_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2970_0_0_0;
extern Il2CppType InternalEnumerator_1_t2970_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2970_GenericClass;
TypeInfo InternalEnumerator_1_t2970_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2970_MethodInfos/* methods */
	, InternalEnumerator_1_t2970_PropertyInfos/* properties */
	, InternalEnumerator_1_t2970_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2970_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2970_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2970_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2970_1_0_0/* this_arg */
	, InternalEnumerator_1_t2970_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2970_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2970_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2970)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t4386_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43781_MethodInfo;
extern MethodInfo IList_1_set_Item_m43782_MethodInfo;
static PropertyInfo IList_1_t4386____Item_PropertyInfo = 
{
	&IList_1_t4386_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43781_MethodInfo/* get */
	, &IList_1_set_Item_m43782_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t4386_PropertyInfos[] =
{
	&IList_1_t4386____Item_PropertyInfo,
	NULL
};
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
static ParameterInfo IList_1_t4386_IList_1_IndexOf_m43783_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43783_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43783_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t4386_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4386_IList_1_IndexOf_m43783_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43783_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
static ParameterInfo IList_1_t4386_IList_1_Insert_m43784_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43784_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43784_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t4386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4386_IList_1_Insert_m43784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43784_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t4386_IList_1_RemoveAt_m43785_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43785_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43785_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t4386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t4386_IList_1_RemoveAt_m43785_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43785_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t4386_IList_1_get_Item_m43781_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43781_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43781_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t4386_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonAbstractBehaviour_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t4386_IList_1_get_Item_m43781_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43781_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
static ParameterInfo IList_1_t4386_IList_1_set_Item_m43782_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43782_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43782_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t4386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4386_IList_1_set_Item_m43782_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43782_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t4386_MethodInfos[] =
{
	&IList_1_IndexOf_m43783_MethodInfo,
	&IList_1_Insert_m43784_MethodInfo,
	&IList_1_RemoveAt_m43785_MethodInfo,
	&IList_1_get_Item_m43781_MethodInfo,
	&IList_1_set_Item_m43782_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t4386_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t4382_il2cpp_TypeInfo,
	&IEnumerable_1_t795_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t4386_0_0_0;
extern Il2CppType IList_1_t4386_1_0_0;
struct IList_1_t4386;
extern Il2CppGenericClass IList_1_t4386_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t4386_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t4386_MethodInfos/* methods */
	, IList_1_t4386_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t4386_il2cpp_TypeInfo/* element_class */
	, IList_1_t4386_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t4386_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t4386_0_0_0/* byval_arg */
	, &IList_1_t4386_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t4386_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7808_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>
extern MethodInfo ICollection_1_get_Count_m43786_MethodInfo;
static PropertyInfo ICollection_1_t7808____Count_PropertyInfo = 
{
	&ICollection_1_t7808_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43786_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43787_MethodInfo;
static PropertyInfo ICollection_1_t7808____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7808_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43787_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7808_PropertyInfos[] =
{
	&ICollection_1_t7808____Count_PropertyInfo,
	&ICollection_1_t7808____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43786_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43786_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7808_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43786_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43787_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43787_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7808_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43787_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorVirtualButtonBehaviour_t169_0_0_0;
extern Il2CppType IEditorVirtualButtonBehaviour_t169_0_0_0;
static ParameterInfo ICollection_1_t7808_ICollection_1_Add_m43788_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t169_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43788_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43788_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7808_ICollection_1_Add_m43788_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43788_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43789_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43789_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43789_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorVirtualButtonBehaviour_t169_0_0_0;
static ParameterInfo ICollection_1_t7808_ICollection_1_Contains_m43790_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t169_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43790_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43790_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7808_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7808_ICollection_1_Contains_m43790_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43790_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorVirtualButtonBehaviourU5BU5D_t5650_0_0_0;
extern Il2CppType IEditorVirtualButtonBehaviourU5BU5D_t5650_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7808_ICollection_1_CopyTo_m43791_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviourU5BU5D_t5650_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43791_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43791_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7808_ICollection_1_CopyTo_m43791_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43791_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorVirtualButtonBehaviour_t169_0_0_0;
static ParameterInfo ICollection_1_t7808_ICollection_1_Remove_m43792_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t169_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43792_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43792_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7808_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7808_ICollection_1_Remove_m43792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43792_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7808_MethodInfos[] =
{
	&ICollection_1_get_Count_m43786_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43787_MethodInfo,
	&ICollection_1_Add_m43788_MethodInfo,
	&ICollection_1_Clear_m43789_MethodInfo,
	&ICollection_1_Contains_m43790_MethodInfo,
	&ICollection_1_CopyTo_m43791_MethodInfo,
	&ICollection_1_Remove_m43792_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7810_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7808_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7810_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7808_0_0_0;
extern Il2CppType ICollection_1_t7808_1_0_0;
struct ICollection_1_t7808;
extern Il2CppGenericClass ICollection_1_t7808_GenericClass;
TypeInfo ICollection_1_t7808_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7808_MethodInfos/* methods */
	, ICollection_1_t7808_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7808_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7808_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7808_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7808_0_0_0/* byval_arg */
	, &ICollection_1_t7808_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7808_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorVirtualButtonBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorVirtualButtonBehaviour>
extern Il2CppType IEnumerator_1_t6118_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43793_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorVirtualButtonBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43793_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7810_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6118_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43793_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7810_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43793_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7810_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7810_0_0_0;
extern Il2CppType IEnumerable_1_t7810_1_0_0;
struct IEnumerable_1_t7810;
extern Il2CppGenericClass IEnumerable_1_t7810_GenericClass;
TypeInfo IEnumerable_1_t7810_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7810_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7810_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7810_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7810_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7810_0_0_0/* byval_arg */
	, &IEnumerable_1_t7810_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7810_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6118_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43794_MethodInfo;
static PropertyInfo IEnumerator_1_t6118____Current_PropertyInfo = 
{
	&IEnumerator_1_t6118_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43794_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6118_PropertyInfos[] =
{
	&IEnumerator_1_t6118____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorVirtualButtonBehaviour_t169_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43794_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43794_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6118_il2cpp_TypeInfo/* declaring_type */
	, &IEditorVirtualButtonBehaviour_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43794_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6118_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43794_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6118_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6118_0_0_0;
extern Il2CppType IEnumerator_1_t6118_1_0_0;
struct IEnumerator_1_t6118;
extern Il2CppGenericClass IEnumerator_1_t6118_GenericClass;
TypeInfo IEnumerator_1_t6118_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6118_MethodInfos/* methods */
	, IEnumerator_1_t6118_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6118_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6118_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6118_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6118_0_0_0/* byval_arg */
	, &IEnumerator_1_t6118_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6118_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_101.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2971_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_101MethodDeclarations.h"

extern TypeInfo IEditorVirtualButtonBehaviour_t169_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15253_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorVirtualButtonBehaviour_t169_m33416_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorVirtualButtonBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorVirtualButtonBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorVirtualButtonBehaviour_t169_m33416(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2971____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2971, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2971____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2971, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2971_FieldInfos[] =
{
	&InternalEnumerator_1_t2971____array_0_FieldInfo,
	&InternalEnumerator_1_t2971____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15250_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2971____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2971_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15250_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2971____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2971_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15253_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2971_PropertyInfos[] =
{
	&InternalEnumerator_1_t2971____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2971____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2971_InternalEnumerator_1__ctor_m15249_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15249_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15249_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2971_InternalEnumerator_1__ctor_m15249_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15249_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15250_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15250_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15250_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15251_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15251_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15251_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15252_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15252_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15252_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorVirtualButtonBehaviour_t169_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15253_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15253_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* declaring_type */
	, &IEditorVirtualButtonBehaviour_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15253_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2971_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15249_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15250_MethodInfo,
	&InternalEnumerator_1_Dispose_m15251_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15252_MethodInfo,
	&InternalEnumerator_1_get_Current_m15253_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15252_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15251_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2971_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15250_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15252_MethodInfo,
	&InternalEnumerator_1_Dispose_m15251_MethodInfo,
	&InternalEnumerator_1_get_Current_m15253_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2971_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6118_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2971_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6118_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorVirtualButtonBehaviour_t169_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2971_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15253_MethodInfo/* Method Usage */,
	&IEditorVirtualButtonBehaviour_t169_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorVirtualButtonBehaviour_t169_m33416_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2971_0_0_0;
extern Il2CppType InternalEnumerator_1_t2971_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2971_GenericClass;
TypeInfo InternalEnumerator_1_t2971_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2971_MethodInfos/* methods */
	, InternalEnumerator_1_t2971_PropertyInfos/* properties */
	, InternalEnumerator_1_t2971_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2971_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2971_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2971_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2971_1_0_0/* this_arg */
	, InternalEnumerator_1_t2971_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2971_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2971_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2971)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7809_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>
extern MethodInfo IList_1_get_Item_m43795_MethodInfo;
extern MethodInfo IList_1_set_Item_m43796_MethodInfo;
static PropertyInfo IList_1_t7809____Item_PropertyInfo = 
{
	&IList_1_t7809_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43795_MethodInfo/* get */
	, &IList_1_set_Item_m43796_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7809_PropertyInfos[] =
{
	&IList_1_t7809____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorVirtualButtonBehaviour_t169_0_0_0;
static ParameterInfo IList_1_t7809_IList_1_IndexOf_m43797_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t169_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43797_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43797_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7809_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7809_IList_1_IndexOf_m43797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43797_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorVirtualButtonBehaviour_t169_0_0_0;
static ParameterInfo IList_1_t7809_IList_1_Insert_m43798_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t169_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43798_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43798_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7809_IList_1_Insert_m43798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43798_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7809_IList_1_RemoveAt_m43799_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43799_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43799_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7809_IList_1_RemoveAt_m43799_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43799_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7809_IList_1_get_Item_m43795_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorVirtualButtonBehaviour_t169_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43795_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43795_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7809_il2cpp_TypeInfo/* declaring_type */
	, &IEditorVirtualButtonBehaviour_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7809_IList_1_get_Item_m43795_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43795_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorVirtualButtonBehaviour_t169_0_0_0;
static ParameterInfo IList_1_t7809_IList_1_set_Item_m43796_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t169_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43796_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43796_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7809_IList_1_set_Item_m43796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43796_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7809_MethodInfos[] =
{
	&IList_1_IndexOf_m43797_MethodInfo,
	&IList_1_Insert_m43798_MethodInfo,
	&IList_1_RemoveAt_m43799_MethodInfo,
	&IList_1_get_Item_m43795_MethodInfo,
	&IList_1_set_Item_m43796_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7809_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7808_il2cpp_TypeInfo,
	&IEnumerable_1_t7810_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7809_0_0_0;
extern Il2CppType IList_1_t7809_1_0_0;
struct IList_1_t7809;
extern Il2CppGenericClass IList_1_t7809_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7809_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7809_MethodInfos/* methods */
	, IList_1_t7809_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7809_il2cpp_TypeInfo/* element_class */
	, IList_1_t7809_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7809_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7809_0_0_0/* byval_arg */
	, &IList_1_t7809_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7809_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_32.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2972_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_32MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_28.h"
extern TypeInfo InvokableCall_1_t2973_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_28MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15256_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15258_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2972____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2972_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2972, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2972_FieldInfos[] =
{
	&CachedInvokableCall_1_t2972____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2972_CachedInvokableCall_1__ctor_m15254_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t61_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15254_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15254_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2972_CachedInvokableCall_1__ctor_m15254_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15254_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2972_CachedInvokableCall_1_Invoke_m15255_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15255_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15255_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2972_CachedInvokableCall_1_Invoke_m15255_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15255_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2972_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15254_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15255_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15255_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15259_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2972_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15255_MethodInfo,
	&InvokableCall_1_Find_m15259_MethodInfo,
};
extern Il2CppType UnityAction_1_t2974_0_0_0;
extern TypeInfo UnityAction_1_t2974_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisVirtualButtonBehaviour_t61_m33426_MethodInfo;
extern TypeInfo VirtualButtonBehaviour_t61_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15261_MethodInfo;
extern TypeInfo VirtualButtonBehaviour_t61_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2972_RGCTXData[8] = 
{
	&UnityAction_1_t2974_0_0_0/* Type Usage */,
	&UnityAction_1_t2974_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVirtualButtonBehaviour_t61_m33426_MethodInfo/* Method Usage */,
	&VirtualButtonBehaviour_t61_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15261_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15256_MethodInfo/* Method Usage */,
	&VirtualButtonBehaviour_t61_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15258_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2972_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2972_1_0_0;
struct CachedInvokableCall_1_t2972;
extern Il2CppGenericClass CachedInvokableCall_1_t2972_GenericClass;
TypeInfo CachedInvokableCall_1_t2972_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2972_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2972_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2973_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2972_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2972_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2972_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2972_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2972_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2972_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2972_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2972)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_35.h"
extern TypeInfo UnityAction_1_t2974_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_35MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VirtualButtonBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VirtualButtonBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisVirtualButtonBehaviour_t61_m33426(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>
extern Il2CppType UnityAction_1_t2974_0_0_1;
FieldInfo InvokableCall_1_t2973____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2974_0_0_1/* type */
	, &InvokableCall_1_t2973_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2973, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2973_FieldInfos[] =
{
	&InvokableCall_1_t2973____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2973_InvokableCall_1__ctor_m15256_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15256_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15256_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2973_InvokableCall_1__ctor_m15256_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15256_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2974_0_0_0;
static ParameterInfo InvokableCall_1_t2973_InvokableCall_1__ctor_m15257_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2974_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15257_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15257_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2973_InvokableCall_1__ctor_m15257_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15257_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2973_InvokableCall_1_Invoke_m15258_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15258_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15258_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2973_InvokableCall_1_Invoke_m15258_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15258_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2973_InvokableCall_1_Find_m15259_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15259_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15259_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2973_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2973_InvokableCall_1_Find_m15259_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15259_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2973_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15256_MethodInfo,
	&InvokableCall_1__ctor_m15257_MethodInfo,
	&InvokableCall_1_Invoke_m15258_MethodInfo,
	&InvokableCall_1_Find_m15259_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2973_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15258_MethodInfo,
	&InvokableCall_1_Find_m15259_MethodInfo,
};
extern TypeInfo UnityAction_1_t2974_il2cpp_TypeInfo;
extern TypeInfo VirtualButtonBehaviour_t61_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2973_RGCTXData[5] = 
{
	&UnityAction_1_t2974_0_0_0/* Type Usage */,
	&UnityAction_1_t2974_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVirtualButtonBehaviour_t61_m33426_MethodInfo/* Method Usage */,
	&VirtualButtonBehaviour_t61_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15261_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2973_0_0_0;
extern Il2CppType InvokableCall_1_t2973_1_0_0;
struct InvokableCall_1_t2973;
extern Il2CppGenericClass InvokableCall_1_t2973_GenericClass;
TypeInfo InvokableCall_1_t2973_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2973_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2973_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2973_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2973_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2973_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2973_0_0_0/* byval_arg */
	, &InvokableCall_1_t2973_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2973_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2973_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2973)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2974_UnityAction_1__ctor_m15260_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15260_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15260_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2974_UnityAction_1__ctor_m15260_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15260_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
static ParameterInfo UnityAction_1_t2974_UnityAction_1_Invoke_m15261_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t61_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15261_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15261_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2974_UnityAction_1_Invoke_m15261_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15261_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t61_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2974_UnityAction_1_BeginInvoke_m15262_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t61_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15262_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15262_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2974_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2974_UnityAction_1_BeginInvoke_m15262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15262_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2974_UnityAction_1_EndInvoke_m15263_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15263_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15263_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2974_UnityAction_1_EndInvoke_m15263_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15263_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2974_MethodInfos[] =
{
	&UnityAction_1__ctor_m15260_MethodInfo,
	&UnityAction_1_Invoke_m15261_MethodInfo,
	&UnityAction_1_BeginInvoke_m15262_MethodInfo,
	&UnityAction_1_EndInvoke_m15263_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15262_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15263_MethodInfo;
static MethodInfo* UnityAction_1_t2974_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15261_MethodInfo,
	&UnityAction_1_BeginInvoke_m15262_MethodInfo,
	&UnityAction_1_EndInvoke_m15263_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2974_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2974_1_0_0;
struct UnityAction_1_t2974;
extern Il2CppGenericClass UnityAction_1_t2974_GenericClass;
TypeInfo UnityAction_1_t2974_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2974_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2974_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2974_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2974_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2974_0_0_0/* byval_arg */
	, &UnityAction_1_t2974_1_0_0/* this_arg */
	, UnityAction_1_t2974_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2974_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2974)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6120_il2cpp_TypeInfo;

// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.WebCamBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WebCamBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43800_MethodInfo;
static PropertyInfo IEnumerator_1_t6120____Current_PropertyInfo = 
{
	&IEnumerator_1_t6120_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43800_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6120_PropertyInfos[] =
{
	&IEnumerator_1_t6120____Current_PropertyInfo,
	NULL
};
extern Il2CppType WebCamBehaviour_t62_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43800_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WebCamBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43800_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6120_il2cpp_TypeInfo/* declaring_type */
	, &WebCamBehaviour_t62_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43800_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6120_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43800_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6120_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6120_0_0_0;
extern Il2CppType IEnumerator_1_t6120_1_0_0;
struct IEnumerator_1_t6120;
extern Il2CppGenericClass IEnumerator_1_t6120_GenericClass;
TypeInfo IEnumerator_1_t6120_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6120_MethodInfos/* methods */
	, IEnumerator_1_t6120_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6120_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6120_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6120_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6120_0_0_0/* byval_arg */
	, &IEnumerator_1_t6120_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6120_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_102.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2975_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_102MethodDeclarations.h"

extern TypeInfo WebCamBehaviour_t62_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15268_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWebCamBehaviour_t62_m33428_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WebCamBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WebCamBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisWebCamBehaviour_t62_m33428(__this, p0, method) (WebCamBehaviour_t62 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2975____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2975_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2975, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2975____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2975_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2975, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2975_FieldInfos[] =
{
	&InternalEnumerator_1_t2975____array_0_FieldInfo,
	&InternalEnumerator_1_t2975____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15265_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2975____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2975_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15265_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2975____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2975_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15268_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2975_PropertyInfos[] =
{
	&InternalEnumerator_1_t2975____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2975____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2975_InternalEnumerator_1__ctor_m15264_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15264_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15264_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2975_InternalEnumerator_1__ctor_m15264_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15264_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15265_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15265_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2975_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15265_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15266_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15266_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15266_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15267_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15267_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2975_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15267_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t62_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15268_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15268_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2975_il2cpp_TypeInfo/* declaring_type */
	, &WebCamBehaviour_t62_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15268_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2975_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15264_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15265_MethodInfo,
	&InternalEnumerator_1_Dispose_m15266_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15267_MethodInfo,
	&InternalEnumerator_1_get_Current_m15268_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15267_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15266_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2975_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15265_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15267_MethodInfo,
	&InternalEnumerator_1_Dispose_m15266_MethodInfo,
	&InternalEnumerator_1_get_Current_m15268_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2975_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6120_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2975_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6120_il2cpp_TypeInfo, 7},
};
extern TypeInfo WebCamBehaviour_t62_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2975_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15268_MethodInfo/* Method Usage */,
	&WebCamBehaviour_t62_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWebCamBehaviour_t62_m33428_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2975_0_0_0;
extern Il2CppType InternalEnumerator_1_t2975_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2975_GenericClass;
TypeInfo InternalEnumerator_1_t2975_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2975_MethodInfos/* methods */
	, InternalEnumerator_1_t2975_PropertyInfos/* properties */
	, InternalEnumerator_1_t2975_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2975_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2975_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2975_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2975_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2975_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2975_1_0_0/* this_arg */
	, InternalEnumerator_1_t2975_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2975_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2975_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2975)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7811_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>
extern MethodInfo ICollection_1_get_Count_m43801_MethodInfo;
static PropertyInfo ICollection_1_t7811____Count_PropertyInfo = 
{
	&ICollection_1_t7811_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43801_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43802_MethodInfo;
static PropertyInfo ICollection_1_t7811____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7811_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43802_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7811_PropertyInfos[] =
{
	&ICollection_1_t7811____Count_PropertyInfo,
	&ICollection_1_t7811____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43801_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43801_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7811_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43801_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43802_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43802_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7811_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43802_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t62_0_0_0;
extern Il2CppType WebCamBehaviour_t62_0_0_0;
static ParameterInfo ICollection_1_t7811_ICollection_1_Add_m43803_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t62_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43803_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43803_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7811_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7811_ICollection_1_Add_m43803_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43803_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43804_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43804_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7811_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43804_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t62_0_0_0;
static ParameterInfo ICollection_1_t7811_ICollection_1_Contains_m43805_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t62_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43805_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43805_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7811_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7811_ICollection_1_Contains_m43805_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43805_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviourU5BU5D_t5387_0_0_0;
extern Il2CppType WebCamBehaviourU5BU5D_t5387_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7811_ICollection_1_CopyTo_m43806_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviourU5BU5D_t5387_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43806_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43806_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7811_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7811_ICollection_1_CopyTo_m43806_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43806_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t62_0_0_0;
static ParameterInfo ICollection_1_t7811_ICollection_1_Remove_m43807_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t62_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43807_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43807_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7811_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7811_ICollection_1_Remove_m43807_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43807_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7811_MethodInfos[] =
{
	&ICollection_1_get_Count_m43801_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43802_MethodInfo,
	&ICollection_1_Add_m43803_MethodInfo,
	&ICollection_1_Clear_m43804_MethodInfo,
	&ICollection_1_Contains_m43805_MethodInfo,
	&ICollection_1_CopyTo_m43806_MethodInfo,
	&ICollection_1_Remove_m43807_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7813_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7811_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7813_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7811_0_0_0;
extern Il2CppType ICollection_1_t7811_1_0_0;
struct ICollection_1_t7811;
extern Il2CppGenericClass ICollection_1_t7811_GenericClass;
TypeInfo ICollection_1_t7811_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7811_MethodInfos/* methods */
	, ICollection_1_t7811_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7811_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7811_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7811_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7811_0_0_0/* byval_arg */
	, &ICollection_1_t7811_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7811_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WebCamBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WebCamBehaviour>
extern Il2CppType IEnumerator_1_t6120_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43808_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WebCamBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43808_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7813_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6120_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43808_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7813_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43808_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7813_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7813_0_0_0;
extern Il2CppType IEnumerable_1_t7813_1_0_0;
struct IEnumerable_1_t7813;
extern Il2CppGenericClass IEnumerable_1_t7813_GenericClass;
TypeInfo IEnumerable_1_t7813_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7813_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7813_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7813_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7813_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7813_0_0_0/* byval_arg */
	, &IEnumerable_1_t7813_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7813_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7812_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>
extern MethodInfo IList_1_get_Item_m43809_MethodInfo;
extern MethodInfo IList_1_set_Item_m43810_MethodInfo;
static PropertyInfo IList_1_t7812____Item_PropertyInfo = 
{
	&IList_1_t7812_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43809_MethodInfo/* get */
	, &IList_1_set_Item_m43810_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7812_PropertyInfos[] =
{
	&IList_1_t7812____Item_PropertyInfo,
	NULL
};
extern Il2CppType WebCamBehaviour_t62_0_0_0;
static ParameterInfo IList_1_t7812_IList_1_IndexOf_m43811_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t62_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43811_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43811_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7812_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7812_IList_1_IndexOf_m43811_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43811_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WebCamBehaviour_t62_0_0_0;
static ParameterInfo IList_1_t7812_IList_1_Insert_m43812_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t62_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43812_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43812_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7812_IList_1_Insert_m43812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43812_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7812_IList_1_RemoveAt_m43813_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43813_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43813_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7812_IList_1_RemoveAt_m43813_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43813_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7812_IList_1_get_Item_m43809_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType WebCamBehaviour_t62_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43809_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43809_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7812_il2cpp_TypeInfo/* declaring_type */
	, &WebCamBehaviour_t62_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7812_IList_1_get_Item_m43809_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43809_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WebCamBehaviour_t62_0_0_0;
static ParameterInfo IList_1_t7812_IList_1_set_Item_m43810_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t62_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43810_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43810_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7812_IList_1_set_Item_m43810_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43810_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7812_MethodInfos[] =
{
	&IList_1_IndexOf_m43811_MethodInfo,
	&IList_1_Insert_m43812_MethodInfo,
	&IList_1_RemoveAt_m43813_MethodInfo,
	&IList_1_get_Item_m43809_MethodInfo,
	&IList_1_set_Item_m43810_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7812_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7811_il2cpp_TypeInfo,
	&IEnumerable_1_t7813_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7812_0_0_0;
extern Il2CppType IList_1_t7812_1_0_0;
struct IList_1_t7812;
extern Il2CppGenericClass IList_1_t7812_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7812_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7812_MethodInfos/* methods */
	, IList_1_t7812_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7812_il2cpp_TypeInfo/* element_class */
	, IList_1_t7812_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7812_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7812_0_0_0/* byval_arg */
	, &IList_1_t7812_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7812_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7814_il2cpp_TypeInfo;

// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehav.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m43814_MethodInfo;
static PropertyInfo ICollection_1_t7814____Count_PropertyInfo = 
{
	&ICollection_1_t7814_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43814_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43815_MethodInfo;
static PropertyInfo ICollection_1_t7814____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7814_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43815_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7814_PropertyInfos[] =
{
	&ICollection_1_t7814____Count_PropertyInfo,
	&ICollection_1_t7814____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43814_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43814_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7814_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43814_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43815_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43815_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7814_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43815_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
static ParameterInfo ICollection_1_t7814_ICollection_1_Add_m43816_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t63_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43816_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43816_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7814_ICollection_1_Add_m43816_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43816_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43817_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43817_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43817_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
static ParameterInfo ICollection_1_t7814_ICollection_1_Contains_m43818_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t63_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43818_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43818_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7814_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7814_ICollection_1_Contains_m43818_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43818_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamAbstractBehaviourU5BU5D_t5651_0_0_0;
extern Il2CppType WebCamAbstractBehaviourU5BU5D_t5651_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7814_ICollection_1_CopyTo_m43819_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviourU5BU5D_t5651_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43819_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43819_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7814_ICollection_1_CopyTo_m43819_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43819_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
static ParameterInfo ICollection_1_t7814_ICollection_1_Remove_m43820_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t63_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43820_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43820_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7814_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7814_ICollection_1_Remove_m43820_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43820_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7814_MethodInfos[] =
{
	&ICollection_1_get_Count_m43814_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43815_MethodInfo,
	&ICollection_1_Add_m43816_MethodInfo,
	&ICollection_1_Clear_m43817_MethodInfo,
	&ICollection_1_Contains_m43818_MethodInfo,
	&ICollection_1_CopyTo_m43819_MethodInfo,
	&ICollection_1_Remove_m43820_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7816_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7814_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7816_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7814_0_0_0;
extern Il2CppType ICollection_1_t7814_1_0_0;
struct ICollection_1_t7814;
extern Il2CppGenericClass ICollection_1_t7814_GenericClass;
TypeInfo ICollection_1_t7814_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7814_MethodInfos/* methods */
	, ICollection_1_t7814_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7814_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7814_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7814_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7814_0_0_0/* byval_arg */
	, &ICollection_1_t7814_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7814_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WebCamAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WebCamAbstractBehaviour>
extern Il2CppType IEnumerator_1_t6122_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43821_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WebCamAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43821_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7816_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43821_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7816_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43821_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7816_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7816_0_0_0;
extern Il2CppType IEnumerable_1_t7816_1_0_0;
struct IEnumerable_1_t7816;
extern Il2CppGenericClass IEnumerable_1_t7816_GenericClass;
TypeInfo IEnumerable_1_t7816_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7816_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7816_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7816_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7816_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7816_0_0_0/* byval_arg */
	, &IEnumerable_1_t7816_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7816_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6122_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.WebCamAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WebCamAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43822_MethodInfo;
static PropertyInfo IEnumerator_1_t6122____Current_PropertyInfo = 
{
	&IEnumerator_1_t6122_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43822_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6122_PropertyInfos[] =
{
	&IEnumerator_1_t6122____Current_PropertyInfo,
	NULL
};
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43822_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WebCamAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43822_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6122_il2cpp_TypeInfo/* declaring_type */
	, &WebCamAbstractBehaviour_t63_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43822_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6122_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43822_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6122_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6122_0_0_0;
extern Il2CppType IEnumerator_1_t6122_1_0_0;
struct IEnumerator_1_t6122;
extern Il2CppGenericClass IEnumerator_1_t6122_GenericClass;
TypeInfo IEnumerator_1_t6122_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6122_MethodInfos/* methods */
	, IEnumerator_1_t6122_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6122_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6122_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6122_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6122_0_0_0/* byval_arg */
	, &IEnumerator_1_t6122_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6122_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_103.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2976_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_103MethodDeclarations.h"

extern TypeInfo WebCamAbstractBehaviour_t63_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15273_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWebCamAbstractBehaviour_t63_m33439_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WebCamAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WebCamAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisWebCamAbstractBehaviour_t63_m33439(__this, p0, method) (WebCamAbstractBehaviour_t63 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2976____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2976, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2976____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2976, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2976_FieldInfos[] =
{
	&InternalEnumerator_1_t2976____array_0_FieldInfo,
	&InternalEnumerator_1_t2976____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15270_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2976____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2976_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15270_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2976____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2976_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15273_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2976_PropertyInfos[] =
{
	&InternalEnumerator_1_t2976____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2976____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2976_InternalEnumerator_1__ctor_m15269_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15269_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15269_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2976_InternalEnumerator_1__ctor_m15269_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15269_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15270_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15270_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15270_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15271_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15271_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15271_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15272_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15272_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15272_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15273_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15273_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* declaring_type */
	, &WebCamAbstractBehaviour_t63_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15273_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2976_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15269_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15270_MethodInfo,
	&InternalEnumerator_1_Dispose_m15271_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15272_MethodInfo,
	&InternalEnumerator_1_get_Current_m15273_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15272_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15271_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2976_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15270_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15272_MethodInfo,
	&InternalEnumerator_1_Dispose_m15271_MethodInfo,
	&InternalEnumerator_1_get_Current_m15273_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2976_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6122_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2976_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6122_il2cpp_TypeInfo, 7},
};
extern TypeInfo WebCamAbstractBehaviour_t63_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2976_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15273_MethodInfo/* Method Usage */,
	&WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWebCamAbstractBehaviour_t63_m33439_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2976_0_0_0;
extern Il2CppType InternalEnumerator_1_t2976_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2976_GenericClass;
TypeInfo InternalEnumerator_1_t2976_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2976_MethodInfos/* methods */
	, InternalEnumerator_1_t2976_PropertyInfos/* properties */
	, InternalEnumerator_1_t2976_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2976_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2976_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2976_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2976_1_0_0/* this_arg */
	, InternalEnumerator_1_t2976_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2976_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2976_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2976)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7815_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m43823_MethodInfo;
extern MethodInfo IList_1_set_Item_m43824_MethodInfo;
static PropertyInfo IList_1_t7815____Item_PropertyInfo = 
{
	&IList_1_t7815_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43823_MethodInfo/* get */
	, &IList_1_set_Item_m43824_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7815_PropertyInfos[] =
{
	&IList_1_t7815____Item_PropertyInfo,
	NULL
};
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
static ParameterInfo IList_1_t7815_IList_1_IndexOf_m43825_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t63_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43825_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43825_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7815_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7815_IList_1_IndexOf_m43825_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43825_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
static ParameterInfo IList_1_t7815_IList_1_Insert_m43826_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t63_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43826_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43826_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7815_IList_1_Insert_m43826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43826_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7815_IList_1_RemoveAt_m43827_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43827_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43827_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7815_IList_1_RemoveAt_m43827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43827_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7815_IList_1_get_Item_m43823_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43823_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43823_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7815_il2cpp_TypeInfo/* declaring_type */
	, &WebCamAbstractBehaviour_t63_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7815_IList_1_get_Item_m43823_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43823_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
static ParameterInfo IList_1_t7815_IList_1_set_Item_m43824_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t63_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43824_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43824_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7815_IList_1_set_Item_m43824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43824_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7815_MethodInfos[] =
{
	&IList_1_IndexOf_m43825_MethodInfo,
	&IList_1_Insert_m43826_MethodInfo,
	&IList_1_RemoveAt_m43827_MethodInfo,
	&IList_1_get_Item_m43823_MethodInfo,
	&IList_1_set_Item_m43824_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7815_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7814_il2cpp_TypeInfo,
	&IEnumerable_1_t7816_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7815_0_0_0;
extern Il2CppType IList_1_t7815_1_0_0;
struct IList_1_t7815;
extern Il2CppGenericClass IList_1_t7815_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7815_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7815_MethodInfos/* methods */
	, IList_1_t7815_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7815_il2cpp_TypeInfo/* element_class */
	, IList_1_t7815_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7815_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7815_0_0_0/* byval_arg */
	, &IList_1_t7815_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7815_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_33.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2977_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_33MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_29.h"
extern TypeInfo InvokableCall_1_t2978_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_29MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15276_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15278_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2977____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2977_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2977, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2977_FieldInfos[] =
{
	&CachedInvokableCall_1_t2977____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType WebCamBehaviour_t62_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2977_CachedInvokableCall_1__ctor_m15274_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t62_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15274_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15274_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2977_CachedInvokableCall_1__ctor_m15274_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15274_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2977_CachedInvokableCall_1_Invoke_m15275_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15275_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15275_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2977_CachedInvokableCall_1_Invoke_m15275_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15275_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2977_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15274_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15275_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15275_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15279_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2977_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15275_MethodInfo,
	&InvokableCall_1_Find_m15279_MethodInfo,
};
extern Il2CppType UnityAction_1_t2979_0_0_0;
extern TypeInfo UnityAction_1_t2979_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisWebCamBehaviour_t62_m33449_MethodInfo;
extern TypeInfo WebCamBehaviour_t62_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15281_MethodInfo;
extern TypeInfo WebCamBehaviour_t62_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2977_RGCTXData[8] = 
{
	&UnityAction_1_t2979_0_0_0/* Type Usage */,
	&UnityAction_1_t2979_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWebCamBehaviour_t62_m33449_MethodInfo/* Method Usage */,
	&WebCamBehaviour_t62_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15281_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15276_MethodInfo/* Method Usage */,
	&WebCamBehaviour_t62_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15278_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2977_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2977_1_0_0;
struct CachedInvokableCall_1_t2977;
extern Il2CppGenericClass CachedInvokableCall_1_t2977_GenericClass;
TypeInfo CachedInvokableCall_1_t2977_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2977_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2977_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2978_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2977_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2977_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2977_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2977_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2977_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2977_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2977_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2977)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_36.h"
extern TypeInfo UnityAction_1_t2979_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_36MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.WebCamBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.WebCamBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisWebCamBehaviour_t62_m33449(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>
extern Il2CppType UnityAction_1_t2979_0_0_1;
FieldInfo InvokableCall_1_t2978____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2979_0_0_1/* type */
	, &InvokableCall_1_t2978_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2978, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2978_FieldInfos[] =
{
	&InvokableCall_1_t2978____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2978_InvokableCall_1__ctor_m15276_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15276_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15276_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2978_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2978_InvokableCall_1__ctor_m15276_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15276_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2979_0_0_0;
static ParameterInfo InvokableCall_1_t2978_InvokableCall_1__ctor_m15277_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2979_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15277_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15277_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2978_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2978_InvokableCall_1__ctor_m15277_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15277_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2978_InvokableCall_1_Invoke_m15278_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15278_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15278_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2978_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2978_InvokableCall_1_Invoke_m15278_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15278_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2978_InvokableCall_1_Find_m15279_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15279_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15279_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2978_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2978_InvokableCall_1_Find_m15279_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15279_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2978_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15276_MethodInfo,
	&InvokableCall_1__ctor_m15277_MethodInfo,
	&InvokableCall_1_Invoke_m15278_MethodInfo,
	&InvokableCall_1_Find_m15279_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2978_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15278_MethodInfo,
	&InvokableCall_1_Find_m15279_MethodInfo,
};
extern TypeInfo UnityAction_1_t2979_il2cpp_TypeInfo;
extern TypeInfo WebCamBehaviour_t62_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2978_RGCTXData[5] = 
{
	&UnityAction_1_t2979_0_0_0/* Type Usage */,
	&UnityAction_1_t2979_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWebCamBehaviour_t62_m33449_MethodInfo/* Method Usage */,
	&WebCamBehaviour_t62_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15281_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2978_0_0_0;
extern Il2CppType InvokableCall_1_t2978_1_0_0;
struct InvokableCall_1_t2978;
extern Il2CppGenericClass InvokableCall_1_t2978_GenericClass;
TypeInfo InvokableCall_1_t2978_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2978_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2978_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2978_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2978_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2978_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2978_0_0_0/* byval_arg */
	, &InvokableCall_1_t2978_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2978_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2978_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2978)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2979_UnityAction_1__ctor_m15280_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15280_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15280_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2979_UnityAction_1__ctor_m15280_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15280_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t62_0_0_0;
static ParameterInfo UnityAction_1_t2979_UnityAction_1_Invoke_m15281_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t62_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15281_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15281_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2979_UnityAction_1_Invoke_m15281_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15281_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t62_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2979_UnityAction_1_BeginInvoke_m15282_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t62_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15282_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15282_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2979_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2979_UnityAction_1_BeginInvoke_m15282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15282_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2979_UnityAction_1_EndInvoke_m15283_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15283_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15283_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2979_UnityAction_1_EndInvoke_m15283_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15283_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2979_MethodInfos[] =
{
	&UnityAction_1__ctor_m15280_MethodInfo,
	&UnityAction_1_Invoke_m15281_MethodInfo,
	&UnityAction_1_BeginInvoke_m15282_MethodInfo,
	&UnityAction_1_EndInvoke_m15283_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15282_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15283_MethodInfo;
static MethodInfo* UnityAction_1_t2979_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15281_MethodInfo,
	&UnityAction_1_BeginInvoke_m15282_MethodInfo,
	&UnityAction_1_EndInvoke_m15283_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2979_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2979_1_0_0;
struct UnityAction_1_t2979;
extern Il2CppGenericClass UnityAction_1_t2979_GenericClass;
TypeInfo UnityAction_1_t2979_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2979_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2979_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2979_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2979_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2979_0_0_0/* byval_arg */
	, &UnityAction_1_t2979_1_0_0/* this_arg */
	, UnityAction_1_t2979_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2979_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2979)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6124_il2cpp_TypeInfo;

// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.WireframeBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WireframeBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43828_MethodInfo;
static PropertyInfo IEnumerator_1_t6124____Current_PropertyInfo = 
{
	&IEnumerator_1_t6124_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43828_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6124_PropertyInfos[] =
{
	&IEnumerator_1_t6124____Current_PropertyInfo,
	NULL
};
extern Il2CppType WireframeBehaviour_t65_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43828_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WireframeBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43828_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6124_il2cpp_TypeInfo/* declaring_type */
	, &WireframeBehaviour_t65_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43828_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6124_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43828_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6124_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6124_0_0_0;
extern Il2CppType IEnumerator_1_t6124_1_0_0;
struct IEnumerator_1_t6124;
extern Il2CppGenericClass IEnumerator_1_t6124_GenericClass;
TypeInfo IEnumerator_1_t6124_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6124_MethodInfos/* methods */
	, IEnumerator_1_t6124_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6124_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6124_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6124_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6124_0_0_0/* byval_arg */
	, &IEnumerator_1_t6124_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6124_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_104.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2980_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_104MethodDeclarations.h"

extern TypeInfo WireframeBehaviour_t65_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15288_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWireframeBehaviour_t65_m33451_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WireframeBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WireframeBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisWireframeBehaviour_t65_m33451(__this, p0, method) (WireframeBehaviour_t65 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2980____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2980_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2980, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2980____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2980_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2980, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2980_FieldInfos[] =
{
	&InternalEnumerator_1_t2980____array_0_FieldInfo,
	&InternalEnumerator_1_t2980____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15285_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2980____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2980_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15285_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2980____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2980_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2980_PropertyInfos[] =
{
	&InternalEnumerator_1_t2980____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2980____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2980_InternalEnumerator_1__ctor_m15284_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15284_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15284_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2980_InternalEnumerator_1__ctor_m15284_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15284_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15285_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15285_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2980_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15285_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15286_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15286_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15286_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15287_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15287_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2980_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15287_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviour_t65_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15288_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15288_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2980_il2cpp_TypeInfo/* declaring_type */
	, &WireframeBehaviour_t65_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15288_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2980_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15284_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15285_MethodInfo,
	&InternalEnumerator_1_Dispose_m15286_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15287_MethodInfo,
	&InternalEnumerator_1_get_Current_m15288_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15287_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15286_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2980_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15285_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15287_MethodInfo,
	&InternalEnumerator_1_Dispose_m15286_MethodInfo,
	&InternalEnumerator_1_get_Current_m15288_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2980_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6124_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2980_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6124_il2cpp_TypeInfo, 7},
};
extern TypeInfo WireframeBehaviour_t65_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2980_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15288_MethodInfo/* Method Usage */,
	&WireframeBehaviour_t65_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWireframeBehaviour_t65_m33451_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2980_0_0_0;
extern Il2CppType InternalEnumerator_1_t2980_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2980_GenericClass;
TypeInfo InternalEnumerator_1_t2980_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2980_MethodInfos/* methods */
	, InternalEnumerator_1_t2980_PropertyInfos/* properties */
	, InternalEnumerator_1_t2980_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2980_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2980_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2980_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2980_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2980_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2980_1_0_0/* this_arg */
	, InternalEnumerator_1_t2980_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2980_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2980_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2980)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7817_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>
extern MethodInfo ICollection_1_get_Count_m43829_MethodInfo;
static PropertyInfo ICollection_1_t7817____Count_PropertyInfo = 
{
	&ICollection_1_t7817_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43829_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43830_MethodInfo;
static PropertyInfo ICollection_1_t7817____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7817_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43830_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7817_PropertyInfos[] =
{
	&ICollection_1_t7817____Count_PropertyInfo,
	&ICollection_1_t7817____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43829_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43829_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7817_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43829_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43830_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43830_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7817_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43830_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviour_t65_0_0_0;
extern Il2CppType WireframeBehaviour_t65_0_0_0;
static ParameterInfo ICollection_1_t7817_ICollection_1_Add_m43831_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t65_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43831_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43831_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7817_ICollection_1_Add_m43831_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43831_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43832_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43832_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43832_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviour_t65_0_0_0;
static ParameterInfo ICollection_1_t7817_ICollection_1_Contains_m43833_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t65_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43833_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43833_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7817_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7817_ICollection_1_Contains_m43833_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43833_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviourU5BU5D_t177_0_0_0;
extern Il2CppType WireframeBehaviourU5BU5D_t177_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7817_ICollection_1_CopyTo_m43834_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviourU5BU5D_t177_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43834_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43834_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7817_ICollection_1_CopyTo_m43834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43834_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviour_t65_0_0_0;
static ParameterInfo ICollection_1_t7817_ICollection_1_Remove_m43835_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t65_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43835_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43835_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7817_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7817_ICollection_1_Remove_m43835_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43835_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7817_MethodInfos[] =
{
	&ICollection_1_get_Count_m43829_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43830_MethodInfo,
	&ICollection_1_Add_m43831_MethodInfo,
	&ICollection_1_Clear_m43832_MethodInfo,
	&ICollection_1_Contains_m43833_MethodInfo,
	&ICollection_1_CopyTo_m43834_MethodInfo,
	&ICollection_1_Remove_m43835_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7819_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7817_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7819_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7817_0_0_0;
extern Il2CppType ICollection_1_t7817_1_0_0;
struct ICollection_1_t7817;
extern Il2CppGenericClass ICollection_1_t7817_GenericClass;
TypeInfo ICollection_1_t7817_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7817_MethodInfos/* methods */
	, ICollection_1_t7817_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7817_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7817_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7817_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7817_0_0_0/* byval_arg */
	, &ICollection_1_t7817_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7817_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WireframeBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WireframeBehaviour>
extern Il2CppType IEnumerator_1_t6124_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43836_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WireframeBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43836_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7819_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43836_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7819_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43836_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7819_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7819_0_0_0;
extern Il2CppType IEnumerable_1_t7819_1_0_0;
struct IEnumerable_1_t7819;
extern Il2CppGenericClass IEnumerable_1_t7819_GenericClass;
TypeInfo IEnumerable_1_t7819_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7819_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7819_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7819_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7819_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7819_0_0_0/* byval_arg */
	, &IEnumerable_1_t7819_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7819_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7818_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>
extern MethodInfo IList_1_get_Item_m43837_MethodInfo;
extern MethodInfo IList_1_set_Item_m43838_MethodInfo;
static PropertyInfo IList_1_t7818____Item_PropertyInfo = 
{
	&IList_1_t7818_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43837_MethodInfo/* get */
	, &IList_1_set_Item_m43838_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7818_PropertyInfos[] =
{
	&IList_1_t7818____Item_PropertyInfo,
	NULL
};
extern Il2CppType WireframeBehaviour_t65_0_0_0;
static ParameterInfo IList_1_t7818_IList_1_IndexOf_m43839_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t65_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43839_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43839_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7818_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7818_IList_1_IndexOf_m43839_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43839_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WireframeBehaviour_t65_0_0_0;
static ParameterInfo IList_1_t7818_IList_1_Insert_m43840_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t65_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43840_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43840_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7818_IList_1_Insert_m43840_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43840_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7818_IList_1_RemoveAt_m43841_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43841_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43841_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7818_IList_1_RemoveAt_m43841_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43841_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7818_IList_1_get_Item_m43837_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType WireframeBehaviour_t65_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43837_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43837_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7818_il2cpp_TypeInfo/* declaring_type */
	, &WireframeBehaviour_t65_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7818_IList_1_get_Item_m43837_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43837_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WireframeBehaviour_t65_0_0_0;
static ParameterInfo IList_1_t7818_IList_1_set_Item_m43838_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t65_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43838_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43838_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7818_IList_1_set_Item_m43838_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43838_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7818_MethodInfos[] =
{
	&IList_1_IndexOf_m43839_MethodInfo,
	&IList_1_Insert_m43840_MethodInfo,
	&IList_1_RemoveAt_m43841_MethodInfo,
	&IList_1_get_Item_m43837_MethodInfo,
	&IList_1_set_Item_m43838_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7818_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7817_il2cpp_TypeInfo,
	&IEnumerable_1_t7819_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7818_0_0_0;
extern Il2CppType IList_1_t7818_1_0_0;
struct IList_1_t7818;
extern Il2CppGenericClass IList_1_t7818_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7818_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7818_MethodInfos/* methods */
	, IList_1_t7818_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7818_il2cpp_TypeInfo/* element_class */
	, IList_1_t7818_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7818_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7818_0_0_0/* byval_arg */
	, &IList_1_t7818_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7818_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_34.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2981_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_34MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_30.h"
extern TypeInfo InvokableCall_1_t2982_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_30MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15291_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15293_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2981____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2981_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2981, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2981_FieldInfos[] =
{
	&CachedInvokableCall_1_t2981____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType WireframeBehaviour_t65_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2981_CachedInvokableCall_1__ctor_m15289_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t65_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15289_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15289_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2981_CachedInvokableCall_1__ctor_m15289_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15289_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2981_CachedInvokableCall_1_Invoke_m15290_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15290_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15290_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2981_CachedInvokableCall_1_Invoke_m15290_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15290_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2981_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15289_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15290_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15290_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15294_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2981_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15290_MethodInfo,
	&InvokableCall_1_Find_m15294_MethodInfo,
};
extern Il2CppType UnityAction_1_t2983_0_0_0;
extern TypeInfo UnityAction_1_t2983_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisWireframeBehaviour_t65_m33461_MethodInfo;
extern TypeInfo WireframeBehaviour_t65_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15296_MethodInfo;
extern TypeInfo WireframeBehaviour_t65_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2981_RGCTXData[8] = 
{
	&UnityAction_1_t2983_0_0_0/* Type Usage */,
	&UnityAction_1_t2983_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWireframeBehaviour_t65_m33461_MethodInfo/* Method Usage */,
	&WireframeBehaviour_t65_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15296_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15291_MethodInfo/* Method Usage */,
	&WireframeBehaviour_t65_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15293_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2981_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2981_1_0_0;
struct CachedInvokableCall_1_t2981;
extern Il2CppGenericClass CachedInvokableCall_1_t2981_GenericClass;
TypeInfo CachedInvokableCall_1_t2981_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2981_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2981_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2982_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2981_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2981_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2981_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2981_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2981_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2981_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2981_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2981)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_37.h"
extern TypeInfo UnityAction_1_t2983_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_37MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.WireframeBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.WireframeBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisWireframeBehaviour_t65_m33461(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>
extern Il2CppType UnityAction_1_t2983_0_0_1;
FieldInfo InvokableCall_1_t2982____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2983_0_0_1/* type */
	, &InvokableCall_1_t2982_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2982, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2982_FieldInfos[] =
{
	&InvokableCall_1_t2982____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2982_InvokableCall_1__ctor_m15291_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15291_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15291_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2982_InvokableCall_1__ctor_m15291_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15291_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2983_0_0_0;
static ParameterInfo InvokableCall_1_t2982_InvokableCall_1__ctor_m15292_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2983_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15292_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15292_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2982_InvokableCall_1__ctor_m15292_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15292_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2982_InvokableCall_1_Invoke_m15293_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15293_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15293_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2982_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2982_InvokableCall_1_Invoke_m15293_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15293_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2982_InvokableCall_1_Find_m15294_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15294_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.WireframeBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15294_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2982_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2982_InvokableCall_1_Find_m15294_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15294_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2982_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15291_MethodInfo,
	&InvokableCall_1__ctor_m15292_MethodInfo,
	&InvokableCall_1_Invoke_m15293_MethodInfo,
	&InvokableCall_1_Find_m15294_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2982_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15293_MethodInfo,
	&InvokableCall_1_Find_m15294_MethodInfo,
};
extern TypeInfo UnityAction_1_t2983_il2cpp_TypeInfo;
extern TypeInfo WireframeBehaviour_t65_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2982_RGCTXData[5] = 
{
	&UnityAction_1_t2983_0_0_0/* Type Usage */,
	&UnityAction_1_t2983_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWireframeBehaviour_t65_m33461_MethodInfo/* Method Usage */,
	&WireframeBehaviour_t65_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15296_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2982_0_0_0;
extern Il2CppType InvokableCall_1_t2982_1_0_0;
struct InvokableCall_1_t2982;
extern Il2CppGenericClass InvokableCall_1_t2982_GenericClass;
TypeInfo InvokableCall_1_t2982_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2982_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2982_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2982_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2982_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2982_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2982_0_0_0/* byval_arg */
	, &InvokableCall_1_t2982_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2982_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2982_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2982)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2983_UnityAction_1__ctor_m15295_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15295_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15295_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2983_UnityAction_1__ctor_m15295_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15295_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviour_t65_0_0_0;
static ParameterInfo UnityAction_1_t2983_UnityAction_1_Invoke_m15296_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t65_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15296_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15296_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2983_UnityAction_1_Invoke_m15296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15296_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviour_t65_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2983_UnityAction_1_BeginInvoke_m15297_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t65_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15297_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15297_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2983_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2983_UnityAction_1_BeginInvoke_m15297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15297_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2983_UnityAction_1_EndInvoke_m15298_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15298_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15298_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2983_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2983_UnityAction_1_EndInvoke_m15298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15298_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2983_MethodInfos[] =
{
	&UnityAction_1__ctor_m15295_MethodInfo,
	&UnityAction_1_Invoke_m15296_MethodInfo,
	&UnityAction_1_BeginInvoke_m15297_MethodInfo,
	&UnityAction_1_EndInvoke_m15298_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15297_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15298_MethodInfo;
static MethodInfo* UnityAction_1_t2983_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15296_MethodInfo,
	&UnityAction_1_BeginInvoke_m15297_MethodInfo,
	&UnityAction_1_EndInvoke_m15298_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2983_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2983_1_0_0;
struct UnityAction_1_t2983;
extern Il2CppGenericClass UnityAction_1_t2983_GenericClass;
TypeInfo UnityAction_1_t2983_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2983_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2983_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2983_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2983_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2983_0_0_0/* byval_arg */
	, &UnityAction_1_t2983_1_0_0/* this_arg */
	, UnityAction_1_t2983_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2983_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2983)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6126_il2cpp_TypeInfo;

// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Camera>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Camera>
extern MethodInfo IEnumerator_1_get_Current_m43842_MethodInfo;
static PropertyInfo IEnumerator_1_t6126____Current_PropertyInfo = 
{
	&IEnumerator_1_t6126_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43842_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6126_PropertyInfos[] =
{
	&IEnumerator_1_t6126____Current_PropertyInfo,
	NULL
};
extern Il2CppType Camera_t168_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43842_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Camera>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43842_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6126_il2cpp_TypeInfo/* declaring_type */
	, &Camera_t168_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43842_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6126_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43842_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6126_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6126_0_0_0;
extern Il2CppType IEnumerator_1_t6126_1_0_0;
struct IEnumerator_1_t6126;
extern Il2CppGenericClass IEnumerator_1_t6126_GenericClass;
TypeInfo IEnumerator_1_t6126_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6126_MethodInfos/* methods */
	, IEnumerator_1_t6126_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6126_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6126_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6126_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6126_0_0_0/* byval_arg */
	, &IEnumerator_1_t6126_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6126_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Camera>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_105.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2984_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Camera>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_105MethodDeclarations.h"

extern TypeInfo Camera_t168_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15303_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCamera_t168_m33463_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Camera>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Camera>(System.Int32)
#define Array_InternalArray__get_Item_TisCamera_t168_m33463(__this, p0, method) (Camera_t168 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Camera>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Camera>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Camera>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Camera>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2984____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2984_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2984, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2984____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2984_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2984, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2984_FieldInfos[] =
{
	&InternalEnumerator_1_t2984____array_0_FieldInfo,
	&InternalEnumerator_1_t2984____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15300_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2984____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2984_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15300_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2984____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2984_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15303_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2984_PropertyInfos[] =
{
	&InternalEnumerator_1_t2984____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2984____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2984_InternalEnumerator_1__ctor_m15299_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15299_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15299_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2984_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2984_InternalEnumerator_1__ctor_m15299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15299_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15300_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Camera>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15300_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2984_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15300_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15301_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15301_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2984_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15301_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15302_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Camera>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15302_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2984_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15302_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_t168_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15303_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Camera>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15303_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2984_il2cpp_TypeInfo/* declaring_type */
	, &Camera_t168_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15303_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2984_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15299_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15300_MethodInfo,
	&InternalEnumerator_1_Dispose_m15301_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15302_MethodInfo,
	&InternalEnumerator_1_get_Current_m15303_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15302_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15301_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2984_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15300_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15302_MethodInfo,
	&InternalEnumerator_1_Dispose_m15301_MethodInfo,
	&InternalEnumerator_1_get_Current_m15303_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2984_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6126_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2984_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6126_il2cpp_TypeInfo, 7},
};
extern TypeInfo Camera_t168_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2984_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15303_MethodInfo/* Method Usage */,
	&Camera_t168_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCamera_t168_m33463_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2984_0_0_0;
extern Il2CppType InternalEnumerator_1_t2984_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2984_GenericClass;
TypeInfo InternalEnumerator_1_t2984_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2984_MethodInfos/* methods */
	, InternalEnumerator_1_t2984_PropertyInfos/* properties */
	, InternalEnumerator_1_t2984_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2984_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2984_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2984_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2984_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2984_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2984_1_0_0/* this_arg */
	, InternalEnumerator_1_t2984_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2984_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2984_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2984)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7820_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Camera>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Camera>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Camera>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Camera>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Camera>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Camera>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Camera>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Camera>
extern MethodInfo ICollection_1_get_Count_m43843_MethodInfo;
static PropertyInfo ICollection_1_t7820____Count_PropertyInfo = 
{
	&ICollection_1_t7820_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43843_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43844_MethodInfo;
static PropertyInfo ICollection_1_t7820____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7820_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43844_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7820_PropertyInfos[] =
{
	&ICollection_1_t7820____Count_PropertyInfo,
	&ICollection_1_t7820____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43843_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Camera>::get_Count()
MethodInfo ICollection_1_get_Count_m43843_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7820_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43843_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43844_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Camera>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43844_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7820_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43844_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_t168_0_0_0;
extern Il2CppType Camera_t168_0_0_0;
static ParameterInfo ICollection_1_t7820_ICollection_1_Add_m43845_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Camera_t168_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43845_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Camera>::Add(T)
MethodInfo ICollection_1_Add_m43845_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7820_ICollection_1_Add_m43845_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43845_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43846_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Camera>::Clear()
MethodInfo ICollection_1_Clear_m43846_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43846_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_t168_0_0_0;
static ParameterInfo ICollection_1_t7820_ICollection_1_Contains_m43847_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Camera_t168_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43847_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Camera>::Contains(T)
MethodInfo ICollection_1_Contains_m43847_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7820_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7820_ICollection_1_Contains_m43847_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43847_GenericMethod/* genericMethod */

};
extern Il2CppType CameraU5BU5D_t172_0_0_0;
extern Il2CppType CameraU5BU5D_t172_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7820_ICollection_1_CopyTo_m43848_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CameraU5BU5D_t172_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43848_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Camera>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43848_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7820_ICollection_1_CopyTo_m43848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43848_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_t168_0_0_0;
static ParameterInfo ICollection_1_t7820_ICollection_1_Remove_m43849_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Camera_t168_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43849_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Camera>::Remove(T)
MethodInfo ICollection_1_Remove_m43849_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7820_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7820_ICollection_1_Remove_m43849_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43849_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7820_MethodInfos[] =
{
	&ICollection_1_get_Count_m43843_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43844_MethodInfo,
	&ICollection_1_Add_m43845_MethodInfo,
	&ICollection_1_Clear_m43846_MethodInfo,
	&ICollection_1_Contains_m43847_MethodInfo,
	&ICollection_1_CopyTo_m43848_MethodInfo,
	&ICollection_1_Remove_m43849_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7822_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7820_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7822_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7820_0_0_0;
extern Il2CppType ICollection_1_t7820_1_0_0;
struct ICollection_1_t7820;
extern Il2CppGenericClass ICollection_1_t7820_GenericClass;
TypeInfo ICollection_1_t7820_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7820_MethodInfos/* methods */
	, ICollection_1_t7820_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7820_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7820_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7820_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7820_0_0_0/* byval_arg */
	, &ICollection_1_t7820_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7820_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Camera>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Camera>
extern Il2CppType IEnumerator_1_t6126_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43850_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Camera>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43850_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7822_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6126_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43850_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7822_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43850_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7822_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7822_0_0_0;
extern Il2CppType IEnumerable_1_t7822_1_0_0;
struct IEnumerable_1_t7822;
extern Il2CppGenericClass IEnumerable_1_t7822_GenericClass;
TypeInfo IEnumerable_1_t7822_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7822_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7822_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7822_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7822_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7822_0_0_0/* byval_arg */
	, &IEnumerable_1_t7822_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7822_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7821_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Camera>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Camera>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Camera>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Camera>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Camera>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Camera>
extern MethodInfo IList_1_get_Item_m43851_MethodInfo;
extern MethodInfo IList_1_set_Item_m43852_MethodInfo;
static PropertyInfo IList_1_t7821____Item_PropertyInfo = 
{
	&IList_1_t7821_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43851_MethodInfo/* get */
	, &IList_1_set_Item_m43852_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7821_PropertyInfos[] =
{
	&IList_1_t7821____Item_PropertyInfo,
	NULL
};
extern Il2CppType Camera_t168_0_0_0;
static ParameterInfo IList_1_t7821_IList_1_IndexOf_m43853_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Camera_t168_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43853_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Camera>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43853_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7821_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7821_IList_1_IndexOf_m43853_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43853_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Camera_t168_0_0_0;
static ParameterInfo IList_1_t7821_IList_1_Insert_m43854_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Camera_t168_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43854_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Camera>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43854_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7821_IList_1_Insert_m43854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43854_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7821_IList_1_RemoveAt_m43855_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43855_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Camera>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43855_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7821_IList_1_RemoveAt_m43855_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43855_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7821_IList_1_get_Item_m43851_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Camera_t168_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43851_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Camera>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43851_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7821_il2cpp_TypeInfo/* declaring_type */
	, &Camera_t168_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7821_IList_1_get_Item_m43851_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43851_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Camera_t168_0_0_0;
static ParameterInfo IList_1_t7821_IList_1_set_Item_m43852_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Camera_t168_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43852_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Camera>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43852_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7821_IList_1_set_Item_m43852_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43852_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7821_MethodInfos[] =
{
	&IList_1_IndexOf_m43853_MethodInfo,
	&IList_1_Insert_m43854_MethodInfo,
	&IList_1_RemoveAt_m43855_MethodInfo,
	&IList_1_get_Item_m43851_MethodInfo,
	&IList_1_set_Item_m43852_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7821_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7820_il2cpp_TypeInfo,
	&IEnumerable_1_t7822_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7821_0_0_0;
extern Il2CppType IList_1_t7821_1_0_0;
struct IList_1_t7821;
extern Il2CppGenericClass IList_1_t7821_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7821_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7821_MethodInfos/* methods */
	, IList_1_t7821_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7821_il2cpp_TypeInfo/* element_class */
	, IList_1_t7821_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7821_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7821_0_0_0/* byval_arg */
	, &IList_1_t7821_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7821_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6127_il2cpp_TypeInfo;

// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
extern MethodInfo IEnumerator_1_get_Current_m43856_MethodInfo;
static PropertyInfo IEnumerator_1_t6127____Current_PropertyInfo = 
{
	&IEnumerator_1_t6127_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43856_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6127_PropertyInfos[] =
{
	&IEnumerator_1_t6127____Current_PropertyInfo,
	NULL
};
extern Il2CppType Vector3_t73_0_0_0;
extern void* RuntimeInvoker_Vector3_t73 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43856_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43856_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6127_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t73_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t73/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43856_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6127_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43856_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6127_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6127_0_0_0;
extern Il2CppType IEnumerator_1_t6127_1_0_0;
struct IEnumerator_1_t6127;
extern Il2CppGenericClass IEnumerator_1_t6127_GenericClass;
TypeInfo IEnumerator_1_t6127_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6127_MethodInfos/* methods */
	, IEnumerator_1_t6127_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6127_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6127_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6127_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6127_0_0_0/* byval_arg */
	, &IEnumerator_1_t6127_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6127_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Vector3>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_106.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2985_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Vector3>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_106MethodDeclarations.h"

extern TypeInfo Vector3_t73_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15308_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVector3_t73_m33474_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
 Vector3_t73  Array_InternalArray__get_Item_TisVector3_t73_m33474 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m15304_MethodInfo;
 void InternalEnumerator_1__ctor_m15304 (InternalEnumerator_1_t2985 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15305_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15305 (InternalEnumerator_1_t2985 * __this, MethodInfo* method){
	{
		Vector3_t73  L_0 = InternalEnumerator_1_get_Current_m15308(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m15308_MethodInfo);
		Vector3_t73  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Vector3_t73_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m15306_MethodInfo;
 void InternalEnumerator_1_Dispose_m15306 (InternalEnumerator_1_t2985 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector3>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m15307_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m15307 (InternalEnumerator_1_t2985 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector3>::get_Current()
 Vector3_t73  InternalEnumerator_1_get_Current_m15308 (InternalEnumerator_1_t2985 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		Vector3_t73  L_8 = Array_InternalArray__get_Item_TisVector3_t73_m33474(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisVector3_t73_m33474_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Vector3>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2985____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2985_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2985, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2985____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2985_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2985, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2985_FieldInfos[] =
{
	&InternalEnumerator_1_t2985____array_0_FieldInfo,
	&InternalEnumerator_1_t2985____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t2985____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2985_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15305_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2985____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2985_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15308_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2985_PropertyInfos[] =
{
	&InternalEnumerator_1_t2985____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2985____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2985_InternalEnumerator_1__ctor_m15304_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15304_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15304_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m15304/* method */
	, &InternalEnumerator_1_t2985_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2985_InternalEnumerator_1__ctor_m15304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15304_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15305_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15305_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15305/* method */
	, &InternalEnumerator_1_t2985_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15305_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15306_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15306_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m15306/* method */
	, &InternalEnumerator_1_t2985_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15306_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15307_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector3>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15307_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m15307/* method */
	, &InternalEnumerator_1_t2985_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15307_GenericMethod/* genericMethod */

};
extern Il2CppType Vector3_t73_0_0_0;
extern void* RuntimeInvoker_Vector3_t73 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15308_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Vector3>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15308_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m15308/* method */
	, &InternalEnumerator_1_t2985_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t73_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t73/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15308_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2985_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15304_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15305_MethodInfo,
	&InternalEnumerator_1_Dispose_m15306_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15307_MethodInfo,
	&InternalEnumerator_1_get_Current_m15308_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t2985_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15305_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15307_MethodInfo,
	&InternalEnumerator_1_Dispose_m15306_MethodInfo,
	&InternalEnumerator_1_get_Current_m15308_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2985_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6127_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2985_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6127_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2985_0_0_0;
extern Il2CppType InternalEnumerator_1_t2985_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2985_GenericClass;
TypeInfo InternalEnumerator_1_t2985_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2985_MethodInfos/* methods */
	, InternalEnumerator_1_t2985_PropertyInfos/* properties */
	, InternalEnumerator_1_t2985_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2985_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2985_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2985_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2985_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2985_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2985_1_0_0/* this_arg */
	, InternalEnumerator_1_t2985_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2985_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2985)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7823_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Vector3>
extern MethodInfo ICollection_1_get_Count_m43857_MethodInfo;
static PropertyInfo ICollection_1_t7823____Count_PropertyInfo = 
{
	&ICollection_1_t7823_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43857_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43858_MethodInfo;
static PropertyInfo ICollection_1_t7823____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7823_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43858_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7823_PropertyInfos[] =
{
	&ICollection_1_t7823____Count_PropertyInfo,
	&ICollection_1_t7823____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43857_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::get_Count()
MethodInfo ICollection_1_get_Count_m43857_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7823_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43857_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43858_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43858_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7823_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43858_GenericMethod/* genericMethod */

};
extern Il2CppType Vector3_t73_0_0_0;
extern Il2CppType Vector3_t73_0_0_0;
static ParameterInfo ICollection_1_t7823_ICollection_1_Add_m43859_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Vector3_t73_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Vector3_t73 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43859_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::Add(T)
MethodInfo ICollection_1_Add_m43859_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Vector3_t73/* invoker_method */
	, ICollection_1_t7823_ICollection_1_Add_m43859_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43859_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43860_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::Clear()
MethodInfo ICollection_1_Clear_m43860_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43860_GenericMethod/* genericMethod */

};
extern Il2CppType Vector3_t73_0_0_0;
static ParameterInfo ICollection_1_t7823_ICollection_1_Contains_m43861_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Vector3_t73_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Vector3_t73 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43861_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::Contains(T)
MethodInfo ICollection_1_Contains_m43861_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7823_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Vector3_t73/* invoker_method */
	, ICollection_1_t7823_ICollection_1_Contains_m43861_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43861_GenericMethod/* genericMethod */

};
extern Il2CppType Vector3U5BU5D_t85_0_0_0;
extern Il2CppType Vector3U5BU5D_t85_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7823_ICollection_1_CopyTo_m43862_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Vector3U5BU5D_t85_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43862_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43862_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7823_ICollection_1_CopyTo_m43862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43862_GenericMethod/* genericMethod */

};
extern Il2CppType Vector3_t73_0_0_0;
static ParameterInfo ICollection_1_t7823_ICollection_1_Remove_m43863_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Vector3_t73_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Vector3_t73 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43863_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::Remove(T)
MethodInfo ICollection_1_Remove_m43863_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7823_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Vector3_t73/* invoker_method */
	, ICollection_1_t7823_ICollection_1_Remove_m43863_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43863_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7823_MethodInfos[] =
{
	&ICollection_1_get_Count_m43857_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43858_MethodInfo,
	&ICollection_1_Add_m43859_MethodInfo,
	&ICollection_1_Clear_m43860_MethodInfo,
	&ICollection_1_Contains_m43861_MethodInfo,
	&ICollection_1_CopyTo_m43862_MethodInfo,
	&ICollection_1_Remove_m43863_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7825_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7823_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7825_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7823_0_0_0;
extern Il2CppType ICollection_1_t7823_1_0_0;
struct ICollection_1_t7823;
extern Il2CppGenericClass ICollection_1_t7823_GenericClass;
TypeInfo ICollection_1_t7823_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7823_MethodInfos/* methods */
	, ICollection_1_t7823_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7823_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7823_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7823_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7823_0_0_0/* byval_arg */
	, &ICollection_1_t7823_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7823_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
extern Il2CppType IEnumerator_1_t6127_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43864_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43864_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7825_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6127_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43864_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7825_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43864_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7825_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7825_0_0_0;
extern Il2CppType IEnumerable_1_t7825_1_0_0;
struct IEnumerable_1_t7825;
extern Il2CppGenericClass IEnumerable_1_t7825_GenericClass;
TypeInfo IEnumerable_1_t7825_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7825_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7825_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7825_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7825_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7825_0_0_0/* byval_arg */
	, &IEnumerable_1_t7825_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7825_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7824_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Vector3>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector3>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Vector3>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Vector3>
extern MethodInfo IList_1_get_Item_m43865_MethodInfo;
extern MethodInfo IList_1_set_Item_m43866_MethodInfo;
static PropertyInfo IList_1_t7824____Item_PropertyInfo = 
{
	&IList_1_t7824_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43865_MethodInfo/* get */
	, &IList_1_set_Item_m43866_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7824_PropertyInfos[] =
{
	&IList_1_t7824____Item_PropertyInfo,
	NULL
};
extern Il2CppType Vector3_t73_0_0_0;
static ParameterInfo IList_1_t7824_IList_1_IndexOf_m43867_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Vector3_t73_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Vector3_t73 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43867_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Vector3>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43867_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7824_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Vector3_t73/* invoker_method */
	, IList_1_t7824_IList_1_IndexOf_m43867_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43867_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Vector3_t73_0_0_0;
static ParameterInfo IList_1_t7824_IList_1_Insert_m43868_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Vector3_t73_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Vector3_t73 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43868_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector3>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43868_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Vector3_t73/* invoker_method */
	, IList_1_t7824_IList_1_Insert_m43868_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43868_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7824_IList_1_RemoveAt_m43869_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43869_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43869_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7824_IList_1_RemoveAt_m43869_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43869_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7824_IList_1_get_Item_m43865_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Vector3_t73_0_0_0;
extern void* RuntimeInvoker_Vector3_t73_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43865_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Vector3>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43865_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7824_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t73_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t73_Int32_t123/* invoker_method */
	, IList_1_t7824_IList_1_get_Item_m43865_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43865_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Vector3_t73_0_0_0;
static ParameterInfo IList_1_t7824_IList_1_set_Item_m43866_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Vector3_t73_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Vector3_t73 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43866_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43866_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Vector3_t73/* invoker_method */
	, IList_1_t7824_IList_1_set_Item_m43866_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43866_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7824_MethodInfos[] =
{
	&IList_1_IndexOf_m43867_MethodInfo,
	&IList_1_Insert_m43868_MethodInfo,
	&IList_1_RemoveAt_m43869_MethodInfo,
	&IList_1_get_Item_m43865_MethodInfo,
	&IList_1_set_Item_m43866_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7824_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7823_il2cpp_TypeInfo,
	&IEnumerable_1_t7825_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7824_0_0_0;
extern Il2CppType IList_1_t7824_1_0_0;
struct IList_1_t7824;
extern Il2CppGenericClass IList_1_t7824_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7824_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7824_MethodInfos/* methods */
	, IList_1_t7824_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7824_il2cpp_TypeInfo/* element_class */
	, IList_1_t7824_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7824_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7824_0_0_0/* byval_arg */
	, &IList_1_t7824_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7824_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6129_il2cpp_TypeInfo;

// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.WireframeTrackableEventHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WireframeTrackableEventHandler>
extern MethodInfo IEnumerator_1_get_Current_m43870_MethodInfo;
static PropertyInfo IEnumerator_1_t6129____Current_PropertyInfo = 
{
	&IEnumerator_1_t6129_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43870_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6129_PropertyInfos[] =
{
	&IEnumerator_1_t6129____Current_PropertyInfo,
	NULL
};
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43870_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WireframeTrackableEventHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43870_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6129_il2cpp_TypeInfo/* declaring_type */
	, &WireframeTrackableEventHandler_t67_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43870_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6129_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43870_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6129_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6129_0_0_0;
extern Il2CppType IEnumerator_1_t6129_1_0_0;
struct IEnumerator_1_t6129;
extern Il2CppGenericClass IEnumerator_1_t6129_GenericClass;
TypeInfo IEnumerator_1_t6129_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6129_MethodInfos/* methods */
	, IEnumerator_1_t6129_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6129_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6129_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6129_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6129_0_0_0/* byval_arg */
	, &IEnumerator_1_t6129_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6129_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_107.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2986_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_107MethodDeclarations.h"

extern TypeInfo WireframeTrackableEventHandler_t67_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15313_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWireframeTrackableEventHandler_t67_m33486_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WireframeTrackableEventHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WireframeTrackableEventHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisWireframeTrackableEventHandler_t67_m33486(__this, p0, method) (WireframeTrackableEventHandler_t67 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2986____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2986_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2986, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2986____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2986_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2986, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2986_FieldInfos[] =
{
	&InternalEnumerator_1_t2986____array_0_FieldInfo,
	&InternalEnumerator_1_t2986____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15310_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2986____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2986_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15310_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2986____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2986_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15313_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2986_PropertyInfos[] =
{
	&InternalEnumerator_1_t2986____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2986____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2986_InternalEnumerator_1__ctor_m15309_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15309_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15309_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2986_InternalEnumerator_1__ctor_m15309_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15309_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15310_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15310_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2986_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15310_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15311_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15311_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15311_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15312_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15312_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15312_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15313_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WireframeTrackableEventHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15313_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2986_il2cpp_TypeInfo/* declaring_type */
	, &WireframeTrackableEventHandler_t67_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15313_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2986_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15309_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15310_MethodInfo,
	&InternalEnumerator_1_Dispose_m15311_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15312_MethodInfo,
	&InternalEnumerator_1_get_Current_m15313_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15312_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15311_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2986_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15310_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15312_MethodInfo,
	&InternalEnumerator_1_Dispose_m15311_MethodInfo,
	&InternalEnumerator_1_get_Current_m15313_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2986_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6129_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2986_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6129_il2cpp_TypeInfo, 7},
};
extern TypeInfo WireframeTrackableEventHandler_t67_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2986_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15313_MethodInfo/* Method Usage */,
	&WireframeTrackableEventHandler_t67_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWireframeTrackableEventHandler_t67_m33486_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2986_0_0_0;
extern Il2CppType InternalEnumerator_1_t2986_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2986_GenericClass;
TypeInfo InternalEnumerator_1_t2986_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2986_MethodInfos/* methods */
	, InternalEnumerator_1_t2986_PropertyInfos/* properties */
	, InternalEnumerator_1_t2986_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2986_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2986_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2986_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2986_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2986_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2986_1_0_0/* this_arg */
	, InternalEnumerator_1_t2986_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2986_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2986_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2986)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7826_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>
extern MethodInfo ICollection_1_get_Count_m43871_MethodInfo;
static PropertyInfo ICollection_1_t7826____Count_PropertyInfo = 
{
	&ICollection_1_t7826_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43871_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43872_MethodInfo;
static PropertyInfo ICollection_1_t7826____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7826_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43872_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7826_PropertyInfos[] =
{
	&ICollection_1_t7826____Count_PropertyInfo,
	&ICollection_1_t7826____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43871_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m43871_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7826_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43871_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43872_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43872_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7826_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43872_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
static ParameterInfo ICollection_1_t7826_ICollection_1_Add_m43873_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeTrackableEventHandler_t67_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43873_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::Add(T)
MethodInfo ICollection_1_Add_m43873_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7826_ICollection_1_Add_m43873_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43873_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43874_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::Clear()
MethodInfo ICollection_1_Clear_m43874_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43874_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
static ParameterInfo ICollection_1_t7826_ICollection_1_Contains_m43875_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeTrackableEventHandler_t67_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43875_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m43875_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7826_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7826_ICollection_1_Contains_m43875_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43875_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeTrackableEventHandlerU5BU5D_t5388_0_0_0;
extern Il2CppType WireframeTrackableEventHandlerU5BU5D_t5388_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7826_ICollection_1_CopyTo_m43876_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WireframeTrackableEventHandlerU5BU5D_t5388_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43876_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43876_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7826_ICollection_1_CopyTo_m43876_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43876_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
static ParameterInfo ICollection_1_t7826_ICollection_1_Remove_m43877_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeTrackableEventHandler_t67_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43877_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeTrackableEventHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m43877_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7826_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7826_ICollection_1_Remove_m43877_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43877_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7826_MethodInfos[] =
{
	&ICollection_1_get_Count_m43871_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43872_MethodInfo,
	&ICollection_1_Add_m43873_MethodInfo,
	&ICollection_1_Clear_m43874_MethodInfo,
	&ICollection_1_Contains_m43875_MethodInfo,
	&ICollection_1_CopyTo_m43876_MethodInfo,
	&ICollection_1_Remove_m43877_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7828_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7826_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7828_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7826_0_0_0;
extern Il2CppType ICollection_1_t7826_1_0_0;
struct ICollection_1_t7826;
extern Il2CppGenericClass ICollection_1_t7826_GenericClass;
TypeInfo ICollection_1_t7826_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7826_MethodInfos/* methods */
	, ICollection_1_t7826_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7826_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7826_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7826_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7826_0_0_0/* byval_arg */
	, &ICollection_1_t7826_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7826_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WireframeTrackableEventHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WireframeTrackableEventHandler>
extern Il2CppType IEnumerator_1_t6129_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43878_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WireframeTrackableEventHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43878_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7828_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6129_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43878_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7828_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43878_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7828_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7828_0_0_0;
extern Il2CppType IEnumerable_1_t7828_1_0_0;
struct IEnumerable_1_t7828;
extern Il2CppGenericClass IEnumerable_1_t7828_GenericClass;
TypeInfo IEnumerable_1_t7828_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7828_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7828_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7828_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7828_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7828_0_0_0/* byval_arg */
	, &IEnumerable_1_t7828_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7828_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7827_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>
extern MethodInfo IList_1_get_Item_m43879_MethodInfo;
extern MethodInfo IList_1_set_Item_m43880_MethodInfo;
static PropertyInfo IList_1_t7827____Item_PropertyInfo = 
{
	&IList_1_t7827_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43879_MethodInfo/* get */
	, &IList_1_set_Item_m43880_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7827_PropertyInfos[] =
{
	&IList_1_t7827____Item_PropertyInfo,
	NULL
};
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
static ParameterInfo IList_1_t7827_IList_1_IndexOf_m43881_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeTrackableEventHandler_t67_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43881_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43881_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7827_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7827_IList_1_IndexOf_m43881_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43881_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
static ParameterInfo IList_1_t7827_IList_1_Insert_m43882_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WireframeTrackableEventHandler_t67_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43882_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43882_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7827_IList_1_Insert_m43882_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43882_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7827_IList_1_RemoveAt_m43883_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43883_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43883_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7827_IList_1_RemoveAt_m43883_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43883_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7827_IList_1_get_Item_m43879_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43879_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43879_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7827_il2cpp_TypeInfo/* declaring_type */
	, &WireframeTrackableEventHandler_t67_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7827_IList_1_get_Item_m43879_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43879_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
static ParameterInfo IList_1_t7827_IList_1_set_Item_m43880_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WireframeTrackableEventHandler_t67_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43880_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeTrackableEventHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43880_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7827_IList_1_set_Item_m43880_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43880_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7827_MethodInfos[] =
{
	&IList_1_IndexOf_m43881_MethodInfo,
	&IList_1_Insert_m43882_MethodInfo,
	&IList_1_RemoveAt_m43883_MethodInfo,
	&IList_1_get_Item_m43879_MethodInfo,
	&IList_1_set_Item_m43880_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7827_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7826_il2cpp_TypeInfo,
	&IEnumerable_1_t7828_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7827_0_0_0;
extern Il2CppType IList_1_t7827_1_0_0;
struct IList_1_t7827;
extern Il2CppGenericClass IList_1_t7827_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7827_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7827_MethodInfos/* methods */
	, IList_1_t7827_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7827_il2cpp_TypeInfo/* element_class */
	, IList_1_t7827_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7827_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7827_0_0_0/* byval_arg */
	, &IList_1_t7827_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7827_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_35.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2987_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_35MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_31.h"
extern TypeInfo InvokableCall_1_t2988_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_31MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15316_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15318_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeTrackableEventHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeTrackableEventHandler>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeTrackableEventHandler>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2987____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2987_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2987, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2987_FieldInfos[] =
{
	&CachedInvokableCall_1_t2987____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2987_CachedInvokableCall_1__ctor_m15314_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &WireframeTrackableEventHandler_t67_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15314_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeTrackableEventHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15314_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2987_CachedInvokableCall_1__ctor_m15314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15314_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2987_CachedInvokableCall_1_Invoke_m15315_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15315_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WireframeTrackableEventHandler>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15315_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2987_CachedInvokableCall_1_Invoke_m15315_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15315_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2987_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15314_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15315_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15315_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15319_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2987_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15315_MethodInfo,
	&InvokableCall_1_Find_m15319_MethodInfo,
};
extern Il2CppType UnityAction_1_t2989_0_0_0;
extern TypeInfo UnityAction_1_t2989_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisWireframeTrackableEventHandler_t67_m33496_MethodInfo;
extern TypeInfo WireframeTrackableEventHandler_t67_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15321_MethodInfo;
extern TypeInfo WireframeTrackableEventHandler_t67_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2987_RGCTXData[8] = 
{
	&UnityAction_1_t2989_0_0_0/* Type Usage */,
	&UnityAction_1_t2989_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWireframeTrackableEventHandler_t67_m33496_MethodInfo/* Method Usage */,
	&WireframeTrackableEventHandler_t67_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15321_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15316_MethodInfo/* Method Usage */,
	&WireframeTrackableEventHandler_t67_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15318_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2987_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2987_1_0_0;
struct CachedInvokableCall_1_t2987;
extern Il2CppGenericClass CachedInvokableCall_1_t2987_GenericClass;
TypeInfo CachedInvokableCall_1_t2987_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2987_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2987_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2988_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2987_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2987_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2987_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2987_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2987_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2987_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2987_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2987)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_38.h"
extern TypeInfo UnityAction_1_t2989_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_38MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.WireframeTrackableEventHandler>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.WireframeTrackableEventHandler>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisWireframeTrackableEventHandler_t67_m33496(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>
extern Il2CppType UnityAction_1_t2989_0_0_1;
FieldInfo InvokableCall_1_t2988____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2989_0_0_1/* type */
	, &InvokableCall_1_t2988_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2988, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2988_FieldInfos[] =
{
	&InvokableCall_1_t2988____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2988_InvokableCall_1__ctor_m15316_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15316_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15316_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2988_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2988_InvokableCall_1__ctor_m15316_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15316_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2989_0_0_0;
static ParameterInfo InvokableCall_1_t2988_InvokableCall_1__ctor_m15317_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2989_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15317_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15317_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2988_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2988_InvokableCall_1__ctor_m15317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15317_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2988_InvokableCall_1_Invoke_m15318_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15318_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15318_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2988_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2988_InvokableCall_1_Invoke_m15318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15318_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2988_InvokableCall_1_Find_m15319_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15319_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.WireframeTrackableEventHandler>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15319_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2988_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2988_InvokableCall_1_Find_m15319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15319_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2988_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15316_MethodInfo,
	&InvokableCall_1__ctor_m15317_MethodInfo,
	&InvokableCall_1_Invoke_m15318_MethodInfo,
	&InvokableCall_1_Find_m15319_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2988_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15318_MethodInfo,
	&InvokableCall_1_Find_m15319_MethodInfo,
};
extern TypeInfo UnityAction_1_t2989_il2cpp_TypeInfo;
extern TypeInfo WireframeTrackableEventHandler_t67_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2988_RGCTXData[5] = 
{
	&UnityAction_1_t2989_0_0_0/* Type Usage */,
	&UnityAction_1_t2989_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWireframeTrackableEventHandler_t67_m33496_MethodInfo/* Method Usage */,
	&WireframeTrackableEventHandler_t67_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15321_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2988_0_0_0;
extern Il2CppType InvokableCall_1_t2988_1_0_0;
struct InvokableCall_1_t2988;
extern Il2CppGenericClass InvokableCall_1_t2988_GenericClass;
TypeInfo InvokableCall_1_t2988_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2988_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2988_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2988_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2988_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2988_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2988_0_0_0/* byval_arg */
	, &InvokableCall_1_t2988_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2988_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2988_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2988)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2989_UnityAction_1__ctor_m15320_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15320_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15320_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2989_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2989_UnityAction_1__ctor_m15320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15320_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
static ParameterInfo UnityAction_1_t2989_UnityAction_1_Invoke_m15321_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WireframeTrackableEventHandler_t67_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15321_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15321_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2989_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2989_UnityAction_1_Invoke_m15321_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15321_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeTrackableEventHandler_t67_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2989_UnityAction_1_BeginInvoke_m15322_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WireframeTrackableEventHandler_t67_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15322_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15322_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2989_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2989_UnityAction_1_BeginInvoke_m15322_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15322_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2989_UnityAction_1_EndInvoke_m15323_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15323_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WireframeTrackableEventHandler>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15323_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2989_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2989_UnityAction_1_EndInvoke_m15323_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15323_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2989_MethodInfos[] =
{
	&UnityAction_1__ctor_m15320_MethodInfo,
	&UnityAction_1_Invoke_m15321_MethodInfo,
	&UnityAction_1_BeginInvoke_m15322_MethodInfo,
	&UnityAction_1_EndInvoke_m15323_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15322_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15323_MethodInfo;
static MethodInfo* UnityAction_1_t2989_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15321_MethodInfo,
	&UnityAction_1_BeginInvoke_m15322_MethodInfo,
	&UnityAction_1_EndInvoke_m15323_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2989_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2989_1_0_0;
struct UnityAction_1_t2989;
extern Il2CppGenericClass UnityAction_1_t2989_GenericClass;
TypeInfo UnityAction_1_t2989_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2989_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2989_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2989_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2989_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2989_0_0_0/* byval_arg */
	, &UnityAction_1_t2989_1_0_0/* this_arg */
	, UnityAction_1_t2989_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2989_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2989)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6131_il2cpp_TypeInfo;

// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.WordBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WordBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43884_MethodInfo;
static PropertyInfo IEnumerator_1_t6131____Current_PropertyInfo = 
{
	&IEnumerator_1_t6131_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43884_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6131_PropertyInfos[] =
{
	&IEnumerator_1_t6131____Current_PropertyInfo,
	NULL
};
extern Il2CppType WordBehaviour_t68_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43884_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WordBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43884_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6131_il2cpp_TypeInfo/* declaring_type */
	, &WordBehaviour_t68_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43884_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6131_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43884_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6131_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6131_0_0_0;
extern Il2CppType IEnumerator_1_t6131_1_0_0;
struct IEnumerator_1_t6131;
extern Il2CppGenericClass IEnumerator_1_t6131_GenericClass;
TypeInfo IEnumerator_1_t6131_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6131_MethodInfos/* methods */
	, IEnumerator_1_t6131_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6131_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6131_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6131_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6131_0_0_0/* byval_arg */
	, &IEnumerator_1_t6131_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6131_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_108.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2990_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_108MethodDeclarations.h"

extern TypeInfo WordBehaviour_t68_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15328_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWordBehaviour_t68_m33499_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WordBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WordBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisWordBehaviour_t68_m33499(__this, p0, method) (WordBehaviour_t68 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2990____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2990_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2990, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2990____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2990_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2990, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2990_FieldInfos[] =
{
	&InternalEnumerator_1_t2990____array_0_FieldInfo,
	&InternalEnumerator_1_t2990____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15325_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2990____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2990_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15325_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2990____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2990_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15328_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2990_PropertyInfos[] =
{
	&InternalEnumerator_1_t2990____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2990____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2990_InternalEnumerator_1__ctor_m15324_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15324_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15324_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2990_InternalEnumerator_1__ctor_m15324_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15324_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15325_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15325_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2990_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15325_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15326_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15326_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15326_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15327_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15327_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2990_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15327_GenericMethod/* genericMethod */

};
extern Il2CppType WordBehaviour_t68_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15328_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WordBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15328_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2990_il2cpp_TypeInfo/* declaring_type */
	, &WordBehaviour_t68_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15328_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2990_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15324_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15325_MethodInfo,
	&InternalEnumerator_1_Dispose_m15326_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15327_MethodInfo,
	&InternalEnumerator_1_get_Current_m15328_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15327_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15326_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2990_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15325_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15327_MethodInfo,
	&InternalEnumerator_1_Dispose_m15326_MethodInfo,
	&InternalEnumerator_1_get_Current_m15328_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2990_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6131_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2990_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6131_il2cpp_TypeInfo, 7},
};
extern TypeInfo WordBehaviour_t68_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2990_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15328_MethodInfo/* Method Usage */,
	&WordBehaviour_t68_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWordBehaviour_t68_m33499_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2990_0_0_0;
extern Il2CppType InternalEnumerator_1_t2990_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2990_GenericClass;
TypeInfo InternalEnumerator_1_t2990_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2990_MethodInfos/* methods */
	, InternalEnumerator_1_t2990_PropertyInfos/* properties */
	, InternalEnumerator_1_t2990_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2990_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2990_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2990_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2990_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2990_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2990_1_0_0/* this_arg */
	, InternalEnumerator_1_t2990_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2990_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2990_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2990)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7829_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>
extern MethodInfo ICollection_1_get_Count_m43885_MethodInfo;
static PropertyInfo ICollection_1_t7829____Count_PropertyInfo = 
{
	&ICollection_1_t7829_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43885_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43886_MethodInfo;
static PropertyInfo ICollection_1_t7829____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7829_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43886_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7829_PropertyInfos[] =
{
	&ICollection_1_t7829____Count_PropertyInfo,
	&ICollection_1_t7829____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43885_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43885_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7829_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43885_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43886_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43886_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43886_GenericMethod/* genericMethod */

};
extern Il2CppType WordBehaviour_t68_0_0_0;
extern Il2CppType WordBehaviour_t68_0_0_0;
static ParameterInfo ICollection_1_t7829_ICollection_1_Add_m43887_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WordBehaviour_t68_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43887_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43887_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7829_ICollection_1_Add_m43887_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43887_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43888_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43888_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43888_GenericMethod/* genericMethod */

};
extern Il2CppType WordBehaviour_t68_0_0_0;
static ParameterInfo ICollection_1_t7829_ICollection_1_Contains_m43889_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WordBehaviour_t68_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43889_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43889_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7829_ICollection_1_Contains_m43889_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43889_GenericMethod/* genericMethod */

};
extern Il2CppType WordBehaviourU5BU5D_t5389_0_0_0;
extern Il2CppType WordBehaviourU5BU5D_t5389_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7829_ICollection_1_CopyTo_m43890_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WordBehaviourU5BU5D_t5389_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43890_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43890_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7829_ICollection_1_CopyTo_m43890_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43890_GenericMethod/* genericMethod */

};
extern Il2CppType WordBehaviour_t68_0_0_0;
static ParameterInfo ICollection_1_t7829_ICollection_1_Remove_m43891_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WordBehaviour_t68_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43891_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43891_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7829_ICollection_1_Remove_m43891_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43891_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7829_MethodInfos[] =
{
	&ICollection_1_get_Count_m43885_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43886_MethodInfo,
	&ICollection_1_Add_m43887_MethodInfo,
	&ICollection_1_Clear_m43888_MethodInfo,
	&ICollection_1_Contains_m43889_MethodInfo,
	&ICollection_1_CopyTo_m43890_MethodInfo,
	&ICollection_1_Remove_m43891_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7831_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7829_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7831_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7829_0_0_0;
extern Il2CppType ICollection_1_t7829_1_0_0;
struct ICollection_1_t7829;
extern Il2CppGenericClass ICollection_1_t7829_GenericClass;
TypeInfo ICollection_1_t7829_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7829_MethodInfos/* methods */
	, ICollection_1_t7829_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7829_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7829_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7829_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7829_0_0_0/* byval_arg */
	, &ICollection_1_t7829_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7829_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WordBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WordBehaviour>
extern Il2CppType IEnumerator_1_t6131_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43892_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WordBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43892_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7831_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43892_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7831_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43892_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7831_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7831_0_0_0;
extern Il2CppType IEnumerable_1_t7831_1_0_0;
struct IEnumerable_1_t7831;
extern Il2CppGenericClass IEnumerable_1_t7831_GenericClass;
TypeInfo IEnumerable_1_t7831_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7831_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7831_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7831_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7831_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7831_0_0_0/* byval_arg */
	, &IEnumerable_1_t7831_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7831_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7830_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WordBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WordBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WordBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WordBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WordBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WordBehaviour>
extern MethodInfo IList_1_get_Item_m43893_MethodInfo;
extern MethodInfo IList_1_set_Item_m43894_MethodInfo;
static PropertyInfo IList_1_t7830____Item_PropertyInfo = 
{
	&IList_1_t7830_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43893_MethodInfo/* get */
	, &IList_1_set_Item_m43894_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7830_PropertyInfos[] =
{
	&IList_1_t7830____Item_PropertyInfo,
	NULL
};
extern Il2CppType WordBehaviour_t68_0_0_0;
static ParameterInfo IList_1_t7830_IList_1_IndexOf_m43895_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WordBehaviour_t68_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43895_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WordBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43895_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7830_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7830_IList_1_IndexOf_m43895_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43895_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WordBehaviour_t68_0_0_0;
static ParameterInfo IList_1_t7830_IList_1_Insert_m43896_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WordBehaviour_t68_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43896_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WordBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43896_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7830_IList_1_Insert_m43896_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43896_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7830_IList_1_RemoveAt_m43897_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43897_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WordBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43897_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7830_IList_1_RemoveAt_m43897_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43897_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7830_IList_1_get_Item_m43893_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType WordBehaviour_t68_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43893_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WordBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43893_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7830_il2cpp_TypeInfo/* declaring_type */
	, &WordBehaviour_t68_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7830_IList_1_get_Item_m43893_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43893_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WordBehaviour_t68_0_0_0;
static ParameterInfo IList_1_t7830_IList_1_set_Item_m43894_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WordBehaviour_t68_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43894_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WordBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43894_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7830_IList_1_set_Item_m43894_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43894_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7830_MethodInfos[] =
{
	&IList_1_IndexOf_m43895_MethodInfo,
	&IList_1_Insert_m43896_MethodInfo,
	&IList_1_RemoveAt_m43897_MethodInfo,
	&IList_1_get_Item_m43893_MethodInfo,
	&IList_1_set_Item_m43894_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7830_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7829_il2cpp_TypeInfo,
	&IEnumerable_1_t7831_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7830_0_0_0;
extern Il2CppType IList_1_t7830_1_0_0;
struct IList_1_t7830;
extern Il2CppGenericClass IList_1_t7830_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7830_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7830_MethodInfos/* methods */
	, IList_1_t7830_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7830_il2cpp_TypeInfo/* element_class */
	, IList_1_t7830_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7830_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7830_0_0_0/* byval_arg */
	, &IList_1_t7830_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7830_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t4183_il2cpp_TypeInfo;

// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m37091_MethodInfo;
static PropertyInfo ICollection_1_t4183____Count_PropertyInfo = 
{
	&ICollection_1_t4183_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m37091_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43898_MethodInfo;
static PropertyInfo ICollection_1_t4183____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t4183_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43898_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t4183_PropertyInfos[] =
{
	&ICollection_1_t4183____Count_PropertyInfo,
	&ICollection_1_t4183____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m37091_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m37091_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t4183_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m37091_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43898_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43898_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t4183_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43898_GenericMethod/* genericMethod */

};
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
static ParameterInfo ICollection_1_t4183_ICollection_1_Add_m43899_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WordAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43899_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43899_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t4183_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t4183_ICollection_1_Add_m43899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43899_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43900_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43900_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t4183_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43900_GenericMethod/* genericMethod */

};
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
static ParameterInfo ICollection_1_t4183_ICollection_1_Contains_m37136_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WordAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m37136_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m37136_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t4183_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t4183_ICollection_1_Contains_m37136_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m37136_GenericMethod/* genericMethod */

};
extern Il2CppType WordAbstractBehaviourU5BU5D_t871_0_0_0;
extern Il2CppType WordAbstractBehaviourU5BU5D_t871_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t4183_ICollection_1_CopyTo_m43901_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WordAbstractBehaviourU5BU5D_t871_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43901_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43901_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t4183_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t4183_ICollection_1_CopyTo_m43901_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43901_GenericMethod/* genericMethod */

};
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
static ParameterInfo ICollection_1_t4183_ICollection_1_Remove_m43902_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WordAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43902_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43902_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t4183_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t4183_ICollection_1_Remove_m43902_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43902_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t4183_MethodInfos[] =
{
	&ICollection_1_get_Count_m37091_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43898_MethodInfo,
	&ICollection_1_Add_m43899_MethodInfo,
	&ICollection_1_Clear_m43900_MethodInfo,
	&ICollection_1_Contains_m37136_MethodInfo,
	&ICollection_1_CopyTo_m43901_MethodInfo,
	&ICollection_1_Remove_m43902_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t737_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t4183_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t737_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t4183_0_0_0;
extern Il2CppType ICollection_1_t4183_1_0_0;
struct ICollection_1_t4183;
extern Il2CppGenericClass ICollection_1_t4183_GenericClass;
TypeInfo ICollection_1_t4183_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t4183_MethodInfos/* methods */
	, ICollection_1_t4183_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t4183_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t4183_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t4183_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t4183_0_0_0/* byval_arg */
	, &ICollection_1_t4183_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t4183_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour>
extern Il2CppType IEnumerator_1_t884_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m5276_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m5276_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t737_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m5276_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t737_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m5276_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t737_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t737_0_0_0;
extern Il2CppType IEnumerable_1_t737_1_0_0;
struct IEnumerable_1_t737;
extern Il2CppGenericClass IEnumerable_1_t737_GenericClass;
TypeInfo IEnumerable_1_t737_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t737_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t737_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t737_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t737_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t737_0_0_0/* byval_arg */
	, &IEnumerable_1_t737_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t737_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t884_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.WordAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WordAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m5277_MethodInfo;
static PropertyInfo IEnumerator_1_t884____Current_PropertyInfo = 
{
	&IEnumerator_1_t884_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m5277_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t884_PropertyInfos[] =
{
	&IEnumerator_1_t884____Current_PropertyInfo,
	NULL
};
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m5277_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WordAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m5277_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t884_il2cpp_TypeInfo/* declaring_type */
	, &WordAbstractBehaviour_t34_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m5277_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t884_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m5277_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t884_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t884_0_0_0;
extern Il2CppType IEnumerator_1_t884_1_0_0;
struct IEnumerator_1_t884;
extern Il2CppGenericClass IEnumerator_1_t884_GenericClass;
TypeInfo IEnumerator_1_t884_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t884_MethodInfos/* methods */
	, IEnumerator_1_t884_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t884_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t884_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t884_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t884_0_0_0/* byval_arg */
	, &IEnumerator_1_t884_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t884_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_109.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2991_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_109MethodDeclarations.h"

extern TypeInfo WordAbstractBehaviour_t34_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15333_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWordAbstractBehaviour_t34_m33510_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WordAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WordAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisWordAbstractBehaviour_t34_m33510(__this, p0, method) (WordAbstractBehaviour_t34 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2991____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2991_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2991, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2991____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2991_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2991, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2991_FieldInfos[] =
{
	&InternalEnumerator_1_t2991____array_0_FieldInfo,
	&InternalEnumerator_1_t2991____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15330_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2991____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2991_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15330_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2991____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2991_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15333_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2991_PropertyInfos[] =
{
	&InternalEnumerator_1_t2991____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2991____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2991_InternalEnumerator_1__ctor_m15329_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15329_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15329_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2991_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2991_InternalEnumerator_1__ctor_m15329_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15329_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15330_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15330_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2991_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15330_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15331_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15331_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2991_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15331_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15332_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15332_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2991_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15332_GenericMethod/* genericMethod */

};
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15333_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WordAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15333_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2991_il2cpp_TypeInfo/* declaring_type */
	, &WordAbstractBehaviour_t34_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15333_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2991_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15329_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15330_MethodInfo,
	&InternalEnumerator_1_Dispose_m15331_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15332_MethodInfo,
	&InternalEnumerator_1_get_Current_m15333_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15332_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15331_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2991_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15330_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15332_MethodInfo,
	&InternalEnumerator_1_Dispose_m15331_MethodInfo,
	&InternalEnumerator_1_get_Current_m15333_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2991_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t884_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2991_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t884_il2cpp_TypeInfo, 7},
};
extern TypeInfo WordAbstractBehaviour_t34_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2991_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15333_MethodInfo/* Method Usage */,
	&WordAbstractBehaviour_t34_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWordAbstractBehaviour_t34_m33510_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2991_0_0_0;
extern Il2CppType InternalEnumerator_1_t2991_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2991_GenericClass;
TypeInfo InternalEnumerator_1_t2991_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2991_MethodInfos/* methods */
	, InternalEnumerator_1_t2991_PropertyInfos/* properties */
	, InternalEnumerator_1_t2991_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2991_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2991_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2991_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2991_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2991_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2991_1_0_0/* this_arg */
	, InternalEnumerator_1_t2991_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2991_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2991_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2991)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t4187_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m37092_MethodInfo;
extern MethodInfo IList_1_set_Item_m43903_MethodInfo;
static PropertyInfo IList_1_t4187____Item_PropertyInfo = 
{
	&IList_1_t4187_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m37092_MethodInfo/* get */
	, &IList_1_set_Item_m43903_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t4187_PropertyInfos[] =
{
	&IList_1_t4187____Item_PropertyInfo,
	NULL
};
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
static ParameterInfo IList_1_t4187_IList_1_IndexOf_m43904_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WordAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43904_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43904_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t4187_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4187_IList_1_IndexOf_m43904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43904_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
static ParameterInfo IList_1_t4187_IList_1_Insert_m43905_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WordAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43905_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43905_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t4187_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4187_IList_1_Insert_m43905_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43905_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t4187_IList_1_RemoveAt_m43906_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43906_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43906_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t4187_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t4187_IList_1_RemoveAt_m43906_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43906_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t4187_IList_1_get_Item_m37092_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m37092_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m37092_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t4187_il2cpp_TypeInfo/* declaring_type */
	, &WordAbstractBehaviour_t34_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t4187_IList_1_get_Item_m37092_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m37092_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
static ParameterInfo IList_1_t4187_IList_1_set_Item_m43903_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WordAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43903_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43903_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t4187_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t4187_IList_1_set_Item_m43903_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43903_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t4187_MethodInfos[] =
{
	&IList_1_IndexOf_m43904_MethodInfo,
	&IList_1_Insert_m43905_MethodInfo,
	&IList_1_RemoveAt_m43906_MethodInfo,
	&IList_1_get_Item_m37092_MethodInfo,
	&IList_1_set_Item_m43903_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t4187_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t4183_il2cpp_TypeInfo,
	&IEnumerable_1_t737_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t4187_0_0_0;
extern Il2CppType IList_1_t4187_1_0_0;
struct IList_1_t4187;
extern Il2CppGenericClass IList_1_t4187_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t4187_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t4187_MethodInfos/* methods */
	, IList_1_t4187_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t4187_il2cpp_TypeInfo/* element_class */
	, IList_1_t4187_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t4187_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t4187_0_0_0/* byval_arg */
	, &IList_1_t4187_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t4187_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7832_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>
extern MethodInfo ICollection_1_get_Count_m43907_MethodInfo;
static PropertyInfo ICollection_1_t7832____Count_PropertyInfo = 
{
	&ICollection_1_t7832_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43907_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43908_MethodInfo;
static PropertyInfo ICollection_1_t7832____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7832_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43908_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7832_PropertyInfos[] =
{
	&ICollection_1_t7832____Count_PropertyInfo,
	&ICollection_1_t7832____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43907_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m43907_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7832_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43907_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43908_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43908_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7832_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43908_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
static ParameterInfo ICollection_1_t7832_ICollection_1_Add_m43909_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorWordBehaviour_t178_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43909_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m43909_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7832_ICollection_1_Add_m43909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43909_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43910_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m43910_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43910_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
static ParameterInfo ICollection_1_t7832_ICollection_1_Contains_m43911_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorWordBehaviour_t178_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43911_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m43911_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7832_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7832_ICollection_1_Contains_m43911_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43911_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorWordBehaviourU5BU5D_t5652_0_0_0;
extern Il2CppType IEditorWordBehaviourU5BU5D_t5652_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7832_ICollection_1_CopyTo_m43912_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorWordBehaviourU5BU5D_t5652_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43912_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43912_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7832_ICollection_1_CopyTo_m43912_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43912_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
static ParameterInfo ICollection_1_t7832_ICollection_1_Remove_m43913_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorWordBehaviour_t178_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43913_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorWordBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m43913_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7832_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7832_ICollection_1_Remove_m43913_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43913_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7832_MethodInfos[] =
{
	&ICollection_1_get_Count_m43907_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43908_MethodInfo,
	&ICollection_1_Add_m43909_MethodInfo,
	&ICollection_1_Clear_m43910_MethodInfo,
	&ICollection_1_Contains_m43911_MethodInfo,
	&ICollection_1_CopyTo_m43912_MethodInfo,
	&ICollection_1_Remove_m43913_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7834_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7832_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7834_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7832_0_0_0;
extern Il2CppType ICollection_1_t7832_1_0_0;
struct ICollection_1_t7832;
extern Il2CppGenericClass ICollection_1_t7832_GenericClass;
TypeInfo ICollection_1_t7832_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7832_MethodInfos/* methods */
	, ICollection_1_t7832_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7832_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7832_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7832_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7832_0_0_0/* byval_arg */
	, &ICollection_1_t7832_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7832_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorWordBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorWordBehaviour>
extern Il2CppType IEnumerator_1_t6133_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43914_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorWordBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43914_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7834_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6133_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43914_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7834_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43914_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7834_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7834_0_0_0;
extern Il2CppType IEnumerable_1_t7834_1_0_0;
struct IEnumerable_1_t7834;
extern Il2CppGenericClass IEnumerable_1_t7834_GenericClass;
TypeInfo IEnumerable_1_t7834_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7834_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7834_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7834_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7834_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7834_0_0_0/* byval_arg */
	, &IEnumerable_1_t7834_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7834_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
