﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<ChooseTeam>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_41.h"
// UnityEngine.Events.CachedInvokableCall`1<ChooseTeam>
struct CachedInvokableCall_1_t3037  : public InvokableCall_1_t3038
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<ChooseTeam>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
