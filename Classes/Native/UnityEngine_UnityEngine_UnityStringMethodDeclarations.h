﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityString
struct UnityString_t1033;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t130;

// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
 String_t* UnityString_Format_m6101 (Object_t * __this/* static, unused */, String_t* ___fmt, ObjectU5BU5D_t130* ___args, MethodInfo* method) IL2CPP_METHOD_ATTR;
