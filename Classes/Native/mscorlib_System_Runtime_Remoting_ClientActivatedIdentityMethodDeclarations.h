﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ClientActivatedIdentity
struct ClientActivatedIdentity_t2102;
// System.MarshalByRefObject
struct MarshalByRefObject_t1401;

// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
 MarshalByRefObject_t1401 * ClientActivatedIdentity_GetServerObject_m11877 (ClientActivatedIdentity_t2102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
