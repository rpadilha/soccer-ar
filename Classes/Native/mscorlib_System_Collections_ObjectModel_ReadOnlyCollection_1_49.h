﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.IVirtualButtonEventHandler>
struct IList_1_t4589;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>
struct ReadOnlyCollection_1_t4585  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>::list
	Object_t* ___list_0;
};
