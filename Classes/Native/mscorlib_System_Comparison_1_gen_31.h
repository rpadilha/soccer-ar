﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
// System.Comparison`1<System.Int32>
struct Comparison_1_t4078  : public MulticastDelegate_t373
{
};
