﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScaleMode>
struct InternalEnumerator_1_t3735;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScaleMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m20408 (InternalEnumerator_1_t3735 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScaleMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20409 (InternalEnumerator_1_t3735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScaleMode>::Dispose()
 void InternalEnumerator_1_Dispose_m20410 (InternalEnumerator_1_t3735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScaleMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m20411 (InternalEnumerator_1_t3735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/ScaleMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m20412 (InternalEnumerator_1_t3735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
