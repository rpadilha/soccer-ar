﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
struct CachedInvokableCall_1_t3887;
// UnityEngine.Object
struct Object_t120;
struct Object_t120_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t141;
// Vuforia.SmartTerrainTrackerAbstractBehaviour
struct SmartTerrainTrackerAbstractBehaviour_t50;
// System.Object[]
struct ObjectU5BU5D_t130;

// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_4MethodDeclarations.h"
#define CachedInvokableCall_1__ctor_m21286(__this, ___target, ___theFunction, ___argument, method) (void)CachedInvokableCall_1__ctor_m14196_gshared((CachedInvokableCall_1_t2761 *)__this, (Object_t120 *)___target, (MethodInfo_t141 *)___theFunction, (Object_t *)___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Invoke(System.Object[])
#define CachedInvokableCall_1_Invoke_m21287(__this, ___args, method) (void)CachedInvokableCall_1_Invoke_m14198_gshared((CachedInvokableCall_1_t2761 *)__this, (ObjectU5BU5D_t130*)___args, method)
