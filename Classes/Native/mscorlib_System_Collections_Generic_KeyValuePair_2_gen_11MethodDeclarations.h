﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>
struct KeyValuePair_2_t4007;
// Vuforia.VirtualButton
struct VirtualButton_t639;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m22377 (KeyValuePair_2_t4007 * __this, int32_t ___key, VirtualButton_t639 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::get_Key()
 int32_t KeyValuePair_2_get_Key_m22378 (KeyValuePair_2_t4007 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m22379 (KeyValuePair_2_t4007 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::get_Value()
 VirtualButton_t639 * KeyValuePair_2_get_Value_m22380 (KeyValuePair_2_t4007 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m22381 (KeyValuePair_2_t4007 * __this, VirtualButton_t639 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::ToString()
 String_t* KeyValuePair_2_ToString_m22382 (KeyValuePair_2_t4007 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
