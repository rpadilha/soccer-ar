﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>
struct IList_1_t4386;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>
struct ReadOnlyCollection_1_t4383  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::list
	Object_t* ___list_0;
};
