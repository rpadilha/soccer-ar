﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Image
struct Image_t360;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.UI.Image>
struct CastHelper_1_t3678 
{
	// T UnityEngine.CastHelper`1<UnityEngine.UI.Image>::t
	Image_t360 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.UI.Image>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
