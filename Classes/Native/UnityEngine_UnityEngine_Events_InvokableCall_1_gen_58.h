﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<ObliqueNear>
struct UnityAction_1_t3137;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<ObliqueNear>
struct InvokableCall_1_t3136  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<ObliqueNear>::Delegate
	UnityAction_1_t3137 * ___Delegate_0;
};
