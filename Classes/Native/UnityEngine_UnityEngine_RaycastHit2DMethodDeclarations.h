﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RaycastHit2D
struct RaycastHit2D_t190;
// UnityEngine.Collider2D
struct Collider2D_t191;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1058;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
 Vector2_t99  RaycastHit2D_get_point_m2209 (RaycastHit2D_t190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
 Vector2_t99  RaycastHit2D_get_normal_m2210 (RaycastHit2D_t190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit2D::get_fraction()
 float RaycastHit2D_get_fraction_m2344 (RaycastHit2D_t190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
 Collider2D_t191 * RaycastHit2D_get_collider_m638 (RaycastHit2D_t190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
 Rigidbody2D_t1058 * RaycastHit2D_get_rigidbody_m6271 (RaycastHit2D_t190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
 Transform_t74 * RaycastHit2D_get_transform_m2212 (RaycastHit2D_t190 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
