﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct InvokableCall_2_t4988;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t141;
// System.Object[]
struct ObjectU5BU5D_t130;

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
 void InvokableCall_2__ctor_m30118_gshared (InvokableCall_2_t4988 * __this, Object_t * ___target, MethodInfo_t141 * ___theFunction, MethodInfo* method);
#define InvokableCall_2__ctor_m30118(__this, ___target, ___theFunction, method) (void)InvokableCall_2__ctor_m30118_gshared((InvokableCall_2_t4988 *)__this, (Object_t *)___target, (MethodInfo_t141 *)___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Invoke(System.Object[])
 void InvokableCall_2_Invoke_m30119_gshared (InvokableCall_2_t4988 * __this, ObjectU5BU5D_t130* ___args, MethodInfo* method);
#define InvokableCall_2_Invoke_m30119(__this, ___args, method) (void)InvokableCall_2_Invoke_m30119_gshared((InvokableCall_2_t4988 *)__this, (ObjectU5BU5D_t130*)___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
 bool InvokableCall_2_Find_m30120_gshared (InvokableCall_2_t4988 * __this, Object_t * ___targetObj, MethodInfo_t141 * ___method, MethodInfo* method);
#define InvokableCall_2_Find_m30120(__this, ___targetObj, ___method, method) (bool)InvokableCall_2_Find_m30120_gshared((InvokableCall_2_t4988 *)__this, (Object_t *)___targetObj, (MethodInfo_t141 *)___method, method)
