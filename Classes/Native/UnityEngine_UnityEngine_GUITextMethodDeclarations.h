﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIText
struct GUIText_t192;
// System.String
struct String_t;

// System.Void UnityEngine.GUIText::set_text(System.String)
 void GUIText_set_text_m645 (GUIText_t192 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
