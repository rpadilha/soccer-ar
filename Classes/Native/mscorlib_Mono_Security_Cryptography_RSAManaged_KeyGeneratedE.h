﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.EventArgs
struct EventArgs_t1620;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
struct KeyGeneratedEventHandler_t1823  : public MulticastDelegate_t373
{
};
