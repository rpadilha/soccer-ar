﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARNullWrapper
struct QCARNullWrapper_t751;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t466;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceInitCamera(System.Int32)
 int32_t QCARNullWrapper_CameraDeviceInitCamera_m3626 (QCARNullWrapper_t751 * __this, int32_t ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceDeinitCamera()
 int32_t QCARNullWrapper_CameraDeviceDeinitCamera_m3627 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceStartCamera()
 int32_t QCARNullWrapper_CameraDeviceStartCamera_m3628 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceStopCamera()
 int32_t QCARNullWrapper_CameraDeviceStopCamera_m3629 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceGetNumVideoModes()
 int32_t QCARNullWrapper_CameraDeviceGetNumVideoModes_m3630 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::CameraDeviceGetVideoMode(System.Int32,System.IntPtr)
 void QCARNullWrapper_CameraDeviceGetVideoMode_m3631 (QCARNullWrapper_t751 * __this, int32_t ___idx, IntPtr_t121 ___videoMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSelectVideoMode(System.Int32)
 int32_t QCARNullWrapper_CameraDeviceSelectVideoMode_m3632 (QCARNullWrapper_t751 * __this, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSetFlashTorchMode(System.Int32)
 int32_t QCARNullWrapper_CameraDeviceSetFlashTorchMode_m3633 (QCARNullWrapper_t751 * __this, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSetFocusMode(System.Int32)
 int32_t QCARNullWrapper_CameraDeviceSetFocusMode_m3634 (QCARNullWrapper_t751 * __this, int32_t ___focusMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
 int32_t QCARNullWrapper_CameraDeviceSetCameraConfiguration_m3635 (QCARNullWrapper_t751 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarSetFrameFormat(System.Int32,System.Int32)
 int32_t QCARNullWrapper_QcarSetFrameFormat_m3636 (QCARNullWrapper_t751 * __this, int32_t ___format, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetExists(System.String,System.Int32)
 int32_t QCARNullWrapper_DataSetExists_m3637 (QCARNullWrapper_t751 * __this, String_t* ___relativePath, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetLoad(System.String,System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_DataSetLoad_m3638 (QCARNullWrapper_t751 * __this, String_t* ___relativePath, int32_t ___storageType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_DataSetGetNumTrackableType_m3639 (QCARNullWrapper_t751 * __this, int32_t ___trackableType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_DataSetGetTrackablesOfType_m3640 (QCARNullWrapper_t751 * __this, int32_t ___trackableType, IntPtr_t121 ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNullWrapper_DataSetGetTrackableName_m3641 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, StringBuilder_t466 * ___trackableName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_DataSetCreateTrackable_m3642 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, IntPtr_t121 ___trackableSourcePtr, StringBuilder_t466 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetDestroyTrackable(System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_DataSetDestroyTrackable_m3643 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetHasReachedTrackableLimit(System.IntPtr)
 int32_t QCARNullWrapper_DataSetHasReachedTrackableLimit_m3644 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::GetCameraThreadID()
 int32_t QCARNullWrapper_GetCameraThreadID_m3645 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetBuilderBuild(System.String,System.Single)
 int32_t QCARNullWrapper_ImageTargetBuilderBuild_m3646 (QCARNullWrapper_t751 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::FrameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
 void QCARNullWrapper_FrameCounterGetBenchmarkingData_m3647 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___benchmarkingData, bool ___isStereoRendering, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ImageTargetBuilderStartScan()
 void QCARNullWrapper_ImageTargetBuilderStartScan_m3648 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ImageTargetBuilderStopScan()
 void QCARNullWrapper_ImageTargetBuilderStopScan_m3649 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetBuilderGetFrameQuality()
 int32_t QCARNullWrapper_ImageTargetBuilderGetFrameQuality_m3650 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::ImageTargetBuilderGetTrackableSource()
 IntPtr_t121 QCARNullWrapper_ImageTargetBuilderGetTrackableSource_m3651 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNullWrapper_ImageTargetCreateVirtualButton_m3652 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
 int32_t QCARNullWrapper_ImageTargetDestroyVirtualButton_m3653 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String)
 int32_t QCARNullWrapper_VirtualButtonGetId_m3654 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetGetNumVirtualButtons(System.IntPtr,System.String)
 int32_t QCARNullWrapper_ImageTargetGetNumVirtualButtons_m3655 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
 int32_t QCARNullWrapper_ImageTargetGetVirtualButtons_m3656 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___virtualButtonDataArray, IntPtr_t121 ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNullWrapper_ImageTargetGetVirtualButtonName_m3657 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t466 * ___vbName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNullWrapper_CylinderTargetGetDimensions_m3658 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___dimensions, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
 int32_t QCARNullWrapper_CylinderTargetSetSideLength_m3659 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___sideLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNullWrapper_CylinderTargetSetTopDiameter_m3660 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNullWrapper_CylinderTargetSetBottomDiameter_m3661 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNullWrapper_ObjectTargetSetSize_m3662 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNullWrapper_ObjectTargetGetSize_m3663 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerStart()
 int32_t QCARNullWrapper_ObjectTrackerStart_m3664 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ObjectTrackerStop()
 void QCARNullWrapper_ObjectTrackerStop_m3665 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::ObjectTrackerCreateDataSet()
 IntPtr_t121 QCARNullWrapper_ObjectTrackerCreateDataSet_m3666 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerDestroyDataSet(System.IntPtr)
 int32_t QCARNullWrapper_ObjectTrackerDestroyDataSet_m3667 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerActivateDataSet(System.IntPtr)
 int32_t QCARNullWrapper_ObjectTrackerActivateDataSet_m3668 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerDeactivateDataSet(System.IntPtr)
 int32_t QCARNullWrapper_ObjectTrackerDeactivateDataSet_m3669 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerPersistExtendedTracking(System.Int32)
 int32_t QCARNullWrapper_ObjectTrackerPersistExtendedTracking_m3670 (QCARNullWrapper_t751 * __this, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerResetExtendedTracking()
 int32_t QCARNullWrapper_ObjectTrackerResetExtendedTracking_m3671 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerTrackerStart()
 int32_t QCARNullWrapper_MarkerTrackerStart_m3672 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::MarkerTrackerStop()
 void QCARNullWrapper_MarkerTrackerStop_m3673 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::OnSurfaceChanged(System.Int32,System.Int32)
 void QCARNullWrapper_OnSurfaceChanged_m3674 (QCARNullWrapper_t751 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::OnPause()
 void QCARNullWrapper_OnPause_m3675 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::OnResume()
 void QCARNullWrapper_OnResume_m3676 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::HasSurfaceBeenRecreated()
 bool QCARNullWrapper_HasSurfaceBeenRecreated_m3677 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerSetSize(System.Int32,System.Single)
 int32_t QCARNullWrapper_MarkerSetSize_m3678 (QCARNullWrapper_t751 * __this, int32_t ___trackableIndex, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerTrackerCreateMarker(System.Int32,System.String,System.Single)
 int32_t QCARNullWrapper_MarkerTrackerCreateMarker_m3679 (QCARNullWrapper_t751 * __this, int32_t ___id, String_t* ___trackableName, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerTrackerDestroyMarker(System.Int32)
 int32_t QCARNullWrapper_MarkerTrackerDestroyMarker_m3680 (QCARNullWrapper_t751 * __this, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::InitPlatformNative()
 void QCARNullWrapper_InitPlatformNative_m3681 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::InitFrameState(System.IntPtr)
 void QCARNullWrapper_InitFrameState_m3682 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::DeinitFrameState(System.IntPtr)
 void QCARNullWrapper_DeinitFrameState_m3683 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::UpdateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_UpdateQCAR_m3684 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t121 ___frameIndex, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererEnd()
 void QCARNullWrapper_RendererEnd_m3685 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32)
 int32_t QCARNullWrapper_QcarGetBufferSize_m3686 (QCARNullWrapper_t751 * __this, int32_t ___width, int32_t ___height, int32_t ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::QcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 void QCARNullWrapper_QcarAddCameraFrame_m3687 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererSetVideoBackgroundCfg(System.IntPtr)
 void QCARNullWrapper_RendererSetVideoBackgroundCfg_m3688 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererGetVideoBackgroundCfg(System.IntPtr)
 void QCARNullWrapper_RendererGetVideoBackgroundCfg_m3689 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererGetVideoBackgroundTextureInfo(System.IntPtr)
 void QCARNullWrapper_RendererGetVideoBackgroundTextureInfo_m3690 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___texInfo, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::RendererSetVideoBackgroundTextureID(System.Int32)
 int32_t QCARNullWrapper_RendererSetVideoBackgroundTextureID_m3691 (QCARNullWrapper_t751 * __this, int32_t ___textureID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::RendererIsVideoBackgroundTextureInfoAvailable()
 int32_t QCARNullWrapper_RendererIsVideoBackgroundTextureInfoAvailable_m3692 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarSetHint(System.Int32,System.Int32)
 int32_t QCARNullWrapper_QcarSetHint_m3693 (QCARNullWrapper_t751 * __this, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::GetProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_GetProjectionGL_m3694 (QCARNullWrapper_t751 * __this, float ___nearClip, float ___farClip, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::SetApplicationEnvironment(System.Int32,System.Int32,System.Int32)
 void QCARNullWrapper_SetApplicationEnvironment_m3695 (QCARNullWrapper_t751 * __this, int32_t ___major, int32_t ___minor, int32_t ___change, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::SetStateBufferSize(System.Int32)
 void QCARNullWrapper_SetStateBufferSize_m3696 (QCARNullWrapper_t751 * __this, int32_t ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::SmartTerrainTrackerStart()
 int32_t QCARNullWrapper_SmartTerrainTrackerStart_m3697 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::SmartTerrainTrackerStop()
 void QCARNullWrapper_SmartTerrainTrackerStop_m3698 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainTrackerSetScaleToMillimeter(System.Single)
 bool QCARNullWrapper_SmartTerrainTrackerSetScaleToMillimeter_m3699 (QCARNullWrapper_t751 * __this, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainTrackerInitBuilder()
 bool QCARNullWrapper_SmartTerrainTrackerInitBuilder_m3700 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainTrackerDeinitBuilder()
 bool QCARNullWrapper_SmartTerrainTrackerDeinitBuilder_m3701 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::SmartTerrainBuilderCreateReconstructionFromTarget()
 IntPtr_t121 QCARNullWrapper_SmartTerrainBuilderCreateReconstructionFromTarget_m3702 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::SmartTerrainBuilderCreateReconstructionFromEnvironment()
 IntPtr_t121 QCARNullWrapper_SmartTerrainBuilderCreateReconstructionFromEnvironment_m3703 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainBuilderAddReconstruction(System.IntPtr)
 bool QCARNullWrapper_SmartTerrainBuilderAddReconstruction_m3704 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainBuilderRemoveReconstruction(System.IntPtr)
 bool QCARNullWrapper_SmartTerrainBuilderRemoveReconstruction_m3705 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainBuilderDestroyReconstruction(System.IntPtr)
 bool QCARNullWrapper_SmartTerrainBuilderDestroyReconstruction_m3706 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionStart(System.IntPtr)
 bool QCARNullWrapper_ReconstructionStart_m3707 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionStop(System.IntPtr)
 bool QCARNullWrapper_ReconstructionStop_m3708 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionIsReconstructing(System.IntPtr)
 bool QCARNullWrapper_ReconstructionIsReconstructing_m3709 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionReset(System.IntPtr)
 bool QCARNullWrapper_ReconstructionReset_m3710 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ReconstructionSetNavMeshPadding(System.IntPtr,System.Single)
 void QCARNullWrapper_ReconstructionSetNavMeshPadding_m3711 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, float ___padding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
 bool QCARNullWrapper_ReconstructionFromTargetSetInitializationTarget_m3712 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, IntPtr_t121 ___occluderMin, IntPtr_t121 ___occluderMax, IntPtr_t121 ___offsetToOccluder, IntPtr_t121 ___rotationAxisToOccluder, float ___rotationAngleToOccluder, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
 bool QCARNullWrapper_ReconstructionSetMaximumArea_m3713 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, IntPtr_t121 ___maximumArea, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ReconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
 int32_t QCARNullWrapper_ReconstructioFromEnvironmentGetReconstructionState_m3714 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderStartInit(System.String,System.String)
 int32_t QCARNullWrapper_TargetFinderStartInit_m3715 (QCARNullWrapper_t751 * __this, String_t* ___userKey, String_t* ___secretKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderGetInitState()
 int32_t QCARNullWrapper_TargetFinderGetInitState_m3716 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderDeinit()
 int32_t QCARNullWrapper_TargetFinderDeinit_m3717 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderStartRecognition()
 int32_t QCARNullWrapper_TargetFinderStartRecognition_m3718 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderStop()
 int32_t QCARNullWrapper_TargetFinderStop_m3719 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
 void QCARNullWrapper_TargetFinderSetUIScanlineColor_m3720 (QCARNullWrapper_t751 * __this, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderSetUIPointColor(System.Single,System.Single,System.Single)
 void QCARNullWrapper_TargetFinderSetUIPointColor_m3721 (QCARNullWrapper_t751 * __this, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderUpdate(System.IntPtr)
 void QCARNullWrapper_TargetFinderUpdate_m3722 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___targetFinderState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderGetResults(System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_TargetFinderGetResults_m3723 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___searchResultArray, int32_t ___searchResultArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderEnableTracking(System.IntPtr,System.IntPtr)
 int32_t QCARNullWrapper_TargetFinderEnableTracking_m3724 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___searchResult, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderGetImageTargets(System.IntPtr,System.Int32)
 void QCARNullWrapper_TargetFinderGetImageTargets_m3725 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___trackableIdArray, int32_t ___trackableIdArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderClearTrackables()
 void QCARNullWrapper_TargetFinderClearTrackables_m3726 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TextTrackerStart()
 int32_t QCARNullWrapper_TextTrackerStart_m3727 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TextTrackerStop()
 void QCARNullWrapper_TextTrackerStop_m3728 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TextTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNullWrapper_TextTrackerSetRegionOfInterest_m3729 (QCARNullWrapper_t751 * __this, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TextTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
 void QCARNullWrapper_TextTrackerGetRegionOfInterest_m3730 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___detectionROI, IntPtr_t121 ___trackingROI, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListLoadWordList(System.String,System.Int32)
 int32_t QCARNullWrapper_WordListLoadWordList_m3731 (QCARNullWrapper_t751 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListAddWordsFromFile(System.String,System.Int32)
 int32_t QCARNullWrapper_WordListAddWordsFromFile_m3732 (QCARNullWrapper_t751 * __this, String_t* ___path, int32_t ___storagetType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListAddWordU(System.IntPtr)
 int32_t QCARNullWrapper_WordListAddWordU_m3733 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListRemoveWordU(System.IntPtr)
 int32_t QCARNullWrapper_WordListRemoveWordU_m3734 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListContainsWordU(System.IntPtr)
 int32_t QCARNullWrapper_WordListContainsWordU_m3735 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListUnloadAllLists()
 int32_t QCARNullWrapper_WordListUnloadAllLists_m3736 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListSetFilterMode(System.Int32)
 int32_t QCARNullWrapper_WordListSetFilterMode_m3737 (QCARNullWrapper_t751 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListGetFilterMode()
 int32_t QCARNullWrapper_WordListGetFilterMode_m3738 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListLoadFilterList(System.String,System.Int32)
 int32_t QCARNullWrapper_WordListLoadFilterList_m3739 (QCARNullWrapper_t751 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListAddWordToFilterListU(System.IntPtr)
 int32_t QCARNullWrapper_WordListAddWordToFilterListU_m3740 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListRemoveWordFromFilterListU(System.IntPtr)
 int32_t QCARNullWrapper_WordListRemoveWordFromFilterListU_m3741 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListClearFilterList()
 int32_t QCARNullWrapper_WordListClearFilterList_m3742 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListGetFilterListWordCount()
 int32_t QCARNullWrapper_WordListGetFilterListWordCount_m3743 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::WordListGetFilterListWordU(System.Int32)
 IntPtr_t121 QCARNullWrapper_WordListGetFilterListWordU_m3744 (QCARNullWrapper_t751 * __this, int32_t ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordGetLetterMask(System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_WordGetLetterMask_m3745 (QCARNullWrapper_t751 * __this, int32_t ___wordID, IntPtr_t121 ___letterMaskImage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_WordGetLetterBoundingBoxes_m3746 (QCARNullWrapper_t751 * __this, int32_t ___wordID, IntPtr_t121 ___letterBoundingBoxes, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TrackerManagerInitTracker(System.Int32)
 int32_t QCARNullWrapper_TrackerManagerInitTracker_m3747 (QCARNullWrapper_t751 * __this, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TrackerManagerDeinitTracker(System.Int32)
 int32_t QCARNullWrapper_TrackerManagerDeinitTracker_m3748 (QCARNullWrapper_t751 * __this, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNullWrapper_VirtualButtonSetEnabled_m3749 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNullWrapper_VirtualButtonSetSensitivity_m3750 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNullWrapper_VirtualButtonSetAreaRectangle_m3751 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarInit(System.String)
 int32_t QCARNullWrapper_QcarInit_m3752 (QCARNullWrapper_t751 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarDeinit()
 int32_t QCARNullWrapper_QcarDeinit_m3753 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::StartExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_StartExtendedTracking_m3754 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::StopExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_StopExtendedTracking_m3755 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsSupportedDeviceDetected()
 bool QCARNullWrapper_EyewearIsSupportedDeviceDetected_m3756 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsSeeThru()
 bool QCARNullWrapper_EyewearIsSeeThru_m3757 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearGetScreenOrientation()
 int32_t QCARNullWrapper_EyewearGetScreenOrientation_m3758 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsStereoCapable()
 bool QCARNullWrapper_EyewearIsStereoCapable_m3759 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsStereoEnabled()
 bool QCARNullWrapper_EyewearIsStereoEnabled_m3760 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsStereoGLOnly()
 bool QCARNullWrapper_EyewearIsStereoGLOnly_m3761 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearSetStereo(System.Boolean)
 bool QCARNullWrapper_EyewearSetStereo_m3762 (QCARNullWrapper_t751 * __this, bool ___enable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearGetDefaultSceneScale(System.IntPtr)
 int32_t QCARNullWrapper_EyewearGetDefaultSceneScale_m3763 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_EyewearGetProjectionMatrix_m3764 (QCARNullWrapper_t751 * __this, int32_t ___eyeID, int32_t ___profileID, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetMaxCount()
 int32_t QCARNullWrapper_EyewearCPMGetMaxCount_m3765 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetUsedCount()
 int32_t QCARNullWrapper_EyewearCPMGetUsedCount_m3766 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMIsProfileUsed(System.Int32)
 bool QCARNullWrapper_EyewearCPMIsProfileUsed_m3767 (QCARNullWrapper_t751 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetActiveProfile()
 int32_t QCARNullWrapper_EyewearCPMGetActiveProfile_m3768 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMSetActiveProfile(System.Int32)
 bool QCARNullWrapper_EyewearCPMSetActiveProfile_m3769 (QCARNullWrapper_t751 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_EyewearCPMGetProjectionMatrix_m3770 (QCARNullWrapper_t751 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 bool QCARNullWrapper_EyewearCPMSetProjectionMatrix_m3771 (QCARNullWrapper_t751 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::EyewearCPMGetProfileName(System.Int32)
 IntPtr_t121 QCARNullWrapper_EyewearCPMGetProfileName_m3772 (QCARNullWrapper_t751 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMSetProfileName(System.Int32,System.IntPtr)
 bool QCARNullWrapper_EyewearCPMSetProfileName_m3773 (QCARNullWrapper_t751 * __this, int32_t ___profileID, IntPtr_t121 ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMClearProfile(System.Int32)
 bool QCARNullWrapper_EyewearCPMClearProfile_m3774 (QCARNullWrapper_t751 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
 bool QCARNullWrapper_EyewearUserCalibratorInit_m3775 (QCARNullWrapper_t751 * __this, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNullWrapper::EyewearUserCalibratorGetMinScaleHint()
 float QCARNullWrapper_EyewearUserCalibratorGetMinScaleHint_m3776 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNullWrapper::EyewearUserCalibratorGetMaxScaleHint()
 float QCARNullWrapper_EyewearUserCalibratorGetMaxScaleHint_m3777 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearUserCalibratorIsStereoStretched()
 bool QCARNullWrapper_EyewearUserCalibratorIsStereoStretched_m3778 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
 bool QCARNullWrapper_EyewearUserCalibratorGetProjectionMatrix_m3779 (QCARNullWrapper_t751 * __this, IntPtr_t121 ___readingsArray, int32_t ___numReadings, IntPtr_t121 ___calibrationResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::.ctor()
 void QCARNullWrapper__ctor_m3780 (QCARNullWrapper_t751 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
