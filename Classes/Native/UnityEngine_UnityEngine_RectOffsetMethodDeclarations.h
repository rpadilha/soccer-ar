﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RectOffset
struct RectOffset_t439;
// UnityEngine.GUIStyle
struct GUIStyle_t999;
// System.String
struct String_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void UnityEngine.RectOffset::.ctor()
 void RectOffset__ctor_m2688 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
 void RectOffset__ctor_m5879 (RectOffset_t439 * __this, GUIStyle_t999 * ___sourceStyle, IntPtr_t121 ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
 void RectOffset__ctor_m5880 (RectOffset_t439 * __this, int32_t ___left, int32_t ___right, int32_t ___top, int32_t ___bottom, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Finalize()
 void RectOffset_Finalize_m5881 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Init()
 void RectOffset_Init_m5882 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Cleanup()
 void RectOffset_Cleanup_m5883 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_left()
 int32_t RectOffset_get_left_m2669 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
 void RectOffset_set_left_m5884 (RectOffset_t439 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_right()
 int32_t RectOffset_get_right_m5885 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
 void RectOffset_set_right_m5886 (RectOffset_t439 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_top()
 int32_t RectOffset_get_top_m2670 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
 void RectOffset_set_top_m5887 (RectOffset_t439 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_bottom()
 int32_t RectOffset_get_bottom_m5888 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
 void RectOffset_set_bottom_m5889 (RectOffset_t439 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_horizontal()
 int32_t RectOffset_get_horizontal_m2663 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_vertical()
 int32_t RectOffset_get_vertical_m2664 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
 Rect_t103  RectOffset_Remove_m5890 (RectOffset_t439 * __this, Rect_t103  ___rect, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)
 Rect_t103  RectOffset_INTERNAL_CALL_Remove_m5891 (Object_t * __this/* static, unused */, RectOffset_t439 * ___self, Rect_t103 * ___rect, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.RectOffset::ToString()
 String_t* RectOffset_ToString_m5892 (RectOffset_t439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
