﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.ITrackableEventHandler>
struct EqualityComparer_1_t3843;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.ITrackableEventHandler>
struct EqualityComparer_1_t3843  : public Object_t
{
};
struct EqualityComparer_1_t3843_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.ITrackableEventHandler>::_default
	EqualityComparer_1_t3843 * ____default_0;
};
