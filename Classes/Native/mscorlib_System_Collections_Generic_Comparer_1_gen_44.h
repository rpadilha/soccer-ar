﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ITrackerEventHandler>
struct Comparer_1_t4500;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ITrackerEventHandler>
struct Comparer_1_t4500  : public Object_t
{
};
struct Comparer_1_t4500_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ITrackerEventHandler>::_default
	Comparer_1_t4500 * ____default_0;
};
