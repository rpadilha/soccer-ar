﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MultiTargetImpl
struct MultiTargetImpl_t671;
// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t612;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.MultiTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
 void MultiTargetImpl__ctor_m3123 (MultiTargetImpl_t671 * __this, String_t* ___name, int32_t ___id, DataSet_t612 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.MultiTargetImpl::GetSize()
 Vector3_t73  MultiTargetImpl_GetSize_m3124 (MultiTargetImpl_t671 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetImpl::SetSize(UnityEngine.Vector3)
 void MultiTargetImpl_SetSize_m3125 (MultiTargetImpl_t671 * __this, Vector3_t73  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
