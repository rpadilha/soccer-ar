﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>
struct EqualityComparer_1_t4511;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>
struct EqualityComparer_1_t4511  : public Object_t
{
};
struct EqualityComparer_1_t4511_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>::_default
	EqualityComparer_1_t4511 * ____default_0;
};
