﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t3;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>
struct UnityAction_1_t2769  : public MulticastDelegate_t373
{
};
