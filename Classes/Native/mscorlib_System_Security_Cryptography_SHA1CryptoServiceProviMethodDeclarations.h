﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SHA1CryptoServiceProvider
struct SHA1CryptoServiceProvider_t2166;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void System.Security.Cryptography.SHA1CryptoServiceProvider::.ctor()
 void SHA1CryptoServiceProvider__ctor_m12240 (SHA1CryptoServiceProvider_t2166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SHA1CryptoServiceProvider::Finalize()
 void SHA1CryptoServiceProvider_Finalize_m12241 (SHA1CryptoServiceProvider_t2166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SHA1CryptoServiceProvider::Dispose(System.Boolean)
 void SHA1CryptoServiceProvider_Dispose_m12242 (SHA1CryptoServiceProvider_t2166 * __this, bool ___disposing, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SHA1CryptoServiceProvider::HashCore(System.Byte[],System.Int32,System.Int32)
 void SHA1CryptoServiceProvider_HashCore_m12243 (SHA1CryptoServiceProvider_t2166 * __this, ByteU5BU5D_t653* ___rgb, int32_t ___ibStart, int32_t ___cbSize, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.SHA1CryptoServiceProvider::HashFinal()
 ByteU5BU5D_t653* SHA1CryptoServiceProvider_HashFinal_m12244 (SHA1CryptoServiceProvider_t2166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SHA1CryptoServiceProvider::Initialize()
 void SHA1CryptoServiceProvider_Initialize_m12245 (SHA1CryptoServiceProvider_t2166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
