﻿#pragma once
#include <stdint.h>
// Vuforia.ReconstructionAbstractBehaviour[]
struct ReconstructionAbstractBehaviourU5BU5D_t865;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>
struct List_1_t714  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::_items
	ReconstructionAbstractBehaviourU5BU5D_t865* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::_version
	int32_t ____version_3;
};
struct List_1_t714_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::EmptyArray
	ReconstructionAbstractBehaviourU5BU5D_t865* ___EmptyArray_4;
};
