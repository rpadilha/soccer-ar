﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct InternalEnumerator_1_t4070;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m23019 (InternalEnumerator_1_t4070 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23020 (InternalEnumerator_1_t4070 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::Dispose()
 void InternalEnumerator_1_Dispose_m23021 (InternalEnumerator_1_t4070 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m23022 (InternalEnumerator_1_t4070 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
 TrackableResultData_t684  InternalEnumerator_1_get_Current_m23023 (InternalEnumerator_1_t4070 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
