﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerStepThroughAttribute>
struct InternalEnumerator_1_t5185;
// System.Object
struct Object_t;
// System.Diagnostics.DebuggerStepThroughAttribute
struct DebuggerStepThroughAttribute_t1898;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerStepThroughAttribute>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m31299(__this, ___array, method) (void)InternalEnumerator_1__ctor_m14156_gshared((InternalEnumerator_1_t2752 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerStepThroughAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31300(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerStepThroughAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m31301(__this, method) (void)InternalEnumerator_1_Dispose_m14160_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerStepThroughAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m31302(__this, method) (bool)InternalEnumerator_1_MoveNext_m14162_gshared((InternalEnumerator_1_t2752 *)__this, method)
// T System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerStepThroughAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m31303(__this, method) (DebuggerStepThroughAttribute_t1898 *)InternalEnumerator_1_get_Current_m14164_gshared((InternalEnumerator_1_t2752 *)__this, method)
