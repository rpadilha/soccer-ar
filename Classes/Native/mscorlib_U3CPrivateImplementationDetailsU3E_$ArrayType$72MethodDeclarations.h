﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$72
struct $ArrayType$72_t2326;
struct $ArrayType$72_t2326_marshaled;

void $ArrayType$72_t2326_marshal(const $ArrayType$72_t2326& unmarshaled, $ArrayType$72_t2326_marshaled& marshaled);
void $ArrayType$72_t2326_marshal_back(const $ArrayType$72_t2326_marshaled& marshaled, $ArrayType$72_t2326& unmarshaled);
void $ArrayType$72_t2326_marshal_cleanup($ArrayType$72_t2326_marshaled& marshaled);
