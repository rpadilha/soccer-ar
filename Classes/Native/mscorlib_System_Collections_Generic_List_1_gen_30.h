﻿#pragma once
#include <stdint.h>
// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t4088;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct List_1_t710  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::_items
	SmartTerrainTrackableU5BU5D_t4088* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::_version
	int32_t ____version_3;
};
struct List_1_t710_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::EmptyArray
	SmartTerrainTrackableU5BU5D_t4088* ___EmptyArray_4;
};
