﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Type,System.UInt16>
struct Enumerator_t4123;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t726;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m23571 (Enumerator_t4123 * __this, Dictionary_2_t726 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Type,System.UInt16>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23572 (Enumerator_t4123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Type,System.UInt16>::Dispose()
 void Enumerator_Dispose_m23573 (Enumerator_t4123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Type,System.UInt16>::MoveNext()
 bool Enumerator_MoveNext_m23574 (Enumerator_t4123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Type,System.UInt16>::get_Current()
 Type_t * Enumerator_get_Current_m23575 (Enumerator_t4123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
