﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<ScoreHUD>
struct UnityAction_1_t3045;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<ScoreHUD>
struct InvokableCall_1_t3044  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<ScoreHUD>::Delegate
	UnityAction_1_t3045 * ___Delegate_0;
};
