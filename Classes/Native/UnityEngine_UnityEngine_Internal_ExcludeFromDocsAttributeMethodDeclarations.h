﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Internal.ExcludeFromDocsAttribute
struct ExcludeFromDocsAttribute_t1147;

// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
 void ExcludeFromDocsAttribute__ctor_m6572 (ExcludeFromDocsAttribute_t1147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
