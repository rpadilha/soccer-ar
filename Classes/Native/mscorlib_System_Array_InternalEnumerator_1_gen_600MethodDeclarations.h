﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UIntPtr>
struct InternalEnumerator_1_t5139;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void System.Array/InternalEnumerator`1<System.UIntPtr>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31069 (InternalEnumerator_1_t5139 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.UIntPtr>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31070 (InternalEnumerator_1_t5139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.UIntPtr>::Dispose()
 void InternalEnumerator_1_Dispose_m31071 (InternalEnumerator_1_t5139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.UIntPtr>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31072 (InternalEnumerator_1_t5139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.UIntPtr>::get_Current()
 UIntPtr_t1743  InternalEnumerator_1_get_Current_m31073 (InternalEnumerator_1_t5139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
