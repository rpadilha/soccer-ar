﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t1120;

// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
 void SharedBetweenAnimatorsAttribute__ctor_m6505 (SharedBetweenAnimatorsAttribute_t1120 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
