﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image,Vuforia.Image>
struct Transform_1_t3956;
// System.Object
struct Object_t;
// Vuforia.Image
struct Image_t604;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image,Vuforia.Image>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m21875 (Transform_1_t3956 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image,Vuforia.Image>::Invoke(TKey,TValue)
 Image_t604 * Transform_1_Invoke_m21876 (Transform_1_t3956 * __this, int32_t ___key, Image_t604 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image,Vuforia.Image>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m21877 (Transform_1_t3956 * __this, int32_t ___key, Image_t604 * ___value, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image,Vuforia.Image>::EndInvoke(System.IAsyncResult)
 Image_t604 * Transform_1_EndInvoke_m21878 (Transform_1_t3956 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
