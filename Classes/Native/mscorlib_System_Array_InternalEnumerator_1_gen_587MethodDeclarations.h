﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int64>
struct InternalEnumerator_1_t5126;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31004 (InternalEnumerator_1_t5126 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31005 (InternalEnumerator_1_t5126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
 void InternalEnumerator_1_Dispose_m31006 (InternalEnumerator_1_t5126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31007 (InternalEnumerator_1_t5126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
 int64_t InternalEnumerator_1_get_Current_m31008 (InternalEnumerator_1_t5126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
