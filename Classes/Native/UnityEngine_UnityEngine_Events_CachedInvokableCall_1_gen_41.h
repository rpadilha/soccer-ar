﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<GoalKeeperJump>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_37.h"
// UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump>
struct CachedInvokableCall_1_t3017  : public InvokableCall_1_t3018
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
