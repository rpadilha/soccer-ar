﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t5143;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t130;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t499;
// System.Exception
struct Exception_t151;

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
 void ArrayReadOnlyList_1__ctor_m31088_gshared (ArrayReadOnlyList_1_t5143 * __this, ObjectU5BU5D_t130* ___array, MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m31088(__this, ___array, method) (void)ArrayReadOnlyList_1__ctor_m31088_gshared((ArrayReadOnlyList_1_t5143 *)__this, (ObjectU5BU5D_t130*)___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m31089_gshared (ArrayReadOnlyList_1_t5143 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m31089(__this, method) (Object_t *)ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m31089_gshared((ArrayReadOnlyList_1_t5143 *)__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
 Object_t * ArrayReadOnlyList_1_get_Item_m31090_gshared (ArrayReadOnlyList_1_t5143 * __this, int32_t ___index, MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m31090(__this, ___index, method) (Object_t *)ArrayReadOnlyList_1_get_Item_m31090_gshared((ArrayReadOnlyList_1_t5143 *)__this, (int32_t)___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
 void ArrayReadOnlyList_1_set_Item_m31091_gshared (ArrayReadOnlyList_1_t5143 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m31091(__this, ___index, ___value, method) (void)ArrayReadOnlyList_1_set_Item_m31091_gshared((ArrayReadOnlyList_1_t5143 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
 int32_t ArrayReadOnlyList_1_get_Count_m31092_gshared (ArrayReadOnlyList_1_t5143 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m31092(__this, method) (int32_t)ArrayReadOnlyList_1_get_Count_m31092_gshared((ArrayReadOnlyList_1_t5143 *)__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
 bool ArrayReadOnlyList_1_get_IsReadOnly_m31093_gshared (ArrayReadOnlyList_1_t5143 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m31093(__this, method) (bool)ArrayReadOnlyList_1_get_IsReadOnly_m31093_gshared((ArrayReadOnlyList_1_t5143 *)__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
 void ArrayReadOnlyList_1_Add_m31094_gshared (ArrayReadOnlyList_1_t5143 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m31094(__this, ___item, method) (void)ArrayReadOnlyList_1_Add_m31094_gshared((ArrayReadOnlyList_1_t5143 *)__this, (Object_t *)___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
 void ArrayReadOnlyList_1_Clear_m31095_gshared (ArrayReadOnlyList_1_t5143 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m31095(__this, method) (void)ArrayReadOnlyList_1_Clear_m31095_gshared((ArrayReadOnlyList_1_t5143 *)__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
 bool ArrayReadOnlyList_1_Contains_m31096_gshared (ArrayReadOnlyList_1_t5143 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m31096(__this, ___item, method) (bool)ArrayReadOnlyList_1_Contains_m31096_gshared((ArrayReadOnlyList_1_t5143 *)__this, (Object_t *)___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
 void ArrayReadOnlyList_1_CopyTo_m31097_gshared (ArrayReadOnlyList_1_t5143 * __this, ObjectU5BU5D_t130* ___array, int32_t ___index, MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m31097(__this, ___array, ___index, method) (void)ArrayReadOnlyList_1_CopyTo_m31097_gshared((ArrayReadOnlyList_1_t5143 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
 Object_t* ArrayReadOnlyList_1_GetEnumerator_m31098_gshared (ArrayReadOnlyList_1_t5143 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m31098(__this, method) (Object_t*)ArrayReadOnlyList_1_GetEnumerator_m31098_gshared((ArrayReadOnlyList_1_t5143 *)__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
 int32_t ArrayReadOnlyList_1_IndexOf_m31099_gshared (ArrayReadOnlyList_1_t5143 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m31099(__this, ___item, method) (int32_t)ArrayReadOnlyList_1_IndexOf_m31099_gshared((ArrayReadOnlyList_1_t5143 *)__this, (Object_t *)___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
 void ArrayReadOnlyList_1_Insert_m31100_gshared (ArrayReadOnlyList_1_t5143 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m31100(__this, ___index, ___item, method) (void)ArrayReadOnlyList_1_Insert_m31100_gshared((ArrayReadOnlyList_1_t5143 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
 bool ArrayReadOnlyList_1_Remove_m31101_gshared (ArrayReadOnlyList_1_t5143 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m31101(__this, ___item, method) (bool)ArrayReadOnlyList_1_Remove_m31101_gshared((ArrayReadOnlyList_1_t5143 *)__this, (Object_t *)___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
 void ArrayReadOnlyList_1_RemoveAt_m31102_gshared (ArrayReadOnlyList_1_t5143 * __this, int32_t ___index, MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m31102(__this, ___index, method) (void)ArrayReadOnlyList_1_RemoveAt_m31102_gshared((ArrayReadOnlyList_1_t5143 *)__this, (int32_t)___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
 Exception_t151 * ArrayReadOnlyList_1_ReadOnlyError_m31103_gshared (Object_t * __this/* static, unused */, MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m31103(__this/* static, unused */, method) (Exception_t151 *)ArrayReadOnlyList_1_ReadOnlyError_m31103_gshared((Object_t *)__this/* static, unused */, method)
