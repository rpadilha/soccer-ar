﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
struct Enumerator_t945;
// System.Object
struct Object_t;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t812;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t811;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26MethodDeclarations.h"
#define Enumerator__ctor_m27477(__this, ___l, method) (void)Enumerator__ctor_m14558_gshared((Enumerator_t2837 *)__this, (List_1_t149 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27478(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14559_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::Dispose()
#define Enumerator_Dispose_m27479(__this, method) (void)Enumerator_Dispose_m14560_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::VerifyState()
#define Enumerator_VerifyState_m27480(__this, method) (void)Enumerator_VerifyState_m14561_gshared((Enumerator_t2837 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::MoveNext()
#define Enumerator_MoveNext_m5540(__this, method) (bool)Enumerator_MoveNext_m14562_gshared((Enumerator_t2837 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::get_Current()
#define Enumerator_get_Current_m5539(__this, method) (Object_t *)Enumerator_get_Current_m14563_gshared((Enumerator_t2837 *)__this, method)
