﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.MonoBehaviour[]
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t5678  : public Array_t
{
};
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct BehaviourU5BU5D_t5679  : public Array_t
{
};
// UnityEngine.Component[]
// UnityEngine.Component[]
struct ComponentU5BU5D_t3217  : public Array_t
{
};
// UnityEngine.Object[]
// UnityEngine.Object[]
struct ObjectU5BU5D_t227  : public Array_t
{
};
// UnityEngine.Renderer[]
// UnityEngine.Renderer[]
struct RendererU5BU5D_t131  : public Array_t
{
};
// UnityEngine.Collider[]
// UnityEngine.Collider[]
struct ColliderU5BU5D_t132  : public Array_t
{
};
// UnityEngine.Material[]
// UnityEngine.Material[]
struct MaterialU5BU5D_t97  : public Array_t
{
};
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct CameraU5BU5D_t172  : public Array_t
{
};
struct CameraU5BU5D_t172_StaticFields{
};
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t85  : public Array_t
{
};
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t95  : public Array_t
{
};
// UnityEngine.Mesh[]
// UnityEngine.Mesh[]
struct MeshU5BU5D_t96  : public Array_t
{
};
// UnityEngine.Touch[]
// UnityEngine.Touch[]
struct TouchU5BU5D_t203  : public Array_t
{
};
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct TransformU5BU5D_t3298  : public Array_t
{
};
// UnityEngine.CharacterController[]
// UnityEngine.CharacterController[]
struct CharacterControllerU5BU5D_t5680  : public Array_t
{
};
// UnityEngine.GUITexture[]
// UnityEngine.GUITexture[]
struct GUITextureU5BU5D_t5681  : public Array_t
{
};
// UnityEngine.GUIElement[]
// UnityEngine.GUIElement[]
struct GUIElementU5BU5D_t5682  : public Array_t
{
};
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t225  : public Array_t
{
};
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t492  : public Array_t
{
};
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t493  : public Array_t
{
};
// UnityEngine.Font[]
// UnityEngine.Font[]
struct FontU5BU5D_t3443  : public Array_t
{
};
struct FontU5BU5D_t3443_StaticFields{
};
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t379  : public Array_t
{
};
struct UIVertexU5BU5D_t379_StaticFields{
};
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t3524  : public Array_t
{
};
struct CanvasU5BU5D_t3524_StaticFields{
};
// UnityEngine.ICanvasRaycastFilter[]
// UnityEngine.ICanvasRaycastFilter[]
struct ICanvasRaycastFilterU5BU5D_t5683  : public Array_t
{
};
// UnityEngine.ISerializationCallbackReceiver[]
// UnityEngine.ISerializationCallbackReceiver[]
struct ISerializationCallbackReceiverU5BU5D_t5684  : public Array_t
{
};
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1083  : public Array_t
{
};
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1082  : public Array_t
{
};
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct CanvasGroupU5BU5D_t3656  : public Array_t
{
};
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3749  : public Array_t
{
};
struct RectTransformU5BU5D_t3749_StaticFields{
};
// UnityEngine.Color32[]
// UnityEngine.Color32[]
struct Color32U5BU5D_t654  : public Array_t
{
};
// UnityEngine.Color[]
// UnityEngine.Color[]
struct ColorU5BU5D_t839  : public Array_t
{
};
// UnityEngine.MeshFilter[]
// UnityEngine.MeshFilter[]
struct MeshFilterU5BU5D_t900  : public Array_t
{
};
// UnityEngine.MeshRenderer[]
// UnityEngine.MeshRenderer[]
struct MeshRendererU5BU5D_t4561  : public Array_t
{
};
// UnityEngine.WebCamDevice[]
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t930  : public Array_t
{
};
// UnityEngine.AssetBundle[]
// UnityEngine.AssetBundle[]
struct AssetBundleU5BU5D_t5685  : public Array_t
{
};
// UnityEngine.SendMessageOptions[]
// UnityEngine.SendMessageOptions[]
struct SendMessageOptionsU5BU5D_t5686  : public Array_t
{
};
// UnityEngine.PrimitiveType[]
// UnityEngine.PrimitiveType[]
struct PrimitiveTypeU5BU5D_t5687  : public Array_t
{
};
// UnityEngine.Space[]
// UnityEngine.Space[]
struct SpaceU5BU5D_t5688  : public Array_t
{
};
// UnityEngine.RuntimePlatform[]
// UnityEngine.RuntimePlatform[]
struct RuntimePlatformU5BU5D_t5689  : public Array_t
{
};
// UnityEngine.LogType[]
// UnityEngine.LogType[]
struct LogTypeU5BU5D_t5690  : public Array_t
{
};
// UnityEngine.ScriptableObject[]
// UnityEngine.ScriptableObject[]
struct ScriptableObjectU5BU5D_t5691  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct IAchievementDescriptionU5BU5D_t1156  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t1159  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t1106  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t1104  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t967  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t968  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct GcLeaderboardU5BU5D_t4622  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t973  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct AchievementU5BU5D_t1158  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t975  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct ScoreU5BU5D_t1160  : public Array_t
{
};
// UnityEngine.Texture[]
// UnityEngine.Texture[]
struct TextureU5BU5D_t5692  : public Array_t
{
};
// UnityEngine.Texture2D[]
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t5693  : public Array_t
{
};
// UnityEngine.RenderTexture[]
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t5694  : public Array_t
{
};
// UnityEngine.ReflectionProbe[]
// UnityEngine.ReflectionProbe[]
struct ReflectionProbeU5BU5D_t5695  : public Array_t
{
};
// UnityEngine.GUILayer[]
// UnityEngine.GUILayer[]
struct GUILayerU5BU5D_t5696  : public Array_t
{
};
// UnityEngine.ScaleMode[]
// UnityEngine.ScaleMode[]
struct ScaleModeU5BU5D_t5697  : public Array_t
{
};
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t1006  : public Array_t
{
};
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct LayoutCacheU5BU5D_t4680  : public Array_t
{
};
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t4699  : public Array_t
{
};
struct GUILayoutEntryU5BU5D_t4699_StaticFields{
};
// UnityEngine.GUILayoutOption/Type[]
// UnityEngine.GUILayoutOption/Type[]
struct TypeU5BU5D_t5698  : public Array_t
{
};
// UnityEngine.GUISkin[]
// UnityEngine.GUISkin[]
struct GUISkinU5BU5D_t5699  : public Array_t
{
};
struct GUISkinU5BU5D_t5699_StaticFields{
};
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t1015  : public Array_t
{
};
struct GUIStyleU5BU5D_t1015_StaticFields{
};
// UnityEngine.FontStyle[]
// UnityEngine.FontStyle[]
struct FontStyleU5BU5D_t5700  : public Array_t
{
};
// UnityEngine.TouchScreenKeyboardType[]
// UnityEngine.TouchScreenKeyboardType[]
struct TouchScreenKeyboardTypeU5BU5D_t5701  : public Array_t
{
};
// UnityEngine.KeyCode[]
// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t5702  : public Array_t
{
};
// UnityEngine.EventType[]
// UnityEngine.EventType[]
struct EventTypeU5BU5D_t5703  : public Array_t
{
};
// UnityEngine.EventModifiers[]
// UnityEngine.EventModifiers[]
struct EventModifiersU5BU5D_t5704  : public Array_t
{
};
// UnityEngine.DrivenTransformProperties[]
// UnityEngine.DrivenTransformProperties[]
struct DrivenTransformPropertiesU5BU5D_t5705  : public Array_t
{
};
// UnityEngine.RectTransform/Edge[]
// UnityEngine.RectTransform/Edge[]
struct EdgeU5BU5D_t5706  : public Array_t
{
};
// UnityEngine.RectTransform/Axis[]
// UnityEngine.RectTransform/Axis[]
struct AxisU5BU5D_t5707  : public Array_t
{
};
// UnityEngine.SerializePrivateVariables[]
// UnityEngine.SerializePrivateVariables[]
struct SerializePrivateVariablesU5BU5D_t5708  : public Array_t
{
};
// UnityEngine.SerializeField[]
// UnityEngine.SerializeField[]
struct SerializeFieldU5BU5D_t5709  : public Array_t
{
};
// UnityEngine.Shader[]
// UnityEngine.Shader[]
struct ShaderU5BU5D_t5710  : public Array_t
{
};
// UnityEngine.Sprite[]
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t5711  : public Array_t
{
};
// UnityEngine.SpriteRenderer[]
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t5712  : public Array_t
{
};
// UnityEngine.Display[]
// UnityEngine.Display[]
struct DisplayU5BU5D_t1039  : public Array_t
{
};
struct DisplayU5BU5D_t1039_StaticFields{
};
// UnityEngine.TouchPhase[]
// UnityEngine.TouchPhase[]
struct TouchPhaseU5BU5D_t5713  : public Array_t
{
};
// UnityEngine.IMECompositionMode[]
// UnityEngine.IMECompositionMode[]
struct IMECompositionModeU5BU5D_t5714  : public Array_t
{
};
// UnityEngine.HideFlags[]
// UnityEngine.HideFlags[]
struct HideFlagsU5BU5D_t5715  : public Array_t
{
};
// UnityEngine.ForceMode[]
// UnityEngine.ForceMode[]
struct ForceModeU5BU5D_t5716  : public Array_t
{
};
// UnityEngine.Rigidbody[]
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t5717  : public Array_t
{
};
// UnityEngine.BoxCollider[]
// UnityEngine.BoxCollider[]
struct BoxColliderU5BU5D_t5718  : public Array_t
{
};
// UnityEngine.MeshCollider[]
// UnityEngine.MeshCollider[]
struct MeshColliderU5BU5D_t5719  : public Array_t
{
};
// UnityEngine.CapsuleCollider[]
// UnityEngine.CapsuleCollider[]
struct CapsuleColliderU5BU5D_t5720  : public Array_t
{
};
// UnityEngine.ContactPoint[]
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t1055  : public Array_t
{
};
// UnityEngine.CollisionFlags[]
// UnityEngine.CollisionFlags[]
struct CollisionFlagsU5BU5D_t5721  : public Array_t
{
};
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t4827  : public Array_t
{
};
// UnityEngine.Collider2D[]
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t5722  : public Array_t
{
};
// UnityEngine.BoxCollider2D[]
// UnityEngine.BoxCollider2D[]
struct BoxCollider2DU5BU5D_t5723  : public Array_t
{
};
// UnityEngine.AudioClip[]
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t5724  : public Array_t
{
};
// UnityEngine.WebCamTexture[]
// UnityEngine.WebCamTexture[]
struct WebCamTextureU5BU5D_t5725  : public Array_t
{
};
// UnityEngine.AnimationEventSource[]
// UnityEngine.AnimationEventSource[]
struct AnimationEventSourceU5BU5D_t5726  : public Array_t
{
};
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068  : public Array_t
{
};
// UnityEngine.PlayMode[]
// UnityEngine.PlayMode[]
struct PlayModeU5BU5D_t5727  : public Array_t
{
};
// UnityEngine.QueueMode[]
// UnityEngine.QueueMode[]
struct QueueModeU5BU5D_t5728  : public Array_t
{
};
// UnityEngine.Animation[]
// UnityEngine.Animation[]
struct AnimationU5BU5D_t5729  : public Array_t
{
};
// UnityEngine.Animator[]
// UnityEngine.Animator[]
struct AnimatorU5BU5D_t5730  : public Array_t
{
};
// UnityEngine.RuntimeAnimatorController[]
// UnityEngine.RuntimeAnimatorController[]
struct RuntimeAnimatorControllerU5BU5D_t5731  : public Array_t
{
};
// UnityEngine.Terrain[]
// UnityEngine.Terrain[]
struct TerrainU5BU5D_t5732  : public Array_t
{
};
// UnityEngine.TextAnchor[]
// UnityEngine.TextAnchor[]
struct TextAnchorU5BU5D_t5733  : public Array_t
{
};
// UnityEngine.HorizontalWrapMode[]
// UnityEngine.HorizontalWrapMode[]
struct HorizontalWrapModeU5BU5D_t5734  : public Array_t
{
};
// UnityEngine.VerticalWrapMode[]
// UnityEngine.VerticalWrapMode[]
struct VerticalWrapModeU5BU5D_t5735  : public Array_t
{
};
// UnityEngine.GUIText[]
// UnityEngine.GUIText[]
struct GUITextU5BU5D_t5736  : public Array_t
{
};
// UnityEngine.RenderMode[]
// UnityEngine.RenderMode[]
struct RenderModeU5BU5D_t5737  : public Array_t
{
};
// UnityEngine.CanvasRenderer[]
// UnityEngine.CanvasRenderer[]
struct CanvasRendererU5BU5D_t5738  : public Array_t
{
};
// UnityEngine.WrapperlessIcall[]
// UnityEngine.WrapperlessIcall[]
struct WrapperlessIcallU5BU5D_t5739  : public Array_t
{
};
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t1086  : public Array_t
{
};
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t1087  : public Array_t
{
};
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t1088  : public Array_t
{
};
// UnityEngine.AddComponentMenu[]
// UnityEngine.AddComponentMenu[]
struct AddComponentMenuU5BU5D_t5740  : public Array_t
{
};
// UnityEngine.HideInInspector[]
// UnityEngine.HideInInspector[]
struct HideInInspectorU5BU5D_t5741  : public Array_t
{
};
// UnityEngine.WritableAttribute[]
// UnityEngine.WritableAttribute[]
struct WritableAttributeU5BU5D_t5742  : public Array_t
{
};
// UnityEngine.AssemblyIsEditorAssembly[]
// UnityEngine.AssemblyIsEditorAssembly[]
struct AssemblyIsEditorAssemblyU5BU5D_t5743  : public Array_t
{
};
// UnityEngine.CameraClearFlags[]
// UnityEngine.CameraClearFlags[]
struct CameraClearFlagsU5BU5D_t5744  : public Array_t
{
};
// UnityEngine.ScreenOrientation[]
// UnityEngine.ScreenOrientation[]
struct ScreenOrientationU5BU5D_t5745  : public Array_t
{
};
// UnityEngine.FilterMode[]
// UnityEngine.FilterMode[]
struct FilterModeU5BU5D_t5746  : public Array_t
{
};
// UnityEngine.TextureWrapMode[]
// UnityEngine.TextureWrapMode[]
struct TextureWrapModeU5BU5D_t5747  : public Array_t
{
};
// UnityEngine.TextureFormat[]
// UnityEngine.TextureFormat[]
struct TextureFormatU5BU5D_t5748  : public Array_t
{
};
// UnityEngine.RenderTextureFormat[]
// UnityEngine.RenderTextureFormat[]
struct RenderTextureFormatU5BU5D_t5749  : public Array_t
{
};
// UnityEngine.RenderTextureReadWrite[]
// UnityEngine.RenderTextureReadWrite[]
struct RenderTextureReadWriteU5BU5D_t5750  : public Array_t
{
};
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t1109  : public Array_t
{
};
// UnityEngine.SocialPlatforms.UserState[]
// UnityEngine.SocialPlatforms.UserState[]
struct UserStateU5BU5D_t5751  : public Array_t
{
};
// UnityEngine.SocialPlatforms.UserScope[]
// UnityEngine.SocialPlatforms.UserScope[]
struct UserScopeU5BU5D_t5752  : public Array_t
{
};
// UnityEngine.SocialPlatforms.TimeScope[]
// UnityEngine.SocialPlatforms.TimeScope[]
struct TimeScopeU5BU5D_t5753  : public Array_t
{
};
// UnityEngine.PropertyAttribute[]
// UnityEngine.PropertyAttribute[]
struct PropertyAttributeU5BU5D_t5754  : public Array_t
{
};
// UnityEngine.TooltipAttribute[]
// UnityEngine.TooltipAttribute[]
struct TooltipAttributeU5BU5D_t5755  : public Array_t
{
};
// UnityEngine.SpaceAttribute[]
// UnityEngine.SpaceAttribute[]
struct SpaceAttributeU5BU5D_t5756  : public Array_t
{
};
// UnityEngine.RangeAttribute[]
// UnityEngine.RangeAttribute[]
struct RangeAttributeU5BU5D_t5757  : public Array_t
{
};
// UnityEngine.TextAreaAttribute[]
// UnityEngine.TextAreaAttribute[]
struct TextAreaAttributeU5BU5D_t5758  : public Array_t
{
};
// UnityEngine.SelectionBaseAttribute[]
// UnityEngine.SelectionBaseAttribute[]
struct SelectionBaseAttributeU5BU5D_t5759  : public Array_t
{
};
// UnityEngine.SharedBetweenAnimatorsAttribute[]
// UnityEngine.SharedBetweenAnimatorsAttribute[]
struct SharedBetweenAnimatorsAttributeU5BU5D_t5760  : public Array_t
{
};
// UnityEngine.StateMachineBehaviour[]
// UnityEngine.StateMachineBehaviour[]
struct StateMachineBehaviourU5BU5D_t5761  : public Array_t
{
};
// UnityEngine.Event[]
// UnityEngine.Event[]
struct EventU5BU5D_t4960  : public Array_t
{
};
struct EventU5BU5D_t4960_StaticFields{
};
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t4961  : public Array_t
{
};
// UnityEngine.TextEditor/DblClickSnapping[]
// UnityEngine.TextEditor/DblClickSnapping[]
struct DblClickSnappingU5BU5D_t5762  : public Array_t
{
};
// UnityEngine.Events.PersistentListenerMode[]
// UnityEngine.Events.PersistentListenerMode[]
struct PersistentListenerModeU5BU5D_t5763  : public Array_t
{
};
// UnityEngine.Events.UnityEventCallState[]
// UnityEngine.Events.UnityEventCallState[]
struct UnityEventCallStateU5BU5D_t5764  : public Array_t
{
};
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t4996  : public Array_t
{
};
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t5010  : public Array_t
{
};
// UnityEngine.UserAuthorizationDialog[]
// UnityEngine.UserAuthorizationDialog[]
struct UserAuthorizationDialogU5BU5D_t5765  : public Array_t
{
};
// UnityEngine.Internal.DefaultValueAttribute[]
// UnityEngine.Internal.DefaultValueAttribute[]
struct DefaultValueAttributeU5BU5D_t5766  : public Array_t
{
};
// UnityEngine.Internal.ExcludeFromDocsAttribute[]
// UnityEngine.Internal.ExcludeFromDocsAttribute[]
struct ExcludeFromDocsAttributeU5BU5D_t5767  : public Array_t
{
};
// UnityEngine.Serialization.FormerlySerializedAsAttribute[]
// UnityEngine.Serialization.FormerlySerializedAsAttribute[]
struct FormerlySerializedAsAttributeU5BU5D_t5768  : public Array_t
{
};
// UnityEngineInternal.TypeInferenceRules[]
// UnityEngineInternal.TypeInferenceRules[]
struct TypeInferenceRulesU5BU5D_t5769  : public Array_t
{
};
// UnityEngineInternal.TypeInferenceRuleAttribute[]
// UnityEngineInternal.TypeInferenceRuleAttribute[]
struct TypeInferenceRuleAttributeU5BU5D_t5770  : public Array_t
{
};
