﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerator_1_t7254_il2cpp_TypeInfo;

// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttribute.h"

// System.Array
#include "mscorlib_System_Array.h"

// T System.Collections.Generic.IEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>
extern MethodInfo IEnumerator_1_get_Current_m51851_MethodInfo;
static PropertyInfo IEnumerator_1_t7254____Current_PropertyInfo = 
{
	&IEnumerator_1_t7254_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51851_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7254_PropertyInfos[] =
{
	&IEnumerator_1_t7254____Current_PropertyInfo,
	NULL
};
extern Il2CppType DebuggerTypeProxyAttribute_t1899_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51851_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51851_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7254_il2cpp_TypeInfo/* declaring_type */
	, &DebuggerTypeProxyAttribute_t1899_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51851_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7254_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51851_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t7254_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7254_0_0_0;
extern Il2CppType IEnumerator_1_t7254_1_0_0;
struct IEnumerator_1_t7254;
extern Il2CppGenericClass IEnumerator_1_t7254_GenericClass;
TypeInfo IEnumerator_1_t7254_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7254_MethodInfos/* methods */
	, IEnumerator_1_t7254_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7254_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7254_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7254_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7254_0_0_0/* byval_arg */
	, &IEnumerator_1_t7254_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7254_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_642.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5186_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_642MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo DebuggerTypeProxyAttribute_t1899_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m31308_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDebuggerTypeProxyAttribute_t1899_m40782_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.Diagnostics.DebuggerTypeProxyAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Diagnostics.DebuggerTypeProxyAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisDebuggerTypeProxyAttribute_t1899_m40782(__this, p0, method) (DebuggerTypeProxyAttribute_t1899 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5186____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5186_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5186, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5186____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5186_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5186, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5186_FieldInfos[] =
{
	&InternalEnumerator_1_t5186____array_0_FieldInfo,
	&InternalEnumerator_1_t5186____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31305_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5186____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5186_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31305_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5186____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5186_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31308_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5186_PropertyInfos[] =
{
	&InternalEnumerator_1_t5186____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5186____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5186_InternalEnumerator_1__ctor_m31304_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31304_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31304_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5186_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5186_InternalEnumerator_1__ctor_m31304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31304_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31305_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31305_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5186_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31305_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31306_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31306_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5186_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31306_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31307_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31307_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5186_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31307_GenericMethod/* genericMethod */

};
extern Il2CppType DebuggerTypeProxyAttribute_t1899_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31308_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Diagnostics.DebuggerTypeProxyAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31308_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5186_il2cpp_TypeInfo/* declaring_type */
	, &DebuggerTypeProxyAttribute_t1899_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31308_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5186_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31304_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31305_MethodInfo,
	&InternalEnumerator_1_Dispose_m31306_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31307_MethodInfo,
	&InternalEnumerator_1_get_Current_m31308_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m31307_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31306_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5186_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31305_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31307_MethodInfo,
	&InternalEnumerator_1_Dispose_m31306_MethodInfo,
	&InternalEnumerator_1_get_Current_m31308_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5186_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7254_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5186_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7254_il2cpp_TypeInfo, 7},
};
extern TypeInfo DebuggerTypeProxyAttribute_t1899_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5186_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31308_MethodInfo/* Method Usage */,
	&DebuggerTypeProxyAttribute_t1899_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDebuggerTypeProxyAttribute_t1899_m40782_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5186_0_0_0;
extern Il2CppType InternalEnumerator_1_t5186_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t5186_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t5186_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5186_MethodInfos/* methods */
	, InternalEnumerator_1_t5186_PropertyInfos/* properties */
	, InternalEnumerator_1_t5186_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5186_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5186_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5186_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5186_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5186_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5186_1_0_0/* this_arg */
	, InternalEnumerator_1_t5186_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5186_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5186_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5186)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9288_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>
extern MethodInfo ICollection_1_get_Count_m51852_MethodInfo;
static PropertyInfo ICollection_1_t9288____Count_PropertyInfo = 
{
	&ICollection_1_t9288_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51852_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51853_MethodInfo;
static PropertyInfo ICollection_1_t9288____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9288_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51853_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9288_PropertyInfos[] =
{
	&ICollection_1_t9288____Count_PropertyInfo,
	&ICollection_1_t9288____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51852_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m51852_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9288_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51852_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51853_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51853_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9288_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51853_GenericMethod/* genericMethod */

};
extern Il2CppType DebuggerTypeProxyAttribute_t1899_0_0_0;
extern Il2CppType DebuggerTypeProxyAttribute_t1899_0_0_0;
static ParameterInfo ICollection_1_t9288_ICollection_1_Add_m51854_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DebuggerTypeProxyAttribute_t1899_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51854_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Add(T)
MethodInfo ICollection_1_Add_m51854_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9288_ICollection_1_Add_m51854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51854_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51855_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Clear()
MethodInfo ICollection_1_Clear_m51855_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51855_GenericMethod/* genericMethod */

};
extern Il2CppType DebuggerTypeProxyAttribute_t1899_0_0_0;
static ParameterInfo ICollection_1_t9288_ICollection_1_Contains_m51856_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DebuggerTypeProxyAttribute_t1899_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51856_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m51856_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9288_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9288_ICollection_1_Contains_m51856_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51856_GenericMethod/* genericMethod */

};
extern Il2CppType DebuggerTypeProxyAttributeU5BU5D_t5497_0_0_0;
extern Il2CppType DebuggerTypeProxyAttributeU5BU5D_t5497_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9288_ICollection_1_CopyTo_m51857_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DebuggerTypeProxyAttributeU5BU5D_t5497_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51857_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51857_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9288_ICollection_1_CopyTo_m51857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51857_GenericMethod/* genericMethod */

};
extern Il2CppType DebuggerTypeProxyAttribute_t1899_0_0_0;
static ParameterInfo ICollection_1_t9288_ICollection_1_Remove_m51858_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DebuggerTypeProxyAttribute_t1899_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51858_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m51858_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9288_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9288_ICollection_1_Remove_m51858_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51858_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9288_MethodInfos[] =
{
	&ICollection_1_get_Count_m51852_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51853_MethodInfo,
	&ICollection_1_Add_m51854_MethodInfo,
	&ICollection_1_Clear_m51855_MethodInfo,
	&ICollection_1_Contains_m51856_MethodInfo,
	&ICollection_1_CopyTo_m51857_MethodInfo,
	&ICollection_1_Remove_m51858_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t9290_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9288_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9290_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9288_0_0_0;
extern Il2CppType ICollection_1_t9288_1_0_0;
struct ICollection_1_t9288;
extern Il2CppGenericClass ICollection_1_t9288_GenericClass;
TypeInfo ICollection_1_t9288_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9288_MethodInfos/* methods */
	, ICollection_1_t9288_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9288_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9288_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9288_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9288_0_0_0/* byval_arg */
	, &ICollection_1_t9288_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9288_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Diagnostics.DebuggerTypeProxyAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Diagnostics.DebuggerTypeProxyAttribute>
extern Il2CppType IEnumerator_1_t7254_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51859_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Diagnostics.DebuggerTypeProxyAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51859_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9290_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7254_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51859_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9290_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51859_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9290_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9290_0_0_0;
extern Il2CppType IEnumerable_1_t9290_1_0_0;
struct IEnumerable_1_t9290;
extern Il2CppGenericClass IEnumerable_1_t9290_GenericClass;
TypeInfo IEnumerable_1_t9290_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9290_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9290_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9290_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9290_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9290_0_0_0/* byval_arg */
	, &IEnumerable_1_t9290_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9290_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9289_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>
extern MethodInfo IList_1_get_Item_m51860_MethodInfo;
extern MethodInfo IList_1_set_Item_m51861_MethodInfo;
static PropertyInfo IList_1_t9289____Item_PropertyInfo = 
{
	&IList_1_t9289_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51860_MethodInfo/* get */
	, &IList_1_set_Item_m51861_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9289_PropertyInfos[] =
{
	&IList_1_t9289____Item_PropertyInfo,
	NULL
};
extern Il2CppType DebuggerTypeProxyAttribute_t1899_0_0_0;
static ParameterInfo IList_1_t9289_IList_1_IndexOf_m51862_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DebuggerTypeProxyAttribute_t1899_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51862_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51862_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9289_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9289_IList_1_IndexOf_m51862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51862_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DebuggerTypeProxyAttribute_t1899_0_0_0;
static ParameterInfo IList_1_t9289_IList_1_Insert_m51863_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DebuggerTypeProxyAttribute_t1899_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51863_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51863_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9289_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9289_IList_1_Insert_m51863_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51863_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9289_IList_1_RemoveAt_m51864_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51864_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51864_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9289_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9289_IList_1_RemoveAt_m51864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51864_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9289_IList_1_get_Item_m51860_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DebuggerTypeProxyAttribute_t1899_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51860_GenericMethod;
// T System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51860_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9289_il2cpp_TypeInfo/* declaring_type */
	, &DebuggerTypeProxyAttribute_t1899_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9289_IList_1_get_Item_m51860_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51860_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DebuggerTypeProxyAttribute_t1899_0_0_0;
static ParameterInfo IList_1_t9289_IList_1_set_Item_m51861_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DebuggerTypeProxyAttribute_t1899_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51861_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.DebuggerTypeProxyAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51861_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9289_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9289_IList_1_set_Item_m51861_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51861_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9289_MethodInfos[] =
{
	&IList_1_IndexOf_m51862_MethodInfo,
	&IList_1_Insert_m51863_MethodInfo,
	&IList_1_RemoveAt_m51864_MethodInfo,
	&IList_1_get_Item_m51860_MethodInfo,
	&IList_1_set_Item_m51861_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9289_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9288_il2cpp_TypeInfo,
	&IEnumerable_1_t9290_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9289_0_0_0;
extern Il2CppType IList_1_t9289_1_0_0;
struct IList_1_t9289;
extern Il2CppGenericClass IList_1_t9289_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9289_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9289_MethodInfos/* methods */
	, IList_1_t9289_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9289_il2cpp_TypeInfo/* element_class */
	, IList_1_t9289_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9289_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9289_0_0_0/* byval_arg */
	, &IList_1_t9289_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9289_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7256_il2cpp_TypeInfo;

// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrame.h"


// T System.Collections.Generic.IEnumerator`1<System.Diagnostics.StackFrame>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Diagnostics.StackFrame>
extern MethodInfo IEnumerator_1_get_Current_m51865_MethodInfo;
static PropertyInfo IEnumerator_1_t7256____Current_PropertyInfo = 
{
	&IEnumerator_1_t7256_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51865_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7256_PropertyInfos[] =
{
	&IEnumerator_1_t7256____Current_PropertyInfo,
	NULL
};
extern Il2CppType StackFrame_t1219_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51865_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Diagnostics.StackFrame>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51865_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7256_il2cpp_TypeInfo/* declaring_type */
	, &StackFrame_t1219_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51865_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7256_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51865_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7256_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7256_0_0_0;
extern Il2CppType IEnumerator_1_t7256_1_0_0;
struct IEnumerator_1_t7256;
extern Il2CppGenericClass IEnumerator_1_t7256_GenericClass;
TypeInfo IEnumerator_1_t7256_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7256_MethodInfos/* methods */
	, IEnumerator_1_t7256_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7256_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7256_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7256_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7256_0_0_0/* byval_arg */
	, &IEnumerator_1_t7256_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7256_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_643.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5187_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_643MethodDeclarations.h"

extern TypeInfo StackFrame_t1219_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31313_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisStackFrame_t1219_m40793_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Diagnostics.StackFrame>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Diagnostics.StackFrame>(System.Int32)
#define Array_InternalArray__get_Item_TisStackFrame_t1219_m40793(__this, p0, method) (StackFrame_t1219 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5187____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5187_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5187, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5187____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5187_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5187, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5187_FieldInfos[] =
{
	&InternalEnumerator_1_t5187____array_0_FieldInfo,
	&InternalEnumerator_1_t5187____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31310_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5187____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5187_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31310_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5187____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5187_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31313_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5187_PropertyInfos[] =
{
	&InternalEnumerator_1_t5187____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5187____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5187_InternalEnumerator_1__ctor_m31309_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31309_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31309_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5187_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5187_InternalEnumerator_1__ctor_m31309_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31309_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31310_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31310_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5187_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31310_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31311_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31311_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5187_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31311_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31312_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31312_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5187_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31312_GenericMethod/* genericMethod */

};
extern Il2CppType StackFrame_t1219_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31313_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Diagnostics.StackFrame>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31313_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5187_il2cpp_TypeInfo/* declaring_type */
	, &StackFrame_t1219_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31313_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5187_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31309_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31310_MethodInfo,
	&InternalEnumerator_1_Dispose_m31311_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31312_MethodInfo,
	&InternalEnumerator_1_get_Current_m31313_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31312_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31311_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5187_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31310_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31312_MethodInfo,
	&InternalEnumerator_1_Dispose_m31311_MethodInfo,
	&InternalEnumerator_1_get_Current_m31313_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5187_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7256_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5187_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7256_il2cpp_TypeInfo, 7},
};
extern TypeInfo StackFrame_t1219_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5187_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31313_MethodInfo/* Method Usage */,
	&StackFrame_t1219_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisStackFrame_t1219_m40793_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5187_0_0_0;
extern Il2CppType InternalEnumerator_1_t5187_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5187_GenericClass;
TypeInfo InternalEnumerator_1_t5187_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5187_MethodInfos/* methods */
	, InternalEnumerator_1_t5187_PropertyInfos/* properties */
	, InternalEnumerator_1_t5187_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5187_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5187_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5187_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5187_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5187_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5187_1_0_0/* this_arg */
	, InternalEnumerator_1_t5187_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5187_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5187_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5187)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9291_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>
extern MethodInfo ICollection_1_get_Count_m51866_MethodInfo;
static PropertyInfo ICollection_1_t9291____Count_PropertyInfo = 
{
	&ICollection_1_t9291_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51866_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51867_MethodInfo;
static PropertyInfo ICollection_1_t9291____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9291_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51867_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9291_PropertyInfos[] =
{
	&ICollection_1_t9291____Count_PropertyInfo,
	&ICollection_1_t9291____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51866_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::get_Count()
MethodInfo ICollection_1_get_Count_m51866_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9291_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51866_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51867_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51867_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9291_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51867_GenericMethod/* genericMethod */

};
extern Il2CppType StackFrame_t1219_0_0_0;
extern Il2CppType StackFrame_t1219_0_0_0;
static ParameterInfo ICollection_1_t9291_ICollection_1_Add_m51868_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StackFrame_t1219_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51868_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::Add(T)
MethodInfo ICollection_1_Add_m51868_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9291_ICollection_1_Add_m51868_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51868_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51869_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::Clear()
MethodInfo ICollection_1_Clear_m51869_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51869_GenericMethod/* genericMethod */

};
extern Il2CppType StackFrame_t1219_0_0_0;
static ParameterInfo ICollection_1_t9291_ICollection_1_Contains_m51870_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StackFrame_t1219_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51870_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::Contains(T)
MethodInfo ICollection_1_Contains_m51870_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9291_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9291_ICollection_1_Contains_m51870_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51870_GenericMethod/* genericMethod */

};
extern Il2CppType StackFrameU5BU5D_t1900_0_0_0;
extern Il2CppType StackFrameU5BU5D_t1900_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9291_ICollection_1_CopyTo_m51871_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &StackFrameU5BU5D_t1900_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51871_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51871_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9291_ICollection_1_CopyTo_m51871_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51871_GenericMethod/* genericMethod */

};
extern Il2CppType StackFrame_t1219_0_0_0;
static ParameterInfo ICollection_1_t9291_ICollection_1_Remove_m51872_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StackFrame_t1219_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51872_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Diagnostics.StackFrame>::Remove(T)
MethodInfo ICollection_1_Remove_m51872_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9291_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9291_ICollection_1_Remove_m51872_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51872_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9291_MethodInfos[] =
{
	&ICollection_1_get_Count_m51866_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51867_MethodInfo,
	&ICollection_1_Add_m51868_MethodInfo,
	&ICollection_1_Clear_m51869_MethodInfo,
	&ICollection_1_Contains_m51870_MethodInfo,
	&ICollection_1_CopyTo_m51871_MethodInfo,
	&ICollection_1_Remove_m51872_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9293_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9291_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9293_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9291_0_0_0;
extern Il2CppType ICollection_1_t9291_1_0_0;
struct ICollection_1_t9291;
extern Il2CppGenericClass ICollection_1_t9291_GenericClass;
TypeInfo ICollection_1_t9291_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9291_MethodInfos/* methods */
	, ICollection_1_t9291_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9291_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9291_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9291_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9291_0_0_0/* byval_arg */
	, &ICollection_1_t9291_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9291_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Diagnostics.StackFrame>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Diagnostics.StackFrame>
extern Il2CppType IEnumerator_1_t7256_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51873_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Diagnostics.StackFrame>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51873_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9293_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7256_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51873_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9293_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51873_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9293_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9293_0_0_0;
extern Il2CppType IEnumerable_1_t9293_1_0_0;
struct IEnumerable_1_t9293;
extern Il2CppGenericClass IEnumerable_1_t9293_GenericClass;
TypeInfo IEnumerable_1_t9293_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9293_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9293_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9293_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9293_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9293_0_0_0/* byval_arg */
	, &IEnumerable_1_t9293_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9293_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9292_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>
extern MethodInfo IList_1_get_Item_m51874_MethodInfo;
extern MethodInfo IList_1_set_Item_m51875_MethodInfo;
static PropertyInfo IList_1_t9292____Item_PropertyInfo = 
{
	&IList_1_t9292_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51874_MethodInfo/* get */
	, &IList_1_set_Item_m51875_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9292_PropertyInfos[] =
{
	&IList_1_t9292____Item_PropertyInfo,
	NULL
};
extern Il2CppType StackFrame_t1219_0_0_0;
static ParameterInfo IList_1_t9292_IList_1_IndexOf_m51876_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StackFrame_t1219_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51876_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51876_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9292_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9292_IList_1_IndexOf_m51876_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51876_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StackFrame_t1219_0_0_0;
static ParameterInfo IList_1_t9292_IList_1_Insert_m51877_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &StackFrame_t1219_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51877_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51877_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9292_IList_1_Insert_m51877_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51877_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9292_IList_1_RemoveAt_m51878_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51878_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51878_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9292_IList_1_RemoveAt_m51878_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51878_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9292_IList_1_get_Item_m51874_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType StackFrame_t1219_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51874_GenericMethod;
// T System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51874_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9292_il2cpp_TypeInfo/* declaring_type */
	, &StackFrame_t1219_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9292_IList_1_get_Item_m51874_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51874_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType StackFrame_t1219_0_0_0;
static ParameterInfo IList_1_t9292_IList_1_set_Item_m51875_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &StackFrame_t1219_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51875_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51875_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9292_IList_1_set_Item_m51875_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51875_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9292_MethodInfos[] =
{
	&IList_1_IndexOf_m51876_MethodInfo,
	&IList_1_Insert_m51877_MethodInfo,
	&IList_1_RemoveAt_m51878_MethodInfo,
	&IList_1_get_Item_m51874_MethodInfo,
	&IList_1_set_Item_m51875_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9292_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9291_il2cpp_TypeInfo,
	&IEnumerable_1_t9293_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9292_0_0_0;
extern Il2CppType IList_1_t9292_1_0_0;
struct IList_1_t9292;
extern Il2CppGenericClass IList_1_t9292_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9292_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9292_MethodInfos/* methods */
	, IList_1_t9292_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9292_il2cpp_TypeInfo/* element_class */
	, IList_1_t9292_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9292_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9292_0_0_0/* byval_arg */
	, &IList_1_t9292_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9292_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7258_il2cpp_TypeInfo;

// System.Globalization.CompareOptions
#include "mscorlib_System_Globalization_CompareOptions.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.CompareOptions>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.CompareOptions>
extern MethodInfo IEnumerator_1_get_Current_m51879_MethodInfo;
static PropertyInfo IEnumerator_1_t7258____Current_PropertyInfo = 
{
	&IEnumerator_1_t7258_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51879_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7258_PropertyInfos[] =
{
	&IEnumerator_1_t7258____Current_PropertyInfo,
	NULL
};
extern Il2CppType CompareOptions_t1734_0_0_0;
extern void* RuntimeInvoker_CompareOptions_t1734 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51879_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.CompareOptions>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51879_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7258_il2cpp_TypeInfo/* declaring_type */
	, &CompareOptions_t1734_0_0_0/* return_type */
	, RuntimeInvoker_CompareOptions_t1734/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51879_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7258_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51879_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7258_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7258_0_0_0;
extern Il2CppType IEnumerator_1_t7258_1_0_0;
struct IEnumerator_1_t7258;
extern Il2CppGenericClass IEnumerator_1_t7258_GenericClass;
TypeInfo IEnumerator_1_t7258_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7258_MethodInfos/* methods */
	, IEnumerator_1_t7258_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7258_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7258_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7258_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7258_0_0_0/* byval_arg */
	, &IEnumerator_1_t7258_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7258_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_644.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5188_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_644MethodDeclarations.h"

extern TypeInfo CompareOptions_t1734_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31318_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCompareOptions_t1734_m40804_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.CompareOptions>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.CompareOptions>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCompareOptions_t1734_m40804 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31314_MethodInfo;
 void InternalEnumerator_1__ctor_m31314 (InternalEnumerator_1_t5188 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31315_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31315 (InternalEnumerator_1_t5188 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31318(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31318_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CompareOptions_t1734_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31316_MethodInfo;
 void InternalEnumerator_1_Dispose_m31316 (InternalEnumerator_1_t5188 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31317_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31317 (InternalEnumerator_1_t5188 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31318 (InternalEnumerator_1_t5188 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCompareOptions_t1734_m40804(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCompareOptions_t1734_m40804_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5188____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5188_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5188, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5188____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5188_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5188, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5188_FieldInfos[] =
{
	&InternalEnumerator_1_t5188____array_0_FieldInfo,
	&InternalEnumerator_1_t5188____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5188____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5188_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31315_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5188____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5188_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31318_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5188_PropertyInfos[] =
{
	&InternalEnumerator_1_t5188____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5188____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5188_InternalEnumerator_1__ctor_m31314_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31314_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31314_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31314/* method */
	, &InternalEnumerator_1_t5188_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5188_InternalEnumerator_1__ctor_m31314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31314_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31315_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31315_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31315/* method */
	, &InternalEnumerator_1_t5188_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31315_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31316_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31316_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31316/* method */
	, &InternalEnumerator_1_t5188_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31316_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31317_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31317_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31317/* method */
	, &InternalEnumerator_1_t5188_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31317_GenericMethod/* genericMethod */

};
extern Il2CppType CompareOptions_t1734_0_0_0;
extern void* RuntimeInvoker_CompareOptions_t1734 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31318_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31318_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31318/* method */
	, &InternalEnumerator_1_t5188_il2cpp_TypeInfo/* declaring_type */
	, &CompareOptions_t1734_0_0_0/* return_type */
	, RuntimeInvoker_CompareOptions_t1734/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31318_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5188_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31314_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31315_MethodInfo,
	&InternalEnumerator_1_Dispose_m31316_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31317_MethodInfo,
	&InternalEnumerator_1_get_Current_m31318_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5188_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31315_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31317_MethodInfo,
	&InternalEnumerator_1_Dispose_m31316_MethodInfo,
	&InternalEnumerator_1_get_Current_m31318_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5188_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7258_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5188_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7258_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5188_0_0_0;
extern Il2CppType InternalEnumerator_1_t5188_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5188_GenericClass;
TypeInfo InternalEnumerator_1_t5188_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5188_MethodInfos/* methods */
	, InternalEnumerator_1_t5188_PropertyInfos/* properties */
	, InternalEnumerator_1_t5188_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5188_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5188_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5188_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5188_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5188_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5188_1_0_0/* this_arg */
	, InternalEnumerator_1_t5188_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5188_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5188)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9294_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>
extern MethodInfo ICollection_1_get_Count_m51880_MethodInfo;
static PropertyInfo ICollection_1_t9294____Count_PropertyInfo = 
{
	&ICollection_1_t9294_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51880_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51881_MethodInfo;
static PropertyInfo ICollection_1_t9294____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9294_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51881_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9294_PropertyInfos[] =
{
	&ICollection_1_t9294____Count_PropertyInfo,
	&ICollection_1_t9294____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51880_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::get_Count()
MethodInfo ICollection_1_get_Count_m51880_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9294_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51880_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51881_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51881_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9294_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51881_GenericMethod/* genericMethod */

};
extern Il2CppType CompareOptions_t1734_0_0_0;
extern Il2CppType CompareOptions_t1734_0_0_0;
static ParameterInfo ICollection_1_t9294_ICollection_1_Add_m51882_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1734_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51882_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Add(T)
MethodInfo ICollection_1_Add_m51882_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9294_ICollection_1_Add_m51882_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51882_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51883_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Clear()
MethodInfo ICollection_1_Clear_m51883_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51883_GenericMethod/* genericMethod */

};
extern Il2CppType CompareOptions_t1734_0_0_0;
static ParameterInfo ICollection_1_t9294_ICollection_1_Contains_m51884_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1734_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51884_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Contains(T)
MethodInfo ICollection_1_Contains_m51884_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9294_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9294_ICollection_1_Contains_m51884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51884_GenericMethod/* genericMethod */

};
extern Il2CppType CompareOptionsU5BU5D_t5498_0_0_0;
extern Il2CppType CompareOptionsU5BU5D_t5498_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9294_ICollection_1_CopyTo_m51885_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CompareOptionsU5BU5D_t5498_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51885_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51885_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9294_ICollection_1_CopyTo_m51885_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51885_GenericMethod/* genericMethod */

};
extern Il2CppType CompareOptions_t1734_0_0_0;
static ParameterInfo ICollection_1_t9294_ICollection_1_Remove_m51886_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1734_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51886_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Remove(T)
MethodInfo ICollection_1_Remove_m51886_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9294_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9294_ICollection_1_Remove_m51886_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51886_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9294_MethodInfos[] =
{
	&ICollection_1_get_Count_m51880_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51881_MethodInfo,
	&ICollection_1_Add_m51882_MethodInfo,
	&ICollection_1_Clear_m51883_MethodInfo,
	&ICollection_1_Contains_m51884_MethodInfo,
	&ICollection_1_CopyTo_m51885_MethodInfo,
	&ICollection_1_Remove_m51886_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9296_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9294_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9296_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9294_0_0_0;
extern Il2CppType ICollection_1_t9294_1_0_0;
struct ICollection_1_t9294;
extern Il2CppGenericClass ICollection_1_t9294_GenericClass;
TypeInfo ICollection_1_t9294_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9294_MethodInfos/* methods */
	, ICollection_1_t9294_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9294_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9294_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9294_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9294_0_0_0/* byval_arg */
	, &ICollection_1_t9294_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9294_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.CompareOptions>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.CompareOptions>
extern Il2CppType IEnumerator_1_t7258_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51887_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.CompareOptions>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51887_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9296_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7258_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51887_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9296_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51887_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9296_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9296_0_0_0;
extern Il2CppType IEnumerable_1_t9296_1_0_0;
struct IEnumerable_1_t9296;
extern Il2CppGenericClass IEnumerable_1_t9296_GenericClass;
TypeInfo IEnumerable_1_t9296_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9296_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9296_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9296_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9296_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9296_0_0_0/* byval_arg */
	, &IEnumerable_1_t9296_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9296_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9295_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.CompareOptions>
extern MethodInfo IList_1_get_Item_m51888_MethodInfo;
extern MethodInfo IList_1_set_Item_m51889_MethodInfo;
static PropertyInfo IList_1_t9295____Item_PropertyInfo = 
{
	&IList_1_t9295_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51888_MethodInfo/* get */
	, &IList_1_set_Item_m51889_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9295_PropertyInfos[] =
{
	&IList_1_t9295____Item_PropertyInfo,
	NULL
};
extern Il2CppType CompareOptions_t1734_0_0_0;
static ParameterInfo IList_1_t9295_IList_1_IndexOf_m51890_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1734_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51890_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51890_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9295_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9295_IList_1_IndexOf_m51890_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51890_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CompareOptions_t1734_0_0_0;
static ParameterInfo IList_1_t9295_IList_1_Insert_m51891_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1734_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51891_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51891_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9295_IList_1_Insert_m51891_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51891_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9295_IList_1_RemoveAt_m51892_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51892_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51892_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9295_IList_1_RemoveAt_m51892_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51892_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9295_IList_1_get_Item_m51888_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CompareOptions_t1734_0_0_0;
extern void* RuntimeInvoker_CompareOptions_t1734_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51888_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51888_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9295_il2cpp_TypeInfo/* declaring_type */
	, &CompareOptions_t1734_0_0_0/* return_type */
	, RuntimeInvoker_CompareOptions_t1734_Int32_t123/* invoker_method */
	, IList_1_t9295_IList_1_get_Item_m51888_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51888_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CompareOptions_t1734_0_0_0;
static ParameterInfo IList_1_t9295_IList_1_set_Item_m51889_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1734_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51889_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51889_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9295_IList_1_set_Item_m51889_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51889_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9295_MethodInfos[] =
{
	&IList_1_IndexOf_m51890_MethodInfo,
	&IList_1_Insert_m51891_MethodInfo,
	&IList_1_RemoveAt_m51892_MethodInfo,
	&IList_1_get_Item_m51888_MethodInfo,
	&IList_1_set_Item_m51889_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9295_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9294_il2cpp_TypeInfo,
	&IEnumerable_1_t9296_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9295_0_0_0;
extern Il2CppType IList_1_t9295_1_0_0;
struct IList_1_t9295;
extern Il2CppGenericClass IList_1_t9295_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9295_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9295_MethodInfos/* methods */
	, IList_1_t9295_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9295_il2cpp_TypeInfo/* element_class */
	, IList_1_t9295_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9295_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9295_0_0_0/* byval_arg */
	, &IList_1_t9295_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9295_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7260_il2cpp_TypeInfo;

// System.Globalization.Calendar
#include "mscorlib_System_Globalization_Calendar.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.Calendar>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.Calendar>
extern MethodInfo IEnumerator_1_get_Current_m51893_MethodInfo;
static PropertyInfo IEnumerator_1_t7260____Current_PropertyInfo = 
{
	&IEnumerator_1_t7260_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51893_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7260_PropertyInfos[] =
{
	&IEnumerator_1_t7260____Current_PropertyInfo,
	NULL
};
extern Il2CppType Calendar_t1901_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51893_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.Calendar>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51893_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7260_il2cpp_TypeInfo/* declaring_type */
	, &Calendar_t1901_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51893_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7260_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51893_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7260_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7260_0_0_0;
extern Il2CppType IEnumerator_1_t7260_1_0_0;
struct IEnumerator_1_t7260;
extern Il2CppGenericClass IEnumerator_1_t7260_GenericClass;
TypeInfo IEnumerator_1_t7260_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7260_MethodInfos/* methods */
	, IEnumerator_1_t7260_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7260_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7260_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7260_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7260_0_0_0/* byval_arg */
	, &IEnumerator_1_t7260_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7260_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.Calendar>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_645.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5189_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.Calendar>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_645MethodDeclarations.h"

extern TypeInfo Calendar_t1901_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31323_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCalendar_t1901_m40815_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.Calendar>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.Calendar>(System.Int32)
#define Array_InternalArray__get_Item_TisCalendar_t1901_m40815(__this, p0, method) (Calendar_t1901 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Globalization.Calendar>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.Calendar>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Globalization.Calendar>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.Calendar>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5189____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5189_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5189, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5189____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5189_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5189, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5189_FieldInfos[] =
{
	&InternalEnumerator_1_t5189____array_0_FieldInfo,
	&InternalEnumerator_1_t5189____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31320_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5189____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5189_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31320_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5189____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5189_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31323_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5189_PropertyInfos[] =
{
	&InternalEnumerator_1_t5189____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5189____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5189_InternalEnumerator_1__ctor_m31319_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31319_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31319_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5189_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5189_InternalEnumerator_1__ctor_m31319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31319_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31320_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.Calendar>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31320_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5189_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31320_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31321_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31321_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5189_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31321_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31322_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.Calendar>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31322_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5189_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31322_GenericMethod/* genericMethod */

};
extern Il2CppType Calendar_t1901_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31323_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.Calendar>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31323_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5189_il2cpp_TypeInfo/* declaring_type */
	, &Calendar_t1901_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31323_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5189_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31319_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31320_MethodInfo,
	&InternalEnumerator_1_Dispose_m31321_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31322_MethodInfo,
	&InternalEnumerator_1_get_Current_m31323_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31322_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31321_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5189_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31320_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31322_MethodInfo,
	&InternalEnumerator_1_Dispose_m31321_MethodInfo,
	&InternalEnumerator_1_get_Current_m31323_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5189_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7260_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5189_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7260_il2cpp_TypeInfo, 7},
};
extern TypeInfo Calendar_t1901_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5189_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31323_MethodInfo/* Method Usage */,
	&Calendar_t1901_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCalendar_t1901_m40815_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5189_0_0_0;
extern Il2CppType InternalEnumerator_1_t5189_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5189_GenericClass;
TypeInfo InternalEnumerator_1_t5189_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5189_MethodInfos/* methods */
	, InternalEnumerator_1_t5189_PropertyInfos/* properties */
	, InternalEnumerator_1_t5189_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5189_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5189_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5189_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5189_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5189_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5189_1_0_0/* this_arg */
	, InternalEnumerator_1_t5189_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5189_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5189_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5189)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9297_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.Calendar>
extern MethodInfo ICollection_1_get_Count_m51894_MethodInfo;
static PropertyInfo ICollection_1_t9297____Count_PropertyInfo = 
{
	&ICollection_1_t9297_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51894_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51895_MethodInfo;
static PropertyInfo ICollection_1_t9297____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9297_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51895_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9297_PropertyInfos[] =
{
	&ICollection_1_t9297____Count_PropertyInfo,
	&ICollection_1_t9297____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51894_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::get_Count()
MethodInfo ICollection_1_get_Count_m51894_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9297_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51894_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51895_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51895_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9297_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51895_GenericMethod/* genericMethod */

};
extern Il2CppType Calendar_t1901_0_0_0;
extern Il2CppType Calendar_t1901_0_0_0;
static ParameterInfo ICollection_1_t9297_ICollection_1_Add_m51896_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Calendar_t1901_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51896_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Add(T)
MethodInfo ICollection_1_Add_m51896_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9297_ICollection_1_Add_m51896_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51896_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51897_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Clear()
MethodInfo ICollection_1_Clear_m51897_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51897_GenericMethod/* genericMethod */

};
extern Il2CppType Calendar_t1901_0_0_0;
static ParameterInfo ICollection_1_t9297_ICollection_1_Contains_m51898_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Calendar_t1901_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51898_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Contains(T)
MethodInfo ICollection_1_Contains_m51898_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9297_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9297_ICollection_1_Contains_m51898_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51898_GenericMethod/* genericMethod */

};
extern Il2CppType CalendarU5BU5D_t1906_0_0_0;
extern Il2CppType CalendarU5BU5D_t1906_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9297_ICollection_1_CopyTo_m51899_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CalendarU5BU5D_t1906_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51899_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51899_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9297_ICollection_1_CopyTo_m51899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51899_GenericMethod/* genericMethod */

};
extern Il2CppType Calendar_t1901_0_0_0;
static ParameterInfo ICollection_1_t9297_ICollection_1_Remove_m51900_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Calendar_t1901_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51900_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Remove(T)
MethodInfo ICollection_1_Remove_m51900_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9297_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9297_ICollection_1_Remove_m51900_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51900_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9297_MethodInfos[] =
{
	&ICollection_1_get_Count_m51894_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51895_MethodInfo,
	&ICollection_1_Add_m51896_MethodInfo,
	&ICollection_1_Clear_m51897_MethodInfo,
	&ICollection_1_Contains_m51898_MethodInfo,
	&ICollection_1_CopyTo_m51899_MethodInfo,
	&ICollection_1_Remove_m51900_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9299_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9297_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9299_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9297_0_0_0;
extern Il2CppType ICollection_1_t9297_1_0_0;
struct ICollection_1_t9297;
extern Il2CppGenericClass ICollection_1_t9297_GenericClass;
TypeInfo ICollection_1_t9297_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9297_MethodInfos/* methods */
	, ICollection_1_t9297_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9297_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9297_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9297_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9297_0_0_0/* byval_arg */
	, &ICollection_1_t9297_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9297_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.Calendar>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.Calendar>
extern Il2CppType IEnumerator_1_t7260_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51901_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.Calendar>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51901_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9299_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7260_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51901_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9299_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51901_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9299_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9299_0_0_0;
extern Il2CppType IEnumerable_1_t9299_1_0_0;
struct IEnumerable_1_t9299;
extern Il2CppGenericClass IEnumerable_1_t9299_GenericClass;
TypeInfo IEnumerable_1_t9299_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9299_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9299_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9299_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9299_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9299_0_0_0/* byval_arg */
	, &IEnumerable_1_t9299_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9299_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9298_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.Calendar>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.Calendar>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.Calendar>
extern MethodInfo IList_1_get_Item_m51902_MethodInfo;
extern MethodInfo IList_1_set_Item_m51903_MethodInfo;
static PropertyInfo IList_1_t9298____Item_PropertyInfo = 
{
	&IList_1_t9298_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51902_MethodInfo/* get */
	, &IList_1_set_Item_m51903_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9298_PropertyInfos[] =
{
	&IList_1_t9298____Item_PropertyInfo,
	NULL
};
extern Il2CppType Calendar_t1901_0_0_0;
static ParameterInfo IList_1_t9298_IList_1_IndexOf_m51904_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Calendar_t1901_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51904_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.Calendar>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51904_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9298_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9298_IList_1_IndexOf_m51904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51904_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Calendar_t1901_0_0_0;
static ParameterInfo IList_1_t9298_IList_1_Insert_m51905_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Calendar_t1901_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51905_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51905_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9298_IList_1_Insert_m51905_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51905_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9298_IList_1_RemoveAt_m51906_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51906_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51906_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9298_IList_1_RemoveAt_m51906_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51906_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9298_IList_1_get_Item_m51902_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Calendar_t1901_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51902_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.Calendar>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51902_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9298_il2cpp_TypeInfo/* declaring_type */
	, &Calendar_t1901_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9298_IList_1_get_Item_m51902_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51902_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Calendar_t1901_0_0_0;
static ParameterInfo IList_1_t9298_IList_1_set_Item_m51903_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Calendar_t1901_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51903_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51903_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9298_IList_1_set_Item_m51903_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51903_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9298_MethodInfos[] =
{
	&IList_1_IndexOf_m51904_MethodInfo,
	&IList_1_Insert_m51905_MethodInfo,
	&IList_1_RemoveAt_m51906_MethodInfo,
	&IList_1_get_Item_m51902_MethodInfo,
	&IList_1_set_Item_m51903_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9298_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9297_il2cpp_TypeInfo,
	&IEnumerable_1_t9299_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9298_0_0_0;
extern Il2CppType IList_1_t9298_1_0_0;
struct IList_1_t9298;
extern Il2CppGenericClass IList_1_t9298_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9298_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9298_MethodInfos/* methods */
	, IList_1_t9298_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9298_il2cpp_TypeInfo/* element_class */
	, IList_1_t9298_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9298_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9298_0_0_0/* byval_arg */
	, &IList_1_t9298_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9298_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7262_il2cpp_TypeInfo;

// System.Globalization.DateTimeFormatFlags
#include "mscorlib_System_Globalization_DateTimeFormatFlags.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeFormatFlags>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeFormatFlags>
extern MethodInfo IEnumerator_1_get_Current_m51907_MethodInfo;
static PropertyInfo IEnumerator_1_t7262____Current_PropertyInfo = 
{
	&IEnumerator_1_t7262_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51907_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7262_PropertyInfos[] =
{
	&IEnumerator_1_t7262____Current_PropertyInfo,
	NULL
};
extern Il2CppType DateTimeFormatFlags_t1907_0_0_0;
extern void* RuntimeInvoker_DateTimeFormatFlags_t1907 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51907_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeFormatFlags>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51907_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7262_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeFormatFlags_t1907_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeFormatFlags_t1907/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51907_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7262_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51907_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7262_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7262_0_0_0;
extern Il2CppType IEnumerator_1_t7262_1_0_0;
struct IEnumerator_1_t7262;
extern Il2CppGenericClass IEnumerator_1_t7262_GenericClass;
TypeInfo IEnumerator_1_t7262_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7262_MethodInfos/* methods */
	, IEnumerator_1_t7262_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7262_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7262_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7262_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7262_0_0_0/* byval_arg */
	, &IEnumerator_1_t7262_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7262_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_646.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5190_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_646MethodDeclarations.h"

extern TypeInfo DateTimeFormatFlags_t1907_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31328_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDateTimeFormatFlags_t1907_m40826_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.DateTimeFormatFlags>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.DateTimeFormatFlags>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisDateTimeFormatFlags_t1907_m40826 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31324_MethodInfo;
 void InternalEnumerator_1__ctor_m31324 (InternalEnumerator_1_t5190 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31325_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31325 (InternalEnumerator_1_t5190 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31328(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31328_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&DateTimeFormatFlags_t1907_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31326_MethodInfo;
 void InternalEnumerator_1_Dispose_m31326 (InternalEnumerator_1_t5190 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31327_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31327 (InternalEnumerator_1_t5190 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31328 (InternalEnumerator_1_t5190 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisDateTimeFormatFlags_t1907_m40826(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisDateTimeFormatFlags_t1907_m40826_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5190____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5190_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5190, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5190____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5190_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5190, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5190_FieldInfos[] =
{
	&InternalEnumerator_1_t5190____array_0_FieldInfo,
	&InternalEnumerator_1_t5190____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5190____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5190_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31325_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5190____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5190_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31328_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5190_PropertyInfos[] =
{
	&InternalEnumerator_1_t5190____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5190____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5190_InternalEnumerator_1__ctor_m31324_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31324_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31324_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31324/* method */
	, &InternalEnumerator_1_t5190_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5190_InternalEnumerator_1__ctor_m31324_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31324_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31325_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31325_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31325/* method */
	, &InternalEnumerator_1_t5190_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31325_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31326_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31326_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31326/* method */
	, &InternalEnumerator_1_t5190_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31326_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31327_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31327_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31327/* method */
	, &InternalEnumerator_1_t5190_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31327_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeFormatFlags_t1907_0_0_0;
extern void* RuntimeInvoker_DateTimeFormatFlags_t1907 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31328_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31328_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31328/* method */
	, &InternalEnumerator_1_t5190_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeFormatFlags_t1907_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeFormatFlags_t1907/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31328_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5190_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31324_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31325_MethodInfo,
	&InternalEnumerator_1_Dispose_m31326_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31327_MethodInfo,
	&InternalEnumerator_1_get_Current_m31328_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5190_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31325_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31327_MethodInfo,
	&InternalEnumerator_1_Dispose_m31326_MethodInfo,
	&InternalEnumerator_1_get_Current_m31328_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5190_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7262_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5190_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7262_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5190_0_0_0;
extern Il2CppType InternalEnumerator_1_t5190_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5190_GenericClass;
TypeInfo InternalEnumerator_1_t5190_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5190_MethodInfos/* methods */
	, InternalEnumerator_1_t5190_PropertyInfos/* properties */
	, InternalEnumerator_1_t5190_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5190_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5190_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5190_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5190_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5190_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5190_1_0_0/* this_arg */
	, InternalEnumerator_1_t5190_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5190_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5190)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9300_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>
extern MethodInfo ICollection_1_get_Count_m51908_MethodInfo;
static PropertyInfo ICollection_1_t9300____Count_PropertyInfo = 
{
	&ICollection_1_t9300_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51908_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51909_MethodInfo;
static PropertyInfo ICollection_1_t9300____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9300_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51909_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9300_PropertyInfos[] =
{
	&ICollection_1_t9300____Count_PropertyInfo,
	&ICollection_1_t9300____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51908_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::get_Count()
MethodInfo ICollection_1_get_Count_m51908_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9300_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51908_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51909_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51909_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9300_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51909_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeFormatFlags_t1907_0_0_0;
extern Il2CppType DateTimeFormatFlags_t1907_0_0_0;
static ParameterInfo ICollection_1_t9300_ICollection_1_Add_m51910_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1907_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51910_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Add(T)
MethodInfo ICollection_1_Add_m51910_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9300_ICollection_1_Add_m51910_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51910_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51911_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Clear()
MethodInfo ICollection_1_Clear_m51911_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51911_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeFormatFlags_t1907_0_0_0;
static ParameterInfo ICollection_1_t9300_ICollection_1_Contains_m51912_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1907_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51912_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Contains(T)
MethodInfo ICollection_1_Contains_m51912_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9300_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9300_ICollection_1_Contains_m51912_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51912_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeFormatFlagsU5BU5D_t5499_0_0_0;
extern Il2CppType DateTimeFormatFlagsU5BU5D_t5499_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9300_ICollection_1_CopyTo_m51913_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlagsU5BU5D_t5499_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51913_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51913_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9300_ICollection_1_CopyTo_m51913_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51913_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeFormatFlags_t1907_0_0_0;
static ParameterInfo ICollection_1_t9300_ICollection_1_Remove_m51914_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1907_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51914_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Remove(T)
MethodInfo ICollection_1_Remove_m51914_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9300_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9300_ICollection_1_Remove_m51914_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51914_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9300_MethodInfos[] =
{
	&ICollection_1_get_Count_m51908_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51909_MethodInfo,
	&ICollection_1_Add_m51910_MethodInfo,
	&ICollection_1_Clear_m51911_MethodInfo,
	&ICollection_1_Contains_m51912_MethodInfo,
	&ICollection_1_CopyTo_m51913_MethodInfo,
	&ICollection_1_Remove_m51914_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9302_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9300_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9302_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9300_0_0_0;
extern Il2CppType ICollection_1_t9300_1_0_0;
struct ICollection_1_t9300;
extern Il2CppGenericClass ICollection_1_t9300_GenericClass;
TypeInfo ICollection_1_t9300_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9300_MethodInfos/* methods */
	, ICollection_1_t9300_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9300_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9300_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9300_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9300_0_0_0/* byval_arg */
	, &ICollection_1_t9300_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9300_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeFormatFlags>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeFormatFlags>
extern Il2CppType IEnumerator_1_t7262_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51915_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeFormatFlags>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51915_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9302_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7262_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51915_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9302_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51915_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9302_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9302_0_0_0;
extern Il2CppType IEnumerable_1_t9302_1_0_0;
struct IEnumerable_1_t9302;
extern Il2CppGenericClass IEnumerable_1_t9302_GenericClass;
TypeInfo IEnumerable_1_t9302_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9302_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9302_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9302_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9302_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9302_0_0_0/* byval_arg */
	, &IEnumerable_1_t9302_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9302_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9301_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>
extern MethodInfo IList_1_get_Item_m51916_MethodInfo;
extern MethodInfo IList_1_set_Item_m51917_MethodInfo;
static PropertyInfo IList_1_t9301____Item_PropertyInfo = 
{
	&IList_1_t9301_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51916_MethodInfo/* get */
	, &IList_1_set_Item_m51917_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9301_PropertyInfos[] =
{
	&IList_1_t9301____Item_PropertyInfo,
	NULL
};
extern Il2CppType DateTimeFormatFlags_t1907_0_0_0;
static ParameterInfo IList_1_t9301_IList_1_IndexOf_m51918_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1907_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51918_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51918_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9301_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9301_IList_1_IndexOf_m51918_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51918_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DateTimeFormatFlags_t1907_0_0_0;
static ParameterInfo IList_1_t9301_IList_1_Insert_m51919_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1907_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51919_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51919_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9301_IList_1_Insert_m51919_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51919_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9301_IList_1_RemoveAt_m51920_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51920_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51920_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9301_IList_1_RemoveAt_m51920_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51920_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9301_IList_1_get_Item_m51916_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DateTimeFormatFlags_t1907_0_0_0;
extern void* RuntimeInvoker_DateTimeFormatFlags_t1907_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51916_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51916_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9301_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeFormatFlags_t1907_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeFormatFlags_t1907_Int32_t123/* invoker_method */
	, IList_1_t9301_IList_1_get_Item_m51916_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51916_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DateTimeFormatFlags_t1907_0_0_0;
static ParameterInfo IList_1_t9301_IList_1_set_Item_m51917_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1907_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51917_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51917_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9301_IList_1_set_Item_m51917_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51917_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9301_MethodInfos[] =
{
	&IList_1_IndexOf_m51918_MethodInfo,
	&IList_1_Insert_m51919_MethodInfo,
	&IList_1_RemoveAt_m51920_MethodInfo,
	&IList_1_get_Item_m51916_MethodInfo,
	&IList_1_set_Item_m51917_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9301_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9300_il2cpp_TypeInfo,
	&IEnumerable_1_t9302_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9301_0_0_0;
extern Il2CppType IList_1_t9301_1_0_0;
struct IList_1_t9301;
extern Il2CppGenericClass IList_1_t9301_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9301_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9301_MethodInfos/* methods */
	, IList_1_t9301_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9301_il2cpp_TypeInfo/* element_class */
	, IList_1_t9301_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9301_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9301_0_0_0/* byval_arg */
	, &IList_1_t9301_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9301_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7264_il2cpp_TypeInfo;

// System.Globalization.DateTimeStyles
#include "mscorlib_System_Globalization_DateTimeStyles.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeStyles>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeStyles>
extern MethodInfo IEnumerator_1_get_Current_m51921_MethodInfo;
static PropertyInfo IEnumerator_1_t7264____Current_PropertyInfo = 
{
	&IEnumerator_1_t7264_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51921_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7264_PropertyInfos[] =
{
	&IEnumerator_1_t7264____Current_PropertyInfo,
	NULL
};
extern Il2CppType DateTimeStyles_t1908_0_0_0;
extern void* RuntimeInvoker_DateTimeStyles_t1908 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51921_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeStyles>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51921_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7264_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeStyles_t1908_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeStyles_t1908/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51921_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7264_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51921_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7264_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7264_0_0_0;
extern Il2CppType IEnumerator_1_t7264_1_0_0;
struct IEnumerator_1_t7264;
extern Il2CppGenericClass IEnumerator_1_t7264_GenericClass;
TypeInfo IEnumerator_1_t7264_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7264_MethodInfos/* methods */
	, IEnumerator_1_t7264_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7264_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7264_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7264_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7264_0_0_0/* byval_arg */
	, &IEnumerator_1_t7264_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7264_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_647.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5191_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_647MethodDeclarations.h"

extern TypeInfo DateTimeStyles_t1908_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31333_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDateTimeStyles_t1908_m40837_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.DateTimeStyles>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.DateTimeStyles>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisDateTimeStyles_t1908_m40837 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31329_MethodInfo;
 void InternalEnumerator_1__ctor_m31329 (InternalEnumerator_1_t5191 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31330_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31330 (InternalEnumerator_1_t5191 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31333(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31333_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&DateTimeStyles_t1908_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31331_MethodInfo;
 void InternalEnumerator_1_Dispose_m31331 (InternalEnumerator_1_t5191 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31332_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31332 (InternalEnumerator_1_t5191 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31333 (InternalEnumerator_1_t5191 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisDateTimeStyles_t1908_m40837(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisDateTimeStyles_t1908_m40837_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5191____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5191_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5191, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5191____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5191_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5191, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5191_FieldInfos[] =
{
	&InternalEnumerator_1_t5191____array_0_FieldInfo,
	&InternalEnumerator_1_t5191____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5191____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5191_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31330_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5191____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5191_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31333_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5191_PropertyInfos[] =
{
	&InternalEnumerator_1_t5191____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5191____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5191_InternalEnumerator_1__ctor_m31329_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31329_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31329_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31329/* method */
	, &InternalEnumerator_1_t5191_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5191_InternalEnumerator_1__ctor_m31329_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31329_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31330_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31330_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31330/* method */
	, &InternalEnumerator_1_t5191_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31330_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31331_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31331_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31331/* method */
	, &InternalEnumerator_1_t5191_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31331_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31332_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31332_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31332/* method */
	, &InternalEnumerator_1_t5191_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31332_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeStyles_t1908_0_0_0;
extern void* RuntimeInvoker_DateTimeStyles_t1908 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31333_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31333_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31333/* method */
	, &InternalEnumerator_1_t5191_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeStyles_t1908_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeStyles_t1908/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31333_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5191_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31329_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31330_MethodInfo,
	&InternalEnumerator_1_Dispose_m31331_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31332_MethodInfo,
	&InternalEnumerator_1_get_Current_m31333_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5191_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31330_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31332_MethodInfo,
	&InternalEnumerator_1_Dispose_m31331_MethodInfo,
	&InternalEnumerator_1_get_Current_m31333_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5191_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7264_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5191_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7264_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5191_0_0_0;
extern Il2CppType InternalEnumerator_1_t5191_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5191_GenericClass;
TypeInfo InternalEnumerator_1_t5191_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5191_MethodInfos/* methods */
	, InternalEnumerator_1_t5191_PropertyInfos/* properties */
	, InternalEnumerator_1_t5191_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5191_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5191_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5191_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5191_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5191_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5191_1_0_0/* this_arg */
	, InternalEnumerator_1_t5191_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5191_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5191)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9303_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>
extern MethodInfo ICollection_1_get_Count_m51922_MethodInfo;
static PropertyInfo ICollection_1_t9303____Count_PropertyInfo = 
{
	&ICollection_1_t9303_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51922_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51923_MethodInfo;
static PropertyInfo ICollection_1_t9303____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9303_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51923_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9303_PropertyInfos[] =
{
	&ICollection_1_t9303____Count_PropertyInfo,
	&ICollection_1_t9303____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51922_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::get_Count()
MethodInfo ICollection_1_get_Count_m51922_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9303_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51922_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51923_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51923_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9303_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51923_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeStyles_t1908_0_0_0;
extern Il2CppType DateTimeStyles_t1908_0_0_0;
static ParameterInfo ICollection_1_t9303_ICollection_1_Add_m51924_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1908_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51924_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Add(T)
MethodInfo ICollection_1_Add_m51924_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9303_ICollection_1_Add_m51924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51924_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51925_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Clear()
MethodInfo ICollection_1_Clear_m51925_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51925_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeStyles_t1908_0_0_0;
static ParameterInfo ICollection_1_t9303_ICollection_1_Contains_m51926_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1908_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51926_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Contains(T)
MethodInfo ICollection_1_Contains_m51926_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9303_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9303_ICollection_1_Contains_m51926_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51926_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeStylesU5BU5D_t5500_0_0_0;
extern Il2CppType DateTimeStylesU5BU5D_t5500_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9303_ICollection_1_CopyTo_m51927_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeStylesU5BU5D_t5500_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51927_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51927_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9303_ICollection_1_CopyTo_m51927_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51927_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeStyles_t1908_0_0_0;
static ParameterInfo ICollection_1_t9303_ICollection_1_Remove_m51928_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1908_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51928_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Remove(T)
MethodInfo ICollection_1_Remove_m51928_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9303_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9303_ICollection_1_Remove_m51928_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51928_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9303_MethodInfos[] =
{
	&ICollection_1_get_Count_m51922_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51923_MethodInfo,
	&ICollection_1_Add_m51924_MethodInfo,
	&ICollection_1_Clear_m51925_MethodInfo,
	&ICollection_1_Contains_m51926_MethodInfo,
	&ICollection_1_CopyTo_m51927_MethodInfo,
	&ICollection_1_Remove_m51928_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9305_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9303_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9305_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9303_0_0_0;
extern Il2CppType ICollection_1_t9303_1_0_0;
struct ICollection_1_t9303;
extern Il2CppGenericClass ICollection_1_t9303_GenericClass;
TypeInfo ICollection_1_t9303_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9303_MethodInfos/* methods */
	, ICollection_1_t9303_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9303_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9303_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9303_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9303_0_0_0/* byval_arg */
	, &ICollection_1_t9303_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9303_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeStyles>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeStyles>
extern Il2CppType IEnumerator_1_t7264_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51929_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeStyles>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51929_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9305_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7264_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51929_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9305_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51929_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9305_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9305_0_0_0;
extern Il2CppType IEnumerable_1_t9305_1_0_0;
struct IEnumerable_1_t9305;
extern Il2CppGenericClass IEnumerable_1_t9305_GenericClass;
TypeInfo IEnumerable_1_t9305_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9305_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9305_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9305_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9305_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9305_0_0_0/* byval_arg */
	, &IEnumerable_1_t9305_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9305_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9304_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>
extern MethodInfo IList_1_get_Item_m51930_MethodInfo;
extern MethodInfo IList_1_set_Item_m51931_MethodInfo;
static PropertyInfo IList_1_t9304____Item_PropertyInfo = 
{
	&IList_1_t9304_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51930_MethodInfo/* get */
	, &IList_1_set_Item_m51931_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9304_PropertyInfos[] =
{
	&IList_1_t9304____Item_PropertyInfo,
	NULL
};
extern Il2CppType DateTimeStyles_t1908_0_0_0;
static ParameterInfo IList_1_t9304_IList_1_IndexOf_m51932_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1908_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51932_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51932_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9304_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9304_IList_1_IndexOf_m51932_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51932_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DateTimeStyles_t1908_0_0_0;
static ParameterInfo IList_1_t9304_IList_1_Insert_m51933_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1908_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51933_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51933_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9304_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9304_IList_1_Insert_m51933_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51933_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9304_IList_1_RemoveAt_m51934_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51934_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51934_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9304_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9304_IList_1_RemoveAt_m51934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51934_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9304_IList_1_get_Item_m51930_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType DateTimeStyles_t1908_0_0_0;
extern void* RuntimeInvoker_DateTimeStyles_t1908_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51930_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51930_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9304_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeStyles_t1908_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeStyles_t1908_Int32_t123/* invoker_method */
	, IList_1_t9304_IList_1_get_Item_m51930_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51930_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType DateTimeStyles_t1908_0_0_0;
static ParameterInfo IList_1_t9304_IList_1_set_Item_m51931_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1908_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51931_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51931_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9304_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9304_IList_1_set_Item_m51931_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51931_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9304_MethodInfos[] =
{
	&IList_1_IndexOf_m51932_MethodInfo,
	&IList_1_Insert_m51933_MethodInfo,
	&IList_1_RemoveAt_m51934_MethodInfo,
	&IList_1_get_Item_m51930_MethodInfo,
	&IList_1_set_Item_m51931_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9304_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9303_il2cpp_TypeInfo,
	&IEnumerable_1_t9305_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9304_0_0_0;
extern Il2CppType IList_1_t9304_1_0_0;
struct IList_1_t9304;
extern Il2CppGenericClass IList_1_t9304_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9304_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9304_MethodInfos/* methods */
	, IList_1_t9304_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9304_il2cpp_TypeInfo/* element_class */
	, IList_1_t9304_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9304_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9304_0_0_0/* byval_arg */
	, &IList_1_t9304_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9304_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7266_il2cpp_TypeInfo;

// System.Globalization.GregorianCalendarTypes
#include "mscorlib_System_Globalization_GregorianCalendarTypes.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.GregorianCalendarTypes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.GregorianCalendarTypes>
extern MethodInfo IEnumerator_1_get_Current_m51935_MethodInfo;
static PropertyInfo IEnumerator_1_t7266____Current_PropertyInfo = 
{
	&IEnumerator_1_t7266_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51935_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7266_PropertyInfos[] =
{
	&IEnumerator_1_t7266____Current_PropertyInfo,
	NULL
};
extern Il2CppType GregorianCalendarTypes_t1911_0_0_0;
extern void* RuntimeInvoker_GregorianCalendarTypes_t1911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51935_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.GregorianCalendarTypes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51935_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7266_il2cpp_TypeInfo/* declaring_type */
	, &GregorianCalendarTypes_t1911_0_0_0/* return_type */
	, RuntimeInvoker_GregorianCalendarTypes_t1911/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51935_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7266_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51935_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7266_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7266_0_0_0;
extern Il2CppType IEnumerator_1_t7266_1_0_0;
struct IEnumerator_1_t7266;
extern Il2CppGenericClass IEnumerator_1_t7266_GenericClass;
TypeInfo IEnumerator_1_t7266_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7266_MethodInfos/* methods */
	, IEnumerator_1_t7266_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7266_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7266_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7266_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7266_0_0_0/* byval_arg */
	, &IEnumerator_1_t7266_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7266_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_648.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5192_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_648MethodDeclarations.h"

extern TypeInfo GregorianCalendarTypes_t1911_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31338_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGregorianCalendarTypes_t1911_m40848_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.GregorianCalendarTypes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.GregorianCalendarTypes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisGregorianCalendarTypes_t1911_m40848 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31334_MethodInfo;
 void InternalEnumerator_1__ctor_m31334 (InternalEnumerator_1_t5192 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31335_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31335 (InternalEnumerator_1_t5192 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31338(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31338_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&GregorianCalendarTypes_t1911_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31336_MethodInfo;
 void InternalEnumerator_1_Dispose_m31336 (InternalEnumerator_1_t5192 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31337_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31337 (InternalEnumerator_1_t5192 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31338 (InternalEnumerator_1_t5192 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisGregorianCalendarTypes_t1911_m40848(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisGregorianCalendarTypes_t1911_m40848_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5192____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5192_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5192, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5192____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5192_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5192, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5192_FieldInfos[] =
{
	&InternalEnumerator_1_t5192____array_0_FieldInfo,
	&InternalEnumerator_1_t5192____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5192____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5192_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31335_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5192____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5192_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31338_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5192_PropertyInfos[] =
{
	&InternalEnumerator_1_t5192____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5192____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5192_InternalEnumerator_1__ctor_m31334_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31334_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31334_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31334/* method */
	, &InternalEnumerator_1_t5192_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5192_InternalEnumerator_1__ctor_m31334_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31334_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31335_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31335_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31335/* method */
	, &InternalEnumerator_1_t5192_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31335_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31336_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31336_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31336/* method */
	, &InternalEnumerator_1_t5192_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31336_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31337_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31337_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31337/* method */
	, &InternalEnumerator_1_t5192_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31337_GenericMethod/* genericMethod */

};
extern Il2CppType GregorianCalendarTypes_t1911_0_0_0;
extern void* RuntimeInvoker_GregorianCalendarTypes_t1911 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31338_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31338_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31338/* method */
	, &InternalEnumerator_1_t5192_il2cpp_TypeInfo/* declaring_type */
	, &GregorianCalendarTypes_t1911_0_0_0/* return_type */
	, RuntimeInvoker_GregorianCalendarTypes_t1911/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31338_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5192_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31334_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31335_MethodInfo,
	&InternalEnumerator_1_Dispose_m31336_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31337_MethodInfo,
	&InternalEnumerator_1_get_Current_m31338_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5192_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31335_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31337_MethodInfo,
	&InternalEnumerator_1_Dispose_m31336_MethodInfo,
	&InternalEnumerator_1_get_Current_m31338_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5192_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7266_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5192_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7266_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5192_0_0_0;
extern Il2CppType InternalEnumerator_1_t5192_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5192_GenericClass;
TypeInfo InternalEnumerator_1_t5192_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5192_MethodInfos/* methods */
	, InternalEnumerator_1_t5192_PropertyInfos/* properties */
	, InternalEnumerator_1_t5192_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5192_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5192_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5192_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5192_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5192_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5192_1_0_0/* this_arg */
	, InternalEnumerator_1_t5192_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5192_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5192)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9306_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>
extern MethodInfo ICollection_1_get_Count_m51936_MethodInfo;
static PropertyInfo ICollection_1_t9306____Count_PropertyInfo = 
{
	&ICollection_1_t9306_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51936_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51937_MethodInfo;
static PropertyInfo ICollection_1_t9306____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9306_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51937_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9306_PropertyInfos[] =
{
	&ICollection_1_t9306____Count_PropertyInfo,
	&ICollection_1_t9306____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51936_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::get_Count()
MethodInfo ICollection_1_get_Count_m51936_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9306_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51936_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51937_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51937_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9306_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51937_GenericMethod/* genericMethod */

};
extern Il2CppType GregorianCalendarTypes_t1911_0_0_0;
extern Il2CppType GregorianCalendarTypes_t1911_0_0_0;
static ParameterInfo ICollection_1_t9306_ICollection_1_Add_m51938_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1911_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51938_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Add(T)
MethodInfo ICollection_1_Add_m51938_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9306_ICollection_1_Add_m51938_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51938_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51939_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Clear()
MethodInfo ICollection_1_Clear_m51939_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51939_GenericMethod/* genericMethod */

};
extern Il2CppType GregorianCalendarTypes_t1911_0_0_0;
static ParameterInfo ICollection_1_t9306_ICollection_1_Contains_m51940_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1911_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51940_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Contains(T)
MethodInfo ICollection_1_Contains_m51940_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9306_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9306_ICollection_1_Contains_m51940_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51940_GenericMethod/* genericMethod */

};
extern Il2CppType GregorianCalendarTypesU5BU5D_t5501_0_0_0;
extern Il2CppType GregorianCalendarTypesU5BU5D_t5501_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9306_ICollection_1_CopyTo_m51941_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypesU5BU5D_t5501_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51941_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51941_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9306_ICollection_1_CopyTo_m51941_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51941_GenericMethod/* genericMethod */

};
extern Il2CppType GregorianCalendarTypes_t1911_0_0_0;
static ParameterInfo ICollection_1_t9306_ICollection_1_Remove_m51942_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1911_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51942_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Remove(T)
MethodInfo ICollection_1_Remove_m51942_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9306_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9306_ICollection_1_Remove_m51942_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51942_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9306_MethodInfos[] =
{
	&ICollection_1_get_Count_m51936_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51937_MethodInfo,
	&ICollection_1_Add_m51938_MethodInfo,
	&ICollection_1_Clear_m51939_MethodInfo,
	&ICollection_1_Contains_m51940_MethodInfo,
	&ICollection_1_CopyTo_m51941_MethodInfo,
	&ICollection_1_Remove_m51942_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9308_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9306_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9308_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9306_0_0_0;
extern Il2CppType ICollection_1_t9306_1_0_0;
struct ICollection_1_t9306;
extern Il2CppGenericClass ICollection_1_t9306_GenericClass;
TypeInfo ICollection_1_t9306_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9306_MethodInfos/* methods */
	, ICollection_1_t9306_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9306_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9306_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9306_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9306_0_0_0/* byval_arg */
	, &ICollection_1_t9306_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9306_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.GregorianCalendarTypes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.GregorianCalendarTypes>
extern Il2CppType IEnumerator_1_t7266_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51943_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.GregorianCalendarTypes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51943_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9308_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51943_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9308_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51943_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9308_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9308_0_0_0;
extern Il2CppType IEnumerable_1_t9308_1_0_0;
struct IEnumerable_1_t9308;
extern Il2CppGenericClass IEnumerable_1_t9308_GenericClass;
TypeInfo IEnumerable_1_t9308_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9308_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9308_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9308_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9308_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9308_0_0_0/* byval_arg */
	, &IEnumerable_1_t9308_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9308_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9307_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>
extern MethodInfo IList_1_get_Item_m51944_MethodInfo;
extern MethodInfo IList_1_set_Item_m51945_MethodInfo;
static PropertyInfo IList_1_t9307____Item_PropertyInfo = 
{
	&IList_1_t9307_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51944_MethodInfo/* get */
	, &IList_1_set_Item_m51945_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9307_PropertyInfos[] =
{
	&IList_1_t9307____Item_PropertyInfo,
	NULL
};
extern Il2CppType GregorianCalendarTypes_t1911_0_0_0;
static ParameterInfo IList_1_t9307_IList_1_IndexOf_m51946_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1911_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51946_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51946_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9307_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9307_IList_1_IndexOf_m51946_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51946_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GregorianCalendarTypes_t1911_0_0_0;
static ParameterInfo IList_1_t9307_IList_1_Insert_m51947_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1911_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51947_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51947_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9307_IList_1_Insert_m51947_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51947_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9307_IList_1_RemoveAt_m51948_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51948_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51948_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9307_IList_1_RemoveAt_m51948_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51948_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9307_IList_1_get_Item_m51944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType GregorianCalendarTypes_t1911_0_0_0;
extern void* RuntimeInvoker_GregorianCalendarTypes_t1911_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51944_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51944_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9307_il2cpp_TypeInfo/* declaring_type */
	, &GregorianCalendarTypes_t1911_0_0_0/* return_type */
	, RuntimeInvoker_GregorianCalendarTypes_t1911_Int32_t123/* invoker_method */
	, IList_1_t9307_IList_1_get_Item_m51944_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51944_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GregorianCalendarTypes_t1911_0_0_0;
static ParameterInfo IList_1_t9307_IList_1_set_Item_m51945_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1911_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51945_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51945_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9307_IList_1_set_Item_m51945_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51945_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9307_MethodInfos[] =
{
	&IList_1_IndexOf_m51946_MethodInfo,
	&IList_1_Insert_m51947_MethodInfo,
	&IList_1_RemoveAt_m51948_MethodInfo,
	&IList_1_get_Item_m51944_MethodInfo,
	&IList_1_set_Item_m51945_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9307_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9306_il2cpp_TypeInfo,
	&IEnumerable_1_t9308_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9307_0_0_0;
extern Il2CppType IList_1_t9307_1_0_0;
struct IList_1_t9307;
extern Il2CppGenericClass IList_1_t9307_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9307_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9307_MethodInfos/* methods */
	, IList_1_t9307_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9307_il2cpp_TypeInfo/* element_class */
	, IList_1_t9307_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9307_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9307_0_0_0/* byval_arg */
	, &IList_1_t9307_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9307_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7268_il2cpp_TypeInfo;

// System.Globalization.NumberStyles
#include "mscorlib_System_Globalization_NumberStyles.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.NumberStyles>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.NumberStyles>
extern MethodInfo IEnumerator_1_get_Current_m51949_MethodInfo;
static PropertyInfo IEnumerator_1_t7268____Current_PropertyInfo = 
{
	&IEnumerator_1_t7268_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51949_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7268_PropertyInfos[] =
{
	&IEnumerator_1_t7268____Current_PropertyInfo,
	NULL
};
extern Il2CppType NumberStyles_t1912_0_0_0;
extern void* RuntimeInvoker_NumberStyles_t1912 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51949_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.NumberStyles>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51949_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7268_il2cpp_TypeInfo/* declaring_type */
	, &NumberStyles_t1912_0_0_0/* return_type */
	, RuntimeInvoker_NumberStyles_t1912/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51949_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7268_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51949_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7268_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7268_0_0_0;
extern Il2CppType IEnumerator_1_t7268_1_0_0;
struct IEnumerator_1_t7268;
extern Il2CppGenericClass IEnumerator_1_t7268_GenericClass;
TypeInfo IEnumerator_1_t7268_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7268_MethodInfos/* methods */
	, IEnumerator_1_t7268_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7268_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7268_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7268_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7268_0_0_0/* byval_arg */
	, &IEnumerator_1_t7268_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7268_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_649.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5193_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_649MethodDeclarations.h"

extern TypeInfo NumberStyles_t1912_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31343_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisNumberStyles_t1912_m40859_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.NumberStyles>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.NumberStyles>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisNumberStyles_t1912_m40859 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31339_MethodInfo;
 void InternalEnumerator_1__ctor_m31339 (InternalEnumerator_1_t5193 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31340_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31340 (InternalEnumerator_1_t5193 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31343(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31343_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&NumberStyles_t1912_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31341_MethodInfo;
 void InternalEnumerator_1_Dispose_m31341 (InternalEnumerator_1_t5193 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31342_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31342 (InternalEnumerator_1_t5193 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31343 (InternalEnumerator_1_t5193 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisNumberStyles_t1912_m40859(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisNumberStyles_t1912_m40859_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5193____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5193_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5193, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5193____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5193_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5193, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5193_FieldInfos[] =
{
	&InternalEnumerator_1_t5193____array_0_FieldInfo,
	&InternalEnumerator_1_t5193____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5193____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5193_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31340_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5193____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5193_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31343_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5193_PropertyInfos[] =
{
	&InternalEnumerator_1_t5193____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5193____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5193_InternalEnumerator_1__ctor_m31339_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31339_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31339_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31339/* method */
	, &InternalEnumerator_1_t5193_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5193_InternalEnumerator_1__ctor_m31339_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31339_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31340_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31340_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31340/* method */
	, &InternalEnumerator_1_t5193_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31340_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31341_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31341_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31341/* method */
	, &InternalEnumerator_1_t5193_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31341_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31342_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31342_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31342/* method */
	, &InternalEnumerator_1_t5193_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31342_GenericMethod/* genericMethod */

};
extern Il2CppType NumberStyles_t1912_0_0_0;
extern void* RuntimeInvoker_NumberStyles_t1912 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31343_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31343_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31343/* method */
	, &InternalEnumerator_1_t5193_il2cpp_TypeInfo/* declaring_type */
	, &NumberStyles_t1912_0_0_0/* return_type */
	, RuntimeInvoker_NumberStyles_t1912/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31343_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5193_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31339_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31340_MethodInfo,
	&InternalEnumerator_1_Dispose_m31341_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31342_MethodInfo,
	&InternalEnumerator_1_get_Current_m31343_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5193_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31340_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31342_MethodInfo,
	&InternalEnumerator_1_Dispose_m31341_MethodInfo,
	&InternalEnumerator_1_get_Current_m31343_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5193_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7268_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5193_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7268_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5193_0_0_0;
extern Il2CppType InternalEnumerator_1_t5193_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5193_GenericClass;
TypeInfo InternalEnumerator_1_t5193_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5193_MethodInfos/* methods */
	, InternalEnumerator_1_t5193_PropertyInfos/* properties */
	, InternalEnumerator_1_t5193_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5193_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5193_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5193_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5193_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5193_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5193_1_0_0/* this_arg */
	, InternalEnumerator_1_t5193_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5193_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5193)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9309_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>
extern MethodInfo ICollection_1_get_Count_m51950_MethodInfo;
static PropertyInfo ICollection_1_t9309____Count_PropertyInfo = 
{
	&ICollection_1_t9309_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51950_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51951_MethodInfo;
static PropertyInfo ICollection_1_t9309____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9309_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51951_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9309_PropertyInfos[] =
{
	&ICollection_1_t9309____Count_PropertyInfo,
	&ICollection_1_t9309____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51950_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::get_Count()
MethodInfo ICollection_1_get_Count_m51950_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9309_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51950_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51951_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51951_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9309_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51951_GenericMethod/* genericMethod */

};
extern Il2CppType NumberStyles_t1912_0_0_0;
extern Il2CppType NumberStyles_t1912_0_0_0;
static ParameterInfo ICollection_1_t9309_ICollection_1_Add_m51952_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1912_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51952_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Add(T)
MethodInfo ICollection_1_Add_m51952_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9309_ICollection_1_Add_m51952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51952_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51953_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Clear()
MethodInfo ICollection_1_Clear_m51953_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51953_GenericMethod/* genericMethod */

};
extern Il2CppType NumberStyles_t1912_0_0_0;
static ParameterInfo ICollection_1_t9309_ICollection_1_Contains_m51954_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1912_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51954_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Contains(T)
MethodInfo ICollection_1_Contains_m51954_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9309_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9309_ICollection_1_Contains_m51954_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51954_GenericMethod/* genericMethod */

};
extern Il2CppType NumberStylesU5BU5D_t5502_0_0_0;
extern Il2CppType NumberStylesU5BU5D_t5502_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9309_ICollection_1_CopyTo_m51955_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &NumberStylesU5BU5D_t5502_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51955_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51955_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9309_ICollection_1_CopyTo_m51955_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51955_GenericMethod/* genericMethod */

};
extern Il2CppType NumberStyles_t1912_0_0_0;
static ParameterInfo ICollection_1_t9309_ICollection_1_Remove_m51956_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1912_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51956_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Remove(T)
MethodInfo ICollection_1_Remove_m51956_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9309_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9309_ICollection_1_Remove_m51956_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51956_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9309_MethodInfos[] =
{
	&ICollection_1_get_Count_m51950_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51951_MethodInfo,
	&ICollection_1_Add_m51952_MethodInfo,
	&ICollection_1_Clear_m51953_MethodInfo,
	&ICollection_1_Contains_m51954_MethodInfo,
	&ICollection_1_CopyTo_m51955_MethodInfo,
	&ICollection_1_Remove_m51956_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9311_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9309_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9311_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9309_0_0_0;
extern Il2CppType ICollection_1_t9309_1_0_0;
struct ICollection_1_t9309;
extern Il2CppGenericClass ICollection_1_t9309_GenericClass;
TypeInfo ICollection_1_t9309_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9309_MethodInfos/* methods */
	, ICollection_1_t9309_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9309_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9309_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9309_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9309_0_0_0/* byval_arg */
	, &ICollection_1_t9309_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9309_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.NumberStyles>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.NumberStyles>
extern Il2CppType IEnumerator_1_t7268_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51957_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.NumberStyles>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51957_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9311_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7268_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51957_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9311_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51957_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9311_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9311_0_0_0;
extern Il2CppType IEnumerable_1_t9311_1_0_0;
struct IEnumerable_1_t9311;
extern Il2CppGenericClass IEnumerable_1_t9311_GenericClass;
TypeInfo IEnumerable_1_t9311_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9311_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9311_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9311_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9311_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9311_0_0_0/* byval_arg */
	, &IEnumerable_1_t9311_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9311_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9310_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.NumberStyles>
extern MethodInfo IList_1_get_Item_m51958_MethodInfo;
extern MethodInfo IList_1_set_Item_m51959_MethodInfo;
static PropertyInfo IList_1_t9310____Item_PropertyInfo = 
{
	&IList_1_t9310_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51958_MethodInfo/* get */
	, &IList_1_set_Item_m51959_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9310_PropertyInfos[] =
{
	&IList_1_t9310____Item_PropertyInfo,
	NULL
};
extern Il2CppType NumberStyles_t1912_0_0_0;
static ParameterInfo IList_1_t9310_IList_1_IndexOf_m51960_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1912_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51960_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51960_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9310_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9310_IList_1_IndexOf_m51960_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51960_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType NumberStyles_t1912_0_0_0;
static ParameterInfo IList_1_t9310_IList_1_Insert_m51961_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1912_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51961_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51961_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9310_IList_1_Insert_m51961_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51961_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9310_IList_1_RemoveAt_m51962_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51962_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51962_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9310_IList_1_RemoveAt_m51962_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51962_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9310_IList_1_get_Item_m51958_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType NumberStyles_t1912_0_0_0;
extern void* RuntimeInvoker_NumberStyles_t1912_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51958_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51958_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9310_il2cpp_TypeInfo/* declaring_type */
	, &NumberStyles_t1912_0_0_0/* return_type */
	, RuntimeInvoker_NumberStyles_t1912_Int32_t123/* invoker_method */
	, IList_1_t9310_IList_1_get_Item_m51958_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51958_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType NumberStyles_t1912_0_0_0;
static ParameterInfo IList_1_t9310_IList_1_set_Item_m51959_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1912_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51959_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51959_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9310_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9310_IList_1_set_Item_m51959_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51959_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9310_MethodInfos[] =
{
	&IList_1_IndexOf_m51960_MethodInfo,
	&IList_1_Insert_m51961_MethodInfo,
	&IList_1_RemoveAt_m51962_MethodInfo,
	&IList_1_get_Item_m51958_MethodInfo,
	&IList_1_set_Item_m51959_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9310_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9309_il2cpp_TypeInfo,
	&IEnumerable_1_t9311_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9310_0_0_0;
extern Il2CppType IList_1_t9310_1_0_0;
struct IList_1_t9310;
extern Il2CppGenericClass IList_1_t9310_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9310_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9310_MethodInfos/* methods */
	, IList_1_t9310_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9310_il2cpp_TypeInfo/* element_class */
	, IList_1_t9310_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9310_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9310_0_0_0/* byval_arg */
	, &IList_1_t9310_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9310_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7270_il2cpp_TypeInfo;

// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.UnicodeCategory>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.UnicodeCategory>
extern MethodInfo IEnumerator_1_get_Current_m51963_MethodInfo;
static PropertyInfo IEnumerator_1_t7270____Current_PropertyInfo = 
{
	&IEnumerator_1_t7270_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51963_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7270_PropertyInfos[] =
{
	&IEnumerator_1_t7270____Current_PropertyInfo,
	NULL
};
extern Il2CppType UnicodeCategory_t1576_0_0_0;
extern void* RuntimeInvoker_UnicodeCategory_t1576 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51963_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.UnicodeCategory>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51963_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7270_il2cpp_TypeInfo/* declaring_type */
	, &UnicodeCategory_t1576_0_0_0/* return_type */
	, RuntimeInvoker_UnicodeCategory_t1576/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51963_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7270_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51963_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7270_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7270_0_0_0;
extern Il2CppType IEnumerator_1_t7270_1_0_0;
struct IEnumerator_1_t7270;
extern Il2CppGenericClass IEnumerator_1_t7270_GenericClass;
TypeInfo IEnumerator_1_t7270_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7270_MethodInfos/* methods */
	, IEnumerator_1_t7270_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7270_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7270_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7270_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7270_0_0_0/* byval_arg */
	, &IEnumerator_1_t7270_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7270_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_650.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5194_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_650MethodDeclarations.h"

extern TypeInfo UnicodeCategory_t1576_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31348_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUnicodeCategory_t1576_m40870_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.UnicodeCategory>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.UnicodeCategory>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisUnicodeCategory_t1576_m40870 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31344_MethodInfo;
 void InternalEnumerator_1__ctor_m31344 (InternalEnumerator_1_t5194 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31345_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31345 (InternalEnumerator_1_t5194 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31348(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31348_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&UnicodeCategory_t1576_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31346_MethodInfo;
 void InternalEnumerator_1_Dispose_m31346 (InternalEnumerator_1_t5194 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31347_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31347 (InternalEnumerator_1_t5194 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31348 (InternalEnumerator_1_t5194 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisUnicodeCategory_t1576_m40870(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisUnicodeCategory_t1576_m40870_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5194____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5194_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5194, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5194____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5194_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5194, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5194_FieldInfos[] =
{
	&InternalEnumerator_1_t5194____array_0_FieldInfo,
	&InternalEnumerator_1_t5194____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5194____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5194_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31345_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5194____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5194_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31348_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5194_PropertyInfos[] =
{
	&InternalEnumerator_1_t5194____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5194____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5194_InternalEnumerator_1__ctor_m31344_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31344_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31344_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31344/* method */
	, &InternalEnumerator_1_t5194_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5194_InternalEnumerator_1__ctor_m31344_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31344_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31345_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31345_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31345/* method */
	, &InternalEnumerator_1_t5194_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31345_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31346_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31346_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31346/* method */
	, &InternalEnumerator_1_t5194_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31346_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31347_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31347_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31347/* method */
	, &InternalEnumerator_1_t5194_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31347_GenericMethod/* genericMethod */

};
extern Il2CppType UnicodeCategory_t1576_0_0_0;
extern void* RuntimeInvoker_UnicodeCategory_t1576 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31348_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31348_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31348/* method */
	, &InternalEnumerator_1_t5194_il2cpp_TypeInfo/* declaring_type */
	, &UnicodeCategory_t1576_0_0_0/* return_type */
	, RuntimeInvoker_UnicodeCategory_t1576/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31348_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5194_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31344_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31345_MethodInfo,
	&InternalEnumerator_1_Dispose_m31346_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31347_MethodInfo,
	&InternalEnumerator_1_get_Current_m31348_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5194_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31345_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31347_MethodInfo,
	&InternalEnumerator_1_Dispose_m31346_MethodInfo,
	&InternalEnumerator_1_get_Current_m31348_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5194_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7270_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5194_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7270_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5194_0_0_0;
extern Il2CppType InternalEnumerator_1_t5194_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5194_GenericClass;
TypeInfo InternalEnumerator_1_t5194_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5194_MethodInfos/* methods */
	, InternalEnumerator_1_t5194_PropertyInfos/* properties */
	, InternalEnumerator_1_t5194_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5194_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5194_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5194_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5194_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5194_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5194_1_0_0/* this_arg */
	, InternalEnumerator_1_t5194_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5194_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5194)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9312_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>
extern MethodInfo ICollection_1_get_Count_m51964_MethodInfo;
static PropertyInfo ICollection_1_t9312____Count_PropertyInfo = 
{
	&ICollection_1_t9312_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51964_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51965_MethodInfo;
static PropertyInfo ICollection_1_t9312____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9312_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51965_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9312_PropertyInfos[] =
{
	&ICollection_1_t9312____Count_PropertyInfo,
	&ICollection_1_t9312____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51964_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::get_Count()
MethodInfo ICollection_1_get_Count_m51964_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9312_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51964_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51965_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51965_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9312_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51965_GenericMethod/* genericMethod */

};
extern Il2CppType UnicodeCategory_t1576_0_0_0;
extern Il2CppType UnicodeCategory_t1576_0_0_0;
static ParameterInfo ICollection_1_t9312_ICollection_1_Add_m51966_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1576_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51966_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Add(T)
MethodInfo ICollection_1_Add_m51966_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9312_ICollection_1_Add_m51966_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51966_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51967_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Clear()
MethodInfo ICollection_1_Clear_m51967_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51967_GenericMethod/* genericMethod */

};
extern Il2CppType UnicodeCategory_t1576_0_0_0;
static ParameterInfo ICollection_1_t9312_ICollection_1_Contains_m51968_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1576_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51968_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Contains(T)
MethodInfo ICollection_1_Contains_m51968_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9312_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9312_ICollection_1_Contains_m51968_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51968_GenericMethod/* genericMethod */

};
extern Il2CppType UnicodeCategoryU5BU5D_t5503_0_0_0;
extern Il2CppType UnicodeCategoryU5BU5D_t5503_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9312_ICollection_1_CopyTo_m51969_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UnicodeCategoryU5BU5D_t5503_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51969_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51969_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9312_ICollection_1_CopyTo_m51969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51969_GenericMethod/* genericMethod */

};
extern Il2CppType UnicodeCategory_t1576_0_0_0;
static ParameterInfo ICollection_1_t9312_ICollection_1_Remove_m51970_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1576_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51970_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Remove(T)
MethodInfo ICollection_1_Remove_m51970_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9312_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9312_ICollection_1_Remove_m51970_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51970_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9312_MethodInfos[] =
{
	&ICollection_1_get_Count_m51964_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51965_MethodInfo,
	&ICollection_1_Add_m51966_MethodInfo,
	&ICollection_1_Clear_m51967_MethodInfo,
	&ICollection_1_Contains_m51968_MethodInfo,
	&ICollection_1_CopyTo_m51969_MethodInfo,
	&ICollection_1_Remove_m51970_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9314_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9312_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9314_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9312_0_0_0;
extern Il2CppType ICollection_1_t9312_1_0_0;
struct ICollection_1_t9312;
extern Il2CppGenericClass ICollection_1_t9312_GenericClass;
TypeInfo ICollection_1_t9312_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9312_MethodInfos/* methods */
	, ICollection_1_t9312_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9312_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9312_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9312_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9312_0_0_0/* byval_arg */
	, &ICollection_1_t9312_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9312_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.UnicodeCategory>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.UnicodeCategory>
extern Il2CppType IEnumerator_1_t7270_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51971_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.UnicodeCategory>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51971_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9314_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7270_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51971_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9314_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51971_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9314_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9314_0_0_0;
extern Il2CppType IEnumerable_1_t9314_1_0_0;
struct IEnumerable_1_t9314;
extern Il2CppGenericClass IEnumerable_1_t9314_GenericClass;
TypeInfo IEnumerable_1_t9314_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9314_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9314_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9314_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9314_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9314_0_0_0/* byval_arg */
	, &IEnumerable_1_t9314_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9314_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9313_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>
extern MethodInfo IList_1_get_Item_m51972_MethodInfo;
extern MethodInfo IList_1_set_Item_m51973_MethodInfo;
static PropertyInfo IList_1_t9313____Item_PropertyInfo = 
{
	&IList_1_t9313_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51972_MethodInfo/* get */
	, &IList_1_set_Item_m51973_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9313_PropertyInfos[] =
{
	&IList_1_t9313____Item_PropertyInfo,
	NULL
};
extern Il2CppType UnicodeCategory_t1576_0_0_0;
static ParameterInfo IList_1_t9313_IList_1_IndexOf_m51974_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1576_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51974_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51974_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9313_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9313_IList_1_IndexOf_m51974_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51974_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UnicodeCategory_t1576_0_0_0;
static ParameterInfo IList_1_t9313_IList_1_Insert_m51975_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1576_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51975_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51975_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9313_IList_1_Insert_m51975_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51975_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9313_IList_1_RemoveAt_m51976_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51976_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51976_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9313_IList_1_RemoveAt_m51976_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51976_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9313_IList_1_get_Item_m51972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType UnicodeCategory_t1576_0_0_0;
extern void* RuntimeInvoker_UnicodeCategory_t1576_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51972_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51972_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9313_il2cpp_TypeInfo/* declaring_type */
	, &UnicodeCategory_t1576_0_0_0/* return_type */
	, RuntimeInvoker_UnicodeCategory_t1576_Int32_t123/* invoker_method */
	, IList_1_t9313_IList_1_get_Item_m51972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51972_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType UnicodeCategory_t1576_0_0_0;
static ParameterInfo IList_1_t9313_IList_1_set_Item_m51973_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1576_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51973_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51973_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9313_IList_1_set_Item_m51973_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51973_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9313_MethodInfos[] =
{
	&IList_1_IndexOf_m51974_MethodInfo,
	&IList_1_Insert_m51975_MethodInfo,
	&IList_1_RemoveAt_m51976_MethodInfo,
	&IList_1_get_Item_m51972_MethodInfo,
	&IList_1_set_Item_m51973_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9313_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9312_il2cpp_TypeInfo,
	&IEnumerable_1_t9314_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9313_0_0_0;
extern Il2CppType IList_1_t9313_1_0_0;
struct IList_1_t9313;
extern Il2CppGenericClass IList_1_t9313_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9313_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9313_MethodInfos/* methods */
	, IList_1_t9313_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9313_il2cpp_TypeInfo/* element_class */
	, IList_1_t9313_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9313_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9313_0_0_0/* byval_arg */
	, &IList_1_t9313_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9313_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7272_il2cpp_TypeInfo;

// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.FileAttributes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileAttributes>
extern MethodInfo IEnumerator_1_get_Current_m51977_MethodInfo;
static PropertyInfo IEnumerator_1_t7272____Current_PropertyInfo = 
{
	&IEnumerator_1_t7272_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51977_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7272_PropertyInfos[] =
{
	&IEnumerator_1_t7272____Current_PropertyInfo,
	NULL
};
extern Il2CppType FileAttributes_t1924_0_0_0;
extern void* RuntimeInvoker_FileAttributes_t1924 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51977_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.FileAttributes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51977_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7272_il2cpp_TypeInfo/* declaring_type */
	, &FileAttributes_t1924_0_0_0/* return_type */
	, RuntimeInvoker_FileAttributes_t1924/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51977_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7272_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51977_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7272_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7272_0_0_0;
extern Il2CppType IEnumerator_1_t7272_1_0_0;
struct IEnumerator_1_t7272;
extern Il2CppGenericClass IEnumerator_1_t7272_GenericClass;
TypeInfo IEnumerator_1_t7272_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7272_MethodInfos/* methods */
	, IEnumerator_1_t7272_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7272_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7272_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7272_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7272_0_0_0/* byval_arg */
	, &IEnumerator_1_t7272_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7272_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.FileAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_651.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5195_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.FileAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_651MethodDeclarations.h"

extern TypeInfo FileAttributes_t1924_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31353_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFileAttributes_t1924_m40881_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.FileAttributes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.FileAttributes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFileAttributes_t1924_m40881 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.FileAttributes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31349_MethodInfo;
 void InternalEnumerator_1__ctor_m31349 (InternalEnumerator_1_t5195 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.FileAttributes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31350_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31350 (InternalEnumerator_1_t5195 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31353(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31353_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FileAttributes_t1924_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAttributes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31351_MethodInfo;
 void InternalEnumerator_1_Dispose_m31351 (InternalEnumerator_1_t5195 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileAttributes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31352_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31352 (InternalEnumerator_1_t5195 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.FileAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31353 (InternalEnumerator_1_t5195 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFileAttributes_t1924_m40881(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFileAttributes_t1924_m40881_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileAttributes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5195____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5195_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5195, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5195____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5195_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5195, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5195_FieldInfos[] =
{
	&InternalEnumerator_1_t5195____array_0_FieldInfo,
	&InternalEnumerator_1_t5195____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5195____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5195_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31350_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5195____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5195_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31353_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5195_PropertyInfos[] =
{
	&InternalEnumerator_1_t5195____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5195____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5195_InternalEnumerator_1__ctor_m31349_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31349_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAttributes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31349_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31349/* method */
	, &InternalEnumerator_1_t5195_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5195_InternalEnumerator_1__ctor_m31349_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31349_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31350_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileAttributes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31350_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31350/* method */
	, &InternalEnumerator_1_t5195_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31350_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31351_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAttributes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31351_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31351/* method */
	, &InternalEnumerator_1_t5195_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31351_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31352_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileAttributes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31352_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31352/* method */
	, &InternalEnumerator_1_t5195_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31352_GenericMethod/* genericMethod */

};
extern Il2CppType FileAttributes_t1924_0_0_0;
extern void* RuntimeInvoker_FileAttributes_t1924 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31353_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.FileAttributes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31353_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31353/* method */
	, &InternalEnumerator_1_t5195_il2cpp_TypeInfo/* declaring_type */
	, &FileAttributes_t1924_0_0_0/* return_type */
	, RuntimeInvoker_FileAttributes_t1924/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31353_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5195_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31349_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31350_MethodInfo,
	&InternalEnumerator_1_Dispose_m31351_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31352_MethodInfo,
	&InternalEnumerator_1_get_Current_m31353_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5195_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31350_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31352_MethodInfo,
	&InternalEnumerator_1_Dispose_m31351_MethodInfo,
	&InternalEnumerator_1_get_Current_m31353_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5195_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7272_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5195_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7272_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5195_0_0_0;
extern Il2CppType InternalEnumerator_1_t5195_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5195_GenericClass;
TypeInfo InternalEnumerator_1_t5195_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5195_MethodInfos/* methods */
	, InternalEnumerator_1_t5195_PropertyInfos/* properties */
	, InternalEnumerator_1_t5195_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5195_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5195_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5195_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5195_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5195_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5195_1_0_0/* this_arg */
	, InternalEnumerator_1_t5195_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5195_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5195)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9315_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileAttributes>
extern MethodInfo ICollection_1_get_Count_m51978_MethodInfo;
static PropertyInfo ICollection_1_t9315____Count_PropertyInfo = 
{
	&ICollection_1_t9315_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51978_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51979_MethodInfo;
static PropertyInfo ICollection_1_t9315____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9315_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51979_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9315_PropertyInfos[] =
{
	&ICollection_1_t9315____Count_PropertyInfo,
	&ICollection_1_t9315____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51978_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::get_Count()
MethodInfo ICollection_1_get_Count_m51978_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9315_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51978_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51979_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51979_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9315_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51979_GenericMethod/* genericMethod */

};
extern Il2CppType FileAttributes_t1924_0_0_0;
extern Il2CppType FileAttributes_t1924_0_0_0;
static ParameterInfo ICollection_1_t9315_ICollection_1_Add_m51980_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1924_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51980_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Add(T)
MethodInfo ICollection_1_Add_m51980_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9315_ICollection_1_Add_m51980_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51980_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51981_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Clear()
MethodInfo ICollection_1_Clear_m51981_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51981_GenericMethod/* genericMethod */

};
extern Il2CppType FileAttributes_t1924_0_0_0;
static ParameterInfo ICollection_1_t9315_ICollection_1_Contains_m51982_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1924_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51982_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Contains(T)
MethodInfo ICollection_1_Contains_m51982_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9315_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9315_ICollection_1_Contains_m51982_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51982_GenericMethod/* genericMethod */

};
extern Il2CppType FileAttributesU5BU5D_t5504_0_0_0;
extern Il2CppType FileAttributesU5BU5D_t5504_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9315_ICollection_1_CopyTo_m51983_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FileAttributesU5BU5D_t5504_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51983_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51983_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9315_ICollection_1_CopyTo_m51983_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51983_GenericMethod/* genericMethod */

};
extern Il2CppType FileAttributes_t1924_0_0_0;
static ParameterInfo ICollection_1_t9315_ICollection_1_Remove_m51984_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1924_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51984_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Remove(T)
MethodInfo ICollection_1_Remove_m51984_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9315_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9315_ICollection_1_Remove_m51984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51984_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9315_MethodInfos[] =
{
	&ICollection_1_get_Count_m51978_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51979_MethodInfo,
	&ICollection_1_Add_m51980_MethodInfo,
	&ICollection_1_Clear_m51981_MethodInfo,
	&ICollection_1_Contains_m51982_MethodInfo,
	&ICollection_1_CopyTo_m51983_MethodInfo,
	&ICollection_1_Remove_m51984_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9317_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9315_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9317_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9315_0_0_0;
extern Il2CppType ICollection_1_t9315_1_0_0;
struct ICollection_1_t9315;
extern Il2CppGenericClass ICollection_1_t9315_GenericClass;
TypeInfo ICollection_1_t9315_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9315_MethodInfos/* methods */
	, ICollection_1_t9315_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9315_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9315_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9315_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9315_0_0_0/* byval_arg */
	, &ICollection_1_t9315_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9315_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileAttributes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileAttributes>
extern Il2CppType IEnumerator_1_t7272_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51985_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileAttributes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51985_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9317_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7272_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51985_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9317_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51985_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9317_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9317_0_0_0;
extern Il2CppType IEnumerable_1_t9317_1_0_0;
struct IEnumerable_1_t9317;
extern Il2CppGenericClass IEnumerable_1_t9317_GenericClass;
TypeInfo IEnumerable_1_t9317_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9317_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9317_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9317_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9317_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9317_0_0_0/* byval_arg */
	, &IEnumerable_1_t9317_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9317_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9316_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.FileAttributes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.FileAttributes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileAttributes>
extern MethodInfo IList_1_get_Item_m51986_MethodInfo;
extern MethodInfo IList_1_set_Item_m51987_MethodInfo;
static PropertyInfo IList_1_t9316____Item_PropertyInfo = 
{
	&IList_1_t9316_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m51986_MethodInfo/* get */
	, &IList_1_set_Item_m51987_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9316_PropertyInfos[] =
{
	&IList_1_t9316____Item_PropertyInfo,
	NULL
};
extern Il2CppType FileAttributes_t1924_0_0_0;
static ParameterInfo IList_1_t9316_IList_1_IndexOf_m51988_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1924_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m51988_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.FileAttributes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m51988_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9316_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9316_IList_1_IndexOf_m51988_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m51988_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FileAttributes_t1924_0_0_0;
static ParameterInfo IList_1_t9316_IList_1_Insert_m51989_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1924_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m51989_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m51989_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9316_IList_1_Insert_m51989_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m51989_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9316_IList_1_RemoveAt_m51990_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m51990_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m51990_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9316_IList_1_RemoveAt_m51990_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m51990_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9316_IList_1_get_Item_m51986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType FileAttributes_t1924_0_0_0;
extern void* RuntimeInvoker_FileAttributes_t1924_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m51986_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.FileAttributes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m51986_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9316_il2cpp_TypeInfo/* declaring_type */
	, &FileAttributes_t1924_0_0_0/* return_type */
	, RuntimeInvoker_FileAttributes_t1924_Int32_t123/* invoker_method */
	, IList_1_t9316_IList_1_get_Item_m51986_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m51986_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FileAttributes_t1924_0_0_0;
static ParameterInfo IList_1_t9316_IList_1_set_Item_m51987_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1924_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m51987_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m51987_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9316_IList_1_set_Item_m51987_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m51987_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9316_MethodInfos[] =
{
	&IList_1_IndexOf_m51988_MethodInfo,
	&IList_1_Insert_m51989_MethodInfo,
	&IList_1_RemoveAt_m51990_MethodInfo,
	&IList_1_get_Item_m51986_MethodInfo,
	&IList_1_set_Item_m51987_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9316_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9315_il2cpp_TypeInfo,
	&IEnumerable_1_t9317_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9316_0_0_0;
extern Il2CppType IList_1_t9316_1_0_0;
struct IList_1_t9316;
extern Il2CppGenericClass IList_1_t9316_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9316_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9316_MethodInfos/* methods */
	, IList_1_t9316_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9316_il2cpp_TypeInfo/* element_class */
	, IList_1_t9316_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9316_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9316_0_0_0/* byval_arg */
	, &IList_1_t9316_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9316_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7274_il2cpp_TypeInfo;

// System.IO.FileMode
#include "mscorlib_System_IO_FileMode.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.FileMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileMode>
extern MethodInfo IEnumerator_1_get_Current_m51991_MethodInfo;
static PropertyInfo IEnumerator_1_t7274____Current_PropertyInfo = 
{
	&IEnumerator_1_t7274_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m51991_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7274_PropertyInfos[] =
{
	&IEnumerator_1_t7274____Current_PropertyInfo,
	NULL
};
extern Il2CppType FileMode_t1925_0_0_0;
extern void* RuntimeInvoker_FileMode_t1925 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m51991_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.FileMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m51991_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7274_il2cpp_TypeInfo/* declaring_type */
	, &FileMode_t1925_0_0_0/* return_type */
	, RuntimeInvoker_FileMode_t1925/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m51991_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7274_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m51991_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7274_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7274_0_0_0;
extern Il2CppType IEnumerator_1_t7274_1_0_0;
struct IEnumerator_1_t7274;
extern Il2CppGenericClass IEnumerator_1_t7274_GenericClass;
TypeInfo IEnumerator_1_t7274_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7274_MethodInfos/* methods */
	, IEnumerator_1_t7274_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7274_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7274_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7274_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7274_0_0_0/* byval_arg */
	, &IEnumerator_1_t7274_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7274_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.FileMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_652.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5196_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.FileMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_652MethodDeclarations.h"

extern TypeInfo FileMode_t1925_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31358_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFileMode_t1925_m40892_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.FileMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.FileMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFileMode_t1925_m40892 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.FileMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31354_MethodInfo;
 void InternalEnumerator_1__ctor_m31354 (InternalEnumerator_1_t5196 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.FileMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31355_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31355 (InternalEnumerator_1_t5196 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31358(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31358_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FileMode_t1925_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.FileMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31356_MethodInfo;
 void InternalEnumerator_1_Dispose_m31356 (InternalEnumerator_1_t5196 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31357_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31357 (InternalEnumerator_1_t5196 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.FileMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31358 (InternalEnumerator_1_t5196 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFileMode_t1925_m40892(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFileMode_t1925_m40892_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5196____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5196_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5196, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5196____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5196_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5196, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5196_FieldInfos[] =
{
	&InternalEnumerator_1_t5196____array_0_FieldInfo,
	&InternalEnumerator_1_t5196____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5196____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5196_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31355_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5196____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5196_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31358_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5196_PropertyInfos[] =
{
	&InternalEnumerator_1_t5196____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5196____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5196_InternalEnumerator_1__ctor_m31354_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31354_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31354_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31354/* method */
	, &InternalEnumerator_1_t5196_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5196_InternalEnumerator_1__ctor_m31354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31354_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31355_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31355_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31355/* method */
	, &InternalEnumerator_1_t5196_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31355_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31356_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31356_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31356/* method */
	, &InternalEnumerator_1_t5196_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31356_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31357_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31357_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31357/* method */
	, &InternalEnumerator_1_t5196_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31357_GenericMethod/* genericMethod */

};
extern Il2CppType FileMode_t1925_0_0_0;
extern void* RuntimeInvoker_FileMode_t1925 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31358_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.FileMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31358_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31358/* method */
	, &InternalEnumerator_1_t5196_il2cpp_TypeInfo/* declaring_type */
	, &FileMode_t1925_0_0_0/* return_type */
	, RuntimeInvoker_FileMode_t1925/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31358_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5196_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31354_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31355_MethodInfo,
	&InternalEnumerator_1_Dispose_m31356_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31357_MethodInfo,
	&InternalEnumerator_1_get_Current_m31358_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5196_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31355_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31357_MethodInfo,
	&InternalEnumerator_1_Dispose_m31356_MethodInfo,
	&InternalEnumerator_1_get_Current_m31358_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5196_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7274_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5196_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7274_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5196_0_0_0;
extern Il2CppType InternalEnumerator_1_t5196_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5196_GenericClass;
TypeInfo InternalEnumerator_1_t5196_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5196_MethodInfos/* methods */
	, InternalEnumerator_1_t5196_PropertyInfos/* properties */
	, InternalEnumerator_1_t5196_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5196_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5196_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5196_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5196_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5196_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5196_1_0_0/* this_arg */
	, InternalEnumerator_1_t5196_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5196_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5196)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9318_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileMode>
extern MethodInfo ICollection_1_get_Count_m51992_MethodInfo;
static PropertyInfo ICollection_1_t9318____Count_PropertyInfo = 
{
	&ICollection_1_t9318_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m51992_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m51993_MethodInfo;
static PropertyInfo ICollection_1_t9318____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9318_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m51993_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9318_PropertyInfos[] =
{
	&ICollection_1_t9318____Count_PropertyInfo,
	&ICollection_1_t9318____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m51992_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileMode>::get_Count()
MethodInfo ICollection_1_get_Count_m51992_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9318_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m51992_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m51993_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m51993_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9318_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m51993_GenericMethod/* genericMethod */

};
extern Il2CppType FileMode_t1925_0_0_0;
extern Il2CppType FileMode_t1925_0_0_0;
static ParameterInfo ICollection_1_t9318_ICollection_1_Add_m51994_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileMode_t1925_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m51994_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::Add(T)
MethodInfo ICollection_1_Add_m51994_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9318_ICollection_1_Add_m51994_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m51994_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m51995_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::Clear()
MethodInfo ICollection_1_Clear_m51995_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m51995_GenericMethod/* genericMethod */

};
extern Il2CppType FileMode_t1925_0_0_0;
static ParameterInfo ICollection_1_t9318_ICollection_1_Contains_m51996_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileMode_t1925_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m51996_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::Contains(T)
MethodInfo ICollection_1_Contains_m51996_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9318_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9318_ICollection_1_Contains_m51996_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m51996_GenericMethod/* genericMethod */

};
extern Il2CppType FileModeU5BU5D_t5505_0_0_0;
extern Il2CppType FileModeU5BU5D_t5505_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9318_ICollection_1_CopyTo_m51997_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FileModeU5BU5D_t5505_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m51997_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m51997_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9318_ICollection_1_CopyTo_m51997_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m51997_GenericMethod/* genericMethod */

};
extern Il2CppType FileMode_t1925_0_0_0;
static ParameterInfo ICollection_1_t9318_ICollection_1_Remove_m51998_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileMode_t1925_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m51998_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::Remove(T)
MethodInfo ICollection_1_Remove_m51998_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9318_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9318_ICollection_1_Remove_m51998_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m51998_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9318_MethodInfos[] =
{
	&ICollection_1_get_Count_m51992_MethodInfo,
	&ICollection_1_get_IsReadOnly_m51993_MethodInfo,
	&ICollection_1_Add_m51994_MethodInfo,
	&ICollection_1_Clear_m51995_MethodInfo,
	&ICollection_1_Contains_m51996_MethodInfo,
	&ICollection_1_CopyTo_m51997_MethodInfo,
	&ICollection_1_Remove_m51998_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9320_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9318_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9320_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9318_0_0_0;
extern Il2CppType ICollection_1_t9318_1_0_0;
struct ICollection_1_t9318;
extern Il2CppGenericClass ICollection_1_t9318_GenericClass;
TypeInfo ICollection_1_t9318_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9318_MethodInfos/* methods */
	, ICollection_1_t9318_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9318_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9318_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9318_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9318_0_0_0/* byval_arg */
	, &ICollection_1_t9318_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9318_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileMode>
extern Il2CppType IEnumerator_1_t7274_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m51999_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m51999_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9320_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7274_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m51999_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9320_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m51999_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9320_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9320_0_0_0;
extern Il2CppType IEnumerable_1_t9320_1_0_0;
struct IEnumerable_1_t9320;
extern Il2CppGenericClass IEnumerable_1_t9320_GenericClass;
TypeInfo IEnumerable_1_t9320_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9320_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9320_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9320_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9320_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9320_0_0_0/* byval_arg */
	, &IEnumerable_1_t9320_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9320_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9319_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.FileMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.FileMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileMode>
extern MethodInfo IList_1_get_Item_m52000_MethodInfo;
extern MethodInfo IList_1_set_Item_m52001_MethodInfo;
static PropertyInfo IList_1_t9319____Item_PropertyInfo = 
{
	&IList_1_t9319_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52000_MethodInfo/* get */
	, &IList_1_set_Item_m52001_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9319_PropertyInfos[] =
{
	&IList_1_t9319____Item_PropertyInfo,
	NULL
};
extern Il2CppType FileMode_t1925_0_0_0;
static ParameterInfo IList_1_t9319_IList_1_IndexOf_m52002_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileMode_t1925_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52002_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.FileMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52002_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9319_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9319_IList_1_IndexOf_m52002_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52002_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FileMode_t1925_0_0_0;
static ParameterInfo IList_1_t9319_IList_1_Insert_m52003_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FileMode_t1925_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52003_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52003_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9319_IList_1_Insert_m52003_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52003_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9319_IList_1_RemoveAt_m52004_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52004_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52004_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9319_IList_1_RemoveAt_m52004_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52004_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9319_IList_1_get_Item_m52000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType FileMode_t1925_0_0_0;
extern void* RuntimeInvoker_FileMode_t1925_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52000_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.FileMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52000_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9319_il2cpp_TypeInfo/* declaring_type */
	, &FileMode_t1925_0_0_0/* return_type */
	, RuntimeInvoker_FileMode_t1925_Int32_t123/* invoker_method */
	, IList_1_t9319_IList_1_get_Item_m52000_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52000_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FileMode_t1925_0_0_0;
static ParameterInfo IList_1_t9319_IList_1_set_Item_m52001_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FileMode_t1925_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52001_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52001_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9319_IList_1_set_Item_m52001_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52001_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9319_MethodInfos[] =
{
	&IList_1_IndexOf_m52002_MethodInfo,
	&IList_1_Insert_m52003_MethodInfo,
	&IList_1_RemoveAt_m52004_MethodInfo,
	&IList_1_get_Item_m52000_MethodInfo,
	&IList_1_set_Item_m52001_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9319_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9318_il2cpp_TypeInfo,
	&IEnumerable_1_t9320_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9319_0_0_0;
extern Il2CppType IList_1_t9319_1_0_0;
struct IList_1_t9319;
extern Il2CppGenericClass IList_1_t9319_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9319_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9319_MethodInfos/* methods */
	, IList_1_t9319_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9319_il2cpp_TypeInfo/* element_class */
	, IList_1_t9319_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9319_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9319_0_0_0/* byval_arg */
	, &IList_1_t9319_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9319_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7276_il2cpp_TypeInfo;

// System.IO.FileOptions
#include "mscorlib_System_IO_FileOptions.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.FileOptions>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileOptions>
extern MethodInfo IEnumerator_1_get_Current_m52005_MethodInfo;
static PropertyInfo IEnumerator_1_t7276____Current_PropertyInfo = 
{
	&IEnumerator_1_t7276_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52005_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7276_PropertyInfos[] =
{
	&IEnumerator_1_t7276____Current_PropertyInfo,
	NULL
};
extern Il2CppType FileOptions_t1927_0_0_0;
extern void* RuntimeInvoker_FileOptions_t1927 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52005_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.FileOptions>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52005_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7276_il2cpp_TypeInfo/* declaring_type */
	, &FileOptions_t1927_0_0_0/* return_type */
	, RuntimeInvoker_FileOptions_t1927/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52005_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7276_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52005_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7276_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7276_0_0_0;
extern Il2CppType IEnumerator_1_t7276_1_0_0;
struct IEnumerator_1_t7276;
extern Il2CppGenericClass IEnumerator_1_t7276_GenericClass;
TypeInfo IEnumerator_1_t7276_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7276_MethodInfos/* methods */
	, IEnumerator_1_t7276_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7276_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7276_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7276_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7276_0_0_0/* byval_arg */
	, &IEnumerator_1_t7276_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7276_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.FileOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_653.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5197_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.FileOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_653MethodDeclarations.h"

extern TypeInfo FileOptions_t1927_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31363_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFileOptions_t1927_m40903_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.FileOptions>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.FileOptions>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFileOptions_t1927_m40903 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.FileOptions>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31359_MethodInfo;
 void InternalEnumerator_1__ctor_m31359 (InternalEnumerator_1_t5197 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.FileOptions>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31360_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31360 (InternalEnumerator_1_t5197 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31363(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31363_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FileOptions_t1927_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.FileOptions>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31361_MethodInfo;
 void InternalEnumerator_1_Dispose_m31361 (InternalEnumerator_1_t5197 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileOptions>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31362_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31362 (InternalEnumerator_1_t5197 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.FileOptions>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31363 (InternalEnumerator_1_t5197 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFileOptions_t1927_m40903(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFileOptions_t1927_m40903_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileOptions>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5197____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5197_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5197, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5197____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5197_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5197, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5197_FieldInfos[] =
{
	&InternalEnumerator_1_t5197____array_0_FieldInfo,
	&InternalEnumerator_1_t5197____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5197____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5197_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31360_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5197____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5197_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31363_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5197_PropertyInfos[] =
{
	&InternalEnumerator_1_t5197____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5197____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5197_InternalEnumerator_1__ctor_m31359_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31359_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileOptions>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31359_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31359/* method */
	, &InternalEnumerator_1_t5197_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5197_InternalEnumerator_1__ctor_m31359_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31359_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31360_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileOptions>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31360_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31360/* method */
	, &InternalEnumerator_1_t5197_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31360_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31361_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileOptions>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31361_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31361/* method */
	, &InternalEnumerator_1_t5197_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31361_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31362_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileOptions>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31362_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31362/* method */
	, &InternalEnumerator_1_t5197_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31362_GenericMethod/* genericMethod */

};
extern Il2CppType FileOptions_t1927_0_0_0;
extern void* RuntimeInvoker_FileOptions_t1927 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31363_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.FileOptions>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31363_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31363/* method */
	, &InternalEnumerator_1_t5197_il2cpp_TypeInfo/* declaring_type */
	, &FileOptions_t1927_0_0_0/* return_type */
	, RuntimeInvoker_FileOptions_t1927/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31363_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5197_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31359_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31360_MethodInfo,
	&InternalEnumerator_1_Dispose_m31361_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31362_MethodInfo,
	&InternalEnumerator_1_get_Current_m31363_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5197_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31360_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31362_MethodInfo,
	&InternalEnumerator_1_Dispose_m31361_MethodInfo,
	&InternalEnumerator_1_get_Current_m31363_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5197_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7276_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5197_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7276_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5197_0_0_0;
extern Il2CppType InternalEnumerator_1_t5197_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5197_GenericClass;
TypeInfo InternalEnumerator_1_t5197_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5197_MethodInfos/* methods */
	, InternalEnumerator_1_t5197_PropertyInfos/* properties */
	, InternalEnumerator_1_t5197_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5197_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5197_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5197_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5197_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5197_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5197_1_0_0/* this_arg */
	, InternalEnumerator_1_t5197_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5197_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5197)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9321_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileOptions>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileOptions>
extern MethodInfo ICollection_1_get_Count_m52006_MethodInfo;
static PropertyInfo ICollection_1_t9321____Count_PropertyInfo = 
{
	&ICollection_1_t9321_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52006_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52007_MethodInfo;
static PropertyInfo ICollection_1_t9321____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9321_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52007_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9321_PropertyInfos[] =
{
	&ICollection_1_t9321____Count_PropertyInfo,
	&ICollection_1_t9321____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52006_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileOptions>::get_Count()
MethodInfo ICollection_1_get_Count_m52006_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9321_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52006_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52007_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52007_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9321_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52007_GenericMethod/* genericMethod */

};
extern Il2CppType FileOptions_t1927_0_0_0;
extern Il2CppType FileOptions_t1927_0_0_0;
static ParameterInfo ICollection_1_t9321_ICollection_1_Add_m52008_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1927_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52008_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Add(T)
MethodInfo ICollection_1_Add_m52008_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9321_ICollection_1_Add_m52008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52008_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52009_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Clear()
MethodInfo ICollection_1_Clear_m52009_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52009_GenericMethod/* genericMethod */

};
extern Il2CppType FileOptions_t1927_0_0_0;
static ParameterInfo ICollection_1_t9321_ICollection_1_Contains_m52010_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1927_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52010_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Contains(T)
MethodInfo ICollection_1_Contains_m52010_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9321_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9321_ICollection_1_Contains_m52010_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52010_GenericMethod/* genericMethod */

};
extern Il2CppType FileOptionsU5BU5D_t5506_0_0_0;
extern Il2CppType FileOptionsU5BU5D_t5506_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9321_ICollection_1_CopyTo_m52011_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FileOptionsU5BU5D_t5506_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52011_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52011_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9321_ICollection_1_CopyTo_m52011_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52011_GenericMethod/* genericMethod */

};
extern Il2CppType FileOptions_t1927_0_0_0;
static ParameterInfo ICollection_1_t9321_ICollection_1_Remove_m52012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1927_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52012_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Remove(T)
MethodInfo ICollection_1_Remove_m52012_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9321_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9321_ICollection_1_Remove_m52012_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52012_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9321_MethodInfos[] =
{
	&ICollection_1_get_Count_m52006_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52007_MethodInfo,
	&ICollection_1_Add_m52008_MethodInfo,
	&ICollection_1_Clear_m52009_MethodInfo,
	&ICollection_1_Contains_m52010_MethodInfo,
	&ICollection_1_CopyTo_m52011_MethodInfo,
	&ICollection_1_Remove_m52012_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9323_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9321_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9323_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9321_0_0_0;
extern Il2CppType ICollection_1_t9321_1_0_0;
struct ICollection_1_t9321;
extern Il2CppGenericClass ICollection_1_t9321_GenericClass;
TypeInfo ICollection_1_t9321_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9321_MethodInfos/* methods */
	, ICollection_1_t9321_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9321_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9321_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9321_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9321_0_0_0/* byval_arg */
	, &ICollection_1_t9321_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9321_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileOptions>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileOptions>
extern Il2CppType IEnumerator_1_t7276_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52013_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileOptions>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52013_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9323_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7276_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52013_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9323_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52013_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9323_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9323_0_0_0;
extern Il2CppType IEnumerable_1_t9323_1_0_0;
struct IEnumerable_1_t9323;
extern Il2CppGenericClass IEnumerable_1_t9323_GenericClass;
TypeInfo IEnumerable_1_t9323_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9323_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9323_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9323_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9323_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9323_0_0_0/* byval_arg */
	, &IEnumerable_1_t9323_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9323_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9322_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.FileOptions>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.FileOptions>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileOptions>
extern MethodInfo IList_1_get_Item_m52014_MethodInfo;
extern MethodInfo IList_1_set_Item_m52015_MethodInfo;
static PropertyInfo IList_1_t9322____Item_PropertyInfo = 
{
	&IList_1_t9322_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52014_MethodInfo/* get */
	, &IList_1_set_Item_m52015_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9322_PropertyInfos[] =
{
	&IList_1_t9322____Item_PropertyInfo,
	NULL
};
extern Il2CppType FileOptions_t1927_0_0_0;
static ParameterInfo IList_1_t9322_IList_1_IndexOf_m52016_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1927_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52016_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.FileOptions>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52016_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9322_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9322_IList_1_IndexOf_m52016_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52016_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FileOptions_t1927_0_0_0;
static ParameterInfo IList_1_t9322_IList_1_Insert_m52017_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1927_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52017_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52017_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9322_IList_1_Insert_m52017_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52017_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9322_IList_1_RemoveAt_m52018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52018_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52018_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9322_IList_1_RemoveAt_m52018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52018_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9322_IList_1_get_Item_m52014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType FileOptions_t1927_0_0_0;
extern void* RuntimeInvoker_FileOptions_t1927_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52014_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.FileOptions>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52014_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9322_il2cpp_TypeInfo/* declaring_type */
	, &FileOptions_t1927_0_0_0/* return_type */
	, RuntimeInvoker_FileOptions_t1927_Int32_t123/* invoker_method */
	, IList_1_t9322_IList_1_get_Item_m52014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52014_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FileOptions_t1927_0_0_0;
static ParameterInfo IList_1_t9322_IList_1_set_Item_m52015_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1927_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52015_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52015_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9322_IList_1_set_Item_m52015_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52015_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9322_MethodInfos[] =
{
	&IList_1_IndexOf_m52016_MethodInfo,
	&IList_1_Insert_m52017_MethodInfo,
	&IList_1_RemoveAt_m52018_MethodInfo,
	&IList_1_get_Item_m52014_MethodInfo,
	&IList_1_set_Item_m52015_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9322_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9321_il2cpp_TypeInfo,
	&IEnumerable_1_t9323_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9322_0_0_0;
extern Il2CppType IList_1_t9322_1_0_0;
struct IList_1_t9322;
extern Il2CppGenericClass IList_1_t9322_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9322_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9322_MethodInfos/* methods */
	, IList_1_t9322_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9322_il2cpp_TypeInfo/* element_class */
	, IList_1_t9322_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9322_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9322_0_0_0/* byval_arg */
	, &IList_1_t9322_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9322_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7278_il2cpp_TypeInfo;

// System.IO.FileShare
#include "mscorlib_System_IO_FileShare.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.FileShare>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileShare>
extern MethodInfo IEnumerator_1_get_Current_m52019_MethodInfo;
static PropertyInfo IEnumerator_1_t7278____Current_PropertyInfo = 
{
	&IEnumerator_1_t7278_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52019_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7278_PropertyInfos[] =
{
	&IEnumerator_1_t7278____Current_PropertyInfo,
	NULL
};
extern Il2CppType FileShare_t1928_0_0_0;
extern void* RuntimeInvoker_FileShare_t1928 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52019_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.FileShare>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52019_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7278_il2cpp_TypeInfo/* declaring_type */
	, &FileShare_t1928_0_0_0/* return_type */
	, RuntimeInvoker_FileShare_t1928/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52019_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7278_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52019_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7278_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7278_0_0_0;
extern Il2CppType IEnumerator_1_t7278_1_0_0;
struct IEnumerator_1_t7278;
extern Il2CppGenericClass IEnumerator_1_t7278_GenericClass;
TypeInfo IEnumerator_1_t7278_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7278_MethodInfos/* methods */
	, IEnumerator_1_t7278_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7278_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7278_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7278_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7278_0_0_0/* byval_arg */
	, &IEnumerator_1_t7278_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7278_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.FileShare>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_654.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5198_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.FileShare>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_654MethodDeclarations.h"

extern TypeInfo FileShare_t1928_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31368_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFileShare_t1928_m40914_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.FileShare>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.FileShare>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFileShare_t1928_m40914 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.FileShare>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31364_MethodInfo;
 void InternalEnumerator_1__ctor_m31364 (InternalEnumerator_1_t5198 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.FileShare>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31365_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31365 (InternalEnumerator_1_t5198 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31368(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31368_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FileShare_t1928_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.FileShare>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31366_MethodInfo;
 void InternalEnumerator_1_Dispose_m31366 (InternalEnumerator_1_t5198 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileShare>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31367_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31367 (InternalEnumerator_1_t5198 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.FileShare>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31368 (InternalEnumerator_1_t5198 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFileShare_t1928_m40914(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFileShare_t1928_m40914_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileShare>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5198____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5198_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5198, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5198____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5198_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5198, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5198_FieldInfos[] =
{
	&InternalEnumerator_1_t5198____array_0_FieldInfo,
	&InternalEnumerator_1_t5198____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5198____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5198_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31365_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5198____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5198_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31368_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5198_PropertyInfos[] =
{
	&InternalEnumerator_1_t5198____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5198____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5198_InternalEnumerator_1__ctor_m31364_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31364_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileShare>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31364_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31364/* method */
	, &InternalEnumerator_1_t5198_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5198_InternalEnumerator_1__ctor_m31364_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31364_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31365_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileShare>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31365_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31365/* method */
	, &InternalEnumerator_1_t5198_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31365_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31366_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileShare>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31366_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31366/* method */
	, &InternalEnumerator_1_t5198_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31366_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31367_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileShare>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31367_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31367/* method */
	, &InternalEnumerator_1_t5198_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31367_GenericMethod/* genericMethod */

};
extern Il2CppType FileShare_t1928_0_0_0;
extern void* RuntimeInvoker_FileShare_t1928 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31368_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.FileShare>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31368_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31368/* method */
	, &InternalEnumerator_1_t5198_il2cpp_TypeInfo/* declaring_type */
	, &FileShare_t1928_0_0_0/* return_type */
	, RuntimeInvoker_FileShare_t1928/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31368_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5198_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31364_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31365_MethodInfo,
	&InternalEnumerator_1_Dispose_m31366_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31367_MethodInfo,
	&InternalEnumerator_1_get_Current_m31368_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5198_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31365_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31367_MethodInfo,
	&InternalEnumerator_1_Dispose_m31366_MethodInfo,
	&InternalEnumerator_1_get_Current_m31368_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5198_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7278_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5198_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7278_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5198_0_0_0;
extern Il2CppType InternalEnumerator_1_t5198_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5198_GenericClass;
TypeInfo InternalEnumerator_1_t5198_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5198_MethodInfos/* methods */
	, InternalEnumerator_1_t5198_PropertyInfos/* properties */
	, InternalEnumerator_1_t5198_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5198_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5198_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5198_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5198_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5198_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5198_1_0_0/* this_arg */
	, InternalEnumerator_1_t5198_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5198_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5198)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9324_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileShare>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileShare>
extern MethodInfo ICollection_1_get_Count_m52020_MethodInfo;
static PropertyInfo ICollection_1_t9324____Count_PropertyInfo = 
{
	&ICollection_1_t9324_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52020_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52021_MethodInfo;
static PropertyInfo ICollection_1_t9324____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9324_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52021_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9324_PropertyInfos[] =
{
	&ICollection_1_t9324____Count_PropertyInfo,
	&ICollection_1_t9324____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52020_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileShare>::get_Count()
MethodInfo ICollection_1_get_Count_m52020_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9324_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52020_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52021_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52021_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9324_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52021_GenericMethod/* genericMethod */

};
extern Il2CppType FileShare_t1928_0_0_0;
extern Il2CppType FileShare_t1928_0_0_0;
static ParameterInfo ICollection_1_t9324_ICollection_1_Add_m52022_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileShare_t1928_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52022_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::Add(T)
MethodInfo ICollection_1_Add_m52022_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9324_ICollection_1_Add_m52022_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52022_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52023_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::Clear()
MethodInfo ICollection_1_Clear_m52023_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52023_GenericMethod/* genericMethod */

};
extern Il2CppType FileShare_t1928_0_0_0;
static ParameterInfo ICollection_1_t9324_ICollection_1_Contains_m52024_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileShare_t1928_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52024_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::Contains(T)
MethodInfo ICollection_1_Contains_m52024_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9324_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9324_ICollection_1_Contains_m52024_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52024_GenericMethod/* genericMethod */

};
extern Il2CppType FileShareU5BU5D_t5507_0_0_0;
extern Il2CppType FileShareU5BU5D_t5507_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9324_ICollection_1_CopyTo_m52025_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FileShareU5BU5D_t5507_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52025_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52025_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9324_ICollection_1_CopyTo_m52025_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52025_GenericMethod/* genericMethod */

};
extern Il2CppType FileShare_t1928_0_0_0;
static ParameterInfo ICollection_1_t9324_ICollection_1_Remove_m52026_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileShare_t1928_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52026_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::Remove(T)
MethodInfo ICollection_1_Remove_m52026_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9324_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9324_ICollection_1_Remove_m52026_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52026_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9324_MethodInfos[] =
{
	&ICollection_1_get_Count_m52020_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52021_MethodInfo,
	&ICollection_1_Add_m52022_MethodInfo,
	&ICollection_1_Clear_m52023_MethodInfo,
	&ICollection_1_Contains_m52024_MethodInfo,
	&ICollection_1_CopyTo_m52025_MethodInfo,
	&ICollection_1_Remove_m52026_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9326_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9324_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9326_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9324_0_0_0;
extern Il2CppType ICollection_1_t9324_1_0_0;
struct ICollection_1_t9324;
extern Il2CppGenericClass ICollection_1_t9324_GenericClass;
TypeInfo ICollection_1_t9324_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9324_MethodInfos/* methods */
	, ICollection_1_t9324_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9324_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9324_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9324_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9324_0_0_0/* byval_arg */
	, &ICollection_1_t9324_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9324_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileShare>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileShare>
extern Il2CppType IEnumerator_1_t7278_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52027_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileShare>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52027_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9326_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7278_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52027_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9326_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52027_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9326_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9326_0_0_0;
extern Il2CppType IEnumerable_1_t9326_1_0_0;
struct IEnumerable_1_t9326;
extern Il2CppGenericClass IEnumerable_1_t9326_GenericClass;
TypeInfo IEnumerable_1_t9326_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9326_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9326_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9326_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9326_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9326_0_0_0/* byval_arg */
	, &IEnumerable_1_t9326_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9326_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9325_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.FileShare>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.FileShare>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileShare>
extern MethodInfo IList_1_get_Item_m52028_MethodInfo;
extern MethodInfo IList_1_set_Item_m52029_MethodInfo;
static PropertyInfo IList_1_t9325____Item_PropertyInfo = 
{
	&IList_1_t9325_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52028_MethodInfo/* get */
	, &IList_1_set_Item_m52029_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9325_PropertyInfos[] =
{
	&IList_1_t9325____Item_PropertyInfo,
	NULL
};
extern Il2CppType FileShare_t1928_0_0_0;
static ParameterInfo IList_1_t9325_IList_1_IndexOf_m52030_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileShare_t1928_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52030_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.FileShare>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52030_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9325_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9325_IList_1_IndexOf_m52030_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52030_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FileShare_t1928_0_0_0;
static ParameterInfo IList_1_t9325_IList_1_Insert_m52031_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FileShare_t1928_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52031_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52031_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9325_IList_1_Insert_m52031_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52031_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9325_IList_1_RemoveAt_m52032_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52032_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52032_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9325_IList_1_RemoveAt_m52032_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52032_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9325_IList_1_get_Item_m52028_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType FileShare_t1928_0_0_0;
extern void* RuntimeInvoker_FileShare_t1928_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52028_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.FileShare>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52028_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9325_il2cpp_TypeInfo/* declaring_type */
	, &FileShare_t1928_0_0_0/* return_type */
	, RuntimeInvoker_FileShare_t1928_Int32_t123/* invoker_method */
	, IList_1_t9325_IList_1_get_Item_m52028_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52028_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FileShare_t1928_0_0_0;
static ParameterInfo IList_1_t9325_IList_1_set_Item_m52029_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FileShare_t1928_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52029_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52029_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9325_IList_1_set_Item_m52029_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52029_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9325_MethodInfos[] =
{
	&IList_1_IndexOf_m52030_MethodInfo,
	&IList_1_Insert_m52031_MethodInfo,
	&IList_1_RemoveAt_m52032_MethodInfo,
	&IList_1_get_Item_m52028_MethodInfo,
	&IList_1_set_Item_m52029_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9325_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9324_il2cpp_TypeInfo,
	&IEnumerable_1_t9326_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9325_0_0_0;
extern Il2CppType IList_1_t9325_1_0_0;
struct IList_1_t9325;
extern Il2CppGenericClass IList_1_t9325_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9325_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9325_MethodInfos/* methods */
	, IList_1_t9325_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9325_il2cpp_TypeInfo/* element_class */
	, IList_1_t9325_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9325_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9325_0_0_0/* byval_arg */
	, &IList_1_t9325_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9325_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7280_il2cpp_TypeInfo;

// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.MonoFileType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.MonoFileType>
extern MethodInfo IEnumerator_1_get_Current_m52033_MethodInfo;
static PropertyInfo IEnumerator_1_t7280____Current_PropertyInfo = 
{
	&IEnumerator_1_t7280_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52033_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7280_PropertyInfos[] =
{
	&IEnumerator_1_t7280____Current_PropertyInfo,
	NULL
};
extern Il2CppType MonoFileType_t1933_0_0_0;
extern void* RuntimeInvoker_MonoFileType_t1933 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52033_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.MonoFileType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52033_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7280_il2cpp_TypeInfo/* declaring_type */
	, &MonoFileType_t1933_0_0_0/* return_type */
	, RuntimeInvoker_MonoFileType_t1933/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52033_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7280_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52033_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7280_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7280_0_0_0;
extern Il2CppType IEnumerator_1_t7280_1_0_0;
struct IEnumerator_1_t7280;
extern Il2CppGenericClass IEnumerator_1_t7280_GenericClass;
TypeInfo IEnumerator_1_t7280_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7280_MethodInfos/* methods */
	, IEnumerator_1_t7280_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7280_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7280_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7280_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7280_0_0_0/* byval_arg */
	, &IEnumerator_1_t7280_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7280_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.MonoFileType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_655.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5199_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.MonoFileType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_655MethodDeclarations.h"

extern TypeInfo MonoFileType_t1933_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31373_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMonoFileType_t1933_m40925_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.MonoFileType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.MonoFileType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisMonoFileType_t1933_m40925 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.MonoFileType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31369_MethodInfo;
 void InternalEnumerator_1__ctor_m31369 (InternalEnumerator_1_t5199 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.MonoFileType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31370_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31370 (InternalEnumerator_1_t5199 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31373(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31373_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&MonoFileType_t1933_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoFileType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31371_MethodInfo;
 void InternalEnumerator_1_Dispose_m31371 (InternalEnumerator_1_t5199 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.MonoFileType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31372_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31372 (InternalEnumerator_1_t5199 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.MonoFileType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31373 (InternalEnumerator_1_t5199 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisMonoFileType_t1933_m40925(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisMonoFileType_t1933_m40925_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.MonoFileType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5199____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5199_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5199, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5199____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5199_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5199, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5199_FieldInfos[] =
{
	&InternalEnumerator_1_t5199____array_0_FieldInfo,
	&InternalEnumerator_1_t5199____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5199____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5199_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31370_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5199____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5199_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31373_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5199_PropertyInfos[] =
{
	&InternalEnumerator_1_t5199____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5199____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5199_InternalEnumerator_1__ctor_m31369_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31369_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoFileType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31369_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31369/* method */
	, &InternalEnumerator_1_t5199_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5199_InternalEnumerator_1__ctor_m31369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31369_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31370_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.MonoFileType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31370_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31370/* method */
	, &InternalEnumerator_1_t5199_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31370_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31371_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoFileType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31371_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31371/* method */
	, &InternalEnumerator_1_t5199_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31371_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31372_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.MonoFileType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31372_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31372/* method */
	, &InternalEnumerator_1_t5199_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31372_GenericMethod/* genericMethod */

};
extern Il2CppType MonoFileType_t1933_0_0_0;
extern void* RuntimeInvoker_MonoFileType_t1933 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31373_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.MonoFileType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31373_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31373/* method */
	, &InternalEnumerator_1_t5199_il2cpp_TypeInfo/* declaring_type */
	, &MonoFileType_t1933_0_0_0/* return_type */
	, RuntimeInvoker_MonoFileType_t1933/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31373_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5199_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31369_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31370_MethodInfo,
	&InternalEnumerator_1_Dispose_m31371_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31372_MethodInfo,
	&InternalEnumerator_1_get_Current_m31373_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5199_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31370_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31372_MethodInfo,
	&InternalEnumerator_1_Dispose_m31371_MethodInfo,
	&InternalEnumerator_1_get_Current_m31373_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5199_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7280_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5199_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7280_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5199_0_0_0;
extern Il2CppType InternalEnumerator_1_t5199_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5199_GenericClass;
TypeInfo InternalEnumerator_1_t5199_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5199_MethodInfos/* methods */
	, InternalEnumerator_1_t5199_PropertyInfos/* properties */
	, InternalEnumerator_1_t5199_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5199_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5199_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5199_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5199_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5199_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5199_1_0_0/* this_arg */
	, InternalEnumerator_1_t5199_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5199_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5199)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9327_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.MonoFileType>
extern MethodInfo ICollection_1_get_Count_m52034_MethodInfo;
static PropertyInfo ICollection_1_t9327____Count_PropertyInfo = 
{
	&ICollection_1_t9327_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52034_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52035_MethodInfo;
static PropertyInfo ICollection_1_t9327____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9327_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52035_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9327_PropertyInfos[] =
{
	&ICollection_1_t9327____Count_PropertyInfo,
	&ICollection_1_t9327____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52034_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::get_Count()
MethodInfo ICollection_1_get_Count_m52034_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9327_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52034_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52035_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52035_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9327_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52035_GenericMethod/* genericMethod */

};
extern Il2CppType MonoFileType_t1933_0_0_0;
extern Il2CppType MonoFileType_t1933_0_0_0;
static ParameterInfo ICollection_1_t9327_ICollection_1_Add_m52036_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1933_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52036_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Add(T)
MethodInfo ICollection_1_Add_m52036_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9327_ICollection_1_Add_m52036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52036_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52037_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Clear()
MethodInfo ICollection_1_Clear_m52037_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52037_GenericMethod/* genericMethod */

};
extern Il2CppType MonoFileType_t1933_0_0_0;
static ParameterInfo ICollection_1_t9327_ICollection_1_Contains_m52038_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1933_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52038_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Contains(T)
MethodInfo ICollection_1_Contains_m52038_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9327_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9327_ICollection_1_Contains_m52038_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52038_GenericMethod/* genericMethod */

};
extern Il2CppType MonoFileTypeU5BU5D_t5508_0_0_0;
extern Il2CppType MonoFileTypeU5BU5D_t5508_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9327_ICollection_1_CopyTo_m52039_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MonoFileTypeU5BU5D_t5508_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52039_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52039_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9327_ICollection_1_CopyTo_m52039_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52039_GenericMethod/* genericMethod */

};
extern Il2CppType MonoFileType_t1933_0_0_0;
static ParameterInfo ICollection_1_t9327_ICollection_1_Remove_m52040_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1933_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52040_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Remove(T)
MethodInfo ICollection_1_Remove_m52040_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9327_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9327_ICollection_1_Remove_m52040_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52040_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9327_MethodInfos[] =
{
	&ICollection_1_get_Count_m52034_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52035_MethodInfo,
	&ICollection_1_Add_m52036_MethodInfo,
	&ICollection_1_Clear_m52037_MethodInfo,
	&ICollection_1_Contains_m52038_MethodInfo,
	&ICollection_1_CopyTo_m52039_MethodInfo,
	&ICollection_1_Remove_m52040_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9329_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9327_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9329_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9327_0_0_0;
extern Il2CppType ICollection_1_t9327_1_0_0;
struct ICollection_1_t9327;
extern Il2CppGenericClass ICollection_1_t9327_GenericClass;
TypeInfo ICollection_1_t9327_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9327_MethodInfos/* methods */
	, ICollection_1_t9327_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9327_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9327_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9327_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9327_0_0_0/* byval_arg */
	, &ICollection_1_t9327_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9327_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.MonoFileType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.MonoFileType>
extern Il2CppType IEnumerator_1_t7280_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52041_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.MonoFileType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52041_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9329_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7280_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52041_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9329_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52041_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9329_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9329_0_0_0;
extern Il2CppType IEnumerable_1_t9329_1_0_0;
struct IEnumerable_1_t9329;
extern Il2CppGenericClass IEnumerable_1_t9329_GenericClass;
TypeInfo IEnumerable_1_t9329_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9329_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9329_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9329_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9329_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9329_0_0_0/* byval_arg */
	, &IEnumerable_1_t9329_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9329_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9328_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.MonoFileType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.MonoFileType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.MonoFileType>
extern MethodInfo IList_1_get_Item_m52042_MethodInfo;
extern MethodInfo IList_1_set_Item_m52043_MethodInfo;
static PropertyInfo IList_1_t9328____Item_PropertyInfo = 
{
	&IList_1_t9328_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52042_MethodInfo/* get */
	, &IList_1_set_Item_m52043_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9328_PropertyInfos[] =
{
	&IList_1_t9328____Item_PropertyInfo,
	NULL
};
extern Il2CppType MonoFileType_t1933_0_0_0;
static ParameterInfo IList_1_t9328_IList_1_IndexOf_m52044_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1933_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52044_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.MonoFileType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52044_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9328_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9328_IList_1_IndexOf_m52044_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52044_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MonoFileType_t1933_0_0_0;
static ParameterInfo IList_1_t9328_IList_1_Insert_m52045_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1933_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52045_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52045_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9328_IList_1_Insert_m52045_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52045_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9328_IList_1_RemoveAt_m52046_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52046_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52046_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9328_IList_1_RemoveAt_m52046_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52046_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9328_IList_1_get_Item_m52042_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MonoFileType_t1933_0_0_0;
extern void* RuntimeInvoker_MonoFileType_t1933_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52042_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.MonoFileType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52042_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9328_il2cpp_TypeInfo/* declaring_type */
	, &MonoFileType_t1933_0_0_0/* return_type */
	, RuntimeInvoker_MonoFileType_t1933_Int32_t123/* invoker_method */
	, IList_1_t9328_IList_1_get_Item_m52042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52042_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MonoFileType_t1933_0_0_0;
static ParameterInfo IList_1_t9328_IList_1_set_Item_m52043_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1933_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52043_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52043_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9328_IList_1_set_Item_m52043_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52043_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9328_MethodInfos[] =
{
	&IList_1_IndexOf_m52044_MethodInfo,
	&IList_1_Insert_m52045_MethodInfo,
	&IList_1_RemoveAt_m52046_MethodInfo,
	&IList_1_get_Item_m52042_MethodInfo,
	&IList_1_set_Item_m52043_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9328_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9327_il2cpp_TypeInfo,
	&IEnumerable_1_t9329_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9328_0_0_0;
extern Il2CppType IList_1_t9328_1_0_0;
struct IList_1_t9328;
extern Il2CppGenericClass IList_1_t9328_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9328_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9328_MethodInfos/* methods */
	, IList_1_t9328_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9328_il2cpp_TypeInfo/* element_class */
	, IList_1_t9328_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9328_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9328_0_0_0/* byval_arg */
	, &IList_1_t9328_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9328_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7281_il2cpp_TypeInfo;

// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.MonoIOError>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.MonoIOError>
extern MethodInfo IEnumerator_1_get_Current_m52047_MethodInfo;
static PropertyInfo IEnumerator_1_t7281____Current_PropertyInfo = 
{
	&IEnumerator_1_t7281_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52047_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7281_PropertyInfos[] =
{
	&IEnumerator_1_t7281____Current_PropertyInfo,
	NULL
};
extern Il2CppType MonoIOError_t1935_0_0_0;
extern void* RuntimeInvoker_MonoIOError_t1935 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52047_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.MonoIOError>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52047_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7281_il2cpp_TypeInfo/* declaring_type */
	, &MonoIOError_t1935_0_0_0/* return_type */
	, RuntimeInvoker_MonoIOError_t1935/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52047_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7281_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52047_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7281_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7281_0_0_0;
extern Il2CppType IEnumerator_1_t7281_1_0_0;
struct IEnumerator_1_t7281;
extern Il2CppGenericClass IEnumerator_1_t7281_GenericClass;
TypeInfo IEnumerator_1_t7281_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7281_MethodInfos/* methods */
	, IEnumerator_1_t7281_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7281_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7281_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7281_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7281_0_0_0/* byval_arg */
	, &IEnumerator_1_t7281_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7281_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.MonoIOError>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_656.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5200_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.MonoIOError>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_656MethodDeclarations.h"

extern TypeInfo MonoIOError_t1935_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31378_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMonoIOError_t1935_m40936_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.MonoIOError>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.MonoIOError>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisMonoIOError_t1935_m40936 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.MonoIOError>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31374_MethodInfo;
 void InternalEnumerator_1__ctor_m31374 (InternalEnumerator_1_t5200 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.MonoIOError>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31375_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31375 (InternalEnumerator_1_t5200 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31378(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31378_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&MonoIOError_t1935_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoIOError>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31376_MethodInfo;
 void InternalEnumerator_1_Dispose_m31376 (InternalEnumerator_1_t5200 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.MonoIOError>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31377_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31377 (InternalEnumerator_1_t5200 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.MonoIOError>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31378 (InternalEnumerator_1_t5200 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisMonoIOError_t1935_m40936(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisMonoIOError_t1935_m40936_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.MonoIOError>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5200____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5200_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5200, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5200____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5200_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5200, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5200_FieldInfos[] =
{
	&InternalEnumerator_1_t5200____array_0_FieldInfo,
	&InternalEnumerator_1_t5200____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5200____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5200_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31375_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5200____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5200_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31378_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5200_PropertyInfos[] =
{
	&InternalEnumerator_1_t5200____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5200____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5200_InternalEnumerator_1__ctor_m31374_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31374_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoIOError>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31374_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31374/* method */
	, &InternalEnumerator_1_t5200_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5200_InternalEnumerator_1__ctor_m31374_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31374_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31375_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.MonoIOError>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31375_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31375/* method */
	, &InternalEnumerator_1_t5200_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31375_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31376_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoIOError>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31376_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31376/* method */
	, &InternalEnumerator_1_t5200_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31376_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31377_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.MonoIOError>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31377_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31377/* method */
	, &InternalEnumerator_1_t5200_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31377_GenericMethod/* genericMethod */

};
extern Il2CppType MonoIOError_t1935_0_0_0;
extern void* RuntimeInvoker_MonoIOError_t1935 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31378_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.MonoIOError>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31378_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31378/* method */
	, &InternalEnumerator_1_t5200_il2cpp_TypeInfo/* declaring_type */
	, &MonoIOError_t1935_0_0_0/* return_type */
	, RuntimeInvoker_MonoIOError_t1935/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31378_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5200_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31374_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31375_MethodInfo,
	&InternalEnumerator_1_Dispose_m31376_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31377_MethodInfo,
	&InternalEnumerator_1_get_Current_m31378_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5200_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31375_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31377_MethodInfo,
	&InternalEnumerator_1_Dispose_m31376_MethodInfo,
	&InternalEnumerator_1_get_Current_m31378_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5200_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7281_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5200_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7281_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5200_0_0_0;
extern Il2CppType InternalEnumerator_1_t5200_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5200_GenericClass;
TypeInfo InternalEnumerator_1_t5200_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5200_MethodInfos/* methods */
	, InternalEnumerator_1_t5200_PropertyInfos/* properties */
	, InternalEnumerator_1_t5200_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5200_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5200_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5200_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5200_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5200_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5200_1_0_0/* this_arg */
	, InternalEnumerator_1_t5200_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5200_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5200)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9330_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.MonoIOError>
extern MethodInfo ICollection_1_get_Count_m52048_MethodInfo;
static PropertyInfo ICollection_1_t9330____Count_PropertyInfo = 
{
	&ICollection_1_t9330_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52048_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52049_MethodInfo;
static PropertyInfo ICollection_1_t9330____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9330_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52049_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9330_PropertyInfos[] =
{
	&ICollection_1_t9330____Count_PropertyInfo,
	&ICollection_1_t9330____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52048_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::get_Count()
MethodInfo ICollection_1_get_Count_m52048_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9330_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52048_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52049_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52049_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9330_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52049_GenericMethod/* genericMethod */

};
extern Il2CppType MonoIOError_t1935_0_0_0;
extern Il2CppType MonoIOError_t1935_0_0_0;
static ParameterInfo ICollection_1_t9330_ICollection_1_Add_m52050_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1935_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52050_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Add(T)
MethodInfo ICollection_1_Add_m52050_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9330_ICollection_1_Add_m52050_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52050_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52051_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Clear()
MethodInfo ICollection_1_Clear_m52051_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52051_GenericMethod/* genericMethod */

};
extern Il2CppType MonoIOError_t1935_0_0_0;
static ParameterInfo ICollection_1_t9330_ICollection_1_Contains_m52052_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1935_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52052_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Contains(T)
MethodInfo ICollection_1_Contains_m52052_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9330_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9330_ICollection_1_Contains_m52052_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52052_GenericMethod/* genericMethod */

};
extern Il2CppType MonoIOErrorU5BU5D_t5509_0_0_0;
extern Il2CppType MonoIOErrorU5BU5D_t5509_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9330_ICollection_1_CopyTo_m52053_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MonoIOErrorU5BU5D_t5509_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52053_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52053_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9330_ICollection_1_CopyTo_m52053_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52053_GenericMethod/* genericMethod */

};
extern Il2CppType MonoIOError_t1935_0_0_0;
static ParameterInfo ICollection_1_t9330_ICollection_1_Remove_m52054_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1935_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52054_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Remove(T)
MethodInfo ICollection_1_Remove_m52054_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9330_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9330_ICollection_1_Remove_m52054_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52054_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9330_MethodInfos[] =
{
	&ICollection_1_get_Count_m52048_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52049_MethodInfo,
	&ICollection_1_Add_m52050_MethodInfo,
	&ICollection_1_Clear_m52051_MethodInfo,
	&ICollection_1_Contains_m52052_MethodInfo,
	&ICollection_1_CopyTo_m52053_MethodInfo,
	&ICollection_1_Remove_m52054_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9332_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9330_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9332_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9330_0_0_0;
extern Il2CppType ICollection_1_t9330_1_0_0;
struct ICollection_1_t9330;
extern Il2CppGenericClass ICollection_1_t9330_GenericClass;
TypeInfo ICollection_1_t9330_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9330_MethodInfos/* methods */
	, ICollection_1_t9330_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9330_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9330_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9330_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9330_0_0_0/* byval_arg */
	, &ICollection_1_t9330_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9330_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.MonoIOError>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.MonoIOError>
extern Il2CppType IEnumerator_1_t7281_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52055_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.MonoIOError>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52055_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9332_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7281_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52055_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9332_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52055_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9332_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9332_0_0_0;
extern Il2CppType IEnumerable_1_t9332_1_0_0;
struct IEnumerable_1_t9332;
extern Il2CppGenericClass IEnumerable_1_t9332_GenericClass;
TypeInfo IEnumerable_1_t9332_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9332_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9332_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9332_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9332_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9332_0_0_0/* byval_arg */
	, &IEnumerable_1_t9332_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9332_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9331_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.MonoIOError>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.MonoIOError>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.MonoIOError>
extern MethodInfo IList_1_get_Item_m52056_MethodInfo;
extern MethodInfo IList_1_set_Item_m52057_MethodInfo;
static PropertyInfo IList_1_t9331____Item_PropertyInfo = 
{
	&IList_1_t9331_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52056_MethodInfo/* get */
	, &IList_1_set_Item_m52057_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9331_PropertyInfos[] =
{
	&IList_1_t9331____Item_PropertyInfo,
	NULL
};
extern Il2CppType MonoIOError_t1935_0_0_0;
static ParameterInfo IList_1_t9331_IList_1_IndexOf_m52058_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1935_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52058_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.MonoIOError>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52058_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9331_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9331_IList_1_IndexOf_m52058_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52058_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MonoIOError_t1935_0_0_0;
static ParameterInfo IList_1_t9331_IList_1_Insert_m52059_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1935_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52059_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52059_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9331_IList_1_Insert_m52059_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52059_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9331_IList_1_RemoveAt_m52060_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52060_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52060_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9331_IList_1_RemoveAt_m52060_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52060_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9331_IList_1_get_Item_m52056_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType MonoIOError_t1935_0_0_0;
extern void* RuntimeInvoker_MonoIOError_t1935_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52056_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.MonoIOError>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52056_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9331_il2cpp_TypeInfo/* declaring_type */
	, &MonoIOError_t1935_0_0_0/* return_type */
	, RuntimeInvoker_MonoIOError_t1935_Int32_t123/* invoker_method */
	, IList_1_t9331_IList_1_get_Item_m52056_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52056_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType MonoIOError_t1935_0_0_0;
static ParameterInfo IList_1_t9331_IList_1_set_Item_m52057_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1935_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52057_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52057_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9331_IList_1_set_Item_m52057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52057_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9331_MethodInfos[] =
{
	&IList_1_IndexOf_m52058_MethodInfo,
	&IList_1_Insert_m52059_MethodInfo,
	&IList_1_RemoveAt_m52060_MethodInfo,
	&IList_1_get_Item_m52056_MethodInfo,
	&IList_1_set_Item_m52057_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9331_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9330_il2cpp_TypeInfo,
	&IEnumerable_1_t9332_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9331_0_0_0;
extern Il2CppType IList_1_t9331_1_0_0;
struct IList_1_t9331;
extern Il2CppGenericClass IList_1_t9331_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9331_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9331_MethodInfos/* methods */
	, IList_1_t9331_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9331_il2cpp_TypeInfo/* element_class */
	, IList_1_t9331_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9331_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9331_0_0_0/* byval_arg */
	, &IList_1_t9331_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9331_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7283_il2cpp_TypeInfo;

// System.IO.SeekOrigin
#include "mscorlib_System_IO_SeekOrigin.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.SeekOrigin>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.SeekOrigin>
extern MethodInfo IEnumerator_1_get_Current_m52061_MethodInfo;
static PropertyInfo IEnumerator_1_t7283____Current_PropertyInfo = 
{
	&IEnumerator_1_t7283_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52061_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7283_PropertyInfos[] =
{
	&IEnumerator_1_t7283____Current_PropertyInfo,
	NULL
};
extern Il2CppType SeekOrigin_t1736_0_0_0;
extern void* RuntimeInvoker_SeekOrigin_t1736 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52061_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.SeekOrigin>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52061_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7283_il2cpp_TypeInfo/* declaring_type */
	, &SeekOrigin_t1736_0_0_0/* return_type */
	, RuntimeInvoker_SeekOrigin_t1736/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52061_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7283_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52061_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7283_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7283_0_0_0;
extern Il2CppType IEnumerator_1_t7283_1_0_0;
struct IEnumerator_1_t7283;
extern Il2CppGenericClass IEnumerator_1_t7283_GenericClass;
TypeInfo IEnumerator_1_t7283_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7283_MethodInfos/* methods */
	, IEnumerator_1_t7283_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7283_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7283_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7283_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7283_0_0_0/* byval_arg */
	, &IEnumerator_1_t7283_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7283_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.SeekOrigin>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_657.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5201_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.SeekOrigin>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_657MethodDeclarations.h"

extern TypeInfo SeekOrigin_t1736_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31383_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSeekOrigin_t1736_m40947_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.SeekOrigin>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.SeekOrigin>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSeekOrigin_t1736_m40947 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m31379_MethodInfo;
 void InternalEnumerator_1__ctor_m31379 (InternalEnumerator_1_t5201 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31380_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31380 (InternalEnumerator_1_t5201 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m31383(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m31383_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&SeekOrigin_t1736_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m31381_MethodInfo;
 void InternalEnumerator_1_Dispose_m31381 (InternalEnumerator_1_t5201 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m31382_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m31382 (InternalEnumerator_1_t5201 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31383 (InternalEnumerator_1_t5201 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisSeekOrigin_t1736_m40947(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisSeekOrigin_t1736_m40947_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.SeekOrigin>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5201____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5201_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5201, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5201____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5201_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5201, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5201_FieldInfos[] =
{
	&InternalEnumerator_1_t5201____array_0_FieldInfo,
	&InternalEnumerator_1_t5201____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5201____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5201_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31380_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5201____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5201_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31383_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5201_PropertyInfos[] =
{
	&InternalEnumerator_1_t5201____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5201____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5201_InternalEnumerator_1__ctor_m31379_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31379_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31379_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m31379/* method */
	, &InternalEnumerator_1_t5201_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5201_InternalEnumerator_1__ctor_m31379_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31379_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31380_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31380_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31380/* method */
	, &InternalEnumerator_1_t5201_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31380_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31381_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31381_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m31381/* method */
	, &InternalEnumerator_1_t5201_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31381_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31382_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31382_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m31382/* method */
	, &InternalEnumerator_1_t5201_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31382_GenericMethod/* genericMethod */

};
extern Il2CppType SeekOrigin_t1736_0_0_0;
extern void* RuntimeInvoker_SeekOrigin_t1736 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31383_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31383_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m31383/* method */
	, &InternalEnumerator_1_t5201_il2cpp_TypeInfo/* declaring_type */
	, &SeekOrigin_t1736_0_0_0/* return_type */
	, RuntimeInvoker_SeekOrigin_t1736/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31383_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5201_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31379_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31380_MethodInfo,
	&InternalEnumerator_1_Dispose_m31381_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31382_MethodInfo,
	&InternalEnumerator_1_get_Current_m31383_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5201_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31380_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31382_MethodInfo,
	&InternalEnumerator_1_Dispose_m31381_MethodInfo,
	&InternalEnumerator_1_get_Current_m31383_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5201_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7283_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5201_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7283_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5201_0_0_0;
extern Il2CppType InternalEnumerator_1_t5201_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5201_GenericClass;
TypeInfo InternalEnumerator_1_t5201_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5201_MethodInfos/* methods */
	, InternalEnumerator_1_t5201_PropertyInfos/* properties */
	, InternalEnumerator_1_t5201_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5201_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5201_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5201_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5201_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5201_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5201_1_0_0/* this_arg */
	, InternalEnumerator_1_t5201_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5201_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5201)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9333_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>
extern MethodInfo ICollection_1_get_Count_m52062_MethodInfo;
static PropertyInfo ICollection_1_t9333____Count_PropertyInfo = 
{
	&ICollection_1_t9333_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52062_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52063_MethodInfo;
static PropertyInfo ICollection_1_t9333____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9333_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52063_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9333_PropertyInfos[] =
{
	&ICollection_1_t9333____Count_PropertyInfo,
	&ICollection_1_t9333____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52062_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::get_Count()
MethodInfo ICollection_1_get_Count_m52062_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9333_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52062_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52063_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52063_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9333_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52063_GenericMethod/* genericMethod */

};
extern Il2CppType SeekOrigin_t1736_0_0_0;
extern Il2CppType SeekOrigin_t1736_0_0_0;
static ParameterInfo ICollection_1_t9333_ICollection_1_Add_m52064_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1736_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52064_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Add(T)
MethodInfo ICollection_1_Add_m52064_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t9333_ICollection_1_Add_m52064_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52064_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52065_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Clear()
MethodInfo ICollection_1_Clear_m52065_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52065_GenericMethod/* genericMethod */

};
extern Il2CppType SeekOrigin_t1736_0_0_0;
static ParameterInfo ICollection_1_t9333_ICollection_1_Contains_m52066_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1736_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52066_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Contains(T)
MethodInfo ICollection_1_Contains_m52066_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9333_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9333_ICollection_1_Contains_m52066_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52066_GenericMethod/* genericMethod */

};
extern Il2CppType SeekOriginU5BU5D_t5510_0_0_0;
extern Il2CppType SeekOriginU5BU5D_t5510_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9333_ICollection_1_CopyTo_m52067_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SeekOriginU5BU5D_t5510_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52067_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52067_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9333_ICollection_1_CopyTo_m52067_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52067_GenericMethod/* genericMethod */

};
extern Il2CppType SeekOrigin_t1736_0_0_0;
static ParameterInfo ICollection_1_t9333_ICollection_1_Remove_m52068_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1736_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52068_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Remove(T)
MethodInfo ICollection_1_Remove_m52068_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9333_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t9333_ICollection_1_Remove_m52068_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52068_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9333_MethodInfos[] =
{
	&ICollection_1_get_Count_m52062_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52063_MethodInfo,
	&ICollection_1_Add_m52064_MethodInfo,
	&ICollection_1_Clear_m52065_MethodInfo,
	&ICollection_1_Contains_m52066_MethodInfo,
	&ICollection_1_CopyTo_m52067_MethodInfo,
	&ICollection_1_Remove_m52068_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9335_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9333_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9335_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9333_0_0_0;
extern Il2CppType ICollection_1_t9333_1_0_0;
struct ICollection_1_t9333;
extern Il2CppGenericClass ICollection_1_t9333_GenericClass;
TypeInfo ICollection_1_t9333_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9333_MethodInfos/* methods */
	, ICollection_1_t9333_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9333_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9333_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9333_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9333_0_0_0/* byval_arg */
	, &ICollection_1_t9333_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9333_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.SeekOrigin>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.SeekOrigin>
extern Il2CppType IEnumerator_1_t7283_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52069_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.SeekOrigin>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52069_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9335_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7283_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52069_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9335_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52069_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9335_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9335_0_0_0;
extern Il2CppType IEnumerable_1_t9335_1_0_0;
struct IEnumerable_1_t9335;
extern Il2CppGenericClass IEnumerable_1_t9335_GenericClass;
TypeInfo IEnumerable_1_t9335_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9335_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9335_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9335_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9335_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9335_0_0_0/* byval_arg */
	, &IEnumerable_1_t9335_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9335_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9334_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.SeekOrigin>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.SeekOrigin>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.SeekOrigin>
extern MethodInfo IList_1_get_Item_m52070_MethodInfo;
extern MethodInfo IList_1_set_Item_m52071_MethodInfo;
static PropertyInfo IList_1_t9334____Item_PropertyInfo = 
{
	&IList_1_t9334_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52070_MethodInfo/* get */
	, &IList_1_set_Item_m52071_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9334_PropertyInfos[] =
{
	&IList_1_t9334____Item_PropertyInfo,
	NULL
};
extern Il2CppType SeekOrigin_t1736_0_0_0;
static ParameterInfo IList_1_t9334_IList_1_IndexOf_m52072_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1736_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52072_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.SeekOrigin>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52072_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9334_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9334_IList_1_IndexOf_m52072_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52072_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SeekOrigin_t1736_0_0_0;
static ParameterInfo IList_1_t9334_IList_1_Insert_m52073_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1736_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52073_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52073_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9334_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9334_IList_1_Insert_m52073_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52073_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9334_IList_1_RemoveAt_m52074_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52074_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52074_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9334_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9334_IList_1_RemoveAt_m52074_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52074_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9334_IList_1_get_Item_m52070_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType SeekOrigin_t1736_0_0_0;
extern void* RuntimeInvoker_SeekOrigin_t1736_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52070_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.SeekOrigin>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52070_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9334_il2cpp_TypeInfo/* declaring_type */
	, &SeekOrigin_t1736_0_0_0/* return_type */
	, RuntimeInvoker_SeekOrigin_t1736_Int32_t123/* invoker_method */
	, IList_1_t9334_IList_1_get_Item_m52070_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52070_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType SeekOrigin_t1736_0_0_0;
static ParameterInfo IList_1_t9334_IList_1_set_Item_m52071_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1736_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52071_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52071_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9334_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t9334_IList_1_set_Item_m52071_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52071_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9334_MethodInfos[] =
{
	&IList_1_IndexOf_m52072_MethodInfo,
	&IList_1_Insert_m52073_MethodInfo,
	&IList_1_RemoveAt_m52074_MethodInfo,
	&IList_1_get_Item_m52070_MethodInfo,
	&IList_1_set_Item_m52071_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9334_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9333_il2cpp_TypeInfo,
	&IEnumerable_1_t9335_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9334_0_0_0;
extern Il2CppType IList_1_t9334_1_0_0;
struct IList_1_t9334;
extern Il2CppGenericClass IList_1_t9334_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9334_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9334_MethodInfos/* methods */
	, IList_1_t9334_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9334_il2cpp_TypeInfo/* element_class */
	, IList_1_t9334_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9334_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9334_0_0_0/* byval_arg */
	, &IList_1_t9334_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9334_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7285_il2cpp_TypeInfo;

// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilder.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ModuleBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ModuleBuilder>
extern MethodInfo IEnumerator_1_get_Current_m52075_MethodInfo;
static PropertyInfo IEnumerator_1_t7285____Current_PropertyInfo = 
{
	&IEnumerator_1_t7285_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52075_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7285_PropertyInfos[] =
{
	&IEnumerator_1_t7285____Current_PropertyInfo,
	NULL
};
extern Il2CppType ModuleBuilder_t1963_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52075_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ModuleBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52075_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7285_il2cpp_TypeInfo/* declaring_type */
	, &ModuleBuilder_t1963_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52075_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7285_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52075_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7285_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7285_0_0_0;
extern Il2CppType IEnumerator_1_t7285_1_0_0;
struct IEnumerator_1_t7285;
extern Il2CppGenericClass IEnumerator_1_t7285_GenericClass;
TypeInfo IEnumerator_1_t7285_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7285_MethodInfos/* methods */
	, IEnumerator_1_t7285_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7285_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7285_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7285_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7285_0_0_0/* byval_arg */
	, &IEnumerator_1_t7285_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7285_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_658.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5202_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_658MethodDeclarations.h"

extern TypeInfo ModuleBuilder_t1963_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31388_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisModuleBuilder_t1963_m40958_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ModuleBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ModuleBuilder>(System.Int32)
#define Array_InternalArray__get_Item_TisModuleBuilder_t1963_m40958(__this, p0, method) (ModuleBuilder_t1963 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5202____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5202_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5202, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5202____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5202_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5202, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5202_FieldInfos[] =
{
	&InternalEnumerator_1_t5202____array_0_FieldInfo,
	&InternalEnumerator_1_t5202____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31385_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5202____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5202_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31385_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5202____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5202_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31388_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5202_PropertyInfos[] =
{
	&InternalEnumerator_1_t5202____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5202____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5202_InternalEnumerator_1__ctor_m31384_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31384_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31384_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5202_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5202_InternalEnumerator_1__ctor_m31384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31384_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31385_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31385_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5202_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31385_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31386_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31386_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5202_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31386_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31387_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31387_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5202_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31387_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleBuilder_t1963_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31388_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31388_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5202_il2cpp_TypeInfo/* declaring_type */
	, &ModuleBuilder_t1963_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31388_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5202_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31384_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31385_MethodInfo,
	&InternalEnumerator_1_Dispose_m31386_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31387_MethodInfo,
	&InternalEnumerator_1_get_Current_m31388_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31387_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31386_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5202_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31385_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31387_MethodInfo,
	&InternalEnumerator_1_Dispose_m31386_MethodInfo,
	&InternalEnumerator_1_get_Current_m31388_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5202_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7285_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5202_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7285_il2cpp_TypeInfo, 7},
};
extern TypeInfo ModuleBuilder_t1963_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5202_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31388_MethodInfo/* Method Usage */,
	&ModuleBuilder_t1963_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisModuleBuilder_t1963_m40958_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5202_0_0_0;
extern Il2CppType InternalEnumerator_1_t5202_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5202_GenericClass;
TypeInfo InternalEnumerator_1_t5202_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5202_MethodInfos/* methods */
	, InternalEnumerator_1_t5202_PropertyInfos/* properties */
	, InternalEnumerator_1_t5202_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5202_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5202_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5202_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5202_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5202_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5202_1_0_0/* this_arg */
	, InternalEnumerator_1_t5202_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5202_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5202_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5202)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9336_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>
extern MethodInfo ICollection_1_get_Count_m52076_MethodInfo;
static PropertyInfo ICollection_1_t9336____Count_PropertyInfo = 
{
	&ICollection_1_t9336_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52076_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52077_MethodInfo;
static PropertyInfo ICollection_1_t9336____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9336_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52077_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9336_PropertyInfos[] =
{
	&ICollection_1_t9336____Count_PropertyInfo,
	&ICollection_1_t9336____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52076_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m52076_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9336_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52076_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52077_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52077_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9336_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52077_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleBuilder_t1963_0_0_0;
extern Il2CppType ModuleBuilder_t1963_0_0_0;
static ParameterInfo ICollection_1_t9336_ICollection_1_Add_m52078_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1963_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52078_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Add(T)
MethodInfo ICollection_1_Add_m52078_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9336_ICollection_1_Add_m52078_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52078_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52079_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Clear()
MethodInfo ICollection_1_Clear_m52079_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52079_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleBuilder_t1963_0_0_0;
static ParameterInfo ICollection_1_t9336_ICollection_1_Contains_m52080_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1963_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52080_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m52080_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9336_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9336_ICollection_1_Contains_m52080_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52080_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleBuilderU5BU5D_t1949_0_0_0;
extern Il2CppType ModuleBuilderU5BU5D_t1949_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9336_ICollection_1_CopyTo_m52081_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ModuleBuilderU5BU5D_t1949_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52081_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52081_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9336_ICollection_1_CopyTo_m52081_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52081_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleBuilder_t1963_0_0_0;
static ParameterInfo ICollection_1_t9336_ICollection_1_Remove_m52082_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1963_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52082_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m52082_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9336_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9336_ICollection_1_Remove_m52082_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52082_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9336_MethodInfos[] =
{
	&ICollection_1_get_Count_m52076_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52077_MethodInfo,
	&ICollection_1_Add_m52078_MethodInfo,
	&ICollection_1_Clear_m52079_MethodInfo,
	&ICollection_1_Contains_m52080_MethodInfo,
	&ICollection_1_CopyTo_m52081_MethodInfo,
	&ICollection_1_Remove_m52082_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9338_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9336_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9338_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9336_0_0_0;
extern Il2CppType ICollection_1_t9336_1_0_0;
struct ICollection_1_t9336;
extern Il2CppGenericClass ICollection_1_t9336_GenericClass;
TypeInfo ICollection_1_t9336_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9336_MethodInfos/* methods */
	, ICollection_1_t9336_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9336_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9336_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9336_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9336_0_0_0/* byval_arg */
	, &ICollection_1_t9336_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9336_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ModuleBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ModuleBuilder>
extern Il2CppType IEnumerator_1_t7285_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52083_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ModuleBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52083_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9338_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52083_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9338_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52083_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9338_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9338_0_0_0;
extern Il2CppType IEnumerable_1_t9338_1_0_0;
struct IEnumerable_1_t9338;
extern Il2CppGenericClass IEnumerable_1_t9338_GenericClass;
TypeInfo IEnumerable_1_t9338_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9338_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9338_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9338_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9338_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9338_0_0_0/* byval_arg */
	, &IEnumerable_1_t9338_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9338_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9337_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>
extern MethodInfo IList_1_get_Item_m52084_MethodInfo;
extern MethodInfo IList_1_set_Item_m52085_MethodInfo;
static PropertyInfo IList_1_t9337____Item_PropertyInfo = 
{
	&IList_1_t9337_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52084_MethodInfo/* get */
	, &IList_1_set_Item_m52085_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9337_PropertyInfos[] =
{
	&IList_1_t9337____Item_PropertyInfo,
	NULL
};
extern Il2CppType ModuleBuilder_t1963_0_0_0;
static ParameterInfo IList_1_t9337_IList_1_IndexOf_m52086_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1963_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52086_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52086_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9337_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9337_IList_1_IndexOf_m52086_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52086_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ModuleBuilder_t1963_0_0_0;
static ParameterInfo IList_1_t9337_IList_1_Insert_m52087_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1963_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52087_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52087_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9337_IList_1_Insert_m52087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52087_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9337_IList_1_RemoveAt_m52088_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52088_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52088_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9337_IList_1_RemoveAt_m52088_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52088_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9337_IList_1_get_Item_m52084_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ModuleBuilder_t1963_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52084_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52084_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9337_il2cpp_TypeInfo/* declaring_type */
	, &ModuleBuilder_t1963_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9337_IList_1_get_Item_m52084_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52084_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ModuleBuilder_t1963_0_0_0;
static ParameterInfo IList_1_t9337_IList_1_set_Item_m52085_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1963_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52085_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52085_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9337_IList_1_set_Item_m52085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52085_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9337_MethodInfos[] =
{
	&IList_1_IndexOf_m52086_MethodInfo,
	&IList_1_Insert_m52087_MethodInfo,
	&IList_1_RemoveAt_m52088_MethodInfo,
	&IList_1_get_Item_m52084_MethodInfo,
	&IList_1_set_Item_m52085_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9337_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9336_il2cpp_TypeInfo,
	&IEnumerable_1_t9338_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9337_0_0_0;
extern Il2CppType IList_1_t9337_1_0_0;
struct IList_1_t9337;
extern Il2CppGenericClass IList_1_t9337_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9337_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9337_MethodInfos/* methods */
	, IList_1_t9337_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9337_il2cpp_TypeInfo/* element_class */
	, IList_1_t9337_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9337_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9337_0_0_0/* byval_arg */
	, &IList_1_t9337_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9337_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9339_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>
extern MethodInfo ICollection_1_get_Count_m52089_MethodInfo;
static PropertyInfo ICollection_1_t9339____Count_PropertyInfo = 
{
	&ICollection_1_t9339_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52089_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52090_MethodInfo;
static PropertyInfo ICollection_1_t9339____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9339_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52090_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9339_PropertyInfos[] =
{
	&ICollection_1_t9339____Count_PropertyInfo,
	&ICollection_1_t9339____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52089_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m52089_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9339_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52089_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52090_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52090_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52090_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleBuilder_t2657_0_0_0;
extern Il2CppType _ModuleBuilder_t2657_0_0_0;
static ParameterInfo ICollection_1_t9339_ICollection_1_Add_m52091_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2657_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52091_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Add(T)
MethodInfo ICollection_1_Add_m52091_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9339_ICollection_1_Add_m52091_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52091_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52092_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Clear()
MethodInfo ICollection_1_Clear_m52092_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52092_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleBuilder_t2657_0_0_0;
static ParameterInfo ICollection_1_t9339_ICollection_1_Contains_m52093_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2657_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52093_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m52093_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9339_ICollection_1_Contains_m52093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52093_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleBuilderU5BU5D_t5511_0_0_0;
extern Il2CppType _ModuleBuilderU5BU5D_t5511_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9339_ICollection_1_CopyTo_m52094_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilderU5BU5D_t5511_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52094_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52094_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9339_ICollection_1_CopyTo_m52094_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52094_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleBuilder_t2657_0_0_0;
static ParameterInfo ICollection_1_t9339_ICollection_1_Remove_m52095_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2657_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52095_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m52095_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9339_ICollection_1_Remove_m52095_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52095_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9339_MethodInfos[] =
{
	&ICollection_1_get_Count_m52089_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52090_MethodInfo,
	&ICollection_1_Add_m52091_MethodInfo,
	&ICollection_1_Clear_m52092_MethodInfo,
	&ICollection_1_Contains_m52093_MethodInfo,
	&ICollection_1_CopyTo_m52094_MethodInfo,
	&ICollection_1_Remove_m52095_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9341_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9339_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9341_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9339_0_0_0;
extern Il2CppType ICollection_1_t9339_1_0_0;
struct ICollection_1_t9339;
extern Il2CppGenericClass ICollection_1_t9339_GenericClass;
TypeInfo ICollection_1_t9339_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9339_MethodInfos/* methods */
	, ICollection_1_t9339_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9339_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9339_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9339_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9339_0_0_0/* byval_arg */
	, &ICollection_1_t9339_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9339_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ModuleBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ModuleBuilder>
extern Il2CppType IEnumerator_1_t7287_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52096_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ModuleBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52096_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9341_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7287_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52096_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9341_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52096_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9341_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9341_0_0_0;
extern Il2CppType IEnumerable_1_t9341_1_0_0;
struct IEnumerable_1_t9341;
extern Il2CppGenericClass IEnumerable_1_t9341_GenericClass;
TypeInfo IEnumerable_1_t9341_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9341_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9341_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9341_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9341_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9341_0_0_0/* byval_arg */
	, &IEnumerable_1_t9341_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9341_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7287_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>
extern MethodInfo IEnumerator_1_get_Current_m52097_MethodInfo;
static PropertyInfo IEnumerator_1_t7287____Current_PropertyInfo = 
{
	&IEnumerator_1_t7287_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52097_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7287_PropertyInfos[] =
{
	&IEnumerator_1_t7287____Current_PropertyInfo,
	NULL
};
extern Il2CppType _ModuleBuilder_t2657_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52097_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52097_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7287_il2cpp_TypeInfo/* declaring_type */
	, &_ModuleBuilder_t2657_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52097_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7287_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52097_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7287_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7287_0_0_0;
extern Il2CppType IEnumerator_1_t7287_1_0_0;
struct IEnumerator_1_t7287;
extern Il2CppGenericClass IEnumerator_1_t7287_GenericClass;
TypeInfo IEnumerator_1_t7287_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7287_MethodInfos/* methods */
	, IEnumerator_1_t7287_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7287_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7287_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7287_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7287_0_0_0/* byval_arg */
	, &IEnumerator_1_t7287_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7287_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_659.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5203_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_659MethodDeclarations.h"

extern TypeInfo _ModuleBuilder_t2657_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31393_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_ModuleBuilder_t2657_m40969_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._ModuleBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._ModuleBuilder>(System.Int32)
#define Array_InternalArray__get_Item_Tis_ModuleBuilder_t2657_m40969(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5203____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5203_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5203, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5203____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5203_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5203, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5203_FieldInfos[] =
{
	&InternalEnumerator_1_t5203____array_0_FieldInfo,
	&InternalEnumerator_1_t5203____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31390_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5203____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5203_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31390_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5203____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5203_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31393_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5203_PropertyInfos[] =
{
	&InternalEnumerator_1_t5203____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5203____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5203_InternalEnumerator_1__ctor_m31389_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31389_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31389_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5203_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5203_InternalEnumerator_1__ctor_m31389_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31389_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31390_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31390_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5203_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31390_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31391_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31391_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5203_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31391_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31392_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31392_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5203_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31392_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleBuilder_t2657_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31393_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31393_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5203_il2cpp_TypeInfo/* declaring_type */
	, &_ModuleBuilder_t2657_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31393_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5203_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31389_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31390_MethodInfo,
	&InternalEnumerator_1_Dispose_m31391_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31392_MethodInfo,
	&InternalEnumerator_1_get_Current_m31393_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31392_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31391_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5203_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31390_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31392_MethodInfo,
	&InternalEnumerator_1_Dispose_m31391_MethodInfo,
	&InternalEnumerator_1_get_Current_m31393_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5203_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7287_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5203_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7287_il2cpp_TypeInfo, 7},
};
extern TypeInfo _ModuleBuilder_t2657_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5203_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31393_MethodInfo/* Method Usage */,
	&_ModuleBuilder_t2657_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_ModuleBuilder_t2657_m40969_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5203_0_0_0;
extern Il2CppType InternalEnumerator_1_t5203_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5203_GenericClass;
TypeInfo InternalEnumerator_1_t5203_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5203_MethodInfos/* methods */
	, InternalEnumerator_1_t5203_PropertyInfos/* properties */
	, InternalEnumerator_1_t5203_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5203_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5203_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5203_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5203_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5203_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5203_1_0_0/* this_arg */
	, InternalEnumerator_1_t5203_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5203_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5203_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5203)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9340_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>
extern MethodInfo IList_1_get_Item_m52098_MethodInfo;
extern MethodInfo IList_1_set_Item_m52099_MethodInfo;
static PropertyInfo IList_1_t9340____Item_PropertyInfo = 
{
	&IList_1_t9340_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52098_MethodInfo/* get */
	, &IList_1_set_Item_m52099_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9340_PropertyInfos[] =
{
	&IList_1_t9340____Item_PropertyInfo,
	NULL
};
extern Il2CppType _ModuleBuilder_t2657_0_0_0;
static ParameterInfo IList_1_t9340_IList_1_IndexOf_m52100_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2657_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52100_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52100_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9340_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9340_IList_1_IndexOf_m52100_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52100_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _ModuleBuilder_t2657_0_0_0;
static ParameterInfo IList_1_t9340_IList_1_Insert_m52101_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2657_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52101_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52101_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9340_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9340_IList_1_Insert_m52101_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52101_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9340_IList_1_RemoveAt_m52102_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52102_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52102_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9340_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9340_IList_1_RemoveAt_m52102_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52102_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9340_IList_1_get_Item_m52098_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType _ModuleBuilder_t2657_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52098_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52098_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9340_il2cpp_TypeInfo/* declaring_type */
	, &_ModuleBuilder_t2657_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9340_IList_1_get_Item_m52098_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52098_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _ModuleBuilder_t2657_0_0_0;
static ParameterInfo IList_1_t9340_IList_1_set_Item_m52099_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2657_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52099_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52099_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9340_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9340_IList_1_set_Item_m52099_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52099_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9340_MethodInfos[] =
{
	&IList_1_IndexOf_m52100_MethodInfo,
	&IList_1_Insert_m52101_MethodInfo,
	&IList_1_RemoveAt_m52102_MethodInfo,
	&IList_1_get_Item_m52098_MethodInfo,
	&IList_1_set_Item_m52099_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9340_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9339_il2cpp_TypeInfo,
	&IEnumerable_1_t9341_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9340_0_0_0;
extern Il2CppType IList_1_t9340_1_0_0;
struct IList_1_t9340;
extern Il2CppGenericClass IList_1_t9340_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9340_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9340_MethodInfos/* methods */
	, IList_1_t9340_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9340_il2cpp_TypeInfo/* element_class */
	, IList_1_t9340_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9340_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9340_0_0_0/* byval_arg */
	, &IList_1_t9340_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9340_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9342_il2cpp_TypeInfo;

// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Module>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Module>
extern MethodInfo ICollection_1_get_Count_m52103_MethodInfo;
static PropertyInfo ICollection_1_t9342____Count_PropertyInfo = 
{
	&ICollection_1_t9342_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52103_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52104_MethodInfo;
static PropertyInfo ICollection_1_t9342____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9342_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52104_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9342_PropertyInfos[] =
{
	&ICollection_1_t9342____Count_PropertyInfo,
	&ICollection_1_t9342____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52103_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Module>::get_Count()
MethodInfo ICollection_1_get_Count_m52103_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9342_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52103_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52104_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52104_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9342_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52104_GenericMethod/* genericMethod */

};
extern Il2CppType Module_t1755_0_0_0;
extern Il2CppType Module_t1755_0_0_0;
static ParameterInfo ICollection_1_t9342_ICollection_1_Add_m52105_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Module_t1755_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52105_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::Add(T)
MethodInfo ICollection_1_Add_m52105_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9342_ICollection_1_Add_m52105_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52105_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52106_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::Clear()
MethodInfo ICollection_1_Clear_m52106_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52106_GenericMethod/* genericMethod */

};
extern Il2CppType Module_t1755_0_0_0;
static ParameterInfo ICollection_1_t9342_ICollection_1_Contains_m52107_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Module_t1755_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52107_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::Contains(T)
MethodInfo ICollection_1_Contains_m52107_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9342_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9342_ICollection_1_Contains_m52107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52107_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleU5BU5D_t1951_0_0_0;
extern Il2CppType ModuleU5BU5D_t1951_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9342_ICollection_1_CopyTo_m52108_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ModuleU5BU5D_t1951_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52108_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52108_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9342_ICollection_1_CopyTo_m52108_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52108_GenericMethod/* genericMethod */

};
extern Il2CppType Module_t1755_0_0_0;
static ParameterInfo ICollection_1_t9342_ICollection_1_Remove_m52109_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Module_t1755_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52109_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::Remove(T)
MethodInfo ICollection_1_Remove_m52109_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9342_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9342_ICollection_1_Remove_m52109_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52109_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9342_MethodInfos[] =
{
	&ICollection_1_get_Count_m52103_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52104_MethodInfo,
	&ICollection_1_Add_m52105_MethodInfo,
	&ICollection_1_Clear_m52106_MethodInfo,
	&ICollection_1_Contains_m52107_MethodInfo,
	&ICollection_1_CopyTo_m52108_MethodInfo,
	&ICollection_1_Remove_m52109_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9344_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9342_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9344_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9342_0_0_0;
extern Il2CppType ICollection_1_t9342_1_0_0;
struct ICollection_1_t9342;
extern Il2CppGenericClass ICollection_1_t9342_GenericClass;
TypeInfo ICollection_1_t9342_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9342_MethodInfos/* methods */
	, ICollection_1_t9342_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9342_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9342_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9342_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9342_0_0_0/* byval_arg */
	, &ICollection_1_t9342_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9342_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Module>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Module>
extern Il2CppType IEnumerator_1_t7288_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52110_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Module>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52110_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9344_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7288_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52110_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9344_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52110_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9344_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9344_0_0_0;
extern Il2CppType IEnumerable_1_t9344_1_0_0;
struct IEnumerable_1_t9344;
extern Il2CppGenericClass IEnumerable_1_t9344_GenericClass;
TypeInfo IEnumerable_1_t9344_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9344_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9344_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9344_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9344_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9344_0_0_0/* byval_arg */
	, &IEnumerable_1_t9344_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9344_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7288_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Reflection.Module>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Module>
extern MethodInfo IEnumerator_1_get_Current_m52111_MethodInfo;
static PropertyInfo IEnumerator_1_t7288____Current_PropertyInfo = 
{
	&IEnumerator_1_t7288_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52111_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7288_PropertyInfos[] =
{
	&IEnumerator_1_t7288____Current_PropertyInfo,
	NULL
};
extern Il2CppType Module_t1755_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52111_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.Module>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52111_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7288_il2cpp_TypeInfo/* declaring_type */
	, &Module_t1755_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52111_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7288_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52111_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7288_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7288_0_0_0;
extern Il2CppType IEnumerator_1_t7288_1_0_0;
struct IEnumerator_1_t7288;
extern Il2CppGenericClass IEnumerator_1_t7288_GenericClass;
TypeInfo IEnumerator_1_t7288_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7288_MethodInfos/* methods */
	, IEnumerator_1_t7288_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7288_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7288_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7288_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7288_0_0_0/* byval_arg */
	, &IEnumerator_1_t7288_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7288_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.Module>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_660.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5204_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.Module>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_660MethodDeclarations.h"

extern TypeInfo Module_t1755_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31398_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisModule_t1755_m40980_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.Module>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Module>(System.Int32)
#define Array_InternalArray__get_Item_TisModule_t1755_m40980(__this, p0, method) (Module_t1755 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.Module>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Module>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Module>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Module>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.Module>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Module>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5204____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5204_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5204, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5204____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5204_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5204, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5204_FieldInfos[] =
{
	&InternalEnumerator_1_t5204____array_0_FieldInfo,
	&InternalEnumerator_1_t5204____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31395_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5204____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5204_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31395_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5204____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5204_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31398_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5204_PropertyInfos[] =
{
	&InternalEnumerator_1_t5204____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5204____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5204_InternalEnumerator_1__ctor_m31394_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31394_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Module>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31394_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5204_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5204_InternalEnumerator_1__ctor_m31394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31394_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31395_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Module>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31395_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5204_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31395_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31396_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Module>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31396_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5204_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31396_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31397_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Module>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31397_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5204_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31397_GenericMethod/* genericMethod */

};
extern Il2CppType Module_t1755_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31398_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.Module>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31398_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5204_il2cpp_TypeInfo/* declaring_type */
	, &Module_t1755_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31398_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5204_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31394_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31395_MethodInfo,
	&InternalEnumerator_1_Dispose_m31396_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31397_MethodInfo,
	&InternalEnumerator_1_get_Current_m31398_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31397_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31396_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5204_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31395_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31397_MethodInfo,
	&InternalEnumerator_1_Dispose_m31396_MethodInfo,
	&InternalEnumerator_1_get_Current_m31398_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5204_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7288_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5204_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7288_il2cpp_TypeInfo, 7},
};
extern TypeInfo Module_t1755_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5204_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31398_MethodInfo/* Method Usage */,
	&Module_t1755_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisModule_t1755_m40980_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5204_0_0_0;
extern Il2CppType InternalEnumerator_1_t5204_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5204_GenericClass;
TypeInfo InternalEnumerator_1_t5204_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5204_MethodInfos/* methods */
	, InternalEnumerator_1_t5204_PropertyInfos/* properties */
	, InternalEnumerator_1_t5204_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5204_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5204_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5204_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5204_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5204_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5204_1_0_0/* this_arg */
	, InternalEnumerator_1_t5204_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5204_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5204_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5204)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9343_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Module>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.Module>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Module>
extern MethodInfo IList_1_get_Item_m52112_MethodInfo;
extern MethodInfo IList_1_set_Item_m52113_MethodInfo;
static PropertyInfo IList_1_t9343____Item_PropertyInfo = 
{
	&IList_1_t9343_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52112_MethodInfo/* get */
	, &IList_1_set_Item_m52113_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9343_PropertyInfos[] =
{
	&IList_1_t9343____Item_PropertyInfo,
	NULL
};
extern Il2CppType Module_t1755_0_0_0;
static ParameterInfo IList_1_t9343_IList_1_IndexOf_m52114_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Module_t1755_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52114_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Module>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52114_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9343_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9343_IList_1_IndexOf_m52114_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52114_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Module_t1755_0_0_0;
static ParameterInfo IList_1_t9343_IList_1_Insert_m52115_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Module_t1755_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52115_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52115_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9343_IList_1_Insert_m52115_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52115_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9343_IList_1_RemoveAt_m52116_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52116_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52116_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9343_IList_1_RemoveAt_m52116_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52116_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9343_IList_1_get_Item_m52112_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Module_t1755_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52112_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.Module>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52112_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9343_il2cpp_TypeInfo/* declaring_type */
	, &Module_t1755_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9343_IList_1_get_Item_m52112_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52112_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Module_t1755_0_0_0;
static ParameterInfo IList_1_t9343_IList_1_set_Item_m52113_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Module_t1755_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52113_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52113_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9343_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9343_IList_1_set_Item_m52113_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52113_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9343_MethodInfos[] =
{
	&IList_1_IndexOf_m52114_MethodInfo,
	&IList_1_Insert_m52115_MethodInfo,
	&IList_1_RemoveAt_m52116_MethodInfo,
	&IList_1_get_Item_m52112_MethodInfo,
	&IList_1_set_Item_m52113_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9343_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9342_il2cpp_TypeInfo,
	&IEnumerable_1_t9344_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9343_0_0_0;
extern Il2CppType IList_1_t9343_1_0_0;
struct IList_1_t9343;
extern Il2CppGenericClass IList_1_t9343_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9343_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9343_MethodInfos/* methods */
	, IList_1_t9343_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9343_il2cpp_TypeInfo/* element_class */
	, IList_1_t9343_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9343_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9343_0_0_0/* byval_arg */
	, &IList_1_t9343_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9343_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t9345_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>
extern MethodInfo ICollection_1_get_Count_m52117_MethodInfo;
static PropertyInfo ICollection_1_t9345____Count_PropertyInfo = 
{
	&ICollection_1_t9345_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m52117_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m52118_MethodInfo;
static PropertyInfo ICollection_1_t9345____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t9345_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m52118_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t9345_PropertyInfos[] =
{
	&ICollection_1_t9345____Count_PropertyInfo,
	&ICollection_1_t9345____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m52117_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::get_Count()
MethodInfo ICollection_1_get_Count_m52117_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t9345_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m52117_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m52118_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m52118_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t9345_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m52118_GenericMethod/* genericMethod */

};
extern Il2CppType _Module_t2658_0_0_0;
extern Il2CppType _Module_t2658_0_0_0;
static ParameterInfo ICollection_1_t9345_ICollection_1_Add_m52119_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_Module_t2658_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m52119_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Add(T)
MethodInfo ICollection_1_Add_m52119_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t9345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t9345_ICollection_1_Add_m52119_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m52119_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m52120_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Clear()
MethodInfo ICollection_1_Clear_m52120_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t9345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m52120_GenericMethod/* genericMethod */

};
extern Il2CppType _Module_t2658_0_0_0;
static ParameterInfo ICollection_1_t9345_ICollection_1_Contains_m52121_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_Module_t2658_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m52121_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Contains(T)
MethodInfo ICollection_1_Contains_m52121_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t9345_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9345_ICollection_1_Contains_m52121_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m52121_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleU5BU5D_t5512_0_0_0;
extern Il2CppType _ModuleU5BU5D_t5512_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t9345_ICollection_1_CopyTo_m52122_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleU5BU5D_t5512_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m52122_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m52122_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t9345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t9345_ICollection_1_CopyTo_m52122_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m52122_GenericMethod/* genericMethod */

};
extern Il2CppType _Module_t2658_0_0_0;
static ParameterInfo ICollection_1_t9345_ICollection_1_Remove_m52123_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_Module_t2658_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m52123_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Remove(T)
MethodInfo ICollection_1_Remove_m52123_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t9345_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t9345_ICollection_1_Remove_m52123_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m52123_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t9345_MethodInfos[] =
{
	&ICollection_1_get_Count_m52117_MethodInfo,
	&ICollection_1_get_IsReadOnly_m52118_MethodInfo,
	&ICollection_1_Add_m52119_MethodInfo,
	&ICollection_1_Clear_m52120_MethodInfo,
	&ICollection_1_Contains_m52121_MethodInfo,
	&ICollection_1_CopyTo_m52122_MethodInfo,
	&ICollection_1_Remove_m52123_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t9347_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t9345_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t9347_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t9345_0_0_0;
extern Il2CppType ICollection_1_t9345_1_0_0;
struct ICollection_1_t9345;
extern Il2CppGenericClass ICollection_1_t9345_GenericClass;
TypeInfo ICollection_1_t9345_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t9345_MethodInfos/* methods */
	, ICollection_1_t9345_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t9345_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t9345_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t9345_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t9345_0_0_0/* byval_arg */
	, &ICollection_1_t9345_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t9345_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._Module>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._Module>
extern Il2CppType IEnumerator_1_t7290_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m52124_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._Module>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m52124_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t9347_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m52124_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t9347_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m52124_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t9347_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t9347_0_0_0;
extern Il2CppType IEnumerable_1_t9347_1_0_0;
struct IEnumerable_1_t9347;
extern Il2CppGenericClass IEnumerable_1_t9347_GenericClass;
TypeInfo IEnumerable_1_t9347_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t9347_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t9347_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t9347_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t9347_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t9347_0_0_0/* byval_arg */
	, &IEnumerable_1_t9347_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t9347_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7290_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._Module>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._Module>
extern MethodInfo IEnumerator_1_get_Current_m52125_MethodInfo;
static PropertyInfo IEnumerator_1_t7290____Current_PropertyInfo = 
{
	&IEnumerator_1_t7290_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m52125_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7290_PropertyInfos[] =
{
	&IEnumerator_1_t7290____Current_PropertyInfo,
	NULL
};
extern Il2CppType _Module_t2658_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m52125_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._Module>::get_Current()
MethodInfo IEnumerator_1_get_Current_m52125_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7290_il2cpp_TypeInfo/* declaring_type */
	, &_Module_t2658_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m52125_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7290_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m52125_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7290_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7290_0_0_0;
extern Il2CppType IEnumerator_1_t7290_1_0_0;
struct IEnumerator_1_t7290;
extern Il2CppGenericClass IEnumerator_1_t7290_GenericClass;
TypeInfo IEnumerator_1_t7290_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7290_MethodInfos/* methods */
	, IEnumerator_1_t7290_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7290_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7290_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7290_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7290_0_0_0/* byval_arg */
	, &IEnumerator_1_t7290_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7290_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_661.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5205_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_661MethodDeclarations.h"

extern TypeInfo _Module_t2658_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m31403_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_Module_t2658_m40991_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._Module>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._Module>(System.Int32)
#define Array_InternalArray__get_Item_Tis_Module_t2658_m40991(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5205____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5205_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5205, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t5205____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t5205_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5205, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5205_FieldInfos[] =
{
	&InternalEnumerator_1_t5205____array_0_FieldInfo,
	&InternalEnumerator_1_t5205____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31400_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5205____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5205_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31400_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5205____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5205_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m31403_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5205_PropertyInfos[] =
{
	&InternalEnumerator_1_t5205____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5205____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5205_InternalEnumerator_1__ctor_m31399_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m31399_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m31399_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t5205_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t5205_InternalEnumerator_1__ctor_m31399_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m31399_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31400_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31400_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t5205_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31400_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m31401_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m31401_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t5205_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m31401_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m31402_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m31402_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t5205_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m31402_GenericMethod/* genericMethod */

};
extern Il2CppType _Module_t2658_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m31403_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m31403_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t5205_il2cpp_TypeInfo/* declaring_type */
	, &_Module_t2658_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m31403_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5205_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m31399_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31400_MethodInfo,
	&InternalEnumerator_1_Dispose_m31401_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31402_MethodInfo,
	&InternalEnumerator_1_get_Current_m31403_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m31402_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m31401_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5205_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31400_MethodInfo,
	&InternalEnumerator_1_MoveNext_m31402_MethodInfo,
	&InternalEnumerator_1_Dispose_m31401_MethodInfo,
	&InternalEnumerator_1_get_Current_m31403_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5205_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t7290_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5205_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7290_il2cpp_TypeInfo, 7},
};
extern TypeInfo _Module_t2658_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5205_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m31403_MethodInfo/* Method Usage */,
	&_Module_t2658_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_Module_t2658_m40991_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5205_0_0_0;
extern Il2CppType InternalEnumerator_1_t5205_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5205_GenericClass;
TypeInfo InternalEnumerator_1_t5205_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5205_MethodInfos/* methods */
	, InternalEnumerator_1_t5205_PropertyInfos/* properties */
	, InternalEnumerator_1_t5205_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5205_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5205_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5205_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5205_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5205_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5205_1_0_0/* this_arg */
	, InternalEnumerator_1_t5205_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5205_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5205_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5205)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t9346_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>
extern MethodInfo IList_1_get_Item_m52126_MethodInfo;
extern MethodInfo IList_1_set_Item_m52127_MethodInfo;
static PropertyInfo IList_1_t9346____Item_PropertyInfo = 
{
	&IList_1_t9346_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m52126_MethodInfo/* get */
	, &IList_1_set_Item_m52127_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t9346_PropertyInfos[] =
{
	&IList_1_t9346____Item_PropertyInfo,
	NULL
};
extern Il2CppType _Module_t2658_0_0_0;
static ParameterInfo IList_1_t9346_IList_1_IndexOf_m52128_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_Module_t2658_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m52128_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::IndexOf(T)
MethodInfo IList_1_IndexOf_m52128_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t9346_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9346_IList_1_IndexOf_m52128_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m52128_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _Module_t2658_0_0_0;
static ParameterInfo IList_1_t9346_IList_1_Insert_m52129_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_Module_t2658_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m52129_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m52129_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t9346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9346_IList_1_Insert_m52129_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m52129_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9346_IList_1_RemoveAt_m52130_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m52130_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m52130_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t9346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t9346_IList_1_RemoveAt_m52130_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m52130_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t9346_IList_1_get_Item_m52126_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType _Module_t2658_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m52126_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m52126_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t9346_il2cpp_TypeInfo/* declaring_type */
	, &_Module_t2658_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t9346_IList_1_get_Item_m52126_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m52126_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType _Module_t2658_0_0_0;
static ParameterInfo IList_1_t9346_IList_1_set_Item_m52127_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_Module_t2658_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m52127_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m52127_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t9346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t9346_IList_1_set_Item_m52127_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m52127_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t9346_MethodInfos[] =
{
	&IList_1_IndexOf_m52128_MethodInfo,
	&IList_1_Insert_m52129_MethodInfo,
	&IList_1_RemoveAt_m52130_MethodInfo,
	&IList_1_get_Item_m52126_MethodInfo,
	&IList_1_set_Item_m52127_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t9346_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t9345_il2cpp_TypeInfo,
	&IEnumerable_1_t9347_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t9346_0_0_0;
extern Il2CppType IList_1_t9346_1_0_0;
struct IList_1_t9346;
extern Il2CppGenericClass IList_1_t9346_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t9346_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t9346_MethodInfos/* methods */
	, IList_1_t9346_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t9346_il2cpp_TypeInfo/* element_class */
	, IList_1_t9346_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t9346_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t9346_0_0_0/* byval_arg */
	, &IList_1_t9346_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t9346_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
