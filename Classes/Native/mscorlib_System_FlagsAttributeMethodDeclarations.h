﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.FlagsAttribute
struct FlagsAttribute_t464;

// System.Void System.FlagsAttribute::.ctor()
 void FlagsAttribute__ctor_m2040 (FlagsAttribute_t464 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
