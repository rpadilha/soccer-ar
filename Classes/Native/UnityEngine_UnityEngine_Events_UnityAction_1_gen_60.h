﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Jump_Button
struct Jump_Button_t207;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Jump_Button>
struct UnityAction_1_t3112  : public MulticastDelegate_t373
{
};
