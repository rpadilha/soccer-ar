﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t65;

// System.Void Vuforia.WireframeBehaviour::.ctor()
 void WireframeBehaviour__ctor_m96 (WireframeBehaviour_t65 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
 void WireframeBehaviour_CreateLineMaterial_m97 (WireframeBehaviour_t65 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
 void WireframeBehaviour_OnRenderObject_m98 (WireframeBehaviour_t65 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
 void WireframeBehaviour_OnDrawGizmos_m99 (WireframeBehaviour_t65 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
