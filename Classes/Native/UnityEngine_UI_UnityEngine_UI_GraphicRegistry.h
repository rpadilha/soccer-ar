﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.GraphicRegistry
struct GraphicRegistry_t350;
// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct Dictionary_2_t351;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t347;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.GraphicRegistry
struct GraphicRegistry_t350  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>> UnityEngine.UI.GraphicRegistry::m_Graphics
	Dictionary_2_t351 * ___m_Graphics_1;
};
struct GraphicRegistry_t350_StaticFields{
	// UnityEngine.UI.GraphicRegistry UnityEngine.UI.GraphicRegistry::s_Instance
	GraphicRegistry_t350 * ___s_Instance_0;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> UnityEngine.UI.GraphicRegistry::s_EmptyList
	List_1_t347 * ___s_EmptyList_2;
};
