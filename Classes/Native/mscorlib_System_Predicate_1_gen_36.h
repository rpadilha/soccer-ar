﻿#pragma once
#include <stdint.h>
// Vuforia.Word
struct Word_t736;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Word>
struct Predicate_1_t4162  : public MulticastDelegate_t373
{
};
