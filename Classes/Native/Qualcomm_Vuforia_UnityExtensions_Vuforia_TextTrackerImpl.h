﻿#pragma once
#include <stdint.h>
// Vuforia.WordList
struct WordList_t723;
// Vuforia.TextTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTracker.h"
// Vuforia.TextTrackerImpl
struct TextTrackerImpl_t725  : public TextTracker_t722
{
	// Vuforia.WordList Vuforia.TextTrackerImpl::mWordList
	WordList_t723 * ___mWordList_1;
};
