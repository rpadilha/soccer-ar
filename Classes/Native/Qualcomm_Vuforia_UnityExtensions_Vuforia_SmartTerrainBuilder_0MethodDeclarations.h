﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainBuilderImpl
struct SmartTerrainBuilderImpl_t715;
// System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>
struct IEnumerable_1_t619;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t46;
// Vuforia.Reconstruction
struct Reconstruction_t620;
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
struct SmartTerrainRevisionDataU5BU5D_t716;
// Vuforia.QCARManagerImpl/SurfaceData[]
struct SurfaceDataU5BU5D_t717;
// Vuforia.QCARManagerImpl/PropData[]
struct PropDataU5BU5D_t718;

// System.Boolean Vuforia.SmartTerrainBuilderImpl::Init()
 bool SmartTerrainBuilderImpl_Init_m3218 (SmartTerrainBuilderImpl_t715 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainBuilderImpl::Deinit()
 bool SmartTerrainBuilderImpl_Deinit_m3219 (SmartTerrainBuilderImpl_t715 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilderImpl::GetReconstructions()
 Object_t* SmartTerrainBuilderImpl_GetReconstructions_m3220 (SmartTerrainBuilderImpl_t715 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainBuilderImpl::AddReconstruction(Vuforia.ReconstructionAbstractBehaviour)
 bool SmartTerrainBuilderImpl_AddReconstruction_m3221 (SmartTerrainBuilderImpl_t715 * __this, ReconstructionAbstractBehaviour_t46 * ___reconstructionBehaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainBuilderImpl::RemoveReconstruction(Vuforia.ReconstructionAbstractBehaviour)
 bool SmartTerrainBuilderImpl_RemoveReconstruction_m3222 (SmartTerrainBuilderImpl_t715 * __this, ReconstructionAbstractBehaviour_t46 * ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainBuilderImpl::DestroyReconstruction(Vuforia.Reconstruction)
 bool SmartTerrainBuilderImpl_DestroyReconstruction_m3223 (SmartTerrainBuilderImpl_t715 * __this, Object_t * ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainBuilderImpl::UpdateSmartTerrainData(Vuforia.QCARManagerImpl/SmartTerrainRevisionData[],Vuforia.QCARManagerImpl/SurfaceData[],Vuforia.QCARManagerImpl/PropData[])
 void SmartTerrainBuilderImpl_UpdateSmartTerrainData_m3224 (SmartTerrainBuilderImpl_t715 * __this, SmartTerrainRevisionDataU5BU5D_t716* ___smartTerrainRevisions, SurfaceDataU5BU5D_t717* ___updatedSurfaces, PropDataU5BU5D_t718* ___updatedProps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainBuilderImpl::.ctor()
 void SmartTerrainBuilderImpl__ctor_m3225 (SmartTerrainBuilderImpl_t715 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
