﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>
struct DefaultComparer_t4381;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor()
 void DefaultComparer__ctor_m26044 (DefaultComparer_t4381 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m26045 (DefaultComparer_t4381 * __this, VirtualButtonData_t685  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>::Equals(T,T)
 bool DefaultComparer_Equals_m26046 (DefaultComparer_t4381 * __this, VirtualButtonData_t685  ___x, VirtualButtonData_t685  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
