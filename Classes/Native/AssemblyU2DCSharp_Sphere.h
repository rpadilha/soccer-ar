﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t29;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t95;
// UnityEngine.Transform
struct Transform_t74;
// Joystick_Script
struct Joystick_Script_t102;
// InGameState_Script
struct InGameState_Script_t83;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Sphere
struct Sphere_t71  : public MonoBehaviour_t10
{
	// UnityEngine.GameObject Sphere::owner
	GameObject_t29 * ___owner_2;
	// UnityEngine.GameObject Sphere::inputPlayer
	GameObject_t29 * ___inputPlayer_3;
	// UnityEngine.GameObject Sphere::lastInputPlayer
	GameObject_t29 * ___lastInputPlayer_4;
	// UnityEngine.GameObject[] Sphere::players
	GameObjectU5BU5D_t95* ___players_5;
	// UnityEngine.GameObject[] Sphere::oponents
	GameObjectU5BU5D_t95* ___oponents_6;
	// UnityEngine.Transform Sphere::shadowBall
	Transform_t74 * ___shadowBall_7;
	// UnityEngine.Transform Sphere::blobPlayerSelected
	Transform_t74 * ___blobPlayerSelected_8;
	// System.Single Sphere::timeToSelectAgain
	float ___timeToSelectAgain_9;
	// UnityEngine.GameObject Sphere::lastCandidatePlayer
	GameObject_t29 * ___lastCandidatePlayer_10;
	// System.Single Sphere::fHorizontal
	float ___fHorizontal_11;
	// System.Single Sphere::fVertical
	float ___fVertical_12;
	// System.Boolean Sphere::bPassButton
	bool ___bPassButton_13;
	// System.Boolean Sphere::bShootButton
	bool ___bShootButton_14;
	// System.Boolean Sphere::bShootButtonFinished
	bool ___bShootButtonFinished_15;
	// System.Boolean Sphere::pressiPhoneShootButton
	bool ___pressiPhoneShootButton_16;
	// System.Boolean Sphere::pressiPhonePassButton
	bool ___pressiPhonePassButton_17;
	// System.Boolean Sphere::pressiPhoneShootButtonEnded
	bool ___pressiPhoneShootButtonEnded_18;
	// Joystick_Script Sphere::joystick
	Joystick_Script_t102 * ___joystick_19;
	// InGameState_Script Sphere::inGame
	InGameState_Script_t83 * ___inGame_20;
	// System.Single Sphere::timeShootButtonPressed
	float ___timeShootButtonPressed_21;
};
