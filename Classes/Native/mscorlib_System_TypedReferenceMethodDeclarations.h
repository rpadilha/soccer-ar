﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.TypedReference
struct TypedReference_t1770;
// System.Object
struct Object_t;

// System.Boolean System.TypedReference::Equals(System.Object)
 bool TypedReference_Equals_m9922 (TypedReference_t1770 * __this, Object_t * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TypedReference::GetHashCode()
 int32_t TypedReference_GetHashCode_m9923 (TypedReference_t1770 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
