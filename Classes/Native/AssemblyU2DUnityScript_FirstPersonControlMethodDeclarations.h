﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// FirstPersonControl
struct FirstPersonControl_t211;

// System.Void FirstPersonControl::.ctor()
 void FirstPersonControl__ctor_m740 (FirstPersonControl_t211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstPersonControl::Start()
 void FirstPersonControl_Start_m741 (FirstPersonControl_t211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstPersonControl::OnEndGame()
 void FirstPersonControl_OnEndGame_m742 (FirstPersonControl_t211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstPersonControl::Update()
 void FirstPersonControl_Update_m743 (FirstPersonControl_t211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirstPersonControl::Main()
 void FirstPersonControl_Main_m744 (FirstPersonControl_t211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
