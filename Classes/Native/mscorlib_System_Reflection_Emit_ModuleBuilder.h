﻿#pragma once
#include <stdint.h>
// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1950;
// System.Char[]
struct CharU5BU5D_t378;
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1963  : public Module_t1755
{
	// System.Reflection.Emit.AssemblyBuilder System.Reflection.Emit.ModuleBuilder::assemblyb
	AssemblyBuilder_t1950 * ___assemblyb_10;
};
struct ModuleBuilder_t1963_StaticFields{
	// System.Char[] System.Reflection.Emit.ModuleBuilder::type_modifiers
	CharU5BU5D_t378* ___type_modifiers_11;
};
