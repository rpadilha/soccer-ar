﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.OrientedBoundingBox3D
struct OrientedBoundingBox3D_t635;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.OrientedBoundingBox3D::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
 void OrientedBoundingBox3D__ctor_m2964 (OrientedBoundingBox3D_t635 * __this, Vector3_t73  ___center, Vector3_t73  ___halfExtents, float ___rotationY, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_Center()
 Vector3_t73  OrientedBoundingBox3D_get_Center_m2965 (OrientedBoundingBox3D_t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_Center(UnityEngine.Vector3)
 void OrientedBoundingBox3D_set_Center_m2966 (OrientedBoundingBox3D_t635 * __this, Vector3_t73  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_HalfExtents()
 Vector3_t73  OrientedBoundingBox3D_get_HalfExtents_m2967 (OrientedBoundingBox3D_t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_HalfExtents(UnityEngine.Vector3)
 void OrientedBoundingBox3D_set_HalfExtents_m2968 (OrientedBoundingBox3D_t635 * __this, Vector3_t73  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.OrientedBoundingBox3D::get_RotationY()
 float OrientedBoundingBox3D_get_RotationY_m2969 (OrientedBoundingBox3D_t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_RotationY(System.Single)
 void OrientedBoundingBox3D_set_RotationY_m2970 (OrientedBoundingBox3D_t635 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
