﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t46;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionAbstractBehaviour>
struct UnityAction_1_t4238  : public MulticastDelegate_t373
{
};
