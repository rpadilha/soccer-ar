﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t11;

// System.Void Vuforia.ReconstructionBehaviour::.ctor()
 void ReconstructionBehaviour__ctor_m82 (ReconstructionBehaviour_t11 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
