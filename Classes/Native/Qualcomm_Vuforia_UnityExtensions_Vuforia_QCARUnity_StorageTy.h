﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.QCARUnity/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_StorageTy.h"
// Vuforia.QCARUnity/StorageType
struct StorageType_t797 
{
	// System.Int32 Vuforia.QCARUnity/StorageType::value__
	int32_t ___value___1;
};
