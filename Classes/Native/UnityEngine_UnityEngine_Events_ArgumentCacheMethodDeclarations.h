﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t1126;
// UnityEngine.Object
struct Object_t120;
struct Object_t120_marshaled;
// System.String
struct String_t;

// System.Void UnityEngine.Events.ArgumentCache::.ctor()
 void ArgumentCache__ctor_m6526 (ArgumentCache_t1126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
 Object_t120 * ArgumentCache_get_unityObjectArgument_m6527 (ArgumentCache_t1126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
 String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6528 (ArgumentCache_t1126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
 int32_t ArgumentCache_get_intArgument_m6529 (ArgumentCache_t1126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
 float ArgumentCache_get_floatArgument_m6530 (ArgumentCache_t1126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
 String_t* ArgumentCache_get_stringArgument_m6531 (ArgumentCache_t1126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
 bool ArgumentCache_get_boolArgument_m6532 (ArgumentCache_t1126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
