﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// GoalKeeper_Script
struct GoalKeeper_Script_t77;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<GoalKeeper_Script>
struct UnityAction_1_t3029  : public MulticastDelegate_t373
{
};
