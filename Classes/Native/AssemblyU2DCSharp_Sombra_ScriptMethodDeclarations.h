﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Sombra_Script
struct Sombra_Script_t110;

// System.Void Sombra_Script::.ctor()
 void Sombra_Script__ctor_m187 (Sombra_Script_t110 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sombra_Script::Start()
 void Sombra_Script_Start_m188 (Sombra_Script_t110 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sombra_Script::LateUpdate()
 void Sombra_Script_LateUpdate_m189 (Sombra_Script_t110 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
