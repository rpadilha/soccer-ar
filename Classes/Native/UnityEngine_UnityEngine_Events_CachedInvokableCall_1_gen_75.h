﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.StandaloneInputModule>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_72.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.StandaloneInputModule>
struct CachedInvokableCall_1_t3367  : public InvokableCall_1_t3368
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.StandaloneInputModule>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
