﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vuforia.OrientedBoundingBox3D
struct OrientedBoundingBox3D_t635 
{
	// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::<Center>k__BackingField
	Vector3_t73  ___U3CCenterU3Ek__BackingField_0;
	// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::<HalfExtents>k__BackingField
	Vector3_t73  ___U3CHalfExtentsU3Ek__BackingField_1;
	// System.Single Vuforia.OrientedBoundingBox3D::<RotationY>k__BackingField
	float ___U3CRotationYU3Ek__BackingField_2;
};
