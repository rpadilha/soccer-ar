﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<SetShield>
struct UnityAction_1_t3054;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<SetShield>
struct InvokableCall_1_t3053  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<SetShield>::Delegate
	UnityAction_1_t3054 * ___Delegate_0;
};
