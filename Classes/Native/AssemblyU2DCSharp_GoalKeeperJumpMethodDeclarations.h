﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GoalKeeperJump
struct GoalKeeperJump_t78;
// UnityEngine.Collider
struct Collider_t70;

// System.Void GoalKeeperJump::.ctor()
 void GoalKeeperJump__ctor_m121 (GoalKeeperJump_t78 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoalKeeperJump::Start()
 void GoalKeeperJump_Start_m122 (GoalKeeperJump_t78 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoalKeeperJump::Update()
 void GoalKeeperJump_Update_m123 (GoalKeeperJump_t78 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoalKeeperJump::OnTriggerEnter(UnityEngine.Collider)
 void GoalKeeperJump_OnTriggerEnter_m124 (GoalKeeperJump_t78 * __this, Collider_t70 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
