﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t653;
// System.Security.Cryptography.HashAlgorithm
#include "mscorlib_System_Security_Cryptography_HashAlgorithm.h"
// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t1636  : public HashAlgorithm_t1612
{
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_t653* ___KeyValue_4;
};
