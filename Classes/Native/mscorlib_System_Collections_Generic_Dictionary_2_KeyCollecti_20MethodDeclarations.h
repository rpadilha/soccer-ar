﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct KeyCollection_t3944;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t646;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t3952;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3940;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_21.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void KeyCollection__ctor_m21824 (KeyCollection_t3944 * __this, Dictionary_2_t646 * ___dictionary, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
 void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m21825 (KeyCollection_t3944 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<TKey>.Clear()
 void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m21826 (KeyCollection_t3944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m21827 (KeyCollection_t3944 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m21828 (KeyCollection_t3944 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
 Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m21829 (KeyCollection_t3944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void KeyCollection_System_Collections_ICollection_CopyTo_m21830 (KeyCollection_t3944 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m21831 (KeyCollection_t3944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m21832 (KeyCollection_t3944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.ICollection.get_IsSynchronized()
 bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m21833 (KeyCollection_t3944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.ICollection.get_SyncRoot()
 Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m21834 (KeyCollection_t3944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::CopyTo(TKey[],System.Int32)
 void KeyCollection_CopyTo_m21835 (KeyCollection_t3944 * __this, PIXEL_FORMATU5BU5D_t3940* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::GetEnumerator()
 Enumerator_t3953  KeyCollection_GetEnumerator_m21836 (KeyCollection_t3944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Count()
 int32_t KeyCollection_get_Count_m21837 (KeyCollection_t3944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
