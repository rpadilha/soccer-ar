﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t726;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>
struct KeyCollection_t4113  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>::dictionary
	Dictionary_2_t726 * ___dictionary_0;
};
