﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.TrackableBehaviour>
struct Comparer_1_t4234;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.TrackableBehaviour>
struct Comparer_1_t4234  : public Object_t
{
};
struct Comparer_1_t4234_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.TrackableBehaviour>::_default
	Comparer_1_t4234 * ____default_0;
};
