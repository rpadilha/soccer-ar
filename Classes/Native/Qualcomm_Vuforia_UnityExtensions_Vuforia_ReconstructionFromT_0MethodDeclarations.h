﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionFromTargetImpl
struct ReconstructionFromTargetImpl_t614;
// Vuforia.CylinderTarget
struct CylinderTarget_t615;
// Vuforia.ImageTarget
struct ImageTarget_t616;
// Vuforia.MultiTarget
struct MultiTarget_t617;
// Vuforia.Trackable
struct Trackable_t594;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void Vuforia.ReconstructionFromTargetImpl::.ctor(System.IntPtr)
 void ReconstructionFromTargetImpl__ctor_m2886 (ReconstructionFromTargetImpl_t614 * __this, IntPtr_t121 ___nativeReconstructionPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3)
 bool ReconstructionFromTargetImpl_SetInitializationTarget_m2887 (ReconstructionFromTargetImpl_t614 * __this, Object_t * ___cylinderTarget, Vector3_t73  ___occluderMin, Vector3_t73  ___occluderMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
 bool ReconstructionFromTargetImpl_SetInitializationTarget_m2888 (ReconstructionFromTargetImpl_t614 * __this, Object_t * ___cylinderTarget, Vector3_t73  ___occluderMin, Vector3_t73  ___occluderMax, Vector3_t73  ___offsetToOccluderOrigin, Quaternion_t108  ___rotationToOccluderOrigin, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.ImageTarget,UnityEngine.Vector3,UnityEngine.Vector3)
 bool ReconstructionFromTargetImpl_SetInitializationTarget_m2889 (ReconstructionFromTargetImpl_t614 * __this, Object_t * ___imageTarget, Vector3_t73  ___occluderMin, Vector3_t73  ___occluderMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.ImageTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
 bool ReconstructionFromTargetImpl_SetInitializationTarget_m2890 (ReconstructionFromTargetImpl_t614 * __this, Object_t * ___imageTarget, Vector3_t73  ___occluderMin, Vector3_t73  ___occluderMax, Vector3_t73  ___offsetToOccluderOrigin, Quaternion_t108  ___rotationToOccluderOrigin, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.MultiTarget,UnityEngine.Vector3,UnityEngine.Vector3)
 bool ReconstructionFromTargetImpl_SetInitializationTarget_m2891 (ReconstructionFromTargetImpl_t614 * __this, Object_t * ___multiTarget, Vector3_t73  ___occluderMin, Vector3_t73  ___occluderMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.MultiTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
 bool ReconstructionFromTargetImpl_SetInitializationTarget_m2892 (ReconstructionFromTargetImpl_t614 * __this, Object_t * ___multiTarget, Vector3_t73  ___occluderMin, Vector3_t73  ___occluderMax, Vector3_t73  ___offsetToOccluderOrigin, Quaternion_t108  ___rotationToOccluderOrigin, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&)
 Object_t * ReconstructionFromTargetImpl_GetInitializationTarget_m2893 (ReconstructionFromTargetImpl_t614 * __this, Vector3_t73 * ___occluderMin, Vector3_t73 * ___occluderMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
 Object_t * ReconstructionFromTargetImpl_GetInitializationTarget_m2894 (ReconstructionFromTargetImpl_t614 * __this, Vector3_t73 * ___occluderMin, Vector3_t73 * ___occluderMax, Vector3_t73 * ___offsetToOccluderOrigin, Quaternion_t108 * ___rotationToOccluderOrigin, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::Reset()
 bool ReconstructionFromTargetImpl_Reset_m2895 (ReconstructionFromTargetImpl_t614 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::Start()
 bool ReconstructionFromTargetImpl_Start_m2896 (ReconstructionFromTargetImpl_t614 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(System.IntPtr,Vuforia.Trackable,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
 bool ReconstructionFromTargetImpl_SetInitializationTarget_m2897 (ReconstructionFromTargetImpl_t614 * __this, IntPtr_t121 ___datasetPtr, Object_t * ___trackable, Vector3_t73  ___occluderMin, Vector3_t73  ___occluderMax, Vector3_t73  ___offsetToOccluderOrigin, Quaternion_t108  ___rotationToOccluderOrigin, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::get_CanAutoSetInitializationTarget()
 bool ReconstructionFromTargetImpl_get_CanAutoSetInitializationTarget_m2898 (ReconstructionFromTargetImpl_t614 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetImpl::set_CanAutoSetInitializationTarget(System.Boolean)
 void ReconstructionFromTargetImpl_set_CanAutoSetInitializationTarget_m2899 (ReconstructionFromTargetImpl_t614 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
