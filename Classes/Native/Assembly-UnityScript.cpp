﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo U3CModuleU3E_t206_il2cpp_TypeInfo;
extern TypeInfo Jump_Button_t207_il2cpp_TypeInfo;
extern TypeInfo CameraRelativeControl_t210_il2cpp_TypeInfo;
extern TypeInfo FirstPersonControl_t211_il2cpp_TypeInfo;
extern TypeInfo FollowTransform_t212_il2cpp_TypeInfo;
extern TypeInfo Boundary_t213_il2cpp_TypeInfo;
extern TypeInfo Joystick_t208_il2cpp_TypeInfo;
extern TypeInfo ObliqueNear_t215_il2cpp_TypeInfo;
extern TypeInfo PlayerRelativeControl_t217_il2cpp_TypeInfo;
extern TypeInfo RollABall_t218_il2cpp_TypeInfo;
extern TypeInfo ConstraintAxis_t219_il2cpp_TypeInfo;
extern TypeInfo RotationConstraint_t220_il2cpp_TypeInfo;
extern TypeInfo SidescrollControl_t221_il2cpp_TypeInfo;
extern TypeInfo SmoothFollow2D_t222_il2cpp_TypeInfo;
extern TypeInfo ControlState_t223_il2cpp_TypeInfo;
extern TypeInfo TapControl_t226_il2cpp_TypeInfo;
extern TypeInfo ZoomCamera_t224_il2cpp_TypeInfo;
#include "utils/RegisterRuntimeInitializeAndCleanup.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_AssemblyU2DUnityScript_Assembly_Types[18] = 
{
	&U3CModuleU3E_t206_il2cpp_TypeInfo,
	&Jump_Button_t207_il2cpp_TypeInfo,
	&CameraRelativeControl_t210_il2cpp_TypeInfo,
	&FirstPersonControl_t211_il2cpp_TypeInfo,
	&FollowTransform_t212_il2cpp_TypeInfo,
	&Boundary_t213_il2cpp_TypeInfo,
	&Joystick_t208_il2cpp_TypeInfo,
	&ObliqueNear_t215_il2cpp_TypeInfo,
	&PlayerRelativeControl_t217_il2cpp_TypeInfo,
	&RollABall_t218_il2cpp_TypeInfo,
	&ConstraintAxis_t219_il2cpp_TypeInfo,
	&RotationConstraint_t220_il2cpp_TypeInfo,
	&SidescrollControl_t221_il2cpp_TypeInfo,
	&SmoothFollow2D_t222_il2cpp_TypeInfo,
	&ControlState_t223_il2cpp_TypeInfo,
	&TapControl_t226_il2cpp_TypeInfo,
	&ZoomCamera_t224_il2cpp_TypeInfo,
	NULL,
};
extern Il2CppImage g_AssemblyU2DUnityScript_Image;
extern CustomAttributesCache g_AssemblyU2DUnityScript_Assembly__CustomAttributeCache;
Il2CppAssembly g_AssemblyU2DUnityScript_Assembly = 
{
	{ "Assembly-UnityScript", 0, 0, 0, { 0 }, 32772, 0, 0, 0, 0, 0, 0 },
	&g_AssemblyU2DUnityScript_Image,
	&g_AssemblyU2DUnityScript_Assembly__CustomAttributeCache,
};
Il2CppImage g_AssemblyU2DUnityScript_Image = 
{
	 "Assembly-UnityScript.dll" ,
	&g_AssemblyU2DUnityScript_Assembly,
	g_AssemblyU2DUnityScript_Assembly_Types,
	17,
	NULL,
};
static void s_AssemblyU2DUnityScriptRegistration()
{
	RegisterAssembly (&g_AssemblyU2DUnityScript_Assembly);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_AssemblyU2DUnityScriptRegistrationVariable(&s_AssemblyU2DUnityScriptRegistration, NULL);
