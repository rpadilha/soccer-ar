﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AsyncOperation
struct AsyncOperation_t952;
struct AsyncOperation_t952_marshaled;

// System.Void UnityEngine.AsyncOperation::.ctor()
 void AsyncOperation__ctor_m6102 (AsyncOperation_t952 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
 void AsyncOperation_InternalDestroy_m6103 (AsyncOperation_t952 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::Finalize()
 void AsyncOperation_Finalize_m6104 (AsyncOperation_t952 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void AsyncOperation_t952_marshal(const AsyncOperation_t952& unmarshaled, AsyncOperation_t952_marshaled& marshaled);
void AsyncOperation_t952_marshal_back(const AsyncOperation_t952_marshaled& marshaled, AsyncOperation_t952& unmarshaled);
void AsyncOperation_t952_marshal_cleanup(AsyncOperation_t952_marshaled& marshaled);
