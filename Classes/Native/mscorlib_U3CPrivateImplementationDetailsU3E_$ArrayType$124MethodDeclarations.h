﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$124
struct $ArrayType$124_t2327;
struct $ArrayType$124_t2327_marshaled;

void $ArrayType$124_t2327_marshal(const $ArrayType$124_t2327& unmarshaled, $ArrayType$124_t2327_marshaled& marshaled);
void $ArrayType$124_t2327_marshal_back(const $ArrayType$124_t2327_marshaled& marshaled, $ArrayType$124_t2327& unmarshaled);
void $ArrayType$124_t2327_marshal_cleanup($ArrayType$124_t2327_marshaled& marshaled);
