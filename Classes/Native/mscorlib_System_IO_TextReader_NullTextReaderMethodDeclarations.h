﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.TextReader/NullTextReader
struct NullTextReader_t1943;
// System.String
struct String_t;

// System.Void System.IO.TextReader/NullTextReader::.ctor()
 void NullTextReader__ctor_m11055 (NullTextReader_t1943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.TextReader/NullTextReader::ReadLine()
 String_t* NullTextReader_ReadLine_m11056 (NullTextReader_t1943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
