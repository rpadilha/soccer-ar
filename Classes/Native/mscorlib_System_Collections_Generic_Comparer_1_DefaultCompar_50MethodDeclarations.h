﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct DefaultComparer_t4635;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t982;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::.ctor()
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_0MethodDeclarations.h"
#define DefaultComparer__ctor_m28079(__this, method) (void)DefaultComparer__ctor_m14676_gshared((DefaultComparer_t2849 *)__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Compare(T,T)
#define DefaultComparer_Compare_m28080(__this, ___x, ___y, method) (int32_t)DefaultComparer_Compare_m14677_gshared((DefaultComparer_t2849 *)__this, (Object_t *)___x, (Object_t *)___y, method)
