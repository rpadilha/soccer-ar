﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct ShimEnumerator_t4280;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct Dictionary_2_t760;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m25116 (ShimEnumerator_t4280 * __this, Dictionary_2_t760 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::MoveNext()
 bool ShimEnumerator_MoveNext_m25117 (ShimEnumerator_t4280 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m25118 (ShimEnumerator_t4280 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Key()
 Object_t * ShimEnumerator_get_Key_m25119 (ShimEnumerator_t4280 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Value()
 Object_t * ShimEnumerator_get_Value_m25120 (ShimEnumerator_t4280 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Current()
 Object_t * ShimEnumerator_get_Current_m25121 (ShimEnumerator_t4280 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
