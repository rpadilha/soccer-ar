﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.DataSetImpl>
struct IList_1_t4025;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSetImpl>
struct ReadOnlyCollection_1_t4020  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSetImpl>::list
	Object_t* ___list_0;
};
