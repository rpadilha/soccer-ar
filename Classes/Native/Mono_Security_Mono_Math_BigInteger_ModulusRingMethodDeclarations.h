﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t1591;
// Mono.Math.BigInteger
struct BigInteger_t1590;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
 void ModulusRing__ctor_m8149 (ModulusRing_t1591 * __this, BigInteger_t1590 * ___modulus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
 void ModulusRing_BarrettReduction_m8150 (ModulusRing_t1591 * __this, BigInteger_t1590 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1590 * ModulusRing_Multiply_m8151 (ModulusRing_t1591 * __this, BigInteger_t1590 * ___a, BigInteger_t1590 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1590 * ModulusRing_Difference_m8152 (ModulusRing_t1591 * __this, BigInteger_t1590 * ___a, BigInteger_t1590 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1590 * ModulusRing_Pow_m8153 (ModulusRing_t1591 * __this, BigInteger_t1590 * ___a, BigInteger_t1590 * ___k, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
 BigInteger_t1590 * ModulusRing_Pow_m8154 (ModulusRing_t1591 * __this, uint32_t ___b, BigInteger_t1590 * ___exp, MethodInfo* method) IL2CPP_METHOD_ATTR;
