﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MonoEventInfo
struct MonoEventInfo_t1987;
// System.Reflection.MonoEvent
struct MonoEvent_t1988;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"

// System.Void System.Reflection.MonoEventInfo::get_event_info(System.Reflection.MonoEvent,System.Reflection.MonoEventInfo&)
 void MonoEventInfo_get_event_info_m11425 (Object_t * __this/* static, unused */, MonoEvent_t1988 * ___ev, MonoEventInfo_t1987 * ___info, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MonoEventInfo System.Reflection.MonoEventInfo::GetEventInfo(System.Reflection.MonoEvent)
 MonoEventInfo_t1987  MonoEventInfo_GetEventInfo_m11426 (Object_t * __this/* static, unused */, MonoEvent_t1988 * ___ev, MethodInfo* method) IL2CPP_METHOD_ATTR;
