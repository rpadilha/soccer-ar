﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Hashtable/HashKeys
struct HashKeys_t1886;
// System.Object
struct Object_t;
// System.Collections.Hashtable
struct Hashtable_t1348;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;

// System.Void System.Collections.Hashtable/HashKeys::.ctor(System.Collections.Hashtable)
 void HashKeys__ctor_m10546 (HashKeys_t1886 * __this, Hashtable_t1348 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Hashtable/HashKeys::get_Count()
 int32_t HashKeys_get_Count_m10547 (HashKeys_t1886 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Hashtable/HashKeys::get_IsSynchronized()
 bool HashKeys_get_IsSynchronized_m10548 (HashKeys_t1886 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Hashtable/HashKeys::get_SyncRoot()
 Object_t * HashKeys_get_SyncRoot_m10549 (HashKeys_t1886 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/HashKeys::CopyTo(System.Array,System.Int32)
 void HashKeys_CopyTo_m10550 (HashKeys_t1886 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Hashtable/HashKeys::GetEnumerator()
 Object_t * HashKeys_GetEnumerator_m10551 (HashKeys_t1886 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
