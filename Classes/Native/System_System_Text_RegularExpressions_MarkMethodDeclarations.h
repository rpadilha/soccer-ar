﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Mark
struct Mark_t1480;

// System.Boolean System.Text.RegularExpressions.Mark::get_IsDefined()
 bool Mark_get_IsDefined_m7481 (Mark_t1480 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.RegularExpressions.Mark::get_Index()
 int32_t Mark_get_Index_m7482 (Mark_t1480 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.RegularExpressions.Mark::get_Length()
 int32_t Mark_get_Length_m7483 (Mark_t1480 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
