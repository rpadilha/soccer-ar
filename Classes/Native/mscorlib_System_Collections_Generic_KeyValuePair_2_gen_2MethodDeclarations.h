﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>
struct KeyValuePair_2_t892;
// Vuforia.Surface
struct Surface_t16;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m24912 (KeyValuePair_2_t892 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::get_Key()
 int32_t KeyValuePair_2_get_Key_m24913 (KeyValuePair_2_t892 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m24914 (KeyValuePair_2_t892 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::get_Value()
 Object_t * KeyValuePair_2_get_Value_m5321 (KeyValuePair_2_t892 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m24915 (KeyValuePair_2_t892 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::ToString()
 String_t* KeyValuePair_2_ToString_m24916 (KeyValuePair_2_t892 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
