﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<ShieldMenu>
struct UnityAction_1_t3057;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<ShieldMenu>
struct InvokableCall_1_t3056  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<ShieldMenu>::Delegate
	UnityAction_1_t3057 * ___Delegate_0;
};
