﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.RemotingSurrogate
struct RemotingSurrogate_t2075;
// System.Object
struct Object_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t2076;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogate::.ctor()
 void RemotingSurrogate__ctor_m11772 (RemotingSurrogate_t2075 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.RemotingSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
 Object_t * RemotingSurrogate_SetObjectData_m11773 (RemotingSurrogate_t2075 * __this, Object_t * ___obj, SerializationInfo_t1118 * ___si, StreamingContext_t1119  ___sc, Object_t * ___selector, MethodInfo* method) IL2CPP_METHOD_ATTR;
