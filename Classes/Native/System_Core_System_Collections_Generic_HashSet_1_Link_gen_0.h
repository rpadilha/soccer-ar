﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>
struct Link_t4560 
{
	// System.Int32 System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>::HashCode
	int32_t ___HashCode_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>::Next
	int32_t ___Next_1;
};
