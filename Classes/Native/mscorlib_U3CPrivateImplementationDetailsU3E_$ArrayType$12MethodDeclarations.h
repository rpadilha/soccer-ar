﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct $ArrayType$12_t2324;
struct $ArrayType$12_t2324_marshaled;

void $ArrayType$12_t2324_marshal(const $ArrayType$12_t2324& unmarshaled, $ArrayType$12_t2324_marshaled& marshaled);
void $ArrayType$12_t2324_marshal_back(const $ArrayType$12_t2324_marshaled& marshaled, $ArrayType$12_t2324& unmarshaled);
void $ArrayType$12_t2324_marshal_cleanup($ArrayType$12_t2324_marshaled& marshaled);
