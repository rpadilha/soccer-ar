﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForSeconds
struct WaitForSeconds_t527;
struct WaitForSeconds_t527_marshaled;

// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
 void WaitForSeconds__ctor_m2426 (WaitForSeconds_t527 * __this, float ___seconds, MethodInfo* method) IL2CPP_METHOD_ATTR;
void WaitForSeconds_t527_marshal(const WaitForSeconds_t527& unmarshaled, WaitForSeconds_t527_marshaled& marshaled);
void WaitForSeconds_t527_marshal_back(const WaitForSeconds_t527_marshaled& marshaled, WaitForSeconds_t527& unmarshaled);
void WaitForSeconds_t527_marshal_cleanup(WaitForSeconds_t527_marshaled& marshaled);
