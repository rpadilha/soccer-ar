﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Int32>
struct Comparer_1_t4081;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Int32>
struct Comparer_1_t4081  : public Object_t
{
};
struct Comparer_1_t4081_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Int32>::_default
	Comparer_1_t4081 * ____default_0;
};
