﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionImpl
struct ReconstructionImpl_t608;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void Vuforia.ReconstructionImpl::.ctor(System.IntPtr)
 void ReconstructionImpl__ctor_m2835 (ReconstructionImpl_t608 * __this, IntPtr_t121 ___nativeReconstructionPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.ReconstructionImpl::get_NativePtr()
 IntPtr_t121 ReconstructionImpl_get_NativePtr_m2836 (ReconstructionImpl_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionImpl::SetMaximumArea(UnityEngine.Rect)
 bool ReconstructionImpl_SetMaximumArea_m2837 (ReconstructionImpl_t608 * __this, Rect_t103  ___maximumArea, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionImpl::GetMaximumArea(UnityEngine.Rect&)
 bool ReconstructionImpl_GetMaximumArea_m2838 (ReconstructionImpl_t608 * __this, Rect_t103 * ___rect, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionImpl::Stop()
 bool ReconstructionImpl_Stop_m2839 (ReconstructionImpl_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionImpl::Start()
 bool ReconstructionImpl_Start_m2840 (ReconstructionImpl_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionImpl::IsReconstructing()
 bool ReconstructionImpl_IsReconstructing_m2841 (ReconstructionImpl_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionImpl::SetNavMeshPadding(System.Single)
 void ReconstructionImpl_SetNavMeshPadding_m2842 (ReconstructionImpl_t608 * __this, float ___padding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.ReconstructionImpl::get_NavMeshPadding()
 float ReconstructionImpl_get_NavMeshPadding_m2843 (ReconstructionImpl_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionImpl::StartNavMeshUpdates()
 void ReconstructionImpl_StartNavMeshUpdates_m2844 (ReconstructionImpl_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionImpl::StopNavMeshUpdates()
 void ReconstructionImpl_StopNavMeshUpdates_m2845 (ReconstructionImpl_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionImpl::IsNavMeshUpdating()
 bool ReconstructionImpl_IsNavMeshUpdating_m2846 (ReconstructionImpl_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionImpl::Reset()
 bool ReconstructionImpl_Reset_m2847 (ReconstructionImpl_t608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
