﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.DataSet>
struct DefaultComparer_t4040;
// Vuforia.DataSet
struct DataSet_t612;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.DataSet>::.ctor()
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_0MethodDeclarations.h"
#define DefaultComparer__ctor_m22724(__this, method) (void)DefaultComparer__ctor_m14665_gshared((DefaultComparer_t2847 *)__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.DataSet>::GetHashCode(T)
#define DefaultComparer_GetHashCode_m22725(__this, ___obj, method) (int32_t)DefaultComparer_GetHashCode_m14666_gshared((DefaultComparer_t2847 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.DataSet>::Equals(T,T)
#define DefaultComparer_Equals_m22726(__this, ___x, ___y, method) (bool)DefaultComparer_Equals_m14667_gshared((DefaultComparer_t2847 *)__this, (Object_t *)___x, (Object_t *)___y, method)
