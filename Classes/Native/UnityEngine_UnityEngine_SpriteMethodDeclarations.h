﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Sprite
struct Sprite_t194;
// UnityEngine.Texture2D
struct Texture2D_t196;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"

// UnityEngine.Rect UnityEngine.Sprite::get_rect()
 Rect_t103  Sprite_get_rect_m2375 (Sprite_t194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
 float Sprite_get_pixelsPerUnit_m2372 (Sprite_t194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
 Texture2D_t196 * Sprite_get_texture_m2369 (Sprite_t194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
 Rect_t103  Sprite_get_textureRect_m2398 (Sprite_t194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
 Vector4_t216  Sprite_get_border_m2370 (Sprite_t194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
