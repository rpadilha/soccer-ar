﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>
struct InternalEnumerator_1_t3603;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.Image/Origin360
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin360.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m19519 (InternalEnumerator_1_t3603 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19520 (InternalEnumerator_1_t3603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::Dispose()
 void InternalEnumerator_1_Dispose_m19521 (InternalEnumerator_1_t3603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m19522 (InternalEnumerator_1_t3603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m19523 (InternalEnumerator_1_t3603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
