﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t25;

// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
 void ComponentFactoryStarterBehaviour__ctor_m45 (ComponentFactoryStarterBehaviour_t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
 void ComponentFactoryStarterBehaviour_Awake_m46 (ComponentFactoryStarterBehaviour_t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
 void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m47 (ComponentFactoryStarterBehaviour_t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
