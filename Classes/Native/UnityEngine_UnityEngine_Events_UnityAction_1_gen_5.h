﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t444;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct UnityAction_1_t456  : public MulticastDelegate_t373
{
};
