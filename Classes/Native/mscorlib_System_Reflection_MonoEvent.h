﻿#pragma once
#include <stdint.h>
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfo.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Reflection.MonoEvent
struct MonoEvent_t1988  : public EventInfo_t1756
{
	// System.IntPtr System.Reflection.MonoEvent::klass
	IntPtr_t121 ___klass_1;
	// System.IntPtr System.Reflection.MonoEvent::handle
	IntPtr_t121 ___handle_2;
};
