﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerTrackerImpl
struct MarkerTrackerImpl_t670;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t32;
// System.String
struct String_t;
// Vuforia.Marker
struct Marker_t667;
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker>
struct IEnumerable_1_t668;

// System.Boolean Vuforia.MarkerTrackerImpl::Start()
 bool MarkerTrackerImpl_Start_m3113 (MarkerTrackerImpl_t670 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerTrackerImpl::Stop()
 void MarkerTrackerImpl_Stop_m3114 (MarkerTrackerImpl_t670 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.MarkerTrackerImpl::CreateMarker(System.Int32,System.String,System.Single)
 MarkerAbstractBehaviour_t32 * MarkerTrackerImpl_CreateMarker_m3115 (MarkerTrackerImpl_t670 * __this, int32_t ___markerID, String_t* ___trackableName, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerTrackerImpl::DestroyMarker(Vuforia.Marker,System.Boolean)
 bool MarkerTrackerImpl_DestroyMarker_m3116 (MarkerTrackerImpl_t670 * __this, Object_t * ___marker, bool ___destroyGameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker> Vuforia.MarkerTrackerImpl::GetMarkers()
 Object_t* MarkerTrackerImpl_GetMarkers_m3117 (MarkerTrackerImpl_t670 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Marker Vuforia.MarkerTrackerImpl::GetMarkerByMarkerID(System.Int32)
 Object_t * MarkerTrackerImpl_GetMarkerByMarkerID_m3118 (MarkerTrackerImpl_t670 * __this, int32_t ___markerID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Marker Vuforia.MarkerTrackerImpl::InternalCreateMarker(System.Int32,System.String,System.Single)
 Object_t * MarkerTrackerImpl_InternalCreateMarker_m3119 (MarkerTrackerImpl_t670 * __this, int32_t ___markerID, String_t* ___name, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerTrackerImpl::DestroyAllMarkers(System.Boolean)
 void MarkerTrackerImpl_DestroyAllMarkers_m3120 (MarkerTrackerImpl_t670 * __this, bool ___destroyGameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.MarkerTrackerImpl::RegisterMarker(System.Int32,System.String,System.Single)
 int32_t MarkerTrackerImpl_RegisterMarker_m3121 (MarkerTrackerImpl_t670 * __this, int32_t ___markerID, String_t* ___trackableName, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerTrackerImpl::.ctor()
 void MarkerTrackerImpl__ctor_m3122 (MarkerTrackerImpl_t670 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
