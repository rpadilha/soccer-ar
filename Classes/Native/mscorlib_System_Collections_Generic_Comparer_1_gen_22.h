﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Type>
struct Comparer_1_t3900;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Type>
struct Comparer_1_t3900  : public Object_t
{
};
struct Comparer_1_t3900_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Type>::_default
	Comparer_1_t3900 * ____default_0;
};
