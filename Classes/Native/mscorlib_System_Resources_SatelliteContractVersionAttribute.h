﻿#pragma once
#include <stdint.h>
// System.Version
struct Version_t1386;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.SatelliteContractVersionAttribute
struct SatelliteContractVersionAttribute_t1334  : public Attribute_t145
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t1386 * ___ver_0;
};
