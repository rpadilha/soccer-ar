﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordImpl
struct WordImpl_t731;
// System.String
struct String_t;
// Vuforia.Image
struct Image_t604;
// Vuforia.RectangleData[]
struct RectangleDataU5BU5D_t730;
// Vuforia.ImageImpl
struct ImageImpl_t655;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Vuforia.QCARManagerImpl/ImageHeaderData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Ima.h"

// System.Void Vuforia.WordImpl::.ctor(System.Int32,System.String,UnityEngine.Vector2)
 void WordImpl__ctor_m3254 (WordImpl_t731 * __this, int32_t ___id, String_t* ___text, Vector2_t99  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WordImpl::get_StringValue()
 String_t* WordImpl_get_StringValue_m3255 (WordImpl_t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vuforia.WordImpl::get_Size()
 Vector2_t99  WordImpl_get_Size_m3256 (WordImpl_t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Image Vuforia.WordImpl::GetLetterMask()
 Image_t604 * WordImpl_GetLetterMask_m3257 (WordImpl_t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.RectangleData[] Vuforia.WordImpl::GetLetterBoundingBoxes()
 RectangleDataU5BU5D_t730* WordImpl_GetLetterBoundingBoxes_m3258 (WordImpl_t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordImpl::InitImageHeader()
 void WordImpl_InitImageHeader_m3259 (WordImpl_t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordImpl::CreateLetterMask()
 void WordImpl_CreateLetterMask_m3260 (WordImpl_t731 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordImpl::SetImageValues(Vuforia.QCARManagerImpl/ImageHeaderData,Vuforia.ImageImpl)
 void WordImpl_SetImageValues_m3261 (Object_t * __this/* static, unused */, ImageHeaderData_t690  ___imageHeader, ImageImpl_t655 * ___image, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordImpl::AllocateImage(Vuforia.ImageImpl)
 void WordImpl_AllocateImage_m3262 (Object_t * __this/* static, unused */, ImageImpl_t655 * ___image, MethodInfo* method) IL2CPP_METHOD_ATTR;
