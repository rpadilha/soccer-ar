﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DataSetLoadAbstractBehaviour
struct DataSetLoadAbstractBehaviour_t8;
// System.String
struct String_t;

// System.Void Vuforia.DataSetLoadAbstractBehaviour::LoadDatasets()
 void DataSetLoadAbstractBehaviour_LoadDatasets_m2953 (DataSetLoadAbstractBehaviour_t8 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs()
 void DataSetLoadAbstractBehaviour_AddOSSpecificExternalDatasetSearchDirs_m2954 (DataSetLoadAbstractBehaviour_t8 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddExternalDatasetSearchDir(System.String)
 void DataSetLoadAbstractBehaviour_AddExternalDatasetSearchDir_m2955 (DataSetLoadAbstractBehaviour_t8 * __this, String_t* ___searchDir, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::Start()
 void DataSetLoadAbstractBehaviour_Start_m2956 (DataSetLoadAbstractBehaviour_t8 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::.ctor()
 void DataSetLoadAbstractBehaviour__ctor_m253 (DataSetLoadAbstractBehaviour_t8 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
