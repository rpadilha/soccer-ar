﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ObjectTrackerImpl
struct ObjectTrackerImpl_t664;
// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t645;
// Vuforia.TargetFinder
struct TargetFinder_t660;
// Vuforia.DataSet
struct DataSet_t612;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet>
struct IEnumerable_1_t661;

// Vuforia.ImageTargetBuilder Vuforia.ObjectTrackerImpl::get_ImageTargetBuilder()
 ImageTargetBuilder_t645 * ObjectTrackerImpl_get_ImageTargetBuilder_m3091 (ObjectTrackerImpl_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TargetFinder Vuforia.ObjectTrackerImpl::get_TargetFinder()
 TargetFinder_t660 * ObjectTrackerImpl_get_TargetFinder_m3092 (ObjectTrackerImpl_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTrackerImpl::.ctor()
 void ObjectTrackerImpl__ctor_m3093 (ObjectTrackerImpl_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::Start()
 bool ObjectTrackerImpl_Start_m3094 (ObjectTrackerImpl_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTrackerImpl::Stop()
 void ObjectTrackerImpl_Stop_m3095 (ObjectTrackerImpl_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DataSet Vuforia.ObjectTrackerImpl::CreateDataSet()
 DataSet_t612 * ObjectTrackerImpl_CreateDataSet_m3096 (ObjectTrackerImpl_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::DestroyDataSet(Vuforia.DataSet,System.Boolean)
 bool ObjectTrackerImpl_DestroyDataSet_m3097 (ObjectTrackerImpl_t664 * __this, DataSet_t612 * ___dataSet, bool ___destroyTrackables, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::ActivateDataSet(Vuforia.DataSet)
 bool ObjectTrackerImpl_ActivateDataSet_m3098 (ObjectTrackerImpl_t664 * __this, DataSet_t612 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::DeactivateDataSet(Vuforia.DataSet)
 bool ObjectTrackerImpl_DeactivateDataSet_m3099 (ObjectTrackerImpl_t664 * __this, DataSet_t612 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::GetActiveDataSets()
 Object_t* ObjectTrackerImpl_GetActiveDataSets_m3100 (ObjectTrackerImpl_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::GetDataSets()
 Object_t* ObjectTrackerImpl_GetDataSets_m3101 (ObjectTrackerImpl_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTrackerImpl::DestroyAllDataSets(System.Boolean)
 void ObjectTrackerImpl_DestroyAllDataSets_m3102 (ObjectTrackerImpl_t664 * __this, bool ___destroyTrackables, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::PersistExtendedTracking(System.Boolean)
 bool ObjectTrackerImpl_PersistExtendedTracking_m3103 (ObjectTrackerImpl_t664 * __this, bool ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::ResetExtendedTracking()
 bool ObjectTrackerImpl_ResetExtendedTracking_m3104 (ObjectTrackerImpl_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
