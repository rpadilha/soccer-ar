﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct EqualityComparer_1_t4403;
// System.Object
struct Object_t;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
 void EqualityComparer_1__ctor_m26320 (EqualityComparer_1_t4403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
 void EqualityComparer_1__cctor_m26321 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
 int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26322 (EqualityComparer_1_t4403 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
 bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26323 (EqualityComparer_1_t4403 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>::get_Default()
 EqualityComparer_1_t4403 * EqualityComparer_1_get_Default_m26324 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
