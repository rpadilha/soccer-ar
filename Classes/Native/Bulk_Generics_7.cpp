﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerator_1_t6133_il2cpp_TypeInfo;


// System.Array
#include "mscorlib_System_Array.h"

// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorWordBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorWordBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m43915_MethodInfo;
static PropertyInfo IEnumerator_1_t6133____Current_PropertyInfo = 
{
	&IEnumerator_1_t6133_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43915_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6133_PropertyInfos[] =
{
	&IEnumerator_1_t6133____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43915_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorWordBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43915_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6133_il2cpp_TypeInfo/* declaring_type */
	, &IEditorWordBehaviour_t178_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43915_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6133_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43915_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6133_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6133_0_0_0;
extern Il2CppType IEnumerator_1_t6133_1_0_0;
struct IEnumerator_1_t6133;
extern Il2CppGenericClass IEnumerator_1_t6133_GenericClass;
TypeInfo IEnumerator_1_t6133_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6133_MethodInfos/* methods */
	, IEnumerator_1_t6133_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6133_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6133_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6133_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6133_0_0_0/* byval_arg */
	, &IEnumerator_1_t6133_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6133_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_110.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2992_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_110MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo IEditorWordBehaviour_t178_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m15338_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorWordBehaviour_t178_m33521_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorWordBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorWordBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorWordBehaviour_t178_m33521(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2992____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2992_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2992, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2992____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2992_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2992, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2992_FieldInfos[] =
{
	&InternalEnumerator_1_t2992____array_0_FieldInfo,
	&InternalEnumerator_1_t2992____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15335_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2992____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2992_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15335_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2992____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2992_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15338_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2992_PropertyInfos[] =
{
	&InternalEnumerator_1_t2992____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2992____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2992_InternalEnumerator_1__ctor_m15334_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15334_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15334_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2992_InternalEnumerator_1__ctor_m15334_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15334_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15335_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15335_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2992_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15335_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15336_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15336_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15336_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15337_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15337_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2992_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15337_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15338_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorWordBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15338_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2992_il2cpp_TypeInfo/* declaring_type */
	, &IEditorWordBehaviour_t178_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15338_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2992_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15334_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15335_MethodInfo,
	&InternalEnumerator_1_Dispose_m15336_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15337_MethodInfo,
	&InternalEnumerator_1_get_Current_m15338_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m15337_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15336_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2992_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15335_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15337_MethodInfo,
	&InternalEnumerator_1_Dispose_m15336_MethodInfo,
	&InternalEnumerator_1_get_Current_m15338_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2992_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6133_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2992_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6133_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorWordBehaviour_t178_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2992_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15338_MethodInfo/* Method Usage */,
	&IEditorWordBehaviour_t178_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorWordBehaviour_t178_m33521_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2992_0_0_0;
extern Il2CppType InternalEnumerator_1_t2992_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2992_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2992_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2992_MethodInfos/* methods */
	, InternalEnumerator_1_t2992_PropertyInfos/* properties */
	, InternalEnumerator_1_t2992_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2992_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2992_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2992_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2992_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2992_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2992_1_0_0/* this_arg */
	, InternalEnumerator_1_t2992_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2992_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2992_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2992)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7833_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>
extern MethodInfo IList_1_get_Item_m43916_MethodInfo;
extern MethodInfo IList_1_set_Item_m43917_MethodInfo;
static PropertyInfo IList_1_t7833____Item_PropertyInfo = 
{
	&IList_1_t7833_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43916_MethodInfo/* get */
	, &IList_1_set_Item_m43917_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7833_PropertyInfos[] =
{
	&IList_1_t7833____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
static ParameterInfo IList_1_t7833_IList_1_IndexOf_m43918_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorWordBehaviour_t178_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43918_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43918_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7833_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7833_IList_1_IndexOf_m43918_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43918_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
static ParameterInfo IList_1_t7833_IList_1_Insert_m43919_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorWordBehaviour_t178_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43919_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43919_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7833_IList_1_Insert_m43919_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43919_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7833_IList_1_RemoveAt_m43920_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43920_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43920_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7833_IList_1_RemoveAt_m43920_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43920_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7833_IList_1_get_Item_m43916_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43916_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43916_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7833_il2cpp_TypeInfo/* declaring_type */
	, &IEditorWordBehaviour_t178_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7833_IList_1_get_Item_m43916_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43916_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEditorWordBehaviour_t178_0_0_0;
static ParameterInfo IList_1_t7833_IList_1_set_Item_m43917_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorWordBehaviour_t178_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43917_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorWordBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43917_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7833_IList_1_set_Item_m43917_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43917_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7833_MethodInfos[] =
{
	&IList_1_IndexOf_m43918_MethodInfo,
	&IList_1_Insert_m43919_MethodInfo,
	&IList_1_RemoveAt_m43920_MethodInfo,
	&IList_1_get_Item_m43916_MethodInfo,
	&IList_1_set_Item_m43917_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7832_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7834_il2cpp_TypeInfo;
static TypeInfo* IList_1_t7833_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7832_il2cpp_TypeInfo,
	&IEnumerable_1_t7834_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7833_0_0_0;
extern Il2CppType IList_1_t7833_1_0_0;
struct IList_1_t7833;
extern Il2CppGenericClass IList_1_t7833_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7833_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7833_MethodInfos/* methods */
	, IList_1_t7833_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7833_il2cpp_TypeInfo/* element_class */
	, IList_1_t7833_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7833_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7833_0_0_0/* byval_arg */
	, &IList_1_t7833_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7833_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WordBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_36.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2993_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WordBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_36MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_32.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo WordBehaviour_t68_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2994_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_32MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15341_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15343_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WordBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WordBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.WordBehaviour>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2993____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2993_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2993, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2993_FieldInfos[] =
{
	&CachedInvokableCall_1_t2993____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType WordBehaviour_t68_0_0_0;
extern Il2CppType WordBehaviour_t68_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2993_CachedInvokableCall_1__ctor_m15339_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &WordBehaviour_t68_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15339_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WordBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15339_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2993_CachedInvokableCall_1__ctor_m15339_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15339_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2993_CachedInvokableCall_1_Invoke_m15340_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15340_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WordBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15340_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2993_CachedInvokableCall_1_Invoke_m15340_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15340_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2993_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15339_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15340_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m15340_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15344_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2993_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15340_MethodInfo,
	&InvokableCall_1_Find_m15344_MethodInfo,
};
extern Il2CppType UnityAction_1_t2995_0_0_0;
extern TypeInfo UnityAction_1_t2995_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisWordBehaviour_t68_m33531_MethodInfo;
extern TypeInfo WordBehaviour_t68_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15346_MethodInfo;
extern TypeInfo WordBehaviour_t68_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2993_RGCTXData[8] = 
{
	&UnityAction_1_t2995_0_0_0/* Type Usage */,
	&UnityAction_1_t2995_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWordBehaviour_t68_m33531_MethodInfo/* Method Usage */,
	&WordBehaviour_t68_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15346_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15341_MethodInfo/* Method Usage */,
	&WordBehaviour_t68_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15343_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2993_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2993_1_0_0;
struct CachedInvokableCall_1_t2993;
extern Il2CppGenericClass CachedInvokableCall_1_t2993_GenericClass;
TypeInfo CachedInvokableCall_1_t2993_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2993_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2993_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2994_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2993_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2993_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2993_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2993_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2993_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2993_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2993_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2993)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_39.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t2995_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_39MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.WordBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.WordBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisWordBehaviour_t68_m33531(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>
extern Il2CppType UnityAction_1_t2995_0_0_1;
FieldInfo InvokableCall_1_t2994____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2995_0_0_1/* type */
	, &InvokableCall_1_t2994_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2994, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2994_FieldInfos[] =
{
	&InvokableCall_1_t2994____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2994_InvokableCall_1__ctor_m15341_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15341_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15341_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2994_InvokableCall_1__ctor_m15341_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15341_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2995_0_0_0;
static ParameterInfo InvokableCall_1_t2994_InvokableCall_1__ctor_m15342_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2995_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15342_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15342_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2994_InvokableCall_1__ctor_m15342_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15342_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2994_InvokableCall_1_Invoke_m15343_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15343_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15343_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2994_InvokableCall_1_Invoke_m15343_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15343_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2994_InvokableCall_1_Find_m15344_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15344_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.WordBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15344_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2994_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2994_InvokableCall_1_Find_m15344_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15344_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2994_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15341_MethodInfo,
	&InvokableCall_1__ctor_m15342_MethodInfo,
	&InvokableCall_1_Invoke_m15343_MethodInfo,
	&InvokableCall_1_Find_m15344_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2994_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15343_MethodInfo,
	&InvokableCall_1_Find_m15344_MethodInfo,
};
extern TypeInfo UnityAction_1_t2995_il2cpp_TypeInfo;
extern TypeInfo WordBehaviour_t68_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2994_RGCTXData[5] = 
{
	&UnityAction_1_t2995_0_0_0/* Type Usage */,
	&UnityAction_1_t2995_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWordBehaviour_t68_m33531_MethodInfo/* Method Usage */,
	&WordBehaviour_t68_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15346_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2994_0_0_0;
extern Il2CppType InvokableCall_1_t2994_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t2994;
extern Il2CppGenericClass InvokableCall_1_t2994_GenericClass;
TypeInfo InvokableCall_1_t2994_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2994_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2994_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2994_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2994_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2994_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2994_0_0_0/* byval_arg */
	, &InvokableCall_1_t2994_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2994_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2994_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2994)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2995_UnityAction_1__ctor_m15345_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15345_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15345_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2995_UnityAction_1__ctor_m15345_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15345_GenericMethod/* genericMethod */

};
extern Il2CppType WordBehaviour_t68_0_0_0;
static ParameterInfo UnityAction_1_t2995_UnityAction_1_Invoke_m15346_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WordBehaviour_t68_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15346_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15346_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2995_UnityAction_1_Invoke_m15346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15346_GenericMethod/* genericMethod */

};
extern Il2CppType WordBehaviour_t68_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2995_UnityAction_1_BeginInvoke_m15347_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WordBehaviour_t68_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15347_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15347_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2995_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2995_UnityAction_1_BeginInvoke_m15347_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15347_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2995_UnityAction_1_EndInvoke_m15348_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15348_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WordBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15348_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2995_UnityAction_1_EndInvoke_m15348_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15348_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2995_MethodInfos[] =
{
	&UnityAction_1__ctor_m15345_MethodInfo,
	&UnityAction_1_Invoke_m15346_MethodInfo,
	&UnityAction_1_BeginInvoke_m15347_MethodInfo,
	&UnityAction_1_EndInvoke_m15348_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m15347_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15348_MethodInfo;
static MethodInfo* UnityAction_1_t2995_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15346_MethodInfo,
	&UnityAction_1_BeginInvoke_m15347_MethodInfo,
	&UnityAction_1_EndInvoke_m15348_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2995_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2995_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_1_t2995;
extern Il2CppGenericClass UnityAction_1_t2995_GenericClass;
TypeInfo UnityAction_1_t2995_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2995_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2995_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2995_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2995_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2995_0_0_0/* byval_arg */
	, &UnityAction_1_t2995_1_0_0/* this_arg */
	, UnityAction_1_t2995_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2995_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2995)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6135_il2cpp_TypeInfo;

// Area_Script
#include "AssemblyU2DCSharp_Area_Script.h"


// T System.Collections.Generic.IEnumerator`1<Area_Script>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Area_Script>
extern MethodInfo IEnumerator_1_get_Current_m43921_MethodInfo;
static PropertyInfo IEnumerator_1_t6135____Current_PropertyInfo = 
{
	&IEnumerator_1_t6135_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43921_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6135_PropertyInfos[] =
{
	&IEnumerator_1_t6135____Current_PropertyInfo,
	NULL
};
extern Il2CppType Area_Script_t69_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43921_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Area_Script>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43921_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6135_il2cpp_TypeInfo/* declaring_type */
	, &Area_Script_t69_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43921_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6135_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43921_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6135_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6135_0_0_0;
extern Il2CppType IEnumerator_1_t6135_1_0_0;
struct IEnumerator_1_t6135;
extern Il2CppGenericClass IEnumerator_1_t6135_GenericClass;
TypeInfo IEnumerator_1_t6135_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6135_MethodInfos/* methods */
	, IEnumerator_1_t6135_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6135_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6135_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6135_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6135_0_0_0/* byval_arg */
	, &IEnumerator_1_t6135_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6135_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Area_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_111.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2996_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Area_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_111MethodDeclarations.h"

extern TypeInfo Area_Script_t69_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15353_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisArea_Script_t69_m33533_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Area_Script>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Area_Script>(System.Int32)
#define Array_InternalArray__get_Item_TisArea_Script_t69_m33533(__this, p0, method) (Area_Script_t69 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Area_Script>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Area_Script>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Area_Script>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Area_Script>::MoveNext()
// T System.Array/InternalEnumerator`1<Area_Script>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Area_Script>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2996____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2996_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2996, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t2996____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t2996_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2996, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2996_FieldInfos[] =
{
	&InternalEnumerator_1_t2996____array_0_FieldInfo,
	&InternalEnumerator_1_t2996____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15350_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2996____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2996_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15350_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2996____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2996_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15353_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2996_PropertyInfos[] =
{
	&InternalEnumerator_1_t2996____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2996____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2996_InternalEnumerator_1__ctor_m15349_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15349_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Area_Script>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15349_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t2996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t2996_InternalEnumerator_1__ctor_m15349_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15349_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15350_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Area_Script>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15350_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t2996_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15350_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15351_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Area_Script>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15351_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t2996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15351_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15352_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Area_Script>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15352_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t2996_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15352_GenericMethod/* genericMethod */

};
extern Il2CppType Area_Script_t69_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15353_GenericMethod;
// T System.Array/InternalEnumerator`1<Area_Script>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15353_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t2996_il2cpp_TypeInfo/* declaring_type */
	, &Area_Script_t69_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15353_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2996_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15349_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15350_MethodInfo,
	&InternalEnumerator_1_Dispose_m15351_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15352_MethodInfo,
	&InternalEnumerator_1_get_Current_m15353_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15352_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15351_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2996_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15350_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15352_MethodInfo,
	&InternalEnumerator_1_Dispose_m15351_MethodInfo,
	&InternalEnumerator_1_get_Current_m15353_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2996_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6135_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2996_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6135_il2cpp_TypeInfo, 7},
};
extern TypeInfo Area_Script_t69_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2996_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15353_MethodInfo/* Method Usage */,
	&Area_Script_t69_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisArea_Script_t69_m33533_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2996_0_0_0;
extern Il2CppType InternalEnumerator_1_t2996_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2996_GenericClass;
TypeInfo InternalEnumerator_1_t2996_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2996_MethodInfos/* methods */
	, InternalEnumerator_1_t2996_PropertyInfos/* properties */
	, InternalEnumerator_1_t2996_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2996_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2996_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2996_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2996_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2996_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2996_1_0_0/* this_arg */
	, InternalEnumerator_1_t2996_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2996_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2996_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2996)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7835_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Area_Script>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Area_Script>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Area_Script>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Area_Script>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Area_Script>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Area_Script>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Area_Script>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Area_Script>
extern MethodInfo ICollection_1_get_Count_m43922_MethodInfo;
static PropertyInfo ICollection_1_t7835____Count_PropertyInfo = 
{
	&ICollection_1_t7835_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43922_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43923_MethodInfo;
static PropertyInfo ICollection_1_t7835____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7835_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43923_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7835_PropertyInfos[] =
{
	&ICollection_1_t7835____Count_PropertyInfo,
	&ICollection_1_t7835____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43922_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Area_Script>::get_Count()
MethodInfo ICollection_1_get_Count_m43922_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7835_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43922_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43923_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Area_Script>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43923_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7835_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43923_GenericMethod/* genericMethod */

};
extern Il2CppType Area_Script_t69_0_0_0;
extern Il2CppType Area_Script_t69_0_0_0;
static ParameterInfo ICollection_1_t7835_ICollection_1_Add_m43924_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Area_Script_t69_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43924_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Area_Script>::Add(T)
MethodInfo ICollection_1_Add_m43924_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7835_ICollection_1_Add_m43924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43924_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43925_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Area_Script>::Clear()
MethodInfo ICollection_1_Clear_m43925_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43925_GenericMethod/* genericMethod */

};
extern Il2CppType Area_Script_t69_0_0_0;
static ParameterInfo ICollection_1_t7835_ICollection_1_Contains_m43926_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Area_Script_t69_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43926_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Area_Script>::Contains(T)
MethodInfo ICollection_1_Contains_m43926_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7835_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7835_ICollection_1_Contains_m43926_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43926_GenericMethod/* genericMethod */

};
extern Il2CppType Area_ScriptU5BU5D_t5390_0_0_0;
extern Il2CppType Area_ScriptU5BU5D_t5390_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7835_ICollection_1_CopyTo_m43927_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Area_ScriptU5BU5D_t5390_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43927_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Area_Script>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43927_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7835_ICollection_1_CopyTo_m43927_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43927_GenericMethod/* genericMethod */

};
extern Il2CppType Area_Script_t69_0_0_0;
static ParameterInfo ICollection_1_t7835_ICollection_1_Remove_m43928_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Area_Script_t69_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43928_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Area_Script>::Remove(T)
MethodInfo ICollection_1_Remove_m43928_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7835_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7835_ICollection_1_Remove_m43928_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43928_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7835_MethodInfos[] =
{
	&ICollection_1_get_Count_m43922_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43923_MethodInfo,
	&ICollection_1_Add_m43924_MethodInfo,
	&ICollection_1_Clear_m43925_MethodInfo,
	&ICollection_1_Contains_m43926_MethodInfo,
	&ICollection_1_CopyTo_m43927_MethodInfo,
	&ICollection_1_Remove_m43928_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7837_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7835_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7837_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7835_0_0_0;
extern Il2CppType ICollection_1_t7835_1_0_0;
struct ICollection_1_t7835;
extern Il2CppGenericClass ICollection_1_t7835_GenericClass;
TypeInfo ICollection_1_t7835_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7835_MethodInfos/* methods */
	, ICollection_1_t7835_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7835_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7835_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7835_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7835_0_0_0/* byval_arg */
	, &ICollection_1_t7835_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7835_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Area_Script>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Area_Script>
extern Il2CppType IEnumerator_1_t6135_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43929_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Area_Script>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43929_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7837_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6135_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43929_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7837_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43929_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7837_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7837_0_0_0;
extern Il2CppType IEnumerable_1_t7837_1_0_0;
struct IEnumerable_1_t7837;
extern Il2CppGenericClass IEnumerable_1_t7837_GenericClass;
TypeInfo IEnumerable_1_t7837_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7837_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7837_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7837_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7837_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7837_0_0_0/* byval_arg */
	, &IEnumerable_1_t7837_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7837_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7836_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Area_Script>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Area_Script>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Area_Script>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Area_Script>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Area_Script>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Area_Script>
extern MethodInfo IList_1_get_Item_m43930_MethodInfo;
extern MethodInfo IList_1_set_Item_m43931_MethodInfo;
static PropertyInfo IList_1_t7836____Item_PropertyInfo = 
{
	&IList_1_t7836_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43930_MethodInfo/* get */
	, &IList_1_set_Item_m43931_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7836_PropertyInfos[] =
{
	&IList_1_t7836____Item_PropertyInfo,
	NULL
};
extern Il2CppType Area_Script_t69_0_0_0;
static ParameterInfo IList_1_t7836_IList_1_IndexOf_m43932_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Area_Script_t69_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43932_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Area_Script>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43932_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7836_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7836_IList_1_IndexOf_m43932_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43932_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Area_Script_t69_0_0_0;
static ParameterInfo IList_1_t7836_IList_1_Insert_m43933_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Area_Script_t69_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43933_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Area_Script>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43933_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7836_IList_1_Insert_m43933_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43933_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7836_IList_1_RemoveAt_m43934_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43934_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Area_Script>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43934_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7836_IList_1_RemoveAt_m43934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43934_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7836_IList_1_get_Item_m43930_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Area_Script_t69_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43930_GenericMethod;
// T System.Collections.Generic.IList`1<Area_Script>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43930_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7836_il2cpp_TypeInfo/* declaring_type */
	, &Area_Script_t69_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7836_IList_1_get_Item_m43930_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43930_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Area_Script_t69_0_0_0;
static ParameterInfo IList_1_t7836_IList_1_set_Item_m43931_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Area_Script_t69_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43931_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Area_Script>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43931_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7836_IList_1_set_Item_m43931_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43931_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7836_MethodInfos[] =
{
	&IList_1_IndexOf_m43932_MethodInfo,
	&IList_1_Insert_m43933_MethodInfo,
	&IList_1_RemoveAt_m43934_MethodInfo,
	&IList_1_get_Item_m43930_MethodInfo,
	&IList_1_set_Item_m43931_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7836_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7835_il2cpp_TypeInfo,
	&IEnumerable_1_t7837_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7836_0_0_0;
extern Il2CppType IList_1_t7836_1_0_0;
struct IList_1_t7836;
extern Il2CppGenericClass IList_1_t7836_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7836_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7836_MethodInfos/* methods */
	, IList_1_t7836_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7836_il2cpp_TypeInfo/* element_class */
	, IList_1_t7836_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7836_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7836_0_0_0/* byval_arg */
	, &IList_1_t7836_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7836_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Area_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_37.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2997_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Area_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_37MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Area_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_33.h"
extern TypeInfo InvokableCall_1_t2998_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Area_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_33MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15356_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15358_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Area_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Area_Script>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Area_Script>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t2997____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t2997_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2997, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2997_FieldInfos[] =
{
	&CachedInvokableCall_1_t2997____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Area_Script_t69_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2997_CachedInvokableCall_1__ctor_m15354_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Area_Script_t69_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15354_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Area_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15354_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t2997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2997_CachedInvokableCall_1__ctor_m15354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15354_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2997_CachedInvokableCall_1_Invoke_m15355_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15355_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Area_Script>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15355_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t2997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2997_CachedInvokableCall_1_Invoke_m15355_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15355_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2997_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15354_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15355_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15355_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15359_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2997_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15355_MethodInfo,
	&InvokableCall_1_Find_m15359_MethodInfo,
};
extern Il2CppType UnityAction_1_t2999_0_0_0;
extern TypeInfo UnityAction_1_t2999_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisArea_Script_t69_m33543_MethodInfo;
extern TypeInfo Area_Script_t69_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15361_MethodInfo;
extern TypeInfo Area_Script_t69_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2997_RGCTXData[8] = 
{
	&UnityAction_1_t2999_0_0_0/* Type Usage */,
	&UnityAction_1_t2999_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisArea_Script_t69_m33543_MethodInfo/* Method Usage */,
	&Area_Script_t69_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15361_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15356_MethodInfo/* Method Usage */,
	&Area_Script_t69_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15358_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2997_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2997_1_0_0;
struct CachedInvokableCall_1_t2997;
extern Il2CppGenericClass CachedInvokableCall_1_t2997_GenericClass;
TypeInfo CachedInvokableCall_1_t2997_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2997_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2997_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2998_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2997_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2997_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2997_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2997_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2997_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2997_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2997_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2997)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Area_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_40.h"
extern TypeInfo UnityAction_1_t2999_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Area_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_40MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Area_Script>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Area_Script>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisArea_Script_t69_m33543(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Area_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Area_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Area_Script>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Area_Script>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Area_Script>
extern Il2CppType UnityAction_1_t2999_0_0_1;
FieldInfo InvokableCall_1_t2998____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2999_0_0_1/* type */
	, &InvokableCall_1_t2998_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2998, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2998_FieldInfos[] =
{
	&InvokableCall_1_t2998____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2998_InvokableCall_1__ctor_m15356_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15356_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Area_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15356_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t2998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2998_InvokableCall_1__ctor_m15356_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15356_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2999_0_0_0;
static ParameterInfo InvokableCall_1_t2998_InvokableCall_1__ctor_m15357_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2999_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15357_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Area_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15357_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t2998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2998_InvokableCall_1__ctor_m15357_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15357_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t2998_InvokableCall_1_Invoke_m15358_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15358_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Area_Script>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15358_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t2998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t2998_InvokableCall_1_Invoke_m15358_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15358_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t2998_InvokableCall_1_Find_m15359_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15359_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Area_Script>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15359_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t2998_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2998_InvokableCall_1_Find_m15359_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15359_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2998_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15356_MethodInfo,
	&InvokableCall_1__ctor_m15357_MethodInfo,
	&InvokableCall_1_Invoke_m15358_MethodInfo,
	&InvokableCall_1_Find_m15359_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2998_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15358_MethodInfo,
	&InvokableCall_1_Find_m15359_MethodInfo,
};
extern TypeInfo UnityAction_1_t2999_il2cpp_TypeInfo;
extern TypeInfo Area_Script_t69_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2998_RGCTXData[5] = 
{
	&UnityAction_1_t2999_0_0_0/* Type Usage */,
	&UnityAction_1_t2999_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisArea_Script_t69_m33543_MethodInfo/* Method Usage */,
	&Area_Script_t69_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15361_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2998_0_0_0;
extern Il2CppType InvokableCall_1_t2998_1_0_0;
struct InvokableCall_1_t2998;
extern Il2CppGenericClass InvokableCall_1_t2998_GenericClass;
TypeInfo InvokableCall_1_t2998_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2998_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2998_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2998_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2998_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2998_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2998_0_0_0/* byval_arg */
	, &InvokableCall_1_t2998_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2998_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2998_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2998)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Area_Script>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Area_Script>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Area_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Area_Script>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Area_Script>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2999_UnityAction_1__ctor_m15360_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15360_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Area_Script>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15360_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t2999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2999_UnityAction_1__ctor_m15360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15360_GenericMethod/* genericMethod */

};
extern Il2CppType Area_Script_t69_0_0_0;
static ParameterInfo UnityAction_1_t2999_UnityAction_1_Invoke_m15361_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Area_Script_t69_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15361_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Area_Script>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15361_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t2999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2999_UnityAction_1_Invoke_m15361_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15361_GenericMethod/* genericMethod */

};
extern Il2CppType Area_Script_t69_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2999_UnityAction_1_BeginInvoke_m15362_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Area_Script_t69_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15362_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Area_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15362_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t2999_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2999_UnityAction_1_BeginInvoke_m15362_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15362_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t2999_UnityAction_1_EndInvoke_m15363_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15363_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Area_Script>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15363_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t2999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t2999_UnityAction_1_EndInvoke_m15363_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15363_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2999_MethodInfos[] =
{
	&UnityAction_1__ctor_m15360_MethodInfo,
	&UnityAction_1_Invoke_m15361_MethodInfo,
	&UnityAction_1_BeginInvoke_m15362_MethodInfo,
	&UnityAction_1_EndInvoke_m15363_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15362_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15363_MethodInfo;
static MethodInfo* UnityAction_1_t2999_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15361_MethodInfo,
	&UnityAction_1_BeginInvoke_m15362_MethodInfo,
	&UnityAction_1_EndInvoke_m15363_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2999_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2999_1_0_0;
struct UnityAction_1_t2999;
extern Il2CppGenericClass UnityAction_1_t2999_GenericClass;
TypeInfo UnityAction_1_t2999_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2999_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2999_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2999_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2999_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2999_0_0_0/* byval_arg */
	, &UnityAction_1_t2999_1_0_0/* this_arg */
	, UnityAction_1_t2999_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2999_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2999)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<GoalKeeper_Script>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_5.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t3000_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<GoalKeeper_Script>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_5MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<GoalKeeper_Script>
extern Il2CppType GoalKeeper_Script_t77_0_0_6;
FieldInfo CastHelper_1_t3000____t_0_FieldInfo = 
{
	"t"/* name */
	, &GoalKeeper_Script_t77_0_0_6/* type */
	, &CastHelper_1_t3000_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3000, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t3000____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t3000_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3000, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t3000_FieldInfos[] =
{
	&CastHelper_1_t3000____t_0_FieldInfo,
	&CastHelper_1_t3000____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t3000_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t3000_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t3000_0_0_0;
extern Il2CppType CastHelper_1_t3000_1_0_0;
extern Il2CppGenericClass CastHelper_1_t3000_GenericClass;
TypeInfo CastHelper_1_t3000_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t3000_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t3000_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t3000_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t3000_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t3000_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t3000_0_0_0/* byval_arg */
	, &CastHelper_1_t3000_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t3000_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t3000)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6137_il2cpp_TypeInfo;

// Banda_Script
#include "AssemblyU2DCSharp_Banda_Script.h"


// T System.Collections.Generic.IEnumerator`1<Banda_Script>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Banda_Script>
extern MethodInfo IEnumerator_1_get_Current_m43935_MethodInfo;
static PropertyInfo IEnumerator_1_t6137____Current_PropertyInfo = 
{
	&IEnumerator_1_t6137_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43935_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6137_PropertyInfos[] =
{
	&IEnumerator_1_t6137____Current_PropertyInfo,
	NULL
};
extern Il2CppType Banda_Script_t72_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43935_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Banda_Script>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43935_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6137_il2cpp_TypeInfo/* declaring_type */
	, &Banda_Script_t72_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43935_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6137_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43935_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6137_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6137_0_0_0;
extern Il2CppType IEnumerator_1_t6137_1_0_0;
struct IEnumerator_1_t6137;
extern Il2CppGenericClass IEnumerator_1_t6137_GenericClass;
TypeInfo IEnumerator_1_t6137_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6137_MethodInfos/* methods */
	, IEnumerator_1_t6137_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6137_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6137_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6137_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6137_0_0_0/* byval_arg */
	, &IEnumerator_1_t6137_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6137_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Banda_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_112.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3001_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Banda_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_112MethodDeclarations.h"

extern TypeInfo Banda_Script_t72_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15368_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBanda_Script_t72_m33545_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Banda_Script>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Banda_Script>(System.Int32)
#define Array_InternalArray__get_Item_TisBanda_Script_t72_m33545(__this, p0, method) (Banda_Script_t72 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Banda_Script>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Banda_Script>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Banda_Script>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Banda_Script>::MoveNext()
// T System.Array/InternalEnumerator`1<Banda_Script>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Banda_Script>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3001____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3001_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3001, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3001____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3001_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3001, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3001_FieldInfos[] =
{
	&InternalEnumerator_1_t3001____array_0_FieldInfo,
	&InternalEnumerator_1_t3001____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15365_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3001____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3001_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15365_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3001____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3001_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15368_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3001_PropertyInfos[] =
{
	&InternalEnumerator_1_t3001____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3001____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3001_InternalEnumerator_1__ctor_m15364_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15364_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Banda_Script>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15364_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3001_InternalEnumerator_1__ctor_m15364_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15364_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15365_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Banda_Script>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15365_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3001_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15365_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15366_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Banda_Script>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15366_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15366_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15367_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Banda_Script>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15367_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3001_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15367_GenericMethod/* genericMethod */

};
extern Il2CppType Banda_Script_t72_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15368_GenericMethod;
// T System.Array/InternalEnumerator`1<Banda_Script>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15368_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3001_il2cpp_TypeInfo/* declaring_type */
	, &Banda_Script_t72_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15368_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3001_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15364_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15365_MethodInfo,
	&InternalEnumerator_1_Dispose_m15366_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15367_MethodInfo,
	&InternalEnumerator_1_get_Current_m15368_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15367_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15366_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3001_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15365_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15367_MethodInfo,
	&InternalEnumerator_1_Dispose_m15366_MethodInfo,
	&InternalEnumerator_1_get_Current_m15368_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3001_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6137_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3001_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6137_il2cpp_TypeInfo, 7},
};
extern TypeInfo Banda_Script_t72_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3001_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15368_MethodInfo/* Method Usage */,
	&Banda_Script_t72_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisBanda_Script_t72_m33545_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3001_0_0_0;
extern Il2CppType InternalEnumerator_1_t3001_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3001_GenericClass;
TypeInfo InternalEnumerator_1_t3001_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3001_MethodInfos/* methods */
	, InternalEnumerator_1_t3001_PropertyInfos/* properties */
	, InternalEnumerator_1_t3001_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3001_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3001_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3001_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3001_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3001_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3001_1_0_0/* this_arg */
	, InternalEnumerator_1_t3001_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3001_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3001_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3001)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7838_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Banda_Script>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Banda_Script>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Banda_Script>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Banda_Script>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Banda_Script>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Banda_Script>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Banda_Script>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Banda_Script>
extern MethodInfo ICollection_1_get_Count_m43936_MethodInfo;
static PropertyInfo ICollection_1_t7838____Count_PropertyInfo = 
{
	&ICollection_1_t7838_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43936_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43937_MethodInfo;
static PropertyInfo ICollection_1_t7838____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7838_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43937_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7838_PropertyInfos[] =
{
	&ICollection_1_t7838____Count_PropertyInfo,
	&ICollection_1_t7838____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43936_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Banda_Script>::get_Count()
MethodInfo ICollection_1_get_Count_m43936_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7838_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43936_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43937_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Banda_Script>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43937_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7838_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43937_GenericMethod/* genericMethod */

};
extern Il2CppType Banda_Script_t72_0_0_0;
extern Il2CppType Banda_Script_t72_0_0_0;
static ParameterInfo ICollection_1_t7838_ICollection_1_Add_m43938_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Banda_Script_t72_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43938_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Banda_Script>::Add(T)
MethodInfo ICollection_1_Add_m43938_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7838_ICollection_1_Add_m43938_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43938_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43939_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Banda_Script>::Clear()
MethodInfo ICollection_1_Clear_m43939_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43939_GenericMethod/* genericMethod */

};
extern Il2CppType Banda_Script_t72_0_0_0;
static ParameterInfo ICollection_1_t7838_ICollection_1_Contains_m43940_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Banda_Script_t72_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43940_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Banda_Script>::Contains(T)
MethodInfo ICollection_1_Contains_m43940_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7838_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7838_ICollection_1_Contains_m43940_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43940_GenericMethod/* genericMethod */

};
extern Il2CppType Banda_ScriptU5BU5D_t5391_0_0_0;
extern Il2CppType Banda_ScriptU5BU5D_t5391_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7838_ICollection_1_CopyTo_m43941_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Banda_ScriptU5BU5D_t5391_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43941_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Banda_Script>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43941_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7838_ICollection_1_CopyTo_m43941_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43941_GenericMethod/* genericMethod */

};
extern Il2CppType Banda_Script_t72_0_0_0;
static ParameterInfo ICollection_1_t7838_ICollection_1_Remove_m43942_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Banda_Script_t72_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43942_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Banda_Script>::Remove(T)
MethodInfo ICollection_1_Remove_m43942_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7838_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7838_ICollection_1_Remove_m43942_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43942_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7838_MethodInfos[] =
{
	&ICollection_1_get_Count_m43936_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43937_MethodInfo,
	&ICollection_1_Add_m43938_MethodInfo,
	&ICollection_1_Clear_m43939_MethodInfo,
	&ICollection_1_Contains_m43940_MethodInfo,
	&ICollection_1_CopyTo_m43941_MethodInfo,
	&ICollection_1_Remove_m43942_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7840_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7838_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7840_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7838_0_0_0;
extern Il2CppType ICollection_1_t7838_1_0_0;
struct ICollection_1_t7838;
extern Il2CppGenericClass ICollection_1_t7838_GenericClass;
TypeInfo ICollection_1_t7838_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7838_MethodInfos/* methods */
	, ICollection_1_t7838_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7838_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7838_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7838_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7838_0_0_0/* byval_arg */
	, &ICollection_1_t7838_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7838_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Banda_Script>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Banda_Script>
extern Il2CppType IEnumerator_1_t6137_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43943_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Banda_Script>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43943_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7840_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6137_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43943_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7840_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43943_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7840_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7840_0_0_0;
extern Il2CppType IEnumerable_1_t7840_1_0_0;
struct IEnumerable_1_t7840;
extern Il2CppGenericClass IEnumerable_1_t7840_GenericClass;
TypeInfo IEnumerable_1_t7840_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7840_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7840_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7840_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7840_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7840_0_0_0/* byval_arg */
	, &IEnumerable_1_t7840_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7840_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7839_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Banda_Script>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Banda_Script>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Banda_Script>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Banda_Script>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Banda_Script>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Banda_Script>
extern MethodInfo IList_1_get_Item_m43944_MethodInfo;
extern MethodInfo IList_1_set_Item_m43945_MethodInfo;
static PropertyInfo IList_1_t7839____Item_PropertyInfo = 
{
	&IList_1_t7839_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43944_MethodInfo/* get */
	, &IList_1_set_Item_m43945_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7839_PropertyInfos[] =
{
	&IList_1_t7839____Item_PropertyInfo,
	NULL
};
extern Il2CppType Banda_Script_t72_0_0_0;
static ParameterInfo IList_1_t7839_IList_1_IndexOf_m43946_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Banda_Script_t72_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43946_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Banda_Script>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43946_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7839_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7839_IList_1_IndexOf_m43946_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43946_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Banda_Script_t72_0_0_0;
static ParameterInfo IList_1_t7839_IList_1_Insert_m43947_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Banda_Script_t72_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43947_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Banda_Script>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43947_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7839_IList_1_Insert_m43947_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43947_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7839_IList_1_RemoveAt_m43948_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43948_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Banda_Script>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43948_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7839_IList_1_RemoveAt_m43948_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43948_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7839_IList_1_get_Item_m43944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Banda_Script_t72_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43944_GenericMethod;
// T System.Collections.Generic.IList`1<Banda_Script>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43944_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7839_il2cpp_TypeInfo/* declaring_type */
	, &Banda_Script_t72_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7839_IList_1_get_Item_m43944_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43944_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Banda_Script_t72_0_0_0;
static ParameterInfo IList_1_t7839_IList_1_set_Item_m43945_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Banda_Script_t72_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43945_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Banda_Script>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43945_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7839_IList_1_set_Item_m43945_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43945_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7839_MethodInfos[] =
{
	&IList_1_IndexOf_m43946_MethodInfo,
	&IList_1_Insert_m43947_MethodInfo,
	&IList_1_RemoveAt_m43948_MethodInfo,
	&IList_1_get_Item_m43944_MethodInfo,
	&IList_1_set_Item_m43945_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7839_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7838_il2cpp_TypeInfo,
	&IEnumerable_1_t7840_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7839_0_0_0;
extern Il2CppType IList_1_t7839_1_0_0;
struct IList_1_t7839;
extern Il2CppGenericClass IList_1_t7839_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7839_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7839_MethodInfos/* methods */
	, IList_1_t7839_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7839_il2cpp_TypeInfo/* element_class */
	, IList_1_t7839_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7839_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7839_0_0_0/* byval_arg */
	, &IList_1_t7839_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7839_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Banda_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_38.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3002_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Banda_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_38MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Banda_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_34.h"
extern TypeInfo InvokableCall_1_t3003_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Banda_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_34MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15371_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15373_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Banda_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Banda_Script>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Banda_Script>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3002____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3002_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3002, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3002_FieldInfos[] =
{
	&CachedInvokableCall_1_t3002____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Banda_Script_t72_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3002_CachedInvokableCall_1__ctor_m15369_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Banda_Script_t72_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15369_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Banda_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15369_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3002_CachedInvokableCall_1__ctor_m15369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15369_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3002_CachedInvokableCall_1_Invoke_m15370_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15370_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Banda_Script>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15370_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3002_CachedInvokableCall_1_Invoke_m15370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15370_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3002_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15369_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15370_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15370_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15374_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3002_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15370_MethodInfo,
	&InvokableCall_1_Find_m15374_MethodInfo,
};
extern Il2CppType UnityAction_1_t3004_0_0_0;
extern TypeInfo UnityAction_1_t3004_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisBanda_Script_t72_m33555_MethodInfo;
extern TypeInfo Banda_Script_t72_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15376_MethodInfo;
extern TypeInfo Banda_Script_t72_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3002_RGCTXData[8] = 
{
	&UnityAction_1_t3004_0_0_0/* Type Usage */,
	&UnityAction_1_t3004_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBanda_Script_t72_m33555_MethodInfo/* Method Usage */,
	&Banda_Script_t72_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15376_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15371_MethodInfo/* Method Usage */,
	&Banda_Script_t72_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15373_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3002_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3002_1_0_0;
struct CachedInvokableCall_1_t3002;
extern Il2CppGenericClass CachedInvokableCall_1_t3002_GenericClass;
TypeInfo CachedInvokableCall_1_t3002_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3002_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3002_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3003_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3002_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3002_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3002_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3002_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3002_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3002_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3002_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3002)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Banda_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_41.h"
extern TypeInfo UnityAction_1_t3004_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Banda_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_41MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Banda_Script>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Banda_Script>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisBanda_Script_t72_m33555(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Banda_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Banda_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Banda_Script>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Banda_Script>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Banda_Script>
extern Il2CppType UnityAction_1_t3004_0_0_1;
FieldInfo InvokableCall_1_t3003____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3004_0_0_1/* type */
	, &InvokableCall_1_t3003_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3003, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3003_FieldInfos[] =
{
	&InvokableCall_1_t3003____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3003_InvokableCall_1__ctor_m15371_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15371_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Banda_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15371_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3003_InvokableCall_1__ctor_m15371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15371_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3004_0_0_0;
static ParameterInfo InvokableCall_1_t3003_InvokableCall_1__ctor_m15372_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3004_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15372_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Banda_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15372_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3003_InvokableCall_1__ctor_m15372_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15372_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3003_InvokableCall_1_Invoke_m15373_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15373_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Banda_Script>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15373_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3003_InvokableCall_1_Invoke_m15373_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15373_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3003_InvokableCall_1_Find_m15374_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15374_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Banda_Script>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15374_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3003_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3003_InvokableCall_1_Find_m15374_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15374_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3003_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15371_MethodInfo,
	&InvokableCall_1__ctor_m15372_MethodInfo,
	&InvokableCall_1_Invoke_m15373_MethodInfo,
	&InvokableCall_1_Find_m15374_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3003_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15373_MethodInfo,
	&InvokableCall_1_Find_m15374_MethodInfo,
};
extern TypeInfo UnityAction_1_t3004_il2cpp_TypeInfo;
extern TypeInfo Banda_Script_t72_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3003_RGCTXData[5] = 
{
	&UnityAction_1_t3004_0_0_0/* Type Usage */,
	&UnityAction_1_t3004_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBanda_Script_t72_m33555_MethodInfo/* Method Usage */,
	&Banda_Script_t72_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15376_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3003_0_0_0;
extern Il2CppType InvokableCall_1_t3003_1_0_0;
struct InvokableCall_1_t3003;
extern Il2CppGenericClass InvokableCall_1_t3003_GenericClass;
TypeInfo InvokableCall_1_t3003_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3003_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3003_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3003_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3003_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3003_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3003_0_0_0/* byval_arg */
	, &InvokableCall_1_t3003_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3003_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3003_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3003)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Banda_Script>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Banda_Script>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Banda_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Banda_Script>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Banda_Script>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3004_UnityAction_1__ctor_m15375_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15375_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Banda_Script>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15375_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3004_UnityAction_1__ctor_m15375_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15375_GenericMethod/* genericMethod */

};
extern Il2CppType Banda_Script_t72_0_0_0;
static ParameterInfo UnityAction_1_t3004_UnityAction_1_Invoke_m15376_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Banda_Script_t72_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15376_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Banda_Script>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15376_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3004_UnityAction_1_Invoke_m15376_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15376_GenericMethod/* genericMethod */

};
extern Il2CppType Banda_Script_t72_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3004_UnityAction_1_BeginInvoke_m15377_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Banda_Script_t72_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15377_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Banda_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15377_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3004_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3004_UnityAction_1_BeginInvoke_m15377_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15377_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3004_UnityAction_1_EndInvoke_m15378_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15378_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Banda_Script>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15378_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3004_UnityAction_1_EndInvoke_m15378_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15378_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3004_MethodInfos[] =
{
	&UnityAction_1__ctor_m15375_MethodInfo,
	&UnityAction_1_Invoke_m15376_MethodInfo,
	&UnityAction_1_BeginInvoke_m15377_MethodInfo,
	&UnityAction_1_EndInvoke_m15378_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15377_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15378_MethodInfo;
static MethodInfo* UnityAction_1_t3004_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15376_MethodInfo,
	&UnityAction_1_BeginInvoke_m15377_MethodInfo,
	&UnityAction_1_EndInvoke_m15378_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3004_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3004_1_0_0;
struct UnityAction_1_t3004;
extern Il2CppGenericClass UnityAction_1_t3004_GenericClass;
TypeInfo UnityAction_1_t3004_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3004_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3004_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3004_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3004_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3004_0_0_0/* byval_arg */
	, &UnityAction_1_t3004_1_0_0/* this_arg */
	, UnityAction_1_t3004_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3004_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3004)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6139_il2cpp_TypeInfo;

// Sphere
#include "AssemblyU2DCSharp_Sphere.h"


// T System.Collections.Generic.IEnumerator`1<Sphere>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Sphere>
extern MethodInfo IEnumerator_1_get_Current_m43949_MethodInfo;
static PropertyInfo IEnumerator_1_t6139____Current_PropertyInfo = 
{
	&IEnumerator_1_t6139_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43949_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6139_PropertyInfos[] =
{
	&IEnumerator_1_t6139____Current_PropertyInfo,
	NULL
};
extern Il2CppType Sphere_t71_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43949_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Sphere>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43949_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6139_il2cpp_TypeInfo/* declaring_type */
	, &Sphere_t71_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43949_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6139_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43949_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6139_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6139_0_0_0;
extern Il2CppType IEnumerator_1_t6139_1_0_0;
struct IEnumerator_1_t6139;
extern Il2CppGenericClass IEnumerator_1_t6139_GenericClass;
TypeInfo IEnumerator_1_t6139_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6139_MethodInfos/* methods */
	, IEnumerator_1_t6139_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6139_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6139_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6139_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6139_0_0_0/* byval_arg */
	, &IEnumerator_1_t6139_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6139_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Sphere>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_113.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3005_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Sphere>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_113MethodDeclarations.h"

extern TypeInfo Sphere_t71_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15383_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSphere_t71_m33557_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Sphere>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Sphere>(System.Int32)
#define Array_InternalArray__get_Item_TisSphere_t71_m33557(__this, p0, method) (Sphere_t71 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Sphere>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Sphere>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Sphere>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Sphere>::MoveNext()
// T System.Array/InternalEnumerator`1<Sphere>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Sphere>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3005____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3005_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3005, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3005____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3005_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3005, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3005_FieldInfos[] =
{
	&InternalEnumerator_1_t3005____array_0_FieldInfo,
	&InternalEnumerator_1_t3005____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15380_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3005____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3005_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15380_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3005____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3005_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15383_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3005_PropertyInfos[] =
{
	&InternalEnumerator_1_t3005____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3005____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3005_InternalEnumerator_1__ctor_m15379_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15379_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Sphere>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15379_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3005_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3005_InternalEnumerator_1__ctor_m15379_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15379_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15380_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Sphere>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15380_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3005_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15380_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15381_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Sphere>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15381_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3005_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15381_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15382_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Sphere>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15382_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3005_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15382_GenericMethod/* genericMethod */

};
extern Il2CppType Sphere_t71_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15383_GenericMethod;
// T System.Array/InternalEnumerator`1<Sphere>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15383_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3005_il2cpp_TypeInfo/* declaring_type */
	, &Sphere_t71_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15383_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3005_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15379_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15380_MethodInfo,
	&InternalEnumerator_1_Dispose_m15381_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15382_MethodInfo,
	&InternalEnumerator_1_get_Current_m15383_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15382_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15381_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3005_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15380_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15382_MethodInfo,
	&InternalEnumerator_1_Dispose_m15381_MethodInfo,
	&InternalEnumerator_1_get_Current_m15383_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3005_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6139_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3005_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6139_il2cpp_TypeInfo, 7},
};
extern TypeInfo Sphere_t71_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3005_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15383_MethodInfo/* Method Usage */,
	&Sphere_t71_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSphere_t71_m33557_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3005_0_0_0;
extern Il2CppType InternalEnumerator_1_t3005_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3005_GenericClass;
TypeInfo InternalEnumerator_1_t3005_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3005_MethodInfos/* methods */
	, InternalEnumerator_1_t3005_PropertyInfos/* properties */
	, InternalEnumerator_1_t3005_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3005_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3005_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3005_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3005_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3005_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3005_1_0_0/* this_arg */
	, InternalEnumerator_1_t3005_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3005_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3005_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3005)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7841_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Sphere>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Sphere>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Sphere>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Sphere>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Sphere>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Sphere>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Sphere>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Sphere>
extern MethodInfo ICollection_1_get_Count_m43950_MethodInfo;
static PropertyInfo ICollection_1_t7841____Count_PropertyInfo = 
{
	&ICollection_1_t7841_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43950_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43951_MethodInfo;
static PropertyInfo ICollection_1_t7841____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7841_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43951_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7841_PropertyInfos[] =
{
	&ICollection_1_t7841____Count_PropertyInfo,
	&ICollection_1_t7841____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43950_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Sphere>::get_Count()
MethodInfo ICollection_1_get_Count_m43950_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7841_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43950_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43951_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Sphere>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43951_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7841_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43951_GenericMethod/* genericMethod */

};
extern Il2CppType Sphere_t71_0_0_0;
extern Il2CppType Sphere_t71_0_0_0;
static ParameterInfo ICollection_1_t7841_ICollection_1_Add_m43952_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sphere_t71_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43952_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Sphere>::Add(T)
MethodInfo ICollection_1_Add_m43952_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7841_ICollection_1_Add_m43952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43952_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43953_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Sphere>::Clear()
MethodInfo ICollection_1_Clear_m43953_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43953_GenericMethod/* genericMethod */

};
extern Il2CppType Sphere_t71_0_0_0;
static ParameterInfo ICollection_1_t7841_ICollection_1_Contains_m43954_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sphere_t71_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43954_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Sphere>::Contains(T)
MethodInfo ICollection_1_Contains_m43954_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7841_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7841_ICollection_1_Contains_m43954_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43954_GenericMethod/* genericMethod */

};
extern Il2CppType SphereU5BU5D_t5392_0_0_0;
extern Il2CppType SphereU5BU5D_t5392_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7841_ICollection_1_CopyTo_m43955_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SphereU5BU5D_t5392_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43955_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Sphere>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43955_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7841_ICollection_1_CopyTo_m43955_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43955_GenericMethod/* genericMethod */

};
extern Il2CppType Sphere_t71_0_0_0;
static ParameterInfo ICollection_1_t7841_ICollection_1_Remove_m43956_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sphere_t71_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43956_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Sphere>::Remove(T)
MethodInfo ICollection_1_Remove_m43956_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7841_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7841_ICollection_1_Remove_m43956_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43956_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7841_MethodInfos[] =
{
	&ICollection_1_get_Count_m43950_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43951_MethodInfo,
	&ICollection_1_Add_m43952_MethodInfo,
	&ICollection_1_Clear_m43953_MethodInfo,
	&ICollection_1_Contains_m43954_MethodInfo,
	&ICollection_1_CopyTo_m43955_MethodInfo,
	&ICollection_1_Remove_m43956_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7843_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7841_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7843_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7841_0_0_0;
extern Il2CppType ICollection_1_t7841_1_0_0;
struct ICollection_1_t7841;
extern Il2CppGenericClass ICollection_1_t7841_GenericClass;
TypeInfo ICollection_1_t7841_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7841_MethodInfos/* methods */
	, ICollection_1_t7841_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7841_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7841_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7841_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7841_0_0_0/* byval_arg */
	, &ICollection_1_t7841_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7841_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Sphere>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Sphere>
extern Il2CppType IEnumerator_1_t6139_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43957_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Sphere>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43957_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7843_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6139_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43957_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7843_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43957_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7843_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7843_0_0_0;
extern Il2CppType IEnumerable_1_t7843_1_0_0;
struct IEnumerable_1_t7843;
extern Il2CppGenericClass IEnumerable_1_t7843_GenericClass;
TypeInfo IEnumerable_1_t7843_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7843_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7843_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7843_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7843_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7843_0_0_0/* byval_arg */
	, &IEnumerable_1_t7843_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7843_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7842_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Sphere>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Sphere>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Sphere>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Sphere>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Sphere>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Sphere>
extern MethodInfo IList_1_get_Item_m43958_MethodInfo;
extern MethodInfo IList_1_set_Item_m43959_MethodInfo;
static PropertyInfo IList_1_t7842____Item_PropertyInfo = 
{
	&IList_1_t7842_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43958_MethodInfo/* get */
	, &IList_1_set_Item_m43959_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7842_PropertyInfos[] =
{
	&IList_1_t7842____Item_PropertyInfo,
	NULL
};
extern Il2CppType Sphere_t71_0_0_0;
static ParameterInfo IList_1_t7842_IList_1_IndexOf_m43960_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sphere_t71_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43960_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Sphere>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43960_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7842_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7842_IList_1_IndexOf_m43960_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43960_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Sphere_t71_0_0_0;
static ParameterInfo IList_1_t7842_IList_1_Insert_m43961_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Sphere_t71_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43961_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Sphere>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43961_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7842_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7842_IList_1_Insert_m43961_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43961_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7842_IList_1_RemoveAt_m43962_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43962_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Sphere>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43962_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7842_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7842_IList_1_RemoveAt_m43962_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43962_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7842_IList_1_get_Item_m43958_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Sphere_t71_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43958_GenericMethod;
// T System.Collections.Generic.IList`1<Sphere>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43958_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7842_il2cpp_TypeInfo/* declaring_type */
	, &Sphere_t71_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7842_IList_1_get_Item_m43958_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43958_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Sphere_t71_0_0_0;
static ParameterInfo IList_1_t7842_IList_1_set_Item_m43959_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Sphere_t71_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43959_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Sphere>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43959_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7842_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7842_IList_1_set_Item_m43959_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43959_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7842_MethodInfos[] =
{
	&IList_1_IndexOf_m43960_MethodInfo,
	&IList_1_Insert_m43961_MethodInfo,
	&IList_1_RemoveAt_m43962_MethodInfo,
	&IList_1_get_Item_m43958_MethodInfo,
	&IList_1_set_Item_m43959_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7842_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7841_il2cpp_TypeInfo,
	&IEnumerable_1_t7843_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7842_0_0_0;
extern Il2CppType IList_1_t7842_1_0_0;
struct IList_1_t7842;
extern Il2CppGenericClass IList_1_t7842_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7842_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7842_MethodInfos/* methods */
	, IList_1_t7842_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7842_il2cpp_TypeInfo/* element_class */
	, IList_1_t7842_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7842_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7842_0_0_0/* byval_arg */
	, &IList_1_t7842_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7842_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<InGameState_Script>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_6.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t3006_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<InGameState_Script>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_6MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<InGameState_Script>
extern Il2CppType InGameState_Script_t83_0_0_6;
FieldInfo CastHelper_1_t3006____t_0_FieldInfo = 
{
	"t"/* name */
	, &InGameState_Script_t83_0_0_6/* type */
	, &CastHelper_1_t3006_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3006, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t3006____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t3006_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3006, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t3006_FieldInfos[] =
{
	&CastHelper_1_t3006____t_0_FieldInfo,
	&CastHelper_1_t3006____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t3006_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t3006_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t3006_0_0_0;
extern Il2CppType CastHelper_1_t3006_1_0_0;
extern Il2CppGenericClass CastHelper_1_t3006_GenericClass;
TypeInfo CastHelper_1_t3006_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t3006_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t3006_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t3006_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t3006_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t3006_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t3006_0_0_0/* byval_arg */
	, &CastHelper_1_t3006_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t3006_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t3006)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<Player_Script>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_7.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t3007_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<Player_Script>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_7MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<Player_Script>
extern Il2CppType Player_Script_t94_0_0_6;
FieldInfo CastHelper_1_t3007____t_0_FieldInfo = 
{
	"t"/* name */
	, &Player_Script_t94_0_0_6/* type */
	, &CastHelper_1_t3007_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3007, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t3007____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t3007_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3007, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t3007_FieldInfos[] =
{
	&CastHelper_1_t3007____t_0_FieldInfo,
	&CastHelper_1_t3007____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t3007_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t3007_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t3007_0_0_0;
extern Il2CppType CastHelper_1_t3007_1_0_0;
extern Il2CppGenericClass CastHelper_1_t3007_GenericClass;
TypeInfo CastHelper_1_t3007_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t3007_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t3007_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t3007_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t3007_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t3007_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t3007_0_0_0/* byval_arg */
	, &CastHelper_1_t3007_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t3007_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t3007)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6141_il2cpp_TypeInfo;

// Camera_Script
#include "AssemblyU2DCSharp_Camera_Script.h"


// T System.Collections.Generic.IEnumerator`1<Camera_Script>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Camera_Script>
extern MethodInfo IEnumerator_1_get_Current_m43963_MethodInfo;
static PropertyInfo IEnumerator_1_t6141____Current_PropertyInfo = 
{
	&IEnumerator_1_t6141_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43963_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6141_PropertyInfos[] =
{
	&IEnumerator_1_t6141____Current_PropertyInfo,
	NULL
};
extern Il2CppType Camera_Script_t75_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43963_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Camera_Script>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43963_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6141_il2cpp_TypeInfo/* declaring_type */
	, &Camera_Script_t75_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43963_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6141_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43963_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6141_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6141_0_0_0;
extern Il2CppType IEnumerator_1_t6141_1_0_0;
struct IEnumerator_1_t6141;
extern Il2CppGenericClass IEnumerator_1_t6141_GenericClass;
TypeInfo IEnumerator_1_t6141_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6141_MethodInfos/* methods */
	, IEnumerator_1_t6141_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6141_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6141_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6141_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6141_0_0_0/* byval_arg */
	, &IEnumerator_1_t6141_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6141_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Camera_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_114.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3008_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Camera_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_114MethodDeclarations.h"

extern TypeInfo Camera_Script_t75_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15388_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCamera_Script_t75_m33568_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Camera_Script>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Camera_Script>(System.Int32)
#define Array_InternalArray__get_Item_TisCamera_Script_t75_m33568(__this, p0, method) (Camera_Script_t75 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Camera_Script>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Camera_Script>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Camera_Script>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Camera_Script>::MoveNext()
// T System.Array/InternalEnumerator`1<Camera_Script>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Camera_Script>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3008____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3008_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3008, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3008____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3008_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3008, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3008_FieldInfos[] =
{
	&InternalEnumerator_1_t3008____array_0_FieldInfo,
	&InternalEnumerator_1_t3008____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15385_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3008____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3008_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15385_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3008____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3008_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15388_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3008_PropertyInfos[] =
{
	&InternalEnumerator_1_t3008____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3008____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3008_InternalEnumerator_1__ctor_m15384_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15384_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Camera_Script>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15384_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3008_InternalEnumerator_1__ctor_m15384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15384_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15385_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Camera_Script>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15385_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3008_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15385_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15386_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Camera_Script>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15386_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15386_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15387_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Camera_Script>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15387_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3008_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15387_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_Script_t75_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15388_GenericMethod;
// T System.Array/InternalEnumerator`1<Camera_Script>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15388_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3008_il2cpp_TypeInfo/* declaring_type */
	, &Camera_Script_t75_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15388_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3008_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15384_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15385_MethodInfo,
	&InternalEnumerator_1_Dispose_m15386_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15387_MethodInfo,
	&InternalEnumerator_1_get_Current_m15388_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15387_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15386_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3008_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15385_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15387_MethodInfo,
	&InternalEnumerator_1_Dispose_m15386_MethodInfo,
	&InternalEnumerator_1_get_Current_m15388_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3008_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6141_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3008_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6141_il2cpp_TypeInfo, 7},
};
extern TypeInfo Camera_Script_t75_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3008_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15388_MethodInfo/* Method Usage */,
	&Camera_Script_t75_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCamera_Script_t75_m33568_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3008_0_0_0;
extern Il2CppType InternalEnumerator_1_t3008_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3008_GenericClass;
TypeInfo InternalEnumerator_1_t3008_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3008_MethodInfos/* methods */
	, InternalEnumerator_1_t3008_PropertyInfos/* properties */
	, InternalEnumerator_1_t3008_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3008_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3008_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3008_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3008_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3008_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3008_1_0_0/* this_arg */
	, InternalEnumerator_1_t3008_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3008_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3008_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3008)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7844_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Camera_Script>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Camera_Script>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Camera_Script>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Camera_Script>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Camera_Script>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Camera_Script>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Camera_Script>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Camera_Script>
extern MethodInfo ICollection_1_get_Count_m43964_MethodInfo;
static PropertyInfo ICollection_1_t7844____Count_PropertyInfo = 
{
	&ICollection_1_t7844_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43964_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43965_MethodInfo;
static PropertyInfo ICollection_1_t7844____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7844_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43965_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7844_PropertyInfos[] =
{
	&ICollection_1_t7844____Count_PropertyInfo,
	&ICollection_1_t7844____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43964_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Camera_Script>::get_Count()
MethodInfo ICollection_1_get_Count_m43964_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7844_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43964_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43965_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Camera_Script>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43965_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7844_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43965_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_Script_t75_0_0_0;
extern Il2CppType Camera_Script_t75_0_0_0;
static ParameterInfo ICollection_1_t7844_ICollection_1_Add_m43966_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Camera_Script_t75_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43966_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Camera_Script>::Add(T)
MethodInfo ICollection_1_Add_m43966_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7844_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7844_ICollection_1_Add_m43966_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43966_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43967_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Camera_Script>::Clear()
MethodInfo ICollection_1_Clear_m43967_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7844_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43967_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_Script_t75_0_0_0;
static ParameterInfo ICollection_1_t7844_ICollection_1_Contains_m43968_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Camera_Script_t75_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43968_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Camera_Script>::Contains(T)
MethodInfo ICollection_1_Contains_m43968_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7844_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7844_ICollection_1_Contains_m43968_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43968_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_ScriptU5BU5D_t5393_0_0_0;
extern Il2CppType Camera_ScriptU5BU5D_t5393_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7844_ICollection_1_CopyTo_m43969_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Camera_ScriptU5BU5D_t5393_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43969_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Camera_Script>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43969_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7844_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7844_ICollection_1_CopyTo_m43969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43969_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_Script_t75_0_0_0;
static ParameterInfo ICollection_1_t7844_ICollection_1_Remove_m43970_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Camera_Script_t75_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43970_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Camera_Script>::Remove(T)
MethodInfo ICollection_1_Remove_m43970_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7844_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7844_ICollection_1_Remove_m43970_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43970_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7844_MethodInfos[] =
{
	&ICollection_1_get_Count_m43964_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43965_MethodInfo,
	&ICollection_1_Add_m43966_MethodInfo,
	&ICollection_1_Clear_m43967_MethodInfo,
	&ICollection_1_Contains_m43968_MethodInfo,
	&ICollection_1_CopyTo_m43969_MethodInfo,
	&ICollection_1_Remove_m43970_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7846_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7844_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7846_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7844_0_0_0;
extern Il2CppType ICollection_1_t7844_1_0_0;
struct ICollection_1_t7844;
extern Il2CppGenericClass ICollection_1_t7844_GenericClass;
TypeInfo ICollection_1_t7844_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7844_MethodInfos/* methods */
	, ICollection_1_t7844_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7844_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7844_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7844_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7844_0_0_0/* byval_arg */
	, &ICollection_1_t7844_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7844_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Camera_Script>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Camera_Script>
extern Il2CppType IEnumerator_1_t6141_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43971_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Camera_Script>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43971_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7846_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6141_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43971_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7846_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43971_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7846_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7846_0_0_0;
extern Il2CppType IEnumerable_1_t7846_1_0_0;
struct IEnumerable_1_t7846;
extern Il2CppGenericClass IEnumerable_1_t7846_GenericClass;
TypeInfo IEnumerable_1_t7846_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7846_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7846_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7846_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7846_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7846_0_0_0/* byval_arg */
	, &IEnumerable_1_t7846_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7846_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7845_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Camera_Script>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Camera_Script>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Camera_Script>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Camera_Script>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Camera_Script>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Camera_Script>
extern MethodInfo IList_1_get_Item_m43972_MethodInfo;
extern MethodInfo IList_1_set_Item_m43973_MethodInfo;
static PropertyInfo IList_1_t7845____Item_PropertyInfo = 
{
	&IList_1_t7845_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43972_MethodInfo/* get */
	, &IList_1_set_Item_m43973_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7845_PropertyInfos[] =
{
	&IList_1_t7845____Item_PropertyInfo,
	NULL
};
extern Il2CppType Camera_Script_t75_0_0_0;
static ParameterInfo IList_1_t7845_IList_1_IndexOf_m43974_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Camera_Script_t75_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43974_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Camera_Script>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43974_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7845_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7845_IList_1_IndexOf_m43974_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43974_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Camera_Script_t75_0_0_0;
static ParameterInfo IList_1_t7845_IList_1_Insert_m43975_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Camera_Script_t75_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43975_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Camera_Script>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43975_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7845_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7845_IList_1_Insert_m43975_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43975_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7845_IList_1_RemoveAt_m43976_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43976_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Camera_Script>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43976_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7845_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7845_IList_1_RemoveAt_m43976_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43976_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7845_IList_1_get_Item_m43972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Camera_Script_t75_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43972_GenericMethod;
// T System.Collections.Generic.IList`1<Camera_Script>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43972_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7845_il2cpp_TypeInfo/* declaring_type */
	, &Camera_Script_t75_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7845_IList_1_get_Item_m43972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43972_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Camera_Script_t75_0_0_0;
static ParameterInfo IList_1_t7845_IList_1_set_Item_m43973_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Camera_Script_t75_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43973_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Camera_Script>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43973_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7845_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7845_IList_1_set_Item_m43973_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43973_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7845_MethodInfos[] =
{
	&IList_1_IndexOf_m43974_MethodInfo,
	&IList_1_Insert_m43975_MethodInfo,
	&IList_1_RemoveAt_m43976_MethodInfo,
	&IList_1_get_Item_m43972_MethodInfo,
	&IList_1_set_Item_m43973_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7845_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7844_il2cpp_TypeInfo,
	&IEnumerable_1_t7846_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7845_0_0_0;
extern Il2CppType IList_1_t7845_1_0_0;
struct IList_1_t7845;
extern Il2CppGenericClass IList_1_t7845_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7845_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7845_MethodInfos/* methods */
	, IList_1_t7845_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7845_il2cpp_TypeInfo/* element_class */
	, IList_1_t7845_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7845_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7845_0_0_0/* byval_arg */
	, &IList_1_t7845_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7845_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Camera_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_39.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3009_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Camera_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_39MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Camera_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_35.h"
extern TypeInfo InvokableCall_1_t3010_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Camera_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_35MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15391_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15393_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Camera_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Camera_Script>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Camera_Script>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3009____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3009_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3009, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3009_FieldInfos[] =
{
	&CachedInvokableCall_1_t3009____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Camera_Script_t75_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3009_CachedInvokableCall_1__ctor_m15389_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Camera_Script_t75_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15389_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Camera_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15389_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3009_CachedInvokableCall_1__ctor_m15389_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15389_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3009_CachedInvokableCall_1_Invoke_m15390_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15390_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Camera_Script>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15390_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3009_CachedInvokableCall_1_Invoke_m15390_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15390_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3009_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15389_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15390_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15390_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15394_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3009_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15390_MethodInfo,
	&InvokableCall_1_Find_m15394_MethodInfo,
};
extern Il2CppType UnityAction_1_t3011_0_0_0;
extern TypeInfo UnityAction_1_t3011_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCamera_Script_t75_m33578_MethodInfo;
extern TypeInfo Camera_Script_t75_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15396_MethodInfo;
extern TypeInfo Camera_Script_t75_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3009_RGCTXData[8] = 
{
	&UnityAction_1_t3011_0_0_0/* Type Usage */,
	&UnityAction_1_t3011_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCamera_Script_t75_m33578_MethodInfo/* Method Usage */,
	&Camera_Script_t75_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15396_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15391_MethodInfo/* Method Usage */,
	&Camera_Script_t75_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15393_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3009_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3009_1_0_0;
struct CachedInvokableCall_1_t3009;
extern Il2CppGenericClass CachedInvokableCall_1_t3009_GenericClass;
TypeInfo CachedInvokableCall_1_t3009_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3009_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3009_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3010_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3009_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3009_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3009_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3009_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3009_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3009_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3009_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3009)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Camera_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_42.h"
extern TypeInfo UnityAction_1_t3011_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Camera_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_42MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Camera_Script>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Camera_Script>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisCamera_Script_t75_m33578(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Camera_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Camera_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Camera_Script>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Camera_Script>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Camera_Script>
extern Il2CppType UnityAction_1_t3011_0_0_1;
FieldInfo InvokableCall_1_t3010____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3011_0_0_1/* type */
	, &InvokableCall_1_t3010_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3010, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3010_FieldInfos[] =
{
	&InvokableCall_1_t3010____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3010_InvokableCall_1__ctor_m15391_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15391_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Camera_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15391_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3010_InvokableCall_1__ctor_m15391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15391_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3011_0_0_0;
static ParameterInfo InvokableCall_1_t3010_InvokableCall_1__ctor_m15392_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3011_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15392_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Camera_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15392_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3010_InvokableCall_1__ctor_m15392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15392_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3010_InvokableCall_1_Invoke_m15393_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15393_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Camera_Script>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15393_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3010_InvokableCall_1_Invoke_m15393_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15393_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3010_InvokableCall_1_Find_m15394_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15394_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Camera_Script>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15394_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3010_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3010_InvokableCall_1_Find_m15394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15394_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3010_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15391_MethodInfo,
	&InvokableCall_1__ctor_m15392_MethodInfo,
	&InvokableCall_1_Invoke_m15393_MethodInfo,
	&InvokableCall_1_Find_m15394_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3010_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15393_MethodInfo,
	&InvokableCall_1_Find_m15394_MethodInfo,
};
extern TypeInfo UnityAction_1_t3011_il2cpp_TypeInfo;
extern TypeInfo Camera_Script_t75_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3010_RGCTXData[5] = 
{
	&UnityAction_1_t3011_0_0_0/* Type Usage */,
	&UnityAction_1_t3011_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCamera_Script_t75_m33578_MethodInfo/* Method Usage */,
	&Camera_Script_t75_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15396_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3010_0_0_0;
extern Il2CppType InvokableCall_1_t3010_1_0_0;
struct InvokableCall_1_t3010;
extern Il2CppGenericClass InvokableCall_1_t3010_GenericClass;
TypeInfo InvokableCall_1_t3010_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3010_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3010_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3010_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3010_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3010_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3010_0_0_0/* byval_arg */
	, &InvokableCall_1_t3010_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3010_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3010_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3010)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Camera_Script>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Camera_Script>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Camera_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Camera_Script>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Camera_Script>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3011_UnityAction_1__ctor_m15395_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15395_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Camera_Script>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15395_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3011_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3011_UnityAction_1__ctor_m15395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15395_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_Script_t75_0_0_0;
static ParameterInfo UnityAction_1_t3011_UnityAction_1_Invoke_m15396_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Camera_Script_t75_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15396_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Camera_Script>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15396_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3011_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3011_UnityAction_1_Invoke_m15396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15396_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_Script_t75_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3011_UnityAction_1_BeginInvoke_m15397_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Camera_Script_t75_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15397_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Camera_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15397_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3011_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3011_UnityAction_1_BeginInvoke_m15397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15397_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3011_UnityAction_1_EndInvoke_m15398_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15398_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Camera_Script>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15398_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3011_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3011_UnityAction_1_EndInvoke_m15398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15398_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3011_MethodInfos[] =
{
	&UnityAction_1__ctor_m15395_MethodInfo,
	&UnityAction_1_Invoke_m15396_MethodInfo,
	&UnityAction_1_BeginInvoke_m15397_MethodInfo,
	&UnityAction_1_EndInvoke_m15398_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15397_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15398_MethodInfo;
static MethodInfo* UnityAction_1_t3011_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15396_MethodInfo,
	&UnityAction_1_BeginInvoke_m15397_MethodInfo,
	&UnityAction_1_EndInvoke_m15398_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3011_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3011_1_0_0;
struct UnityAction_1_t3011;
extern Il2CppGenericClass UnityAction_1_t3011_GenericClass;
TypeInfo UnityAction_1_t3011_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3011_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3011_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3011_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3011_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3011_0_0_0/* byval_arg */
	, &UnityAction_1_t3011_1_0_0/* this_arg */
	, UnityAction_1_t3011_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3011_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3011)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6143_il2cpp_TypeInfo;

// Corner_Script
#include "AssemblyU2DCSharp_Corner_Script.h"


// T System.Collections.Generic.IEnumerator`1<Corner_Script>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Corner_Script>
extern MethodInfo IEnumerator_1_get_Current_m43977_MethodInfo;
static PropertyInfo IEnumerator_1_t6143____Current_PropertyInfo = 
{
	&IEnumerator_1_t6143_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43977_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6143_PropertyInfos[] =
{
	&IEnumerator_1_t6143____Current_PropertyInfo,
	NULL
};
extern Il2CppType Corner_Script_t76_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43977_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Corner_Script>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43977_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6143_il2cpp_TypeInfo/* declaring_type */
	, &Corner_Script_t76_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43977_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6143_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43977_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6143_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6143_0_0_0;
extern Il2CppType IEnumerator_1_t6143_1_0_0;
struct IEnumerator_1_t6143;
extern Il2CppGenericClass IEnumerator_1_t6143_GenericClass;
TypeInfo IEnumerator_1_t6143_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6143_MethodInfos/* methods */
	, IEnumerator_1_t6143_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6143_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6143_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6143_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6143_0_0_0/* byval_arg */
	, &IEnumerator_1_t6143_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6143_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Corner_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_115.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3012_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Corner_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_115MethodDeclarations.h"

extern TypeInfo Corner_Script_t76_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15403_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCorner_Script_t76_m33580_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Corner_Script>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Corner_Script>(System.Int32)
#define Array_InternalArray__get_Item_TisCorner_Script_t76_m33580(__this, p0, method) (Corner_Script_t76 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Corner_Script>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Corner_Script>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Corner_Script>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Corner_Script>::MoveNext()
// T System.Array/InternalEnumerator`1<Corner_Script>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Corner_Script>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3012____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3012_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3012, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3012____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3012_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3012, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3012_FieldInfos[] =
{
	&InternalEnumerator_1_t3012____array_0_FieldInfo,
	&InternalEnumerator_1_t3012____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15400_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3012____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3012_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15400_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3012____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3012_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15403_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3012_PropertyInfos[] =
{
	&InternalEnumerator_1_t3012____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3012____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3012_InternalEnumerator_1__ctor_m15399_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15399_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Corner_Script>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15399_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3012_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3012_InternalEnumerator_1__ctor_m15399_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15399_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15400_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Corner_Script>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15400_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3012_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15400_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15401_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Corner_Script>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15401_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3012_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15401_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15402_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Corner_Script>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15402_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3012_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15402_GenericMethod/* genericMethod */

};
extern Il2CppType Corner_Script_t76_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15403_GenericMethod;
// T System.Array/InternalEnumerator`1<Corner_Script>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15403_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3012_il2cpp_TypeInfo/* declaring_type */
	, &Corner_Script_t76_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15403_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3012_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15399_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15400_MethodInfo,
	&InternalEnumerator_1_Dispose_m15401_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15402_MethodInfo,
	&InternalEnumerator_1_get_Current_m15403_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15402_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15401_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3012_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15400_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15402_MethodInfo,
	&InternalEnumerator_1_Dispose_m15401_MethodInfo,
	&InternalEnumerator_1_get_Current_m15403_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3012_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6143_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3012_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6143_il2cpp_TypeInfo, 7},
};
extern TypeInfo Corner_Script_t76_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3012_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15403_MethodInfo/* Method Usage */,
	&Corner_Script_t76_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCorner_Script_t76_m33580_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3012_0_0_0;
extern Il2CppType InternalEnumerator_1_t3012_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3012_GenericClass;
TypeInfo InternalEnumerator_1_t3012_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3012_MethodInfos/* methods */
	, InternalEnumerator_1_t3012_PropertyInfos/* properties */
	, InternalEnumerator_1_t3012_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3012_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3012_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3012_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3012_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3012_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3012_1_0_0/* this_arg */
	, InternalEnumerator_1_t3012_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3012_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3012_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3012)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7847_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Corner_Script>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Corner_Script>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Corner_Script>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Corner_Script>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Corner_Script>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Corner_Script>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Corner_Script>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Corner_Script>
extern MethodInfo ICollection_1_get_Count_m43978_MethodInfo;
static PropertyInfo ICollection_1_t7847____Count_PropertyInfo = 
{
	&ICollection_1_t7847_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43978_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43979_MethodInfo;
static PropertyInfo ICollection_1_t7847____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7847_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43979_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7847_PropertyInfos[] =
{
	&ICollection_1_t7847____Count_PropertyInfo,
	&ICollection_1_t7847____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43978_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Corner_Script>::get_Count()
MethodInfo ICollection_1_get_Count_m43978_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7847_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43978_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43979_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Corner_Script>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43979_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7847_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43979_GenericMethod/* genericMethod */

};
extern Il2CppType Corner_Script_t76_0_0_0;
extern Il2CppType Corner_Script_t76_0_0_0;
static ParameterInfo ICollection_1_t7847_ICollection_1_Add_m43980_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Corner_Script_t76_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43980_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Corner_Script>::Add(T)
MethodInfo ICollection_1_Add_m43980_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7847_ICollection_1_Add_m43980_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43980_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43981_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Corner_Script>::Clear()
MethodInfo ICollection_1_Clear_m43981_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43981_GenericMethod/* genericMethod */

};
extern Il2CppType Corner_Script_t76_0_0_0;
static ParameterInfo ICollection_1_t7847_ICollection_1_Contains_m43982_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Corner_Script_t76_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43982_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Corner_Script>::Contains(T)
MethodInfo ICollection_1_Contains_m43982_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7847_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7847_ICollection_1_Contains_m43982_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43982_GenericMethod/* genericMethod */

};
extern Il2CppType Corner_ScriptU5BU5D_t5394_0_0_0;
extern Il2CppType Corner_ScriptU5BU5D_t5394_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7847_ICollection_1_CopyTo_m43983_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Corner_ScriptU5BU5D_t5394_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43983_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Corner_Script>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43983_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7847_ICollection_1_CopyTo_m43983_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43983_GenericMethod/* genericMethod */

};
extern Il2CppType Corner_Script_t76_0_0_0;
static ParameterInfo ICollection_1_t7847_ICollection_1_Remove_m43984_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Corner_Script_t76_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43984_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Corner_Script>::Remove(T)
MethodInfo ICollection_1_Remove_m43984_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7847_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7847_ICollection_1_Remove_m43984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43984_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7847_MethodInfos[] =
{
	&ICollection_1_get_Count_m43978_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43979_MethodInfo,
	&ICollection_1_Add_m43980_MethodInfo,
	&ICollection_1_Clear_m43981_MethodInfo,
	&ICollection_1_Contains_m43982_MethodInfo,
	&ICollection_1_CopyTo_m43983_MethodInfo,
	&ICollection_1_Remove_m43984_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7849_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7847_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7849_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7847_0_0_0;
extern Il2CppType ICollection_1_t7847_1_0_0;
struct ICollection_1_t7847;
extern Il2CppGenericClass ICollection_1_t7847_GenericClass;
TypeInfo ICollection_1_t7847_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7847_MethodInfos/* methods */
	, ICollection_1_t7847_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7847_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7847_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7847_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7847_0_0_0/* byval_arg */
	, &ICollection_1_t7847_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7847_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Corner_Script>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Corner_Script>
extern Il2CppType IEnumerator_1_t6143_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43985_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Corner_Script>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43985_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7849_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6143_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43985_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7849_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43985_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7849_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7849_0_0_0;
extern Il2CppType IEnumerable_1_t7849_1_0_0;
struct IEnumerable_1_t7849;
extern Il2CppGenericClass IEnumerable_1_t7849_GenericClass;
TypeInfo IEnumerable_1_t7849_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7849_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7849_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7849_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7849_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7849_0_0_0/* byval_arg */
	, &IEnumerable_1_t7849_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7849_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7848_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Corner_Script>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Corner_Script>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Corner_Script>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Corner_Script>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Corner_Script>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Corner_Script>
extern MethodInfo IList_1_get_Item_m43986_MethodInfo;
extern MethodInfo IList_1_set_Item_m43987_MethodInfo;
static PropertyInfo IList_1_t7848____Item_PropertyInfo = 
{
	&IList_1_t7848_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m43986_MethodInfo/* get */
	, &IList_1_set_Item_m43987_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7848_PropertyInfos[] =
{
	&IList_1_t7848____Item_PropertyInfo,
	NULL
};
extern Il2CppType Corner_Script_t76_0_0_0;
static ParameterInfo IList_1_t7848_IList_1_IndexOf_m43988_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Corner_Script_t76_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m43988_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Corner_Script>::IndexOf(T)
MethodInfo IList_1_IndexOf_m43988_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7848_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7848_IList_1_IndexOf_m43988_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m43988_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Corner_Script_t76_0_0_0;
static ParameterInfo IList_1_t7848_IList_1_Insert_m43989_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Corner_Script_t76_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m43989_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Corner_Script>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m43989_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7848_IList_1_Insert_m43989_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m43989_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7848_IList_1_RemoveAt_m43990_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m43990_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Corner_Script>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m43990_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7848_IList_1_RemoveAt_m43990_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m43990_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7848_IList_1_get_Item_m43986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Corner_Script_t76_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m43986_GenericMethod;
// T System.Collections.Generic.IList`1<Corner_Script>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m43986_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7848_il2cpp_TypeInfo/* declaring_type */
	, &Corner_Script_t76_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7848_IList_1_get_Item_m43986_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m43986_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Corner_Script_t76_0_0_0;
static ParameterInfo IList_1_t7848_IList_1_set_Item_m43987_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Corner_Script_t76_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m43987_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Corner_Script>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m43987_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7848_IList_1_set_Item_m43987_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m43987_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7848_MethodInfos[] =
{
	&IList_1_IndexOf_m43988_MethodInfo,
	&IList_1_Insert_m43989_MethodInfo,
	&IList_1_RemoveAt_m43990_MethodInfo,
	&IList_1_get_Item_m43986_MethodInfo,
	&IList_1_set_Item_m43987_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7848_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7847_il2cpp_TypeInfo,
	&IEnumerable_1_t7849_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7848_0_0_0;
extern Il2CppType IList_1_t7848_1_0_0;
struct IList_1_t7848;
extern Il2CppGenericClass IList_1_t7848_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7848_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7848_MethodInfos/* methods */
	, IList_1_t7848_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7848_il2cpp_TypeInfo/* element_class */
	, IList_1_t7848_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7848_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7848_0_0_0/* byval_arg */
	, &IList_1_t7848_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7848_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Corner_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_40.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3013_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Corner_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_40MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Corner_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_36.h"
extern TypeInfo InvokableCall_1_t3014_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Corner_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_36MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15406_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15408_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Corner_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Corner_Script>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Corner_Script>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3013____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3013_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3013, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3013_FieldInfos[] =
{
	&CachedInvokableCall_1_t3013____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Corner_Script_t76_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3013_CachedInvokableCall_1__ctor_m15404_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Corner_Script_t76_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15404_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Corner_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15404_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3013_CachedInvokableCall_1__ctor_m15404_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15404_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3013_CachedInvokableCall_1_Invoke_m15405_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15405_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Corner_Script>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15405_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3013_CachedInvokableCall_1_Invoke_m15405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15405_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3013_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15404_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15405_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15405_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15409_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3013_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15405_MethodInfo,
	&InvokableCall_1_Find_m15409_MethodInfo,
};
extern Il2CppType UnityAction_1_t3015_0_0_0;
extern TypeInfo UnityAction_1_t3015_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCorner_Script_t76_m33590_MethodInfo;
extern TypeInfo Corner_Script_t76_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15411_MethodInfo;
extern TypeInfo Corner_Script_t76_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3013_RGCTXData[8] = 
{
	&UnityAction_1_t3015_0_0_0/* Type Usage */,
	&UnityAction_1_t3015_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCorner_Script_t76_m33590_MethodInfo/* Method Usage */,
	&Corner_Script_t76_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15411_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15406_MethodInfo/* Method Usage */,
	&Corner_Script_t76_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15408_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3013_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3013_1_0_0;
struct CachedInvokableCall_1_t3013;
extern Il2CppGenericClass CachedInvokableCall_1_t3013_GenericClass;
TypeInfo CachedInvokableCall_1_t3013_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3013_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3013_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3014_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3013_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3013_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3013_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3013_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3013_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3013_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3013_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3013)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Corner_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_43.h"
extern TypeInfo UnityAction_1_t3015_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Corner_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_43MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Corner_Script>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Corner_Script>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisCorner_Script_t76_m33590(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Corner_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Corner_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Corner_Script>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Corner_Script>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Corner_Script>
extern Il2CppType UnityAction_1_t3015_0_0_1;
FieldInfo InvokableCall_1_t3014____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3015_0_0_1/* type */
	, &InvokableCall_1_t3014_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3014, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3014_FieldInfos[] =
{
	&InvokableCall_1_t3014____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3014_InvokableCall_1__ctor_m15406_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15406_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Corner_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15406_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3014_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3014_InvokableCall_1__ctor_m15406_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15406_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3015_0_0_0;
static ParameterInfo InvokableCall_1_t3014_InvokableCall_1__ctor_m15407_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3015_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15407_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Corner_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15407_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3014_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3014_InvokableCall_1__ctor_m15407_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15407_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3014_InvokableCall_1_Invoke_m15408_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15408_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Corner_Script>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15408_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3014_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3014_InvokableCall_1_Invoke_m15408_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15408_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3014_InvokableCall_1_Find_m15409_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15409_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Corner_Script>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15409_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3014_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3014_InvokableCall_1_Find_m15409_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15409_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3014_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15406_MethodInfo,
	&InvokableCall_1__ctor_m15407_MethodInfo,
	&InvokableCall_1_Invoke_m15408_MethodInfo,
	&InvokableCall_1_Find_m15409_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3014_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15408_MethodInfo,
	&InvokableCall_1_Find_m15409_MethodInfo,
};
extern TypeInfo UnityAction_1_t3015_il2cpp_TypeInfo;
extern TypeInfo Corner_Script_t76_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3014_RGCTXData[5] = 
{
	&UnityAction_1_t3015_0_0_0/* Type Usage */,
	&UnityAction_1_t3015_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCorner_Script_t76_m33590_MethodInfo/* Method Usage */,
	&Corner_Script_t76_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15411_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3014_0_0_0;
extern Il2CppType InvokableCall_1_t3014_1_0_0;
struct InvokableCall_1_t3014;
extern Il2CppGenericClass InvokableCall_1_t3014_GenericClass;
TypeInfo InvokableCall_1_t3014_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3014_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3014_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3014_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3014_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3014_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3014_0_0_0/* byval_arg */
	, &InvokableCall_1_t3014_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3014_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3014_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3014)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Corner_Script>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Corner_Script>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Corner_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Corner_Script>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Corner_Script>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3015_UnityAction_1__ctor_m15410_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15410_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Corner_Script>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15410_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3015_UnityAction_1__ctor_m15410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15410_GenericMethod/* genericMethod */

};
extern Il2CppType Corner_Script_t76_0_0_0;
static ParameterInfo UnityAction_1_t3015_UnityAction_1_Invoke_m15411_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Corner_Script_t76_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15411_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Corner_Script>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15411_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3015_UnityAction_1_Invoke_m15411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15411_GenericMethod/* genericMethod */

};
extern Il2CppType Corner_Script_t76_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3015_UnityAction_1_BeginInvoke_m15412_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Corner_Script_t76_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15412_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Corner_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15412_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3015_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3015_UnityAction_1_BeginInvoke_m15412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15412_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3015_UnityAction_1_EndInvoke_m15413_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15413_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Corner_Script>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15413_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3015_UnityAction_1_EndInvoke_m15413_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15413_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3015_MethodInfos[] =
{
	&UnityAction_1__ctor_m15410_MethodInfo,
	&UnityAction_1_Invoke_m15411_MethodInfo,
	&UnityAction_1_BeginInvoke_m15412_MethodInfo,
	&UnityAction_1_EndInvoke_m15413_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15412_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15413_MethodInfo;
static MethodInfo* UnityAction_1_t3015_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15411_MethodInfo,
	&UnityAction_1_BeginInvoke_m15412_MethodInfo,
	&UnityAction_1_EndInvoke_m15413_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3015_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3015_1_0_0;
struct UnityAction_1_t3015;
extern Il2CppGenericClass UnityAction_1_t3015_GenericClass;
TypeInfo UnityAction_1_t3015_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3015_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3015_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3015_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3015_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3015_0_0_0/* byval_arg */
	, &UnityAction_1_t3015_1_0_0/* this_arg */
	, UnityAction_1_t3015_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3015_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3015)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6145_il2cpp_TypeInfo;

// GoalKeeperJump
#include "AssemblyU2DCSharp_GoalKeeperJump.h"


// T System.Collections.Generic.IEnumerator`1<GoalKeeperJump>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<GoalKeeperJump>
extern MethodInfo IEnumerator_1_get_Current_m43991_MethodInfo;
static PropertyInfo IEnumerator_1_t6145____Current_PropertyInfo = 
{
	&IEnumerator_1_t6145_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m43991_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6145_PropertyInfos[] =
{
	&IEnumerator_1_t6145____Current_PropertyInfo,
	NULL
};
extern Il2CppType GoalKeeperJump_t78_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m43991_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<GoalKeeperJump>::get_Current()
MethodInfo IEnumerator_1_get_Current_m43991_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6145_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeperJump_t78_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m43991_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6145_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m43991_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6145_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6145_0_0_0;
extern Il2CppType IEnumerator_1_t6145_1_0_0;
struct IEnumerator_1_t6145;
extern Il2CppGenericClass IEnumerator_1_t6145_GenericClass;
TypeInfo IEnumerator_1_t6145_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6145_MethodInfos/* methods */
	, IEnumerator_1_t6145_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6145_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6145_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6145_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6145_0_0_0/* byval_arg */
	, &IEnumerator_1_t6145_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6145_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<GoalKeeperJump>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_116.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3016_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<GoalKeeperJump>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_116MethodDeclarations.h"

extern TypeInfo GoalKeeperJump_t78_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15418_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGoalKeeperJump_t78_m33592_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<GoalKeeperJump>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<GoalKeeperJump>(System.Int32)
#define Array_InternalArray__get_Item_TisGoalKeeperJump_t78_m33592(__this, p0, method) (GoalKeeperJump_t78 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<GoalKeeperJump>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<GoalKeeperJump>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<GoalKeeperJump>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<GoalKeeperJump>::MoveNext()
// T System.Array/InternalEnumerator`1<GoalKeeperJump>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<GoalKeeperJump>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3016____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3016_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3016, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3016____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3016_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3016, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3016_FieldInfos[] =
{
	&InternalEnumerator_1_t3016____array_0_FieldInfo,
	&InternalEnumerator_1_t3016____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15415_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3016____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3016_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15415_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3016____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3016_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15418_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3016_PropertyInfos[] =
{
	&InternalEnumerator_1_t3016____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3016____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3016_InternalEnumerator_1__ctor_m15414_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15414_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<GoalKeeperJump>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15414_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3016_InternalEnumerator_1__ctor_m15414_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15414_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15415_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<GoalKeeperJump>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15415_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3016_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15415_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15416_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<GoalKeeperJump>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15416_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15416_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15417_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<GoalKeeperJump>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15417_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3016_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15417_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_t78_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15418_GenericMethod;
// T System.Array/InternalEnumerator`1<GoalKeeperJump>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15418_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3016_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeperJump_t78_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15418_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3016_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15414_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15415_MethodInfo,
	&InternalEnumerator_1_Dispose_m15416_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15417_MethodInfo,
	&InternalEnumerator_1_get_Current_m15418_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15417_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15416_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3016_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15415_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15417_MethodInfo,
	&InternalEnumerator_1_Dispose_m15416_MethodInfo,
	&InternalEnumerator_1_get_Current_m15418_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3016_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6145_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3016_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6145_il2cpp_TypeInfo, 7},
};
extern TypeInfo GoalKeeperJump_t78_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3016_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15418_MethodInfo/* Method Usage */,
	&GoalKeeperJump_t78_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGoalKeeperJump_t78_m33592_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3016_0_0_0;
extern Il2CppType InternalEnumerator_1_t3016_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3016_GenericClass;
TypeInfo InternalEnumerator_1_t3016_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3016_MethodInfos/* methods */
	, InternalEnumerator_1_t3016_PropertyInfos/* properties */
	, InternalEnumerator_1_t3016_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3016_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3016_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3016_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3016_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3016_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3016_1_0_0/* this_arg */
	, InternalEnumerator_1_t3016_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3016_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3016_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3016)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7850_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<GoalKeeperJump>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<GoalKeeperJump>
extern MethodInfo ICollection_1_get_Count_m43992_MethodInfo;
static PropertyInfo ICollection_1_t7850____Count_PropertyInfo = 
{
	&ICollection_1_t7850_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m43992_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m43993_MethodInfo;
static PropertyInfo ICollection_1_t7850____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7850_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m43993_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7850_PropertyInfos[] =
{
	&ICollection_1_t7850____Count_PropertyInfo,
	&ICollection_1_t7850____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m43992_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<GoalKeeperJump>::get_Count()
MethodInfo ICollection_1_get_Count_m43992_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7850_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m43992_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m43993_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m43993_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7850_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m43993_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_t78_0_0_0;
extern Il2CppType GoalKeeperJump_t78_0_0_0;
static ParameterInfo ICollection_1_t7850_ICollection_1_Add_m43994_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_t78_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m43994_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump>::Add(T)
MethodInfo ICollection_1_Add_m43994_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7850_ICollection_1_Add_m43994_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m43994_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m43995_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump>::Clear()
MethodInfo ICollection_1_Clear_m43995_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m43995_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_t78_0_0_0;
static ParameterInfo ICollection_1_t7850_ICollection_1_Contains_m43996_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_t78_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m43996_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump>::Contains(T)
MethodInfo ICollection_1_Contains_m43996_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7850_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7850_ICollection_1_Contains_m43996_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m43996_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJumpU5BU5D_t5395_0_0_0;
extern Il2CppType GoalKeeperJumpU5BU5D_t5395_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7850_ICollection_1_CopyTo_m43997_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJumpU5BU5D_t5395_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m43997_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m43997_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7850_ICollection_1_CopyTo_m43997_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m43997_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_t78_0_0_0;
static ParameterInfo ICollection_1_t7850_ICollection_1_Remove_m43998_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_t78_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m43998_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump>::Remove(T)
MethodInfo ICollection_1_Remove_m43998_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7850_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7850_ICollection_1_Remove_m43998_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m43998_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7850_MethodInfos[] =
{
	&ICollection_1_get_Count_m43992_MethodInfo,
	&ICollection_1_get_IsReadOnly_m43993_MethodInfo,
	&ICollection_1_Add_m43994_MethodInfo,
	&ICollection_1_Clear_m43995_MethodInfo,
	&ICollection_1_Contains_m43996_MethodInfo,
	&ICollection_1_CopyTo_m43997_MethodInfo,
	&ICollection_1_Remove_m43998_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7852_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7850_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7852_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7850_0_0_0;
extern Il2CppType ICollection_1_t7850_1_0_0;
struct ICollection_1_t7850;
extern Il2CppGenericClass ICollection_1_t7850_GenericClass;
TypeInfo ICollection_1_t7850_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7850_MethodInfos/* methods */
	, ICollection_1_t7850_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7850_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7850_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7850_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7850_0_0_0/* byval_arg */
	, &ICollection_1_t7850_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7850_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<GoalKeeperJump>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<GoalKeeperJump>
extern Il2CppType IEnumerator_1_t6145_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m43999_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<GoalKeeperJump>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m43999_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7852_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6145_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m43999_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7852_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m43999_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7852_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7852_0_0_0;
extern Il2CppType IEnumerable_1_t7852_1_0_0;
struct IEnumerable_1_t7852;
extern Il2CppGenericClass IEnumerable_1_t7852_GenericClass;
TypeInfo IEnumerable_1_t7852_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7852_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7852_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7852_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7852_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7852_0_0_0/* byval_arg */
	, &IEnumerable_1_t7852_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7852_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7851_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<GoalKeeperJump>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<GoalKeeperJump>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<GoalKeeperJump>
extern MethodInfo IList_1_get_Item_m44000_MethodInfo;
extern MethodInfo IList_1_set_Item_m44001_MethodInfo;
static PropertyInfo IList_1_t7851____Item_PropertyInfo = 
{
	&IList_1_t7851_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44000_MethodInfo/* get */
	, &IList_1_set_Item_m44001_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7851_PropertyInfos[] =
{
	&IList_1_t7851____Item_PropertyInfo,
	NULL
};
extern Il2CppType GoalKeeperJump_t78_0_0_0;
static ParameterInfo IList_1_t7851_IList_1_IndexOf_m44002_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_t78_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44002_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<GoalKeeperJump>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44002_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7851_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7851_IList_1_IndexOf_m44002_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44002_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GoalKeeperJump_t78_0_0_0;
static ParameterInfo IList_1_t7851_IList_1_Insert_m44003_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_t78_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44003_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44003_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7851_IList_1_Insert_m44003_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44003_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7851_IList_1_RemoveAt_m44004_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44004_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44004_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7851_IList_1_RemoveAt_m44004_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44004_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7851_IList_1_get_Item_m44000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType GoalKeeperJump_t78_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44000_GenericMethod;
// T System.Collections.Generic.IList`1<GoalKeeperJump>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44000_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7851_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeperJump_t78_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7851_IList_1_get_Item_m44000_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44000_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GoalKeeperJump_t78_0_0_0;
static ParameterInfo IList_1_t7851_IList_1_set_Item_m44001_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_t78_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44001_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44001_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7851_IList_1_set_Item_m44001_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44001_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7851_MethodInfos[] =
{
	&IList_1_IndexOf_m44002_MethodInfo,
	&IList_1_Insert_m44003_MethodInfo,
	&IList_1_RemoveAt_m44004_MethodInfo,
	&IList_1_get_Item_m44000_MethodInfo,
	&IList_1_set_Item_m44001_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7851_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7850_il2cpp_TypeInfo,
	&IEnumerable_1_t7852_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7851_0_0_0;
extern Il2CppType IList_1_t7851_1_0_0;
struct IList_1_t7851;
extern Il2CppGenericClass IList_1_t7851_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7851_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7851_MethodInfos/* methods */
	, IList_1_t7851_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7851_il2cpp_TypeInfo/* element_class */
	, IList_1_t7851_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7851_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7851_0_0_0/* byval_arg */
	, &IList_1_t7851_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7851_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_41.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3017_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_41MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<GoalKeeperJump>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_37.h"
extern TypeInfo InvokableCall_1_t3018_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<GoalKeeperJump>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_37MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15421_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15423_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3017____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3017_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3017, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3017_FieldInfos[] =
{
	&CachedInvokableCall_1_t3017____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType GoalKeeperJump_t78_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3017_CachedInvokableCall_1__ctor_m15419_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_t78_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15419_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15419_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3017_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3017_CachedInvokableCall_1__ctor_m15419_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15419_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3017_CachedInvokableCall_1_Invoke_m15420_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15420_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15420_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3017_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3017_CachedInvokableCall_1_Invoke_m15420_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15420_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3017_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15419_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15420_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15420_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15424_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3017_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15420_MethodInfo,
	&InvokableCall_1_Find_m15424_MethodInfo,
};
extern Il2CppType UnityAction_1_t3019_0_0_0;
extern TypeInfo UnityAction_1_t3019_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeperJump_t78_m33602_MethodInfo;
extern TypeInfo GoalKeeperJump_t78_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15426_MethodInfo;
extern TypeInfo GoalKeeperJump_t78_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3017_RGCTXData[8] = 
{
	&UnityAction_1_t3019_0_0_0/* Type Usage */,
	&UnityAction_1_t3019_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeperJump_t78_m33602_MethodInfo/* Method Usage */,
	&GoalKeeperJump_t78_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15426_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15421_MethodInfo/* Method Usage */,
	&GoalKeeperJump_t78_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15423_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3017_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3017_1_0_0;
struct CachedInvokableCall_1_t3017;
extern Il2CppGenericClass CachedInvokableCall_1_t3017_GenericClass;
TypeInfo CachedInvokableCall_1_t3017_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3017_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3017_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3018_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3017_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3017_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3017_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3017_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3017_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3017_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3017_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3017)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<GoalKeeperJump>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_44.h"
extern TypeInfo UnityAction_1_t3019_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<GoalKeeperJump>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_44MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<GoalKeeperJump>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<GoalKeeperJump>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeperJump_t78_m33602(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<GoalKeeperJump>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<GoalKeeperJump>
extern Il2CppType UnityAction_1_t3019_0_0_1;
FieldInfo InvokableCall_1_t3018____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3019_0_0_1/* type */
	, &InvokableCall_1_t3018_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3018, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3018_FieldInfos[] =
{
	&InvokableCall_1_t3018____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3018_InvokableCall_1__ctor_m15421_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15421_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15421_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3018_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3018_InvokableCall_1__ctor_m15421_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15421_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3019_0_0_0;
static ParameterInfo InvokableCall_1_t3018_InvokableCall_1__ctor_m15422_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3019_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15422_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15422_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3018_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3018_InvokableCall_1__ctor_m15422_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15422_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3018_InvokableCall_1_Invoke_m15423_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15423_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15423_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3018_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3018_InvokableCall_1_Invoke_m15423_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15423_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3018_InvokableCall_1_Find_m15424_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15424_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<GoalKeeperJump>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15424_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3018_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3018_InvokableCall_1_Find_m15424_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15424_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3018_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15421_MethodInfo,
	&InvokableCall_1__ctor_m15422_MethodInfo,
	&InvokableCall_1_Invoke_m15423_MethodInfo,
	&InvokableCall_1_Find_m15424_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3018_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15423_MethodInfo,
	&InvokableCall_1_Find_m15424_MethodInfo,
};
extern TypeInfo UnityAction_1_t3019_il2cpp_TypeInfo;
extern TypeInfo GoalKeeperJump_t78_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3018_RGCTXData[5] = 
{
	&UnityAction_1_t3019_0_0_0/* Type Usage */,
	&UnityAction_1_t3019_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeperJump_t78_m33602_MethodInfo/* Method Usage */,
	&GoalKeeperJump_t78_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15426_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3018_0_0_0;
extern Il2CppType InvokableCall_1_t3018_1_0_0;
struct InvokableCall_1_t3018;
extern Il2CppGenericClass InvokableCall_1_t3018_GenericClass;
TypeInfo InvokableCall_1_t3018_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3018_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3018_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3018_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3018_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3018_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3018_0_0_0/* byval_arg */
	, &InvokableCall_1_t3018_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3018_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3018_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3018)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<GoalKeeperJump>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<GoalKeeperJump>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3019_UnityAction_1__ctor_m15425_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15425_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15425_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3019_UnityAction_1__ctor_m15425_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15425_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_t78_0_0_0;
static ParameterInfo UnityAction_1_t3019_UnityAction_1_Invoke_m15426_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_t78_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15426_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15426_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3019_UnityAction_1_Invoke_m15426_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15426_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_t78_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3019_UnityAction_1_BeginInvoke_m15427_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_t78_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15427_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<GoalKeeperJump>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15427_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3019_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3019_UnityAction_1_BeginInvoke_m15427_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15427_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3019_UnityAction_1_EndInvoke_m15428_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15428_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15428_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3019_UnityAction_1_EndInvoke_m15428_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15428_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3019_MethodInfos[] =
{
	&UnityAction_1__ctor_m15425_MethodInfo,
	&UnityAction_1_Invoke_m15426_MethodInfo,
	&UnityAction_1_BeginInvoke_m15427_MethodInfo,
	&UnityAction_1_EndInvoke_m15428_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15427_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15428_MethodInfo;
static MethodInfo* UnityAction_1_t3019_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15426_MethodInfo,
	&UnityAction_1_BeginInvoke_m15427_MethodInfo,
	&UnityAction_1_EndInvoke_m15428_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3019_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3019_1_0_0;
struct UnityAction_1_t3019;
extern Il2CppGenericClass UnityAction_1_t3019_GenericClass;
TypeInfo UnityAction_1_t3019_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3019_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3019_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3019_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3019_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3019_0_0_0/* byval_arg */
	, &UnityAction_1_t3019_1_0_0/* this_arg */
	, UnityAction_1_t3019_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3019_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3019)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.Rigidbody>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_8.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t3020_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.Rigidbody>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_8MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.Rigidbody>
extern Il2CppType Rigidbody_t180_0_0_6;
FieldInfo CastHelper_1_t3020____t_0_FieldInfo = 
{
	"t"/* name */
	, &Rigidbody_t180_0_0_6/* type */
	, &CastHelper_1_t3020_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3020, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t3020____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t3020_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3020, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t3020_FieldInfos[] =
{
	&CastHelper_1_t3020____t_0_FieldInfo,
	&CastHelper_1_t3020____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t3020_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t3020_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t3020_0_0_0;
extern Il2CppType CastHelper_1_t3020_1_0_0;
extern Il2CppGenericClass CastHelper_1_t3020_GenericClass;
TypeInfo CastHelper_1_t3020_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t3020_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t3020_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t3020_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t3020_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t3020_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t3020_0_0_0/* byval_arg */
	, &CastHelper_1_t3020_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t3020_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t3020)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.Animation>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_9.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t3021_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.Animation>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_9MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.Animation>
extern Il2CppType Animation_t181_0_0_6;
FieldInfo CastHelper_1_t3021____t_0_FieldInfo = 
{
	"t"/* name */
	, &Animation_t181_0_0_6/* type */
	, &CastHelper_1_t3021_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3021, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t3021____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t3021_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3021, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t3021_FieldInfos[] =
{
	&CastHelper_1_t3021____t_0_FieldInfo,
	&CastHelper_1_t3021____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t3021_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t3021_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t3021_0_0_0;
extern Il2CppType CastHelper_1_t3021_1_0_0;
extern Il2CppGenericClass CastHelper_1_t3021_GenericClass;
TypeInfo CastHelper_1_t3021_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t3021_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t3021_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t3021_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t3021_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t3021_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t3021_0_0_0/* byval_arg */
	, &CastHelper_1_t3021_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t3021_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t3021)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6147_il2cpp_TypeInfo;

// GoalKeeperJump_Down
#include "AssemblyU2DCSharp_GoalKeeperJump_Down.h"


// T System.Collections.Generic.IEnumerator`1<GoalKeeperJump_Down>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<GoalKeeperJump_Down>
extern MethodInfo IEnumerator_1_get_Current_m44005_MethodInfo;
static PropertyInfo IEnumerator_1_t6147____Current_PropertyInfo = 
{
	&IEnumerator_1_t6147_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44005_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6147_PropertyInfos[] =
{
	&IEnumerator_1_t6147____Current_PropertyInfo,
	NULL
};
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44005_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<GoalKeeperJump_Down>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44005_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6147_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeperJump_Down_t79_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44005_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6147_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44005_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6147_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6147_0_0_0;
extern Il2CppType IEnumerator_1_t6147_1_0_0;
struct IEnumerator_1_t6147;
extern Il2CppGenericClass IEnumerator_1_t6147_GenericClass;
TypeInfo IEnumerator_1_t6147_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6147_MethodInfos/* methods */
	, IEnumerator_1_t6147_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6147_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6147_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6147_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6147_0_0_0/* byval_arg */
	, &IEnumerator_1_t6147_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6147_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<GoalKeeperJump_Down>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_117.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3022_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<GoalKeeperJump_Down>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_117MethodDeclarations.h"

extern TypeInfo GoalKeeperJump_Down_t79_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15433_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGoalKeeperJump_Down_t79_m33604_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<GoalKeeperJump_Down>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<GoalKeeperJump_Down>(System.Int32)
#define Array_InternalArray__get_Item_TisGoalKeeperJump_Down_t79_m33604(__this, p0, method) (GoalKeeperJump_Down_t79 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::MoveNext()
// T System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<GoalKeeperJump_Down>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3022____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3022_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3022, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3022____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3022_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3022, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3022_FieldInfos[] =
{
	&InternalEnumerator_1_t3022____array_0_FieldInfo,
	&InternalEnumerator_1_t3022____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15430_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3022____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3022_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15430_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3022____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3022_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15433_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3022_PropertyInfos[] =
{
	&InternalEnumerator_1_t3022____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3022____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3022_InternalEnumerator_1__ctor_m15429_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15429_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15429_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3022_InternalEnumerator_1__ctor_m15429_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15429_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15430_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15430_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3022_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15430_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15431_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15431_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15431_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15432_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15432_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3022_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15432_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15433_GenericMethod;
// T System.Array/InternalEnumerator`1<GoalKeeperJump_Down>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15433_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3022_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeperJump_Down_t79_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15433_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3022_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15429_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15430_MethodInfo,
	&InternalEnumerator_1_Dispose_m15431_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15432_MethodInfo,
	&InternalEnumerator_1_get_Current_m15433_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15432_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15431_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3022_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15430_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15432_MethodInfo,
	&InternalEnumerator_1_Dispose_m15431_MethodInfo,
	&InternalEnumerator_1_get_Current_m15433_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3022_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6147_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3022_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6147_il2cpp_TypeInfo, 7},
};
extern TypeInfo GoalKeeperJump_Down_t79_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3022_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15433_MethodInfo/* Method Usage */,
	&GoalKeeperJump_Down_t79_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGoalKeeperJump_Down_t79_m33604_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3022_0_0_0;
extern Il2CppType InternalEnumerator_1_t3022_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3022_GenericClass;
TypeInfo InternalEnumerator_1_t3022_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3022_MethodInfos/* methods */
	, InternalEnumerator_1_t3022_PropertyInfos/* properties */
	, InternalEnumerator_1_t3022_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3022_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3022_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3022_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3022_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3022_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3022_1_0_0/* this_arg */
	, InternalEnumerator_1_t3022_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3022_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3022_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3022)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7853_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>
extern MethodInfo ICollection_1_get_Count_m44006_MethodInfo;
static PropertyInfo ICollection_1_t7853____Count_PropertyInfo = 
{
	&ICollection_1_t7853_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44006_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44007_MethodInfo;
static PropertyInfo ICollection_1_t7853____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7853_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44007_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7853_PropertyInfos[] =
{
	&ICollection_1_t7853____Count_PropertyInfo,
	&ICollection_1_t7853____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44006_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::get_Count()
MethodInfo ICollection_1_get_Count_m44006_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7853_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44006_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44007_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44007_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7853_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44007_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
static ParameterInfo ICollection_1_t7853_ICollection_1_Add_m44008_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_Down_t79_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44008_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::Add(T)
MethodInfo ICollection_1_Add_m44008_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7853_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7853_ICollection_1_Add_m44008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44008_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44009_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::Clear()
MethodInfo ICollection_1_Clear_m44009_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7853_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44009_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
static ParameterInfo ICollection_1_t7853_ICollection_1_Contains_m44010_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_Down_t79_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44010_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::Contains(T)
MethodInfo ICollection_1_Contains_m44010_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7853_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7853_ICollection_1_Contains_m44010_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44010_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_DownU5BU5D_t5396_0_0_0;
extern Il2CppType GoalKeeperJump_DownU5BU5D_t5396_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7853_ICollection_1_CopyTo_m44011_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_DownU5BU5D_t5396_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44011_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44011_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7853_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7853_ICollection_1_CopyTo_m44011_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44011_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
static ParameterInfo ICollection_1_t7853_ICollection_1_Remove_m44012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_Down_t79_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44012_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeperJump_Down>::Remove(T)
MethodInfo ICollection_1_Remove_m44012_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7853_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7853_ICollection_1_Remove_m44012_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44012_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7853_MethodInfos[] =
{
	&ICollection_1_get_Count_m44006_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44007_MethodInfo,
	&ICollection_1_Add_m44008_MethodInfo,
	&ICollection_1_Clear_m44009_MethodInfo,
	&ICollection_1_Contains_m44010_MethodInfo,
	&ICollection_1_CopyTo_m44011_MethodInfo,
	&ICollection_1_Remove_m44012_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7855_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7853_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7855_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7853_0_0_0;
extern Il2CppType ICollection_1_t7853_1_0_0;
struct ICollection_1_t7853;
extern Il2CppGenericClass ICollection_1_t7853_GenericClass;
TypeInfo ICollection_1_t7853_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7853_MethodInfos/* methods */
	, ICollection_1_t7853_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7853_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7853_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7853_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7853_0_0_0/* byval_arg */
	, &ICollection_1_t7853_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7853_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<GoalKeeperJump_Down>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<GoalKeeperJump_Down>
extern Il2CppType IEnumerator_1_t6147_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44013_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<GoalKeeperJump_Down>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44013_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7855_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6147_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44013_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7855_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44013_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7855_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7855_0_0_0;
extern Il2CppType IEnumerable_1_t7855_1_0_0;
struct IEnumerable_1_t7855;
extern Il2CppGenericClass IEnumerable_1_t7855_GenericClass;
TypeInfo IEnumerable_1_t7855_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7855_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7855_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7855_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7855_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7855_0_0_0/* byval_arg */
	, &IEnumerable_1_t7855_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7855_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7854_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<GoalKeeperJump_Down>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump_Down>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump_Down>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<GoalKeeperJump_Down>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump_Down>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<GoalKeeperJump_Down>
extern MethodInfo IList_1_get_Item_m44014_MethodInfo;
extern MethodInfo IList_1_set_Item_m44015_MethodInfo;
static PropertyInfo IList_1_t7854____Item_PropertyInfo = 
{
	&IList_1_t7854_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44014_MethodInfo/* get */
	, &IList_1_set_Item_m44015_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7854_PropertyInfos[] =
{
	&IList_1_t7854____Item_PropertyInfo,
	NULL
};
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
static ParameterInfo IList_1_t7854_IList_1_IndexOf_m44016_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_Down_t79_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44016_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<GoalKeeperJump_Down>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44016_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7854_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7854_IList_1_IndexOf_m44016_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44016_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
static ParameterInfo IList_1_t7854_IList_1_Insert_m44017_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_Down_t79_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44017_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump_Down>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44017_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7854_IList_1_Insert_m44017_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44017_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7854_IList_1_RemoveAt_m44018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44018_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump_Down>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44018_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7854_IList_1_RemoveAt_m44018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44018_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7854_IList_1_get_Item_m44014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44014_GenericMethod;
// T System.Collections.Generic.IList`1<GoalKeeperJump_Down>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44014_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7854_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeperJump_Down_t79_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7854_IList_1_get_Item_m44014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44014_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
static ParameterInfo IList_1_t7854_IList_1_set_Item_m44015_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_Down_t79_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44015_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeperJump_Down>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44015_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7854_IList_1_set_Item_m44015_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44015_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7854_MethodInfos[] =
{
	&IList_1_IndexOf_m44016_MethodInfo,
	&IList_1_Insert_m44017_MethodInfo,
	&IList_1_RemoveAt_m44018_MethodInfo,
	&IList_1_get_Item_m44014_MethodInfo,
	&IList_1_set_Item_m44015_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7854_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7853_il2cpp_TypeInfo,
	&IEnumerable_1_t7855_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7854_0_0_0;
extern Il2CppType IList_1_t7854_1_0_0;
struct IList_1_t7854;
extern Il2CppGenericClass IList_1_t7854_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7854_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7854_MethodInfos/* methods */
	, IList_1_t7854_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7854_il2cpp_TypeInfo/* element_class */
	, IList_1_t7854_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7854_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7854_0_0_0/* byval_arg */
	, &IList_1_t7854_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7854_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump_Down>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_42.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3023_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump_Down>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_42MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_38.h"
extern TypeInfo InvokableCall_1_t3024_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_38MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15436_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15438_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump_Down>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump_Down>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump_Down>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3023____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3023_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3023, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3023_FieldInfos[] =
{
	&CachedInvokableCall_1_t3023____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3023_CachedInvokableCall_1__ctor_m15434_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_Down_t79_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15434_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump_Down>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15434_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3023_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3023_CachedInvokableCall_1__ctor_m15434_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15434_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3023_CachedInvokableCall_1_Invoke_m15435_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15435_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeperJump_Down>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15435_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3023_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3023_CachedInvokableCall_1_Invoke_m15435_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15435_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3023_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15434_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15435_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15435_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15439_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3023_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15435_MethodInfo,
	&InvokableCall_1_Find_m15439_MethodInfo,
};
extern Il2CppType UnityAction_1_t3025_0_0_0;
extern TypeInfo UnityAction_1_t3025_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeperJump_Down_t79_m33614_MethodInfo;
extern TypeInfo GoalKeeperJump_Down_t79_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15441_MethodInfo;
extern TypeInfo GoalKeeperJump_Down_t79_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3023_RGCTXData[8] = 
{
	&UnityAction_1_t3025_0_0_0/* Type Usage */,
	&UnityAction_1_t3025_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeperJump_Down_t79_m33614_MethodInfo/* Method Usage */,
	&GoalKeeperJump_Down_t79_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15441_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15436_MethodInfo/* Method Usage */,
	&GoalKeeperJump_Down_t79_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15438_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3023_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3023_1_0_0;
struct CachedInvokableCall_1_t3023;
extern Il2CppGenericClass CachedInvokableCall_1_t3023_GenericClass;
TypeInfo CachedInvokableCall_1_t3023_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3023_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3023_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3024_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3023_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3023_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3023_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3023_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3023_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3023_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3023_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3023)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_45.h"
extern TypeInfo UnityAction_1_t3025_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_45MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<GoalKeeperJump_Down>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<GoalKeeperJump_Down>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeperJump_Down_t79_m33614(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>
extern Il2CppType UnityAction_1_t3025_0_0_1;
FieldInfo InvokableCall_1_t3024____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3025_0_0_1/* type */
	, &InvokableCall_1_t3024_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3024, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3024_FieldInfos[] =
{
	&InvokableCall_1_t3024____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3024_InvokableCall_1__ctor_m15436_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15436_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15436_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3024_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3024_InvokableCall_1__ctor_m15436_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15436_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3025_0_0_0;
static ParameterInfo InvokableCall_1_t3024_InvokableCall_1__ctor_m15437_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3025_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15437_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15437_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3024_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3024_InvokableCall_1__ctor_m15437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15437_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3024_InvokableCall_1_Invoke_m15438_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15438_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15438_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3024_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3024_InvokableCall_1_Invoke_m15438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15438_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3024_InvokableCall_1_Find_m15439_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15439_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<GoalKeeperJump_Down>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15439_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3024_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3024_InvokableCall_1_Find_m15439_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15439_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3024_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15436_MethodInfo,
	&InvokableCall_1__ctor_m15437_MethodInfo,
	&InvokableCall_1_Invoke_m15438_MethodInfo,
	&InvokableCall_1_Find_m15439_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3024_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15438_MethodInfo,
	&InvokableCall_1_Find_m15439_MethodInfo,
};
extern TypeInfo UnityAction_1_t3025_il2cpp_TypeInfo;
extern TypeInfo GoalKeeperJump_Down_t79_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3024_RGCTXData[5] = 
{
	&UnityAction_1_t3025_0_0_0/* Type Usage */,
	&UnityAction_1_t3025_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeperJump_Down_t79_m33614_MethodInfo/* Method Usage */,
	&GoalKeeperJump_Down_t79_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15441_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3024_0_0_0;
extern Il2CppType InvokableCall_1_t3024_1_0_0;
struct InvokableCall_1_t3024;
extern Il2CppGenericClass InvokableCall_1_t3024_GenericClass;
TypeInfo InvokableCall_1_t3024_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3024_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3024_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3024_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3024_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3024_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3024_0_0_0/* byval_arg */
	, &InvokableCall_1_t3024_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3024_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3024_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3024)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3025_UnityAction_1__ctor_m15440_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15440_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15440_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3025_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3025_UnityAction_1__ctor_m15440_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15440_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
static ParameterInfo UnityAction_1_t3025_UnityAction_1_Invoke_m15441_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_Down_t79_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15441_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15441_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3025_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3025_UnityAction_1_Invoke_m15441_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15441_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeperJump_Down_t79_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3025_UnityAction_1_BeginInvoke_m15442_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeperJump_Down_t79_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15442_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15442_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3025_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3025_UnityAction_1_BeginInvoke_m15442_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15442_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3025_UnityAction_1_EndInvoke_m15443_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15443_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeperJump_Down>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15443_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3025_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3025_UnityAction_1_EndInvoke_m15443_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15443_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3025_MethodInfos[] =
{
	&UnityAction_1__ctor_m15440_MethodInfo,
	&UnityAction_1_Invoke_m15441_MethodInfo,
	&UnityAction_1_BeginInvoke_m15442_MethodInfo,
	&UnityAction_1_EndInvoke_m15443_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15442_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15443_MethodInfo;
static MethodInfo* UnityAction_1_t3025_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15441_MethodInfo,
	&UnityAction_1_BeginInvoke_m15442_MethodInfo,
	&UnityAction_1_EndInvoke_m15443_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3025_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3025_1_0_0;
struct UnityAction_1_t3025;
extern Il2CppGenericClass UnityAction_1_t3025_GenericClass;
TypeInfo UnityAction_1_t3025_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3025_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3025_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3025_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3025_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3025_0_0_0/* byval_arg */
	, &UnityAction_1_t3025_1_0_0/* this_arg */
	, UnityAction_1_t3025_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3025_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3025)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6149_il2cpp_TypeInfo;

// GoalKeeper_Script
#include "AssemblyU2DCSharp_GoalKeeper_Script.h"


// T System.Collections.Generic.IEnumerator`1<GoalKeeper_Script>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<GoalKeeper_Script>
extern MethodInfo IEnumerator_1_get_Current_m44019_MethodInfo;
static PropertyInfo IEnumerator_1_t6149____Current_PropertyInfo = 
{
	&IEnumerator_1_t6149_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44019_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6149_PropertyInfos[] =
{
	&IEnumerator_1_t6149____Current_PropertyInfo,
	NULL
};
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44019_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<GoalKeeper_Script>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44019_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6149_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeper_Script_t77_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44019_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6149_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44019_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6149_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6149_0_0_0;
extern Il2CppType IEnumerator_1_t6149_1_0_0;
struct IEnumerator_1_t6149;
extern Il2CppGenericClass IEnumerator_1_t6149_GenericClass;
TypeInfo IEnumerator_1_t6149_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6149_MethodInfos/* methods */
	, IEnumerator_1_t6149_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6149_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6149_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6149_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6149_0_0_0/* byval_arg */
	, &IEnumerator_1_t6149_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6149_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<GoalKeeper_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_118.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3026_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<GoalKeeper_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_118MethodDeclarations.h"

extern TypeInfo GoalKeeper_Script_t77_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15448_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGoalKeeper_Script_t77_m33616_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<GoalKeeper_Script>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<GoalKeeper_Script>(System.Int32)
#define Array_InternalArray__get_Item_TisGoalKeeper_Script_t77_m33616(__this, p0, method) (GoalKeeper_Script_t77 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<GoalKeeper_Script>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<GoalKeeper_Script>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<GoalKeeper_Script>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<GoalKeeper_Script>::MoveNext()
// T System.Array/InternalEnumerator`1<GoalKeeper_Script>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<GoalKeeper_Script>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3026____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3026_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3026, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3026____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3026_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3026, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3026_FieldInfos[] =
{
	&InternalEnumerator_1_t3026____array_0_FieldInfo,
	&InternalEnumerator_1_t3026____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15445_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3026____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3026_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15445_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3026____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3026_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15448_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3026_PropertyInfos[] =
{
	&InternalEnumerator_1_t3026____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3026____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3026_InternalEnumerator_1__ctor_m15444_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15444_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<GoalKeeper_Script>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15444_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3026_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3026_InternalEnumerator_1__ctor_m15444_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15444_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15445_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<GoalKeeper_Script>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15445_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3026_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15445_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15446_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<GoalKeeper_Script>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15446_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3026_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15446_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15447_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<GoalKeeper_Script>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15447_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3026_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15447_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15448_GenericMethod;
// T System.Array/InternalEnumerator`1<GoalKeeper_Script>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15448_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3026_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeper_Script_t77_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15448_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3026_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15444_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15445_MethodInfo,
	&InternalEnumerator_1_Dispose_m15446_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15447_MethodInfo,
	&InternalEnumerator_1_get_Current_m15448_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15447_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15446_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3026_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15445_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15447_MethodInfo,
	&InternalEnumerator_1_Dispose_m15446_MethodInfo,
	&InternalEnumerator_1_get_Current_m15448_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3026_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6149_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3026_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6149_il2cpp_TypeInfo, 7},
};
extern TypeInfo GoalKeeper_Script_t77_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3026_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15448_MethodInfo/* Method Usage */,
	&GoalKeeper_Script_t77_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGoalKeeper_Script_t77_m33616_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3026_0_0_0;
extern Il2CppType InternalEnumerator_1_t3026_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3026_GenericClass;
TypeInfo InternalEnumerator_1_t3026_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3026_MethodInfos/* methods */
	, InternalEnumerator_1_t3026_PropertyInfos/* properties */
	, InternalEnumerator_1_t3026_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3026_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3026_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3026_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3026_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3026_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3026_1_0_0/* this_arg */
	, InternalEnumerator_1_t3026_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3026_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3026_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3026)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7856_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<GoalKeeper_Script>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<GoalKeeper_Script>
extern MethodInfo ICollection_1_get_Count_m44020_MethodInfo;
static PropertyInfo ICollection_1_t7856____Count_PropertyInfo = 
{
	&ICollection_1_t7856_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44020_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44021_MethodInfo;
static PropertyInfo ICollection_1_t7856____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7856_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44021_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7856_PropertyInfos[] =
{
	&ICollection_1_t7856____Count_PropertyInfo,
	&ICollection_1_t7856____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44020_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<GoalKeeper_Script>::get_Count()
MethodInfo ICollection_1_get_Count_m44020_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7856_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44020_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44021_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44021_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7856_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44021_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
static ParameterInfo ICollection_1_t7856_ICollection_1_Add_m44022_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_Script_t77_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44022_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script>::Add(T)
MethodInfo ICollection_1_Add_m44022_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7856_ICollection_1_Add_m44022_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44022_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44023_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script>::Clear()
MethodInfo ICollection_1_Clear_m44023_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44023_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
static ParameterInfo ICollection_1_t7856_ICollection_1_Contains_m44024_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_Script_t77_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44024_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script>::Contains(T)
MethodInfo ICollection_1_Contains_m44024_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7856_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7856_ICollection_1_Contains_m44024_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44024_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_ScriptU5BU5D_t5397_0_0_0;
extern Il2CppType GoalKeeper_ScriptU5BU5D_t5397_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7856_ICollection_1_CopyTo_m44025_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_ScriptU5BU5D_t5397_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44025_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44025_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7856_ICollection_1_CopyTo_m44025_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44025_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
static ParameterInfo ICollection_1_t7856_ICollection_1_Remove_m44026_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_Script_t77_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44026_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script>::Remove(T)
MethodInfo ICollection_1_Remove_m44026_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7856_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7856_ICollection_1_Remove_m44026_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44026_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7856_MethodInfos[] =
{
	&ICollection_1_get_Count_m44020_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44021_MethodInfo,
	&ICollection_1_Add_m44022_MethodInfo,
	&ICollection_1_Clear_m44023_MethodInfo,
	&ICollection_1_Contains_m44024_MethodInfo,
	&ICollection_1_CopyTo_m44025_MethodInfo,
	&ICollection_1_Remove_m44026_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7858_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7856_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7858_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7856_0_0_0;
extern Il2CppType ICollection_1_t7856_1_0_0;
struct ICollection_1_t7856;
extern Il2CppGenericClass ICollection_1_t7856_GenericClass;
TypeInfo ICollection_1_t7856_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7856_MethodInfos/* methods */
	, ICollection_1_t7856_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7856_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7856_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7856_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7856_0_0_0/* byval_arg */
	, &ICollection_1_t7856_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7856_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<GoalKeeper_Script>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<GoalKeeper_Script>
extern Il2CppType IEnumerator_1_t6149_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44027_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<GoalKeeper_Script>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44027_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7858_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6149_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44027_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7858_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44027_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7858_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7858_0_0_0;
extern Il2CppType IEnumerable_1_t7858_1_0_0;
struct IEnumerable_1_t7858;
extern Il2CppGenericClass IEnumerable_1_t7858_GenericClass;
TypeInfo IEnumerable_1_t7858_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7858_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7858_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7858_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7858_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7858_0_0_0/* byval_arg */
	, &IEnumerable_1_t7858_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7858_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7857_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<GoalKeeper_Script>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<GoalKeeper_Script>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<GoalKeeper_Script>
extern MethodInfo IList_1_get_Item_m44028_MethodInfo;
extern MethodInfo IList_1_set_Item_m44029_MethodInfo;
static PropertyInfo IList_1_t7857____Item_PropertyInfo = 
{
	&IList_1_t7857_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44028_MethodInfo/* get */
	, &IList_1_set_Item_m44029_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7857_PropertyInfos[] =
{
	&IList_1_t7857____Item_PropertyInfo,
	NULL
};
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
static ParameterInfo IList_1_t7857_IList_1_IndexOf_m44030_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_Script_t77_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44030_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<GoalKeeper_Script>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44030_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7857_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7857_IList_1_IndexOf_m44030_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44030_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
static ParameterInfo IList_1_t7857_IList_1_Insert_m44031_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_Script_t77_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44031_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44031_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7857_IList_1_Insert_m44031_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44031_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7857_IList_1_RemoveAt_m44032_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44032_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44032_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7857_IList_1_RemoveAt_m44032_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44032_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7857_IList_1_get_Item_m44028_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44028_GenericMethod;
// T System.Collections.Generic.IList`1<GoalKeeper_Script>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44028_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7857_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeper_Script_t77_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7857_IList_1_get_Item_m44028_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44028_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
static ParameterInfo IList_1_t7857_IList_1_set_Item_m44029_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_Script_t77_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44029_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44029_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7857_IList_1_set_Item_m44029_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44029_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7857_MethodInfos[] =
{
	&IList_1_IndexOf_m44030_MethodInfo,
	&IList_1_Insert_m44031_MethodInfo,
	&IList_1_RemoveAt_m44032_MethodInfo,
	&IList_1_get_Item_m44028_MethodInfo,
	&IList_1_set_Item_m44029_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7857_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7856_il2cpp_TypeInfo,
	&IEnumerable_1_t7858_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7857_0_0_0;
extern Il2CppType IList_1_t7857_1_0_0;
struct IList_1_t7857;
extern Il2CppGenericClass IList_1_t7857_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7857_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7857_MethodInfos/* methods */
	, IList_1_t7857_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7857_il2cpp_TypeInfo/* element_class */
	, IList_1_t7857_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7857_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7857_0_0_0/* byval_arg */
	, &IList_1_t7857_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7857_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<GoalKeeper_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_43.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3027_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<GoalKeeper_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_43MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_39.h"
extern TypeInfo InvokableCall_1_t3028_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_39MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15451_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15453_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeper_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeper_Script>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<GoalKeeper_Script>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3027____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3027_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3027, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3027_FieldInfos[] =
{
	&CachedInvokableCall_1_t3027____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3027_CachedInvokableCall_1__ctor_m15449_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_Script_t77_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15449_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeper_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15449_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3027_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3027_CachedInvokableCall_1__ctor_m15449_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15449_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3027_CachedInvokableCall_1_Invoke_m15450_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15450_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<GoalKeeper_Script>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15450_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3027_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3027_CachedInvokableCall_1_Invoke_m15450_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15450_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3027_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15449_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15450_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15450_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15454_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3027_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15450_MethodInfo,
	&InvokableCall_1_Find_m15454_MethodInfo,
};
extern Il2CppType UnityAction_1_t3029_0_0_0;
extern TypeInfo UnityAction_1_t3029_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeper_Script_t77_m33626_MethodInfo;
extern TypeInfo GoalKeeper_Script_t77_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15456_MethodInfo;
extern TypeInfo GoalKeeper_Script_t77_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3027_RGCTXData[8] = 
{
	&UnityAction_1_t3029_0_0_0/* Type Usage */,
	&UnityAction_1_t3029_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeper_Script_t77_m33626_MethodInfo/* Method Usage */,
	&GoalKeeper_Script_t77_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15456_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15451_MethodInfo/* Method Usage */,
	&GoalKeeper_Script_t77_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15453_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3027_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3027_1_0_0;
struct CachedInvokableCall_1_t3027;
extern Il2CppGenericClass CachedInvokableCall_1_t3027_GenericClass;
TypeInfo CachedInvokableCall_1_t3027_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3027_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3027_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3028_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3027_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3027_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3027_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3027_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3027_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3027_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3027_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3027)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<GoalKeeper_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_46.h"
extern TypeInfo UnityAction_1_t3029_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<GoalKeeper_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_46MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<GoalKeeper_Script>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<GoalKeeper_Script>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeper_Script_t77_m33626(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>
extern Il2CppType UnityAction_1_t3029_0_0_1;
FieldInfo InvokableCall_1_t3028____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3029_0_0_1/* type */
	, &InvokableCall_1_t3028_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3028, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3028_FieldInfos[] =
{
	&InvokableCall_1_t3028____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3028_InvokableCall_1__ctor_m15451_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15451_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15451_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3028_InvokableCall_1__ctor_m15451_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15451_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3029_0_0_0;
static ParameterInfo InvokableCall_1_t3028_InvokableCall_1__ctor_m15452_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3029_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15452_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15452_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3028_InvokableCall_1__ctor_m15452_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15452_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3028_InvokableCall_1_Invoke_m15453_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15453_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15453_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3028_InvokableCall_1_Invoke_m15453_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15453_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3028_InvokableCall_1_Find_m15454_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15454_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<GoalKeeper_Script>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15454_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3028_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3028_InvokableCall_1_Find_m15454_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15454_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3028_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15451_MethodInfo,
	&InvokableCall_1__ctor_m15452_MethodInfo,
	&InvokableCall_1_Invoke_m15453_MethodInfo,
	&InvokableCall_1_Find_m15454_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3028_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15453_MethodInfo,
	&InvokableCall_1_Find_m15454_MethodInfo,
};
extern TypeInfo UnityAction_1_t3029_il2cpp_TypeInfo;
extern TypeInfo GoalKeeper_Script_t77_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3028_RGCTXData[5] = 
{
	&UnityAction_1_t3029_0_0_0/* Type Usage */,
	&UnityAction_1_t3029_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGoalKeeper_Script_t77_m33626_MethodInfo/* Method Usage */,
	&GoalKeeper_Script_t77_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15456_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3028_0_0_0;
extern Il2CppType InvokableCall_1_t3028_1_0_0;
struct InvokableCall_1_t3028;
extern Il2CppGenericClass InvokableCall_1_t3028_GenericClass;
TypeInfo InvokableCall_1_t3028_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3028_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3028_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3028_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3028_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3028_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3028_0_0_0/* byval_arg */
	, &InvokableCall_1_t3028_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3028_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3028_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3028)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<GoalKeeper_Script>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeper_Script>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<GoalKeeper_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeper_Script>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<GoalKeeper_Script>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3029_UnityAction_1__ctor_m15455_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15455_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeper_Script>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15455_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3029_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3029_UnityAction_1__ctor_m15455_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15455_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
static ParameterInfo UnityAction_1_t3029_UnityAction_1_Invoke_m15456_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_Script_t77_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15456_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeper_Script>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15456_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3029_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3029_UnityAction_1_Invoke_m15456_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15456_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_Script_t77_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3029_UnityAction_1_BeginInvoke_m15457_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_Script_t77_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15457_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<GoalKeeper_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15457_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3029_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3029_UnityAction_1_BeginInvoke_m15457_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15457_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3029_UnityAction_1_EndInvoke_m15458_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15458_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GoalKeeper_Script>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15458_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3029_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3029_UnityAction_1_EndInvoke_m15458_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15458_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3029_MethodInfos[] =
{
	&UnityAction_1__ctor_m15455_MethodInfo,
	&UnityAction_1_Invoke_m15456_MethodInfo,
	&UnityAction_1_BeginInvoke_m15457_MethodInfo,
	&UnityAction_1_EndInvoke_m15458_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15457_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15458_MethodInfo;
static MethodInfo* UnityAction_1_t3029_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15456_MethodInfo,
	&UnityAction_1_BeginInvoke_m15457_MethodInfo,
	&UnityAction_1_EndInvoke_m15458_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3029_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3029_1_0_0;
struct UnityAction_1_t3029;
extern Il2CppGenericClass UnityAction_1_t3029_GenericClass;
TypeInfo UnityAction_1_t3029_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3029_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3029_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3029_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3029_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3029_0_0_0/* byval_arg */
	, &UnityAction_1_t3029_1_0_0/* this_arg */
	, UnityAction_1_t3029_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3029_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3029)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6151_il2cpp_TypeInfo;

// GoalKeeper_Script/GoalKeeper_State
#include "AssemblyU2DCSharp_GoalKeeper_Script_GoalKeeper_State.h"


// T System.Collections.Generic.IEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<GoalKeeper_Script/GoalKeeper_State>
extern MethodInfo IEnumerator_1_get_Current_m44033_MethodInfo;
static PropertyInfo IEnumerator_1_t6151____Current_PropertyInfo = 
{
	&IEnumerator_1_t6151_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44033_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6151_PropertyInfos[] =
{
	&IEnumerator_1_t6151____Current_PropertyInfo,
	NULL
};
extern Il2CppType GoalKeeper_State_t80_0_0_0;
extern void* RuntimeInvoker_GoalKeeper_State_t80 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44033_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44033_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6151_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeper_State_t80_0_0_0/* return_type */
	, RuntimeInvoker_GoalKeeper_State_t80/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44033_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6151_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44033_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6151_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6151_0_0_0;
extern Il2CppType IEnumerator_1_t6151_1_0_0;
struct IEnumerator_1_t6151;
extern Il2CppGenericClass IEnumerator_1_t6151_GenericClass;
TypeInfo IEnumerator_1_t6151_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6151_MethodInfos/* methods */
	, IEnumerator_1_t6151_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6151_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6151_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6151_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6151_0_0_0/* byval_arg */
	, &IEnumerator_1_t6151_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6151_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_119.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3030_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_119MethodDeclarations.h"

extern TypeInfo GoalKeeper_State_t80_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15463_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGoalKeeper_State_t80_m33628_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<GoalKeeper_Script/GoalKeeper_State>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<GoalKeeper_Script/GoalKeeper_State>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisGoalKeeper_State_t80_m33628 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m15459_MethodInfo;
 void InternalEnumerator_1__ctor_m15459 (InternalEnumerator_1_t3030 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460 (InternalEnumerator_1_t3030 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m15463(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m15463_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&GoalKeeper_State_t80_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m15461_MethodInfo;
 void InternalEnumerator_1_Dispose_m15461 (InternalEnumerator_1_t3030 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m15462_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m15462 (InternalEnumerator_1_t3030 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m15463 (InternalEnumerator_1_t3030 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisGoalKeeper_State_t80_m33628(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisGoalKeeper_State_t80_m33628_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3030____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3030_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3030, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3030____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3030_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3030, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3030_FieldInfos[] =
{
	&InternalEnumerator_1_t3030____array_0_FieldInfo,
	&InternalEnumerator_1_t3030____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3030____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3030_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3030____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3030_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15463_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3030_PropertyInfos[] =
{
	&InternalEnumerator_1_t3030____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3030____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3030_InternalEnumerator_1__ctor_m15459_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15459_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15459_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m15459/* method */
	, &InternalEnumerator_1_t3030_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3030_InternalEnumerator_1__ctor_m15459_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15459_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460/* method */
	, &InternalEnumerator_1_t3030_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15461_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15461_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m15461/* method */
	, &InternalEnumerator_1_t3030_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15461_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15462_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15462_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m15462/* method */
	, &InternalEnumerator_1_t3030_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15462_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_State_t80_0_0_0;
extern void* RuntimeInvoker_GoalKeeper_State_t80 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15463_GenericMethod;
// T System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15463_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m15463/* method */
	, &InternalEnumerator_1_t3030_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeper_State_t80_0_0_0/* return_type */
	, RuntimeInvoker_GoalKeeper_State_t80/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15463_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3030_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15459_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460_MethodInfo,
	&InternalEnumerator_1_Dispose_m15461_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15462_MethodInfo,
	&InternalEnumerator_1_get_Current_m15463_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3030_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15462_MethodInfo,
	&InternalEnumerator_1_Dispose_m15461_MethodInfo,
	&InternalEnumerator_1_get_Current_m15463_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3030_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6151_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3030_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6151_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3030_0_0_0;
extern Il2CppType InternalEnumerator_1_t3030_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3030_GenericClass;
TypeInfo InternalEnumerator_1_t3030_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3030_MethodInfos/* methods */
	, InternalEnumerator_1_t3030_PropertyInfos/* properties */
	, InternalEnumerator_1_t3030_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3030_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3030_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3030_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3030_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3030_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3030_1_0_0/* this_arg */
	, InternalEnumerator_1_t3030_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3030_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3030)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7859_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>
extern MethodInfo ICollection_1_get_Count_m44034_MethodInfo;
static PropertyInfo ICollection_1_t7859____Count_PropertyInfo = 
{
	&ICollection_1_t7859_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44034_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44035_MethodInfo;
static PropertyInfo ICollection_1_t7859____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7859_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44035_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7859_PropertyInfos[] =
{
	&ICollection_1_t7859____Count_PropertyInfo,
	&ICollection_1_t7859____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44034_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::get_Count()
MethodInfo ICollection_1_get_Count_m44034_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7859_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44034_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44035_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44035_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44035_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_State_t80_0_0_0;
extern Il2CppType GoalKeeper_State_t80_0_0_0;
static ParameterInfo ICollection_1_t7859_ICollection_1_Add_m44036_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_State_t80_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44036_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::Add(T)
MethodInfo ICollection_1_Add_m44036_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t7859_ICollection_1_Add_m44036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44036_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44037_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::Clear()
MethodInfo ICollection_1_Clear_m44037_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44037_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_State_t80_0_0_0;
static ParameterInfo ICollection_1_t7859_ICollection_1_Contains_m44038_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_State_t80_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44038_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::Contains(T)
MethodInfo ICollection_1_Contains_m44038_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7859_ICollection_1_Contains_m44038_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44038_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_StateU5BU5D_t5398_0_0_0;
extern Il2CppType GoalKeeper_StateU5BU5D_t5398_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7859_ICollection_1_CopyTo_m44039_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_StateU5BU5D_t5398_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44039_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44039_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7859_ICollection_1_CopyTo_m44039_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44039_GenericMethod/* genericMethod */

};
extern Il2CppType GoalKeeper_State_t80_0_0_0;
static ParameterInfo ICollection_1_t7859_ICollection_1_Remove_m44040_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_State_t80_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44040_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GoalKeeper_Script/GoalKeeper_State>::Remove(T)
MethodInfo ICollection_1_Remove_m44040_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7859_ICollection_1_Remove_m44040_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44040_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7859_MethodInfos[] =
{
	&ICollection_1_get_Count_m44034_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44035_MethodInfo,
	&ICollection_1_Add_m44036_MethodInfo,
	&ICollection_1_Clear_m44037_MethodInfo,
	&ICollection_1_Contains_m44038_MethodInfo,
	&ICollection_1_CopyTo_m44039_MethodInfo,
	&ICollection_1_Remove_m44040_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7861_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7859_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7861_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7859_0_0_0;
extern Il2CppType ICollection_1_t7859_1_0_0;
struct ICollection_1_t7859;
extern Il2CppGenericClass ICollection_1_t7859_GenericClass;
TypeInfo ICollection_1_t7859_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7859_MethodInfos/* methods */
	, ICollection_1_t7859_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7859_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7859_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7859_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7859_0_0_0/* byval_arg */
	, &ICollection_1_t7859_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7859_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<GoalKeeper_Script/GoalKeeper_State>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<GoalKeeper_Script/GoalKeeper_State>
extern Il2CppType IEnumerator_1_t6151_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44041_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<GoalKeeper_Script/GoalKeeper_State>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44041_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7861_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6151_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44041_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7861_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44041_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7861_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7861_0_0_0;
extern Il2CppType IEnumerable_1_t7861_1_0_0;
struct IEnumerable_1_t7861;
extern Il2CppGenericClass IEnumerable_1_t7861_GenericClass;
TypeInfo IEnumerable_1_t7861_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7861_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7861_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7861_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7861_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7861_0_0_0/* byval_arg */
	, &IEnumerable_1_t7861_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7861_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7860_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>
extern MethodInfo IList_1_get_Item_m44042_MethodInfo;
extern MethodInfo IList_1_set_Item_m44043_MethodInfo;
static PropertyInfo IList_1_t7860____Item_PropertyInfo = 
{
	&IList_1_t7860_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44042_MethodInfo/* get */
	, &IList_1_set_Item_m44043_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7860_PropertyInfos[] =
{
	&IList_1_t7860____Item_PropertyInfo,
	NULL
};
extern Il2CppType GoalKeeper_State_t80_0_0_0;
static ParameterInfo IList_1_t7860_IList_1_IndexOf_m44044_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_State_t80_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44044_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44044_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7860_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7860_IList_1_IndexOf_m44044_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44044_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GoalKeeper_State_t80_0_0_0;
static ParameterInfo IList_1_t7860_IList_1_Insert_m44045_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_State_t80_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44045_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44045_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7860_IList_1_Insert_m44045_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44045_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7860_IList_1_RemoveAt_m44046_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44046_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44046_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7860_IList_1_RemoveAt_m44046_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44046_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7860_IList_1_get_Item_m44042_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType GoalKeeper_State_t80_0_0_0;
extern void* RuntimeInvoker_GoalKeeper_State_t80_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44042_GenericMethod;
// T System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44042_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7860_il2cpp_TypeInfo/* declaring_type */
	, &GoalKeeper_State_t80_0_0_0/* return_type */
	, RuntimeInvoker_GoalKeeper_State_t80_Int32_t123/* invoker_method */
	, IList_1_t7860_IList_1_get_Item_m44042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44042_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GoalKeeper_State_t80_0_0_0;
static ParameterInfo IList_1_t7860_IList_1_set_Item_m44043_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GoalKeeper_State_t80_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44043_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GoalKeeper_Script/GoalKeeper_State>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44043_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7860_IList_1_set_Item_m44043_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44043_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7860_MethodInfos[] =
{
	&IList_1_IndexOf_m44044_MethodInfo,
	&IList_1_Insert_m44045_MethodInfo,
	&IList_1_RemoveAt_m44046_MethodInfo,
	&IList_1_get_Item_m44042_MethodInfo,
	&IList_1_set_Item_m44043_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7860_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7859_il2cpp_TypeInfo,
	&IEnumerable_1_t7861_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7860_0_0_0;
extern Il2CppType IList_1_t7860_1_0_0;
struct IList_1_t7860;
extern Il2CppGenericClass IList_1_t7860_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7860_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7860_MethodInfos/* methods */
	, IList_1_t7860_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7860_il2cpp_TypeInfo/* element_class */
	, IList_1_t7860_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7860_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7860_0_0_0/* byval_arg */
	, &IList_1_t7860_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7860_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7862_il2cpp_TypeInfo;

// System.Enum
#include "mscorlib_System_Enum.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Enum>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Enum>
extern MethodInfo ICollection_1_get_Count_m44047_MethodInfo;
static PropertyInfo ICollection_1_t7862____Count_PropertyInfo = 
{
	&ICollection_1_t7862_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44047_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44048_MethodInfo;
static PropertyInfo ICollection_1_t7862____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7862_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44048_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7862_PropertyInfos[] =
{
	&ICollection_1_t7862____Count_PropertyInfo,
	&ICollection_1_t7862____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44047_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Enum>::get_Count()
MethodInfo ICollection_1_get_Count_m44047_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7862_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44047_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44048_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44048_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7862_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44048_GenericMethod/* genericMethod */

};
extern Il2CppType Enum_t185_0_0_0;
extern Il2CppType Enum_t185_0_0_0;
static ParameterInfo ICollection_1_t7862_ICollection_1_Add_m44049_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Enum_t185_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44049_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::Add(T)
MethodInfo ICollection_1_Add_m44049_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7862_ICollection_1_Add_m44049_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44049_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44050_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::Clear()
MethodInfo ICollection_1_Clear_m44050_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44050_GenericMethod/* genericMethod */

};
extern Il2CppType Enum_t185_0_0_0;
static ParameterInfo ICollection_1_t7862_ICollection_1_Contains_m44051_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Enum_t185_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44051_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::Contains(T)
MethodInfo ICollection_1_Contains_m44051_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7862_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7862_ICollection_1_Contains_m44051_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44051_GenericMethod/* genericMethod */

};
extern Il2CppType EnumU5BU5D_t5428_0_0_0;
extern Il2CppType EnumU5BU5D_t5428_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7862_ICollection_1_CopyTo_m44052_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EnumU5BU5D_t5428_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44052_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44052_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7862_ICollection_1_CopyTo_m44052_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44052_GenericMethod/* genericMethod */

};
extern Il2CppType Enum_t185_0_0_0;
static ParameterInfo ICollection_1_t7862_ICollection_1_Remove_m44053_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Enum_t185_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44053_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::Remove(T)
MethodInfo ICollection_1_Remove_m44053_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7862_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7862_ICollection_1_Remove_m44053_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44053_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7862_MethodInfos[] =
{
	&ICollection_1_get_Count_m44047_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44048_MethodInfo,
	&ICollection_1_Add_m44049_MethodInfo,
	&ICollection_1_Clear_m44050_MethodInfo,
	&ICollection_1_Contains_m44051_MethodInfo,
	&ICollection_1_CopyTo_m44052_MethodInfo,
	&ICollection_1_Remove_m44053_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7864_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7862_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7864_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7862_0_0_0;
extern Il2CppType ICollection_1_t7862_1_0_0;
struct ICollection_1_t7862;
extern Il2CppGenericClass ICollection_1_t7862_GenericClass;
TypeInfo ICollection_1_t7862_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7862_MethodInfos/* methods */
	, ICollection_1_t7862_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7862_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7862_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7862_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7862_0_0_0/* byval_arg */
	, &ICollection_1_t7862_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7862_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Enum>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Enum>
extern Il2CppType IEnumerator_1_t6153_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44054_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Enum>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44054_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7864_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6153_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44054_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7864_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44054_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7864_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7864_0_0_0;
extern Il2CppType IEnumerable_1_t7864_1_0_0;
struct IEnumerable_1_t7864;
extern Il2CppGenericClass IEnumerable_1_t7864_GenericClass;
TypeInfo IEnumerable_1_t7864_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7864_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7864_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7864_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7864_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7864_0_0_0/* byval_arg */
	, &IEnumerable_1_t7864_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7864_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6153_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Enum>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Enum>
extern MethodInfo IEnumerator_1_get_Current_m44055_MethodInfo;
static PropertyInfo IEnumerator_1_t6153____Current_PropertyInfo = 
{
	&IEnumerator_1_t6153_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44055_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6153_PropertyInfos[] =
{
	&IEnumerator_1_t6153____Current_PropertyInfo,
	NULL
};
extern Il2CppType Enum_t185_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44055_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Enum>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44055_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6153_il2cpp_TypeInfo/* declaring_type */
	, &Enum_t185_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44055_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6153_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44055_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6153_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6153_0_0_0;
extern Il2CppType IEnumerator_1_t6153_1_0_0;
struct IEnumerator_1_t6153;
extern Il2CppGenericClass IEnumerator_1_t6153_GenericClass;
TypeInfo IEnumerator_1_t6153_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6153_MethodInfos/* methods */
	, IEnumerator_1_t6153_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6153_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6153_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6153_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6153_0_0_0/* byval_arg */
	, &IEnumerator_1_t6153_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6153_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Enum>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_120.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3031_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Enum>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_120MethodDeclarations.h"

extern TypeInfo Enum_t185_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15468_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEnum_t185_m33639_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Enum>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Enum>(System.Int32)
#define Array_InternalArray__get_Item_TisEnum_t185_m33639(__this, p0, method) (Enum_t185 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Enum>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Enum>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Enum>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Enum>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Enum>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Enum>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3031____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3031_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3031, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3031____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3031_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3031, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3031_FieldInfos[] =
{
	&InternalEnumerator_1_t3031____array_0_FieldInfo,
	&InternalEnumerator_1_t3031____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15465_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3031____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3031_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15465_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3031____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3031_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15468_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3031_PropertyInfos[] =
{
	&InternalEnumerator_1_t3031____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3031____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3031_InternalEnumerator_1__ctor_m15464_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15464_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Enum>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15464_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3031_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3031_InternalEnumerator_1__ctor_m15464_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15464_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15465_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Enum>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15465_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3031_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15465_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15466_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Enum>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15466_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3031_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15466_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15467_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Enum>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15467_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3031_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15467_GenericMethod/* genericMethod */

};
extern Il2CppType Enum_t185_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15468_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Enum>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15468_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3031_il2cpp_TypeInfo/* declaring_type */
	, &Enum_t185_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15468_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3031_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15464_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15465_MethodInfo,
	&InternalEnumerator_1_Dispose_m15466_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15467_MethodInfo,
	&InternalEnumerator_1_get_Current_m15468_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15467_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15466_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3031_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15465_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15467_MethodInfo,
	&InternalEnumerator_1_Dispose_m15466_MethodInfo,
	&InternalEnumerator_1_get_Current_m15468_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3031_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6153_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3031_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6153_il2cpp_TypeInfo, 7},
};
extern TypeInfo Enum_t185_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3031_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15468_MethodInfo/* Method Usage */,
	&Enum_t185_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisEnum_t185_m33639_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3031_0_0_0;
extern Il2CppType InternalEnumerator_1_t3031_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3031_GenericClass;
TypeInfo InternalEnumerator_1_t3031_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3031_MethodInfos/* methods */
	, InternalEnumerator_1_t3031_PropertyInfos/* properties */
	, InternalEnumerator_1_t3031_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3031_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3031_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3031_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3031_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3031_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3031_1_0_0/* this_arg */
	, InternalEnumerator_1_t3031_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3031_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3031_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3031)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7863_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Enum>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Enum>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Enum>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Enum>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Enum>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Enum>
extern MethodInfo IList_1_get_Item_m44056_MethodInfo;
extern MethodInfo IList_1_set_Item_m44057_MethodInfo;
static PropertyInfo IList_1_t7863____Item_PropertyInfo = 
{
	&IList_1_t7863_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44056_MethodInfo/* get */
	, &IList_1_set_Item_m44057_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7863_PropertyInfos[] =
{
	&IList_1_t7863____Item_PropertyInfo,
	NULL
};
extern Il2CppType Enum_t185_0_0_0;
static ParameterInfo IList_1_t7863_IList_1_IndexOf_m44058_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Enum_t185_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44058_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Enum>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44058_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7863_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7863_IList_1_IndexOf_m44058_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44058_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Enum_t185_0_0_0;
static ParameterInfo IList_1_t7863_IList_1_Insert_m44059_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Enum_t185_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44059_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Enum>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44059_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7863_IList_1_Insert_m44059_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44059_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7863_IList_1_RemoveAt_m44060_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44060_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Enum>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44060_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7863_IList_1_RemoveAt_m44060_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44060_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7863_IList_1_get_Item_m44056_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Enum_t185_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44056_GenericMethod;
// T System.Collections.Generic.IList`1<System.Enum>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44056_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7863_il2cpp_TypeInfo/* declaring_type */
	, &Enum_t185_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7863_IList_1_get_Item_m44056_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44056_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Enum_t185_0_0_0;
static ParameterInfo IList_1_t7863_IList_1_set_Item_m44057_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Enum_t185_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44057_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Enum>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44057_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7863_IList_1_set_Item_m44057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44057_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7863_MethodInfos[] =
{
	&IList_1_IndexOf_m44058_MethodInfo,
	&IList_1_Insert_m44059_MethodInfo,
	&IList_1_RemoveAt_m44060_MethodInfo,
	&IList_1_get_Item_m44056_MethodInfo,
	&IList_1_set_Item_m44057_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7863_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7862_il2cpp_TypeInfo,
	&IEnumerable_1_t7864_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7863_0_0_0;
extern Il2CppType IList_1_t7863_1_0_0;
struct IList_1_t7863;
extern Il2CppGenericClass IList_1_t7863_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7863_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7863_MethodInfos/* methods */
	, IList_1_t7863_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7863_il2cpp_TypeInfo/* element_class */
	, IList_1_t7863_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7863_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7863_0_0_0/* byval_arg */
	, &IList_1_t7863_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7863_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6155_il2cpp_TypeInfo;

// Goal_Script
#include "AssemblyU2DCSharp_Goal_Script.h"


// T System.Collections.Generic.IEnumerator`1<Goal_Script>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Goal_Script>
extern MethodInfo IEnumerator_1_get_Current_m44061_MethodInfo;
static PropertyInfo IEnumerator_1_t6155____Current_PropertyInfo = 
{
	&IEnumerator_1_t6155_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44061_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6155_PropertyInfos[] =
{
	&IEnumerator_1_t6155____Current_PropertyInfo,
	NULL
};
extern Il2CppType Goal_Script_t86_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44061_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Goal_Script>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44061_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6155_il2cpp_TypeInfo/* declaring_type */
	, &Goal_Script_t86_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44061_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6155_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44061_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6155_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6155_0_0_0;
extern Il2CppType IEnumerator_1_t6155_1_0_0;
struct IEnumerator_1_t6155;
extern Il2CppGenericClass IEnumerator_1_t6155_GenericClass;
TypeInfo IEnumerator_1_t6155_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6155_MethodInfos/* methods */
	, IEnumerator_1_t6155_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6155_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6155_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6155_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6155_0_0_0/* byval_arg */
	, &IEnumerator_1_t6155_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6155_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Goal_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_121.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3032_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Goal_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_121MethodDeclarations.h"

extern TypeInfo Goal_Script_t86_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15473_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGoal_Script_t86_m33650_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Goal_Script>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Goal_Script>(System.Int32)
#define Array_InternalArray__get_Item_TisGoal_Script_t86_m33650(__this, p0, method) (Goal_Script_t86 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Goal_Script>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Goal_Script>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Goal_Script>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Goal_Script>::MoveNext()
// T System.Array/InternalEnumerator`1<Goal_Script>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Goal_Script>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3032____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3032_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3032, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3032____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3032_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3032, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3032_FieldInfos[] =
{
	&InternalEnumerator_1_t3032____array_0_FieldInfo,
	&InternalEnumerator_1_t3032____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15470_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3032____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3032_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15470_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3032____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3032_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15473_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3032_PropertyInfos[] =
{
	&InternalEnumerator_1_t3032____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3032____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3032_InternalEnumerator_1__ctor_m15469_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15469_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Goal_Script>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15469_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3032_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3032_InternalEnumerator_1__ctor_m15469_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15469_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15470_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Goal_Script>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15470_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3032_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15470_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15471_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Goal_Script>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15471_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3032_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15471_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15472_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Goal_Script>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15472_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3032_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15472_GenericMethod/* genericMethod */

};
extern Il2CppType Goal_Script_t86_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15473_GenericMethod;
// T System.Array/InternalEnumerator`1<Goal_Script>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15473_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3032_il2cpp_TypeInfo/* declaring_type */
	, &Goal_Script_t86_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15473_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3032_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15469_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15470_MethodInfo,
	&InternalEnumerator_1_Dispose_m15471_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15472_MethodInfo,
	&InternalEnumerator_1_get_Current_m15473_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15472_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15471_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3032_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15470_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15472_MethodInfo,
	&InternalEnumerator_1_Dispose_m15471_MethodInfo,
	&InternalEnumerator_1_get_Current_m15473_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3032_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6155_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3032_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6155_il2cpp_TypeInfo, 7},
};
extern TypeInfo Goal_Script_t86_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3032_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15473_MethodInfo/* Method Usage */,
	&Goal_Script_t86_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGoal_Script_t86_m33650_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3032_0_0_0;
extern Il2CppType InternalEnumerator_1_t3032_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3032_GenericClass;
TypeInfo InternalEnumerator_1_t3032_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3032_MethodInfos/* methods */
	, InternalEnumerator_1_t3032_PropertyInfos/* properties */
	, InternalEnumerator_1_t3032_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3032_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3032_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3032_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3032_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3032_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3032_1_0_0/* this_arg */
	, InternalEnumerator_1_t3032_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3032_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3032_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3032)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7865_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Goal_Script>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Goal_Script>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Goal_Script>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Goal_Script>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Goal_Script>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Goal_Script>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Goal_Script>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Goal_Script>
extern MethodInfo ICollection_1_get_Count_m44062_MethodInfo;
static PropertyInfo ICollection_1_t7865____Count_PropertyInfo = 
{
	&ICollection_1_t7865_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44062_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44063_MethodInfo;
static PropertyInfo ICollection_1_t7865____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7865_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44063_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7865_PropertyInfos[] =
{
	&ICollection_1_t7865____Count_PropertyInfo,
	&ICollection_1_t7865____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44062_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Goal_Script>::get_Count()
MethodInfo ICollection_1_get_Count_m44062_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44062_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44063_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Goal_Script>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44063_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44063_GenericMethod/* genericMethod */

};
extern Il2CppType Goal_Script_t86_0_0_0;
extern Il2CppType Goal_Script_t86_0_0_0;
static ParameterInfo ICollection_1_t7865_ICollection_1_Add_m44064_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Goal_Script_t86_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44064_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Goal_Script>::Add(T)
MethodInfo ICollection_1_Add_m44064_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7865_ICollection_1_Add_m44064_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44064_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44065_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Goal_Script>::Clear()
MethodInfo ICollection_1_Clear_m44065_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44065_GenericMethod/* genericMethod */

};
extern Il2CppType Goal_Script_t86_0_0_0;
static ParameterInfo ICollection_1_t7865_ICollection_1_Contains_m44066_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Goal_Script_t86_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44066_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Goal_Script>::Contains(T)
MethodInfo ICollection_1_Contains_m44066_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7865_ICollection_1_Contains_m44066_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44066_GenericMethod/* genericMethod */

};
extern Il2CppType Goal_ScriptU5BU5D_t5399_0_0_0;
extern Il2CppType Goal_ScriptU5BU5D_t5399_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7865_ICollection_1_CopyTo_m44067_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Goal_ScriptU5BU5D_t5399_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44067_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Goal_Script>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44067_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7865_ICollection_1_CopyTo_m44067_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44067_GenericMethod/* genericMethod */

};
extern Il2CppType Goal_Script_t86_0_0_0;
static ParameterInfo ICollection_1_t7865_ICollection_1_Remove_m44068_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Goal_Script_t86_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44068_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Goal_Script>::Remove(T)
MethodInfo ICollection_1_Remove_m44068_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7865_ICollection_1_Remove_m44068_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44068_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7865_MethodInfos[] =
{
	&ICollection_1_get_Count_m44062_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44063_MethodInfo,
	&ICollection_1_Add_m44064_MethodInfo,
	&ICollection_1_Clear_m44065_MethodInfo,
	&ICollection_1_Contains_m44066_MethodInfo,
	&ICollection_1_CopyTo_m44067_MethodInfo,
	&ICollection_1_Remove_m44068_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7867_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7865_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7867_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7865_0_0_0;
extern Il2CppType ICollection_1_t7865_1_0_0;
struct ICollection_1_t7865;
extern Il2CppGenericClass ICollection_1_t7865_GenericClass;
TypeInfo ICollection_1_t7865_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7865_MethodInfos/* methods */
	, ICollection_1_t7865_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7865_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7865_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7865_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7865_0_0_0/* byval_arg */
	, &ICollection_1_t7865_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7865_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Goal_Script>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Goal_Script>
extern Il2CppType IEnumerator_1_t6155_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44069_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Goal_Script>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44069_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7867_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6155_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44069_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7867_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44069_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7867_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7867_0_0_0;
extern Il2CppType IEnumerable_1_t7867_1_0_0;
struct IEnumerable_1_t7867;
extern Il2CppGenericClass IEnumerable_1_t7867_GenericClass;
TypeInfo IEnumerable_1_t7867_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7867_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7867_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7867_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7867_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7867_0_0_0/* byval_arg */
	, &IEnumerable_1_t7867_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7867_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7866_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Goal_Script>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Goal_Script>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Goal_Script>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Goal_Script>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Goal_Script>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Goal_Script>
extern MethodInfo IList_1_get_Item_m44070_MethodInfo;
extern MethodInfo IList_1_set_Item_m44071_MethodInfo;
static PropertyInfo IList_1_t7866____Item_PropertyInfo = 
{
	&IList_1_t7866_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44070_MethodInfo/* get */
	, &IList_1_set_Item_m44071_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7866_PropertyInfos[] =
{
	&IList_1_t7866____Item_PropertyInfo,
	NULL
};
extern Il2CppType Goal_Script_t86_0_0_0;
static ParameterInfo IList_1_t7866_IList_1_IndexOf_m44072_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Goal_Script_t86_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44072_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Goal_Script>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44072_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7866_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7866_IList_1_IndexOf_m44072_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44072_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Goal_Script_t86_0_0_0;
static ParameterInfo IList_1_t7866_IList_1_Insert_m44073_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Goal_Script_t86_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44073_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Goal_Script>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44073_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7866_IList_1_Insert_m44073_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44073_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7866_IList_1_RemoveAt_m44074_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44074_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Goal_Script>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44074_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7866_IList_1_RemoveAt_m44074_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44074_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7866_IList_1_get_Item_m44070_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Goal_Script_t86_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44070_GenericMethod;
// T System.Collections.Generic.IList`1<Goal_Script>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44070_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7866_il2cpp_TypeInfo/* declaring_type */
	, &Goal_Script_t86_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7866_IList_1_get_Item_m44070_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44070_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Goal_Script_t86_0_0_0;
static ParameterInfo IList_1_t7866_IList_1_set_Item_m44071_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Goal_Script_t86_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44071_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Goal_Script>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44071_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7866_IList_1_set_Item_m44071_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44071_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7866_MethodInfos[] =
{
	&IList_1_IndexOf_m44072_MethodInfo,
	&IList_1_Insert_m44073_MethodInfo,
	&IList_1_RemoveAt_m44074_MethodInfo,
	&IList_1_get_Item_m44070_MethodInfo,
	&IList_1_set_Item_m44071_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7866_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7865_il2cpp_TypeInfo,
	&IEnumerable_1_t7867_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7866_0_0_0;
extern Il2CppType IList_1_t7866_1_0_0;
struct IList_1_t7866;
extern Il2CppGenericClass IList_1_t7866_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7866_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7866_MethodInfos/* methods */
	, IList_1_t7866_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7866_il2cpp_TypeInfo/* element_class */
	, IList_1_t7866_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7866_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7866_0_0_0/* byval_arg */
	, &IList_1_t7866_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7866_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Goal_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_44.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3033_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Goal_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_44MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Goal_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_40.h"
extern TypeInfo InvokableCall_1_t3034_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Goal_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_40MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15476_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15478_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Goal_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Goal_Script>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Goal_Script>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3033____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3033_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3033, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3033_FieldInfos[] =
{
	&CachedInvokableCall_1_t3033____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Goal_Script_t86_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3033_CachedInvokableCall_1__ctor_m15474_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Goal_Script_t86_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15474_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Goal_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15474_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3033_CachedInvokableCall_1__ctor_m15474_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15474_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3033_CachedInvokableCall_1_Invoke_m15475_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15475_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Goal_Script>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15475_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3033_CachedInvokableCall_1_Invoke_m15475_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15475_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3033_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15474_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15475_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15475_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15479_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3033_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15475_MethodInfo,
	&InvokableCall_1_Find_m15479_MethodInfo,
};
extern Il2CppType UnityAction_1_t3035_0_0_0;
extern TypeInfo UnityAction_1_t3035_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisGoal_Script_t86_m33660_MethodInfo;
extern TypeInfo Goal_Script_t86_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15481_MethodInfo;
extern TypeInfo Goal_Script_t86_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3033_RGCTXData[8] = 
{
	&UnityAction_1_t3035_0_0_0/* Type Usage */,
	&UnityAction_1_t3035_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGoal_Script_t86_m33660_MethodInfo/* Method Usage */,
	&Goal_Script_t86_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15481_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15476_MethodInfo/* Method Usage */,
	&Goal_Script_t86_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15478_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3033_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3033_1_0_0;
struct CachedInvokableCall_1_t3033;
extern Il2CppGenericClass CachedInvokableCall_1_t3033_GenericClass;
TypeInfo CachedInvokableCall_1_t3033_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3033_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3033_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3034_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3033_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3033_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3033_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3033_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3033_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3033_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3033_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3033)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Goal_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_47.h"
extern TypeInfo UnityAction_1_t3035_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Goal_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_47MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Goal_Script>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Goal_Script>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisGoal_Script_t86_m33660(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Goal_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Goal_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Goal_Script>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Goal_Script>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Goal_Script>
extern Il2CppType UnityAction_1_t3035_0_0_1;
FieldInfo InvokableCall_1_t3034____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3035_0_0_1/* type */
	, &InvokableCall_1_t3034_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3034, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3034_FieldInfos[] =
{
	&InvokableCall_1_t3034____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3034_InvokableCall_1__ctor_m15476_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15476_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Goal_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15476_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3034_InvokableCall_1__ctor_m15476_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15476_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3035_0_0_0;
static ParameterInfo InvokableCall_1_t3034_InvokableCall_1__ctor_m15477_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3035_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15477_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Goal_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15477_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3034_InvokableCall_1__ctor_m15477_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15477_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3034_InvokableCall_1_Invoke_m15478_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15478_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Goal_Script>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15478_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3034_InvokableCall_1_Invoke_m15478_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15478_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3034_InvokableCall_1_Find_m15479_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15479_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Goal_Script>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15479_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3034_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3034_InvokableCall_1_Find_m15479_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15479_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3034_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15476_MethodInfo,
	&InvokableCall_1__ctor_m15477_MethodInfo,
	&InvokableCall_1_Invoke_m15478_MethodInfo,
	&InvokableCall_1_Find_m15479_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3034_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15478_MethodInfo,
	&InvokableCall_1_Find_m15479_MethodInfo,
};
extern TypeInfo UnityAction_1_t3035_il2cpp_TypeInfo;
extern TypeInfo Goal_Script_t86_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3034_RGCTXData[5] = 
{
	&UnityAction_1_t3035_0_0_0/* Type Usage */,
	&UnityAction_1_t3035_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGoal_Script_t86_m33660_MethodInfo/* Method Usage */,
	&Goal_Script_t86_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15481_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3034_0_0_0;
extern Il2CppType InvokableCall_1_t3034_1_0_0;
struct InvokableCall_1_t3034;
extern Il2CppGenericClass InvokableCall_1_t3034_GenericClass;
TypeInfo InvokableCall_1_t3034_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3034_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3034_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3034_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3034_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3034_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3034_0_0_0/* byval_arg */
	, &InvokableCall_1_t3034_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3034_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3034_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3034)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Goal_Script>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Goal_Script>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Goal_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Goal_Script>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Goal_Script>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3035_UnityAction_1__ctor_m15480_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15480_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Goal_Script>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15480_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3035_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3035_UnityAction_1__ctor_m15480_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15480_GenericMethod/* genericMethod */

};
extern Il2CppType Goal_Script_t86_0_0_0;
static ParameterInfo UnityAction_1_t3035_UnityAction_1_Invoke_m15481_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Goal_Script_t86_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15481_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Goal_Script>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15481_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3035_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3035_UnityAction_1_Invoke_m15481_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15481_GenericMethod/* genericMethod */

};
extern Il2CppType Goal_Script_t86_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3035_UnityAction_1_BeginInvoke_m15482_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Goal_Script_t86_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15482_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Goal_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15482_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3035_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3035_UnityAction_1_BeginInvoke_m15482_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15482_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3035_UnityAction_1_EndInvoke_m15483_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15483_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Goal_Script>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15483_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3035_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3035_UnityAction_1_EndInvoke_m15483_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15483_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3035_MethodInfos[] =
{
	&UnityAction_1__ctor_m15480_MethodInfo,
	&UnityAction_1_Invoke_m15481_MethodInfo,
	&UnityAction_1_BeginInvoke_m15482_MethodInfo,
	&UnityAction_1_EndInvoke_m15483_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15482_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15483_MethodInfo;
static MethodInfo* UnityAction_1_t3035_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15481_MethodInfo,
	&UnityAction_1_BeginInvoke_m15482_MethodInfo,
	&UnityAction_1_EndInvoke_m15483_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3035_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3035_1_0_0;
struct UnityAction_1_t3035;
extern Il2CppGenericClass UnityAction_1_t3035_GenericClass;
TypeInfo UnityAction_1_t3035_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3035_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3035_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3035_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3035_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3035_0_0_0/* byval_arg */
	, &UnityAction_1_t3035_1_0_0/* this_arg */
	, UnityAction_1_t3035_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3035_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3035)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6157_il2cpp_TypeInfo;

// ChooseTeam
#include "AssemblyU2DCSharp_ChooseTeam.h"


// T System.Collections.Generic.IEnumerator`1<ChooseTeam>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<ChooseTeam>
extern MethodInfo IEnumerator_1_get_Current_m44075_MethodInfo;
static PropertyInfo IEnumerator_1_t6157____Current_PropertyInfo = 
{
	&IEnumerator_1_t6157_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44075_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6157_PropertyInfos[] =
{
	&IEnumerator_1_t6157____Current_PropertyInfo,
	NULL
};
extern Il2CppType ChooseTeam_t88_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44075_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<ChooseTeam>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44075_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6157_il2cpp_TypeInfo/* declaring_type */
	, &ChooseTeam_t88_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44075_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6157_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44075_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6157_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6157_0_0_0;
extern Il2CppType IEnumerator_1_t6157_1_0_0;
struct IEnumerator_1_t6157;
extern Il2CppGenericClass IEnumerator_1_t6157_GenericClass;
TypeInfo IEnumerator_1_t6157_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6157_MethodInfos/* methods */
	, IEnumerator_1_t6157_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6157_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6157_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6157_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6157_0_0_0/* byval_arg */
	, &IEnumerator_1_t6157_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6157_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<ChooseTeam>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_122.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3036_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<ChooseTeam>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_122MethodDeclarations.h"

extern TypeInfo ChooseTeam_t88_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15488_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisChooseTeam_t88_m33662_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<ChooseTeam>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<ChooseTeam>(System.Int32)
#define Array_InternalArray__get_Item_TisChooseTeam_t88_m33662(__this, p0, method) (ChooseTeam_t88 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<ChooseTeam>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<ChooseTeam>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<ChooseTeam>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<ChooseTeam>::MoveNext()
// T System.Array/InternalEnumerator`1<ChooseTeam>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<ChooseTeam>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3036____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3036_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3036, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3036____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3036_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3036, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3036_FieldInfos[] =
{
	&InternalEnumerator_1_t3036____array_0_FieldInfo,
	&InternalEnumerator_1_t3036____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15485_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3036____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3036_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15485_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3036____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3036_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15488_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3036_PropertyInfos[] =
{
	&InternalEnumerator_1_t3036____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3036____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3036_InternalEnumerator_1__ctor_m15484_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15484_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ChooseTeam>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15484_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3036_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3036_InternalEnumerator_1__ctor_m15484_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15484_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15485_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<ChooseTeam>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15485_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3036_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15485_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15486_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ChooseTeam>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15486_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3036_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15486_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15487_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<ChooseTeam>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15487_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3036_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15487_GenericMethod/* genericMethod */

};
extern Il2CppType ChooseTeam_t88_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15488_GenericMethod;
// T System.Array/InternalEnumerator`1<ChooseTeam>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15488_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3036_il2cpp_TypeInfo/* declaring_type */
	, &ChooseTeam_t88_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15488_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3036_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15484_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15485_MethodInfo,
	&InternalEnumerator_1_Dispose_m15486_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15487_MethodInfo,
	&InternalEnumerator_1_get_Current_m15488_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15487_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15486_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3036_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15485_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15487_MethodInfo,
	&InternalEnumerator_1_Dispose_m15486_MethodInfo,
	&InternalEnumerator_1_get_Current_m15488_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3036_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6157_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3036_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6157_il2cpp_TypeInfo, 7},
};
extern TypeInfo ChooseTeam_t88_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3036_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15488_MethodInfo/* Method Usage */,
	&ChooseTeam_t88_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisChooseTeam_t88_m33662_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3036_0_0_0;
extern Il2CppType InternalEnumerator_1_t3036_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3036_GenericClass;
TypeInfo InternalEnumerator_1_t3036_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3036_MethodInfos/* methods */
	, InternalEnumerator_1_t3036_PropertyInfos/* properties */
	, InternalEnumerator_1_t3036_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3036_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3036_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3036_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3036_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3036_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3036_1_0_0/* this_arg */
	, InternalEnumerator_1_t3036_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3036_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3036_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3036)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7868_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<ChooseTeam>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<ChooseTeam>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<ChooseTeam>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<ChooseTeam>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<ChooseTeam>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<ChooseTeam>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<ChooseTeam>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<ChooseTeam>
extern MethodInfo ICollection_1_get_Count_m44076_MethodInfo;
static PropertyInfo ICollection_1_t7868____Count_PropertyInfo = 
{
	&ICollection_1_t7868_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44076_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44077_MethodInfo;
static PropertyInfo ICollection_1_t7868____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7868_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44077_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7868_PropertyInfos[] =
{
	&ICollection_1_t7868____Count_PropertyInfo,
	&ICollection_1_t7868____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44076_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<ChooseTeam>::get_Count()
MethodInfo ICollection_1_get_Count_m44076_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7868_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44076_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44077_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ChooseTeam>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44077_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44077_GenericMethod/* genericMethod */

};
extern Il2CppType ChooseTeam_t88_0_0_0;
extern Il2CppType ChooseTeam_t88_0_0_0;
static ParameterInfo ICollection_1_t7868_ICollection_1_Add_m44078_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ChooseTeam_t88_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44078_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ChooseTeam>::Add(T)
MethodInfo ICollection_1_Add_m44078_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7868_ICollection_1_Add_m44078_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44078_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44079_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ChooseTeam>::Clear()
MethodInfo ICollection_1_Clear_m44079_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44079_GenericMethod/* genericMethod */

};
extern Il2CppType ChooseTeam_t88_0_0_0;
static ParameterInfo ICollection_1_t7868_ICollection_1_Contains_m44080_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ChooseTeam_t88_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44080_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ChooseTeam>::Contains(T)
MethodInfo ICollection_1_Contains_m44080_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7868_ICollection_1_Contains_m44080_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44080_GenericMethod/* genericMethod */

};
extern Il2CppType ChooseTeamU5BU5D_t5400_0_0_0;
extern Il2CppType ChooseTeamU5BU5D_t5400_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7868_ICollection_1_CopyTo_m44081_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ChooseTeamU5BU5D_t5400_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44081_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ChooseTeam>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44081_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7868_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7868_ICollection_1_CopyTo_m44081_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44081_GenericMethod/* genericMethod */

};
extern Il2CppType ChooseTeam_t88_0_0_0;
static ParameterInfo ICollection_1_t7868_ICollection_1_Remove_m44082_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ChooseTeam_t88_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44082_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ChooseTeam>::Remove(T)
MethodInfo ICollection_1_Remove_m44082_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7868_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7868_ICollection_1_Remove_m44082_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44082_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7868_MethodInfos[] =
{
	&ICollection_1_get_Count_m44076_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44077_MethodInfo,
	&ICollection_1_Add_m44078_MethodInfo,
	&ICollection_1_Clear_m44079_MethodInfo,
	&ICollection_1_Contains_m44080_MethodInfo,
	&ICollection_1_CopyTo_m44081_MethodInfo,
	&ICollection_1_Remove_m44082_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7870_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7868_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7870_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7868_0_0_0;
extern Il2CppType ICollection_1_t7868_1_0_0;
struct ICollection_1_t7868;
extern Il2CppGenericClass ICollection_1_t7868_GenericClass;
TypeInfo ICollection_1_t7868_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7868_MethodInfos/* methods */
	, ICollection_1_t7868_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7868_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7868_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7868_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7868_0_0_0/* byval_arg */
	, &ICollection_1_t7868_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7868_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ChooseTeam>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<ChooseTeam>
extern Il2CppType IEnumerator_1_t6157_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44083_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ChooseTeam>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44083_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7870_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6157_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44083_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7870_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44083_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7870_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7870_0_0_0;
extern Il2CppType IEnumerable_1_t7870_1_0_0;
struct IEnumerable_1_t7870;
extern Il2CppGenericClass IEnumerable_1_t7870_GenericClass;
TypeInfo IEnumerable_1_t7870_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7870_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7870_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7870_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7870_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7870_0_0_0/* byval_arg */
	, &IEnumerable_1_t7870_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7870_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7869_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<ChooseTeam>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<ChooseTeam>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<ChooseTeam>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<ChooseTeam>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<ChooseTeam>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<ChooseTeam>
extern MethodInfo IList_1_get_Item_m44084_MethodInfo;
extern MethodInfo IList_1_set_Item_m44085_MethodInfo;
static PropertyInfo IList_1_t7869____Item_PropertyInfo = 
{
	&IList_1_t7869_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44084_MethodInfo/* get */
	, &IList_1_set_Item_m44085_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7869_PropertyInfos[] =
{
	&IList_1_t7869____Item_PropertyInfo,
	NULL
};
extern Il2CppType ChooseTeam_t88_0_0_0;
static ParameterInfo IList_1_t7869_IList_1_IndexOf_m44086_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ChooseTeam_t88_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44086_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<ChooseTeam>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44086_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7869_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7869_IList_1_IndexOf_m44086_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44086_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ChooseTeam_t88_0_0_0;
static ParameterInfo IList_1_t7869_IList_1_Insert_m44087_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ChooseTeam_t88_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44087_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ChooseTeam>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44087_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7869_IList_1_Insert_m44087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44087_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7869_IList_1_RemoveAt_m44088_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44088_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ChooseTeam>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44088_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7869_IList_1_RemoveAt_m44088_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44088_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7869_IList_1_get_Item_m44084_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ChooseTeam_t88_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44084_GenericMethod;
// T System.Collections.Generic.IList`1<ChooseTeam>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44084_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7869_il2cpp_TypeInfo/* declaring_type */
	, &ChooseTeam_t88_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7869_IList_1_get_Item_m44084_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44084_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ChooseTeam_t88_0_0_0;
static ParameterInfo IList_1_t7869_IList_1_set_Item_m44085_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ChooseTeam_t88_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44085_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ChooseTeam>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44085_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7869_IList_1_set_Item_m44085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44085_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7869_MethodInfos[] =
{
	&IList_1_IndexOf_m44086_MethodInfo,
	&IList_1_Insert_m44087_MethodInfo,
	&IList_1_RemoveAt_m44088_MethodInfo,
	&IList_1_get_Item_m44084_MethodInfo,
	&IList_1_set_Item_m44085_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7869_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7868_il2cpp_TypeInfo,
	&IEnumerable_1_t7870_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7869_0_0_0;
extern Il2CppType IList_1_t7869_1_0_0;
struct IList_1_t7869;
extern Il2CppGenericClass IList_1_t7869_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7869_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7869_MethodInfos/* methods */
	, IList_1_t7869_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7869_il2cpp_TypeInfo/* element_class */
	, IList_1_t7869_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7869_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7869_0_0_0/* byval_arg */
	, &IList_1_t7869_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7869_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<ChooseTeam>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_45.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3037_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<ChooseTeam>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_45MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<ChooseTeam>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_41.h"
extern TypeInfo InvokableCall_1_t3038_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<ChooseTeam>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_41MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15491_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15493_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<ChooseTeam>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<ChooseTeam>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<ChooseTeam>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3037____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3037_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3037, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3037_FieldInfos[] =
{
	&CachedInvokableCall_1_t3037____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType ChooseTeam_t88_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3037_CachedInvokableCall_1__ctor_m15489_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &ChooseTeam_t88_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15489_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<ChooseTeam>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15489_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3037_CachedInvokableCall_1__ctor_m15489_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15489_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3037_CachedInvokableCall_1_Invoke_m15490_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15490_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<ChooseTeam>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15490_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3037_CachedInvokableCall_1_Invoke_m15490_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15490_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3037_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15489_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15490_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15490_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15494_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3037_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15490_MethodInfo,
	&InvokableCall_1_Find_m15494_MethodInfo,
};
extern Il2CppType UnityAction_1_t3039_0_0_0;
extern TypeInfo UnityAction_1_t3039_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisChooseTeam_t88_m33672_MethodInfo;
extern TypeInfo ChooseTeam_t88_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15496_MethodInfo;
extern TypeInfo ChooseTeam_t88_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3037_RGCTXData[8] = 
{
	&UnityAction_1_t3039_0_0_0/* Type Usage */,
	&UnityAction_1_t3039_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisChooseTeam_t88_m33672_MethodInfo/* Method Usage */,
	&ChooseTeam_t88_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15496_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15491_MethodInfo/* Method Usage */,
	&ChooseTeam_t88_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15493_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3037_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3037_1_0_0;
struct CachedInvokableCall_1_t3037;
extern Il2CppGenericClass CachedInvokableCall_1_t3037_GenericClass;
TypeInfo CachedInvokableCall_1_t3037_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3037_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3037_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3038_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3037_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3037_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3037_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3037_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3037_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3037_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3037_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3037)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<ChooseTeam>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_48.h"
extern TypeInfo UnityAction_1_t3039_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<ChooseTeam>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_48MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<ChooseTeam>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<ChooseTeam>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisChooseTeam_t88_m33672(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<ChooseTeam>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<ChooseTeam>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<ChooseTeam>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<ChooseTeam>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<ChooseTeam>
extern Il2CppType UnityAction_1_t3039_0_0_1;
FieldInfo InvokableCall_1_t3038____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3039_0_0_1/* type */
	, &InvokableCall_1_t3038_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3038, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3038_FieldInfos[] =
{
	&InvokableCall_1_t3038____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3038_InvokableCall_1__ctor_m15491_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15491_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ChooseTeam>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15491_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3038_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3038_InvokableCall_1__ctor_m15491_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15491_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3039_0_0_0;
static ParameterInfo InvokableCall_1_t3038_InvokableCall_1__ctor_m15492_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3039_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15492_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ChooseTeam>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15492_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3038_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3038_InvokableCall_1__ctor_m15492_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15492_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3038_InvokableCall_1_Invoke_m15493_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15493_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ChooseTeam>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15493_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3038_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3038_InvokableCall_1_Invoke_m15493_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15493_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3038_InvokableCall_1_Find_m15494_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15494_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<ChooseTeam>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15494_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3038_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3038_InvokableCall_1_Find_m15494_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15494_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3038_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15491_MethodInfo,
	&InvokableCall_1__ctor_m15492_MethodInfo,
	&InvokableCall_1_Invoke_m15493_MethodInfo,
	&InvokableCall_1_Find_m15494_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3038_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15493_MethodInfo,
	&InvokableCall_1_Find_m15494_MethodInfo,
};
extern TypeInfo UnityAction_1_t3039_il2cpp_TypeInfo;
extern TypeInfo ChooseTeam_t88_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3038_RGCTXData[5] = 
{
	&UnityAction_1_t3039_0_0_0/* Type Usage */,
	&UnityAction_1_t3039_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisChooseTeam_t88_m33672_MethodInfo/* Method Usage */,
	&ChooseTeam_t88_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15496_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3038_0_0_0;
extern Il2CppType InvokableCall_1_t3038_1_0_0;
struct InvokableCall_1_t3038;
extern Il2CppGenericClass InvokableCall_1_t3038_GenericClass;
TypeInfo InvokableCall_1_t3038_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3038_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3038_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3038_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3038_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3038_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3038_0_0_0/* byval_arg */
	, &InvokableCall_1_t3038_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3038_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3038_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3038)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<ChooseTeam>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<ChooseTeam>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<ChooseTeam>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<ChooseTeam>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<ChooseTeam>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3039_UnityAction_1__ctor_m15495_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15495_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ChooseTeam>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15495_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3039_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3039_UnityAction_1__ctor_m15495_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15495_GenericMethod/* genericMethod */

};
extern Il2CppType ChooseTeam_t88_0_0_0;
static ParameterInfo UnityAction_1_t3039_UnityAction_1_Invoke_m15496_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ChooseTeam_t88_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15496_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ChooseTeam>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15496_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3039_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3039_UnityAction_1_Invoke_m15496_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15496_GenericMethod/* genericMethod */

};
extern Il2CppType ChooseTeam_t88_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3039_UnityAction_1_BeginInvoke_m15497_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ChooseTeam_t88_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15497_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<ChooseTeam>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15497_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3039_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3039_UnityAction_1_BeginInvoke_m15497_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15497_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3039_UnityAction_1_EndInvoke_m15498_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15498_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ChooseTeam>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15498_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3039_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3039_UnityAction_1_EndInvoke_m15498_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15498_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3039_MethodInfos[] =
{
	&UnityAction_1__ctor_m15495_MethodInfo,
	&UnityAction_1_Invoke_m15496_MethodInfo,
	&UnityAction_1_BeginInvoke_m15497_MethodInfo,
	&UnityAction_1_EndInvoke_m15498_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15497_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15498_MethodInfo;
static MethodInfo* UnityAction_1_t3039_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15496_MethodInfo,
	&UnityAction_1_BeginInvoke_m15497_MethodInfo,
	&UnityAction_1_EndInvoke_m15498_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3039_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3039_1_0_0;
struct UnityAction_1_t3039;
extern Il2CppGenericClass UnityAction_1_t3039_GenericClass;
TypeInfo UnityAction_1_t3039_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3039_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3039_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3039_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3039_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3039_0_0_0/* byval_arg */
	, &UnityAction_1_t3039_1_0_0/* this_arg */
	, UnityAction_1_t3039_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3039_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3039)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6159_il2cpp_TypeInfo;

// ShieldMenu
#include "AssemblyU2DCSharp_ShieldMenu.h"


// T System.Collections.Generic.IEnumerator`1<ShieldMenu>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<ShieldMenu>
extern MethodInfo IEnumerator_1_get_Current_m44089_MethodInfo;
static PropertyInfo IEnumerator_1_t6159____Current_PropertyInfo = 
{
	&IEnumerator_1_t6159_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44089_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6159_PropertyInfos[] =
{
	&IEnumerator_1_t6159____Current_PropertyInfo,
	NULL
};
extern Il2CppType ShieldMenu_t92_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44089_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<ShieldMenu>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44089_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6159_il2cpp_TypeInfo/* declaring_type */
	, &ShieldMenu_t92_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44089_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6159_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44089_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6159_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6159_0_0_0;
extern Il2CppType IEnumerator_1_t6159_1_0_0;
struct IEnumerator_1_t6159;
extern Il2CppGenericClass IEnumerator_1_t6159_GenericClass;
TypeInfo IEnumerator_1_t6159_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6159_MethodInfos/* methods */
	, IEnumerator_1_t6159_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6159_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6159_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6159_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6159_0_0_0/* byval_arg */
	, &IEnumerator_1_t6159_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6159_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<ShieldMenu>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_123.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3040_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<ShieldMenu>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_123MethodDeclarations.h"

extern TypeInfo ShieldMenu_t92_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15503_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisShieldMenu_t92_m33674_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<ShieldMenu>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<ShieldMenu>(System.Int32)
#define Array_InternalArray__get_Item_TisShieldMenu_t92_m33674(__this, p0, method) (ShieldMenu_t92 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<ShieldMenu>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<ShieldMenu>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<ShieldMenu>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<ShieldMenu>::MoveNext()
// T System.Array/InternalEnumerator`1<ShieldMenu>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<ShieldMenu>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3040____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3040_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3040, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3040____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3040_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3040, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3040_FieldInfos[] =
{
	&InternalEnumerator_1_t3040____array_0_FieldInfo,
	&InternalEnumerator_1_t3040____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15500_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3040____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3040_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15500_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3040____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3040_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15503_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3040_PropertyInfos[] =
{
	&InternalEnumerator_1_t3040____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3040____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3040_InternalEnumerator_1__ctor_m15499_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15499_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ShieldMenu>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15499_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3040_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3040_InternalEnumerator_1__ctor_m15499_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15499_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15500_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<ShieldMenu>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15500_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3040_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15500_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15501_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ShieldMenu>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15501_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3040_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15501_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15502_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<ShieldMenu>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15502_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3040_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15502_GenericMethod/* genericMethod */

};
extern Il2CppType ShieldMenu_t92_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15503_GenericMethod;
// T System.Array/InternalEnumerator`1<ShieldMenu>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15503_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3040_il2cpp_TypeInfo/* declaring_type */
	, &ShieldMenu_t92_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15503_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3040_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15499_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15500_MethodInfo,
	&InternalEnumerator_1_Dispose_m15501_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15502_MethodInfo,
	&InternalEnumerator_1_get_Current_m15503_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15502_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15501_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3040_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15500_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15502_MethodInfo,
	&InternalEnumerator_1_Dispose_m15501_MethodInfo,
	&InternalEnumerator_1_get_Current_m15503_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3040_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6159_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3040_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6159_il2cpp_TypeInfo, 7},
};
extern TypeInfo ShieldMenu_t92_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3040_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15503_MethodInfo/* Method Usage */,
	&ShieldMenu_t92_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisShieldMenu_t92_m33674_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3040_0_0_0;
extern Il2CppType InternalEnumerator_1_t3040_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3040_GenericClass;
TypeInfo InternalEnumerator_1_t3040_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3040_MethodInfos/* methods */
	, InternalEnumerator_1_t3040_PropertyInfos/* properties */
	, InternalEnumerator_1_t3040_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3040_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3040_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3040_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3040_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3040_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3040_1_0_0/* this_arg */
	, InternalEnumerator_1_t3040_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3040_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3040_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3040)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
