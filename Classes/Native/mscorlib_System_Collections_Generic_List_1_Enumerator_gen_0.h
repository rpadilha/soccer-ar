﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Joystick_Script>
struct List_1_t101;
// Joystick_Script
struct Joystick_Script_t102;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Joystick_Script>
struct Enumerator_t200 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Joystick_Script>::l
	List_1_t101 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Joystick_Script>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Joystick_Script>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Joystick_Script>::current
	Joystick_Script_t102 * ___current_3;
};
