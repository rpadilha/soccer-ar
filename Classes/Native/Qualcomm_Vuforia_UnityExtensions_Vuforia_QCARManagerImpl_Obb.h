﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Vuforia.QCARManagerImpl/Obb2D
#pragma pack(push, tp, 1)
struct Obb2D_t686 
{
	// UnityEngine.Vector2 Vuforia.QCARManagerImpl/Obb2D::center
	Vector2_t99  ___center_0;
	// UnityEngine.Vector2 Vuforia.QCARManagerImpl/Obb2D::halfExtents
	Vector2_t99  ___halfExtents_1;
	// System.Single Vuforia.QCARManagerImpl/Obb2D::rotation
	float ___rotation_2;
	// System.Int32 Vuforia.QCARManagerImpl/Obb2D::unused
	int32_t ___unused_3;
};
#pragma pack(pop, tp)
