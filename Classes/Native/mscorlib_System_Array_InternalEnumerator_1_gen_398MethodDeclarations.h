﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.PrimitiveType>
struct InternalEnumerator_1_t4608;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.PrimitiveType
#include "UnityEngine_UnityEngine_PrimitiveType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.PrimitiveType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m27864 (InternalEnumerator_1_t4608 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.PrimitiveType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27865 (InternalEnumerator_1_t4608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.PrimitiveType>::Dispose()
 void InternalEnumerator_1_Dispose_m27866 (InternalEnumerator_1_t4608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.PrimitiveType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m27867 (InternalEnumerator_1_t4608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.PrimitiveType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m27868 (InternalEnumerator_1_t4608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
