﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Vuforia.BackgroundPlaneAbstractBehaviour[]
// Vuforia.BackgroundPlaneAbstractBehaviour[]
struct BackgroundPlaneAbstractBehaviourU5BU5D_t938  : public Array_t
{
};
struct BackgroundPlaneAbstractBehaviourU5BU5D_t938_StaticFields{
};
// Vuforia.IVideoBackgroundEventHandler[]
// Vuforia.IVideoBackgroundEventHandler[]
struct IVideoBackgroundEventHandlerU5BU5D_t4502  : public Array_t
{
};
// Vuforia.CloudRecoAbstractBehaviour[]
// Vuforia.CloudRecoAbstractBehaviour[]
struct CloudRecoAbstractBehaviourU5BU5D_t5620  : public Array_t
{
};
// Vuforia.CylinderTargetAbstractBehaviour[]
// Vuforia.CylinderTargetAbstractBehaviour[]
struct CylinderTargetAbstractBehaviourU5BU5D_t5621  : public Array_t
{
};
// Vuforia.IEditorCylinderTargetBehaviour[]
// Vuforia.IEditorCylinderTargetBehaviour[]
struct IEditorCylinderTargetBehaviourU5BU5D_t5622  : public Array_t
{
};
// Vuforia.IEditorDataSetTrackableBehaviour[]
// Vuforia.IEditorDataSetTrackableBehaviour[]
struct IEditorDataSetTrackableBehaviourU5BU5D_t5623  : public Array_t
{
};
// Vuforia.IEditorTrackableBehaviour[]
// Vuforia.IEditorTrackableBehaviour[]
struct IEditorTrackableBehaviourU5BU5D_t5624  : public Array_t
{
};
// Vuforia.DataSetTrackableBehaviour[]
// Vuforia.DataSetTrackableBehaviour[]
struct DataSetTrackableBehaviourU5BU5D_t909  : public Array_t
{
};
// Vuforia.WorldCenterTrackableBehaviour[]
// Vuforia.WorldCenterTrackableBehaviour[]
struct WorldCenterTrackableBehaviourU5BU5D_t5625  : public Array_t
{
};
// Vuforia.TrackableBehaviour[]
// Vuforia.TrackableBehaviour[]
struct TrackableBehaviourU5BU5D_t857  : public Array_t
{
};
// Vuforia.DataSetLoadAbstractBehaviour[]
// Vuforia.DataSetLoadAbstractBehaviour[]
struct DataSetLoadAbstractBehaviourU5BU5D_t5626  : public Array_t
{
};
// Vuforia.QCARAbstractBehaviour[]
// Vuforia.QCARAbstractBehaviour[]
struct QCARAbstractBehaviourU5BU5D_t5627  : public Array_t
{
};
// Vuforia.IEditorQCARBehaviour[]
// Vuforia.IEditorQCARBehaviour[]
struct IEditorQCARBehaviourU5BU5D_t5628  : public Array_t
{
};
// Vuforia.ITrackableEventHandler[]
// Vuforia.ITrackableEventHandler[]
struct ITrackableEventHandlerU5BU5D_t3834  : public Array_t
{
};
// Vuforia.HideExcessAreaAbstractBehaviour[]
// Vuforia.HideExcessAreaAbstractBehaviour[]
struct HideExcessAreaAbstractBehaviourU5BU5D_t5629  : public Array_t
{
};
// Vuforia.ImageTargetAbstractBehaviour[]
// Vuforia.ImageTargetAbstractBehaviour[]
struct ImageTargetAbstractBehaviourU5BU5D_t5630  : public Array_t
{
};
// Vuforia.IEditorImageTargetBehaviour[]
// Vuforia.IEditorImageTargetBehaviour[]
struct IEditorImageTargetBehaviourU5BU5D_t5631  : public Array_t
{
};
// Vuforia.KeepAliveAbstractBehaviour[]
// Vuforia.KeepAliveAbstractBehaviour[]
struct KeepAliveAbstractBehaviourU5BU5D_t5632  : public Array_t
{
};
struct KeepAliveAbstractBehaviourU5BU5D_t5632_StaticFields{
};
// Vuforia.MarkerAbstractBehaviour[]
// Vuforia.MarkerAbstractBehaviour[]
struct MarkerAbstractBehaviourU5BU5D_t908  : public Array_t
{
};
// Vuforia.IEditorMarkerBehaviour[]
// Vuforia.IEditorMarkerBehaviour[]
struct IEditorMarkerBehaviourU5BU5D_t5633  : public Array_t
{
};
// Vuforia.MaskOutAbstractBehaviour[]
// Vuforia.MaskOutAbstractBehaviour[]
struct MaskOutAbstractBehaviourU5BU5D_t5634  : public Array_t
{
};
// Vuforia.MultiTargetAbstractBehaviour[]
// Vuforia.MultiTargetAbstractBehaviour[]
struct MultiTargetAbstractBehaviourU5BU5D_t5635  : public Array_t
{
};
// Vuforia.IEditorMultiTargetBehaviour[]
// Vuforia.IEditorMultiTargetBehaviour[]
struct IEditorMultiTargetBehaviourU5BU5D_t5636  : public Array_t
{
};
// Vuforia.ObjectTargetAbstractBehaviour[]
// Vuforia.ObjectTargetAbstractBehaviour[]
struct ObjectTargetAbstractBehaviourU5BU5D_t5637  : public Array_t
{
};
// Vuforia.IEditorObjectTargetBehaviour[]
// Vuforia.IEditorObjectTargetBehaviour[]
struct IEditorObjectTargetBehaviourU5BU5D_t5638  : public Array_t
{
};
// Vuforia.PropAbstractBehaviour[]
// Vuforia.PropAbstractBehaviour[]
struct PropAbstractBehaviourU5BU5D_t901  : public Array_t
{
};
// Vuforia.IEditorPropBehaviour[]
// Vuforia.IEditorPropBehaviour[]
struct IEditorPropBehaviourU5BU5D_t5639  : public Array_t
{
};
// Vuforia.SmartTerrainTrackableBehaviour[]
// Vuforia.SmartTerrainTrackableBehaviour[]
struct SmartTerrainTrackableBehaviourU5BU5D_t864  : public Array_t
{
};
// Vuforia.ReconstructionAbstractBehaviour[]
// Vuforia.ReconstructionAbstractBehaviour[]
struct ReconstructionAbstractBehaviourU5BU5D_t865  : public Array_t
{
};
// Vuforia.IEditorReconstructionBehaviour[]
// Vuforia.IEditorReconstructionBehaviour[]
struct IEditorReconstructionBehaviourU5BU5D_t5640  : public Array_t
{
};
// Vuforia.ReconstructionFromTargetAbstractBehaviour[]
// Vuforia.ReconstructionFromTargetAbstractBehaviour[]
struct ReconstructionFromTargetAbstractBehaviourU5BU5D_t5641  : public Array_t
{
};
// Vuforia.SmartTerrainTrackerAbstractBehaviour[]
// Vuforia.SmartTerrainTrackerAbstractBehaviour[]
struct SmartTerrainTrackerAbstractBehaviourU5BU5D_t5642  : public Array_t
{
};
// Vuforia.IEditorSmartTerrainTrackerBehaviour[]
// Vuforia.IEditorSmartTerrainTrackerBehaviour[]
struct IEditorSmartTerrainTrackerBehaviourU5BU5D_t5643  : public Array_t
{
};
// Vuforia.SurfaceAbstractBehaviour[]
// Vuforia.SurfaceAbstractBehaviour[]
struct SurfaceAbstractBehaviourU5BU5D_t4267  : public Array_t
{
};
// Vuforia.IEditorSurfaceBehaviour[]
// Vuforia.IEditorSurfaceBehaviour[]
struct IEditorSurfaceBehaviourU5BU5D_t5644  : public Array_t
{
};
// Vuforia.TextRecoAbstractBehaviour[]
// Vuforia.TextRecoAbstractBehaviour[]
struct TextRecoAbstractBehaviourU5BU5D_t5645  : public Array_t
{
};
// Vuforia.IEditorTextRecoBehaviour[]
// Vuforia.IEditorTextRecoBehaviour[]
struct IEditorTextRecoBehaviourU5BU5D_t5646  : public Array_t
{
};
// Vuforia.TurnOffAbstractBehaviour[]
// Vuforia.TurnOffAbstractBehaviour[]
struct TurnOffAbstractBehaviourU5BU5D_t5647  : public Array_t
{
};
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour[]
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour[]
struct UserDefinedTargetBuildingAbstractBehaviourU5BU5D_t5648  : public Array_t
{
};
// Vuforia.VideoBackgroundAbstractBehaviour[]
// Vuforia.VideoBackgroundAbstractBehaviour[]
struct VideoBackgroundAbstractBehaviourU5BU5D_t803  : public Array_t
{
};
// Vuforia.VideoTextureRendererAbstractBehaviour[]
// Vuforia.VideoTextureRendererAbstractBehaviour[]
struct VideoTextureRendererAbstractBehaviourU5BU5D_t5649  : public Array_t
{
};
// Vuforia.VirtualButtonAbstractBehaviour[]
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t773  : public Array_t
{
};
// Vuforia.IEditorVirtualButtonBehaviour[]
// Vuforia.IEditorVirtualButtonBehaviour[]
struct IEditorVirtualButtonBehaviourU5BU5D_t5650  : public Array_t
{
};
// Vuforia.WebCamAbstractBehaviour[]
// Vuforia.WebCamAbstractBehaviour[]
struct WebCamAbstractBehaviourU5BU5D_t5651  : public Array_t
{
};
// Vuforia.WordAbstractBehaviour[]
// Vuforia.WordAbstractBehaviour[]
struct WordAbstractBehaviourU5BU5D_t871  : public Array_t
{
};
// Vuforia.IEditorWordBehaviour[]
// Vuforia.IEditorWordBehaviour[]
struct IEditorWordBehaviourU5BU5D_t5652  : public Array_t
{
};
// Vuforia.FactorySetter[]
// Vuforia.FactorySetter[]
struct FactorySetterU5BU5D_t5653  : public Array_t
{
};
// Vuforia.InternalEyewear/EyewearCalibrationReading[]
// Vuforia.InternalEyewear/EyewearCalibrationReading[]
#pragma pack(push, tp, 1)
struct EyewearCalibrationReadingU5BU5D_t590  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.TrackableBehaviour/Status[]
// Vuforia.TrackableBehaviour/Status[]
struct StatusU5BU5D_t5654  : public Array_t
{
};
// Vuforia.CameraDevice/CameraDeviceMode[]
// Vuforia.CameraDevice/CameraDeviceMode[]
struct CameraDeviceModeU5BU5D_t5655  : public Array_t
{
};
// Vuforia.CameraDevice/FocusMode[]
// Vuforia.CameraDevice/FocusMode[]
struct FocusModeU5BU5D_t5656  : public Array_t
{
};
// Vuforia.CameraDevice/CameraDirection[]
// Vuforia.CameraDevice/CameraDirection[]
struct CameraDirectionU5BU5D_t5657  : public Array_t
{
};
// Vuforia.ICloudRecoEventHandler[]
// Vuforia.ICloudRecoEventHandler[]
struct ICloudRecoEventHandlerU5BU5D_t3862  : public Array_t
{
};
// Vuforia.InternalEyewear/EyeID[]
// Vuforia.InternalEyewear/EyeID[]
struct EyeIDU5BU5D_t5658  : public Array_t
{
};
// Vuforia.DataSet/StorageType[]
// Vuforia.DataSet/StorageType[]
struct StorageTypeU5BU5D_t5659  : public Array_t
{
};
// Vuforia.VirtualButton[]
// Vuforia.VirtualButton[]
struct VirtualButtonU5BU5D_t3925  : public Array_t
{
};
// Vuforia.ImageTargetType[]
// Vuforia.ImageTargetType[]
struct ImageTargetTypeU5BU5D_t5660  : public Array_t
{
};
// Vuforia.ImageTargetBuilder/FrameQuality[]
// Vuforia.ImageTargetBuilder/FrameQuality[]
struct FrameQualityU5BU5D_t5661  : public Array_t
{
};
// Vuforia.Image/PIXEL_FORMAT[]
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3940  : public Array_t
{
};
// Vuforia.Image[]
// Vuforia.Image[]
struct ImageU5BU5D_t3941  : public Array_t
{
};
// Vuforia.Trackable[]
// Vuforia.Trackable[]
struct TrackableU5BU5D_t3973  : public Array_t
{
};
// Vuforia.WordTemplateMode[]
// Vuforia.WordTemplateMode[]
struct WordTemplateModeU5BU5D_t5662  : public Array_t
{
};
// Vuforia.DataSetImpl[]
// Vuforia.DataSetImpl[]
struct DataSetImplU5BU5D_t4016  : public Array_t
{
};
// Vuforia.DataSet[]
// Vuforia.DataSet[]
struct DataSetU5BU5D_t4031  : public Array_t
{
};
// Vuforia.Marker[]
// Vuforia.Marker[]
struct MarkerU5BU5D_t4045  : public Array_t
{
};
// Vuforia.QCARManagerImpl/TrackableResultData[]
// Vuforia.QCARManagerImpl/TrackableResultData[]
#pragma pack(push, tp, 1)
struct TrackableResultDataU5BU5D_t698  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/WordData[]
// Vuforia.QCARManagerImpl/WordData[]
#pragma pack(push, tp, 1)
struct WordDataU5BU5D_t699  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/WordResultData[]
// Vuforia.QCARManagerImpl/WordResultData[]
#pragma pack(push, tp, 1)
struct WordResultDataU5BU5D_t700  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
#pragma pack(push, tp, 1)
struct SmartTerrainRevisionDataU5BU5D_t716  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/SurfaceData[]
// Vuforia.QCARManagerImpl/SurfaceData[]
#pragma pack(push, tp, 1)
struct SurfaceDataU5BU5D_t717  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/PropData[]
// Vuforia.QCARManagerImpl/PropData[]
#pragma pack(push, tp, 1)
struct PropDataU5BU5D_t718  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARRenderer/VideoBackgroundReflection[]
// Vuforia.QCARRenderer/VideoBackgroundReflection[]
struct VideoBackgroundReflectionU5BU5D_t5663  : public Array_t
{
};
// Vuforia.QCARRendererImpl/RenderEvent[]
// Vuforia.QCARRendererImpl/RenderEvent[]
struct RenderEventU5BU5D_t5664  : public Array_t
{
};
// Vuforia.SmartTerrainTrackable[]
// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t4088  : public Array_t
{
};
// Vuforia.TextTrackerImpl/UpDirection[]
// Vuforia.TextTrackerImpl/UpDirection[]
struct UpDirectionU5BU5D_t5665  : public Array_t
{
};
// Vuforia.RectangleData[]
// Vuforia.RectangleData[]
#pragma pack(push, tp, 1)
struct RectangleDataU5BU5D_t730  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.WordPrefabCreationMode[]
// Vuforia.WordPrefabCreationMode[]
struct WordPrefabCreationModeU5BU5D_t5666  : public Array_t
{
};
// Vuforia.WordResult[]
// Vuforia.WordResult[]
struct WordResultU5BU5D_t4135  : public Array_t
{
};
// Vuforia.Word[]
// Vuforia.Word[]
struct WordU5BU5D_t4159  : public Array_t
{
};
// Vuforia.ILoadLevelEventHandler[]
// Vuforia.ILoadLevelEventHandler[]
struct ILoadLevelEventHandlerU5BU5D_t4211  : public Array_t
{
};
// Vuforia.ISmartTerrainEventHandler[]
// Vuforia.ISmartTerrainEventHandler[]
struct ISmartTerrainEventHandlerU5BU5D_t4239  : public Array_t
{
};
// Vuforia.Surface[]
// Vuforia.Surface[]
struct SurfaceU5BU5D_t4253  : public Array_t
{
};
// Vuforia.Prop[]
// Vuforia.Prop[]
struct PropU5BU5D_t4283  : public Array_t
{
};
// Vuforia.QCARManagerImpl/VirtualButtonData[]
// Vuforia.QCARManagerImpl/VirtualButtonData[]
#pragma pack(push, tp, 1)
struct VirtualButtonDataU5BU5D_t4363  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.TargetFinder/InitState[]
// Vuforia.TargetFinder/InitState[]
struct InitStateU5BU5D_t5667  : public Array_t
{
};
// Vuforia.TargetFinder/UpdateState[]
// Vuforia.TargetFinder/UpdateState[]
struct UpdateStateU5BU5D_t5668  : public Array_t
{
};
// Vuforia.TargetFinder/TargetSearchResult[]
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t4394  : public Array_t
{
};
// Vuforia.ImageTarget[]
// Vuforia.ImageTarget[]
struct ImageTargetU5BU5D_t920  : public Array_t
{
};
// Vuforia.ObjectTarget[]
// Vuforia.ObjectTarget[]
struct ObjectTargetU5BU5D_t5669  : public Array_t
{
};
// Vuforia.ExtendedTrackable[]
// Vuforia.ExtendedTrackable[]
struct ExtendedTrackableU5BU5D_t5670  : public Array_t
{
};
// Vuforia.VirtualButton/Sensitivity[]
// Vuforia.VirtualButton/Sensitivity[]
struct SensitivityU5BU5D_t5671  : public Array_t
{
};
// Vuforia.WebCamProfile/ProfileData[]
// Vuforia.WebCamProfile/ProfileData[]
struct ProfileDataU5BU5D_t4438  : public Array_t
{
};
// Vuforia.QCARUnity/InitError[]
// Vuforia.QCARUnity/InitError[]
struct InitErrorU5BU5D_t5672  : public Array_t
{
};
// Vuforia.QCARUnity/QCARHint[]
// Vuforia.QCARUnity/QCARHint[]
struct QCARHintU5BU5D_t5673  : public Array_t
{
};
// Vuforia.QCARUnity/StorageType[]
// Vuforia.QCARUnity/StorageType[]
struct StorageTypeU5BU5D_t5674  : public Array_t
{
};
// Vuforia.ITrackerEventHandler[]
// Vuforia.ITrackerEventHandler[]
struct ITrackerEventHandlerU5BU5D_t4488  : public Array_t
{
};
// Vuforia.QCARAbstractBehaviour/WorldCenterMode[]
// Vuforia.QCARAbstractBehaviour/WorldCenterMode[]
struct WorldCenterModeU5BU5D_t5675  : public Array_t
{
};
// Vuforia.QCARRuntimeUtilities/WebCamUsed[]
// Vuforia.QCARRuntimeUtilities/WebCamUsed[]
struct WebCamUsedU5BU5D_t5676  : public Array_t
{
};
// Vuforia.ITextRecoEventHandler[]
// Vuforia.ITextRecoEventHandler[]
struct ITextRecoEventHandlerU5BU5D_t4522  : public Array_t
{
};
// Vuforia.IUserDefinedTargetEventHandler[]
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t4542  : public Array_t
{
};
// Vuforia.IVirtualButtonEventHandler[]
// Vuforia.IVirtualButtonEventHandler[]
struct IVirtualButtonEventHandlerU5BU5D_t4581  : public Array_t
{
};
// Vuforia.WordFilterMode[]
// Vuforia.WordFilterMode[]
struct WordFilterModeU5BU5D_t5677  : public Array_t
{
};
