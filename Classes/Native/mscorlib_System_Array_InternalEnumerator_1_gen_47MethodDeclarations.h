﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Double>
struct InternalEnumerator_1_t2857;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m14713 (InternalEnumerator_1_t2857 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14714 (InternalEnumerator_1_t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
 void InternalEnumerator_1_Dispose_m14715 (InternalEnumerator_1_t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m14716 (InternalEnumerator_1_t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
 double InternalEnumerator_1_get_Current_m14717 (InternalEnumerator_1_t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
