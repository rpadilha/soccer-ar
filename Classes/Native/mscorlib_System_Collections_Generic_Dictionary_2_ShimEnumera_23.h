﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,Vuforia.WebCamProfile/ProfileData>
struct ShimEnumerator_t4454  : public Object_t
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,Vuforia.WebCamProfile/ProfileData>::host_enumerator
	Enumerator_t4445  ___host_enumerator_0;
};
