﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>
struct List_1_t912;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t30;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>
struct Enumerator_t915 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::l
	List_1_t912 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::current
	VirtualButtonAbstractBehaviour_t30 * ___current_3;
};
