﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SkeletonBone
struct SkeletonBone_t1074;
struct SkeletonBone_t1074_marshaled;

void SkeletonBone_t1074_marshal(const SkeletonBone_t1074& unmarshaled, SkeletonBone_t1074_marshaled& marshaled);
void SkeletonBone_t1074_marshal_back(const SkeletonBone_t1074_marshaled& marshaled, SkeletonBone_t1074& unmarshaled);
void SkeletonBone_t1074_marshal_cleanup(SkeletonBone_t1074_marshaled& marshaled);
