﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
extern TypeInfo BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo;
extern TypeInfo CloudRecoBehaviour_t3_il2cpp_TypeInfo;
extern TypeInfo CylinderTargetBehaviour_t5_il2cpp_TypeInfo;
extern TypeInfo DataSetLoadBehaviour_t7_il2cpp_TypeInfo;
extern TypeInfo DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo;
extern TypeInfo DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo;
extern TypeInfo DefaultTrackableEventHandler_t18_il2cpp_TypeInfo;
extern TypeInfo GLErrorHandler_t19_il2cpp_TypeInfo;
extern TypeInfo HideExcessAreaBehaviour_t20_il2cpp_TypeInfo;
extern TypeInfo ImageTargetBehaviour_t22_il2cpp_TypeInfo;
extern TypeInfo AndroidUnityPlayer_t24_il2cpp_TypeInfo;
extern TypeInfo ComponentFactoryStarterBehaviour_t25_il2cpp_TypeInfo;
extern TypeInfo IOSUnityPlayer_t26_il2cpp_TypeInfo;
extern TypeInfo VuforiaBehaviourComponentFactory_t27_il2cpp_TypeInfo;
extern TypeInfo KeepAliveBehaviour_t37_il2cpp_TypeInfo;
extern TypeInfo MarkerBehaviour_t39_il2cpp_TypeInfo;
extern TypeInfo MaskOutBehaviour_t40_il2cpp_TypeInfo;
extern TypeInfo MultiTargetBehaviour_t41_il2cpp_TypeInfo;
extern TypeInfo ObjectTargetBehaviour_t42_il2cpp_TypeInfo;
extern TypeInfo PropBehaviour_t12_il2cpp_TypeInfo;
extern TypeInfo QCARBehaviour_t44_il2cpp_TypeInfo;
extern TypeInfo ReconstructionBehaviour_t11_il2cpp_TypeInfo;
extern TypeInfo ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo;
extern TypeInfo SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo;
extern TypeInfo SurfaceBehaviour_t13_il2cpp_TypeInfo;
extern TypeInfo TextRecoBehaviour_t52_il2cpp_TypeInfo;
extern TypeInfo TurnOffBehaviour_t53_il2cpp_TypeInfo;
extern TypeInfo TurnOffWordBehaviour_t54_il2cpp_TypeInfo;
extern TypeInfo UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo;
extern TypeInfo VideoBackgroundBehaviour_t57_il2cpp_TypeInfo;
extern TypeInfo VideoTextureRenderer_t59_il2cpp_TypeInfo;
extern TypeInfo VirtualButtonBehaviour_t61_il2cpp_TypeInfo;
extern TypeInfo WebCamBehaviour_t62_il2cpp_TypeInfo;
extern TypeInfo WireframeBehaviour_t65_il2cpp_TypeInfo;
extern TypeInfo WireframeTrackableEventHandler_t67_il2cpp_TypeInfo;
extern TypeInfo WordBehaviour_t68_il2cpp_TypeInfo;
extern TypeInfo Area_Script_t69_il2cpp_TypeInfo;
extern TypeInfo Banda_Script_t72_il2cpp_TypeInfo;
extern TypeInfo Camera_Script_t75_il2cpp_TypeInfo;
extern TypeInfo Corner_Script_t76_il2cpp_TypeInfo;
extern TypeInfo GoalKeeperJump_t78_il2cpp_TypeInfo;
extern TypeInfo GoalKeeperJump_Down_t79_il2cpp_TypeInfo;
extern TypeInfo GoalKeeper_State_t80_il2cpp_TypeInfo;
extern TypeInfo GoalKeeper_Script_t77_il2cpp_TypeInfo;
extern TypeInfo Goal_Script_t86_il2cpp_TypeInfo;
extern TypeInfo ChooseTeam_t88_il2cpp_TypeInfo;
extern TypeInfo ScoreHUD_t89_il2cpp_TypeInfo;
extern TypeInfo ScorerTimeHUD_t90_il2cpp_TypeInfo;
extern TypeInfo SetShield_t91_il2cpp_TypeInfo;
extern TypeInfo ShieldMenu_t92_il2cpp_TypeInfo;
extern TypeInfo InGameState_t93_il2cpp_TypeInfo;
extern TypeInfo InGameState_Script_t83_il2cpp_TypeInfo;
extern TypeInfo Boundary_t98_il2cpp_TypeInfo;
extern TypeInfo Joystick_Script_t102_il2cpp_TypeInfo;
extern TypeInfo Pass_Script_t104_il2cpp_TypeInfo;
extern TypeInfo TypePlayer_t105_il2cpp_TypeInfo;
extern TypeInfo Player_State_t106_il2cpp_TypeInfo;
extern TypeInfo Player_Script_t94_il2cpp_TypeInfo;
extern TypeInfo Shoot_Script_t109_il2cpp_TypeInfo;
extern TypeInfo Sombra_Script_t110_il2cpp_TypeInfo;
extern TypeInfo Sphere_t71_il2cpp_TypeInfo;
#include "utils/RegisterRuntimeInitializeAndCleanup.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_AssemblyU2DCSharp_Assembly_Types[63] = 
{
	&U3CModuleU3E_t0_il2cpp_TypeInfo,
	&BackgroundPlaneBehaviour_t1_il2cpp_TypeInfo,
	&CloudRecoBehaviour_t3_il2cpp_TypeInfo,
	&CylinderTargetBehaviour_t5_il2cpp_TypeInfo,
	&DataSetLoadBehaviour_t7_il2cpp_TypeInfo,
	&DefaultInitializationErrorHandler_t9_il2cpp_TypeInfo,
	&DefaultSmartTerrainEventHandler_t14_il2cpp_TypeInfo,
	&DefaultTrackableEventHandler_t18_il2cpp_TypeInfo,
	&GLErrorHandler_t19_il2cpp_TypeInfo,
	&HideExcessAreaBehaviour_t20_il2cpp_TypeInfo,
	&ImageTargetBehaviour_t22_il2cpp_TypeInfo,
	&AndroidUnityPlayer_t24_il2cpp_TypeInfo,
	&ComponentFactoryStarterBehaviour_t25_il2cpp_TypeInfo,
	&IOSUnityPlayer_t26_il2cpp_TypeInfo,
	&VuforiaBehaviourComponentFactory_t27_il2cpp_TypeInfo,
	&KeepAliveBehaviour_t37_il2cpp_TypeInfo,
	&MarkerBehaviour_t39_il2cpp_TypeInfo,
	&MaskOutBehaviour_t40_il2cpp_TypeInfo,
	&MultiTargetBehaviour_t41_il2cpp_TypeInfo,
	&ObjectTargetBehaviour_t42_il2cpp_TypeInfo,
	&PropBehaviour_t12_il2cpp_TypeInfo,
	&QCARBehaviour_t44_il2cpp_TypeInfo,
	&ReconstructionBehaviour_t11_il2cpp_TypeInfo,
	&ReconstructionFromTargetBehaviour_t47_il2cpp_TypeInfo,
	&SmartTerrainTrackerBehaviour_t49_il2cpp_TypeInfo,
	&SurfaceBehaviour_t13_il2cpp_TypeInfo,
	&TextRecoBehaviour_t52_il2cpp_TypeInfo,
	&TurnOffBehaviour_t53_il2cpp_TypeInfo,
	&TurnOffWordBehaviour_t54_il2cpp_TypeInfo,
	&UserDefinedTargetBuildingBehaviour_t55_il2cpp_TypeInfo,
	&VideoBackgroundBehaviour_t57_il2cpp_TypeInfo,
	&VideoTextureRenderer_t59_il2cpp_TypeInfo,
	&VirtualButtonBehaviour_t61_il2cpp_TypeInfo,
	&WebCamBehaviour_t62_il2cpp_TypeInfo,
	&WireframeBehaviour_t65_il2cpp_TypeInfo,
	&WireframeTrackableEventHandler_t67_il2cpp_TypeInfo,
	&WordBehaviour_t68_il2cpp_TypeInfo,
	&Area_Script_t69_il2cpp_TypeInfo,
	&Banda_Script_t72_il2cpp_TypeInfo,
	&Camera_Script_t75_il2cpp_TypeInfo,
	&Corner_Script_t76_il2cpp_TypeInfo,
	&GoalKeeperJump_t78_il2cpp_TypeInfo,
	&GoalKeeperJump_Down_t79_il2cpp_TypeInfo,
	&GoalKeeper_State_t80_il2cpp_TypeInfo,
	&GoalKeeper_Script_t77_il2cpp_TypeInfo,
	&Goal_Script_t86_il2cpp_TypeInfo,
	&ChooseTeam_t88_il2cpp_TypeInfo,
	&ScoreHUD_t89_il2cpp_TypeInfo,
	&ScorerTimeHUD_t90_il2cpp_TypeInfo,
	&SetShield_t91_il2cpp_TypeInfo,
	&ShieldMenu_t92_il2cpp_TypeInfo,
	&InGameState_t93_il2cpp_TypeInfo,
	&InGameState_Script_t83_il2cpp_TypeInfo,
	&Boundary_t98_il2cpp_TypeInfo,
	&Joystick_Script_t102_il2cpp_TypeInfo,
	&Pass_Script_t104_il2cpp_TypeInfo,
	&TypePlayer_t105_il2cpp_TypeInfo,
	&Player_State_t106_il2cpp_TypeInfo,
	&Player_Script_t94_il2cpp_TypeInfo,
	&Shoot_Script_t109_il2cpp_TypeInfo,
	&Sombra_Script_t110_il2cpp_TypeInfo,
	&Sphere_t71_il2cpp_TypeInfo,
	NULL,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern CustomAttributesCache g_AssemblyU2DCSharp_Assembly__CustomAttributeCache;
Il2CppAssembly g_AssemblyU2DCSharp_Assembly = 
{
	{ "Assembly-CSharp", 0, 0, 0, { 0 }, 32772, 0, 0, 0, 0, 0, 0 },
	&g_AssemblyU2DCSharp_dll_Image,
	&g_AssemblyU2DCSharp_Assembly__CustomAttributeCache,
};
Il2CppImage g_AssemblyU2DCSharp_dll_Image = 
{
	 "Assembly-CSharp.dll" ,
	&g_AssemblyU2DCSharp_Assembly,
	g_AssemblyU2DCSharp_Assembly_Types,
	62,
	NULL,
};
static void s_AssemblyU2DCSharpRegistration()
{
	RegisterAssembly (&g_AssemblyU2DCSharp_Assembly);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_AssemblyU2DCSharpRegistrationVariable(&s_AssemblyU2DCSharpRegistration, NULL);
