﻿#pragma once
#include <stdint.h>
// Vuforia.IEyewearComponentFactory
struct IEyewearComponentFactory_t585;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.EyewearComponentFactory
struct EyewearComponentFactory_t586  : public Object_t
{
};
struct EyewearComponentFactory_t586_StaticFields{
	// Vuforia.IEyewearComponentFactory Vuforia.EyewearComponentFactory::sInstance
	Object_t * ___sInstance_0;
};
