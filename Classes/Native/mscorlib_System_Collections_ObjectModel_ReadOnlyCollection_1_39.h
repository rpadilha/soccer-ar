﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.ISmartTerrainEventHandler>
struct IList_1_t4247;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ISmartTerrainEventHandler>
struct ReadOnlyCollection_1_t4243  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ISmartTerrainEventHandler>::list
	Object_t* ___list_0;
};
