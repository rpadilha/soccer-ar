﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>
struct UnityAction_1_t2947;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>
struct InvokableCall_1_t2946  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::Delegate
	UnityAction_1_t2947 * ___Delegate_0;
};
