﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t58;

// System.Boolean Vuforia.VideoBackgroundAbstractBehaviour::get_VideoBackGroundMirrored()
 bool VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
 void VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369 (VideoBackgroundAbstractBehaviour_t58 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
 void VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4370 (VideoBackgroundAbstractBehaviour_t58 * __this, bool ___disable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
 void VideoBackgroundAbstractBehaviour_SetStereoDepth_m4371 (VideoBackgroundAbstractBehaviour_t58 * __this, float ___depth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
 void VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4372 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
 void VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4373 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
 void VideoBackgroundAbstractBehaviour_Awake_m4374 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
 void VideoBackgroundAbstractBehaviour_OnPreRender_m4375 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
 void VideoBackgroundAbstractBehaviour_OnPostRender_m4376 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
 void VideoBackgroundAbstractBehaviour_OnDestroy_m4377 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
 void VideoBackgroundAbstractBehaviour__ctor_m491 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
