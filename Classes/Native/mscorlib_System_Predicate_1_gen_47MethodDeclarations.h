﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>
struct Predicate_1_t4507;
// System.Object
struct Object_t;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t112;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m27229(__this, ___object, ___method, method) (void)Predicate_1__ctor_m14668_gshared((Predicate_1_t2832 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Boolean System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>::Invoke(T)
#define Predicate_1_Invoke_m27230(__this, ___obj, method) (bool)Predicate_1_Invoke_m14669_gshared((Predicate_1_t2832 *)__this, (Object_t *)___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m27231(__this, ___obj, ___callback, ___object, method) (Object_t *)Predicate_1_BeginInvoke_m14670_gshared((Predicate_1_t2832 *)__this, (Object_t *)___obj, (AsyncCallback_t251 *)___callback, (Object_t *)___object, method)
// System.Boolean System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m27232(__this, ___result, method) (bool)Predicate_1_EndInvoke_m14671_gshared((Predicate_1_t2832 *)__this, (Object_t *)___result, method)
