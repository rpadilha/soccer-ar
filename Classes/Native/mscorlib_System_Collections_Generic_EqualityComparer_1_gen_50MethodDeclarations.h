﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct EqualityComparer_1_t4361;
// System.Object
struct Object_t;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::.ctor()
 void EqualityComparer_1__ctor_m25901 (EqualityComparer_1_t4361 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::.cctor()
 void EqualityComparer_1__cctor_m25902 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
 int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25903 (EqualityComparer_1_t4361 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
 bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25904 (EqualityComparer_1_t4361 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::get_Default()
 EqualityComparer_1_t4361 * EqualityComparer_1_get_Default_m25905 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
