﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.PointerInputModule>
struct UnityAction_1_t3323;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.PointerInputModule>
struct InvokableCall_1_t3322  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.PointerInputModule>::Delegate
	UnityAction_1_t3323 * ___Delegate_0;
};
