﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.HorizontalLayoutGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_100.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.HorizontalLayoutGroup>
struct CachedInvokableCall_1_t3769  : public InvokableCall_1_t3770
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.HorizontalLayoutGroup>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
