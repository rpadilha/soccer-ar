﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>
struct GenericEqualityComparer_1_t4131;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::.ctor()
 void GenericEqualityComparer_1__ctor_m23634 (GenericEqualityComparer_1_t4131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::GetHashCode(T)
 int32_t GenericEqualityComparer_1_GetHashCode_m23635 (GenericEqualityComparer_1_t4131 * __this, uint16_t ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::Equals(T,T)
 bool GenericEqualityComparer_1_Equals_m23636 (GenericEqualityComparer_1_t4131 * __this, uint16_t ___x, uint16_t ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
