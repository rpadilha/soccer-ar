﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.RuntimeMethodHandle
struct RuntimeMethodHandle_t1956 
{
	// System.IntPtr System.RuntimeMethodHandle::value
	IntPtr_t121 ___value_0;
};
