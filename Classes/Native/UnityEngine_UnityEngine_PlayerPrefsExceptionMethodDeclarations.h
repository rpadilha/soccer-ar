﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.PlayerPrefsException
struct PlayerPrefsException_t1049;
// System.String
struct String_t;

// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
 void PlayerPrefsException__ctor_m6230 (PlayerPrefsException_t1049 * __this, String_t* ___error, MethodInfo* method) IL2CPP_METHOD_ATTR;
