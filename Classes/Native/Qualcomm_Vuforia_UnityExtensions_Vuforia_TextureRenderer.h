﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t168;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.TextureRenderer
struct TextureRenderer_t785  : public Object_t
{
	// UnityEngine.Camera Vuforia.TextureRenderer::mTextureBufferCamera
	Camera_t168 * ___mTextureBufferCamera_0;
	// System.Int32 Vuforia.TextureRenderer::mTextureWidth
	int32_t ___mTextureWidth_1;
	// System.Int32 Vuforia.TextureRenderer::mTextureHeight
	int32_t ___mTextureHeight_2;
};
