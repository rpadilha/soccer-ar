﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct CriticalFinalizerObject_t2016;

// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::.ctor()
 void CriticalFinalizerObject__ctor_m11579 (CriticalFinalizerObject_t2016 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::Finalize()
 void CriticalFinalizerObject_Finalize_m11580 (CriticalFinalizerObject_t2016 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
