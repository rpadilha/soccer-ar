﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.EventSystem>
struct UnityAction_1_t3176;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>
struct InvokableCall_1_t3175  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>::Delegate
	UnityAction_1_t3176 * ___Delegate_0;
};
