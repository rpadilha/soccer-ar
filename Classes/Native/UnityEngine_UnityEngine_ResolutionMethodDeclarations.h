﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Resolution
struct Resolution_t986;

// System.Int32 UnityEngine.Resolution::get_width()
 int32_t Resolution_get_width_m5689 (Resolution_t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Resolution::set_width(System.Int32)
 void Resolution_set_width_m5690 (Resolution_t986 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_height()
 int32_t Resolution_get_height_m5691 (Resolution_t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Resolution::set_height(System.Int32)
 void Resolution_set_height_m5692 (Resolution_t986 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_refreshRate()
 int32_t Resolution_get_refreshRate_m5693 (Resolution_t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Resolution::set_refreshRate(System.Int32)
 void Resolution_set_refreshRate_m5694 (Resolution_t986 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
