﻿#pragma once
#include <stdint.h>
// UnityEngine.BoxCollider
struct BoxCollider_t197;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.BoxCollider>
struct CastHelper_1_t3063 
{
	// T UnityEngine.CastHelper`1<UnityEngine.BoxCollider>::t
	BoxCollider_t197 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.BoxCollider>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
