﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Canvas>
struct EqualityComparer_1_t3535;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Canvas>
struct EqualityComparer_1_t3535  : public Object_t
{
};
struct EqualityComparer_1_t3535_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Canvas>::_default
	EqualityComparer_1_t3535 * ____default_0;
};
