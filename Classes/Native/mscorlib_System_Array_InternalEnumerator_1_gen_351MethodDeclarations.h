﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.WordPrefabCreationMode>
struct InternalEnumerator_1_t4134;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.WordPrefabCreationMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.WordPrefabCreationMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m23645 (InternalEnumerator_1_t4134 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WordPrefabCreationMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23646 (InternalEnumerator_1_t4134 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WordPrefabCreationMode>::Dispose()
 void InternalEnumerator_1_Dispose_m23647 (InternalEnumerator_1_t4134 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WordPrefabCreationMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m23648 (InternalEnumerator_1_t4134 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.WordPrefabCreationMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m23649 (InternalEnumerator_1_t4134 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
