﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$64
struct $ArrayType$64_t1705;
struct $ArrayType$64_t1705_marshaled;

void $ArrayType$64_t1705_marshal(const $ArrayType$64_t1705& unmarshaled, $ArrayType$64_t1705_marshaled& marshaled);
void $ArrayType$64_t1705_marshal_back(const $ArrayType$64_t1705_marshaled& marshaled, $ArrayType$64_t1705& unmarshaled);
void $ArrayType$64_t1705_marshal_cleanup($ArrayType$64_t1705_marshaled& marshaled);
