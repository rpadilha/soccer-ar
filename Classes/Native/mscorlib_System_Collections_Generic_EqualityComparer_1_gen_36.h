﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.Marker>
struct EqualityComparer_1_t4060;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.Marker>
struct EqualityComparer_1_t4060  : public Object_t
{
};
struct EqualityComparer_1_t4060_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.Marker>::_default
	EqualityComparer_1_t4060 * ____default_0;
};
