﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_21.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>
struct CachedInvokableCall_1_t2934  : public InvokableCall_1_t2935
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
