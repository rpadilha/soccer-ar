﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t4564;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t499;
// System.Object[]
struct ObjectU5BU5D_t130;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3402;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"

// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
 void HashSet_1__ctor_m27579_gshared (HashSet_1_t4564 * __this, MethodInfo* method);
#define HashSet_1__ctor_m27579(__this, method) (void)HashSet_1__ctor_m27579_gshared((HashSet_1_t4564 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void HashSet_1__ctor_m27581_gshared (HashSet_1_t4564 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method);
#define HashSet_1__ctor_m27581(__this, ___info, ___context, method) (void)HashSet_1__ctor_m27581_gshared((HashSet_1_t4564 *)__this, (SerializationInfo_t1118 *)___info, (StreamingContext_t1119 )___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
 Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27583_gshared (HashSet_1_t4564 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27583(__this, method) (Object_t*)HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27583_gshared((HashSet_1_t4564 *)__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
 bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27585_gshared (HashSet_1_t4564 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27585(__this, method) (bool)HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27585_gshared((HashSet_1_t4564 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
 void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m27587_gshared (HashSet_1_t4564 * __this, ObjectU5BU5D_t130* ___array, int32_t ___index, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m27587(__this, ___array, ___index, method) (void)HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m27587_gshared((HashSet_1_t4564 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
 void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27589_gshared (HashSet_1_t4564 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27589(__this, ___item, method) (void)HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m27589_gshared((HashSet_1_t4564 *)__this, (Object_t *)___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m27591_gshared (HashSet_1_t4564 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m27591(__this, method) (Object_t *)HashSet_1_System_Collections_IEnumerable_GetEnumerator_m27591_gshared((HashSet_1_t4564 *)__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
 int32_t HashSet_1_get_Count_m27593_gshared (HashSet_1_t4564 * __this, MethodInfo* method);
#define HashSet_1_get_Count_m27593(__this, method) (int32_t)HashSet_1_get_Count_m27593_gshared((HashSet_1_t4564 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
 void HashSet_1_Init_m27595_gshared (HashSet_1_t4564 * __this, int32_t ___capacity, Object_t* ___comparer, MethodInfo* method);
#define HashSet_1_Init_m27595(__this, ___capacity, ___comparer, method) (void)HashSet_1_Init_m27595_gshared((HashSet_1_t4564 *)__this, (int32_t)___capacity, (Object_t*)___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::InitArrays(System.Int32)
 void HashSet_1_InitArrays_m27597_gshared (HashSet_1_t4564 * __this, int32_t ___size, MethodInfo* method);
#define HashSet_1_InitArrays_m27597(__this, ___size, method) (void)HashSet_1_InitArrays_m27597_gshared((HashSet_1_t4564 *)__this, (int32_t)___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::SlotsContainsAt(System.Int32,System.Int32,T)
 bool HashSet_1_SlotsContainsAt_m27599_gshared (HashSet_1_t4564 * __this, int32_t ___index, int32_t ___hash, Object_t * ___item, MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m27599(__this, ___index, ___hash, ___item, method) (bool)HashSet_1_SlotsContainsAt_m27599_gshared((HashSet_1_t4564 *)__this, (int32_t)___index, (int32_t)___hash, (Object_t *)___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32)
 void HashSet_1_CopyTo_m27601_gshared (HashSet_1_t4564 * __this, ObjectU5BU5D_t130* ___array, int32_t ___index, MethodInfo* method);
#define HashSet_1_CopyTo_m27601(__this, ___array, ___index, method) (void)HashSet_1_CopyTo_m27601_gshared((HashSet_1_t4564 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32,System.Int32)
 void HashSet_1_CopyTo_m27603_gshared (HashSet_1_t4564 * __this, ObjectU5BU5D_t130* ___array, int32_t ___index, int32_t ___count, MethodInfo* method);
#define HashSet_1_CopyTo_m27603(__this, ___array, ___index, ___count, method) (void)HashSet_1_CopyTo_m27603_gshared((HashSet_1_t4564 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, (int32_t)___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Resize()
 void HashSet_1_Resize_m27605_gshared (HashSet_1_t4564 * __this, MethodInfo* method);
#define HashSet_1_Resize_m27605(__this, method) (void)HashSet_1_Resize_m27605_gshared((HashSet_1_t4564 *)__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetLinkHashCode(System.Int32)
 int32_t HashSet_1_GetLinkHashCode_m27607_gshared (HashSet_1_t4564 * __this, int32_t ___index, MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m27607(__this, ___index, method) (int32_t)HashSet_1_GetLinkHashCode_m27607_gshared((HashSet_1_t4564 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetItemHashCode(T)
 int32_t HashSet_1_GetItemHashCode_m27609_gshared (HashSet_1_t4564 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_GetItemHashCode_m27609(__this, ___item, method) (int32_t)HashSet_1_GetItemHashCode_m27609_gshared((HashSet_1_t4564 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
 bool HashSet_1_Add_m27610_gshared (HashSet_1_t4564 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_Add_m27610(__this, ___item, method) (bool)HashSet_1_Add_m27610_gshared((HashSet_1_t4564 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
 void HashSet_1_Clear_m27611_gshared (HashSet_1_t4564 * __this, MethodInfo* method);
#define HashSet_1_Clear_m27611(__this, method) (void)HashSet_1_Clear_m27611_gshared((HashSet_1_t4564 *)__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(T)
 bool HashSet_1_Contains_m27612_gshared (HashSet_1_t4564 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_Contains_m27612(__this, ___item, method) (bool)HashSet_1_Contains_m27612_gshared((HashSet_1_t4564 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
 bool HashSet_1_Remove_m27614_gshared (HashSet_1_t4564 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_Remove_m27614(__this, ___item, method) (bool)HashSet_1_Remove_m27614_gshared((HashSet_1_t4564 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void HashSet_1_GetObjectData_m27616_gshared (HashSet_1_t4564 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method);
#define HashSet_1_GetObjectData_m27616(__this, ___info, ___context, method) (void)HashSet_1_GetObjectData_m27616_gshared((HashSet_1_t4564 *)__this, (SerializationInfo_t1118 *)___info, (StreamingContext_t1119 )___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::OnDeserialization(System.Object)
 void HashSet_1_OnDeserialization_m27618_gshared (HashSet_1_t4564 * __this, Object_t * ___sender, MethodInfo* method);
#define HashSet_1_OnDeserialization_m27618(__this, ___sender, method) (void)HashSet_1_OnDeserialization_m27618_gshared((HashSet_1_t4564 *)__this, (Object_t *)___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
 Enumerator_t4567  HashSet_1_GetEnumerator_m27619 (HashSet_1_t4564 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
