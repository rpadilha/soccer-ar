﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.ImageTarget>
struct EqualityComparer_1_t4423;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.ImageTarget>
struct EqualityComparer_1_t4423  : public Object_t
{
};
struct EqualityComparer_1_t4423_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.ImageTarget>::_default
	EqualityComparer_1_t4423 * ____default_0;
};
