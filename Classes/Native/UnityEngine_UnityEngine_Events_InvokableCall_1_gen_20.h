﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>
struct UnityAction_1_t2930;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
struct InvokableCall_1_t2929  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Delegate
	UnityAction_1_t2930 * ___Delegate_0;
};
