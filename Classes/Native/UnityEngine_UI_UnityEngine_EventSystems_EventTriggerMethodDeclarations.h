﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t246;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t235;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t239;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t247;
// UnityEngine.EventSystems.EventTriggerType
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTriggerType.h"

// System.Void UnityEngine.EventSystems.EventTrigger::.ctor()
 void EventTrigger__ctor_m892 (EventTrigger_t246 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::Execute(UnityEngine.EventSystems.EventTriggerType,UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_Execute_m893 (EventTrigger_t246 * __this, int32_t ___id, BaseEventData_t235 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnPointerEnter_m894 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnPointerExit_m895 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnDrag(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnDrag_m896 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnDrop(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnDrop_m897 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnPointerDown_m898 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnPointerUp_m899 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnPointerClick_m900 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnSelect(UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_OnSelect_m901 (EventTrigger_t246 * __this, BaseEventData_t235 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnDeselect(UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_OnDeselect_m902 (EventTrigger_t246 * __this, BaseEventData_t235 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnScroll(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnScroll_m903 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnMove(UnityEngine.EventSystems.AxisEventData)
 void EventTrigger_OnMove_m904 (EventTrigger_t246 * __this, AxisEventData_t247 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnUpdateSelected(UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_OnUpdateSelected_m905 (EventTrigger_t246 * __this, BaseEventData_t235 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnInitializePotentialDrag_m906 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnBeginDrag_m907 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnEndDrag_m908 (EventTrigger_t246 * __this, PointerEventData_t239 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnSubmit(UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_OnSubmit_m909 (EventTrigger_t246 * __this, BaseEventData_t235 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnCancel(UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_OnCancel_m910 (EventTrigger_t246 * __this, BaseEventData_t235 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
