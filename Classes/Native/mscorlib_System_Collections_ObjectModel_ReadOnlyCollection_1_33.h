﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>
struct IList_1_t4106;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>
struct ReadOnlyCollection_1_t4103  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::list
	Object_t* ___list_0;
};
