﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo UIVertex_t362_il2cpp_TypeInfo;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t3514  : public MulticastDelegate_t373
{
};
