﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>
struct IList_1_t3841;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<Vuforia.ITrackableEventHandler>
struct Collection_1_t3842  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.ITrackableEventHandler>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.ITrackableEventHandler>::syncRoot
	Object_t * ___syncRoot_1;
};
