﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>
struct InternalEnumerator_1_t3856;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21083 (InternalEnumerator_1_t3856 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21084 (InternalEnumerator_1_t3856 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>::Dispose()
 void InternalEnumerator_1_Dispose_m21085 (InternalEnumerator_1_t3856 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21086 (InternalEnumerator_1_t3856 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m21087 (InternalEnumerator_1_t3856 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
