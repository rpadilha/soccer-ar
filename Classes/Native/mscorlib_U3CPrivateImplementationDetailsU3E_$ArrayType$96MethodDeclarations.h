﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$96
struct $ArrayType$96_t2328;
struct $ArrayType$96_t2328_marshaled;

void $ArrayType$96_t2328_marshal(const $ArrayType$96_t2328& unmarshaled, $ArrayType$96_t2328_marshaled& marshaled);
void $ArrayType$96_t2328_marshal_back(const $ArrayType$96_t2328_marshaled& marshaled, $ArrayType$96_t2328& unmarshaled);
void $ArrayType$96_t2328_marshal_cleanup($ArrayType$96_t2328_marshaled& marshaled);
