﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamTexAdaptorImpl
struct WebCamTexAdaptorImpl_t729;
// UnityEngine.Texture
struct Texture_t107;
// System.String
struct String_t;
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"

// System.Boolean Vuforia.WebCamTexAdaptorImpl::get_DidUpdateThisFrame()
 bool WebCamTexAdaptorImpl_get_DidUpdateThisFrame_m3248 (WebCamTexAdaptorImpl_t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamTexAdaptorImpl::get_IsPlaying()
 bool WebCamTexAdaptorImpl_get_IsPlaying_m3249 (WebCamTexAdaptorImpl_t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture Vuforia.WebCamTexAdaptorImpl::get_Texture()
 Texture_t107 * WebCamTexAdaptorImpl_get_Texture_m3250 (WebCamTexAdaptorImpl_t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::.ctor(System.String,System.Int32,Vuforia.QCARRenderer/Vec2I)
 void WebCamTexAdaptorImpl__ctor_m3251 (WebCamTexAdaptorImpl_t729 * __this, String_t* ___deviceName, int32_t ___requestedFPS, Vec2I_t675  ___requestedTextureSize, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::Play()
 void WebCamTexAdaptorImpl_Play_m3252 (WebCamTexAdaptorImpl_t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::Stop()
 void WebCamTexAdaptorImpl_Stop_m3253 (WebCamTexAdaptorImpl_t729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
