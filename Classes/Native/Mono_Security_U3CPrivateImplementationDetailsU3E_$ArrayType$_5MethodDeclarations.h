﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct $ArrayType$12_t1706;
struct $ArrayType$12_t1706_marshaled;

void $ArrayType$12_t1706_marshal(const $ArrayType$12_t1706& unmarshaled, $ArrayType$12_t1706_marshaled& marshaled);
void $ArrayType$12_t1706_marshal_back(const $ArrayType$12_t1706_marshaled& marshaled, $ArrayType$12_t1706& unmarshaled);
void $ArrayType$12_t1706_marshal_cleanup($ArrayType$12_t1706_marshaled& marshaled);
