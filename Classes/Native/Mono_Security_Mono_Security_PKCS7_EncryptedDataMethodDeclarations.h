﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.PKCS7/EncryptedData
struct EncryptedData_t1604;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t1603;
// System.Byte[]
struct ByteU5BU5D_t653;
// Mono.Security.ASN1
struct ASN1_t1418;

// System.Void Mono.Security.PKCS7/EncryptedData::.ctor()
 void EncryptedData__ctor_m8242 (EncryptedData_t1604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EncryptedData::.ctor(Mono.Security.ASN1)
 void EncryptedData__ctor_m8243 (EncryptedData_t1604 * __this, ASN1_t1418 * ___asn1, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EncryptedData::get_EncryptionAlgorithm()
 ContentInfo_t1603 * EncryptedData_get_EncryptionAlgorithm_m8244 (EncryptedData_t1604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/EncryptedData::get_EncryptedContent()
 ByteU5BU5D_t653* EncryptedData_get_EncryptedContent_m8245 (EncryptedData_t1604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
