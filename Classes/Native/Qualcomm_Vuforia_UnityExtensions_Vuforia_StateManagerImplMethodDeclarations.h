﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.StateManagerImpl
struct StateManagerImpl_t703;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
struct IEnumerable_1_t769;
// Vuforia.WordManager
struct WordManager_t733;
// Vuforia.Trackable
struct Trackable_t594;
// Vuforia.DataSet
struct DataSet_t612;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t17;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t23;
// Vuforia.ImageTarget
struct ImageTarget_t616;
// UnityEngine.GameObject
struct GameObject_t29;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t32;
// Vuforia.Marker
struct Marker_t667;
// System.String
struct String_t;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t701;
// UnityEngine.Transform
struct Transform_t74;
// Vuforia.QCARManagerImpl/TrackableResultData[]
struct TrackableResultDataU5BU5D_t698;
// Vuforia.QCARManagerImpl/WordData[]
struct WordDataU5BU5D_t699;
// Vuforia.QCARManagerImpl/WordResultData[]
struct WordResultDataU5BU5D_t700;
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t773;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t33;
// Vuforia.MultiTarget
struct MultiTarget_t617;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t6;
// Vuforia.CylinderTarget
struct CylinderTarget_t615;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t36;
// Vuforia.ObjectTarget
struct ObjectTarget_t598;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pos.h"

// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManagerImpl::GetActiveTrackableBehaviours()
 Object_t* StateManagerImpl_GetActiveTrackableBehaviours_m4150 (StateManagerImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManagerImpl::GetTrackableBehaviours()
 Object_t* StateManagerImpl_GetTrackableBehaviours_m4151 (StateManagerImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordManager Vuforia.StateManagerImpl::GetWordManager()
 WordManager_t733 * StateManagerImpl_GetWordManager_m4152 (StateManagerImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean)
 void StateManagerImpl_DestroyTrackableBehavioursForTrackable_m4153 (StateManagerImpl_t703 * __this, Object_t * ___trackable, bool ___destroyGameObjects, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::AssociateMarkerBehaviours()
 void StateManagerImpl_AssociateMarkerBehaviours_m4154 (StateManagerImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::AssociateTrackableBehavioursForDataSet(Vuforia.DataSet)
 void StateManagerImpl_AssociateTrackableBehavioursForDataSet_m4155 (StateManagerImpl_t703 * __this, DataSet_t612 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::RegisterExternallyManagedTrackableBehaviour(Vuforia.TrackableBehaviour)
 void StateManagerImpl_RegisterExternallyManagedTrackableBehaviour_m4156 (StateManagerImpl_t703 * __this, TrackableBehaviour_t17 * ___trackableBehaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UnregisterExternallyManagedTrackableBehaviour(System.Int32)
 void StateManagerImpl_UnregisterExternallyManagedTrackableBehaviour_m4157 (StateManagerImpl_t703 * __this, int32_t ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::RemoveDestroyedTrackables()
 void StateManagerImpl_RemoveDestroyedTrackables_m4158 (StateManagerImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::ClearTrackableBehaviours()
 void StateManagerImpl_ClearTrackableBehaviours_m4159 (StateManagerImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.StateManagerImpl::FindOrCreateImageTargetBehaviourForTrackable(Vuforia.ImageTarget,UnityEngine.GameObject)
 ImageTargetAbstractBehaviour_t23 * StateManagerImpl_FindOrCreateImageTargetBehaviourForTrackable_m4160 (StateManagerImpl_t703 * __this, Object_t * ___trackable, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.StateManagerImpl::FindOrCreateImageTargetBehaviourForTrackable(Vuforia.ImageTarget,UnityEngine.GameObject,Vuforia.DataSet)
 ImageTargetAbstractBehaviour_t23 * StateManagerImpl_FindOrCreateImageTargetBehaviourForTrackable_m4161 (StateManagerImpl_t703 * __this, Object_t * ___trackable, GameObject_t29 * ___gameObject, DataSet_t612 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.StateManagerImpl::CreateNewMarkerBehaviourForMarker(Vuforia.Marker,System.String)
 MarkerAbstractBehaviour_t32 * StateManagerImpl_CreateNewMarkerBehaviourForMarker_m4162 (StateManagerImpl_t703 * __this, Object_t * ___trackable, String_t* ___gameObjectName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.StateManagerImpl::CreateNewMarkerBehaviourForMarker(Vuforia.Marker,UnityEngine.GameObject)
 MarkerAbstractBehaviour_t32 * StateManagerImpl_CreateNewMarkerBehaviourForMarker_m4163 (StateManagerImpl_t703 * __this, Object_t * ___trackable, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::SetTrackableBehavioursForTrackableToNotFound(Vuforia.Trackable)
 void StateManagerImpl_SetTrackableBehavioursForTrackableToNotFound_m4164 (StateManagerImpl_t703 * __this, Object_t * ___trackable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::EnableTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean)
 void StateManagerImpl_EnableTrackableBehavioursForTrackable_m4165 (StateManagerImpl_t703 * __this, Object_t * ___trackable, bool ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::RemoveDisabledTrackablesFromQueue(System.Collections.Generic.LinkedList`1<System.Int32>&)
 void StateManagerImpl_RemoveDisabledTrackablesFromQueue_m4166 (StateManagerImpl_t703 * __this, LinkedList_1_t701 ** ___trackableIDs, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateCameraPose(UnityEngine.Transform,Vuforia.QCARManagerImpl/TrackableResultData[],System.Int32)
 void StateManagerImpl_UpdateCameraPose_m4167 (StateManagerImpl_t703 * __this, Transform_t74 * ___arCameraTransform, TrackableResultDataU5BU5D_t698* ___trackableResultDataArray, int32_t ___originTrackableID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateTrackablePoses(UnityEngine.Transform,Vuforia.QCARManagerImpl/TrackableResultData[],System.Int32,System.Int32)
 void StateManagerImpl_UpdateTrackablePoses_m4168 (StateManagerImpl_t703 * __this, Transform_t74 * ___arCameraTransform, TrackableResultDataU5BU5D_t698* ___trackableResultDataArray, int32_t ___originTrackableID, int32_t ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateVirtualButtons(System.Int32,System.IntPtr)
 void StateManagerImpl_UpdateVirtualButtons_m4169 (StateManagerImpl_t703 * __this, int32_t ___numVirtualButtons, IntPtr_t121 ___virtualButtonPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateWords(UnityEngine.Transform,Vuforia.QCARManagerImpl/WordData[],Vuforia.QCARManagerImpl/WordResultData[])
 void StateManagerImpl_UpdateWords_m4170 (StateManagerImpl_t703 * __this, Transform_t74 * ___arCameraTransform, WordDataU5BU5D_t699* ___wordData, WordResultDataU5BU5D_t700* ___wordResultData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::AssociateVirtualButtonBehaviours(Vuforia.VirtualButtonAbstractBehaviour[],Vuforia.DataSet)
 void StateManagerImpl_AssociateVirtualButtonBehaviours_m4171 (StateManagerImpl_t703 * __this, VirtualButtonAbstractBehaviourU5BU5D_t773* ___vbBehaviours, DataSet_t612 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::CreateMissingDataSetTrackableBehaviours(Vuforia.DataSet)
 void StateManagerImpl_CreateMissingDataSetTrackableBehaviours_m4172 (StateManagerImpl_t703 * __this, DataSet_t612 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateImageTargetBehaviour(Vuforia.ImageTarget)
 ImageTargetAbstractBehaviour_t23 * StateManagerImpl_CreateImageTargetBehaviour_m4173 (StateManagerImpl_t703 * __this, Object_t * ___imageTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateMultiTargetBehaviour(Vuforia.MultiTarget)
 MultiTargetAbstractBehaviour_t33 * StateManagerImpl_CreateMultiTargetBehaviour_m4174 (StateManagerImpl_t703 * __this, Object_t * ___multiTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateCylinderTargetBehaviour(Vuforia.CylinderTarget)
 CylinderTargetAbstractBehaviour_t6 * StateManagerImpl_CreateCylinderTargetBehaviour_m4175 (StateManagerImpl_t703 * __this, Object_t * ___cylinderTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateObjectTargetBehaviour(Vuforia.ObjectTarget)
 ObjectTargetAbstractBehaviour_t36 * StateManagerImpl_CreateObjectTargetBehaviour_m4176 (StateManagerImpl_t703 * __this, Object_t * ___objectTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::InitializeMarkerBehaviour(Vuforia.MarkerAbstractBehaviour,Vuforia.Marker)
 void StateManagerImpl_InitializeMarkerBehaviour_m4177 (StateManagerImpl_t703 * __this, MarkerAbstractBehaviour_t32 * ___markerBehaviour, Object_t * ___marker, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::PositionCamera(Vuforia.TrackableBehaviour,UnityEngine.Transform,Vuforia.QCARManagerImpl/PoseData)
 void StateManagerImpl_PositionCamera_m4178 (StateManagerImpl_t703 * __this, TrackableBehaviour_t17 * ___trackableBehaviour, Transform_t74 * ___arCameraTransform, PoseData_t683  ___camToTargetPose, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::PositionTrackable(Vuforia.TrackableBehaviour,UnityEngine.Transform,Vuforia.QCARManagerImpl/PoseData)
 void StateManagerImpl_PositionTrackable_m4179 (StateManagerImpl_t703 * __this, TrackableBehaviour_t17 * ___trackableBehaviour, Transform_t74 * ___arCameraTransform, PoseData_t683  ___camToTargetPose, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::.ctor()
 void StateManagerImpl__ctor_m4180 (StateManagerImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
