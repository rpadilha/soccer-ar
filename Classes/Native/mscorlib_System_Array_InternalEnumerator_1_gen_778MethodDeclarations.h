﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.StringComparison>
struct InternalEnumerator_1_t5349;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.StringComparison
#include "mscorlib_System_StringComparison.h"

// System.Void System.Array/InternalEnumerator`1<System.StringComparison>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m32190 (InternalEnumerator_1_t5349 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.StringComparison>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m32191 (InternalEnumerator_1_t5349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.StringComparison>::Dispose()
 void InternalEnumerator_1_Dispose_m32192 (InternalEnumerator_1_t5349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.StringComparison>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m32193 (InternalEnumerator_1_t5349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.StringComparison>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m32194 (InternalEnumerator_1_t5349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
