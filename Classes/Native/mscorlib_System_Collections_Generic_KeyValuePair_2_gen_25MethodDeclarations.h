﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
struct KeyValuePair_2_t4722;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t999;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m28597(__this, ___key, ___value, method) (void)KeyValuePair_2__ctor_m18343_gshared((KeyValuePair_2_t3458 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Key()
#define KeyValuePair_2_get_Key_m28598(__this, method) (String_t*)KeyValuePair_2_get_Key_m18344_gshared((KeyValuePair_2_t3458 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m28599(__this, ___value, method) (void)KeyValuePair_2_set_Key_m18345_gshared((KeyValuePair_2_t3458 *)__this, (Object_t *)___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Value()
#define KeyValuePair_2_get_Value_m28600(__this, method) (GUIStyle_t999 *)KeyValuePair_2_get_Value_m18346_gshared((KeyValuePair_2_t3458 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m28601(__this, ___value, method) (void)KeyValuePair_2_set_Value_m18347_gshared((KeyValuePair_2_t3458 *)__this, (Object_t *)___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::ToString()
#define KeyValuePair_2_ToString_m28602(__this, method) (String_t*)KeyValuePair_2_ToString_m18348_gshared((KeyValuePair_2_t3458 *)__this, method)
