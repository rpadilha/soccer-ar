﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<System.String>
struct Comparison_1_t3917  : public MulticastDelegate_t373
{
};
