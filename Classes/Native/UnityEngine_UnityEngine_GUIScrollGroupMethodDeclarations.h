﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIScrollGroup
struct GUIScrollGroup_t1009;

// System.Void UnityEngine.GUIScrollGroup::.ctor()
 void GUIScrollGroup__ctor_m5790 (GUIScrollGroup_t1009 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIScrollGroup::CalcWidth()
 void GUIScrollGroup_CalcWidth_m5791 (GUIScrollGroup_t1009 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIScrollGroup::SetHorizontal(System.Single,System.Single)
 void GUIScrollGroup_SetHorizontal_m5792 (GUIScrollGroup_t1009 * __this, float ___x, float ___width, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIScrollGroup::CalcHeight()
 void GUIScrollGroup_CalcHeight_m5793 (GUIScrollGroup_t1009 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIScrollGroup::SetVertical(System.Single,System.Single)
 void GUIScrollGroup_SetVertical_m5794 (GUIScrollGroup_t1009 * __this, float ___y, float ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
