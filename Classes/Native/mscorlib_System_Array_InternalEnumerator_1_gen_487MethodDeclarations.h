﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>
struct InternalEnumerator_1_t4939;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29859 (InternalEnumerator_1_t4939 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860 (InternalEnumerator_1_t4939 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::Dispose()
 void InternalEnumerator_1_Dispose_m29861 (InternalEnumerator_1_t4939 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29862 (InternalEnumerator_1_t4939 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.TextureFormat>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29863 (InternalEnumerator_1_t4939 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
