﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Syntax.CompositeExpression
struct CompositeExpression_t1501;
// System.Text.RegularExpressions.Syntax.ExpressionCollection
struct ExpressionCollection_t1498;

// System.Void System.Text.RegularExpressions.Syntax.CompositeExpression::.ctor()
 void CompositeExpression__ctor_m7597 (CompositeExpression_t1501 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.CompositeExpression::get_Expressions()
 ExpressionCollection_t1498 * CompositeExpression_get_Expressions_m7598 (CompositeExpression_t1501 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.CompositeExpression::GetWidth(System.Int32&,System.Int32&,System.Int32)
 void CompositeExpression_GetWidth_m7599 (CompositeExpression_t1501 * __this, int32_t* ___min, int32_t* ___max, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.Syntax.CompositeExpression::IsComplex()
 bool CompositeExpression_IsComplex_m7600 (CompositeExpression_t1501 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
