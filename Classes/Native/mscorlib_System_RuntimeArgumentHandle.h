﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.RuntimeArgumentHandle
struct RuntimeArgumentHandle_t1769 
{
	// System.IntPtr System.RuntimeArgumentHandle::args
	IntPtr_t121 ___args_0;
};
