﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>
struct EqualityComparer_1_t4380;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>
struct EqualityComparer_1_t4380  : public Object_t
{
};
struct EqualityComparer_1_t4380_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>::_default
	EqualityComparer_1_t4380 * ____default_0;
};
