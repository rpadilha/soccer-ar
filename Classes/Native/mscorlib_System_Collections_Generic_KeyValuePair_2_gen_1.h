﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t15;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>
struct KeyValuePair_2_t890 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::value
	Object_t * ___value_1;
};
